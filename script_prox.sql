CREATE TABLE `dtb_logo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caminho` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `eventos_criados` ADD `frequencia` VARCHAR(500) NULL;


INSERT INTO `dtb_logo` (`id`, `caminho`) VALUES ('1', 'assets/img/logos/logo-relatorio.png');


CREATE TABLE `dtb_exclusao_simples_notificacao_historico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assunto` varchar(500) DEFAULT NULL,
  `recebida_em` datetime DEFAULT NULL,
  `caixa_postal_id` int(11) DEFAULT NULL,
  `cnpj` varchar(100) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome_usuario` varchar(500) DEFAULT NULL,
  `situacao` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `dtb_termo_intimacao_historico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assunto` varchar(500) DEFAULT NULL,
  `recebida_em` datetime DEFAULT NULL,
  `caixa_postal_id` int(11) DEFAULT NULL,
  `cnpj` varchar(100) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome_usuario` varchar(500) DEFAULT NULL,
  `situacao` varchar(1000) DEFAULT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `status_atual` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `dtb_exclusao_simples_notificacao_historico` ADD `descricao` VARCHAR(1000) NULL;

ALTER TABLE `dtb_exclusao_simples_notificacao_historico` ADD `status_atual` VARCHAR(500) NULL;



ALTER TABLE `dtb_malha_fiscal_historico` ADD `descricao` VARCHAR(1000) NULL;

ALTER TABLE `dtb_malha_fiscal_historico` ADD `status_atual` VARCHAR(500) NULL;


CREATE TABLE `dtb_situacao_fiscal_ecac_historico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cnpj` varchar(100) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome_usuario` varchar(500) DEFAULT NULL,
  `situacao` varchar(1000) DEFAULT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `status_atual` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `dtb_socios_empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cnpj_empresa` varchar(100) DEFAULT NULL,
  `cpf_cnpj` varchar(100) DEFAULT NULL,
  `nome_socio` varchar(1000) DEFAULT NULL,
  `situacao` varchar(500) DEFAULT NULL,
  `qualificacao` varchar(1000) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



UPDATE `dtb_certidao_receita` SET data_execucao = '2021-08-25 11:35:54';

UPDATE `dtb_certidao_caixa` SET data_execucao = '2021-08-25 11:35:54';



UPDATE `dtb_certidao_santa_catarina` SET data_execucao = '2021-08-25 11:35:54';

UPDATE `dtb_certidao_trabalhista` SET data_execucao = '2021-09-25 11:35:54';



UPDATE `dtb_certidao_florianopolis` SET data_execucao = '2021-08-25 11:35:54';

UPDATE `dtb_certidao_portoalegre` SET data_execucao = '2021-08-25 11:35:54';


UPDATE `dtb_certidao_estadual_sp` SET data_execucao = '2021-08-25 11:35:54';
