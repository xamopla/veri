<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Dívida Ativa</h2> 
        </div> 
      </div>
    </div>
  </div>

<div class="container-fluid general-widget">
  <div class="row">
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("dividas_ativas/listar_fgts"); ?>">   
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="alert-triangle"></i></div>
              <div class="media-body"><span class="m-0 text-white">FGTS</span>
                <h4 class="mb-0 counter"><?php echo $qtd_empresas_divida_fgts->valor; ?></h4><i class="icon-bg" data-feather="alert-triangle"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("dividas_ativas/listar_previdencia"); ?>">   
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="alert-triangle"></i></div>
              <div class="media-body"><span class="m-0 text-white">Previdenciária</span>
                <h4 class="mb-0 counter"><?php echo $qtd_empresas_divida_previdencia->valor; ?></h4><i class="icon-bg" data-feather="alert-triangle"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div> 

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("dividas_ativas/listar_nao_previdenciaria"); ?>">   
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="alert-triangle"></i></div>
              <div class="media-body"><span class="m-0 text-white">Não Previdenciária</span>
                <h4 class="mb-0 counter"><?php echo $qtd_empresas_divida_nao_previdencia->valor; ?></h4><i class="icon-bg" data-feather="alert-triangle"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div> 

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("dividas_ativas/listar_multa_trabalhista"); ?>">   
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="alert-triangle"></i></div>
              <div class="media-body"><span class="m-0 text-white">Multa Trabalhista</span>
                <h4 class="mb-0 counter"><?php echo $qtd_empresas_divida_multatrabalhista->valor; ?></h4><i class="icon-bg" data-feather="alert-triangle"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div> 

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("dividas_ativas/listar_multa_eleitoral"); ?>">   
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="alert-triangle"></i></div>
              <div class="media-body"><span class="m-0 text-white">Multa Eleitoral</span>
                <h4 class="mb-0 counter"><?php echo $qtd_empresas_divida_multaeleitoral->valor; ?></h4><i class="icon-bg" data-feather="alert-triangle"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("dividas_ativas/listar_multa_criminal"); ?>">   
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="alert-triangle"></i></div>
              <div class="media-body"><span class="m-0 text-white">Multa Criminal</span>
                <h4 class="mb-0 counter"><?php echo $qtd_empresas_divida_multacriminal->valor; ?></h4><i class="icon-bg" data-feather="alert-triangle"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>    
    
    </div>
  </div>
</div> 

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->