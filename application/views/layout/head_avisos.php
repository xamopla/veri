<?php defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
header("Cache-Control: private, max-age=3600");
if (! $this->session->has_userdata('userprimesession')) :
    redirect('login', 'refresh');
    endif;
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>VERI - Você no controle</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/app.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/bundles/izitoast/css/iziToast.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/bundles/bootstrap-social/bootstrap-social.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/bundles/owlcarousel2/dist/assets/owl.carousel.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/bundles/owlcarousel2/dist/assets/owl.theme.default.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/bundles/summernote/summernote-bs4.css'); ?>">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/components.css'); ?>">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url('assets/img/logo/favicon.png'); ?>' />

  <script src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>

  <script src="<?php echo base_url("assets/bundles/datatables/datatables.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/bundles/jquery-ui/jquery-ui.min.js"); ?>"></script>
  <!-- Page Specific JS File -->
  <!-- <script src="<?php echo base_url("assets/js/page/datatables.js"); ?>"></script> -->

  <script src="<?php echo base_url('assets/bundles/izitoast/js/iziToast.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/sweetalert.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/scripts/sweetalert-demo.js') ?>"></script>
  
  <script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
</head>
<body class="light light-sidebar theme-white sidebar-mini"> 