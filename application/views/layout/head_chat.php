<?php defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
header("Cache-Control: private, max-age=3600");
if (! $this->session->has_userdata('userprimesession')) :
    redirect('login', 'refresh');
    endif;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="VERI - Você no Controle">
    <meta name="keywords" content="sistema veri, veri soluções, monitoramento">
    <meta name="author" content="Grupo DataByte">
    <link rel="icon" href="<?php echo base_url('assets/images/logo/logo-icon.png'); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo-icon.png'); ?>" type="image/x-icon">
    <title>VERI - Você no Controle</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fontawesome.css'); ?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/icofont.css'); ?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/themify.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-icons/material-icons.css'); ?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/flag-icon.css'); ?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/feather-icon.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.css'); ?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/print.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/pe7-icon.css'); ?>">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
    <link id="color" rel="stylesheet" href="<?php echo base_url('assets/css/light-1.css'); ?>" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/scrollable.css'); ?>">

    <script src="<?php echo base_url('assets/js/scrollable/perfect-scrollbar.min.js'); ?>"></script>

  </head>
  <style type="text/css">
    .card-header{
        color:#24a0e9;
    }

    .modal-title{
        color:#24a0e9;
    }
  </style>
  <style type="text/css">
      .btn-primary{
        background-color: #24a0e9 !important;
      }
      .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active {
          background-color: #24a0e9 !important;
          border-color: #24a0e9 !important;
      }
      .dt-button{
         background: #24a0e9 !important;
      }
      .select2-container--default .select2-results__options .select2-results__option[aria-selected=true] {
        background-color: #24a0e9 !important;
        color: #fff !important;
        }

      .page-body{
        background-color: #f5f4f4!important;
      }

      .customizer-links{
        display: none;
      }

      .gradient-primary {
          background-image: -webkit-gradient(linear, left top, left bottom, from(#26c6da), to(#0288d1));
          background-image: linear-gradient(#26c6da, #0288d1);
        }

</style>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>Carregando..</h1>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
