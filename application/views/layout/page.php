<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/
</style>

<style type="text/css">
  .card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details {
    position: absolute;
    top: 0;
    left: 0;
    width: 70%;/*100%*/
    height: 100%;
    transition: .5s;
    transform-origin: left;
    transform: perspective(2000px) rotateY(-90deg);
}
.card:hover .details {
    transform: perspective(2000px) rotateY(0deg);
}
.card .details .center {
    width: 100% !important;
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details .center p {
    margin: 10px 0;
    padding: 0;
    color: #262626;
}
.card .details .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details .center ul li a:hover {
    background: #ff3636;
}


.div_hover { 
  background: white;
  border-radius: 50px; 
}

.div_hover a:hover { 
  background-color: #24a0e9; 
  color: white !important;
}

.tag_a_hover {
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.span_hover:hover{
  background-color: #24a0e9; 
  color: white !important;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.media-body{
  padding-left: 0px !important; 
}
</style>

<style type="text/css">

  .welcome-popup2 .modal-content {
  background-image: url(<?php echo base_url("assets/img/comunicado3.png"); ?>);
  /*background-image: url(../images/dashboard/popup.png);*/
  background-repeat: no-repeat;
  background-position: top center;
  border-radius: 30px;
  -webkit-box-shadow: 2px 12px 26px 3px rgba(47, 46, 46, 0.3);
          box-shadow: 2px 12px 26px 3px rgba(47, 46, 46, 0.3); }

  .welcome-popup2 .close {
    z-index: 9;
    position: absolute;
    background-color: #fff;
    color: #24a0e9;
    opacity: 1;
    border-radius: 15px;
    padding: 10px 15px;
    left: -20px;
    top: -10px;
    -webkit-box-shadow: -2px 6px 11px 3px rgba(126, 55, 216, 0.3);
            box-shadow: -2px 6px 11px 3px rgba(126, 55, 216, 0.3); }

  .welcome-popup2 .modal-header {
    height: 360px;
    border: none; }

  .welcome-popup2 .contain {
    padding-top: 80px; }

</style>
<?php
  if($modal == "teste"){
    $link = base_url("assets/Veri.pdf");
    echo '
          <div class="welcome-popup2 modal fade" id="loadModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <div class="modal-header"></div>
                  <div class="contain p-30">
                    <div class="text-center">
                      
                    </div>
                  </div>
                </div>
                <div style="text-align: center;">
                  <a href="'.$link.'" download class="btn btn-lg btn-default btn-rounded" style="background-color: white;
                    color: #24a0e9; margin-bottom: 10px;" >Tutorial</a>
                </div>
              </div>
            </div>
          </div>';
  }
?>

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Estadual</h2>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid general-widget">
    <!-- <div class="row"> -->
        <!-- <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #ff4e4e">MULTAS E AUTUAÇÕES</h5></div>

        <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #03a9f3">IRREGULARIDADES</h5></div>

        <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #00d74c">ACOMPANHAMENTO</h5></div>
      </div> -->

    <div class="row">
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("empresa/consultaintimadas"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Diário Oficial - Intimadas </span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas intimadas para inaptidão segundo o Diário Oficial' data-original-title='' title=''><?php echo $qtd_intimadas->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("empresa/descredenciadas"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Descre. Antec. Parcial</span>
                <h4 class="mb-0" ><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas descredenciadas por Antecipação Parcial' data-original-title='' title=''><?php echo $qtd_descredenciadas->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/divergencia1"); ?>">  
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
              </div>
              <div class="media-body"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Div. ICMS</span>
                <h4 class="mb-0 text-white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que possuem divergência entre o ICMS normal e o recolhido' data-original-title='' title=''><?php echo $qtd_div1->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("empresa/compras_e_vendas"); ?>"> 
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="align-self-center text-center" ></div>
              </div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px"><div style="width: 90px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Consultar MDF-e nas notas de compra' data-original-title='' title=''>MDF-e</div></span>
                <h4 class="mb-0 text-white"></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/os1"); ?>"> 
        <div class="card gradient-info o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="search"></div>
              </div>
              <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;">O.S. de Monitoramento</span>
                <h4 class="mb-0 text-white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas com O.S. de Monitoramento Ativa' data-original-title='' title=''><?php echo $qtd_osativa->valor; ?></div></h4><i class="icon-bg" data-feather="search"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div> -->

    </div>


    <div class="row">
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("empresa/empresasnaoativas"); ?>"> 
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Diário Oficial - Inaptas </span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas Inaptas segundo o Diário Oficial' data-original-title='' title=''><?php echo $qtd_nao_ativas->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

       <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/pafativo"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0" style="font-weight: bold;font-size: 18px">PAFs Ativos</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas com PAFs ativos' data-original-title='' title=''><?php echo $pafsativos->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/divergencia2"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0" style="font-weight: bold;font-size: 18px">Div. ICMS ST Antecipação</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que possuem Divergência entre o ICMS de Substituição Tributária por Antecipação e o recolhido' data-original-title='' title=''><?php echo $qtd_div2->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("empresa/listar"); ?>"> 
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i"></div>
              </div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px"><div data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Consultar a relação de DAE de uma empresa específica' data-original-title='' title=''>Relação de DAE</div></span>
                <h4 class="mb-0 text-white"></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      
    </div>


    <div class="row">
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/omissoefd"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">EFD - Omissos</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que estão com Omissão de EFD' data-original-title='' title=''><?php echo $qtd_omissoefd->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>
      
       <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/socioirregular"); ?>">   
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0" style="font-weight: bold;font-size: 18px">Sócio Irregular</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que possuem sócios irregulares' data-original-title='' title=''><?php echo $qtd_socioirregular->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/divergencia3"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" ></div>
              </div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Div. ICMS ST Retenção</span>
                <h4 class="mb-0 text-white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que possuem Divergência entre o ICMS de Substituição Tributária por Retenção e o recolhido' data-original-title='' title=''><?php echo $qtd_div3->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/os2"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">O.S. Ativas</span>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("resumofiscal/os1"); ?>" style="font-size: 18px;" class="tag_a_hover">Monitoramento: <?php echo $qtd_osativa->valor; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                        <a href="<?php echo base_url("resumofiscal/os2"); ?>" style="font-size: 18px;" class="tag_a_hover">Auditoria: <?php echo $qtd_osativa2->valor; ?></a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center">
                <div class="text-white i"></div>
              </div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">O.S. Ativas</span>
                <h4 class="mb-0 text-white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas com O.S. de Monitoramento e/ou Auditoria Ativa' data-original-title='' title=''><?php echo $qtd_osativa->valor+$qtd_osativa2->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

    </div>

    <div class="row">
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/omissodma"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">DMA - Omissos</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que estão com Omissão de DMA' data-original-title='' title=''><?php echo $qtd_omissodma->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>
      
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("ipva"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0" style="font-weight: bold;font-size: 18px">IPVA - Pendências</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas com Débito de IPVA' data-original-title='' title=''><?php echo $qtd_debito_ipva->qtd; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>
      
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("verificador_senhas"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Senhas Inválidas</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que estão com o login e senha Sefaz inválidos no Veri' data-original-title='' title=''><?php echo $qtd_empresas_senhas_invalidas->valor ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>

      

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("juceb/listar_dados"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body" style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <h1><br><span style="font-size: 20px;color: #24a0e9" class="span_hover">JUCEB</span></h1>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("juceb/registrosProxVencimento"); ?>" style="font-size: 18px;" class="tag_a_hover">Próximos a Vencer: <?php echo $qtd_juceb_prox_vencer; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("juceb/registrosVencidos"); ?>" style="font-size: 18px;" class="tag_a_hover">Vencidos: <?php echo $qtd_eventos_juceb_vencidos; ?></a>
                    </div>  
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">JUCEB</span>
                <h4 class="mb-0" style="color:white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que estão com o prazo para fazer alguma alteração contratual na JUCEB próximo a vencer ou vencido' data-original-title='' title=''><?php echo $qtd_eventos_juceb_vencidos + $qtd_juceb_prox_vencer; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
      </a>
      </div>
    </div>

   


  </div>
  <!-- Container-fluid Ends-->
</div>

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->

<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->


<script type="text/javascript">
  $(document).ready(function() {
    //var modal = document.getElementById('loadModal');
      if($('#loadModal') != null){
        $('#loadModal').modal('show');
      }

      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>
