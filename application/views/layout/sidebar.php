<style type="text/css">
  .iconbar-header2 {
      font-size: 10px;
      color: #0288d1;
      padding: 10px 0 10px 10px;
      background-color: #bde7fe;
      font-weight: 700;
  }

  .page-wrapper .page-body-wrapper .iconsidebar-menu .iconMenu-bar {
    width: 100px !important;
  }
</style>
<div class="page-body-wrapper">
<!-- Page Sidebar Start-->
<div class="iconsidebar-menu iconbar-mainmenu-close">
  <div class="sidebar">

    <?php
      $x = unserialize($this->session->userdata['userprimesession']['permissoes_nova']);
    ?>

    <?php if ($this->session->userdata['userprimesession']['nivel'] == 2){ ?>

      <ul class="iconMenu-bar custom-scrollbar">

        <?php if(array_key_exists('mMsgEcac', $x)): ?>
          <?php if ($x['mMsgEcac'] == 1): ?>
            <li><a class="bar-icons" href="#">
            <!--img(src='assets/images/menu/home.png' alt='')-->
            <i class="pe-7s-mail"></i><span style="">Mensagens</span></a>
            <ul class="iconbar-mainmenu custom-scrollbar">
              <li class="iconbar-header">Mensagens</li>
              <?php if(array_key_exists('mMsgEcac', $x)): ?>
                <?php if ($x['mMsgEcac'] == 1): ?>
                  <li><a href="<?php echo base_url("mensagens/"); ?>">Dashboard Mensagens</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mMsgDte', $x)): ?>
                <?php if ($x['mMsgDte'] == 1): ?>
                  <li><a href="<?php echo base_url("dec/listar_dec_recebidas"); ?>">Mensagens do DEC</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mMsgEcac', $x)): ?>
                <?php if ($x['mMsgEcac'] == 1): ?>
                <li><a href="<?php echo base_url("ecac/listar_mensagens"); ?>">Mensagens do ECAC</a></li>
                <?php endif ?>
              <?php endif ?>

            </ul>
          </li>
          <?php endif ?>
        <?php endif ?>

      

      <?php
        if(array_key_exists('mFederal', $x)){ ?>
          <?php if ($x['mFederal'] == 1): ?>
          <li><a class="bar-icons" href="#">
          <i class="pe-7s"><img src="<?php echo base_url("assets/img/icons/brasil3.png"); ?>" width="32px"></i><span>Federal</span></a>
            <ul class="iconbar-mainmenu custom-scrollbar">
              <li class="iconbar-header">Pendências Federais</li>
              <?php if(array_key_exists('mDashboardFederal', $x)): ?>
                <?php if ($x['mDashboardFederal'] == 1): ?>
                <li><a href="<?php echo base_url("painel_receita"); ?>">Dashboard Federal</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mSituacaoFiscalEcac', $x)): ?>
                <?php if ($x['mSituacaoFiscalEcac'] == 1): ?>
                <li><a href="<?php echo base_url("ecac/listar_situacao_fiscal_geral"); ?>">e-CAC- Diagnóstico Fiscal</a></li> 
                <?php endif ?>
              <?php endif ?>

              <li><a href="<?php echo base_url("ecac/listar_cadin"); ?>">CADIN</a></li>
              <li><a href="<?php echo base_url("dctf/index"); ?>">DCTF</a></li>
              <!-- <li><a href="<?php echo base_url("dctf/index"); ?>">DCTF</a></li> -->
            
              
            </ul>
          </li>
          <?php endif ?>  
      <?php } ?>

      <?php if(array_key_exists('mCertidaoNegativa', $x)): ?>
        <?php if ($x['mCertidaoNegativa'] == 1): ?>
        <li><a class="bar-icons" href="#">
          <i class="pe-7s"><img src="<?php echo base_url("assets/css/icones_header/certidao.png"); ?>" width="20px"></i><span>Certidões</span></a>
          <ul class="iconbar-mainmenu custom-scrollbar">
            <li class="iconbar-header">Certidões</li>
            <li><a href="<?php echo base_url("certidao_negativa/dashboard"); ?>">Dashboard Certidões</a></li>

            <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Federais</li> 
            
            <li><a href="<?php echo base_url("certidao_negativa/listar_federal_pgfn"); ?>">PGFN - Certidão de Débitos</a></li>
            
            

            <li><a href="<?php echo base_url("certidao_negativa/listar_caixa"); ?>">FGTS</a></li>
            <li><a href="<?php echo base_url("certidao_negativa/listar_trabalhista"); ?>">Trabalhista</a></li>
            

            <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Estaduais</li> 
            <li><a href="<?php echo base_url("certidao_negativa/listar_estadual"); ?>">Estadual</a></li>

            <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Municipais</li> 
            <li><a href="<?php echo base_url("certidao_negativa/listar_municipal"); ?>">Municipal</a></li>
          </ul>
        </li>
        <?php endif ?>
      <?php endif ?>

      <li><a class="bar-icons" href="#">
        <i class="pe-7s-hammer"></i><span>Processos</span></a>
        <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Processos</li>
          <!-- <li><a href="<?php echo base_url("processos/dashboard"); ?>">Dashboard Processos</a></li> -->

          <li><a href="<?php echo base_url("ecac/listar_eprocessos_todos"); ?>">e-Processos</a></li>

          <?php if(array_key_exists('mProcessosSipro', $x)): ?>
            <?php if ($x['mProcessosSipro'] == 1): ?>
              <!-- <li><a href="<?php echo base_url("consulta_sipro"); ?>">SIPRO</a></li>  -->
            <?php endif ?>
          <?php endif ?>

          <?php if(array_key_exists('mJucebViabilidade', $x)): ?>
            <?php if ($x['mJucebViabilidade'] == 1): ?>
              <!-- <li><a href="<?php echo base_url("juceb/consultaproabertos"); ?>">JUCEB - Consulta de Viabilidade</a></li>  -->
            <?php endif ?>
          <?php endif ?>

          <!-- <li><a href="<?php echo base_url("ecac/listar_mensagens"); ?>">REDE SIM</a></li> -->

          
          
        </ul>
      </li>

      

      <?php
        if(array_key_exists('mCalendario', $x)){ ?>
          <?php if ($x['mCalendario'] == 1): ?>
          <li><a class="bar-icons" href="<?php echo base_url("calendario/listar"); ?>"><i class="pe-7s"><img src="<?php echo base_url("assets/css/icones_header/calendario2.png"); ?>" width="20px"></i><span style="margin-left: -10%;">Tarefas</span></a></li>
          <?php endif ?>  
      <?php } ?>

      <li><a class="bar-icons" href="#">
        <i class="pe-7s"><img src="<?php echo base_url("assets/css/icones_header/icone_parcela2.png"); ?>" width="23px"></i><span style="margin-left: -15%;font-size: 12px;font-weight: bold;">Parcelamentos</span></a>
        <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Parcelamentos</li>

          <li><a href="<?php echo base_url("parcelamentos/dashboard"); ?>">Dashboard Parcelamentos</a></li>

          <?php if(array_key_exists('mParcelamentoDasEcac', $x)): ?>
            <?php if ($x['mParcelamentoDasEcac'] == 1): ?>
            <li><a href="<?php echo base_url("parcelamento_das/listar"); ?>">Simples Nacional - DAS</a></li>
            <?php endif ?>
          <?php endif ?>

          <!-- <li><a href="<?php echo base_url("ecac/listar_mensagens"); ?>">SEFAZ</a></li>
          <li><a href="<?php echo base_url("ecac/listar_mensagens"); ?>">REGULARIZE</a></li>
          <li><a href="<?php echo base_url("ecac/listar_mensagens"); ?>">REGIME NORMAL</a></li> -->
        </ul>
      </li>

      <li><a class="bar-icons" href="<?php echo base_url("diagnostico_veri/listar"); ?>">
        <i class="pe-7s"><img src="<?php echo base_url("assets/css/fab/images/diagnostico-veri.png"); ?>" width="53px"></i><span style="margin-left: -15%;font-size: 12px;font-weight: bold;">Diagnóstico Veri</span></a>
      </li>
      
      <!-- <?php
        if(array_key_exists('mRelatorios', $x)){ ?>
          <?php if ($x['mRelatorios'] == 1): ?>
          <li><a class="bar-icons" href="#"><i class="pe-7s-graph2"></i><span style="margin-left: -10%;">Relatórios</span></a>
            <ul class="iconbar-mainmenu custom-scrollbar">
              <li class="iconbar-header">Relatórios</li>
              <li><a href="<?php echo base_url("relatorio/dashboard"); ?>">Dashboard de Relatórios</a></li>
              
              <?php if(array_key_exists('mRelatorioPorEmpresas', $x)): ?>
                <?php if ($x['mRelatorioPorEmpresas'] == 1): ?>
                <li><a href="<?php echo base_url("relatorio/pendencia_empresa"); ?>">Pendências Por Empresa</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mRelatorioTimeline', $x)): ?>
                <?php if ($x['mRelatorioTimeline'] == 1): ?>
                <li><a href="<?php echo base_url("relatorio/timeline"); ?>">Timeline de Alterações</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mRelatorioGeral', $x)): ?>
                <?php if ($x['mRelatorioGeral'] == 1): ?>
                <li><a href="<?php echo base_url("empresa/relatoriogeral"); ?>">Geral</a></li>
                <li><a href="<?php echo base_url("relatorio/pendencia_resumo_fiscal"); ?>">Omissão DMA e EFD</a></li> 
                <?php endif ?>
              <?php endif ?>

              <li><a href="<?php echo base_url("empresa/contadornaovinculado"); ?>">Contador não Vinculado</a></li>
              
              <?php if(array_key_exists('mRelatorioPorColaborador', $x)): ?>
                <?php if ($x['mRelatorioPorColaborador'] == 1): ?>
                <li><a href="<?php echo base_url("relatorioempresacolaborador/gerar"); ?>">Empresas por Colaborador</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mRelatorioSemColaborador', $x)): ?>
                <?php if ($x['mRelatorioSemColaborador'] == 1): ?>
                <li><a href="<?php echo base_url("relatorioempresacolaborador/gerarEmpresaSemColaborador"); ?>">Empresas sem Colaborador</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mRelatorioComCertificado', $x)): ?>
                <?php if ($x['mRelatorioComCertificado'] == 1): ?>
                <li><a href="<?php echo base_url("ecac/listar_empresas_com_certificado"); ?>">Empresas com Certificado</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mRelatorioSemCertificado', $x)): ?>
                <?php if ($x['mRelatorioSemCertificado'] == 1): ?>
                <li><a href="<?php echo base_url("ecac/listar_empresas_sem_certificado"); ?>">Empresas sem Certificado</a></li>
                <?php endif ?>
              <?php endif ?>
            </ul>
          </li>
          <?php endif ?>  
      <?php } ?> -->
          

      
          <li><a class="bar-icons" href="#"><i class="pe-7s-tools"></i><span>Painel</span></a>
            <ul class="iconbar-mainmenu custom-scrollbar">
              <li class="iconbar-header">Painel</li>

              <?php
               if(array_key_exists('mProtocolos', $x)){ ?>
                <?php if ($x['mProtocolos'] == 1): ?>
                  <li><a href="<?php echo base_url("protocolo/listar"); ?>">Protocolo de Notificação</a></li>
                <?php endif ?>  
              <?php } ?>

              <?php
              if(array_key_exists('mCadastros', $x)){ ?>
                <?php if ($x['mCadastros'] == 1): ?>
                <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Cadastros</li> 
                <?php if(array_key_exists('mEmpresas', $x)): ?>
                  <?php if ($x['mEmpresas'] == 1): ?>
                  <li><a href="<?php echo base_url("empresa/listar"); ?>">Empresas</a></li>
                  <?php endif ?>
                <?php endif ?>

                <?php if(array_key_exists('mUsuarios', $x)): ?>
                  <?php if ($x['mUsuarios'] == 1): ?>
                  <li><a href="<?php echo base_url("funcionario/listar"); ?>">Usuários</a></li>
                  <?php endif ?>
                <?php endif ?>

                <?php if(array_key_exists('mContadores', $x)): ?>
                  <?php if ($x['mContadores'] == 1): ?>
                  <li><a href="<?php echo base_url("contador/listar"); ?>">Procurações</a></li>
                  <?php endif ?>
                <?php endif ?>

                <?php if(array_key_exists('mDocumentos', $x)): ?>
                  <?php if ($x['mDocumentos'] == 1): ?>
                  <li><a href="<?php echo base_url("documento/listar"); ?>">Documentos</a></li>
                  <?php endif ?>
                <?php endif ?>

                <?php if(array_key_exists('mTiposDeDocumentos', $x)): ?>
                  <?php if ($x['mTiposDeDocumentos'] == 1): ?>
                  <li><a href="<?php echo base_url("tipodocumento/listar"); ?>">Tipos de Documentos</a></li>
                  <?php endif ?>
                <?php endif ?>
              <?php endif ?>  
            <?php } ?>


              <li class="iconbar-header">Configurações</li>
              <?php if(array_key_exists('mRegistroDeAcesso', $x)): ?>
                <?php if ($x['mRegistroDeAcesso'] == 1): ?>
                <li><a href="<?php echo base_url("registrodeacesso/listar"); ?>">Registro de Acesso</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mPlano', $x)): ?>
                <?php if ($x['mPlano'] == 1): ?>
                <li><a href="<?php echo base_url("meuplano"); ?>">Plano Contratado</a></li>
                <?php endif ?>
              <?php endif ?>

              <?php if(array_key_exists('mPainelCliente', $x)): ?>
                <?php if ($x['mPainelCliente'] == 1): ?>
                <li><a href="https://databyte.superlogica.net/clients/areadocliente" target="_blank">Painel do Cliente</a></li>
                <?php endif ?>
              <?php endif ?>
            </ul>
          </li> 
          
          <li><a class="bar-icons" href="<?php echo base_url("suporte/listar_cliente"); ?>"><i class=" pe-7s-headphones"></i><span style="margin-left: -10%;">Suporte</span></a> 

    </ul>
    <?php } else { ?>

      <ul class="iconMenu-bar custom-scrollbar">

      <?php if ($this->session->userdata['userprimesession']['nivel'] == 0): ?>
          <li><a class="bar-icons" href="<?php echo base_url("suporte/listar"); ?>"><i class="pe-7s-tools"></i><span style="margin-left: -10%;">Suporte</span></a> 
        <?php endif ?>
        
      <li><a class="bar-icons" href="<?php echo base_url("mensagens/"); ?>">
        <!--img(src='assets/images/menu/home.png' alt='')-->
        <i class="pe-7s-mail"></i><span style="">Mensagens</span></a>
        <!-- <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Mensagens</li>
          <li><a href="<?php echo base_url("mensagens/"); ?>">Dashboard Mensagens</a></li>
          <li><a href="<?php echo base_url("caixadeemail/listar_aux_nova"); ?>">Mensagens do DTE</a></li>
          <li><a href="<?php echo base_url("ecac/listar_mensagens"); ?>">Mensagens do e-CAC</a></li>
        </ul> -->
      </li>

      

      <li><a class="bar-icons" href="<?php echo base_url("painel_receita"); ?>">
          <i class="pe-7s"><img src="<?php echo base_url("assets/img/icons/brasil3.png"); ?>" width="32px"></i><span>Federal</span></a>
        <!-- <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Pendências Federais</li>
          <li><a href="<?php echo base_url("painel_receita"); ?>">Dashboard Federal</a></li>

          <li><a href="<?php echo base_url("ecac/listar_situacao_fiscal_geral"); ?>">e-CAC - Diagnóstico Fiscal</a></li>

          <li><a href="<?php echo base_url("dctf/index"); ?>">DCTF</a></li>


          <li><a href="<?php echo base_url("ecac/listar_cadin"); ?>">Cadin</a></li>

        
          <li><a href="<?php echo base_url("dividas_ativas"); ?>">Regularize - Dívida Ativa</a></li>
        </ul> -->
      </li>

      <li><a class="bar-icons" href="<?php echo base_url("certidao_negativa/dashboard"); ?>">
          <i class="pe-7s"><img src="<?php echo base_url("assets/css/icones_header/certidao.png"); ?>" width="20px"></i><span>Certidões</span></a>
        <!-- <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Certidões</li>
          <li><a href="<?php echo base_url("certidao_negativa/dashboard"); ?>">Dashboard Certidões</a></li>

          <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Federais</li> 
          
          <li><a href="<?php echo base_url("certidao_negativa/listar_federal_pgfn"); ?>">PGFN - Certidão de Débitos</a></li>
          
          <li><a href="<?php echo base_url("certidao_negativa/listar_caixa"); ?>">FGTS</a></li>
          <li><a href="<?php echo base_url("certidao_negativa/listar_trabalhista"); ?>">Trabalhista</a></li>
          
           
          <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Estaduais</li> 
          <li><a href="<?php echo base_url("certidao_negativa/listar_estadual"); ?>">Estadual</a></li>

          <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Municipais</li> 
          <li><a href="<?php echo base_url("certidao_negativa/listar_municipal"); ?>">Municipal</a></li>
        </ul> -->
      </li>

      <li><a class="bar-icons" href="<?php echo base_url("processos/dashboard"); ?>">
        <i class="pe-7s-hammer"></i><span>Processos</span></a>
        <!-- <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Processos</li>
          <li><a href="<?php echo base_url("processos/dashboard"); ?>">Dashboard Processos</a></li>

          <li><a href="<?php echo base_url("juceb/consultaproabertos"); ?>">JUCEB - Consulta de Viabilidade</a></li>
          <li><a href="<?php echo base_url("ecac/listar_eprocessos_todos"); ?>">e-Processos</a></li>
          <li><a href="<?php echo base_url("consulta_sipro"); ?>">SIPRO</a></li> 

            
          
        </ul> -->
      </li>

      

      <li><a class="bar-icons" href="<?php echo base_url("calendario/listar"); ?>"><i class="pe-7s"><img src="<?php echo base_url("assets/css/icones_header/calendario2.png"); ?>" width="20px"></i><span style="margin-left: -10%;">Tarefas</span></a></li>

      <li><a class="bar-icons" href="<?php echo base_url("parcelamentos/dashboard"); ?>">
        

        <i class="pe-7s"><img src="<?php echo base_url("assets/css/icones_header/icone_parcela2.png"); ?>" width="23px"></i><span style="margin-left: -15%;font-size: 12px;font-weight: bold;">Parcelamentos</span></a>
        <!-- <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Parcelamentos</li>
          <li><a href="<?php echo base_url("parcelamentos/dashboard"); ?>">Dashboard Parcelamentos</a></li>

          <li><a href="<?php echo base_url("parcelamento_das/listar"); ?>">Simples Nacional - DAS</a></li>
        </ul> -->
      </li>
      
      <!-- <li><a class="bar-icons" href="<?php echo base_url("relatorio/dashboard"); ?>"><i class="pe-7s-graph2"></i><span >Relatórios</span></a> -->
        <!-- <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Relatórios</li>
          <li><a href="<?php echo base_url("relatorio/dashboard"); ?>">Dashboard de Relatórios</a></li>

          <li><a href="<?php echo base_url("relatorio/pendencia_empresa"); ?>">Pendências Por Empresa</a></li>
          <li><a href="<?php echo base_url("relatorio/timeline"); ?>">Timeline de Alterações</a></li>
          <li><a href="<?php echo base_url("empresa/relatoriogeral"); ?>">Geral</a></li>
          <li><a href="<?php echo base_url("relatorio/pendencia_resumo_fiscal"); ?>">Omissão DMA e EFD</a></li>  
          <li><a href="<?php echo base_url("empresa/contadornaovinculado"); ?>">Contador não Vinculado</a></li>
          <li><a href="<?php echo base_url("relatorioempresacolaborador/gerar"); ?>">Empresas por Colaborador</a></li>
          <li><a href="<?php echo base_url("relatorioempresacolaborador/gerarEmpresaSemColaborador"); ?>">Empresas sem Colaborador</a></li>
          <li><a href="<?php echo base_url("ecac/listar_empresas_com_certificado"); ?>">Empresas com Certificado</a></li>
          <li><a href="<?php echo base_url("ecac/listar_empresas_sem_certificado"); ?>">Empresas sem Certificado</a></li>
        </ul> -->
      <!-- </li> -->

      <li><a class="bar-icons" href="<?php echo base_url("diagnostico_veri/listar"); ?>">
        <i class="pe-7s"><img src="<?php echo base_url("assets/css/fab/images/diagnostico-veri.png"); ?>" width="53px"></i><span style="margin-left: -15%;font-size: 12px;font-weight: bold;">Diagnóstico Veri</span></a>
      </li>

      <li><a class="bar-icons" href="#"><i class="pe-7s-tools"></i><span>Painel</span></a>
        <ul class="iconbar-mainmenu custom-scrollbar">
          <li class="iconbar-header">Painel</li>

          <li><a href="<?php echo base_url("protocolo/listar"); ?>">Protocolo de Notificação</a></li>

          <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Cadastros</li> 
          <li><a href="<?php echo base_url("empresa/listar"); ?>">Empresas</a></li>
          <li><a href="<?php echo base_url("funcionario/listar"); ?>">Usuários</a></li>
          <li><a href="<?php echo base_url("contador/listar"); ?>">Procurações</a></li>

          <?php 
          $hostCompleto = $_SERVER['HTTP_HOST'];
          $server = explode('.', $hostCompleto);
          $server = $server[0];

          $banco = $server;

          ?>

          <?php if($banco != 'demo'){ ?>
            <li><a href="<?php echo base_url("documento/listar"); ?>">Documentos</a></li>
            <li><a href="<?php echo base_url("tipodocumento/listar"); ?>">Tipos de Documentos</a></li>
          <?php } ?>
          
          <!-- <li><a href="<?php echo base_url("documento/listar"); ?>">Documentos</a></li>
          <li><a href="<?php echo base_url("tipodocumento/listar"); ?>">Tipos de Documentos</a></li> -->

          <li class="iconbar-header2" style="margin-top: 5px;font-size: 15px;">Configurações</li> 
          <li><a href="<?php echo base_url("registrodeacesso/listar"); ?>">Registro de Acesso</a></li>
          <li><a href="<?php echo base_url("caixadeemail/listar"); ?>">Caixa de Email</a></li>
          <li><a href="<?php echo base_url("meuplano"); ?>">Plano Contratado</a></li>
          <li><a href="https://databyte.superlogica.net/clients/areadocliente" target="_blank">Painel do Cliente</a></li> 
        </ul>
      </li> 

      <li><a class="bar-icons" href="<?php echo base_url("suporte/listar_cliente"); ?>"><i class=" pe-7s-headphones"></i><span style="margin-left: -10%;">Suporte</span></a> 
     
      <!-- <li><a class="bar-icons" href="<?php echo base_url("login/logout"); ?>"><i class="pe-7s-bottom-arrow"></i><span>Sair</span></a>  -->        
      </li>
    </ul>
    <?php } ?>
    
  </div>
</div>
<!-- Page Sidebar Ends-->
<!-- Right sidebar Start-->

