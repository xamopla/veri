<!-- footer start-->
<footer class="footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 footer-copyright">
        <p class="mb-0">Copyright &copy; 2020 <b>Veri</b> - Todos os direitos reservados. <a href="https://www.veri.com.br/doc-system/TermosDeUso.pdf" style="color: blue;" target="_blank" rel="noopener">Termo de Uso</a> do sistema.</p>
      </div> 
    </div>
  </div>
</footer>
</div>
</div>

</body>
</html>