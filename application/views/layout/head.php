<?php defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
header("Cache-Control: private, max-age=3600");
if (! $this->session->has_userdata('userprimesession')) :
    redirect('login', 'refresh');
    endif;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="VERI - Você no Controle">
    <meta name="keywords" content="sistema veri, veri soluções, monitoramento">
    <meta name="author" content="Grupo DataByte">
    <link rel="icon" href="<?php echo base_url('assets/images/logo/logo-icon.png'); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo-icon.png'); ?>" type="image/x-icon">
    <title>VERI - Você no Controle</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fontawesome.css'); ?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/icofont.css'); ?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/themify.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-icons/material-icons.css'); ?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/flag-icon.css'); ?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/feather-icon.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.css'); ?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/print.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/pe7-icon.css'); ?>">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
    <link id="color" rel="stylesheet" href="<?php echo base_url('assets/css/light-1.css'); ?>" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/scrollable.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fab/normalize.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/ionic-icon.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fab/mfb.css'); ?>">

    <script src="<?php echo base_url('assets/js/scrollable/perfect-scrollbar.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/fab/modernizr.touch.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/fab/mfb.js'); ?>"></script>

    <script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>

  </head>
  <style type="text/css">
    .card-header{
        color:#24a0e9;
    }

    .modal-title{
        color:#24a0e9;
    }
  </style>
  <style type="text/css">
      .btn-primary{
        background-color: #24a0e9 !important;
      }
      .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active {
          background-color: #24a0e9 !important;
          border-color: #24a0e9 !important;
      }
      .dt-button{
         background: #24a0e9 !important;
      }
      .select2-container--default .select2-results__options .select2-results__option[aria-selected=true] {
        background-color: #24a0e9 !important;
        color: #fff !important;
        }

      .page-body{
        background-color: #f5f4f4!important;
      }

      .customizer-links{
        display: none;
      }

      .gradient-primary {
          background-image: -webkit-gradient(linear, left top, left bottom, from(#26c6da), to(#0288d1));
          background-image: linear-gradient(#26c6da, #0288d1);
        }

</style>

<style type="text/css">
  .float_b{
  position:fixed;
  width:60px;
  height:60px;
  bottom:40px;
  right:40px;
  background-color:#25d366;
  color:#FFF;
  border-radius:50px;
  text-align:center;
  font-size:30px;
  box-shadow: 2px 2px 3px #999;
  z-index:100;
}

.my-float{
  margin-top:16px;
}
</style>

<script type="text/javascript">
  $(document).ready(function() {
    // monitora_outras_conversas();
  });

  function monitora_outras_conversas(){
    var id_usuario = 0;
    $.ajax({
        type: 'POST',
          url: "<?php echo base_url(); ?>chat/monitora_outras_conversas_head/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
        cache: false,
        success: function (data) {
          
          $.each(data, function(i, item) {
            
            $("#numero_msg_chat_icon").text(item.qtd);
            $("#numero_msg_chat").text(item.qtd);

            if(item.notificacao != 1){
              play();
            }
            
          });
          
        },
    });
    setTimeout(monitora_outras_conversas, 5000);
  }

function play(){
    audio = document.getElementById('audio_teste');
    audio.play();
  }
</script>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>Carregando..</h1>
      </div>
    </div>

    <div style="display: none;">
      <audio controls id="audio_teste">
        <source src="<?php echo base_url('assets/audio/alert.mp3'); ?>" type="audio/mp3">
      </audio>
    </div>

    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <a href="https://wa.me/message/XXIYOL2WZOMNH1" class="float_b" target="_blank" data-toggle="tooltip" data-placement="top" title="Suporte">
      <i class="fa fa-whatsapp my-float"></i>
      </a>
      
      <!-- <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
      <li class="mfb-component__wrap">
        <span class="badge2 badge2-pill badge2-secondary2" style="float: right;border-radius: 50%;width: 15px;text-align: center;" id="numero_msg_chat"></span>
        <a href="#" class="mfb-component__button--main">
          <i class="mfb-component__main-icon--resting ion ion-plus-round"></i>
          <i class="mfb-component__main-icon--active ion-close-round"></i>
        </a>
        <ul class="mfb-component__list">

          <li>
            <span class="badge2 badge2-pill badge2-secondary2" style="float: right;border-radius: 50%;width: 15px;text-align: center;" id="numero_msg_chat_icon">0</span>
            <a href="<?php echo base_url("chat"); ?>" data-mfb-label="Chat" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-chatboxes"></i>
            </a>
          </li>
        </ul>
      </li>
    </ul> -->