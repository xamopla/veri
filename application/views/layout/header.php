<style type="text/css">
  .page-main-header .main-header-right .nav-right .notification-box {
    position: relative;
}
.page-main-header .main-header-right .nav-right .notification-box svg {
    
}
.page-main-header .main-header-right .nav-right .notification-box .badge2 {
    position: absolute;
    right: -5px;
    top: -6px;
    padding: 2px 4px;
    font-size: 10px;
    font-weight: 700;
    font-family: work-Sans, sans-serif;
    border-radius: 10rem;
}

.page-main-header .main-header-right .nav-right .notification-box .badge3 {
    position: absolute;
    /*right: -5px;*/
    /*top: -6px;*/
    bottom: -30px;
    padding: 2px 4px;
    font-size: 12px;
    font-weight: 700;
    left: -8px;
    font-family: work-Sans, sans-serif;
    border-radius: 10rem;
}

.badge3-secondary2 {
    background-color: #f10505;
    color: #fff;
}

.badge2-secondary2 {
    background-color: #f10505;
    color: #fff;
}

.page-main-header .main-header-right .nav-right ul li svg {
    margin-top: 10px;
    width: 28px;
    height: 22px;
}

.scroll-demo2 {
    /* padding: 10px; */
    position: relative;
    border: 1px solid #f8f5fd;
    overflow: auto;
    height: 300px;
}


.page-main-header .main-header-right .nav-right > ul > li .badge {
    display: inline !important;
}

.li_nova{
    width: 50px !important;
    height: 50px !important;
    background-color: #f8f5fd !important;
    border-radius: 100% !important;
    display: -webkit-box !important;
    display: -ms-flexbox !important;
    display: flex !important;
    -webkit-box-align: center !important;
    -ms-flex-align: center !important;
    align-items: center !important;
    -webkit-box-pack: center !important;
    -ms-flex-pack: center !important;
    justify-content: center !important;
    cursor: pointer !important;
    margin-top: 6px !important;
    margin-right: 12px;
}

.li_nova2{
    width: 60px !important;
    height: 60px !important;
    background-color: #f8f5fd !important;
    border-radius: 100% !important;
    display: -webkit-box !important;
    display: -ms-flexbox !important;
    display: flex !important;
    -webkit-box-align: center !important;
    -ms-flex-align: center !important;
    align-items: center !important;
    -webkit-box-pack: center !important;
    -ms-flex-pack: center !important;
    justify-content: center !important;
    cursor: pointer !important;
    margin-top: 6px !important;
    margin-left: 20px !important;
    margin-right: 20px !important;
}
</style>

<?php
  $x = unserialize($this->session->userdata['userprimesession']['permissoes_nova']);
?>

<div class="page-main-header" style="background: linear-gradient(to left, #0f68d7 , #00c6ff) !important;">
  <div class="main-header-right" style="padding-bottom: 5px;">
    <div class="main-header-left text-center">
      <?php if ($this->session->userdata['userprimesession']['nivel'] == 2){ ?>
      <div class="logo-wrapper"><a href="#"><img style="width: 120px;" src="<?php echo base_url('assets/img/logos/logo-branca-inline-1.png'); ?>" alt=""></a></div>
      <?php }else{  ?>
        <div class="logo-wrapper"><a href="<?php echo base_url("painel/home"); ?>"><img style="width: 120px;" src="<?php echo base_url('assets/img/logos/logo-branca-inline-1.png'); ?>" alt=""></a></div>
      <?php } ?>
    </div>
    <div class="mobile-sidebar">
      <div class="media-body text-right switch-sm">
        <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
      </div>
    </div>
    <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar">               </i></div>
    <div class="nav-right col pull-right right-menu">
      <ul class="nav-menus">
        <li>
          
        </li>

        <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-maximize"><path d="M8 3H5a2 2 0 0 0-2 2v3m18 0V5a2 2 0 0 0-2-2h-3m0 18h3a2 2 0 0 0 2-2v-3M3 16v3a2 2 0 0 0 2 2h3" style="color: white;"></path></svg></a></li>

       <!-- Mensages -->
       <?php if ($this->session->userdata['userprimesession']['nivel'] == 2){ ?>

          <?php if(array_key_exists('mMsgEcac', $x)): ?>
            <?php if ($x['mMsgEcac'] == 1): ?>
            <li class="onhover-dropdown li_nova">
              <img src="<?php echo base_url('assets/css/icones_header/mensagens.png'); ?>" style="width: 30px;margin-bottom: 5px;">
                
                <div class="notification-box text-white" id="div_notificacoes_mensagens">

                </div>
            </li>
            <?php endif ?>
          <?php endif ?>
        
         <?php }else{ ?>

          <li class="onhover-dropdown li_nova">
            <img src="<?php echo base_url('assets/css/icones_header/mensagens.png'); ?>" style="width: 30px;margin-bottom: 5px;">
              
              <div class="notification-box text-white" id="div_notificacoes_mensagens">

              </div>
          </li>

         <?php } ?>

        <!-- ESTADUAIS -->
        <!-- <li class="onhover-dropdown li_nova">
          <img src="<?php echo base_url('assets/css/icones_header/estadual.png'); ?>" style="width: 40px;margin-bottom: 5px;">
            
            <div class="notification-box text-white" id="div_notificacoes_estaduais">

            </div>
        </li> -->

        <!-- FEDERAIS -->
        <li class="onhover-dropdown li_nova">
          <img src="<?php echo base_url('assets/css/icones_header/federal.png'); ?>" style="width: 30px;margin-bottom: 5px;">
            
            <div class="notification-box text-white" id="div_notificacoes_federais">

            </div>
        </li>

        <!-- CERTIDÕES -->
        <li class="onhover-dropdown li_nova">
          <img src="<?php echo base_url('assets/css/icones_header/certidao.png'); ?>" style="width: 30px;margin-bottom: 5px;">
            
            <div class="notification-box text-white" id="div_notificacoes_certidoes">

            </div>
        </li>

        <!-- PROCESSOS -->
        <li class="onhover-dropdown li_nova">
          <img src="<?php echo base_url('assets/css/icones_header/processos.png'); ?>" style="width: 30px;margin-bottom: 5px;">
            
            <div class="notification-box text-white" id="div_notificacoes_processos">

            </div>
        </li>

        <!-- TAREFAS -->
        <?php if ($qtd_notificacoes_documentos->valor){ ?>

          <li class="onhover-dropdown li_nova">
            <img src="<?php echo base_url('assets/css/icones_header/calendario2.png'); ?>" style="width: 30px;margin-bottom: 5px;">
              
              <div class="notification-box text-white">
                <span class="badge3 badge3-pill badge3-secondary2" style="bottom: -35px !important;" id="span_qtd_vencimentos"><?php echo $qtd_notificacoes_documentos->valor; ?></span>
              </div>

              <ul class="onhover-show-div notification-dropdown">
                <div class="vertical-scroll scroll-demo2" id="div_vencimentos">
                <li class="gradient-primary">
                  <h5 class="f-w-700">Vencimentos</h5><a href="<?php echo base_url("documento/listar"); ?>" style="color: white"><span id="span_texto_qtd_vencimentos">Você tem <?php echo $qtd_notificacoes_documentos->valor; ?> novas notificações</span></a>
                </li>

                <?php 
                $cont = 0;
                foreach ($notificacoes_documentos as $d) { 

                $data_final = strtotime($d->dataValidade);
                $data_atual = strtotime(date(''). ' + 30 days');

                $df = new DateTime($d->dataValidade);
                $da = new DateTime(date(''));
                $intervalo = $df->diff($da);

                    if ($data_final < strtotime(date('Y-m-d'))){
                      $corbolinha = '#DD4B39';
                      $descricao = "Vencido";
                      $descricao2 = "venceu";
                      $descricao3 = "dias atrás";
                      $tag = 'danger';

                    } else if ($intervalo->format('%a') <= 30) {
                      $corbolinha = '#F39C12';
                      $descricao = "Próximo de Vencer";
                      $descricao2 = "faltam";
                      $descricao3 = "dias";

                      $tag = 'warning';
                    }

                    else {
                      $corbolinha = '#2ECC71'; 
                      $tag = 'success';
                    }
                ?>

                  
                <li>
                  <div class="media">
                    <div class="notification-icons bg-<?php echo $tag; ?> mr-3"><i class="mt-0" data-feather="calendar" style="color: white;"></i></div>
                    <div class="media-body">
                      <a href="<?php echo base_url("documento/listar/$d->id_empresa"); ?>">
                      <h6></i><?php echo "Um documento da empresa $d->razao_social está $descricao, $descricao2 ".$intervalo->format('%a')." $descricao3."; ?></h6>
                      <p class="mb-0"><?php echo date('d/m/Y'); ?></p>
                      </a>
                    </div>
                  </div>
                </li>


                <?php  $cont++; } ?>
                </div>
                <!-- <li class="bg-light txt-dark"><a href="<?php echo base_url("documento/listar"); ?>">Ver todos </a> os documentos</li> -->

              </ul>

          </li>

        <?php } else { ?>
          <li class="onhover-dropdown li_nova">
            <img src="<?php echo base_url('assets/css/icones_header/calendario2.png'); ?>" style="width: 30px;margin-bottom: 5px;">
              
              <div class="notification-box text-white">
                <!-- <span class="badge3 badge3-pill badge3-secondary2" id="span_qtd_vencimentos">0</span> -->
              </div>
              <ul class="onhover-show-div notification-dropdown">
                <li class="gradient-primary">
                  <h5 class="f-w-700">Vencimentos</h5><span>Você não tem nenhuma nova notificação</span>
                </li>
              </ul>
          </li> 
        <?php } ?>


        <!-- PARCELAMENTOS -->
        <li class="onhover-dropdown li_nova">
          <img src="<?php echo base_url('assets/css/icones_header/icone_parcela.jpeg'); ?>" style="width: 30px;margin-bottom: 5px;margin-left: 5px;">
            
            <div class="notification-box text-white" id="div_notificacoes_parcelamentos">

            </div>
        </li>
        
         <!-- ATUALIZAÇÕES -->
       <li class="onhover-dropdown li_nova">
          <img src="<?php echo base_url('assets/css/icones_header/rocket.png'); ?>" style="width: 30px;margin-bottom: 5px;">
            
            <div class="notification-box text-white" id="div_notificacoes_atualizacoes">

            </div>
        </li>

        <!-- Menu de perfil -->
        <li class="onhover-dropdown li_nova2"> <img class="img-fluid" src="<?php echo base_url('assets/css/icones_header/user2.png'); ?>" alt="" style="filter: invert(0); width: 60px !important;margin-right: 50%;">
          <ul class="onhover-show-div profile-dropdown">
            <li class="gradient-primary">
              <h6 class="f-w-600 mb-0">
              <?php 
                if ($this->session->userdata['userprimesession']['nivel'] == 1){
                  echo $this->session->userdata['userprimesession']['razao_social'];
                } else {
                  echo $this->session->userdata['userprimesession']['nome']; 
                }
              ?>
              </h6>
            </li>
            <?php if ($this->session->userdata['userprimesession']['nivel'] != 2){ ?>
              <li><a href="<?php echo base_url("usuario/perfil/").$this->session->userdata['userprimesession']['id']; ?>"><i data-feather="user"> </i>Perfil</a></li>
              <li><a href="<?php echo base_url("meuplano"); ?>"><i data-feather="book" > </i>Meu Plano</a></li>
            <?php } ?>

            <?php if ($this->session->userdata['userprimesession']['nivel'] == 2){ ?>
              <li><a href="<?php echo base_url("usuario/alterarsenha/").$this->session->userdata['userprimesession']['id']; ?>"><i data-feather="lock"> </i>Alterar Senha</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url("tutorial/listar/") ?>"><i data-feather="book-open"> </i>Tutoriais</a></li>
            <li><a href="<?php echo base_url("changelog") ?>"><i data-feather="list"> </i>Log</a></li>

            <?php 
              $hostCompleto = $_SERVER['HTTP_HOST'];
              $server = explode('.', $hostCompleto);
              $server = $server[0];


              if($server == "demo"){
               echo '<li><a href="https://portal-databyte.npcsolucoes.com.br" target="_blank"><i data-feather="file-text"> </i>Contratos</a></li>';
              }
            ?>

            <li><a href="<?php echo base_url("login/logout"); ?>"><i data-feather="log-out"> </i>Sair</a></li>
          </ul>
        </li>
      </ul>
      <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
    </div>
    <script id="result-template" type="text/x-handlebars-template">
      <div class="ProfileCard u-cf">                        
      <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
      <div class="ProfileCard-details">
      <div class="ProfileCard-realName">{{name}}</div>
      </div>
      </div>
    </script>
    <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
  </div>
</div>

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>

<script type="text/javascript">
  function marcar_msg_dte_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_msg_dte_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_msg_dte_lida2(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_msg_dte_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_msg_ecac_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_msg_ecac_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_situacao_lida(id_notificacao){
    $.post("<?php echo base_url();?>protocolo/ler_mensagem_situacao",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_situacao_ecac_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_situacao_ecac_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_situacao_cadin_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_situacao_cadin_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_resumo_lida(id_notificacao){
    $.post("<?php echo base_url();?>protocolo/ler_mensagem_resumo",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_sipro_lida(id_notificacao){
    $.post("<?php echo base_url();?>protocolo/ler_mensagem_sipro",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_notificacao_certidao_federal(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_notificacao_certidao_federal",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_notificacao_certidao_estadual(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_notificacao_certidao_estadual",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_notificacao_certidao_sao_paulo(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_notificacao_certidao_sao_paulo",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_notificacao_certidao_caixa(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_notificacao_certidao_caixa",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_notificacao_certidao_trabalhista(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_notificacao_certidao_trabalhista",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_eprocesso_ativo_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_eprocesso_ativo_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_eprocesso_inativo_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_eprocesso_inativo_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_dctf_vencida_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_dctf_vencida_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_dctf_proxima_vencer_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_dctf_proxima_vencer_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_dctf_sem_movimento_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_dctf_sem_movimento_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_pgdas_proxima_vencer_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_pgdas_proxima_vencer_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_pgdas_vencidas_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_pgdas_vencidas_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_das_nao_pagos_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_das_nao_pagos_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_sublimite_simples_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_sublimite_simples_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_gfip_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_gfip_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_malha_fiscal_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_malha_fiscal_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }

  function marcar_atualizacao_lida(id_notificacao){
    $.post("<?php echo base_url();?>notificacao/marcar_atualizacao_lida",
    {
        id_notificacao:id_notificacao
    },
    function(data){
      if(data == 1){
        location.reload();
      }
    });
  }
  function marcar_parcelamento_pertsn_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_pertsn_status_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_das_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_das_status_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_das_pago_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_das_pago_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_pert_pago_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_pertsn_pago_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_nao_previdenciario_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_nao_previdenciario_status_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_nao_previdenciario_pago_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_nao_previdenciario_pago_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_nao_previdenciario_em_atraso_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_nao_previdenciario_em_atraso_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_lei_12996_pago_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_lei_12996_pago_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_lei_12996_em_atraso_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_lei_12996_em_atraso_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_lei_12996_status_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_lei_12996_status_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_pert_rfb_pago_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_pert_rfb_pago_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_pert_rfb_em_atraso_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_pert_rfb_em_atraso_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_pert_rfb_status_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_pert_rfb_status_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_mei_pago_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_mei_pago_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_mei_em_atraso_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_mei_em_atraso_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }

  function marcar_parcelamento_mei_status_lida(id_notificacao){
      $.post("<?php echo base_url();?>notificacao/marcar_parcelamento_mei_status_lida",
          {
              id_notificacao:id_notificacao
          },
          function(data){
              if(data == 1){
                  location.reload();
              }
          });
  }
</script>

<script type="text/javascript">

  $(document).ready(function() {

    mensagens();
    // estaduais();
    federais();
    certidoes();
    processos();
    calendario();
    atualizacoes();
    parcelamentos();

      //dctf();
  });

  function mensagens(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/mensagens'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
             
             var div = $("#div_notificacoes_mensagens");
             div.append('<li class="onhover-dropdown"><div class="notification-box text-white"></div><ul class="onhover-show-div notification-dropdown"><li class="gradient-primary"><h5 class="f-w-700">Mensagens</h5><span>Você não tem nenhuma nova mensagem</span></li></ul></li>');           
          }else{
            var div = $("#div_notificacoes_mensagens");
            var qtd = 0;
            var html = '<li class="onhover-dropdown"><div class="notification-box text-white">';

            var lista = "";
            var razao_social;

            for(var i in data){
              

              if(data[i].tipo == 1){

                data[i].dataemail = convertDate2(data[i].dataemail);
                lista = lista + '<li><div class="media"><div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail mt-0"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z" style="color:white"></path><polyline points="22,6 12,13 2,6" style="color:white"></polyline></svg></div><div class="media-body"><a href="<?php echo base_url("dec/listar_dec_recebidas/"); ?>'+data[i].cnpj+'"><h6>DEC - A empresa '+data[i].razao_social+' tem nova(s) mensagem(s) no DEC</h6><p class="mb-0">'+data[i].dataemail+'</p></a></div></div><div style="text-align: center;"></div></li>';

                qtd++;
              }else if(data[i].tipo == 30){
                data[i].dataemail = convertDate2(data[i].dataemail);
                lista = lista + '<li><div class="media"><div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail mt-0"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z" style="color:white"></path><polyline points="22,6 12,13 2,6" style="color:white"></polyline></svg></div><div class="media-body"><a href="<?php echo base_url("dec/listar/"); ?>'+data[i].cnpj+'"><h6>DEC - A empresa '+data[i].razao_social+' tem nova(s) mensagem(s) no DEC do tipo '+data[i].codigo_tipo+'</h6><p class="mb-0">'+data[i].dataemail+'</p></a></div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_msg_dte_lida2('+data[i].id+')" >Marcar como Lida</button></div></li>';
                qtd++;
              }else{
                data[i].dataemail = convertDate2(data[i].dataemail);
                lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail mt-0"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z" style="color:white"></path><polyline points="22,6 12,13 2,6" style="color:white"></polyline></svg></div><div class="media-body"><a href="<?php echo base_url("ecac/listar_mensagens/"); ?>'+data[i].cnpj+'"><h6>e-CAC - A empresa '+data[i].razao_social+' tem '+data[i].nao_lidas+' mensagem(s) não lidas no e-CAC</h6><p class="mb-0">'+data[i].dataemail+'</p> </a></div></div></li>';
                qtd = qtd + parseInt(data[i].nao_lidas);
              }

              
            }

            var html2 = '<span class="badge3 badge3-pill badge3-secondary2">'+qtd+'</span></div><ul class="onhover-show-div notification-dropdown"><div class="vertical-scroll scroll-demo2"><li class="gradient-primary"><h5 class="f-w-700">Mensagens</h5><span>Você tem '+qtd+' mensagens não lidas</span></li>';

            var html3 = '</div><li class="bg-light txt-dark"></li></ul></li>';
            div.append(html+html2+lista+html3);
          }
      }
    });
  }


  /////////////////////////ESTADUAIS/////////////////////////////////
  function estaduais(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/estaduais'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
             
             var div = $("#div_notificacoes_estaduais");
             div.append('<li class="onhover-dropdown"><div class="notification-box text-white"></div><ul class="onhover-show-div notification-dropdown"><li class="gradient-primary"><h5 class="f-w-700">Notificações Estaduais</h5><span>Você não tem nenhuma nova notificação</span></li></ul></li>');           
          }else{
            var div = $("#div_notificacoes_estaduais");
            var qtd = 0;
            var html = '<li class="onhover-dropdown"><div class="notification-box text-white">';

            var lista = "";
            var razao_social;
            var html_aux = "";
            var cnpj_so_numero;

            for(var i in data){
              data[i].data = convertDate2(data[i].data);

              if(data[i].tipo == 1){

                if(data[i].descricao == 'Credenciado'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'<div class="media-body"><a href="<?php echo base_url("empresa/consultageral/"); ?>'+data[i].id_empresa+'"><h6>A empresa '+data[i].razao_social+' alterou o status para '+data[i].descricao+'.</h6><p class="mb-0">'+data[i].data+'</p></a></div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_situacao_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else{
                cnpj_so_numero = only_number(data[i].cnpjstring);

                lista = lista + '<li><div class="media"><div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("resumofiscal/consultageral/"); ?>'+cnpj_so_numero+'"><h6>A empresa '+data[i].razao_social+' tem uma nova alteração no Resumo Fiscal ('+data[i].descricao+')</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_resumo_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }

              qtd++;
            }

            var html2 = '<span class="badge3 badge3-pill badge3-secondary2" style="left:-13px">'+qtd+'</span></div><ul class="onhover-show-div notification-dropdown"><div class="vertical-scroll scroll-demo2"><li class="gradient-primary"><h5 class="f-w-700">Notificações Estaduais</h5><a href="<?php echo base_url("protocolo/listar"); ?>" style="color: white"><span>Você tem '+qtd+' novas notificações</span></a></li>';

            var html3 = '</div></ul></li>';

            div.append(html+html2+lista+html3);
          }
      }
    });
  }

  /////////////////////////FEDERAIS/////////////////////////////////
  function federais(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/federais'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
             
             var div = $("#div_notificacoes_federais");
             div.append('<li class="onhover-dropdown"><div class="notification-box text-white"></div><ul class="onhover-show-div notification-dropdown"><li class="gradient-primary"><h5 class="f-w-700">Notificações Federais</h5><span>Você não tem nenhuma nova notificação</span></li></ul></li>');           
          }else{
            var div = $("#div_notificacoes_federais");
            var qtd = 0;
            var html = '<li class="onhover-dropdown"><div class="notification-box text-white">';

            var lista = "";
            var razao_social;
            var html_aux = "";
            var cnpj_so_numero;

            for(var i in data){
              data[i].data = convertDate2(data[i].data);

              if(data[i].tipo == 1){

                if(data[i].descricao == 'NORMAL'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("ecac/listar_situacao_fiscal_geral/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' alterou a situação fiscal do e-CAC para REGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("ecac/listar_situacao_fiscal_geral/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' alterou a situação fiscal do E-CAC para PENDÊNCIA</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_situacao_ecac_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 2){
                if(data[i].descricao == 'NORMAL'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("ecac/listar_cadin/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' está agora REGULAR no CADIN</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("ecac/listar_cadin/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' está com uma pendência no CADIN</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_situacao_cadin_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 3){

                html_aux = '<div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("dctf/listar_from_notificacao/"); ?>'+data[i].cnpj+'/'+data[i].descricao+'"><h6>A empresa '+data[i].razao_social+' tem uma DCTF do mês '+data[i].descricao+' próxima a ser entregue </h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_dctf_proxima_vencer_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 4){

                html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("dctf/listar_from_notificacao/"); ?>'+data[i].cnpj+'/'+data[i].descricao+'"><h6>A empresa '+data[i].razao_social+' tem uma DCTF do mês '+data[i].descricao+' não entregue </h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_dctf_vencida_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 5){

                html_aux = '<div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("dctf/listar_from_notificacao/"); ?>'+data[i].cnpj+'/'+data[i].descricao+'"><h6>A empresa '+data[i].razao_social+' tem uma DCTF do mês '+data[i].descricao+' marcada como SEM MOVIMENTO </h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_dctf_sem_movimento_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 6){

                html_aux = '<div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("pgdas/listar_from_notificacao/"); ?>'+data[i].cnpj+'/'+data[i].descricao+'"><h6>A empresa '+data[i].razao_social+' tem uma PGDAS do mês '+data[i].descricao+' não entregue </h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_pgdas_vencidas_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 7){

                html_aux = '<div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("pgdas/listar_from_notificacao/"); ?>'+data[i].cnpj+'/'+data[i].descricao+'"><h6>A empresa '+data[i].razao_social+' tem uma PGDAS do mês '+data[i].descricao+' Próxima a ser entregue </h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_pgdas_proxima_vencer_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 8){

                html_aux = '<div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("pgdas/listar_from_notificacao/"); ?>'+data[i].cnpj+'/'+data[i].descricao+'"><h6>A empresa '+data[i].razao_social+' tem um DAS do mês '+data[i].descricao+' não pago </h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_das_nao_pagos_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 9){

                html_aux = '<div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle mt-0"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13" style="color: white;"></line><line x1="12" y1="17" x2="12" y2="17" style="color: white;"></line></svg></div><div class="media-body"><a href="<?php echo base_url("limite_simples/listar/"); ?>"><h6>A empresa '+data[i].razao_social+' atingiu '+data[i].descricao+'% do Sublimite do Simples </h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_sublimite_simples_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 10){

                if(data[i].descricao == 'REGULAR'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("gfip/listar/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' não possui mais Divergência no GFIP X GPS</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("gfip/listar/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui Divergência no GFIP X GPS</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_gfip_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 11){

                html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("malha_fiscal/listar/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma notificação da Malha Fiscal</h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_malha_fiscal_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 12){

                html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("simples_nacional/listar/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' foi excluída do Simples Nacional</h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_excluida_simples_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 13){

                html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("mei/listar/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' foi excluída do MEI</h6><p class="mb-0">'+data[i].data+'</p></a></div>';

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_excluida_mei_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }

              qtd++;
            }

            var html2 = '<span class="badge3 badge3-pill badge3-secondary2">'+qtd+'</span></div><ul class="onhover-show-div notification-dropdown"><div class="vertical-scroll scroll-demo2"><li class="gradient-primary"><h5 class="f-w-700">Notificações Federais</h5><a href="#" style="color: white"><span>Você tem '+qtd+' novas notificações</span></a></li>';

            var html3 = '</div><li class="bg-light txt-dark"></li></ul></li>';

            div.append(html+html2+lista+html3);
          }
      }
    });
  }


  /////////////////////////CERTIDÕES/////////////////////////////////
  function certidoes(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/certidoes'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
             
             var div = $("#div_notificacoes_certidoes");
             div.append('<li class="onhover-dropdown"><div class="notification-box text-white"></div><ul class="onhover-show-div notification-dropdown"><li class="gradient-primary"><h5 class="f-w-700">Certidões</h5><span>Você não tem nenhuma nova notificação</span></li></ul></li>');           
          }else{
            var div = $("#div_notificacoes_certidoes");
            var qtd = 0;
            var html = '<li class="onhover-dropdown"><div class="notification-box text-white">';

            var lista = "";
            var razao_social;
            var html_aux = "";
            var cnpj_so_numero;

            for(var i in data){
              data[i].data = convertDate2(data[i].data);
              data[i].cnpj = only_number(data[i].cnpj);

              if(data[i].tipo == 1){
                //FEDERAL PGFN
                if(data[i].descricao == 'PENDENCIA'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_federal_pgfn/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão de débitos da PGFN REGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_federal_pgfn/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão de débitos da PGFN IRREGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_notificacao_certidao_federal('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 2){
                //ESTADUAL
                 if(data[i].descricao == 'NORMAL'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_estadual/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão estadual REGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_estadual/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão estadual IRREGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_notificacao_certidao_estadual('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 3){
                //SÃO PAULO
                 if(data[i].descricao == 'NORMAL'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_municipal/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão do município de São Paulo REGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_municipal/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão do município de São Paulo IRREGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_notificacao_certidao_sao_paulo('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 7){
                //CAIXA 
                 if(data[i].descricao == 'NORMAL'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_caixa/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão de Regularidade do FGTS REGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_caixa/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão de Regularidade do FGTS IRREGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_notificacao_certidao_caixa('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 8){
                //TRABALHISTA 
                 if(data[i].descricao == 'NORMAL'){
                  html_aux = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_trabalhista/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão de Débitos Trabalhista REGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }else{
                  html_aux = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("certidao_negativa/listar_trabalhista/"); ?>'+data[i].cnpj+'"><h6>A empresa '+data[i].razao_social+' possui uma certidão de Débitos Trabalhista IRREGULAR</h6><p class="mb-0">'+data[i].data+'</p></a></div>';
                }

                lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_notificacao_certidao_trabalhista('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }

              qtd++;
            }

            var html2 = '<span class="badge3 badge3-pill badge3-secondary2" style="margin-top: 5px;">'+qtd+'</span></div><ul class="onhover-show-div notification-dropdown"><div class="vertical-scroll scroll-demo2"><li class="gradient-primary"><h5 class="f-w-700">Certidões</h5><a href="#" style="color: white"><span>Você tem '+qtd+' novas notificações</span></a></li>';

            var html3 = '</div></ul></li>';

            div.append(html+html2+lista+html3);
          }
      }
    });
  }

  /////////////////////////PROCESSOS/////////////////////////////////
  function processos(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/processos'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
             
             var div = $("#div_notificacoes_processos");
             div.append('<li class="onhover-dropdown"><div class="notification-box text-white"></div><ul class="onhover-show-div notification-dropdown"><li class="gradient-primary"><h5 class="f-w-700">Processos</h5><span>Você não tem nenhuma nova notificação</span></li></ul></li>');           
          }else{
            var div = $("#div_notificacoes_processos");
            var qtd = 0;
            var html = '<li class="onhover-dropdown"><div class="notification-box text-white">';

            var lista = "";
            var razao_social;
            var html_aux = "";
            var cnpj_so_numero;

            for(var i in data){
              data[i].data = convertDate2(data[i].data);

              if(data[i].tipo == 1){
                //SIPRO
                lista = lista + '<li><div class="media"><div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mt-0" style="color: white;"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg></div><div class="media-body"><a href="<?php echo base_url("consulta_sipro/"); ?>"><h6>SIPRO - Há uma alteração no processo SIPRO da empresa '+data[i].razao_social+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_sipro_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';

                
              }else if(data[i].tipo == 2){
                //e-PROCESSOS ATIVOS
                lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mt-0" style="color: white;"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg></div><div class="media-body"><a href="<?php echo base_url("ecac/listar_eprocessos_ativos/"); ?>'+data[i].cnpj+'"><h6>e-Processos - '+data[i].razao_social+' - '+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_eprocesso_ativo_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }else if(data[i].tipo == 3){
                //e-PROCESSOS INATIVOS
                lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mt-0" style="color: white;"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg></div><div class="media-body"><a href="<?php echo base_url("ecac/listar_eprocessos_inativos/"); ?>'+data[i].cnpj+'"><h6>e-Processos - '+data[i].razao_social+' - '+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_eprocesso_inativo_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
              }

              qtd++;
            }

            var html2 = '<span class="badge3 badge3-pill badge3-secondary2" style="margin-top: 5px;">'+qtd+'</span></div><ul class="onhover-show-div notification-dropdown"><div class="vertical-scroll scroll-demo2"><li class="gradient-primary"><h5 class="f-w-700">Processos</h5><a href="#" style="color: white"><span>Você tem '+qtd+' novas notificações</span></a></li>';

            var html3 = '</div></ul></li>';

            div.append(html+html2+lista+html3);
          }
      }
    });
  }


  /////////////////////////DCTF/////////////////////////////////
  function dctf(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/dctf'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
                      
          }else{
            var div = $("#div_vencimentos");
            var qtd = 0;
            var lista = "";
            var razao_social;
            var html_aux = "";
            var cnpj_so_numero;

            for(var i in data){
              //data[i].data = convertDate2(data[i].data);

              if(data[i].vencido == true){
                //DCTF VENCIDO
                lista = lista + '<li><div class="media"><div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar mt-0" style="color: white;"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg></div><div class="media-body"><a href="<?php echo base_url("dctf/list_from_notification/"); ?>'+data[i].cnpj+'"><h6></i>A entrega do DCTF da empresa '+data[i].razao_social+' está '+data[i].dias+' dia(s) atrasado(s)</h6><p class="mb-0">Data de Entrega era '+data[i].ultimo_dia_dctf+'</p></a></div></div></li>';     
              }else{
                //DCTF PROXIMO VENCIMENTO
                lista = lista + '<li><div class="media"><div class="notification-icons bg-warning mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar mt-0" style="color: white;"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg></div><div class="media-body"><a href="<?php echo base_url("dctf/list_from_notification/"); ?>'+data[i].cnpj+'"><h6></i>A entrega do DCTF da empresa '+data[i].razao_social+' está próximo a vencer, restam '+data[i].dias+' dia(s)</h6><p class="mb-0">Data de Entrega é '+data[i].ultimo_dia_dctf+'</p></a></div></div></li>'; 
              }

              qtd++;
            }

            
            if(qtd != 0){
              var qtd_notificacao = $("#span_qtd_vencimentos").text();
              var t = parseInt(qtd_notificacao)+qtd;
              $("#span_qtd_vencimentos").text(t);
              $("#span_texto_qtd_vencimentos").text("Você tem "+t+" novas notificações");
            }
            


            div.append(lista);
          }
      }
    });
  }


  function only_number(a){
    var res = a.replace(/\D/g, "");
    return res;
  }
  function convertDate2(a){
    var day = a.split('-')[2];
    day = day.split(' ')[0];
    var month = a.split('-')[1];
    var year = a.split('-')[0];

    var date = day + "/" + month + "/" + year;
    return date;
  }

  /////////////////////////CALENDARIO/////////////////////////////////
  function calendario(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/calendario'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
                      
          }else{
            var div = $("#div_vencimentos");
            var qtd = 0;
            var lista = "";
            var razao_social;
            var html_aux = "";
            var cnpj_so_numero;

            for(var i in data){
              //data[i].data = convertDate2(data[i].data);

              if(data[i].dias_vencimento >= 0){
                
                lista = lista + '<li><div class="media"><div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar mt-0" style="color: white;"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg></div><div class="media-body"><a href="<?php echo base_url("calendario/listar"); ?>"><h6></i>A tarefa '+data[i].title+' está próxima da data de vencimento, restam '+data[i].dias_vencimento+' dia(s) </h6><p class="mb-0"></p></a></div></div></li>';     
              }

              qtd++;
            }

            
            if(qtd != 0){
              var qtd_notificacao = $("#span_qtd_vencimentos").text();
              var t = parseInt(qtd_notificacao)+qtd;
              $("#span_qtd_vencimentos").text(t);
              $("#span_texto_qtd_vencimentos").text("Você tem "+t+" novas notificações");
            }
            


            div.append(lista);
          }
      }
    });
  }


  //////////////////////ATUALIZAÇÕES//////////////////////////////
  function atualizacoes(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('notificacao/atualizacoes'); ?>',
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          
          if(data == "") {
             
             var div = $("#div_notificacoes_atualizacoes");
             div.append('<li class="onhover-dropdown"><div class="notification-box text-white"></div><ul class="onhover-show-div notification-dropdown"><li class="gradient-primary"><h5 class="f-w-700">Atualizações Veri</h5><span>Você não tem nenhuma nova notificação</span></li></ul></li>');           
          }else{
            var div = $("#div_notificacoes_atualizacoes");
            var qtd = 0;
            var html = '<li class="onhover-dropdown"><div class="notification-box text-white">';

            var lista = "";
            var html_aux = "";

            for(var i in data){
              data[i].data = convertDate2(data[i].data);
              data[i].descricao = data[i].descricao.replaceAll("opcao", "opção");
              
              html_aux = '<div class="notification-icons mr-3"> <i class="pe-7s"><img src="<?php echo base_url("assets/css/icones_header/rocket-emji.png"); ?>" width="20px"></i></div><div class="media-body"><a href="<?php echo base_url("assets/"); ?>'+data[i].link_download+'" download ><h6>'+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a></div>';

              lista = lista + '<li><div class="media">'+html_aux+'</div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_atualizacao_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';

              qtd++;
            }

            var html2 = '<span class="badge3 badge3-pill badge3-secondary2">'+qtd+'</span></div><ul class="onhover-show-div notification-dropdown"><div class="vertical-scroll scroll-demo2"><li class="gradient-primary"><h5 class="f-w-700">Atualizações Veri</h5><a href="#" style="color: white"><span>Você tem '+qtd+' novas notificações</span></a></li>';

            var html3 = '</div></ul></li>';

            div.append(html+html2+lista+html3);
          }
      }
    });
  }

  //////////////////////PARCELAMENTOS//////////////////////////////
  function parcelamentos(){
      $.ajax({
          type: "POST",
          url: '<?php echo base_url('notificacao/parcelamentos'); ?>',
          async: true,
          success: function(result){
              var data = JSON.parse(result);
          
              if(data == "") {
                 
                 var div = $("#div_notificacoes_parcelamentos");
                 div.append('<li class="onhover-dropdown"><div class="notification-box text-white"></div><ul class="onhover-show-div notification-dropdown"><li class="gradient-primary"><h5 class="f-w-700">Parcelamentos</h5><span>Você não tem nenhuma nova notificação</span></li></ul></li>');           
              }else{
                var div = $("#div_notificacoes_parcelamentos");
                var qtd = 0;
       
                var html = '<li class="onhover-dropdown"><div class="notification-box text-white">';

                var lista = "";
                var razao_social;
                var html_aux = "";
                var cnpj_so_numero;

                for(var i in data){
                  data[i].data = convertDate2(data[i].data);

                  if(data[i].tipo == 1){
                    //PARCELAMENTO DAS STATUS
                    if(data[i].descricao != 'Em parcelamento'){
                      var icone = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div>';
                    }else{
                      var icone = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>';
                    }
                    lista = lista + '<li><div class="media">'+icone+'<div class="media-body"><a href="<?php echo base_url("parcelamento_das/listar/"); ?>"><h6>O Parcelamento do DAS da empresa '+data[i].razao_social+' mudou o status para '+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_das_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';

                    
                  }else if(data[i].tipo == 2){
                    //PARCELAMENTO DAS PAGO
                    lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("parcelamento_das/listar/"); ?>"><h6>A parcela do Parcelamento do DAS da empresa '+data[i].razao_social+' período '+data[i].descricao+' foi pago</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_das_pago_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';

                  }else if(data[i].tipo == 3){
                    //PARCELAMENTO PERT STATUS
                    if(data[i].descricao != 'Em parcelamento'){
                      var icone = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div>';
                    }else{
                      var icone = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>';
                    }
                    lista = lista + '<li><div class="media">'+icone+'<div class="media-body"><a href="<?php echo base_url("parcelamento_pert/listar/"); ?>"><h6>O Parcelamento do PERT-SN da empresa '+data[i].razao_social+' mudou o status para '+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_pertsn_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 4){
                    //PARCELAMENTO PERT PAGO
                    lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("parcelamento_pert/listar/"); ?>"><h6>A parcela do Parcelamento do PERT-SN da empresa '+data[i].razao_social+' período '+data[i].descricao+' foi pago</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_pert_pago_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';

                  }else if(data[i].tipo == 5){
                      // PARCELAMENTO NAO PREVIDENCIARIO STATUS
                      if(data[i].descricao != 'Parcelado'){
                          var icone = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div>';
                      }else{
                          var icone = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>';
                      }
                      lista = lista + '<li><div class="media">'+icone+'<div class="media-body"><a href="<?php echo base_url("Parcelamento_nao_previdenciario/listar/"); ?>"><h6>O Parcelamento Não Previdenciario da empresa '+data[i].razao_social+' mudou o status para '+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_nao_previdenciario_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 6){
                      //PARCELAMENTO NAO PREVIDENCIARIO PAGO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_nao_previdenciario/listar/"); ?>"><h6>A parcela do Parcelamento do Não Previdenciario da empresa '+data[i].razao_social+' período '+data[i].descricao+' foi pago</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_nao_previdenciario_pago_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else  if(data[i].tipo == 7){
                      //PARCELAMENTO NAO PREVIDENCIARIO EM ATRASO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_nao_previdenciario/listar/"); ?>"><h6>A parcela do Parcelamento Não Previdenciário da empresa '+data[i].razao_social+' período '+data[i].descricao+' está em atraso</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_nao_previdenciario_em_atraso_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 8){
                      // PARCELAMENTO LEI 19996 STATUS
                      if(data[i].descricao != 'Em Parcelamento'){
                          var icone = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div>';
                      }else{
                          var icone = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>';
                      }
                      lista = lista + '<li><div class="media">'+icone+'<div class="media-body"><a href="<?php echo base_url("Parcelamento_lei_12996/listar/"); ?>"><h6>O Parcelamento Lei 12.996 da empresa '+data[i].razao_social+' mudou o status para '+data[i].descricao.substring(6)+'/'+data[i].descricao.substring(4, 6)+'/'+data[i].descricao.substring(0, 4)+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_lei_12996_status_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 9){
                      //PARCELAMENTO LEI 19996 PAGO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_lei_12996/listar/"); ?>"><h6>A parcela do Parcelamento Lei 12.996 da empresa '+data[i].razao_social+' período '+data[i].descricao.substring(6)+'/'+data[i].descricao.substring(4, 6)+'/'+data[i].descricao.substring(0, 4)+' foi pago</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_lei_12996_pago_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 10){
                      //PARCELAMENTO LEI 19996 EM ATRASO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_lei_12996/listar/"); ?>"><h6>A parcela do Parcelamento Lei 12.996 da empresa '+data[i].razao_social+' período '+data[i].descricao.substring(6)+'/'+data[i].descricao.substring(4, 6)+'/'+data[i].descricao.substring(0, 4)+' está em atraso</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_lei_12996_em_atraso_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 11){
                      // PARCELAMENTO PERT RFB STATUS
                      if(data[i].descricao != 'Ativo'){
                          var icone = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div>';
                      }else{
                          var icone = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>';
                      }
                      lista = lista + '<li><div class="media">'+icone+'<div class="media-body"><a href="<?php echo base_url("Parcelamento_pert_rfb/listar/"); ?>"><h6>O Parcelamento Pert RFB da empresa '+data[i].razao_social+' mudou o status para '+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_pert_rfb_status_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 12){
                      //PARCELAMENTO PERT RFB PAGO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_pert_rfb/listar/"); ?>"><h6>A parcela do Parcelamento Pert RFB da empresa '+data[i].razao_social+' período '+data[i].descricao+' foi pago</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_pert_rfb_pago_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 13){
                      //PARCELAMENTO PERT RFB EM ATRASO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_pert_rfb/listar/"); ?>"><h6>A parcela do Parcelamento Pert RFB da empresa '+data[i].razao_social+' período '+data[i].descricao+' está em atraso</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_pert_rfb_em_atraso_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 14){
                      // PARCELAMENTO MEI STATUS
                      if(data[i].descricao != 'Em parcelamento'){
                          var icone = '<div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div>';
                      }else{
                          var icone = '<div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>';
                      }
                      lista = lista + '<li><div class="media">'+icone+'<div class="media-body"><a href="<?php echo base_url("Parcelamento_mei/listar/"); ?>"><h6>O Parcelamento MEI da empresa '+data[i].razao_social+' mudou o status para '+data[i].descricao+'</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_mei_status_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 15){
                      //PARCELAMENTO MEI PAGO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-success mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up mt-0"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_mei/listar/"); ?>"><h6>A parcela do Parcelamento MEI da empresa '+data[i].razao_social+' período '+data[i].descricao+' foi pago</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_mei_pago_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }else if(data[i].tipo == 16){
                      //PARCELAMENTO MEI EM ATRASO
                      lista = lista + '<li><div class="media"><div class="notification-icons bg-danger mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down mt-0"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></div><div class="media-body"><a href="<?php echo base_url("Parcelamento_mei/listar/"); ?>"><h6>A parcela do Parcelamento MEI da empresa '+data[i].razao_social+' período '+data[i].descricao+' está em atraso</h6><p class="mb-0">'+data[i].data+'</p></a> </div></div><div style="text-align: center;"><button class="badge badge-info badge-shadow" style="border: none;" onclick="marcar_parcelamento_mei_em_atraso_lida('+data[i].id+')" >Marcar como Lida</button></div></li>';
                  }

                  qtd++;
                }

                var html2 = '<span class="badge3 badge3-pill badge3-secondary2" style="margin-top: 5px;">'+qtd+'</span></div><ul class="onhover-show-div notification-dropdown"><div class="vertical-scroll scroll-demo2"><li class="gradient-primary"><h5 class="f-w-700">Parcelamentos</h5><a href="#" style="color: white"><span>Você tem '+qtd+' novas notificações</span></a></li>';

                var html3 = '</div></ul></li>';

                div.append(html+html2+lista+html3);
              }
          }
      });
  }
</script>