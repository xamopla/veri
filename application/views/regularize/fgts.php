<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 600px;
        max-width : 600px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Dívida Ativa - FGTS <a class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="As informações são obtidas através do portal Regularize e atualizadas 1 vez por quinzena." style="color: black;"></a></h5>
          </div>
          <div class="card-body"> 
            <br>
            <div class="table-responsive">
                <table class="table table-striped" id="example">
                  <thead>
                    <tr>
                      <th >Razão Social</th> 
                      <th >CNPJ</th>                     
                      <th >Tipo da Dívida</th> 
                      <th >Total da Dívida</th> 
                      <th >Detalhe</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                  $cont = 1;
                    if ($consulta){ 
                      foreach ($consulta as $c){?>
                      <tr>
                          <td ><?php echo $c->razao_social; ?></td>  
                          <td ><?php echo $c->cnpj; ?></td>
                          <td ><?php echo $c->tipo_de_divida; ?></td>
                          <td ><?php echo "R$ ".number_format($c->total, 2, ',', '.'); ?></td>
                          <td ><a href="javascript:openModalDetalhe(<?php echo $c->id_empresa; ?>)" class='badge badge-info badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes da dívida'>Detalhes</a></td>
                      </tr>
                  <?php 
                  $cont ++;} 
                  }
                  ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<div class="modal fade" id="modalDebitos" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         

        <div class="panel panel-success">
          <div class="panel-heading title-menu">
              <h4 class="panel-title">
                  <a >
                      <strong><span class="glyphicon glyphicon-menu glyphicon-usd" ></span>
                      Relação de Inscrições em Dívida Ativa</strong>
                  </a>
              </h4>
          </div>
          <div id="collapse2018" >
              <table class="table table-striped" style="margin-top: 0px;" id="tabela_modal">
                
                <tbody id="body_modal">
                  <thead _ngcontent-ypo-c9=""><tr _ngcontent-ypo-c9="" class="table-secondary"><th _ngcontent-ypo-c9="" scope="col"> Número de Inscrição </th><th _ngcontent-ypo-c9="" scope="col"> Valor total da dívida (R$) </th></tr></thead>
                    
                </tbody>
              </table>
          </div>
        </div>

      </div>
    </div>
  </div>

</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->
 
<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
        "zeroRecords": "Nenhum registro encontrado",
          "info": "Mostando página _PAGE_ de _PAGES_",
          "infoEmpty": "Nenhum registo",
          "infoFiltered": "(filtrado do total de _MAX_ registros)",
            "search": "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
          },
          "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
          }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );

});


function openModalDetalhe(id){
  $("#body_modal").html('');
  // $("#tabela_modal").html('');

  $.ajax({
      type: "POST",
      url: '<?php echo base_url('regularize/find_debitos_for_modal'); ?>',
      async: true,
      data: { 
          'id': id,
          'tipo': 'FGTS'
      },
      success: function(result){
        var re = JSON.parse(result);
        var total = 0;
        var valor = 0;
        var final = 0;

        if(re != ''){
          
          const options1 = { style: 'currency', currency: 'BRL' };
          const numberFormat1 = new Intl.NumberFormat('pt-BR', options1);

          for(var i in re.resultado){
            valor = numberFormat1.format(re.resultado[i].total);

            $("#body_modal").append('<tr>');
            $("#body_modal").append('<td style="padding: 2px" class="text-center">'+re.resultado[i].numero_inscricao+'</td>');
            $("#body_modal").append('<td style="padding: 2px" class="text-center">'+valor+'</td>');
            $("#body_modal").append('</tr>');

            final = numberFormat1.format(re.total);
          }

          $("#body_modal").append('<tr>');
          $("#body_modal").append('<td style="padding: 2px" class="text-center"></td>');
          $("#body_modal").append('<td style="padding: 2px; font-weight: bold;" class="text-center">Total: '+final+'</td>');
          $("#body_modal").append('</tr>');

          // $("#body_modal").append('<tfoot _ngcontent-ypo-c9=""><tr _ngcontent-ypo-c9=""><td _ngcontent-ypo-c9="" class="pr-2 text-right" colspan="2"><span _ngcontent-ypo-c9="" class="pr-2" style="font-weight: 700;">Total:</span> &nbsp;'+final+'</td></tr></tfoot>');
        }
      },
  });

  $('#modalDebitos').modal();
}
</script>
