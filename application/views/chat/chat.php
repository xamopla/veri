<!-- Plugins css start-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/todo.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-design-icon.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/pe7-icon.css'); ?>">

<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/chat/materialize.css'); ?>"> -->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/chat/style.css'); ?>"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/chat/app-chat.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/chat/custom.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/chat/perfect-scrollbar.css'); ?>">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/emoji_aux/css/jquery.emojipicker.aux.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/emoji_aux/css/jquery.emojipicker.a.aux.css'); ?>">
<style type="text/css">
  .emojiPicker{
    left: 40% !important
    /*left: 35% !important;*/
  }

  .chat-application .app-chat .chat-content .chat-content-area .chat-footer .chat-input .message {
    margin-right: 1.5rem;
    padding: 0 1.5rem;
    color: #9e9e9e;
    border-bottom: 0;
    border-radius: 5px;
    background-color: #eceff1;
    width: 100%;
    border-color: transparent;
}

.chat-application .app-chat .chat-content .chat-content-area .chat-footer .chat-input .send {
    height: 42px;
    padding: .3rem 3rem .3rem 2.5rem;
    border-radius: .25rem;
    background-color: #00c6ff;
    color: white;
}
.chat-application .app-chat .chat-content .sidebar .sidebar-chat .chat-list .chat-user .info-section .badge {
    font-size: .75rem;
    width: 20px;
    margin-top: .25rem;
}

span.badge.pill {
    font-size: .8rem;
    line-height: 20px;
    min-width: 1rem;
    height: 20px;
    border-radius: 50%;
    border-radius: 9px;
}
span.badge {
    color: white !important;
}

span.badge {
    font-size: 1rem;
    line-height: 22px;
    float: right;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    min-width: 3rem;
    height: 22px;
    margin-left: 14px;
    padding: 0 6px;
    text-align: center;
    color: #757575;
}

.red {
    background-color: #f44336 !important;
}

.circle {
    border-radius: 50% !important;
}

.online {
     background-color: transparent; 
}

.away {
    background-color: transparent;
}

.offline {
    background-color: transparent;
}

.page-wrapper .page-body-wrapper footer {
    margin-left: 0px !important;
    -webkit-transition: .6s;
    transition: .6s;
    bottom: 0;
    z-index: 9;
    position: relative;
    -webkit-box-shadow: 19px 8px 16px 7px rgb(21 141 247 / 5%);
    box-shadow: 19px 8px 16px 7px rgb(21 141 247 / 5%);
}
</style>
<!-- Plugins css Ends-->
<div class="page-body">
  <div class="container-fluid"> 
  <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="chat-application">
  <div class="chat-content-head">
    
  </div>
  <div class="app-chat">
    <div class="content-area content-right">
      <div class="app-wrapper">
        <!-- Sidebar menu for small screen -->
        <a href="#" data-target="chat-sidenav" class="sidenav-trigger hide-on-large-only">
          <i class="material-icons">menu</i>
        </a>
        <!--/ Sidebar menu for small screen -->

        <div class="card card card-default scrollspy border-radius-6 fixed-width">
          <div class="card-content chat-content p-0">
            <!-- Sidebar Area -->
            <div class="sidebar-left sidebar-fixed animate fadeUp animation-fast">
              <div class="sidebar animate fadeUp">
                <div class="sidebar-content">
                  <div id="sidebar-list" class="sidebar-menu chat-sidebar list-group position-relative">
                    <div class="sidebar-list-padding app-sidebar sidenav" id="chat-sidenav">
                      <!-- Sidebar Header -->
                      <div class="sidebar-header">
                        <div class="row valign-wrapper">
                          <div class="col s2 media-image pr-0">
                            <input type="text" name="id_usuario" id="id_usuario" style="display: none" />
                            <img src="<?php echo $foto_usuario_logado; ?>" alt="" class="circle z-depth-2 responsive-img" id="foto_usuario_logado">
                          </div>
                          <div style="display: none;">
                            <audio controls id="audio_teste">
                              <source src="assets/audio/alert.mp3" type="audio/mp3">
                            </audio>
                          </div>

                          <div class="col s10">
                            <p class="m-0 blue-grey-text text-darken-4 font-weight-700"><?php echo $this->session->userdata['userprimesession']['nome']; ?></p>
                            <p class="m-0 info-text">Online</p>
                          </div>
                        </div>
                        <span class="option-icon" data-toggle='popover' data-trigger='hover' data-placement='top' title='Alterar foto perfil' onclick="openModalFoto()">
                          <i class="material-icons">more_vert</i>
                        </span>
                      </div>
                      <!--/ Sidebar Header -->

                      <!-- Sidebar Content List -->
                      <div class="sidebar-content sidebar-chat">
                        <div class="chat-list">

                          <?php
                            if ($usuarios){ 
                              $i=0;
                                foreach ($usuarios as $e){?>
                                  
                                  <div class="chat-user animate fadeUp <?php if($i==0){echo 'delay-'.$i.' active'; }else{echo 'delay-'.$i.''; } ?>" id="item<?php echo $e->id ?>" onclick="showConversa(<?php echo $e->id ?>, <?php echo $id_usuario_logado ?>, true); marcarComoLido_Aux(<?php echo $e->id ?>)" >
                                    <div class="user-section">
                                      <div class="row valign-wrapper">
                                        <div class="col s2 media-image online pr-0">
                                          <img src="<?php echo $e->foto_perfil; ?>" alt="" class="circle z-depth-2 responsive-img">
                                        </div>
                                        <div class="col s10">
                                          <p class="m-0 blue-grey-text text-darken-4 font-weight-700" style="font-weight: bold; font-size: 14px;"><?php echo $e->nome; ?></p>
                                          <p class="m-0 info-text" id="ultima_mensagem_usuario<?php echo $e->id; ?>"><?php echo $e->ultima_mensagem; ?></p>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="info-section">
                                      <div class="star-timing">
                                        <div class="favorite">
                                          <!-- <i class="material-icons amber-text">star</i> -->
                                        </div>
                                        <div class="time">
                                          <span id="hora_ultima_conversa<?php echo $e->id; ?>"><?php echo $e->data; ?></span>
                                        </div>
                                      </div>
                                      <div id="badge-outras-conversas<?php echo $e->id; ?>">
                                        <span class="float-right info" id="info<?php echo $e->id; ?>">
                                        <?php if($e->numero_msg != 0){ ?>
                                          <span class="badge badge pill red"><?php echo $e->numero_msg; ?></span>
                                        <?php } ?>
                                        </span>
                                      </div>
                                    </div>
                                  </div>

                                <?php $i++; } 
                            }
                          ?>

                  
                        </div>
                      </div>
                      <!--/ Sidebar Content List -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--/ Sidebar Area -->

            <!-- Content Area -->
            <div class="chat-content-area animate fadeUp">
              <!-- Chat header -->
              <div class="chat-header">
                <div class="row valign-wrapper">
                  <div class="col media-image online pr-0">
                    <img src="<?php echo $primeiro_colaborador->foto_perfil ?>" alt="" class="circle z-depth-2 responsive-img" id="foto_perfil_chat">
                  </div>
                  <div class="col">
                    <p class="m-0 blue-grey-text text-darken-4 font-weight-700" id="nome_contato"><?php echo $primeiro_colaborador->nome ?></p>
                    <p class="m-0 chat-text truncate" id="status_contato">Online</p>
                  </div>
                </div>
                <span class="option-icon">
                  <span class="favorite">
                    
                  </span>
                  <!-- <i class="material-icons">delete</i> -->
                </span>
              </div>
              <!--/ Chat header -->

              <!-- Chat content area -->
              <div class="chat-area" id="chat-app-window_altura">
                <div class="chats">
                  <div class="chats" id="chat-app-window">
                    <!-- Area do chat -->
                   

                  </div>
                </div>
              </div>
              <!--/ Chat content area -->

              <!-- Chat footer <-->
              <div class="chat-footer">
                <form action="javascript:void(0);" class="chat-input">
                  <input type="text" placeholder="Digite aqui.." class="message" id="mensagem"  maxlength="150">
                  <button class="btn waves-effect waves-light send" type="button" id="button-nova-mensagem" onclick="novaMensagem()">Enviar</button>
                </form>
              </div>
              <!--/ Chat footer -->
            </div>
            <!--/ Content Area -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- START RIGHT SIDEBAR NAV -->
  </div>
  <!-- Container-fluid Ends-->

  <div class="modal fade" id="modalFoto" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tipoDocumentoformModal">Alterar Foto Perfil</h5>
        <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <?php echo form_open_multipart("usuario/atualizarfotoperfil/".$this->session->userdata['userprimesession']['id'].""); ?>
          <div class="form-group">
            <div>
              <div class="form-group">
                <input id="userfile" name="userfile" class="file" type="file" required accept="image/*" />
              </div>
            </div>

            <div class="modal-footer justify-content-between bg-primary-50">           
              <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
            </div>
        <?php echo form_close(); ?>

      </div>
    </div>
  </div>
</div>

</div>




<!-- Plugins JS start-->

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<!-- <script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script> -->
<!-- <script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script> -->
<!-- <script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script> -->
<!-- <script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script> -->
<!-- <script src="<?php echo base_url('assets/js/print.js'); ?>"></script> -->
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->

<script src="<?php echo base_url('assets/js/chat/app-chat.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat/perfect-scrollbar.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/plugins/emoji_aux/js/jquery.emojipicker_aux.js') ?>"></script>
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->


<script type="text/javascript">
  function enter_chat(source) {
   var message = $(".message").val();
   if(message != ""){
    var html = '<div class="chat-text">' + "<p>" + message + "</p>" + "</div>";
    $(".chat:last-child .chat-body").append(html);
    $(".message").val("");
    $(".chat-area").scrollTop($(".chat-area > .chats").height());
   }
}
</script>

<script type="text/javascript">
  function emoji(){
    //e.preventDefault();
    $('#mensagem').emojiPicker('toggle');
  }

  $('.emojiable-question').emojiPicker({
    width: '300px',
    height: '200px',
    // left: '40% !important',
    button: false
  });



  $(document).ready(function() {

    $('[data-toggle="popover"]').popover({
      boundary:'window',
      html: true
    })

    var input = document.getElementById("mensagem");

    input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("button-nova-mensagem").click();
      }
    });

    showConversa(<?php echo $primeiro_colaborador->id ?>, <?php echo $id_usuario_logado; ?>, false);
    monitora_conversa_atual();
    monitora_outras_conversas();
  });

  function showConversa(id_usuario, id_usuario_logado, boll){
    if(boll){
      $(".active").removeClass("active");
      $("#item"+id_usuario).addClass('active');
    }
    
    var altura = $("#chat-app-window_altura").height() * 100;
    

    $("#id_usuario").val(id_usuario);
    $("#chat-app-window").html("");

    $.ajax({
        type: 'POST',
          url: "<?php echo base_url(); ?>chat/find_usuario/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
        cache: false,
        success: function (data) {
          $("#nome_contato").text(data.user_name);
          $("#status_contato").text(data.status);
          $("#foto_perfil_chat").attr("src",data.foto_conversa);
          
        }
    });

    $.ajax({
        type: 'POST',
          url: "<?php echo base_url(); ?>chat/buscar_conversa/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
        cache: false,
        success: function (data) {
          var foto_perfil;
          var foto_conversa;
          var i_ = 0;

          $.each(data, function(i, item) {
            if(i_ == 0){
              foto_perfil = item.foto_usuario;
              foto_conversa = item.foto_conversa;
            }
            i_++;

            if(item.id_envia == 'SIM'){

              $("#chat-app-window").append('<div class="chat chat-right"><div class="chat-avatar"><a class="avatar"><img src="'+foto_perfil+'" class="circle" alt="avatar" /></a></div><div class="chat-body"><div class="chat-text"><p>'+item.mensagem+'  <span class="message-data-time" style="font-size: 8px;">'+item.hora+'</span> </p><div class="float-right" style="padding: 5px;" id="confirmacao-leitura"><i class="mdi mdi-check-all" style="color:'+item.cor+'" id="icon-confirmacao" ></i></div></div></div></div>');
            }else{
              $("#chat-app-window").append('<div class="chat"><div class="chat-avatar"><a class="avatar"><img src="'+foto_conversa+'" class="circle" alt="avatar" /></a></div><div class="chat-body"><div class="chat-text"><p>'+item.mensagem+'  <span class="message-data-time" style="font-size: 8px;">'+item.hora+'</span> </p><div class="float-right" style="padding: 5px;" id="confirmacao-leitura"><i class="mdi mdi-check-all" style="color:'+item.cor+'" id="icon-confirmacao" ></i></div></div></div></div>');
            }
          });

          $("#chat-app-window_altura").animate({ scrollTop: altura }, "slow");
        },
    });

    monitora_outras_conversas();
  }

  function novaMensagem(){
    var altura = $("#chat-app-window_altura").height() * 100;
    var mensagem = $("#mensagem").val();
    var id_usuario = $("#id_usuario").val();
    var foto_conversa = $("#foto_usuario_logado").attr('src');

    var data = new Date();
    var hora    = data.getHours();          
    var min     = data.getMinutes();        

    var horario = hora + ':' + min;

    if(mensagem != "" && id_usuario != ""){
      $.ajax({
        type: 'POST',
            url: "<?php echo base_url(); ?>chat/cadastrarMensagem/",
            dataType: 'json',
            data: { 
              'mensagem': mensagem, 
              'id_usuario': id_usuario
          },
            cache: false,
            success: function (data) {
              if(data == 'TRUE'){

                // $("#chat_conversa").append('<li class="clearfix"><div class="message other-message pull-right"><img class="rounded-circle float-right chat-user-img img-50" src="'+foto_conversa+'" alt=""><div class="message-data"><span class="message-data-time">'+horario+'</span></div>'+mensagem+'<div class="float-right" id="confirmacao-leitura"><i class="mdi mdi-check-all" style="color:black" id="icon-confirmacao"></i></div></div></li>');

                $("#chat-app-window").append('<div class="chat chat-right"><div class="chat-avatar"><a class="avatar"><img src="'+foto_conversa+'" class="circle" alt="avatar" /></a></div><div class="chat-body"><div class="chat-text"><p>'+mensagem+'  <span class="message-data-time" style="font-size: 8px;">'+horario+'</span> </p><div class="float-right" style="padding: 5px;" id="confirmacao-leitura"><i class="mdi mdi-check-all" style="color:black" id="icon-confirmacao" ></i></div></div></div></div>');

                $("#chat-app-window_altura").animate({ scrollTop: altura }, "slow");
              }

              $("#mensagem").val("");
            },
      });
    }
    
  }

  function marcarComoLido(){
    var id_usuario = $("#id_usuario").val();
    $.ajax({
      type: 'POST',
          url: "<?php echo base_url(); ?>chat/marcar_como_lida/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
          cache: false,
          success: function (data) {
            if(data == 'TRUE'){
              $("#info"+id_usuario).html('');
            }
          },
    });
  }

  function marcarComoLido_Aux(id_usuario){
    $.ajax({
      type: 'POST',
          url: "<?php echo base_url(); ?>chat/marcar_como_lida/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
          cache: false,
          success: function (data) {
            if(data == 'TRUE'){
              $("#info"+id_usuario).html('');
            }
          },
    });
  }

  function monitora_conversa_atual(){
    var altura = $("#chat-app-window_altura").height() * 100;
    var id_usuario = $("#id_usuario").val();
    var foto_conversa = $("#foto_perfil_chat").attr("src");
    

    $.ajax({
        type: 'POST',
          url: "<?php echo base_url(); ?>chat/verifica_ja_lida/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
        cache: false,
        success: function (data) {
          
          if(data == 0){
            $(".mdi-check-all").css("color", "#37c8ff");
          }
        },
    });

    $.ajax({
        type: 'POST',
          url: "<?php echo base_url(); ?>chat/monitora_conversa_atual/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
        cache: false,
        success: function (data) {
          
          $.each(data, function(i, item) {
            
            // $("#chat_conversa").append('<li ><div class="message my-message"><img class="rounded-circle float-left chat-user-img img-50" src="'+foto_conversa+'" alt=""><div class="message-data text-right"><span class="message-data-time">'+item.hora+'</span></div>'+item.mensagem+'<div class="float-right" id="confirmacao-leitura"><i class="mdi mdi-check-all" style="color:#37c8ff" id="icon-confirmacao"></i></div></div></li>');

            $("#chat-app-window").append('<div class="chat"><div class="chat-avatar"><a class="avatar"><img src="'+foto_conversa+'" class="circle" alt="avatar" /></a></div><div class="chat-body"><div class="chat-text"><p>'+item.mensagem+'  <span class="message-data-time" style="font-size: 8px;">'+item.hora+'</span> </p><div class="float-right" style="padding: 5px;" id="confirmacao-leitura"><i class="mdi mdi-check-all" style="color:#37c8ff" id="icon-confirmacao" ></i></div></div></div></div>');
            
            $("#chat-app-window_altura").animate({ scrollTop: altura }, "slow");

            $("#info-data"+id_usuario).text(item.hora);
            if(item.notificacao != 1){
              play();
            }
            
          });
        },
    });
    setTimeout(monitora_conversa_atual, 5000);
  }

  function monitora_outras_conversas(){
    var id_usuario = $("#id_usuario").val();
    $.ajax({
        type: 'POST',
          url: "<?php echo base_url(); ?>chat/monitora_outras_conversas/",
          dataType: 'json',
          data: { 
            'id_usuario': id_usuario
        },
        cache: false,
        success: function (data) {
          
          $.each(data, function(i, item) {
            
            // $("#badge-outras-conversas"+item.id_usuario_bagde).html(item.mensagem+'<span class="float-right info" id="info'+item.id_usuario_bagde+'"><span class="badge badge-pill badge-info float-right" style="margin-left: 40px">'+item.qtd+'</span> ');
            
            $("#badge-outras-conversas"+item.id_usuario_bagde).html('<span class="float-right info" id="info'+item.id_usuario_bagde+'"><span class="badge badge pill red">'+item.qtd+'</span></span>');

            $("#ultima_mensagem_usuario"+item.id_usuario_bagde).text(item.mensagem);

            $("#hora_ultima_conversa"+item.id_usuario_bagde).text(item.data);

            if(item.notificacao != 1){
              play();
            }
            
          });
        },
    });
    setTimeout(monitora_outras_conversas, 5000);
  }

  function play(){
    audio = document.getElementById('audio_teste');
    audio.play();
  }

  function openModalFoto(){
    $('#modalFoto').modal();
  }
</script>

