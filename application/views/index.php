<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
if ($this->session->has_userdata('userprimesession')){ 
    redirect('painel/home', 'refresh');
}
?>
<!doctype html>
<html lang="en">

<head>
<title>VERI - Você no controle</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="VERI - Você no controle">
<meta name="author" content="Grupo DataByte LTDA">

<link rel="icon" href="<?php echo base_url('assets/images/logo/logo-icon.png'); ?>" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="assets/login/assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/login/assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/login/assets/vendor/animate-css/vivify.min.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="assets/login/assets/css/site.min.css">

</head>

<style type="text/css">
body {
background-image: url("assets/login/assets/images/background-image.png");
position: relative;
width: 100%;
height: 100%;
background-repeat: no-repeat;
 -webkit-background-size: cover;
-moz-background-size: cover;
background-size: cover;
-o-background-size: cover;
}
</style>

<body style="background: linear-gradient(0deg, #0b4182 1%, #1e88e5 100%);">
    <div class="pattern">
        <span class="red"></span>
        <span class="indigo"></span>
        <span class="blue"></span>
        <span class="green"></span>
        <span class="orange"></span>
    </div>
    <div class="auth-main particles_js">        
        <div class="auth_div vivify popIn"> 
            <div class="card">
                <div class="body" style="background-color: #f9f9f9">
                    <p class="lead"><img src="assets/img/logo/logo-transparent (1).png" style="width: 100px;"><br><b>&nbsp;&nbsp;&nbsp;&nbsp; Você no Controle!</b></p>
                    <form class="form-auth-small m-t-20" method="post">

                        <?php 
                        if ($msg == 'invalido'){
                            echo '
                            <div class="alert alert-danger alert-dismissible mensagem" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>ERRO!</strong> <span class="alertas">'.$this->session->flashdata('invalido').'</span>
                            </div>';
                        }
                        if ($msg == 'inativo'){                 
                            echo '
                            <div class="alert alert-warning alert-dismissible mensagem" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>ATENÇÃO!</strong> <span class="alertas">'.$this->session->flashdata('inativo').'</span>
                            </div>';
                        }
                        if ($msg == 'expirado'){
                            echo '
                            <div class="alert alert-warning alert-dismissible mensagem" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>ATENÇÃO!</strong> <span class="alertas">'.$this->session->flashdata('expirado').'</span>
                            </div>';
                        }
                        ?>

                        <div class="form-group">
                            <label for="signin-email" class="control-label sr-only">Email</label>
                            <input type="email" class="form-control round" name="login" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="signin-password" class="control-label sr-only">Password</label>
                            <input type="password" class="form-control round" name="senha" placeholder="Senha">
                        </div>
                        <div class="form-group clearfix">
                            <label class="fancy-checkbox element-left">
                                <input type="checkbox">
                                <span>Lembrar?</span>
                            </label>								
                        </div>
                        <button type="submit" class="btn btn-primary btn-round btn-block" name="btn_login" style="background-color: #11569f">ACESSAR</button>
                        <div class="bottom">
                            <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="<?php echo base_url("login/esqueceu_senha"); ?>">Esqueceu a senha?</a></span>
                            <span>Desenvolvido por<a href="https://databytetecnologia.com.br/" target="_blank"> Grupo Databyte</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="particles-js"></div>
    </div>
    <!-- END WRAPPER -->
    
<script src="assets/login/assets/bundles/libscripts.bundle.js"></script>    
<script src="assets/login/assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/login/assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>
