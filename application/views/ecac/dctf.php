<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>" rel="stylesheet" />

<style>
.datepicker{z-index:1151 !important;}

.badge-yellow{
  background-color: #ffc50e;
  color: #fff;
}

.badge-orange{
  background-color: #fb790a;
  color: #fff;
}

</style>

<style type="text/css">
 .swal2-styled{
    background-color: #dc2727 !important;
 } 

.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .righter{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}


.left, .center, .right, .lefter, .righter {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}
.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/refresh.png'); ?>);
}
.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}


.modal-dialog {
    width: 1000px;
    max-width : 1000px;
}
</style>

<script type="text/javascript">
<?php
switch ($m) {
        case "01":    $mes = Janeiro;     break;
        case "02":    $mes = Fevereiro;   break;
        case "03":    $mes = Marco;       break;
        case "04":    $mes = Abril;       break;
        case "05":    $mes = Maio;        break;
        case "06":    $mes = Junho;       break;
        case "07":    $mes = Julho;       break;
        case "08":    $mes = Agosto;      break;
        case "09":    $mes = Setembro;    break;
        case "10":    $mes = Outubro;     break;
        case "11":    $mes = Novembro;    break;
        case "12":    $mes = Dezembro;    break; 
 } 

?>
</script>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div> 

<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <!-- <a class="btn mr-1 shadow-z-2 btn-round btn-info pull-right" data-toggle="modal" data-target="#modal_filtro" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Filtro"><span class="fa fa-filter"></span></a> -->
                     
                      <h4 class="card-title">DCTF</h4>
                      <h6 class="card-title" style="color: red">Atenção ! A data limite é uma estimativa calculada pelo sistema com base na regra do 15º dia útil do 2º mês subsequente. Essa data pode não ser a exata que deve ser entregue a DCTF.</h6>
                    </div>

                    <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_1"> 

                        <br>
                        <div class="card-block text-center col-">
                          <div class="form-body">
                            <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("dctf/index/anterior/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                              <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="block-button">
                                  <a class="btn btn-outline-success btn-lg btn-block"><?php if($mes == "Marco"){
                                    echo "Março";
                                  }else{
                                    echo $mes;
                                  }?> </a>
                                   <?php echo $ano ?>
                                </div>
                              </div>

                              <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("dctf/index/proximo/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                            </div>
                          </div>
                        </div>

                    </div>

                    <!-- OPÇÃO 02 - -->
                    <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_2" style="display: none"> 
                        <br>
                        <?php echo form_open("dctf/index", array('role'=>'form')); ?>
                        <div class="card-block text-center col-">
                          <div class="form-body">
                            <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:diminuiAno()">                             
                                </a> 
                              </span>
                              <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="block-button">
                                  <input type="text" name="ano_selecionado" id="ano_selecionado" value="<?php echo $ano ?>" class="btn btn-outline-success btn-lg btn-block" placeholder="<?php echo $ano ?>" />
                                </div>
                              </div>

                              <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:aumentaAno()">                             
                                </a> 
                              </span>
                            </div>
                          </div>
                        </div>

                        <div class="card-block text-center col-" style="margin-top: 5px;">
                          <div class="form-body">
                            <div class="row justify-content-center"> 
                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Dezembro')" name="mes_selecionado" id="mes_selecionado" value="01">JAN.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Janeiro')" name="mes_selecionado" id="mes_selecionado" value="02">FEV.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Fevereiro')" name="mes_selecionado" id="mes_selecionado" value="03">MAR.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Março')" name="mes_selecionado" id="mes_selecionado" value="04">ABR.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Abril')" name="mes_selecionado" id="mes_selecionado" value="05">MAI.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Maio')" name="mes_selecionado" id="mes_selecionado" value="06">JUN.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Junho')" name="mes_selecionado" id="mes_selecionado" value="07">JUL.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Julho')" name="mes_selecionado" id="mes_selecionado" value="08">AGO.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Agosto')" name="mes_selecionado" id="mes_selecionado" value="09">SET.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Setembro')" name="mes_selecionado" id="mes_selecionado" value="10">OUT.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Outubro')" name="mes_selecionado" id="mes_selecionado" value="11">NOV.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Novembro')" name="mes_selecionado" id="mes_selecionado" value="12">DEZ.</button>  
                                </div>
                              </div> 
                            </div>
                          </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- OPÇÃO 02 - -->

                </div>

                <div class="card-body">
                  <div class="form-group col-lg-3 col-xs-3 pull-right">
                    <label>Filtro:</label>
                    <?php 
                    $valores_filtro = array(
                        'TODAS'=>'Todas',
                        'PENDENTE'=>'Vencidas',
                        'REGULAR'=>'Transmitidas',
                        'ANALISAR'=>'Analisar',
                        'SEM_MOVIMENTO'=>'Sem Movimento'
                        
                    );
                    echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
                    ?>
                  </div>
                  <div class="form-group pull-right" style="margin-top: 27px">
                    <button class="btn btn-lg btn-default btn-rounded" onclick="sem_movimento_lote()"  style="color: #FFFFFF; background-color: #24a0e9;margin-right: 5px;">Sem Movimentação Em Lote    <img style="width: 25px;" src="<?php echo base_url('assets/img/icons/no_money.png'); ?>"></button>
                  </div>
                  <br> <br>
                    <div class="card-block">
                        <table class="table table-bordered table-responsive-md text-center" id="example" data-toggle="datatables">
                            <thead>
                                <tr>
                                    
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Período</th>
                                    <th>Status</th>
                                    <th>Situação  <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone Federal localizado na parte superior da tela sempre que uma DCTF for marcada como sem movimento, e também quando for transmitida. Faltando 3 dias antes do vencimento e 1 dia após vencido é gerado também um alerta."><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                                    <th>Data Limite Estimada</th>
                                    <th>Data da Recepção</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($consulta){ 
                                    foreach ($consulta as $e){
                                      $razao_aux = str_replace("'", "", $e->razao_social);
                                      $razao_aux = str_replace("&", "", $razao_aux);

                                      $e->razao_social = $razao_aux;
                                      
                                      ?>
                                <tr>
                                   
                                    <td><?php echo $e->razao_social; ?></td>
                                    <td><?php echo $e->cnpj; ?></td>
                                    <td><?php echo $e->periodo; ?></td>
                                    <td><?php echo $e->tipo_status; ?></td>

                                    <!-- <td> -->
                                      <?php 
                                        $data_final = strtotime($ultimo_dia_transmitir);

                                        $df = new DateTime($ultimo_dia_transmitir);
                                        $da = new DateTime(date(''));
                                        $intervalo = $df->diff($da);
                                      ?>

                                      <?php if ($e->status_geral == "TRANSMITIDA"){ ?>
                                        <?php 


                                        echo '<td><span class="badge badge-success" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Transmitida"  data-content="DCTF entregue">Transmitida</span></td>'; 

                                        echo "<td><span class='badge badge-success badge-shadow'  data-container='body'  data-toggle='popover' data-trigger='hover' data-placement='top' >".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span></td>";
                                        ?>

                                      <?php } else if($e->status_geral == "VENCIDA") { ?>
                                        <?php 
                                        echo '<td><span class="badge badge-danger" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Vencida" data-content="Vencida">Vencida</i></span></td>'; 

                                        echo "<td><span class='badge badge-danger' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Vencida' data-content='Prazo para entrega da DCTF vencido à ".$intervalo->format('%a')." dia(s)'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span></td>";
                                        ?>

                                      <?php } else if($e->status_geral == "SEM_MOVIMENTO") { ?>
                                        <?php 
                                        echo '<td><span class="badge badge-orange" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Sem Movimento" data-content="'.$e->texto.'" >Sem Movimento</span></td>'; 

                                        echo "<td><span class='badge badge-orange'  data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Sem Movimento' data-content='Sem Movimento'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span></td>";

                                        ?>

                                      <?php } else if($e->status_geral == "EM_ANALISE") { ?>
                                        <?php 
                                        echo '<td><span class="badge badge-yellow" data-toggle="tooltip" data-placement="top" title="Em Análise">Em Análise</span></td>'; 

                                        echo "<td><span class='badge badge-yellow'  data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Em Análise' data-content='Em Análise'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span></td>";

                                        ?>

                                      <?php } ?> 

                                    <!-- </td> -->
                                    

                                    <td><?php echo $e->data_recepcao; ?></td>
                                    
                                    <?php if(empty($e->sem_dctf != 1)){ ?>

                                      <?php if(!empty($e->caminho_download_declaracao)){ ?>
                                        <td>
                                        <div class="all">
                                          <div class="lefter">
                                            <a href="mailto:?subject=Notificação Sistema VERI&body=A declaração do DCTF de <?php echo $e->periodo; ?> da empresa <?php echo $e->razao_social; ?> está disponível no link <?php echo $e->caminho_download_declaracao; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>
                                          <div class="left">
                                             <a href="https://api.whatsapp.com/send?text=A declaração do DCTF de <?php echo $e->periodo; ?> da empresa <?php echo $e->razao_social; ?> está disponível no link <?php echo $e->caminho_download_declaracao; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                              <div class="explainer"></div>
                                              <div class="text">Whatsapp</div>
                                            </a>
                                          </div>
                                          <div class="center">
                                            <a href="<?php echo $e->caminho_download_declaracao; ?>" download  data-toggle="tooltip" data-placement="top" title="Download" class="">
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>
                                          <div class="right">
                                            <a href="<?php echo $e->caminho_download_declaracao; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                              <div class="explainer"></div>
                                              <div class="text">Visualizar</div>
                                            </a>
                                          </div>

                                          <div class="righter">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Buscar Declaração Novamente" class="" onclick="buscarDeclaracao('<?php echo $e->cnpj; ?>','<?php echo $e->periodo; ?>')">
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>

                                        </div>
                                        </td>
                                      <?php }else{ ?>
                                        <td>
                                        <div class="all">
                                          <div class="center">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Consultar Declaração" class="" onclick="buscarDeclaracao('<?php echo $e->cnpj; ?>','<?php echo $e->periodo; ?>')">
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>
                                        </div>
                                        </td>
                                      <?php } ?>


                                    <?php }elseif($e->status_geral != "SEM_MOVIMENTO"){ ?>

                                      <?php $razao_aux = str_replace("'", "", $e->razao_social);?>
                                    <td>
                                      <a href="javascript:marcar_sem_movimento('<?php echo $e->cnpj; ?>', '<?php echo $m; ?>', '<?php echo $ano; ?>', '<?php echo $razao_aux; ?>')" data-toggle="tooltip" data-placement="top" title="Sem movimentação no período" class=""><img style="width: 20px;" src="<?php echo base_url('assets/img/icons/no_money.png'); ?>"></a>
                                      
                                    </td>
                                    <?php }else{ ?>
                                      <td></td>
                                    <?php } ?>

                                </tr>
                            <?php } 
                            }
                            ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Período</th>
                                    <th>Status</th>
                                    <th>Situação  <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone Federal localizado na parte superior da tela sempre que uma DCTF for marcada como sem movimento, e também quando for transmitida. Faltando 3 dias antes do vencimento e 1 dia após vencido é gerado também um alerta."><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                                    <th>Data Limite</th>
                                    <th>Data da Recepção</th>
                                    <th>Ações</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


<div class="modal fade" id="modalRegime" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tipoRegimeModal">Selecione as empresas que deseja marcar sem movimento no período de <?php echo $mes."/".$ano; ?> </h5>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php echo form_open("dctf/sem_movimento_lote/$m/$ano", array('role'=>'form')); ?>
        <div class="modal-body">
          
            
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-4">
                            <a id="select-all" href="#" class="btn btn-success">Selecionar Todas</a>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group mb-4">
                            <a id="deselect-all" href="#" style="float: right;" class="btn btn-danger">Desselecionar Todas</a>
                        </div>
                    </div>

                    <div class="col-md-12" style="width: inherit !important;">
                        <select class="searchable" id="custom-headers" multiple="multiple" name="multi_select[]">
                            <?php foreach ($array_empresas_sem_mov as $row) : ?>
                                <option value="<?php echo $row->cnpj; ?>" <?php echo $row->selected; ?>><?php echo $row->razao_social.' - '.$row->cnpj; ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                
            <br>
      </div>
      <div class="modal-footer">
          <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>

</div>
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script src="<?php echo base_url('assets/plugins/multiselect/js/jquery.multi-select.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/multiselect/js/jquery.quicksearch.js'); ?>"></script>

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    var url = window.location.href;
    url = url.substring(0,url.indexOf('?'));
    window.location.replace(url+"?filtro="+filtro+"");
  });
});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) { 

// BOTÃO DE BUSCAR ANOS E MESES
$('#mostrar_meses_opcao_1').click(function(event) {     
  $('#mostrar_meses_opcao_2').show(); 
  $('#mostrar_meses_opcao_1').hide();
});


});

</script>

<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  })
var table = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});

</script>

<script type="text/javascript">
    function diminuiAno(){
      var ano = $("#ano_selecionado").val();
      ano = parseInt(ano);
      $("#ano_selecionado").val(ano - 1);
    }
    function aumentaAno(){
      var ano = $("#ano_selecionado").val();
      ano = parseInt(ano);
      $("#ano_selecionado").val(ano + 1);
    }

    function envia_form(mes){
      var mes_selecionado = $("#mes_selecionado").val();
      var ano_selecionado = $("#ano_selecionado").val();

      if(mes == 'Dezembro'){
        ano_selecionado = ano_selecionado - 1;
      }
      

      window.location = '<?php echo base_url("dctf/index/proximo"); ?>'+"/"+mes+"/"+ano_selecionado;
    }

    function update_simples(id){
      swal({
        title: 'Simples Nacional',
        text: 'Deseja Marcar essa empresa como Simples Nacional ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Não, cancelar!',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
      }).then(function () {

        $.post("<?php echo base_url();?>dctf/update_simples",
        {
            id:id
        },
        function(data){
          if(data == 1){
            swal(
              'Concluído!',
              'A empresa foi marcada como Simples Nacional',
              'success'
            ).done(); window.setTimeout(function(){

                  // Move to a new location or you can do something else
                  window.location = global_url;

              }, 2000);
          }
        });

        
      }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'Processo cancelado!',
            'error'
          ).done();
        }
      }).done();

      
    }

    function buscarDeclaracao(cnpj, periodo){
      swal({ 
            title: "Buscando...",
            text: 'Buscando Declaração.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });

        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/dctf_declaracao/buscar_declaracao/";
        // url = "https://veri.com.br/crons-api/dctf_declaracao/buscar_declaracao/";
        var request = $.post(url, { cnpj: cnpj, periodo: periodo, banco: banco }, function(data, status){

            if(data != "ERRO"){
              swal.close();
              // window.open(data, '_blank');
              setTimeout(reload_page, 5000)
              
            }else{
              $(document).ready(function() {                
                    swal({
                        title: "Erro ao buscar declaração !",
                        text: "Erro",
                        type: "error"
                    });
                });
            }
        });
    }

    function reload_page(){
      window.location = global_url;
    }


    function marcar_sem_movimento(cnpj, mes, ano, razao_social){
      var raz = "<label style='font-weight: bold;'>"+razao_social+"</label>";
      swal({
        title: 'Sem Movimento',
        text: 'Deseja Marcar a empresa '+raz+' como sem movimentação no período ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Não, cancelar!',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
      }).then(function () {

        $.post("<?php echo base_url();?>dctf/marcar_sem_movimento",
        {
            cnpj:cnpj,
            mes:mes,
            ano:ano
        },
        function(data){
          if(data == 1){
            swal(
              'Concluído!',
              'A empresa foi marcada como sem movimentação no período',
              'success'
            ).done(); window.setTimeout(function(){

                  // Move to a new location or you can do something else
                  window.location = global_url;

              }, 2000);
          }
        });

        
      }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'Processo cancelado!',
            'error'
          ).done();
        }
      }).done();

      
    }
</script>


<script type="text/javascript">
    function sem_movimento_lote(){
      $('#modalRegime').modal('show');
    }
    //$('#custom-headers').multiSelect();

    $('.searchable').multiSelect({
        selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Buscar...'>",
        selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Buscar...'>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    $('#select-all').click(function(){
        $('#custom-headers').multiSelect('select_all');
        return false;
    });
    $('#deselect-all').click(function(){
        $('#custom-headers').multiSelect('deselect_all');
        return false;
    });
</script>