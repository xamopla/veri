<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2><?php echo $nome_tela; ?></h2>
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'listar_eprocessos_todos'=>'Todas',
                  'listar_eprocessos_ativos'=>'Ativos',
                  'listar_eprocessos_inativos'=>'Inativos'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br> <br>
            <div class="table-responsive row">
                <table class="table table-striped" id="example" data-toggle="datatables">
                    <thead class="thead-default thead-lg">
                        <tr  id="passo11">
                            <th style="display: none;"></th>
                            <th style="font-size: 12px;">Razão Social</th>
                            <th style="font-size: 12px;">CNPJ</th>
                            <th style="font-size: 12px;">Número do Processo</th> 
                            <th style="font-size: 12px;">Data do Protocolo</th> 
                            <th style="font-size: 12px;">Tipo do Processo</th> 
                            <th style="font-size: 12px;">Subtipo do Processo</th> 
                            <th style="font-size: 12px;">Localização</th> 
                            <th style="font-size: 12px;">Situação</th> 
                            <th style="font-size: 12px;">Ativo ?</th> 
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ($consulta){ 
                        foreach ($consulta as $e){
                                $razao_aux = str_replace("'", "", $e->razao_social);
                                $razao_aux = str_replace("&", "", $razao_aux);

                                $e->razao_social = $razao_aux;
                            ?>
                        <tr>
                              <td style="display: none;"><?php echo $e->dataProtocolo; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->razao_social; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->cnpj_completo; ?></td>
                              

                              <td style="font-size: 12px;"><?php echo $e->numero; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->dataProtocoloFormatada; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->tipo; ?></td> 
                              <td style="font-size: 12px;"><?php echo $e->subtipo; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->localizacao; ?></td>

                              <?php
                                if ($e->ativo == 1){
                                    echo "<td><span class='badge badge-pill badge-success' style=' ' >".$e->situacao."</span></td>";
                                } else {
                                    echo "<td><span class='badge badge-pill badge-warning' style=' ' >".$e->situacao."</span></td>";
                                }
                              ?>

                              <?php
                                if ($e->ativo == 1){
                                    echo "<td><span class='badge badge-pill badge-success' style=' ' >ATIVO</span></td>";
                                } else {
                                    echo "<td><span class='badge badge-pill badge-danger' style=' ' >INATIVO</span></td>";
                                }
                              ?>

                              

                              <td>   

                                <?php
                                if ($e->ativo == 1){ ?>
                                    <a href="javascript:void(0)" onclick="consulta_historico('<?php echo $e->idProcesso;?>',1)" data-toggle="tooltip" data-placement="top" title="Consultar Histórico" class="btn btn-default b" style="color: #24a0e9"><i class="material-icons" aria-hidden="true">search</i></a> 
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="consulta_historico('<?php echo $e->idProcesso;?>',0)" data-toggle="tooltip" data-placement="top" title="Consultar Histórico" class="btn btn-default b" style="color: #24a0e9"><i class="material-icons" aria-hidden="true">search</i></a> 
                                <?php }
                              ?>

                              
                              </td>
                        </tr>
                    <?php } 
                    }
                    ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- Modal das mensagens -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_historico">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Consultar Histórico do Processo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <table id="modal_table_ecac" class="table table-striped" style="width: 100% !important">    
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th>Unidade</th>
                          <th>Equipe/Operação</th>
                          <th>Atividade</th>
                          <th>Data de Entrada</th>
                          <th>Tempo na Atividade</th>
                          <th>Tempo Médio na Atividade (Últimos dois Anos)</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>                
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('ecac'); ?>/"+filtro+"");
  });
});
</script>
<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
var table2 = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });
  
   table2.search('<?php echo $razao_social_filtro; ?>').draw();
    table2.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});


function consulta_historico(id_processo, ativo){
  $.ajax({
    type: "POST",
    url: '<?php echo base_url('ecac/consulta_historico_eprocesso'); ?>',
    data: {id_processo:id_processo, ativo: ativo},
    async: true,
    success: function(result){
        var data = JSON.parse(result);
        if(data == "") {
            var array = [];
            popularDataTable(array);  
            jQuery('[data-toggle="popover"]').popover();   
            jQuery('[data-toggle="tooltip"]').tooltip();   
            $('#modal_historico').modal('show');

        }else{
          var array = json2array(data);
          popularDataTable(array);
          jQuery('[data-toggle="popover"]').popover();
          jQuery('[data-toggle="tooltip"]').tooltip(); 

          $('#modal_historico').modal('show');
        }
    }
  });

}

function popularDataTable(json){
  $.fn.dataTable.moment( 'DD/MM/YYYY HH:mm:ss' ); 
  $('#modal_table_ecac').dataTable().fnDestroy();
  var table = $('#modal_table_ecac').DataTable({
      "columnDefs": [
          {   
            type: 'date-br',
              "targets": [ 4 ],

              // "visible": false,
              // "searchable": false
          }
      ],
      lengthMenu: [10, 20, 50, 100],
      "order": [[ 3, "desc" ]],
      language: {
          "sEmptyTable": "Nenhum registro encontrado",
          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "_MENU_ resultados por página",
          "sLoadingRecords": "Carregando...",
          "sProcessing": "Processando...",
          "sZeroRecords": "Nenhum registro encontrado",
          "sSearch": "Pesquisar",
          "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
          },
          "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
          },
          "select": {
              "rows": {
                  "_": "Selecionado %d linhas",
                  "0": "Nenhuma linha selecionada",
                  "1": "Selecionado 1 linha"
              }
          }
      },
      data: json
  });


}


function json2array(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];

    var unidade = "";
    var equipe = "";
    var atividade = "";
    var data_entrada = "";
    var tempo = "";
    var tempo_medio = "";

    for(var i in data){
      array = [];

      unidade = "";
      equipe = "";
      atividade = "";
      data_entrada = "";
      tempo = "";
      tempo_medio = "";

      if(data[i].siglaUnidade != null){
        unidade = data[i].siglaUnidade;
      }
      if(data[i].equipeOuOperacao != null){
        equipe = data[i].equipeOuOperacao;
      }
      if(data[i].nomeAtividade != null){
        atividade = data[i].nomeAtividade;
      }
      if(data[i].dataEntradaFormatada != null){
        data_entrada = data[i].dataEntradaFormatada;
      }
      if(data[i].tempoAtividade != null && data[i].tempoAtividade != ""){
        tempo = data[i].tempoAtividade+" dia(s) ";
      }
      if(data[i].tempoMedioAtividade != null && data[i].tempoMedioAtividade != ""){
        tempo_medio = data[i].tempoMedioAtividade+" dia(s) ";
      }

      array.push('<td style="text-align: left;">'+unidade+'</td>');
      array.push('<td style="text-align: left;">'+equipe+'</td>');
      array.push('<td style="text-align: left;">'+atividade+'</td>');
      array.push('<td style="text-align: left;">'+data_entrada+'</td>');
      array.push('<td style="text-align: left;">'+tempo+'</td>');
      array.push('<td style="text-align: left;">'+tempo_medio+'</td>');

      arrayMultiple.push(array);
    }
    
    return arrayMultiple;
}

</script>