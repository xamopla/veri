<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<style>
.datepicker{z-index:1151 !important;}

.badge-yellow{
  background-color: #ffc50e;
  color: #fff;
}

.badge-orange{
  background-color: #fb790a;
  color: #fff;
}

</style>

<style type="text/css">
  
.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.left, .center, .right, .lefter, .righter {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}
.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
}
.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}

</style>
<script type="text/javascript">
<?php
switch ($m) {
        case "01":    $mes = 'Janeiro';     break;
        case "02":    $mes = 'Fevereiro';   break;
        case "03":    $mes = 'Marco';       break;
        case "04":    $mes = 'Abril';       break;
        case "05":    $mes = 'Maio';        break;
        case "06":    $mes = 'Junho';       break;
        case "07":    $mes = 'Julho';       break;
        case "08":    $mes = 'Agosto';      break;
        case "09":    $mes = 'Setembro';    break;
        case "10":    $mes = 'Outubro';     break;
        case "11":    $mes = 'Novembro';    break;
        case "12":    $mes = 'Dezembro';    break;
 } 

?>
</script>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div> 

<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <!-- <a class="btn mr-1 shadow-z-2 btn-round btn-info pull-right" data-toggle="modal" data-target="#modal_filtro" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Filtro"><span class="fa fa-filter"></span></a> -->
                     
                      <h4 class="card-title">PGDAS</h4>
                      <h6 class="card-title" style="color: red">Atenção ! A data limite é uma estimativa calculada pelo sistema com base na regra do dia 20 do 1º mês subsequente. Essa data pode não ser a exata que deve ser entregue a PGDAS.</h6>
                    </div>

                    <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_1"> 

                        <br>
                        <div class="card-block text-center col-">
                          <div class="form-body">
                            <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("pgdas/index/anterior/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                              <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="block-button">
                                  <a class="btn btn-outline-success btn-lg btn-block"><?php if($mes == "Marco"){
                                    echo "Março";
                                  }else{
                                    echo $mes;
                                  }?> </a>
                                   <?php echo $ano ?>
                                </div>
                              </div>

                              <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("pgdas/index/proximo/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                            </div>
                          </div>
                        </div>

                    </div>

                    <!-- OPÇÃO 02 - -->
                    <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_2" style="display: none"> 
                        <br>
                        <?php echo form_open("pgdas/index", array('role'=>'form')); ?>
                        <div class="card-block text-center col-">
                          <div class="form-body">
                            <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:diminuiAno()">                             
                                </a> 
                              </span>
                              <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="block-button">
                                  <input type="text" name="ano_selecionado" id="ano_selecionado" value="<?php echo $ano ?>" class="btn btn-outline-success btn-lg btn-block" placeholder="<?php echo $ano ?>" />
                                </div>
                              </div>

                              <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:aumentaAno()">                             
                                </a> 
                              </span>
                            </div>
                          </div>
                        </div>

                        <div class="card-block text-center col-" style="margin-top: 5px;">
                          <div class="form-body">
                            <div class="row justify-content-center"> 
                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Dezembro')" name="mes_selecionado" id="mes_selecionado" value="01">JAN.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Janeiro')" name="mes_selecionado" id="mes_selecionado" value="02">FEV.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Fevereiro')" name="mes_selecionado" id="mes_selecionado" value="03">MAR.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Março')" name="mes_selecionado" id="mes_selecionado" value="04">ABR.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Abril')" name="mes_selecionado" id="mes_selecionado" value="05">MAI.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Maio')" name="mes_selecionado" id="mes_selecionado" value="06">JUN.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Junho')" name="mes_selecionado" id="mes_selecionado" value="07">JUL.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Julho')" name="mes_selecionado" id="mes_selecionado" value="08">AGO.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Agosto')" name="mes_selecionado" id="mes_selecionado" value="09">SET.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Setembro')" name="mes_selecionado" id="mes_selecionado" value="10">OUT.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Outubro')" name="mes_selecionado" id="mes_selecionado" value="11">NOV.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Novembro')" name="mes_selecionado" id="mes_selecionado" value="12">DEZ.</button>  
                                </div>
                              </div> 
                            </div>
                          </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- OPÇÃO 02 - -->

                </div>

                <div class="card-body">
                  <div class="form-group col-lg-4 col-xs-12 pull-right">
                    <label>Filtro:</label>
                    <?php 
                    $valores_filtro = array(
                        'TODAS'=>'Todas',
                        'PENDENTE'=>'Vencidas',
                        'REGULAR'=>'Transmitidas',
                        'ANALISAR'=>'Analisar',
                        'SEM_MOVIMENTO'=>'Sem movimento'
                        
                    );
                    echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
                    ?>
                  </div><br> <br>
                    <div class="card-block">
                        <table class="table table-bordered table-responsive text-center" id="example" data-toggle="datatables">
                            <thead>
                                <tr style="background-color:#DEF0C1">
                                  <th class="pa" colspan="13">PA <?php echo $m.'/'.$ano; ?></th>
                                </tr>
                                <tr style="background-color:#F0F0F0">
                                    <th colspan="7" class="header">Declaração</th>
                                    <th colspan="6" class="header">DAS</th>
                                </tr>
                                <tr>
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Nº Declaração</th>
                                    <th>Data/hora Transmissão</th>
                                    <th>Recibo</th>
                                    <th>Declaração</th>
                                    <th>Status PGDAS <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone Federal localizado na parte superior da tela sempre que estiver faltando 3 dias antes de vencer e um dia após o vencimento do PGDAS, quando constar como transmitida em nossa ferramenta também será gerado um alerta."><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                                    <th>Nº DAS</th>
                                    <th>Data/hora Emissão</th>
                                    <th>Extrato</th>
                                    <th>Status DAS</th>
                                    <th>Data Limite Estimada</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($consulta){ 
                                    foreach ($consulta as $e){
                                      $razao_aux = str_replace("'", "", $e->razao_social);
                                      $razao_aux = str_replace("&", "", $razao_aux);

                                      $e->razao_social = $razao_aux;

                                      ?>
                                <tr>
                                   
                                    <td><?php echo $e->razao_social; ?></td>
                                    <td><?php echo $e->cnpj; ?></td>
                                    <td><?php echo $e->numero_declaracao; ?></td>
                                    <td><?php echo $e->data_hora_transmissao; ?></td>
                                    <td>


                                      <?php if(!empty($e->numero_declaracao) ){ ?>
                                      <div class="all">
                                        <div class="lefter">
                                          <a href="javascript:void(0)" onclick="enviar_email_recibo('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->razao_social; ?>', '<?php echo $e->caminho_download_recibo; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="left">
                                          <a href="javascript:void(0)" onclick="enviar_whatsapp_recibo('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->razao_social; ?>', '<?php echo $e->caminho_download_recibo; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Whatsapp</div>
                                          </a>
                                        </div>
                                        <div class="center">
                                          <a href="javascript:void(0)" onclick="baixar_recibo('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->caminho_download_recibo; ?>')" download data-toggle="tooltip" data-placement="top" title="Download" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="right">
                                          <a href="javascript:void(0)" onclick="visualizar_recibo('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->caminho_download_recibo; ?>')" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Visualizar</div>
                                          </a>
                                        </div>
                                      </div>

                                      <?php } ?>
                                    </td>

                                    <td> 

                                      <?php if(!empty($e->numero_declaracao) ){ ?>
                                        <div class="all">
                                        <div class="lefter">
                                           <a href="javascript:void(0)" onclick="enviar_email_declaracao('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->razao_social; ?>', '<?php echo $e->caminho_download_declaracao; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="left">
                                          <a href="javascript:void(0)" onclick="enviar_whatsapp_declaracao('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->razao_social; ?>', '<?php echo $e->caminho_download_declaracao; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Whatsapp</div>
                                          </a>
                                        </div>
                                        <div class="center">
                                          <a href="javascript:void(0)" onclick="baixar_declaracao('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->caminho_download_declaracao; ?>')" download data-toggle="tooltip" data-placement="top" title="Download" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="right">
                                          <a href="javascript:void(0)" onclick="visualizar_declaracao('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->caminho_download_declaracao; ?>')" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Visualizar</div>
                                          </a>
                                        </div>
                                        <!-- <div class="righter">
                                          <div class="text">SEO</div>
                                        </div> -->
                                      </div>
                                      <?php } ?>
                                    </td>

                                    <?php 
                                        $data_final = strtotime($ultimo_dia_transmitir);

                                        $df = new DateTime($ultimo_dia_transmitir);
                                        $da = new DateTime(date(''));
                                        $intervalo = $df->diff($da);
                                    ?>

                                    <?php if ($e->status_geral == "TRANSMITIDA"){ ?>
                                      <?php 


                                      echo '<td><span class="badge badge-success" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Transmitida"  data-content="PGDAS transmitido">Transmitida</span></td>'; 

                                      ?>

                                    <?php } else if($e->status_geral == "VENCIDA") { ?>
                                      <?php 
                                      echo '<td><span class="badge badge-danger" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Vencida" data-content="Vencida">Vencida</i></span></td>'; 

                                      ?>

                                    <?php } else if($e->status_geral == "EM_ANALISE") { ?>
                                      <?php 
                                      echo '<td><span class="badge badge-yellow" data-toggle="tooltip" data-placement="top" title="Em Análise">Em Análise</span></td>'; 


                                      ?>

                                    <?php } ?> 

                                    <td><?php echo $e->numero_das; ?></td>
                                    <td><?php echo $e->data_hora_emissao; ?></td>
                                    <td>
                                        <?php if(!empty($e->numero_das) ){ ?>

                                            <div class="all">
                                        <div class="lefter">
                                          <a href="javascript:void(0)" onclick="enviar_email_extrato('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->numero_das; ?>', '<?php echo $e->razao_social; ?>', '<?php echo $e->caminho_download_extrato; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="left">
                                          <a href="javascript:void(0)" onclick="enviar_whatsapp_extrato('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->numero_das; ?>', '<?php echo $e->razao_social; ?>', '<?php echo $e->caminho_download_extrato; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Whatsapp</div>
                                          </a>
                                        </div>
                                        <div class="center">
                                          <a href="javascript:void(0)" onclick="baixar_extrato('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->caminho_download_extrato; ?>')" download data-toggle="tooltip" data-placement="top" title="Download" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="right">
                                          <a href="javascript:void(0)" onclick="visualizar_extrato('<?php echo $e->numero_declaracao; ?>', '<?php echo $e->caminho_download_extrato; ?>')" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Visualizar</div>
                                          </a>
                                        </div>
                                        <!-- <div class="righter">
                                          <div class="text">SEO</div>
                                        </div> -->
                                      </div>
                                        <?php } ?>
                                    </td>

                                    <?php if ($e->sub_status_geral == "SEM_MOVIMENTO"){ ?>
                                      <?php 

                                        echo '<td><span class="badge badge-orange" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Sem Movimento" data-content="Sem movimento no período" >Sem Movimento</span></td>';  

                                      ?>

                                    <?php } else if($e->sub_status_geral == "EM_ANALISE") { ?>
                                      <?php 
                                      echo '<td><span class="badge badge-yellow" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Em análise" data-content="Em análise">Em análise</i></span></td>'; 

                                      ?>

                                    <?php } else if(!empty($e->pago) && $e->pago == 'Sim') { ?>
                                      <?php 
                                      echo '<td><span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Pago">Pago</span></td>' ;

                                      ?>

                                    <?php } else if(!empty($e->pago) && $e->pago == 'Não') { ?>
                                      <?php 
                                      echo '<td><span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Não Pago">Não Pago</span></td>' ;

                                      ?>
                                    <?php } else { ?>
                                      <?php 
                                      echo '<td><span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Vencida">Vencida</span></td>' ;

                                      ?>

                                    <?php } ?>

                                    <!-- <td>
                                      <?php if(!empty($e->numero_das)){ ?> 

                                        <?php if (!empty($e->pago) && $e->pago == 'Sim'){ ?>
                                          <?php echo '<span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Sim">Sim</span>' ?>
                                        <?php } else { ?>
                                          <?php echo '<span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Não">Não</span>' ?>
                                        <?php } ?> 

                                      <?php }else{ ?>
                                        <?php echo '<span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Sim">Sem DAS</span>' ?>
                                      <?php } ?>

                                      
                                    </td> -->

                                    <?php if(empty($e->sem_pgdas == 1)){ ?>
                                      <td>
                                        <?php 

                                        $data_final = strtotime($ultimo_dia_transmitir);

                                        $df = new DateTime($ultimo_dia_transmitir);
                                        $da = new DateTime(date(''));
                                        $intervalo = $df->diff($da);

                                        if ($data_final < strtotime(date('Y-m-d'))) {
                                          echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Pendente' data-content='Prazo para entrega da PGDAS vencido à ".$intervalo->format('%a')." dia(s)'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span>";
                                        } else{
                                          echo "<span class='badge badge-yellow badge-shadow'  data-container='body'  data-toggle='popover' data-trigger='hover' data-placement='top' title='PGDAS dentro do prazo' data-content='Faltam ".$intervalo->format('%a')." dia(s) para a entrega da PGDAS'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span>";
                                        }

                                        ?>                        
                                      </td>
                                    <?php }else{ ?>
                                      <td>
                                        <?php echo "<span class='badge badge-success badge-shadow'  data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='PGDAS entregue' data-content='PGDAS entregue'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span>"; ?>
                                      </td>
                                    <?php } ?>

                                     

                                </tr>
                            <?php } 
                            }
                            ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Nº Declaração</th>
                                    <th>Data/hora Transmissão</th>
                                    <th>Recibo</th>
                                    <th>Declaração</th>
                                    <th>Status PGDAS <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone Federal localizado na parte superior da tela sempre que estiver faltando 3 dias antes de vencer e um dia após o vencimento do PGDAS, quando constar como transmitida em nossa ferramenta também será gerado um alerta."><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                                    <th>Nº DAS</th>
                                    <th>Data/hora Emissão</th>
                                    <th>Extrato</th>
                                    <th>Status DAS </th>
                                    <th>Data Limite Estimada</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<div class="modal fade" id="modalRecibo" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="numero_pgdas">Recibo PGDAS Nº </h5>
          <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div><i class="fa fa-share-alt" style="color: #24a0e9; font-size: large;">   <label style="color: #24a0e9;">Compartilhar</label></i></div>
          <div class="form-group">
            <div>

              <a href=""><i class="ion ion-social-whatsapp"></i></a>
            </div>
          </div>


          <label>Compartilhar</label>
          <div class="form-group">
            <div>

              <div class="form-group">
                <input id="userfile" name="userfile" class="file" type="file" required accept="image/*" />
              </div>
            </div>
          </div>


        </div>
    </div>
  </div>
</div>


<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    var url = window.location.href;
    url = url.substring(0,url.indexOf('?'));
    window.location.replace(url+"?filtro="+filtro+"");
  });
});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) { 

// BOTÃO DE BUSCAR ANOS E MESES
$('#mostrar_meses_opcao_1').click(function(event) {     
  $('#mostrar_meses_opcao_2').show(); 
  $('#mostrar_meses_opcao_1').hide();
});


});

</script>

<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  })
var table = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});

</script>

<script type="text/javascript">
    function diminuiAno(){
      var ano = $("#ano_selecionado").val();
      ano = parseInt(ano);
      $("#ano_selecionado").val(ano - 1);
    }
    function aumentaAno(){
      var ano = $("#ano_selecionado").val();
      ano = parseInt(ano);
      $("#ano_selecionado").val(ano + 1);
    }

    function envia_form(mes){
      var mes_selecionado = $("#mes_selecionado").val();
      var ano_selecionado = $("#ano_selecionado").val();

      if(mes == 'Dezembro'){
        ano_selecionado = ano_selecionado - 1;
      }
      

      window.location = '<?php echo base_url("pgdas/index/proximo"); ?>'+"/"+mes+"/"+ano_selecionado;
    }

    function openModalRecibo(numero){
      $.ajax({
          type: "POST",
          url: '<?php echo base_url('pgdas/busca_dados_modal'); ?>',
          async: true,
          data: { 
              'numero': numero
          },
          success: function(result){
            var resultado = JSON.parse(result);
            if(resultado != ''){

              $("#numero_pgdas").text("Recibo PGDAS Nº "+resultado.numero_declaracao);
              $("#modalRecibo").modal();
            }
          },
      });
    }

    function update_simples(id){
      swal({
        title: 'Não é Simples Nacional',
        text: 'Deseja Marcar essa empresa como não sendo do Simples Nacional ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Não, cancelar!',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
      }).then(function () {

        $.post("<?php echo base_url();?>pgdas/update_simples",
        {
            id:id
        },
        function(data){
          if(data == 1){
            swal(
              'Concluído!',
              'A empresa foi marcada como não sendo do Simples Nacional',
              'success'
            ).done(); window.setTimeout(function(){

                  // Move to a new location or you can do something else
                  window.location = global_url;

              }, 2000);
          }
        });

        
      }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'Processo cancelado!',
            'error'
          ).done();
        }
      }).done();

      
    }


    function marcar_sem_movimento(cnpj, mes, ano, razao_social){
      var raz = "<label style='font-weight: bold;'>"+razao_social+"</label>";
      swal({
        title: 'Sem Movimento',
        text: 'Deseja Marcar a empresa '+raz+' como sem movimentação no período ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Não, cancelar!',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
      }).then(function () {

        $.post("<?php echo base_url();?>pgdas/marcar_sem_movimento",
        {
            cnpj:cnpj,
            mes:mes,
            ano:ano
        },
        function(data){
          if(data == 1){
            swal(
              'Concluído!',
              'A empresa foi marcada como sem movimentação no período',
              'success'
            ).done(); window.setTimeout(function(){

                  // Move to a new location or you can do something else
                  window.location = global_url;

              }, 2000);
          }
        });

        
      }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'Processo cancelado!',
            'error'
          ).done();
        }
      }).done();

      
    }


    function buscar_pgdas_mes(cnpj, competencia){
      swal({
            title: "Buscado...",
            text: 'Buscando DAS.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });

        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/PgDas_individual/buscar_das_function/";
        var request = $.post(url, { cnpj: cnpj, banco: banco, competencia: competencia }, function(data, status){

            if(data.includes("ERRO")){

              $(document).ready(function() {
                  swal({
                      title: "Erro ao gerar DAS !",
                      text: "Erro",
                      type: "error"
                  });
              });

            }else{
              swal.close();
              // window.open(data, '_blank');
              setTimeout(reload_page, 5000);
            }
        });
    }
    // recibo
    function enviar_email_recibo(numero_declaracao, razao_social, caminho_download_recibo){
        if(caminho_download_recibo){
            window.open('mailto:?subject=Notificação Sistema VERI&body=O recibo do PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link '+caminho_download_recibo, '_blank').focus();
        }

        if(!caminho_download_recibo){
            swal({
                title: 'Consultando recibo, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_recibo_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.location='mailto:?subject=Notificação Sistema VERI&body=O recibo do PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link '+data;
                        window.location.reload()
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });

        }
    }
    function enviar_whatsapp_recibo(numero_declaracao, razao_social, caminho_download_recibo){
        if(caminho_download_recibo){
            window.open('https://api.whatsapp.com/send?text=O recibo dp PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link ' + caminho_download_recibo, '_blank').focus();
        }

        if(!caminho_download_recibo){
            swal({
                title: 'Consultando recibo, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_recibo_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.open('https://api.whatsapp.com/send?text=O recibo dp PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link ' + data, '_blank').focus();
                        window.location.reload()
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }
    function baixar_recibo(numero_declaracao, caminho_download_recibo){
        if(caminho_download_recibo){
            window.location = caminho_download_recibo
        }

        if(!caminho_download_recibo){
            swal({
                title: 'Consultando recibo, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_recibo_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.location=data;
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }
    function visualizar_recibo(numero_declaracao, caminho_download_recibo){
        if(caminho_download_recibo){
            window.open(caminho_download_recibo, '_blank').focus();
        }

        if(!caminho_download_recibo){
            swal({
                title: 'Consultando recibo, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_recibo_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.open(data, '_blank').focus();
                        window.reload()

                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }

    //declaracao
    function enviar_email_declaracao(numero_declaracao, razao_social, caminho_download_declaracao){
        if(caminho_download_declaracao){
            window.open('mailto:?subject=Notificação Sistema VERI&body=A declaração do PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link ' + caminho_download_declaracao, '_blank').focus();
        }

        if(!caminho_download_declaracao){
            swal({
                title: 'Consultando declaração, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_declaracao_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.location='mailto:?subject=Notificação Sistema VERI&body=A declaração do PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link ' + data;
                        window.location.reload()
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });

        }
    }
    function enviar_whatsapp_declaracao(numero_declaracao, razao_social, caminho_download_declaracao){
        if(caminho_download_declaracao){
            window.open('https://api.whatsapp.com/send?text=A declaração do PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link '+caminho_download_declaracao, '_blank').focus();
        }

        if(!caminho_download_declaracao){
            swal({
                title: 'Consultando declaração, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_declaracao_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.open('https://api.whatsapp.com/send?text=A declaração do PGDAS nº '+numero_declaracao+' da empresa '+razao_social+' está disponível no link '+ data, '_blank').focus();
                        window.location.reload()
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }
    function baixar_declaracao(numero_declaracao, caminho_download_declaracao){
        if(caminho_download_declaracao){
            window.location = caminho_download_declaracao
        }

        if(!caminho_download_declaracao){
            swal({
                title: 'Consultando declaração, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_declaracao_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.location=data;
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }
    function visualizar_declaracao(numero_declaracao, caminho_download_declaracao){
        if(caminho_download_declaracao){
            window.open(caminho_download_declaracao, '_blank').focus();
        }

        if(!caminho_download_declaracao){
            swal({
                title: 'Consultando declaração, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_declaracao_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.open(data, '_blank').focus();
                        window.reload()

                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }

    //extrato
    function enviar_email_extrato(numero_declaracao, numero_das, razao_social, caminho_download_extrato){
        if(caminho_download_extrato){
            window.open('mailto:?subject=Notificação Sistema VERI&body=O extrato do DAS nº '+numero_das+' da empresa '+razao_social+' está disponível no link '+caminho_download_extrato,'_blank').focus();
        }

        if(!caminho_download_extrato){
            swal({
                title: 'Consultando extrato, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_extrato_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.location='mailto:?subject=Notificação Sistema VERI&body=O extrato do DAS nº '+numero_das+' da empresa '+razao_social+' está disponível no link '+data;
                        window.location.reload()
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });

        }
    }
    function enviar_whatsapp_extrato(numero_declaracao, numero_das, razao_social, caminho_download_extrato){
        if(caminho_download_extrato){
            window.open('https://api.whatsapp.com/send?text=O extrato do DAS nº '+numero_das+' da empresa '+razao_social +' está disponível no link ' +caminho_download_extrato , '_blank').focus();
        }

        if(!caminho_download_extrato){
            swal({
                title: 'Consultando extrato, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_extrato_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.open('https://api.whatsapp.com/send?text=O extrato do DAS nº '+numero_das+' da empresa '+razao_social +' está disponível no link ' +data , '_blank').focus();
                        window.location.reload()
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }
    function baixar_extrato(numero_declaracao, caminho_download_extrato){
        if(caminho_download_extrato){
            window.location = caminho_download_extrato
        }

        if(!caminho_download_extrato){
            swal({
                title: 'Consultando extrato, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_extrato_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.location=data;
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }
    function visualizar_extrato(numero_declaracao,caminho_download_extrato){
        if(caminho_download_extrato){
            window.open(caminho_download_extrato, '_blank').focus();
        }

        if(!caminho_download_extrato){
            swal({
                title: 'Consultando extrato, aguarde...',
                type: 'warning',
                showConfirmButton: false
            })
            $.post("<?php echo base_url();?>crons-api/Das_extrato_emitir/emitir_function",
                {
                    numero_declaracao:numero_declaracao
                },
                function(data){
                    swal.close()
                    if(!data.includes('ERRO')){
                        window.open(data, '_blank').focus();
                        window.reload()
                    }else{
                        swal({
                            title: "Erro",
                            text: "Não foi possivel gerar devido a uma instabilidade no portal do e-Ecac. Tente novamente em alguns instântes.",
                            type: "error"
                        });
                    }
                });
        }
    }
</script>