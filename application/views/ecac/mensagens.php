<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Mensagens não lidas no e-CAC</h2> 
            <label style="color: red; font-size: 16px; background-color: yellow;" >Atenção, Conforme Parâmetro de Visualização do Portal e-CAC buscamos até as 50 últimas mensagens de cada empresa.<br>
             Mensagens mais antigas devem ser lidas direto pelo portal do e-CAC.</label>
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'TODAS'=>'Todas as empresas',
                  'LIDAS'=>'Lidas',
                  'NAO_LIDAS'=>'Não Lidas',
                  'IMPORTANTES'=>'Mensagens Importantes não lidas'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br>
            <h5 class="text-center">  <span class='label' style='color: #ff1404; font-size: 19px;'> <?php echo $qtd_registros ?></span> EMPRESAS&nbsp;&nbsp;&nbsp;  <span class='label' style="font-size: 19px; color: #ff1404;"> <?php echo $msg_nao_lidas ?></span> MENSAGENS NÃO LIDAS</h5>
            <div class="table-responsive row">
              <table class="table table-striped" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr>
                        <th width="1%">#</th>
                        <th style="font-size: 12px;">Razão Social</th>
                        <th style="font-size: 12px;">CNPJ</th>
                        <th style="font-size: 12px;">Mensagens e-CAC</th>
                        <th width="18%"></th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $cont = 1;
                    if ($consulta){ 
                      foreach ($consulta as $c){
                        $razao_aux = str_replace("'", "", $e->razao_social);
                        $razao_aux = str_replace("&", "", $razao_aux);

                        $e->razao_social = $razao_aux;
                        ?> 
                      <tr>
                            <td><strong><?php echo $cont; ?></strong></td>
                            <td style="text-align: left;"><?php echo $c->razao_social; ?></td>
                            <td ><?php echo $c->cnpj_completo; ?></td>
                            <td style="text-align: center;">
                              <?php if($c->nao_lidas != 0){
                                echo '<span class="badge badge-warning badge-shadow"><i class="ti-alert">&nbsp;</i>'.$c->nao_lidas.' não lidas</span>';
                              }else{
                                echo '<span class="badge badge-success badge-shadow"><i class="ti-alert">&nbsp;</i>Todas lidas</span>';
                              }
                              
                              ?>
                            </td>

                            <td style="font-size: 12px;">
                             
                             <a onclick="get_mensagens_ecac('<?php echo $c->id_caixa_postal ?>', '<?php echo $c->cnpj ?>');" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens do e-CAC" target="_blank" class="btn btn-default b" style="color: #1b6dc1; text-align: center;"><i class="material-icons" aria-hidden="true">email</i></a> 

                             <!-- <?php if($c->nao_lidas != 0){ ?>
                               <a href='<?php echo base_url("caixadeemail/marcar_msg_ecac_lida/$c->id_caixa_postal");?>' data-toggle='tooltip' data-placement='top' title='Marcar como Lido no sistema' class='btn btn-default b'><i class='fa fa-eye' aria-hidden='true'></i></a>
                             <?php } ?> -->
                            

                            </td>
                      </tr>
                  <?php $cont++; } 
                  }
                  ?>
                  </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- Modal das mensagens -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_mensagens">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagens e-CAC</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div style="width: 200px;margin-left: 15px;" id="div_filter">
                    <p>
                        Situação: 
                        <select id="table-filter" class="form-control form-control-sm">
                            <option value="">Todas Mensagens</option>
                            <option value="Não Lida">Mensagens Não Lidas</option>
                            <option value="Relevante Não">Importantes Não Lidas</option>
                        </select>
                    </p>
                </div>
                <table id="modal_table" class="table table-striped" style="width: 100% !important">    
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th></th>
                          <th width="8%"></th>
                          <th width="8%"></th>
                          <th></th>
                          <th>Remetente</th>
                          <th>Assunto da Mensagem</th>
                          <th>Enviada em</th>
                          <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>                
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Leitura da Mensagem -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ler_mensagem">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudo_modal">
            <div class="modal-header">
                <h5 class="modal-title" id="title_mensagem"></h5>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body_mensagem1">
                <pre id="body_mensagem">
                    
                </pre>
            </div>
            <div class="modal-footer">
                <button id="link_pdf_mensagem" onclick="imprimir();" type="button" class="btn btn-secondary" ><i class="ti-printer"></i>&nbsp;Imprimir</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <div class="printable"></div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    var url = window.location.href;
    url = url.substring(0,url.indexOf('?'));
    window.location.replace(url+"?filtro="+filtro+"");
  });
});
</script>

<script type="text/javascript">

    function get_mensagens_ecac($id_caixa_postal, $cnpj){
        var url = "<?= base_url() ?>/Ecac/listar_mensagens_by_empresa/"+$id_caixa_postal+"/"+$cnpj;
        var request = $.get(url, function(resultado){             
            response = JSON.parse(resultado);
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                $('#modal_mensagens').modal('show');
                $(document).ready( function () {
                    var array = json2array(response['table'], response['possui_certificado']);
                    
                    $.fn.dataTable.moment( 'DD/MM/YYYY' ); 
                    $('#modal_table').dataTable().fnDestroy();
                    var table = $('#modal_table').DataTable({
                        "columnDefs": [
                            {
                                "targets": [ 0 ],
                                "visible": false,
                                "searchable": false
                            }
                        ],
                        drawCallback: function() {
                            $('[data-toggle="popover"]').popover({
                                boundary:'window',
                                html: true
                              });
                          }, 
                        lengthMenu: [10, 20, 50, 100],
                        "order": [[ 6, "desc" ]],
                        language: {
                            "sEmptyTable": "Nenhum registro encontrado",
                            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sInfoThousands": ".",
                            "sLengthMenu": "_MENU_ resultados por página",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sZeroRecords": "Nenhum registro encontrado",
                            "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sNext": "Próximo",
                                "sPrevious": "Anterior",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                            },
                            "oAria": {
                                "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                            },
                            "select": {
                                "rows": {
                                    "_": "Selecionado %d linhas",
                                    "0": "Nenhuma linha selecionada",
                                    "1": "Selecionado 1 linha"
                                }
                            }
                        },
                        data: array
                    });
                    // $("#div_filter select").val("Não Lida");
                    // table.search('Não Lida').draw();
                    $('[data-toggle="popover"]').popover({
                        boundary:'window',
                        html: true
                      })

                    var filtro_e = <?php echo '"'.$filtro.'"'; ?>;
                    if(filtro_e == "NAO_LIDAS"){
                      $('#table-filter').val("Não Lida");
                      table.search("Não Lida").draw();
                    }else if(filtro_e == "IMPORTANTES"){
                      $('#table-filter').val("Relevante Não");
                      table.search("Relevante Não").draw();
                    }
                    
                    $('#table-filter').on('change', function(){
                        table.search(this.value).draw();   
                    });
                });    
            }        
        });
    }

    function json2array(data, possui_certificado){
        var string = "";
        var array = [];
        var arrayMultiple = [];

        for(var i in data){
          array = [];

          array.push('<td style="display:none;">'+data[i].id+'</td>');
          if(data[i].importante == 1){
            array.push('<td style="text-align: left;"><img title="Mensagem Relevante" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/exclamation.gif'); ?>" style="border-width:0px;"></td>');
          }else{
            array.push('<td style="text-align: left;"></td>');
          }

          if(data[i].lida_por == undefined){
            data[i].lida_por = "";
          }
          
          if(data[i].lida == 1){
            array.push('<td style="text-align: left;"><img title="Mensagem lida" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/aaMsgLida.gif'); ?>" style="border-width:0px;"></td>');
            array.push('<td><span class="badge badge-success badge-shadow" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Lida" data-content="'+data[i].lida_por+'" ><i class="ti-alert">&nbsp;</i>Lida</span> </td>');
            
          }else{
            array.push('<td style="text-align: left;"><img title="Mensagem não lida" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/aaMsgNaoLida.gif'); ?>" style="border-width:0px;"></td>');
            array.push('<td><span class="badge badge-warning badge-shadow" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Não Lida" data-content="'+data[i].lida_por+'" ><i class="ti-alert">&nbsp;</i>Não lida</span> </td>');

          }
          
          
          array.push('<td style="text-align: left;">'+data[i].remetente+'</td>');
          array.push('<td style="text-align: left;">'+data[i].assunto+'</td>');

          data[i].data = convertDate2(data[i].recebida_em);
          array.push('<td style="text-align: left;">'+data[i].data+'</td>');

          if(data[i].lida == 1){
            array.push('<td width="5%"><center><a title="Ler Mensagem" class="badge badge-info badge-shadow" rel="tooltip" href="javascript:ler_mensagem('+data[i].id+', &quot;'+data[i].assunto+'&quot;)"><i class="icon-zoom-in "></i> Ler</a></center></td>');
          }else{
            if(possui_certificado){
              array.push('<td width="5%"><center><a title="Ler Mensagem" class="badge badge-info badge-shadow" rel="tooltip" href="javascript:ler_mensagem_ecac('+data[i].caixa_postal_id+', '+data[i].id+', &quot;'+data[i].assunto+'&quot;)"><i class="icon-zoom-in "></i> Ler</a></center></td>');
            }else{
              array.push('<td width="5%"><center><a title="Ler Mensagem" class="badge badge-info badge-shadow" rel="tooltip" href="javascript:ler_mensagem_ecac_proc('+data[i].caixa_postal_id+', '+data[i].id+', &quot;'+data[i].assunto+'&quot;)"><i class="icon-zoom-in "></i> Ler</a></center></td>');
            }
            
          }
          
          arrayMultiple.push(array);
        }
        
        return arrayMultiple;
    }

    function ler_mensagem($id, $assunto){        
        //$('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/Ecac/ler_mensagem/"+$id;
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem').html($assunto);
                $('#body_mensagem').html(response['body']);
                $('#modal_ler_mensagem').modal('show');
            } 
        });
    }

    function ler_mensagem_ecac(id, id_mensagem,assunto){        
        //$('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/Ler_mensagem_ecac/ler_mensagem/"+id+"/"+id_mensagem+"/"+banco;
        var request = $.get(url, function(data, status){

            var response;
            if(data.includes("PFX")){
                var string_data = data.substring(data.indexOf("{"));
                response = JSON.parse(string_data); 
            }else{
              response = JSON.parse(data);  
            }
            
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "ERRO: Devido instabilidade no portal do e-CAC, tente novamente em alguns instantes",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem').html(assunto);
                $('#body_mensagem').html(response['mensagem']);
                $('#modal_ler_mensagem').modal('show');

                marcarLidaPor(id,id_mensagem);
            } 
        });
    }

    function ler_mensagem_ecac_proc(id, id_mensagem,assunto){        
        //$('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/Ler_mensagem_ecac/ler_mensagem_proc/"+id+"/"+id_mensagem+"/"+banco;
        var request = $.get(url, function(data, status){

            var response;
            if(data.includes("PFX")){
                var string_data = data.substring(data.indexOf("{"));
                response = JSON.parse(string_data); 
            }else{
              response = JSON.parse(data);  
            }
            
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "ERRO: Devido instabilidade no portal do e-CAC, tente novamente em alguns instantes",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem').html(assunto);
                $('#body_mensagem').html(response['mensagem']);
                $('#modal_ler_mensagem').modal('show');

                marcarLidaPor(id,id_mensagem);
            } 
        });
    }

    function imprimir(){
        $(".printable").html($("#conteudo_modal").html());
        $(".printable").printThis();
    }

    function convertDate2(a){
      var day = a.split('-')[2];
      day = day.split(' ')[0];
      var month = a.split('-')[1];
      var year = a.split('-')[0];

      var date = day + "/" + month + "/" + year;
      return date;
    }

    function  marcarLidaPor(id,id_mensagem){
        $.post("<?php echo base_url();?>mensagens/marcar_lida_por",
          {
              id:id,
              id_mensagem: id_mensagem
          },
          function(data){
            if(data == 1){
            }
          });
    }
</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
var table = $('#example').DataTable({
        "iDisplayLength": 50,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );


});
</script>



