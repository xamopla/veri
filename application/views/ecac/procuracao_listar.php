<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Procurações</h2> 
          </div>
          <div class="card-body">
          <div class="form-group col-lg-3 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'listar'=>'Todas',
                  'vencidas'=>'Vencidas',
                  'proximas_vencer'=>'Próximas de Vencer',
                  'regulares'=>'Regulares'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br><br>
            <div class="table-responsive">
              <table class="table table-striped" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr>
                        <th></th>
                        <th style="text-align: center;">Razão Social</th>
                        <th style="text-align: center;">CNPJ</th>
                        <th style="text-align: center;">Data Inicio</th>
                        <th style="text-align: center;">Data Fim</th> 
                        <th style="text-align: center;">Situação</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php 
                  if ($consulta){ 
                    
                      foreach ($consulta as $c){
                  ?>
                      <tr>
                        <td><?php echo $c->data_or; ?></td>
                        <td><?php echo $c->nome_outorgante; ?></td>
                        <td><?php echo $c->cnpj_outorgante; ?></td>
                        <td><?php echo $c->data_inicio; ?></td>
                        <td><?php echo $c->data_fim; ?></td>

                        <td><?php 

                            $data_banco = explode("/", $c->data_fim);
                            $data_final_format = $data_banco[2]."-".$data_banco[1]."-".$data_banco[0];

                            $data_final = strtotime($data_final_format);
                            $data_atual = strtotime(date(''). ' + 30 days');

                            $df = new DateTime($data_final_format);
                            $da = new DateTime(date(''));
                            $intervalo = $df->diff($da);

                            if ($data_final < strtotime(date('Y-m-d'))) {
                              echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Vencido' data-content='Vencido à ".$intervalo->format('%a')." dia(s)'>Vencido</span>";
                            } else if ($intervalo->format('%a') <= 30) {
                              echo "<span class='badge badge-warning badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Próximo de Vencer' data-content='Faltam ".$intervalo->format('%a')." dia(s) para vencer'>Próximo a Vencer</span>";
                            } else{
                              echo "<span class='badge badge-success badge-shadow'  data-container='body' style='background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' title='Dentro do prazo' data-content='Faltam ".$intervalo->format('%a')." dia(s) para vencer'>Regular</span>";
                            }

                            ?>                        
                        </td>

                        
                      </tr>
                  <?php } 
                  }
                  ?>
                </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>
 
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->
<script src="<?php echo base_url('assets/js/popover-custom.js'); ?>"></script>
<style type="text/css">
    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<script>
$(document).ready(function() {
   $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  })

   $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('procuracao'); ?>/"+filtro+"");
  });

$('#example').DataTable({
        "order": [[ 0, "asc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});
</script>