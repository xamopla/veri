<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<style>
    .datepicker{z-index:1151 !important;}
</style>
<style type="text/css">

    .swal2-styled{
        background-color: #dc2727 !important;
    }

    .all {
        display: flex;
        perspective: 10px;
        transform: perspective(300px) rotateX(20deg);
        will-change: perspective;
        perspective-origin: center center;
        transition: all 0.5s ease-out;
        justify-content: center;
        transform-style: preserve-3d;
    }
    .all:hover {
        perspective: 1000px;
        transition: all 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);
    .text {
        opacity: 1;
    }
    & > div {
          opacity: 1;
          transition-delay: 0s;
      }
    .explainer {
        opacity: 0;
    }
    }

    .all:hover .lefter{
        opacity: 1;
        margin-right: 10px;
        perspective: 2000px;
        transition: left 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);
    .text {
        opacity: 1;
    }
    }

    .all:hover .left{
        opacity: 1;
        margin-right: 10px;
        perspective: 2000px;
        transition: left 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);
    .text {
        opacity: 1;
    }
    }

    .all:hover .right{
        opacity: 1;
        margin-left: 10px;
        perspective: 2000px;
        transition: right 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);
    .text {
        opacity: 1;
    }
    }

    .left, .center, .right, .lefter, .righter {
        width: 25px;
        height: 25px;
        transform-style: preserve-3d;
        border-radius: 10px;
        border: 1px solid #fff;
        box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
        opacity: 0;
        transition: all .3s ease;
        transition-delay: 1s;
        position: relative;
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
        background-color: white;
        cursor: pointer;
        /*background-blend-mode: color-burn;*/


    }

    .center2 {
        width: 25px;
        height: 25px;
        transform-style: preserve-3d;
        /*border-radius: 8px;*/
        border: 1px solid #fff;
        box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
        opacity: 0;
        transition: all .3s ease;
        transition-delay: 1s;
        position: relative;
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
        background-color: white;
        cursor: pointer;
        /*background-blend-mode: color-burn;*/


    }

    .text {
        transform: translateY(30px);
        opacity: 0;
        transition: all .3s ease;
        bottom: 0;
        left: 5px;
        position: absolute;
        will-change: transform;
        color: #fff;
        text-shadow: 0 0 5px rgba(100, 100, 255, .6)
    }
    .lefter {
        transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
        background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
    }
    .left {
        transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
        background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
    }
    .center2 {
        opacity: 1;
        background-image: url(<?php echo base_url('assets/css/fab/images/barcode.png'); ?>);
    }
    .center {
        opacity: 1;
        background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
    }
    .right {
        transform: translateX(30px) translateZ(-25px) rotateY(5deg);
        background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
    }
    .righter {
        transform: translateX(60px) translateZ(-50px) rotateY(10deg);
        background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
    }
    .explainer {
        font-weight: 300;
        font-size: 2rem;
        color: #fff;
        transition: all .6s ease;
        width: 100%;
        height: 100%;
        background-color: transparent;
        background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
        border-radius: 10px;
        text-shadow: 0 0 10px rgba(255, 255, 255, .8);

        display: flex;
        justify-content: center;
        align-items: center;
    }


    .ref {
        background-color: #000;
        background-image: linear-gradient(to bottom, #d80, #c00);
        border-radius: 3px;
        padding: 7px 10px;
        position: absolute;
        font-size: 16px;
        bottom: 10px;
        right: 10px;
        color: #fff;
        text-decoration: none;
        text-shadow: 0 0 3px rgba(0, 0, 0, .4);
    &::first-letter {
         font-size: 12px;
     }
    }


    .modal-dialog {
        width: 1000px;
        max-width : 1000px;
    }
</style>

<style type="text/css">
    .title-menu {
        cursor: pointer;
        color: #0A4C62 !important;
        background-color: #def0c1 !important;
        border-color: #def0c1 !important;
    }
    .div-menu {
        padding:0px;
    }
    .item-menu {
        cursor: pointer;
        padding-left: 15px;
    }
    .item-menu:hover {
        cursor: pointer;
        /*padding-left: 15px;*/
        background-color: #f8f8f8;
        font-weight: bold;
    }
    .item-menu.active {
        background-color: #eeeeee !important;
        font-weight: bolder;
    }
    .table-menu {
        margin-bottom: 0px;
    }
    .glyphicon-menu {
        margin-right:10px;
    }
    .text-menu {
        color: #0A4C62;
    }
    .text-menu:hover, .text-menu:active, .text-menu:visited, .text-menu:focus {
        text-decoration: none;
    }
    .text-title-menu:hover, .text-title-menu:active, .text-title-menu:visited, .text-title-menu:focus {
        text-decoration: none;
    }


    .table-contribuinte {
        width: 100%;
        margin-bottom: 6px;
    }

    .mensagem-inicial {
        margin-top: 20px;
        text-align: center;
    }

    .mensagem-inicial p {
        color: #0A4C62;
        font-size: 12px;
    }

    .form-ano-calendario {
        margin-top: 20px;
    }

    .consulta-emissao, .consulta-extrato {
        text-align: center;
    }

    /* Tabela */

    table.trabalhar td {
        font-size: 11px;
    }


    table.consulta {
        margin: 0 auto;
        margin-top: 10px;
    }

    table.consulta th.pa {
        vertical-align:middle !important;
        text-align: left;
        padding: 4px !important;
        border: 2px solid white;
        color: #0A4C62;
    }

    table.consulta th.header {
        vertical-align:middle !important;
        text-align: center;
        padding: 4px !important;
        border: 1px solid white;
        color: #0A4C62;
    }

    table.consulta td.header
    {
        color: #555555;
        padding: 1px;
        font-weight:bold;
        font-size: 10px !important;
        text-align:center;
    }

    table.emissao th.detalhe
    {
        padding: 0px !important;
        width: 1%; /* !important;*/
        border: 1px solid #99BBE8 !important;
        background-color: #D6E3F2;
        cursor: pointer;
    }

    table.emissao th.check {
        width: 2%;
    }

    table.emissao th.periodo {
        width: 20%;
    }

    table.emissao th.apurado {
        width: 8%;
    }

    table.emissao th.situacao, table.emissao th.beneficio {
        width: 10%;
    }

    table.emissao th.resumo {
        width: 45%;
    }

    table.emissao th.principal, table.emissao th.multa, table.emissao th.juros, table.emissao th.total  {
        width: 9%;
        white-space: nowrap;
    }

    table.emissao td {
        padding: 2px !important;
        padding-left: 6px !important;
        vertical-align: middle !important;
    }

    table.emissao td.pa {
        font-weight: bold;
        color:#0A4C62;
    }

    table.emissao td.selecionar, table.emissao td.beneficio {
        text-align: center;
    }

    table.emissao td.checkall {
        color:#0A4C62;
        /*padding-top: 4px;*/
        border: 0px !important;
    }

    table.emissao td.simnao a
    {
        font-size: 12px;
        text-decoration: underline;
        color: #0A4C62;
    }

    table.emissao td.situacaoPa
    {
        font-weight: bold;
        color:#0A4C62;
    }

    table.emissao td.pago, table.emissao td.pago a
    {
        color: #1caa12;
    }

    table.emissao td.devedor
    {
        color: #FF0000;
    }

    /*  Detalhes do DAS pago */

    div.modal-body p span:first-child
    {
        font-weight: bold;
        color: #0A4C62;
    }

</style>
<script type="text/javascript">
    <?php
    switch ($m) {
        case "01":    $mes = 'Janeiro';     break;
        case "02":    $mes = 'Fevereiro';   break;
        case "03":    $mes = 'Marco';       break;
        case "04":    $mes = 'Abril';       break;
        case "05":    $mes = 'Maio';        break;
        case "06":    $mes = 'Junho';       break;
        case "07":    $mes = 'Julho';       break;
        case "08":    $mes = 'Agosto';      break;
        case "09":    $mes = 'Setembro';    break;
        case "10":    $mes = 'Outubro';     break;
        case "11":    $mes = 'Novembro';    break;
        case "12":    $mes = 'Dezembro';    break;
    }

    ?>
</script>

<script type="text/javascript">
    var global_url = '';

    function preencher_url(url){
        global_url = url;
    }
</script>

<div class="page-body">
    <div class="container-fluid">
        <br>
    </div>

    <section id="extended">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title-wrap bar-success">
                            <!-- <a class="btn mr-1 shadow-z-2 btn-round btn-info pull-right" data-toggle="modal" data-target="#modal_filtro" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Filtro"><span class="fa fa-filter"></span></a> -->

                            <h4 class="card-title">DAS</h4>
                            <h6 class="card-title" style="color: red">Atenção ! A data de vencimento é uma estimativa calculada pelo sistema com base na regra do dia 20 do 1º mês subsequente. Essa data pode não ser a exata que deve ser pago o DAS.</h6>
                        </div>

                        <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_1">

                            <br>
                            <div class="card-block text-center col-">
                                <div class="form-body">
                                    <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("das/index/anterior/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                                        <div class="col-sm-12 col-md-6 col-lg-4">
                                            <div class="block-button">
                                                <a class="btn btn-outline-success btn-lg btn-block"><?php if($mes == "Marco"){
                                                        echo "Março";
                                                    }else{
                                                        echo $mes;
                                                    }?> </a>
                                                <?php echo $ano ?>
                                            </div>
                                        </div>

                                        <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("das/index/proximo/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- OPÇÃO 02 - -->
                        <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_2" style="display: none">
                            <br>
                            <?php echo form_open("das/index", array('role'=>'form')); ?>
                            <div class="card-block text-center col-">
                                <div class="form-body">
                                    <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:diminuiAno()">                             
                                </a> 
                              </span>
                                        <div class="col-sm-12 col-md-6 col-lg-4">
                                            <div class="block-button">
                                                <input type="text" name="ano_selecionado" id="ano_selecionado" value="<?php echo $ano ?>" class="btn btn-outline-success btn-lg btn-block" placeholder="<?php echo $ano ?>" />
                                            </div>
                                        </div>

                                        <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:aumentaAno()">                             
                                </a> 
                              </span>
                                    </div>
                                </div>
                            </div>

                            <div class="card-block text-center col-" style="margin-top: 5px;">
                                <div class="form-body">
                                    <div class="row justify-content-center">
                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Dezembro')" name="mes_selecionado" id="mes_selecionado" value="01">JAN.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Janeiro')" name="mes_selecionado" id="mes_selecionado" value="02">FEV.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Fevereiro')" name="mes_selecionado" id="mes_selecionado" value="03">MAR.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Março')" name="mes_selecionado" id="mes_selecionado" value="04">ABR.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Abril')" name="mes_selecionado" id="mes_selecionado" value="05">MAI.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Maio')" name="mes_selecionado" id="mes_selecionado" value="06">JUN.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Junho')" name="mes_selecionado" id="mes_selecionado" value="07">JUL.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Julho')" name="mes_selecionado" id="mes_selecionado" value="08">AGO.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Agosto')" name="mes_selecionado" id="mes_selecionado" value="09">SET.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Setembro')" name="mes_selecionado" id="mes_selecionado" value="10">OUT.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Outubro')" name="mes_selecionado" id="mes_selecionado" value="11">NOV.</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <div class="block-button">
                                                <button class="btn btn-outline-success" type="button" onclick="envia_form('Novembro')" name="mes_selecionado" id="mes_selecionado" value="12">DEZ.</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- OPÇÃO 02 - -->

                    </div>

                    <div class="card-body">
                        <div class="form-group col-lg-4 col-xs-12 pull-right">
                            <label>Filtro:</label>
                            <?php
                            $valores_filtro = array(
                                'TODAS'=>'Todas',
                                'REGULAR'=>'Pagos',
                                'PENDENTE'=>'Não Pagos',
                                'DEBITOS'=>'Débitos',
                                'DUPLICIDADE'=>'Em Duplicidade',
                            );
                            echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro);
                            ?>
                        </div><br> <br>
                        <div class="card-block">
                            <table class="table table-bordered table-responsive-md text-center" id="example" data-toggle="datatables">
                                <thead>
                                <tr style="background-color:#DEF0C1">
                                    <th class="pa" colspan="12">PA <?php echo $m.'/'.$ano; ?></th>
                                </tr>
                                <tr>
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Nº DAS</th>
                                    <th>Data/hora Emissão</th>
                                    <th>Extrato</th>
                                    <th>Pago <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone Federal localizado na parte superior da tela sempre que virar o mês e não for constado o pagamento do DAS referente ao mês anterior"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                                    <th width="1px">Gerar/Recalcular DAS</th>
                                    <th>DAS gerado em duplicidade<a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone Federal localizado na parte superior da tela sempre que for constado um DAS gerado em duplicidade no mês"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                                    <th>Data Vencimento Estimada</th>
                                    <th>Débitos <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone Federal localizado na parte superior da tela sempre que for constado um novo débito de DAS"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/> </th>
                                    <!-- <th>Ações</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($consulta){
                                    foreach ($consulta as $e){
                                        $razao_aux = str_replace("'", "", $e->razao_social);
                                        $razao_aux = str_replace("&", "", $razao_aux);

                                        $e->razao_social = $razao_aux;
                                        ?>
                                        <tr>

                                            <td><?php echo $e->razao_social; ?></td>
                                            <td><?php echo $e->cnpj_completo; ?></td>

                                            <td><?php echo $e->numero_das; ?></td>
                                            <td><?php echo $e->data_hora_emissao; ?></td>
                                            <td>
                                                <?php if(!empty($e->caminho_download_extrato)){ ?>
                                                    <div class="all">
                                                        <div class="lefter">
                                                            <a href="mailto:?subject=Notificação Sistema VERI&body=O extrato do DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> está disponível no link <?php echo $e->caminho_download_extrato; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                                                <div class="explainer"></div>
                                                                <div class="text"></div>
                                                            </a>
                                                        </div>
                                                        <div class="left">
                                                            <a href="https://api.whatsapp.com/send?text=O extrato do DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> está disponível no link <?php echo $e->caminho_download_extrato; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                                                <div class="explainer"></div>
                                                                <div class="text">Whatsapp</div>
                                                            </a>
                                                        </div>
                                                        <div class="center">
                                                            <a href="<?php echo $e->caminho_download_extrato; ?>" download  data-toggle="tooltip" data-placement="top" title="Download" class="">
                                                                <div class="explainer"></div>
                                                                <div class="text"></div>
                                                            </a>
                                                        </div>
                                                        <div class="right">
                                                            <a href="<?php echo $e->caminho_download_extrato; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                                                <div class="explainer"></div>
                                                                <div class="text">Visualizar</div>
                                                            </a>
                                                        </div>
                                                        <!-- <div class="righter">
                                                          <div class="text">SEO</div>
                                                        </div> -->
                                                    </div>
                                                <?php } ?>
                                            </td>



                                            <td>
                                                <?php if(!empty($e->numero_das)){ ?>
                                                    <?php
                                                    if (!empty($e->pago) && $e->pago == 'Sim')
                                                        echo '<span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Sim">Sim</span>';
                                                    else if($e->possui_das_pago_retificado)
                                                        echo "<span class='badge badge-warning' data-toggle='popover' data-trigger='hover' data-placement='top' title='Empresa possui boleto pago, mas consta retificação' data-content='Clique para ver detalhes' onclick=\"openModalDasRetificadoPago('{$e->numero_declaracao}', {$m}, {$ano})\">Não</i></span>";
                                                    else
                                                        echo '<span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Não">Não</span>';
                                                    ?>
                                                <?php }else{ ?>
                                                    <?php echo '<span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Sim">Sem DAS</span>' ?>
                                                <?php } ?>


                                            </td>



                                            <td>
                                                <?php if(!empty($e->numero_das)){ ?>

                                                    <?php if (!empty($e->pago) && $e->pago == 'Sim'){ ?>

                                                        <div class="all">
                                                            <div class="lefter">
                                                                <a href="mailto:?subject=Notificação Sistema VERI&body=O DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> do período<?php echo $e->compentencia ?> foi pago" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                                                    <div class="explainer"></div>
                                                                    <div class="text"></div>
                                                                </a>
                                                            </div>
                                                            <div class="left">
                                                                <a href="https://api.whatsapp.com/send?text=O DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> do período <?php echo $e->compentencia ?> foi pago" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                                                    <div class="explainer"></div>
                                                                    <div class="text">Whatsapp</div>
                                                                </a>
                                                            </div>
                                                            <div class="center2">
                                                                <a href="javascript:void(0)" disabled style='cursor:not-allowed;' data-toggle="tooltip" data-placement="top" title="DAS já pago" class="">
                                                                    <div class="explainer"></div>
                                                                    <div class="text"></div>
                                                                </a>
                                                            </div>
                                                            <div class="right">
                                                                <a href="javascript:void(0)" disabled style='cursor:not-allowed;' data-toggle="tooltip" data-placement="top" title="DAS já pago" class="">
                                                                    <div class="explainer"></div>
                                                                    <div class="text">Visualizar</div>
                                                                </a>
                                                            </div>
                                                        </div>





                                                    <?php } else {

                                                        $hostCompleto = $_SERVER['HTTP_HOST'];
                                                        $server = explode('.', $hostCompleto);
                                                        $server = $server[0];

                                                        if($server == "demo"){
                                                            $caminho_download_das = base_url("assets/DAS.pdf");
                                                        }else{
                                                            $caminho_download_das = $e->caminho_download_das;
                                                        }



                                                        ?>

                                                        <?php if(!empty($caminho_download_das)){ ?>
                                                            <div class="all">
                                                                <div class="lefter">
                                                                    <a href="mailto:?subject=Notificação Sistema VERI&body=O DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> do período <?php echo $e->compentencia ?> está disponível no link <?php echo $caminho_download_das; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text"></div>
                                                                    </a>
                                                                </div>
                                                                <div class="left">
                                                                    <a href="https://api.whatsapp.com/send?text=O DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> do período <?php echo $e->compentencia ?> está disponível no link <?php echo $caminho_download_das; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text">Whatsapp</div>
                                                                    </a>
                                                                </div>
                                                                <div class="center2">
                                                                    <a href="javascript:void(0);" onclick="gerar_das_aux('<?php echo $e->numero_declaracao ?>','<?php echo $e->compentencia ?>','<?php echo $e->cnpj ?>')" data-trigger="hover" data-placement="top" title="Gerar DAS" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text"></div>
                                                                    </a>
                                                                </div>
                                                                <div class="right">
                                                                    <a href="<?php echo $caminho_download_das; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text">Visualizar</div>
                                                                    </a>
                                                                </div>
                                                            </div>

                                                        <?php }else{ ?>


                                                            <div class="all">
                                                                <div class="lefter">
                                                                    <a href="mailto:?subject=Notificação Sistema VERI&body=O DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> do período <?php echo $e->compentencia ?> não foi pago" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text"></div>
                                                                    </a>
                                                                </div>
                                                                <div class="left">
                                                                    <a href="https://api.whatsapp.com/send?text=O DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> do período <?php echo $e->compentencia ?> não foi pago" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text">Whatsapp</div>
                                                                    </a>
                                                                </div>
                                                                <div class="center2">
                                                                    <a href="javascript:void(0);" onclick="gerar_das('<?php echo $e->numero_declaracao ?>','<?php echo $e->compentencia ?>','<?php echo $e->cnpj ?>')" data-trigger="hover" data-placement="top" title="Gerar DAS" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text"></div>
                                                                    </a>
                                                                </div>

                                                                <div class="right">
                                                                    <a href="javascript:void(0)" disabled style='cursor:not-allowed;' data-toggle="tooltip" data-placement="top" title="DAS não gerado pelo sistema" class="">
                                                                        <div class="explainer"></div>
                                                                        <div class="text">Visualizar</div>
                                                                    </a>
                                                                </div>

                                                            </div>



                                                        <?php } ?>

                                                    <?php } ?>

                                                <?php }else{ ?>

                                                <?php } ?>

                                            </td>
                                            <td>
                                                <?php if ($e->possui_das_duplicidade){ ?>
                                                    <?php echo '<span class="badge badge-warning" data-toggle="popover" data-trigger="hover" data-placement="top" title="Empresa possui DAS gerados em duplicidade" data-content="Clique para ver detalhes" onclick="openModalDasDuplicidade(&quot;'.$e->cnpj.'&quot;, &quot;'.$e->razao_social.'&quot; , &quot;'.$e->compentencia.'&quot;)">Sim</i></span>' ?>
                                                <?php } else { ?>
                                                    <?php echo '<span class="badge badge-success" data-toggle="popover" data-trigger="hover" data-placement="top" title="Empresa não possui DAS gerados em duplicidade">Não</span>' ?>
                                                <?php } ?>
                                            </td>
                                            <?php if(!empty($e->numero_das)){ ?>

                                                <?php if(!empty($e->pago) && $e->pago == 'Não'){ ?>
                                                    <td>
                                                        <?php

                                                        $data_final = strtotime($ultimo_dia_transmitir);

                                                        $df = new DateTime($ultimo_dia_transmitir);
                                                        $da = new DateTime(date(''));
                                                        $intervalo = $df->diff($da);

                                                        if ($data_final < strtotime(date('Y-m-d'))) {
                                                            echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Vencido' data-content='Prazo para pagamento do DAS vencido à ".$intervalo->format('%a')." dia(s)'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span>";
                                                        } else if ($intervalo->format('%a') <= 5) {
                                                            echo "<span class='badge badge-warning badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Próximo de Vencer' data-content='Faltam ".$intervalo->format('%a')." dia(s) para vencer o prazo de pagamento do DAS'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span>";
                                                        } else{
                                                            echo "<span class='badge badge-success badge-shadow'  data-container='body' style='background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' title='DAS dentro do prazo' data-content='Faltam ".$intervalo->format('%a')." dia(s) para o pagamento do DAS'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span>";
                                                        }

                                                        ?>
                                                    </td>
                                                <?php }else{ ?>
                                                    <td>
                                                        <?php echo "<span class='badge badge-success badge-shadow'  data-container='body' style='background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' title='DAS pago' data-content='DAS pago'>".date('d/m/Y', strtotime($ultimo_dia_transmitir))."</span>"; ?>
                                                    </td>
                                                <?php } ?>

                                            <?php }else{ ?>
                                                <td></td>
                                            <?php } ?>

                                            <td>
                                                <?php if ($e->possui_debito == 1){ ?>
                                                    <?php echo '<span class="badge badge-danger" data-toggle="popover" data-trigger="hover" data-placement="top" title="Empresa possui débitos no DAS" data-content="Clique para ver detalhes" onclick="openModalDebitos(&quot;'.$e->cnpj.'&quot;, &quot;'.$e->razao_social.'&quot;)">Sim</i></span>' ?>
                                                <?php } else { ?>
                                                    <?php echo '<span class="badge badge-success" data-toggle="popover" data-trigger="hover" data-placement="top" title="Empresa não possui débitos no DAS">Não</span>' ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php }
                                }
                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Nº DAS</th>
                                    <th>Data/hora Emissão</th>
                                    <th>Extrato</th>
                                    <th>Pago</th>
                                    <th></th>
                                    <th></th>
                                    <th>Data Vencimento Estimada</th>
                                    <th>Débitos ? </th>
                                    <!-- <th>Ações</th> -->
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalDebitos" tabindex="-1" role="dialog" aria-labelledby="formModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="excel('area_impressao')">EXCEL</button>
                &nbsp;
                <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="imprimir('area_impressao')">PDF</button>

                <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="panel panel-success" id="area_impressao">
                    <div class="panel-heading title-menu">
                        <h4 class="panel-title">
                            <a>
                                <strong style="color: #0A4C62;"><span class="glyphicon glyphicon-menu glyphicon-usd" ></span>
                                    Relação Mensal de DAS Gerados <label id="razao_empresa_modal" style="font-size: 15px;"></label></strong>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse2018" >
                        <table class="table consulta" style="margin-top: 0px;">
                            <tbody id="body_modal">


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="ModalDasDuplicidade" tabindex="-1" role="dialog" aria-labelledby="formModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="excel()">EXCEL</button>
                &nbsp;
                <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="imprimir('area_impressao_das_duplicidade')">PDF</button>

                <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="panel panel-success" id="area_impressao_das_duplicidade">
                    <div class="panel-heading title-menu">
                        <h4 class="panel-title">
                            <a>
                                <strong style="color: #0A4C62;"><span class="glyphicon glyphicon-menu glyphicon-usd" ></span>
                                    Relação de DAS Gerados em duplicidade <label id="razao_empresa_modal" style="font-size: 15px;"></label></strong>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse2018" >
                        <table class="table consulta" style="margin-top: 0px;">
                            <tbody id="body_modal_duplicidade">


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="modalDasPagoRetificado" tabindex="-1" role="dialog" aria-labelledby="formModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" >
            <div class="modal-body">
                <div class="panel panel-success" id="area_impressao">
                    <div id="collapse2018" >
                        <table class="table consulta" style="margin-top: 0px;">
                            <tbody id="body_modalDasPagoRetificado">

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>

<script>
    $(document).ready(function() {
        $('#filtro').change(function(){
            var filtro = $('#filtro option:selected').val();
            var url = window.location.href;
            url = url.substring(0,url.indexOf('?'));
            window.location.replace(url+"?filtro="+filtro+"");
        });
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function($) {

// BOTÃO DE BUSCAR ANOS E MESES
        $('#mostrar_meses_opcao_1').click(function(event) {
            $('#mostrar_meses_opcao_2').show();
            $('#mostrar_meses_opcao_1').hide();
        });


    });

</script>

<script>
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover({
            boundary:'window',
            html: true
        })
        var table = $('#example').DataTable({
            "iDisplayLength": 50,
            lengthChange: false,
            //buttons: ['excel', 'pdf', 'colvis' ],
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'EXCEL',
                    exportOptions: {
                        modifier: {
                            page: 'all'
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
                {
                    extend: 'colvis',
                    text: 'COLUNAS'
                }
            ],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "bDestroy": true
        });

        table.search('<?php echo $razao_social_filtro; ?>').draw();
        table.buttons().container()
            .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    });

</script>

<script type="text/javascript">
    function diminuiAno(){
        var ano = $("#ano_selecionado").val();
        ano = parseInt(ano);
        $("#ano_selecionado").val(ano - 1);
    }
    function aumentaAno(){
        var ano = $("#ano_selecionado").val();
        ano = parseInt(ano);
        $("#ano_selecionado").val(ano + 1);
    }

    function envia_form(mes){
        var mes_selecionado = $("#mes_selecionado").val();
        var ano_selecionado = $("#ano_selecionado").val();

        if(mes == 'Dezembro'){
            ano_selecionado = ano_selecionado - 1;
        }


        window.location = '<?php echo base_url("das/index/proximo"); ?>'+"/"+mes+"/"+ano_selecionado;
    }

    function update_simples(id){
        swal({
            title: 'Deseja Marcar essa empresa como Simples Nacional ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Sim!',
            cancelButtonText: 'Não, cancelar!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then(function () {

            $.post("<?php echo base_url();?>das/update_simples",
                {
                    id:id
                },
                function(data){
                    if(data == 1){
                        swal(
                            'Concluído!',
                            'A empresa foi marcada como Simples Nacional',
                            'success'
                        ).done(); window.setTimeout(function(){

                            // Move to a new location or you can do something else
                            window.location = global_url;

                        }, 2000);
                    }
                });


        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelado',
                    'Processo cancelado!',
                    'error'
                ).done();
            }
        }).done();


    }


    function openModalDebitos(cnpj, razao_empresa_modal){
        $("#body_modal").html('');
        $("#razao_empresa_modal").text(" - "+razao_empresa_modal);
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('das/find_debitos_for_modal'); ?>',
            async: true,
            data: {
                'cnpj': cnpj
            },
            success: function(result){
                var resultado = JSON.parse(result);
                if(resultado != ''){
                    $("#body_modal").append('<tr style="background-color:#F0F0F0"><th rowspan="2" class="header">Mês de Apuração</th><th rowspan="2" class="header">Data de Vencimento</th><th rowspan="2" class="header">Valor</th></th></tr>');

                    for(var i in resultado){
                        $("#body_modal").append('<tr>');
                        $("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].periodo_apuracao+'</td>');
                        $("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].data_vencimento+'</td>');
                        $("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].debito_declarado+'</td>');
                        //$("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].principal+'</td>');
                        // $("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].multa+'</td>');
                        //$("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].juros+'</td>');
                        //$("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].total+'</td>');
                        //$("#body_modal").append('<td style="padding: 2px" class="text-center">'+resultado[i].exigibilidade_suspensa+'</td>');
                        //$("#body_modal").append('</tr>');
                    }
                }
            },
        });

        $('#modalDebitos').modal();

    }

    function openModalDasDuplicidade(cnpj, razao_empresa_modal, compentencia){
        $("#body_modal_duplicidade").html('');
        $("#razao_empresa_modal").text(" - "+razao_empresa_modal);
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('das/find_das_duplicidade_modal'); ?>',
            async: true,
            data: {
                'cnpj': cnpj,
                'compentencia': compentencia
            },
            success: function(result){
                var resultado = JSON.parse(result);
                console.log(resultado);
                if(resultado != ''){
                    $("#body_modal_duplicidade").append('<tr><tr style="background-color:#DEF0C1"> <th class="pa" colspan="12">'+ compentencia +'</th>     </tr><th>Razão Social</th><th>CNPJ</th><th>Nº DAS</th><th>Data/hora Emissão</th><th>Pago</th><th width="1px"></th></tr>');
                    for(i=0; i < resultado.length; i++){
                        $("#body_modal_duplicidade").append('<tr>');
                        $("#body_modal_duplicidade").append('<td>'+ resultado[i].razao_social+'</td>');
                        $("#body_modal_duplicidade").append('<td>'+ resultado[i].cnpj_completo+'</td>');
                        $("#body_modal_duplicidade").append('<td>'+ resultado[i].numero_das+'</td>');
                        $("#body_modal_duplicidade").append('<td>'+ resultado[i].data_hora_emissao+'</td>');
                        if( resultado[i].pago == 'Sim' )
                            $("#body_modal_duplicidade").append('<td><span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Sim">Sim</span></td>');
                        else
                            $("#body_modal_duplicidade").append('<td><span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Não">Não</span></td>');

                        $("#body_modal_duplicidade").append('</tr>');
                    }

                }
            },
        });

        $('#ModalDasDuplicidade').modal();

    }


    function openModalDasRetificadoPago(numero_declaracao, m, ano){
        $("#body_modalDasPagoRetificado").html('');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('das/find_das_pago_retificado_for_modal'); ?>',
            async: true,
            data: {
                'numero_declaracao': numero_declaracao
            },
            success: function(result){
                var resultado = JSON.parse(result);
                if(resultado != ''){

                    $("#body_modalDasPagoRetificado").append('<tr><tr style="background-color:#DEF0C1"> <th class="pa" colspan="12">PA '+ m +'/' + ano +'</th>     </tr><th>Razão Social</th><th>CNPJ</th><th>Nº DAS</th><th>Data/hora Emissão</th><th>Extrato</th><th>Pago</th><th width="1px"></th></tr>');

                    $("#body_modalDasPagoRetificado").append('<tr>');
                    $("#body_modalDasPagoRetificado").append('<td>'+ resultado.razao_social+'</td>');
                    $("#body_modalDasPagoRetificado").append('<td>'+ resultado.cnpj_completo+'</td>');
                    $("#body_modalDasPagoRetificado").append('<td>'+ resultado.numero_das+'</td>');
                    $("#body_modalDasPagoRetificado").append('<td>'+ resultado.data_hora_emissao+'</td>');
                    var  extrato = `<div class="all">
                                          <div class="lefter">
                                            <a href="mailto:?subject=Notificação Sistema VERI&body=O extrato do DAS nº ${resultado.numero_das} da empresa ${resultado.razao_social} está disponível no link ${resultado.caminho_download_extrato}" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>
                                          <div class="left">
                                             <a href="https://api.whatsapp.com/send?text=O extrato do DAS nº ${resultado.numero_das} da empresa ${resultado.razao_social} está disponível no link ${resultado.numero_das}" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                              <div class="explainer"></div>
                                              <div class="text">Whatsapp</div>
                                            </a>
                                          </div>
                                          <div class="center">
                                            <a href="${resultado.caminho_download_extrato}" download  data-toggle="tooltip" data-placement="top" title="Download" class="">
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>
                                          <div class="right">
                                            <a href="${resultado.caminho_download_extrato}" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                              <div class="explainer"></div>
                                              <div class="text">Visualizar</div>
                                            </a>
                                          </div>
                                          <!-- <div class="righter">
                                            <div class="text">SEO</div>
                                          </div> -->
                                        </div>`
                    $("#body_modalDasPagoRetificado").append('<td>'+ extrato +'</td>');
                    $("#body_modalDasPagoRetificado").append('<td><span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Sim">Sim</span></td>');
                    $("#body_modalDasPagoRetificado").append('</tr>');
                }
            },
        });

        $('#modalDasPagoRetificado').modal();
    }

    //função para gerar DAS que ainda não foi gerado pelo sistema
    function gerar_das(numero_declaracao, competencia, cnpj){
        swal({
            title: 'Gerar DAS ?',
            text: 'Deseja gerar o DAS dessa empresa ? Se desejar pode digitar outra data a ser consolidada no campo abaixo, datas inválidas irão gerar o DAS com vencimento padrão.',
            type: 'warning',
            html: 'Deseja gerar o DAS dessa empresa ? Se desejar pode digitar outra data a ser consolidada no campo abaixo. Datas inválidas irão gerar o DAS com vencimento padrão. <input id="datepicker" readonly class="swal2-input" data-language="en" placeholder="Data de consolidação">',
            onOpen:function(){
                $('#datepicker').datepicker({
                    dateFormat: 'dd/mm/yyyy'
                });
            },
            inputPlaceholder: 'Data de consolidação',
            inputAttributes: {
                maxlength: 10,
                autocapitalize: 'off',
                autocorrect: 'off',
                id: 'datepicker'
            },
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Sim!',
            cancelButtonText: 'Não, cancelar!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then(function (input) {

            gerar(numero_declaracao, competencia, cnpj, $("#datepicker").val());


        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelado',
                    'Geração do DAS cancelada!',
                    'error'
                ).done();
            }
        }).done();
    }

    //função para gerar DAS que já foi gerado pelo sistema
    function gerar_das_aux(numero_declaracao, competencia, cnpj){
        swal({
            title: 'Gerar DAS ?',
            text: 'Já foi gerado um DAS anteriormente para essa empresa para o mesmo periodo! Deseja gerar um novo DAS ?',
            type: 'warning',
            html: 'Já foi gerado um DAS anteriormente para essa empresa para o mesmo periodo! Deseja gerar um novo DAS ? Se desejar pode digitar outra data a ser consolidada no campo abaixo. Datas inválidas irão gerar o DAS com vencimento padrão. <input id="datepicker" readonly class="swal2-input" data-language="en" placeholder="Data de consolidação">',
            onOpen:function(){
                $('#datepicker').datepicker({
                    dateFormat: 'dd/mm/yyyy'
                });
            },
            inputPlaceholder: 'Data de consolidação',
            inputAttributes: {
                maxlength: 10,
                autocapitalize: 'off',
                autocorrect: 'off',
                id: 'datepicker'
            },
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Sim!',
            cancelButtonText: 'Não, cancelar!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then(function (input) {

            gerar(numero_declaracao, competencia, cnpj, $("#datepicker").val());


        }, function (dismiss) {
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelado',
                    'Geração do DAS cancelada!',
                    'error'
                ).done();
            }
        }).done();
    }



    function gerar(numero_declaracao, competencia, cnpj, outra_data){
        swal({
            title: "Gerando...",
            text: 'Gerando DAS.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });

        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/das/gerar_das_function/";
        // url = "https://veri.com.br/crons-api/dctf_declaracao/buscar_declaracao/";
        var request = $.post(url, { cnpj: cnpj, competencia : competencia, numero_declaracao: numero_declaracao, banco: banco, outra_data: outra_data }, function(data, status){

            if(data.includes("ERRO")){

                $(document).ready(function() {
                    swal({
                        title: "Erro ao gerar DAS !",
                        text: "Erro",
                        type: "error"
                    });
                });

            }else{
                swal.close();
                // window.open(data, '_blank');
                setTimeout(reload_page, 5000);
            }
        });
    }

    function reload_page(){
        window.location = global_url;
    }

    function imprimir(id){
        $("#"+id).printThis();
    }

    function excel(id){
        window.open('data:application/vnd.ms-excel,' +  encodeURIComponent($('#'+id).html()));
    }
</script>