<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Cadastrar Tipo de Documento</h2><span>Preencha os campos abaixo:</span>
              </div>
              <div class="card-body">
                <?php echo form_open("tipodocumento/cadastrar", array('role'=>'form')); ?>
                    <div class="row">
                      <div class="col-md-6">
                          <div class="form-group mb-4">
                            <label>Nome: *</label>
                            <?php echo form_input(array('name'=>'nome', 'class'=>'form-control', 'style'=>'border-color: #5c6bc0;'), '', 'required'); ?>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group mb-4">
                              <label>Descrição:</label>
                              <?php echo form_input(array('name'=>'descricao', 'class'=>'form-control', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group mb-4">
                            <label>URL Consulta:</label>&nbsp;<a class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Endereço do site utilizado para consultar o documento."></a>
                            <?php echo form_input(array('name'=>'url_consulta', 'class'=>'form-control', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group mb-4">
                               <label>Orgão Expedidor:</label>
                               <?php echo form_input(array('name'=>'orgao_expedidor', 'class'=>'form-control', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group mb-4">
                               <label for="nome">Dias para Notificação*: <a class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Defina a quantidade de dias antes do vencimento que você deseja ser alertado ao acesar o sistema."></a></label>
                              <?php echo form_input(array('name'=>'diasNotificacao', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>
                  </div>   

                  <div class="ibox-footer text-right">
                  <?php echo form_submit(array('name'=>'btn_cadastrar', 'class'=>'btn btn-primary mr-2'), 'Cadastrar'); ?>
                  <a href="<?php echo base_url("tipodocumento/listar"); ?>" class="btn btn-outline-secondary">Voltar</a>
              </div>
                             
              </div>

              
              <?php echo form_close(); ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
 