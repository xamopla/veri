<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/
</style>

<style type="text/css">
  .card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details {
    position: absolute;
    top: 0;
    left: 0;
    width: 70%;/*100%*/
    height: 100%;
    transition: .5s;
    transform-origin: left;
    transform: perspective(2000px) rotateY(-90deg);
}
.card:hover .details {
    transform: perspective(2000px) rotateY(0deg);
}
.card .details .center {
    width: 100% !important;
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details .center p {
    margin: 3px 0;
    padding: 0;
    color: #262626;
}
.card .details .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details .center ul li a:hover {
    background: #ff3636;
}


.div_hover { 
  background: white;
  border-radius: 50px; 
}

.div_hover a:hover { 
  background-color: #24a0e9; 
  color: white !important;
}

.tag_a_hover {
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.span_hover:hover{
  background-color: #24a0e9; 
  color: white !important;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.media-body{
  padding-left: 0px !important; 
}

.teste {
    margin: 1px 0 !important;
    padding: 0;
    color: #262626;
}
</style>

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Mensagens</h2> 
        </div> 
      </div>
    </div>
  </div>

<div class="container-fluid general-widget">

  <div class="row">

    <?php if($id_colaborador != 19){ ?>
      <div class="col-sm-4 col-xl-4 col-lg-4 box-col-4">
        <a href="<?php echo base_url("dec/listar_dec_recebidas/TODAS"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">

              <div class="details" style="padding-top: 20px !important;">
                <div class="center" style="padding-top: 40px !important;">
                  <br><span style="font-size: 20px;color: #24a0e9; text-align: center;" class="span_hover">Mensagens DEC</span>
                  <!-- <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("dec/listar/LIDAS"); ?>" style="font-size: 18px;" class="tag_a_hover">Lidas: <?php if($msg_lidas_dec){echo $msg_lidas_dec;}else{ echo "0"; } ?></a>
                    </div>
                  </p> -->
                  <p>
                    <div class="div_hover">
                        <a href="<?php echo base_url("dec/listar_dec_recebidas/NAO_LIDAS"); ?>" style="font-size: 18px;" class="tag_a_hover">Não Lidas: <?php if($msg_nao_lidas_dec){echo $msg_nao_lidas_dec;}else{ echo "0"; } ?>
                          
                        </a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Mensagens DEC</span>
                <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Mensagens do DTE não lidas' data-original-title='' title=''><?php 
                if($msg_nao_lidas_dec){echo $msg_nao_lidas_dec;}else{
                  echo "0";
                }  ?></div></h4><i class="icon-bg" data-feather="mail"></i>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>
    <?php } ?>
    

    <div class="col-sm-4 col-xl-4 col-lg-4 box-col-4">
      <a href="<?php echo base_url("ecac/listar_mensagens"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">Mensagens e-CAC</span>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ecac/listar_mensagens?filtro=LIDAS"); ?>" style="font-size: 18px;" class="tag_a_hover">Lidas: <?php echo $msg_lidas; ?></a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("ecac/listar_mensagens?filtro=NAO_LIDAS"); ?>" style="font-size: 18px;" class="tag_a_hover">Não Lidas: <?php echo $msg_nao_lidas; ?>
                        
                      </a>  
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("ecac/listar_mensagens?filtro=IMPORTANTES"); ?>" style="font-size: 18px;" class="tag_a_hover">Importantes Não Lidas: <?php echo $qtd_ecac_importante; ?>
                        
                      </a>  
                  </div>
                </teste>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Mensagens e-CAC </span>
              <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Mensagens do e-CAC não lidas' data-original-title='' title=''><?php echo $msg_nao_lidas; ?></div></h4><i class="icon-bg" data-feather="mail"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    
    </div>

    

    </div>

  </div>
</div> 

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->

<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->


<script type="text/javascript">
  $(document).ready(function() {
    

      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>