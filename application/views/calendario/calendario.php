
<!-- Responsive css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">

<link href='<?php echo base_url('assets/bundles/fullcalendar/packages/core/main.min.css') ?>' rel='stylesheet' />
<link href='<?php echo base_url('assets/bundles/fullcalendar/packages/daygrid/main.min.css') ?>' rel='stylesheet' />
<link href='<?php echo base_url('assets/bundles/fullcalendar/packages/timegrid/main.min.css') ?>' rel='stylesheet' />
<link rel="stylesheet" href="<?php echo base_url('assets/bundles/bootstrap-daterangepicker/daterangepicker.css') ?>">
<style>
.datepicker{z-index:1151 !important;}

.clockpicker-popover {
    z-index: 999999;
}
</style>

<style type="text/css">
  .fc-next-button{
    margin-left: 5px !important;
    background-color: rgb(0, 192, 239) !important;
  }

  .fc-event {
      border: 0px solid #3788d8 !important;
  }

  .fc-time{
    display: none;
  }
  .fc-prev-button {
    background-color: rgb(0, 192, 239) !important;
  }

  .fc-button-group>.fc-button:not(:first-child) {
      margin-left: 10px;
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
  }

  .fc-dayGridMonth-button{
    background-color: rgb(0, 192, 239);
  }

  .fc-timeGridWeek-button{
    background-color: rgb(0, 192, 239);
  }

  .fc-timeGridDay-button{
    background-color: rgb(0, 192, 239);
  }

  .fc-today-button{
    background-color: rgb(0, 192, 239) !important;
  }

  h2{
    color: #24a0e9;
  }
  
  .card .card-body2 {
    padding: 10px !important;
    background-color: transparent;
}
</style>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>

<div class="page-body">
  <div class="container-fluid">
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="calendar-wrap">
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header">
              <h2 style="color: #24a0e9;">Tarefas</h2>
            </div>
            <div class="section-body">
              <div class="row">
                <div class="col-md-3 col-sm-12">

                  <div class="card">
                    <div class="card-body2">
                      <div class="">
                        <div class="row no-gutters">
                          <div class="col-10 mx-auto filter-container">
                            <h4>Documentos</h4>
                            <div class="form-group">
                                <a class="btn btn-default btn-rounded" style="color: #FFFFFF; background-color: #24a0e9;margin-right: 5px !important;" href="<?php echo base_url("documento/listar"); ?>"  data-original-title="" title="">Ver Documentos</a>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-body2">
                      <div class="">
                        <div class="row no-gutters">
                          <div class="col-10 mx-auto filter-container">
                            <h4>Tipos de Documentos</h4>
                            <div class="form-group">
                                <a class="btn btn-default btn-rounded" style="color: #FFFFFF; background-color: #24a0e9;margin-right: 5px !important;" href="<?php echo base_url("tipodocumento/listar"); ?>"  data-original-title="" title="">Ver Tipos de Documentos</a>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-body">
                      <div class="">
                        <div class="row no-gutters">
                          <div class="col-10 mx-auto filter-container">
                            <h4>Filtro</h4>
                            <div class="form-group">
                                <label for="nome">Tipo de Evento:</label>
                                <?php echo form_dropdown(array('name'=>'id_evento', 'id'=>'filtro_id' ,'class'=>'select2_demo_1 form-control', 'placeholder'=>'', 'onchange' => 'findCalendario()' , 'style'=>'width:100% !important'), $lista_evento, ''); ?>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-body">
                      <h4>Tipos de Eventos</h4>
                      <br>
                      <div id='external-events'>
                        <?php 
                          if ($eventos){ 
                            foreach ($eventos as $c){?>
                              <?php if($c->remover == 0){ ?>

                                <div class="fc-event fc-event-<?php echo $c->cor; ?>" style="background-color: <?php echo $c->cor; ?> ;margin-bottom: 4px;" data-class="fc-event-success" id="<?php echo $c->id; ?>"><?php echo $c->nome; ?></div>


                              <?php } ?>
                            <?php } 
                          }
                        ?>
                        
                        <br>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id='drop-remove'>
                          <label class="custom-control-label" for="drop-remove">Remover depois de arrastar</label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="card">
                    <div class="card-body">
                      <h4>Cadastrar Tipo de Evento</h4>
                      <br>
                      <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                        <ul class="fc-color-picker" id="color-chooser">
                          <a class="text-success" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-warning" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-primary" style="color:#e517d5 !important;" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-danger" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-info" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-light" style="color: blue !important;" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-dark" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-success" style="color: #9e4df8 !important;" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-success" style="color: brown !important;" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-success" style="color: gold !important;" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-success" style="color: #24ffff !important;" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                          <a class="text-success" style="color: #237c05 !important;" href="javascript:void(0)"><i class="fa fa-square"></i></a>
                        </ul>
                      </div>
                      <div class="input-group" id="passo28">
                        <input id="new-event" type="text" class="form-control" placeholder="Tipo de evento">

                        <div class="input-group-btn" style="padding-left: 10px">
                          <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Adicionar</button>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-md-9 col-sm-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="panel-body">
                        <div id="calendar" class="has-toolbar"> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addEventTitle">Adicionar Evento</h5>
        <h5 class="modal-title" id="editEventTitle">Editar Evento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="hidden" name="id_evento_criado" id="id_evento_criado" style="display: none;">
                <label>Nome</label>
                <div class="input-group">
                  <input type="text" class="form-control" name="title" id="title">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-4">
              <label>Tipo de Evento</label>
                <?php echo form_dropdown(array('name'=>'id_evento', 'id'=>'id_evento', 'class'=>'form-control js-example-basic', 'placeholder'=>''), $lista_evento, ''); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label>Início</label>
                <input type="text" class="datepicker-here form-control digits" data-language="en" name="starts_at"
                  id="starts-at">
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label>Fim</label>
                <input type="text" class="datepicker-here form-control digits" data-language="en" name="ends_at"
                  id="ends-at">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Detalhes</label>
                <textarea id="eventDetails" name="eventDetails"
                  class="form-control"></textarea>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12" id="opcao_periodo">
              <div class="form-group"> 
                  <label>Frequência</label>
                  <select name="opcao_periodo_selecionada" class="form-control" id="opcao_periodo_selecionada">
                    <option value="NENHUMA" ></option>
                    <option value="DIAS">Diária (Período máximo de 30 dias)</option>
                    <option value="SEMANAS">Semanal (Período máximo de 8 semanas)</option>
                    <option value="MESES">Mensal (Período máximo de 12 meses)</option>
                    <option value="ANOS">Anual (Período máximo de 5 anos)</option> 
                  </select>
              </div>
            </div> 
          </div>

          <div class="modal-footer bg-whitesmoke pr-0">
            <button type="button" class="btn btn-info" id="add-event">Adicionar</button>
            <button type="button" class="btn btn-success" id="edit-event">Editar</button>
            <button type="button" class="btn btn-danger" id="excluir-evento" onclick="excluir_evento()">Excluir</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- latest jquery-->
<script src="../assets/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url("assets/bundles/jquery-ui/jquery-ui.min.js"); ?>"></script>
<script src="../assets/js/bootstrap/popper.min.js"></script>
<script src="../assets/js/bootstrap/bootstrap.js"></script>
<!-- feather icon js-->
<script src="../assets/js/icons/feather-icon/feather.min.js"></script>
<script src="../assets/js/icons/feather-icon/feather-icon.js"></script>
<!-- Sidebar jquery-->
<script src="../assets/js/sidebar-menu.js"></script>
<script src="../assets/js/config.js"></script>
<!-- Plugins JS start-->
<script src="../assets/js/jquery.ui.min.js"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="../assets/js/script.js"></script>
<script src="<?php echo base_url('assets/js/app.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>

<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/core/main.min.js') ?>'></script>
<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/interaction/main.min.js') ?>'></script>
<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/daygrid/main.min.js') ?>'></script>
<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/timegrid/main.min.js') ?>'></script>


<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>

<script type="text/javascript">

  $(function() {
    var currColor = "#f56954"; //Red by default
          //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    var btnAdd = $("#add-new-event");
    $("#color-chooser > a").click(function(e) {
        e.preventDefault();
        //Save color
        currColor = $(this).css("color");
        //Add color effect to button
        colorChooser
                .css({"background-color": currColor, "border-color": currColor})
                .html($(this).text()+' <span class="caret"></span>');
        btnAdd
                .css({"background-color": currColor, "border-color": currColor});;
        $('#add-new-event').attr('style', 'background-color: '+currColor+'!important;border-color:'+currColor+'!important;');
    });
    $("#add-new-event").click(function(e) {
        e.preventDefault();
        //Get value and make sure it is not null
        var val = $("#new-event").val();
        if (val.length == 0) {
            return;
        }else{

            $.post("<?php echo base_url();?>Calendario/cadastrarEvento",
            {
                id:0,
                nome:val,
                cor:currColor
            },
            function(data){
                //alert(data);
               
                var str = data;
                var id = str.substring(0, str.indexOf(";"));
                var colaborador = str.substring(str.indexOf(";") + 1, str.length);

                var event = $('<div class="fc-event fc-event-success" data-class="fc-event-success" id="">'+data+'</div>');//$("<div id="+data+"/>");
                event.css({"background-color": currColor, "border-color": currColor, "color": "#fff", "margin-bottom": '4px'}).addClass("external-event");
                
                event.id = id;
                event.title = val;
                event.description = colaborador;

                event.html(val);

                $('#external-events').prepend(event);
                //Add draggable funtionality
                ini_events(event);
                //Remove event from text input
                $("#new-event").val("");    
            });


            //Create event
            //var event = $("<div />");
            
        }
    });
  });
</script>

<script type="text/javascript">
  
  let calendar;
  var Draggable = FullCalendarInteraction.Draggable;
  let date_picker;
  let filter_option = "all";

  var containerEl = document.getElementById("external-events");
  var checkbox = document.getElementById("drop-remove");
  var addEvent = document.getElementById("add-event");
  var editEvent = document.getElementById("edit-event");
  var addEventTitle = document.getElementById("addEventTitle");
  var editEventTitle = document.getElementById("editEventTitle");

  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth();
  var year = date.getFullYear();

  (this.$eventModal = $("#event-modal")),
  new Draggable(containerEl, {
    itemSelector: ".fc-event",
    eventData: function (eventEl) {
      return {
        title: eventEl.innerText,
        stick: true,
        className: eventEl.dataset.class,
        backgroundColor: eventEl.style.backgroundColor,
        id: eventEl.id
      };
    },
  });

  $(document).ready(function () {
    initCalendar($('#filtro_id'));
    addEvetClick();
    refresh_calendar_filter();
    $(".select2_demo_1").select2();
  });

  function initCalendar(filtro) {

     $.ajax({
      type: "POST",
      url: '<?php echo base_url('calendario/listar_eventos_calendario'); ?>',
      data: {filtro : filtro.val()},
      async: true,
      success: function(result){
        var data = JSON.parse(result);

        var calendarEl = $("#calendar").get(0);
        calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: ["interaction", "dayGrid", "timeGrid"],
          header: {
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay",
          },
          buttonText: {//This is to add icons to the visible buttons
              today: 'Hoje',
              month: 'mês',
              week: 'semana',
              day: 'dia'
          },
          monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
          monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
          dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
          dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
          locale: 'pt-br',
          editable: true,
          droppable: true,
          navLinks: true,
          eventLimit: false,
          weekNumberCalculation: "ISO",
          displayEventEnd: true,
          lazyFetching: true,
          selectable: true,
          eventMouseEnter: function (info) {
            // $(info.el).attr('id', info.event.id);

            // $('#' + info.event.id).popover({
            //   template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
            //   title: info.event.title,
            //   content: info.event.extendedProps.description,
            //   placement: 'top',
            //   html: true
            // });
            // $('#' + info.event.id).popover('show');
            // $('.popover .popover-header').css('color', $(info.el).css('background-color'));
          },
          eventMouseLeave: function (info) {
            //$('#' + info.event.id).popover('hide');
          },
          drop: function (info) {
            var remover = 0;
            if (checkbox.checked) {
              info.draggedEl.parentNode.removeChild(info.draggedEl);
              remover = 1;
            }
            var id_evento = info.draggedEl.id;
            var start = info.dateStr;
            var background_color = info.draggedEl.style.backgroundColor;
            var nome = info.draggedEl.outerText;

            $.post("<?php echo base_url();?>Calendario/cadastrarEventoCriado",
            {
                nome:nome,
                fecini:start,
                end:start,
                cor:background_color,
                id_evento: id_evento,
                remover: remover
            },
            function(data){
                if (data != 0) {
                  calendar.destroy();
                  initCalendar($('#filtro_id'));
                }else{
                  alert('ERROR.');
                }
            });

          },
          views: {
            dayGridMonth: {
              eventLimit: 3,
            },
          },
          eventReceive:function(info ){
            var aa = info;
          },

          events: events(data),
          eventDrop: function(event, delta, revertFunc){
            var id = event.event.id;
            id = id.substring(6);
              
            var fi = moment(event.event.start).format("YYYY-MM-DD HH:mm:ss");
            var ff = "";//event.end.format();
            if(event.event.end != null){
                ff = moment(event.event.end).format("YYYY-MM-DD HH:mm:ss");
            }else{
                ff = moment(event.event.start).format("YYYY-MM-DD HH:mm:ss");
            }

              $.post("<?php echo base_url();?>Calendario/updEvento",
              {
                  id:id,
                  fecini:fi,
                  fecfin:ff
              },
              function(data){
                  if (data == 1) {
                    //alert('Atualizou');
                  }else{
                    alert('ERROR.');
                  }
              });
          },

          eventRender: function (info) {

            if (filter_option !== "all" && info.event.groupId !== filter_option) {
              return false;
            }
          },
          select: function (start, end) {
            addEvent.style.display = "block";
            editEvent.style.display = "none";
            addEventTitle.style.display = "block";
            editEventTitle.style.display = "none";
            clearModalForm();

            $("#starts-at").val(moment(start.startStr).format("DD/MM/YYYY HH:mm:ss"));
            $("#ends-at").val(moment(start.endStr).format("DD/MM/YYYY HH:mm:ss"));

            
            $(".modal").modal("show");
          },
          eventClick: function (info) {
            addEvent.style.display = "none";
            editEvent.style.display = "block";
            addEventTitle.style.display = "none";
            editEventTitle.style.display = "block";

            let startDate = moment(info.event.start).format("DD/MM/YYYY HH:mm:ss");
            let endDate;
            if(info.event.end == null){
              endDate = moment(info.event.start).format("DD/MM/YYYY HH:mm:ss");
            }else{
              endDate = moment(info.event.end).format("DD/MM/YYYY HH:mm:ss");
            }
            

            console.log(info.event.extendedProps.description);
            $(".modal").modal("show");
            $(".modal").find("#title").val(info.event.title);
            var id_e = info.event.id.substring(6);
            $(".modal").find('#id_evento_criado').val(id_e);
            $(".modal").find("#starts-at").val(startDate);
            $(".modal").find("#ends-at").val(endDate);
            $(".modal").find('#id_evento').val(info.event.constraint);
            $("#categorySelect").val(info.event.classNames[0]);
            $("#categorySelect").change();
            $(".modal")
              .find("#eventDetails")
              .val(info.event.extendedProps.description);

            $(".modal")
              .find("#opcao_periodo_selecionada")
              .val(info.event.extendedProps.frequencia);

            $("#edit-event")
              .off("click")
              .on("click", function (event) {
                event.preventDefault();

                var title = $("#title").val();
                var id_evento = $("#id_evento").find(":selected").val();
                var inicio = $("#starts-at").val();

                if(inicio.includes('/')){
                  var dt = inicio.split("/");
                  var hora = dt[2].split(' ');

                  inicio = hora[0]+"-"+dt[1]+"-"+dt[0]+" "+hora[1];
                }

                var end = $("#ends-at").val();
                if(end.includes('/')){
                  var dt = end.split("/");
                  var hora = dt[2].split(' ');

                  end = hora[0]+"-"+dt[1]+"-"+dt[0]+" "+hora[1];
                }
                
                var detalhes = document.getElementById("eventDetails").value;
                var frequencia = document.getElementById("opcao_periodo_selecionada").value;
                var id = $("#id_evento_criado").val();
                $.post("<?php echo base_url();?>Calendario/editarEventoCriadoModal",
                {
                    id:id,
                    nome:title,
                    fecini:inicio,
                    end:end,
                    id_evento: id_evento,
                    remover: 0,
                    detalhes: detalhes,
                    frequencia: frequencia
                },
                function(data){
                    if (data != 0) {
                      calendar.destroy();

                      initCalendar($('#filtro_id'));

                    }else{
                      alert('ERROR.');
                    }
                });

                // var category = $("#categorySelect").find(":selected").val();

                // var event = calendar.getEventById(info.event.id);
                // event.setProp("title", document.getElementById("title").value);
                // event.setStart(
                //   moment(document.getElementById("starts-at").value).format(
                //     "YYYY-MM-DD HH:mm:ss"
                //   )
                // );
                // event.setEnd(
                //   moment(document.getElementById("ends-at").value).format(
                //     "YYYY-MM-DD HH:mm:ss"
                //   )
                // );
                // event.setProp("classNames", [category]);
                $(".modal").modal("hide");
              });
          },
        });

        calendar.render();
        
      }
    });

    
  }

  function refresh_calendar_filter() {
    $(document).on("click", ".filter-container .filter", function (e) {
      filter_option = $(this).val();
      calendar.rerenderEvents();
    });
  }

  function clearModalForm() {
    var input = document.querySelectorAll('input[type="text"]');
    var textarea = document.getElementsByTagName("textarea");
    for (i = 0; i < input.length; i++) {
      input[i].value = "";
    }
    for (j = 0; j < textarea.length; j++) {
      textarea[j].value = "";
      i;
    }
  }

  function addEvetClick() {
    $("#add-event").on("click", function (event) {
      var title = $("#title").val();
      var id_evento = $("#id_evento").find(":selected").val();
      var inicio = $("#starts-at").val();

      if(inicio.includes('/')){
        var dt = inicio.split("/");
        var hora = dt[2].split(' ');

        inicio = hora[0]+"-"+dt[1]+"-"+dt[0]+" "+hora[1];
      }

      var end = $("#ends-at").val();
      if(end.includes('/')){
        var dt = end.split("/");
        var hora = dt[2].split(' ');

        end = hora[0]+"-"+dt[1]+"-"+dt[0]+" "+hora[1];
      }
      
      var detalhes = document.getElementById("eventDetails").value;
      var frequencia = document.getElementById("opcao_periodo_selecionada").value;

      $.post("<?php echo base_url();?>Calendario/cadastrarEventoCriadoModal",
      {
          nome:title,
          fecini:inicio,
          end:end,
          id_evento: id_evento,
          remover: 0,
          detalhes: detalhes,
          frequencia: frequencia
      },
      function(data){
          if (data != 0) {
            calendar.destroy();

            initCalendar($('#filtro_id'));

          }else{
            alert('ERROR.');
          }
      });

      
      // Clear modal inputs
      $(".modal").find("input").val("");
      // hide modal
      $(".modal").modal("hide");
    });
  }

  function randomIDGenerate(length, chars) {
    var result = "";
    for (var i = length; i > 0; --i)
      result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
  }

  function events(data) {
    var array = [];

    for(var i in data){
      array.push(
        {id: "event-"+data[i].id,
        title: ""+data[i].title,
        start: data[i].start,
        end: data[i].end,
        color: data[i].backgroundColor,
        description: data[i].observacao,
        constraint: data[i].id_evento,
        frequencia: data[i].frequencia}
        );
    } 

    return array;  
  }

  function findCalendario(){
    calendar.destroy();

    initCalendar($('#filtro_id'));
  }

  function excluir_evento(){
    var id = $("#id_evento_criado").val();
    swal({
        title: 'Deseja excluir essa tarefa ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Não, cancelar!',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
      }).then(function () {

        $.post("<?php echo base_url();?>calendario/excluir_tarefa",
        {
            id:id
        },
        function(data){
          if(data == 1){
            swal(
              'Concluído!',
              'Tarefa excluída',
              'success'
            ).done(); window.setTimeout(function(){

                  // Move to a new location or you can do something else
                  window.location = global_url;

              }, 2000);
          }
        });

        
      }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'Processo cancelado!',
            'error'
          ).done();
        }
      }).done();
  }
</script>