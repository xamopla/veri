<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h4 class="page-title m-b-0">Visualizar Empresa <small class="text-info" style="font-size: 15px;">- <?php  echo $empresa->razao_social; ?></small></h4>
              </div>
              <div class="card-body">
                <div class="page-content fade-in-up">
                  <div class="ibox">
                    <div class="ibox-head">
                        <ul class="nav nav-tabs tabs-line">
                          <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Identificação</a></li>
                          <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Endereço</a></li>
                          <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Domicílio Tributário Eletrônico - DTE</a></li>
                          <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">Informações Complementares</a></li>
                          <li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab">Contador</a></li>
                          <li class="nav-item"><a class="nav-link" href="#tab_6" data-toggle="tab">Contatos&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li>
                          <?php if ($this->session->userdata['userprimesession']['nivel'] != 2){ ?>
                          <li class="nav-item"><a class="nav-link" href="#tab_7" data-toggle="tab">Senhas&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li>
                          <?php } ?>
                          <li class="nav-item"><a class="nav-link" href="#tab_8" data-toggle="tab">Anotações&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="ibox-body">
                        <div class="tab-content">
                            
                        <!-- TAB 1 -->
                        <div class="tab-pane fade show active" id="tab_1">
                            <h3 class="box-title">Identificação</h3>
                            <div class="col-lg-12">
                                <hr align="center" width="100%" size="1" color=#e4e9f0>
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Inscrição Estadual:</label>
                                         <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>CNPJ</label>
                                         <?php echo form_input(array('name'=>'cnpj', 'class'=>'form-control', 'placeholder'=>'CNPJ'), $empresa->cnpj_completo, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Razão Social:</label>
                                         <?php echo form_input(array('name'=>'razao_social', 'style'=>'font-weight:bold; color: #000000;', 'class'=>'form-control', 'placeholder'=>'Razão Social'), $empresa->razao_social, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Micro Empreendedor Individual - MEI:</label>
                                    <?php echo form_input(array('name'=>'mei', 'class'=>'form-control', 'placeholder'=>'MEI'), $empresa->mei, 'readonly'); ?>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Nome Fantasia:</label>
                                         <?php echo form_input(array('name'=>'nome_fantasia', 'class'=>'form-control', 'placeholder'=>'Nome Fantasia'), $empresa->nome_fantasia, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Natureza Jurídica:</label>
                                         <?php echo form_input(array('name'=>'natureza_juridica', 'class'=>'form-control', 'placeholder'=>'Natureza Jurídica'), $empresa->natureza_juridica, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Unidade de Atendimento:</label>
                                         <?php echo form_input(array('name'=>'unidade_atendimento', 'class'=>'form-control', 'placeholder'=>'Unidade de Atendimento'), $empresa->unidade_atendimento, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Unidade de Fiscalização:</label>
                                         <?php echo form_input(array('name'=>'unidade_fiscalizacao', 'class'=>'form-control', 'placeholder'=>'Unidade de Fiscalização'), $empresa->unidade_fiscalizacao, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group mb-4">
                                         <label>Atividade Econômica Principal:</label>
                                         <?php echo form_input(array('name'=>'atividade_principal', 'class'=>'form-control', 'placeholder'=>'Atividade Econômica Principal'), $empresa->atividade_principal, 'readonly'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- TAB 2 -->
                        <div class="tab-pane fade" id="tab_2">
                            <h3 class="box-title">Endereço</h3>
                            <div class="col-lg-12">
                                <hr align="center" width="100%" size="1" color=#e4e9f0>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>CEP</label>
                                         <?php echo form_input(array('name'=>'cep', 'class'=>'form-control', 'placeholder'=>'CEP'), $empresa->cep, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Logradouro</label>
                                         <?php echo form_input(array('name'=>'logradouro', 'class'=>'form-control', 'placeholder'=>'Logradouro'), $empresa->logradouro, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Número</label>
                                         <?php echo form_input(array('name'=>'numero', 'class'=>'form-control', 'placeholder'=>'Número'), $empresa->numero, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Complemento</label>
                                         <?php echo form_input(array('name'=>'complemento', 'class'=>'form-control', 'placeholder'=>'Complemento'), $empresa->complemento, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Bairro/Distrito</label>
                                         <?php echo form_input(array('name'=>'bairro', 'class'=>'form-control', 'placeholder'=>'Bairro'), $empresa->bairro, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Cidade</label>
                                         <?php echo form_input(array('name'=>'cidade', 'class'=>'form-control', 'placeholder'=>'Cidade'), $empresa->cidade, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>UF</label>
                                         <?php echo form_input(array('name'=>'uf', 'class'=>'form-control', 'placeholder'=>'UF'), $empresa->uf, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Referência</label>
                                         <?php echo form_input(array('name'=>'referencia', 'class'=>'form-control', 'placeholder'=>'Referência'), $empresa->referencia, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Localização</label>
                                         <?php echo form_input(array('name'=>'localizacao', 'class'=>'form-control', 'placeholder'=>'Localização'), $empresa->localizacao, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Telefone</label>
                                         <?php echo form_input(array('name'=>'telefone', 'class'=>'form-control', 'placeholder'=>'Telefone'), $empresa->telefone, 'readonly'); ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group mb-4">
                                         <label>E-mail</label>
                                         <?php echo form_input(array('name'=>'email', 'class'=>'form-control', 'placeholder'=>'E-mail'), $empresa->email, 'readonly'); ?>
                                    </div>
                                </div>
                            </div>                                
                        </div>

                        <!-- TAB 3 -->
                        <div class="tab-pane fade" id="tab_3">
                                <h3 class="box-title">Domicílio Tributário Eletrônico - DTE</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Situação do DTE</label>
                                            <?php echo form_input(array('name'=>'situacao_dte', 'class'=>'form-control', 'placeholder'=>'Situação do DTE'), $empresa->situacao_dte, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Situação da Conta</label>
                                            <?php echo form_input(array('name'=>'situacao_conta_dte', 'class'=>'form-control', 'placeholder'=>'Situação da Conta'), $empresa->situacao_conta_dte, 'readonly'); ?>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <!-- TAB 4 -->
                        <div class="tab-pane fade" id="tab_4">
                                <h3 class="box-title">Informações Complementares</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Situação Cadastral</label>
                                            <?php echo form_input(array('name'=>'situacao_cadastral', 'class'=>'form-control', 'placeholder'=>'Situação Cadastral'), $empresa->situacao_cadastral, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Situação</label>
                                            <?php echo form_input(array('name'=>'situacao', 'class'=>'form-control', 'placeholder'=>'Situação'), $empresa->situacao, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Motivo do Credenciamento / Descredenciamento</label>
                                            <?php echo form_input(array('name'=>'motivo', 'class'=>'form-control', 'placeholder'=>'Motivo'), $empresa->motivo, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Condição</label>
                                            <?php echo form_input(array('name'=>'condicao', 'class'=>'form-control', 'placeholder'=>'Condição'), $empresa->condicao, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Forma de Pagamento</label>
                                            <?php echo form_input(array('name'=>'forma_pagamento', 'class'=>'form-control', 'placeholder'=>'Forma de Pagamento'), $empresa->forma_pagamento, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>NIRE</label>
                                            <?php echo form_input(array('name'=>'nire', 'class'=>'form-control', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), $empresa->nire, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group mb-4">
                                            <label>Motivo da Situação Cadastral</label>
                                            <?php echo form_input(array('name'=>'motivo_situacao_cadastral', 'class'=>'form-control', 'placeholder'=>'Motivo desta Situação Cadastral'), $empresa->motivo_situacao_cadastral, 'readonly'); ?>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <!-- TAB 5 -->
                        <div class="tab-pane fade" id="tab_5">
                                <h3 class="box-title">Informações do Contador</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Nome do Contador</label>
                                            <?php echo form_input(array('name'=>'nome_contador', 'class'=>'form-control', 'placeholder'=>'Nome do Contador'), $empresa->nome_contador, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>CRC do Contador</label>
                                            <?php echo form_input(array('name'=>'crc_contador', 'class'=>'form-control', 'placeholder'=>'CRC do Contador'), $empresa->crc_contador, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Nome do Responsável</label>
                                            <?php echo form_input(array('name'=>'nome_responsavel', 'class'=>'form-control', 'placeholder'=>'Nome do Responsável'), $empresa->nome_responsavel, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>CRC do Responsável</label>
                                            <?php echo form_input(array('name'=>'crc_responsavel', 'class'=>'form-control', 'placeholder'=>'CRC do Responsável'), $empresa->crc_responsavel, 'readonly'); ?>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <!-- TAB 6 -->
                        <div class="tab-pane fade" id="tab_6">
                                <h3 class="box-title">Adicionar Contatos</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Telefone Alternativo</label>
                                            <?php echo form_input(array('type'=>'tel', 'maxlength'=>'14', 'onKeyPress'=>'MascaraTelefone(this)', 'name'=>'telefone_alternativo', 'id'=>'telefone_alternativo', 'class'=>'form-control', 'placeholder'=>'Telefone Alternativo'), $empresa->telefone_alternativo, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Celular Alternativo</label>
                                            <?php echo form_input(array('type'=>'tel', 'maxlength'=>'15', 'onKeyPress'=>'MascaraCelular(this)', 'name'=>'celular_alternativo', 'id'=>'celular_alternativo', 'class'=>'form-control', 'placeholder'=>'Celular Alternativo'), $empresa->celular_alternativo, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>E-mail Alternativo</label>
                                            <?php echo form_input(array('type'=>'email', 'name'=>'email_alternativo', 'class'=>'form-control', 'placeholder'=>'E-mail Alternativo'), $empresa->email_alternativo, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Funcionário</label>
                                            <?php echo form_dropdown(array('name'=>'id_funcionario', 'class'=>'selectpicker form-control', 'data-live-search'=>'true', 'placeholder'=>'Selecione o Funcionário'), $lista_funcionarios, $empresa->nomefuncionario, 'readonly'); ?>
                                        </div>
                                    </div>        
                                </div>
                        </div>

                        <!-- TAB 7 -->
                        <div class="tab-pane fade" id="tab_7">
                                <h3 class="box-title">Senhas</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Login DTE Sefaz</label>
                                            <?php echo form_input(array('name'=>'login_sefaz', 'type'=>'password', 'class'=>'form-control', 'placeholder'=>'Login Sefaz'), $empresa->login_sefaz, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Senha DTE Sefaz</label>
                                            <?php echo form_input(array('name'=>'senha_sefaz', 'type'=>'password', 'class'=>'form-control', 'placeholder'=>'Senha Sefaz'), $empresa->senha_sefaz, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>CPF do Representante</label>
                                            <?php echo form_input(array('name'=>'login_mei', 'type'=>'password', 'class'=>'form-control', 'id'=>'cpf', 'placeholder'=>'Digite sem pontuação'), $empresa->login_mei, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                            <label>Código de Acesso - Simples Nacional</label>
                                            <?php echo form_input(array('name'=>'senha_mei', 'type'=>'password', 'class'=>'form-control', 'id'=>'codigodeacesso', 'placeholder'=>'Código de Acesso - Simples Nacional'), $empresa->senha_mei, 'readonly'); ?>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <!-- TAB 8 -->
                        <div class="tab-pane fade" id="tab_8">
                                <h3 class="box-title">Outras Informações</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" hidden="true">
                                        <div class="form-group mb-4">
                                            <label>CPF do Representante</label>
                                            <?php echo form_input(array('name'=>'cpf_alvara', 'class'=>'form-control', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), $empresa->cpf_alvara, 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group mb-4">
                                            <label>Anotações</label>
                                            <?php echo form_textarea(array('name'=>'anotacoes', 'class'=>'form-control', 'id'=>'cpf', 'placeholder'=>'Anotações', 'style'=>'border-color: #5c6bc0;'), $empresa->anotacoes, 'readonly'); ?>
                                        </div>
                                    </div>

                                </div>
                        </div>

                        </div>
                        </div>
                        <div class="ibox-footer text-right">
                            <a href="<?php echo base_url("empresa/listar"); ?>" class="btn btn-outline-danger">Voltar</a>
                        </div>
                        <?php echo form_close(); ?>
                </div>
              </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

