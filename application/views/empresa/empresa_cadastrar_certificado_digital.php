<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Cadastro de Certificado Digital</h2>
              </div>
              <div class="card-body">
                <div class="ibox-body">                        
                <?php echo form_open("empresa/editar/$id", array('role'=>'form')); ?> 
                    <br>
                    <div class="row">                                
                        <div class="col-md-6">
                            <div class="form-group mb-4">
                                <label for="nome">Empresa:</label>
                                <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->razao_social, 'readonly'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group mb-4">
                                <label for="nome">CNPJ:</label>
                                <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->cnpj_completo, 'readonly'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group mb-4">
                                <label for="nome">Inscrição Estadual:</label>
                                <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                            </div>
                        </div>
                      </div>

                      <br>

                      <div>
                        <div class="text-center" >
                        <h1 style="font-size: 28px;">Certificado</h1> 
                        </div>
                      </div>

                      <br>
                      
                     <div class="row">     
                        <div class="col-md-3">
                                <div class="form-group mb-4">    
                                    <label for="nome">Situação do Certificado:</label>                                                 
                                    <input type="text" value="<?= $certificado['situacao'] ?>" class="form-control" style="background-color:<?= $certificado['cor'] ?>;color:white;" readonly="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mb-4">
                                    <label>Validade do Certificado:</label>
                                    <input type="text" name="validade_certificado" value="<?= $certificado['validade'] ?>" class="form-control" style="border-color: #5c6bc0;"  disabled />
                                </div>
                            </div>                                    
                            <div class="col-md-6">
                               <div class="form-group mb-4" style="display: inline-grid;">
                                <label>Configurar A1</label>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fullCalModal">
                                    <span class="btn-icon"><i class="la la-cog"></i>Configurar</span>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>  
                <div class="ibox-footer text-right">                            
                    <a href="<?php echo base_url("empresa/listar"); ?>" class="btn btn-primary">Voltar</a>
                </div>
                <?php echo form_close(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>



<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<div class="modal fade bd-example-modal-lg" id="fullCalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Configuração Certificado A1</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="certificado_form" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-4" style="display: table-caption;">
                            <label for="logo">Arquivo:</label>
                             <br>
                            <label class="btn btn-info file-input mr-4">
                                <span class="btn-icon" id="span_certificado"><i class="la la-upload"></i>Procurar</span>
                                <input name="certificado_file" class="file" type="file" accept=".pfx" id="certificado_input" required>
                            </label>   
                            <script type="text/javascript">
                                $('#certificado_input').change(function() {
                                    document.getElementById("span_certificado").innerHTML = '<i class="la la-upload"></i>'+ ($('#certificado_input')[0].files[0].name).substring(0,20);
                                });
                            </script> 
                            <small class="text-muted">Extensões: .pfx</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-4">
                            <label>Senha:</label>
                            <input type="password" name="certificado_senha" value="" class="form-control" placeholder="Senha Certificado" style="border-color: #5c6bc0;" required/>
                        </div>
                    </div>
                    <input type="text" name="certificado_cnpj" value="<?php echo $empresa->cnpj ?>" hidden/>
                    <input type="text" name="id_empresa" value="<?php echo $empresa->id ?>" hidden/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnCancelarModalA1">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="submit_certificado();">Configurar</button>       

                <script type="text/javascript">

                    function submit_certificado(){
                        var form = $('#certificado_form')[0];
                        var data = new FormData(form);

                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: "<?= base_url() ?>Certificado_empresa/upload", /*Alterar aqui*/
                            data: data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            success: function (data) {
                                var jsonResponse = JSON.parse(data);
                                if(jsonResponse["error"]){
                                    swal_certificado_error(jsonResponse["msg"]);
                                }else{
                                    swal_certificado_success(jsonResponse["msg"]);
                                }
                            },
                            error: function (e) {
                                alert("error");
                            }
                        });
                    }

                    function swal_certificado_success($msg){
                        document.getElementById('btnCancelarModalA1').click();
                        $(document).ready(function(){
                            swal({ 
                                title: 'Configurado!',
                                text: $msg,
                                type: "success" 
                            },
                            function(){
                                location.reload();
                            });
                            location.reload();
                        });
                    }

                    function swal_certificado_error($msg){
                        document.getElementById('btnCancelarModalA1').click();
                        $(document).ready(function() {
                            swal({ 
                                title: "Ops, Algo Aconteceu!",
                                text: $msg,
                                type: "error" 
                            },
                            function(){
                                $('#fullCalModal').modal('show'); 
                            });
                    });
                    }
                </script>

                <style type="text/css">
                    .sweet-alert .sa-icon {
                        margin-bottom: 40px;
                    }
                </style>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script type="text/javascript">

$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>