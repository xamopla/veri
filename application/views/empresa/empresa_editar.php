<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h4 class="page-title m-b-0">Editar Empresa <small class="text-info" style="font-size: 15px;">- <?php  echo $empresa->razao_social; ?></small></h4>
              </div>
              <div class="card-body">
                <div class="page-content fade-in-up">
                  <div class="ibox">
                      <div class="ibox-head">
                          <ul class="nav nav-tabs tabs-line">
                            <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Identificação</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Endereço</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Domicílio Tributário Eletrônico - DTE</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">Informações Complementares</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab">Contador</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab_6" data-toggle="tab">Contatos&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li> 
                            <li class="nav-item"><a class="nav-link" href="#tab_7" data-toggle="tab">Senhas&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li> 
                            <li class="nav-item"><a class="nav-link" href="#tab_8" data-toggle="tab">Anotações&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                      <div class="ibox-body">
                          <?php echo form_open("empresa/editar/$id/$id_contador", array('role'=>'form')); ?>
                          <div class="tab-content">
                              
                          <!-- TAB 1 -->
                          <div class="tab-pane fade show active" id="tab_1">
                              <br>
                              <h3 class="box-title">Identificação</h3>
                              <div class="col-lg-12">
                                  <hr align="center" width="100%" size="1" color=#e4e9f0>
                              </div>    
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Inscrição Estadual:</label>
                                           <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual', 'style'=>'border-color: #5c6bc0;'), $empresa->inscricao_estadual_completo); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>CNPJ</label>
                                           <?php echo form_input(array('name'=>'cnpj', 'class'=>'form-control', 'placeholder'=>'CNPJ', 'style'=>'border-color: #5c6bc0;'), $empresa->cnpj_completo, 'required readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Razão Social:</label>
                                           <?php echo form_input(array('name'=>'razao_social', 'style'=>'font-weight:bold; color: #000000;', 'class'=>'form-control', 'placeholder'=>'Razão Social', 'style'=>'border-color: #5c6bc0;'), $empresa->razao_social, 'required'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <label>Micro Empreendedor Individual - MEI:</label>
                                      <?php echo form_input(array('name'=>'mei', 'class'=>'form-control', 'placeholder'=>'MEI', 'style'=>'border-color: #5c6bc0;'), $empresa->mei, 'readonly'); ?>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Nome Fantasia:</label>
                                           <?php echo form_input(array('name'=>'nome_fantasia', 'class'=>'form-control', 'placeholder'=>'Nome Fantasia', 'style'=>'border-color: #5c6bc0;'), $empresa->nome_fantasia); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Natureza Jurídica:</label>
                                           <?php echo form_input(array('name'=>'natureza_juridica', 'class'=>'form-control', 'placeholder'=>'Natureza Jurídica', 'style'=>'border-color: #5c6bc0;'), $empresa->natureza_juridica, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Unidade de Atendimento:</label>
                                           <?php echo form_input(array('name'=>'unidade_atendimento', 'class'=>'form-control', 'placeholder'=>'Unidade de Atendimento', 'style'=>'border-color: #5c6bc0;'), $empresa->unidade_atendimento, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Unidade de Fiscalização:</label>
                                           <?php echo form_input(array('name'=>'unidade_fiscalizacao', 'class'=>'form-control', 'placeholder'=>'Unidade de Fiscalização', 'style'=>'border-color: #5c6bc0;'), $empresa->unidade_fiscalizacao, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-12">
                                      <div class="form-group mb-4">
                                           <label>Atividade Econômica Principal:</label>
                                           <?php echo form_input(array('name'=>'atividade_principal', 'class'=>'form-control', 'placeholder'=>'Atividade Econômica Principal', 'style'=>'border-color: #5c6bc0;'), $empresa->atividade_principal, 'readonly'); ?>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <!-- TAB 2 -->
                          <div class="tab-pane fade" id="tab_2">
                              <br>
                              <h3 class="box-title">Endereço</h3>
                              <div class="col-lg-12">
                                  <hr align="center" width="100%" size="1" color=#e4e9f0>
                              </div>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>CEP</label>
                                           <?php echo form_input(array('name'=>'cep', 'class'=>'form-control', 'placeholder'=>'CEP', 'style'=>'border-color: #5c6bc0;'), $empresa->cep, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Logradouro</label>
                                           <?php echo form_input(array('name'=>'logradouro', 'class'=>'form-control', 'placeholder'=>'Logradouro', 'style'=>'border-color: #5c6bc0;'), $empresa->logradouro, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Número</label>
                                           <?php echo form_input(array('name'=>'numero', 'class'=>'form-control', 'placeholder'=>'Número', 'style'=>'border-color: #5c6bc0;'), $empresa->numero, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Complemento</label>
                                           <?php echo form_input(array('name'=>'complemento', 'class'=>'form-control', 'placeholder'=>'Complemento', 'style'=>'border-color: #5c6bc0;'), $empresa->complemento, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Bairro/Distrito</label>
                                           <?php echo form_input(array('name'=>'bairro', 'class'=>'form-control', 'placeholder'=>'Bairro', 'style'=>'border-color: #5c6bc0;'), $empresa->bairro, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Cidade</label>
                                           <?php echo form_input(array('name'=>'cidade', 'class'=>'form-control', 'placeholder'=>'Cidade', 'style'=>'border-color: #5c6bc0;'), $empresa->cidade, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>UF</label>
                                           <?php echo form_input(array('name'=>'uf', 'class'=>'form-control', 'placeholder'=>'UF', 'style'=>'border-color: #5c6bc0;'), $empresa->uf, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Referência</label>
                                           <?php echo form_input(array('name'=>'referencia', 'class'=>'form-control', 'placeholder'=>'Referência', 'style'=>'border-color: #5c6bc0;'), $empresa->referencia, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Localização</label>
                                           <?php echo form_input(array('name'=>'localizacao', 'class'=>'form-control', 'placeholder'=>'Localização', 'style'=>'border-color: #5c6bc0;'), $empresa->localizacao, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group mb-4">
                                           <label>Telefone</label>
                                           <?php echo form_input(array('name'=>'telefone', 'class'=>'form-control', 'placeholder'=>'Telefone', 'style'=>'border-color: #5c6bc0;'), $empresa->telefone, 'readonly'); ?>
                                      </div>
                                  </div>

                                  <div class="col-md-12">
                                      <div class="form-group mb-4">
                                           <label>E-mail</label>
                                           <?php echo form_input(array('name'=>'email', 'class'=>'form-control', 'placeholder'=>'E-mail', 'style'=>'border-color: #5c6bc0;'), $empresa->email, 'readonly'); ?>
                                      </div>
                                  </div>
                              </div>                              
                          </div>

                          <!-- TAB 3 -->
                          <div class="tab-pane fade" id="tab_3">
                              <br>
                              <h3 class="box-title">Domicílio Tributário Eletrônico - DTE</h3>
                                  <div class="col-lg-12">
                                      <hr align="center" width="100%" size="1" color=#e4e9f0>
                                  </div>

                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Situação do DTE</label>
                                              <?php echo form_input(array('name'=>'situacao_dte', 'class'=>'form-control', 'placeholder'=>'Situação do DTE', 'style'=>'border-color: #5c6bc0;'), $empresa->situacao_dte, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Situação da Conta</label>
                                              <?php echo form_input(array('name'=>'situacao_conta_dte', 'class'=>'form-control', 'placeholder'=>'Situação da Conta', 'style'=>'border-color: #5c6bc0;'), $empresa->situacao_conta_dte, 'readonly'); ?>
                                          </div>
                                      </div>
                                  </div>
                          </div>

                          <!-- TAB 4 -->
                          <div class="tab-pane fade" id="tab_4">
                              <br>
                              <h3 class="box-title">Informações Complementares</h3>
                                  <div class="col-lg-12">
                                      <hr align="center" width="100%" size="1" color=#e4e9f0>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Situação Cadastral</label>
                                              <?php echo form_input(array('name'=>'situacao_cadastral', 'class'=>'form-control', 'placeholder'=>'Situação Cadastral', 'style'=>'border-color: #5c6bc0;'), $empresa->situacao_cadastral, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Situação</label>
                                              <?php echo form_input(array('name'=>'situacao', 'class'=>'form-control', 'placeholder'=>'Situação', 'style'=>'border-color: #5c6bc0;'), $empresa->situacao, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Motivo do Credenciamento / Descredenciamento</label>
                                              <?php echo form_input(array('name'=>'motivo', 'class'=>'form-control', 'placeholder'=>'Motivo', 'style'=>'border-color: #5c6bc0;'), $empresa->motivo, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Condição</label>
                                              <?php echo form_input(array('name'=>'condicao', 'class'=>'form-control', 'placeholder'=>'Condição', 'style'=>'border-color: #5c6bc0;'), $empresa->condicao, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Forma de Pagamento</label>
                                              <?php echo form_input(array('name'=>'forma_pagamento', 'class'=>'form-control', 'placeholder'=>'Forma de Pagamento', 'style'=>'border-color: #5c6bc0;'), $empresa->forma_pagamento, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>NIRE</label>
                                              <?php echo form_input(array('name'=>'nire', 'class'=>'form-control', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), $empresa->nire, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-12">
                                          <div class="form-group mb-4">
                                              <label>Motivo da Situação Cadastral</label>
                                              <?php echo form_input(array('name'=>'motivo_situacao_cadastral', 'class'=>'form-control', 'placeholder'=>'Motivo desta Situação Cadastral', 'style'=>'border-color: #5c6bc0;'), $empresa->motivo_situacao_cadastral, 'readonly'); ?>
                                          </div>
                                      </div>
                                  </div>      
                          </div>

                          <!-- TAB 5 -->
                          <div class="tab-pane fade" id="tab_5">
                              <br>
                              <h3 class="box-title">Informações do Contador</h3>
                                  <div class="col-lg-12">
                                      <hr align="center" width="100%" size="1" color=#e4e9f0>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Nome do Contador</label>
                                              <?php echo form_input(array('name'=>'nome_contador', 'class'=>'form-control', 'placeholder'=>'Nome do Contador', 'style'=>'border-color: #5c6bc0;'), $empresa->nome_contador, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>CRC do Contador</label>
                                              <?php echo form_input(array('name'=>'crc_contador', 'class'=>'form-control', 'placeholder'=>'CRC do Contador', 'style'=>'border-color: #5c6bc0;'), $empresa->crc_contador, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Nome do Responsável</label>
                                              <?php echo form_input(array('name'=>'nome_responsavel', 'class'=>'form-control', 'placeholder'=>'Nome do Responsável', 'style'=>'border-color: #5c6bc0;'), $empresa->nome_responsavel, 'readonly'); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>CRC do Responsável</label>
                                              <?php echo form_input(array('name'=>'crc_responsavel', 'class'=>'form-control', 'placeholder'=>'CRC do Responsável', 'style'=>'border-color: #5c6bc0;'), $empresa->crc_responsavel, 'readonly'); ?>
                                          </div>
                                      </div>
                                  </div>
                          </div>

                          <!-- TAB 6 -->
                          <div class="tab-pane fade" id="tab_6">
                              <br>
                              <h3 class="box-title">Adicionar Contatos</h3>
                                  <div class="col-lg-12">
                                      <hr align="center" width="100%" size="1" color=#e4e9f0>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Telefone Alternativo</label>
                                              <?php echo form_input(array('type'=>'tel', 'maxlength'=>'14', 'onKeyPress'=>'MascaraTelefone(this)', 'name'=>'telefone_alternativo', 'id'=>'telefone_alternativo', 'class'=>'form-control', 'placeholder'=>'Telefone Alternativo', 'style'=>'border-color: #5c6bc0;'), $empresa->telefone_alternativo, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Celular Alternativo</label>
                                              <?php echo form_input(array('type'=>'tel', 'maxlength'=>'15', 'onKeyPress'=>'MascaraCelular(this)', 'name'=>'celular_alternativo', 'id'=>'celular_alternativo', 'class'=>'form-control', 'placeholder'=>'Celular Alternativo', 'style'=>'border-color: #5c6bc0;'), $empresa->celular_alternativo, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>E-mail Alternativo</label>
                                              <?php echo form_input(array('type'=>'email', 'name'=>'email_alternativo', 'class'=>'form-control', 'placeholder'=>'E-mail Alternativo', 'style'=>'border-color: #5c6bc0;'), $empresa->email_alternativo, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4"> 
                                              <div class="col-form-label">Colaborador Principal</div>
                                              <?php echo form_dropdown(array('name'=>'id_funcionario', 'class'=>'js-example-basic-single col-sm-6', 'placeholder'=>'Selecione o Funcionário'), $lista_funcionarios, $empresa->id_funcionario, ''); ?>
                                          </div>
                                      </div> 

                                      <div class="col-md-6">
                                          <div class="form-group"> 
                                              <div class="col-form-label">Colaboradores Auxiliares</div>
                                              <?php echo form_dropdown('id_usuario[]', $lista_funcionarios2, $listaSelecionados, 'class="form-control js-example-basic-multiple col-sm-12" id="id_empresa_usuario" multiple="multiple" style="width: 500px;"');?>

                                          </div>
                                      </div> 
                                  </div>
                          </div>

                          <!-- TAB 7 -->
                          <div class="tab-pane fade" id="tab_7">
                              <br>
                              <h3 class="box-title">Senhas</h3>
                                  <div class="col-lg-12">
                                      <hr align="center" width="100%" size="1" color=#e4e9f0>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Login DTE Sefaz</label>
                                              <?php echo form_input(array('name'=>'login_sefaz', 'type'=>'password', 'class'=>'form-control', 'placeholder'=>'Login Sefaz', 'style'=>'border-color: #5c6bc0;'), $empresa->login_sefaz, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>Senha DTE Sefaz</label>
                                              <?php echo form_input(array('name'=>'senha_sefaz', 'type'=>'password', 'class'=>'form-control', 'placeholder'=>'Senha Sefaz', 'style'=>'border-color: #5c6bc0;'), $empresa->senha_sefaz, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6">
                                          <div class="form-group mb-4">
                                              <label>CPF do Representante</label>
                                              <?php echo form_input(array('name'=>'login_mei', 'class'=>'form-control', 'id'=>'cpf', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), $empresa->login_mei, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-6" hidden="true">
                                          <div class="form-group mb-4">
                                              <label>Código de Acesso - Simples Nacional</label>
                                              <?php echo form_input(array('name'=>'senha_mei', 'type'=>'password', 'class'=>'form-control', 'id'=>'codigodeacesso', 'placeholder'=>'Código de Acesso - Simples Nacional', 'style'=>'border-color: #5c6bc0;'), $empresa->senha_mei, ''); ?>
                                          </div>
                                      </div>
                                  </div>
                          </div>

                          <!-- TAB 8 -->
                          <div class="tab-pane fade" id="tab_8">
                              <br>
                              <h3 class="box-title">Anotações</h3>
                                  <div class="col-lg-12">
                                      <hr align="center" width="100%" size="1" color=#e4e9f0>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6" hidden="true">
                                          <div class="form-group mb-4">
                                              <label>CPF do Representante</label>
                                              <?php echo form_input(array('name'=>'cpf_alvara', 'class'=>'form-control', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), $empresa->cpf_alvara, ''); ?>
                                          </div>
                                      </div>

                                      <div class="col-md-12">
                                          <div class="form-group mb-4">
                                              <label>Anotações</label>
                                              <?php echo form_textarea(array('name'=>'anotacoes', 'class'=>'form-control', 'placeholder'=>'Anotações', 'style'=>'border-color: #5c6bc0;'), $empresa->anotacoes, ''); ?>
                                          </div>
                                      </div>

                                  </div>
                          </div>

                          </div>
                          </div>
                          <div class="ibox-footer text-right">
                              <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                              <a href="<?php echo base_url("empresa/listar"); ?>" class="btn btn-outline-secondary">Cancelar</a>
                          </div>
                          <?php echo form_close(); ?>
                  </div>
              </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
 
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

