<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Lista de Empresas</h2> 
            <div class="card-header-action" style="float: right !important;">
                <?php if ($e->valor >= $emax->valor) { ?>
                <a disabled data-toggle="tooltip" data-placement="top" title="Você atingiu o limite de empresas cadastradas em seu plano, você pode solicitar um aumento da quantidade de empresas entrando em contado conosco." class="btn btn-lg btn-default btn-rounded pull-right" style="color: #FFFFFF; background-color: #24a0e9; cursor:not-allowed;" href="#">Nova Empresa</a> 
              <?php } else { ?>
                <a class="btn btn-lg btn-default btn-rounded pull-right" id="passo10" style="color: #FFFFFF; background-color: #24a0e9;" href="<?php echo base_url("empresa/cadastrar_empresa_etapa1"); ?>">Nova Empresa</a> 
              <?php } ?>

              <a class="btn btn-lg btn-default btn-rounded pull-right"  style="color: #FFFFFF; background-color: #24a0e9;margin-right: 5px;" href="<?php echo base_url("contador/cadastrar_novo"); ?>">Nova Procuração</a>
            </div>
          </div>
          
          <div class="card-body">
            <div class="table-responsive" id="tabela_excel">
                <table class="table table-striped table-hover" id="example" style="width:100%;">
                  <thead>
                    <tr>
                      <th >Razão Social</th>
                      <th >Nome Fantasia</th>
                      <th >CNPJ</th>
                      <th >Inscrição Estadual</th>
                      <th >Natureza Jurídica</th>
                      <th >Endereço</th>
                      <th>Município</th>
                      <th >Regime</th>
                      <th style="display:none;font-family: serif;color: #7f2222 !important;">E-mail</th>
                      <th style="display:none;font-family: serif;color: #7f2222 !important;">Telefone</th>
                      <th style="text-align: center;">Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($consulta){ 
                      foreach ($consulta as $e){?>
                      <tr>
                        <td ><?php echo $e->razao_social; ?></td>
                        <td ><?php echo $e->nome_fantasia; ?></td>
                        <td ><?php echo $e->cnpj_completo; ?></td>
                        <td ><?php echo $e->inscricao_estadual_completo; ?></td>
                        <td ><?php echo $e->natureza_juridica; ?></td>
                        <td ><?php 
                        if ($e->logradouro){
                          echo $e->logradouro;
                        }
                        if ($e->numero){
                          echo ', Nº '.$e->numero;
                        }
                        if ($e->complemento){
                          echo ', '.$e->complemento;
                        }
                        if ($e->bairro){
                          echo ', '.$e->bairro;
                        }
                        if ($e->cidade){
                          echo ' - '.$e->cidade;
                        }
                        if ($e->uf){
                          echo ' - '.$e->uf;
                        }
                        if ($e->cep){
                          echo ' - '.$e->cep;
                        }
                        ?></td>
                        <td><?php echo $e->cidade; ?></td>
                        <td style="font-family: serif;"><?php echo $e->tipo_regime; ?></td>
                        <td style="display:none;font-family: serif;"><?php echo $e->email; ?></td>
                        <td style="display:none;font-family: serif;"><?php echo $e->telefone; ?></td>
                        <td width="10%">
                          <a href="<?php echo base_url("empresa/editar/$e->id/$e->id_contador"); ?>" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-default b" style="padding: 1px !important;" ><i class="material-icons" style="color: #24a0e9;">create</i></a>

                          <?php if ($nome_plano->valor == "Plano Free"): ?>
                          <a disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Não é possível excluir empresas no plano gratuito" style="padding: 1px !important;"><i class="material-icons" style="color: #24a0e9;">delete_forever</i>
                          </a>     
                          <?php else: ?>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="Excluir" onclick='preencher_url("<?php echo base_url("empresa/excluir/$e->id/$e->id_contador"); ?>")'><span class="btn sweet-10" style="padding: 1px 4px;"><i class="material-icons" style="color: #24a0e9;">delete_forever</i></span>
                          </a>   
                          <?php endif ?>                           

                          <a href="<?php echo base_url("certificado_empresa/cadastrar/$e->id"); ?>" data-toggle="tooltip" data-placement="top" title="Cadastrar Certificado A1 - Apenas para fins de controle de vencimento" class="btn btn-default b" style="padding: 1px !important;"><i class="material-icons" style="color: #24a0e9;">cloud_upload</i></a>

                          <a href="javascript:void(0);" onclick="alterar_regime('<?php echo $e->id; ?>')" data-toggle="tooltip" data-placement="top" title="Alterar Regime" class="btn btn-default b" style="padding: 1px !important;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9;">attach_money</i></a> 
                          
                        </td>
                      </tr>
                  <?php } 
                  }
                  ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modalRegime" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tipoRegimeModal">Alterar Regime</h5>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
            <div class="form-group col-lg-4 col-xs-4">
              <input type="hidden" name="id_empresa_modal" id="id_empresa_modal">
              <label>Selecione o Regime:</label>
              <?php 
              $valores_filtro = array(
                  'SIMPLES'=>'Simples Nacional ',
                  'LUCRO_REAL'=>'Lucro Real',
                  'LUCRO_PRESUMIDO'=>'Lucro Presumido'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'name'=>'tipo_regime', 'id'=>'tipo_regime'), $valores_filtro, $filtro); 
              ?>
            </div>

      </div>
      <div class="modal-footer">
          <button type="button" onclick="salva_regime()" class="btn btn-success">Salvar</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>

</div>

<!-- Modal das mensagens -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_mensagens">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagens DTE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div style="width: 200px;margin-left: 15px;" id="div_filter">
                    <p>
                        Situação: 
                        <select id="table-filter" class="form-control form-control-sm">
                            <option value="">Todas Mensagens</option>
                            <option value="Não Lida">Mensagens Não Lidas</option>
                        </select>
                    </p>
                </div>
                <table id="modal_table" class="table table-striped">                    
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Leitura da Mensagem -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ler_mensagem">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_mensagem"></h5>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body_mensagem">
            </div>
            <div class="modal-footer">
                <a id="link_pdf_mensagem" href="#" type="button" class="btn btn-success" target="_blank"><i class="ti-printer"></i>&nbsp;Imprimir</a>
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<style type="text/css">
    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }

    function consultaDTE(id){
    $("#btn-submit"+id).click();
    }

    function iniciaTour2(){
      var tour = new Tour({
        storage: false,
        steps: [
            {
                element: "#passo10",
                title: "Botão de Cadastro",
                content: "Este botão permite adicionar registros ao sistema, ex.: Empresa, Colaborador, Contador, Documentos, etc.",
                placement: 'auto',
                backdrop: true
            },

            {
                element: "#passo11",
                title: "Registros",
                content: "Neste grid é possível ver diversas informações sobre os registros listados em tela, podendo organizar em ordem crescente ou decrescente clicando apenas na coluna desejada.",
                placement: 'top',
                backdrop: true
            },

            {
                element: "#passo12",
                title: "Ações do Registro",
                content: "Possibilitam ao usuário Editar, Visualizar, Remover ou Consultar dados da empresa selecionada, além de acesso facilitado ao DT-E e portal do simples nacional (necessário login e senha cadastrados para dt-e e portal do SN).",
                placement: 'top',
                backdrop: true
            },

            {         
            title: "Caixa de Email",
            content: "A caixa de email permite baixar as mensagens DTE.",
              onNext: function() {
                window.location = "<?php echo base_url();?>caixadeemail/listar/1";
              }
            }
        ]
        });

        tour.init();
        tour.start();
    }
</script>

<?php if(isset($fromTour)){
  echo '<script type="text/javascript">',
        '$(document).ready(function() { iniciaTour2(); });',
        '</script>';
;} ?>

<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        // lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
            "lengthMenu": "_MENU_  resultados por página",
        "zeroRecords": "Nenhum registro encontrado",
          "info": "Mostando página _PAGE_ de _PAGES_",
          "infoEmpty": "Nenhum registo",
          "infoFiltered": "(filtrado do total de _MAX_ registros)",
            "search": "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
          },
          "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
          }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );


});
</script>

<script type="text/javascript">  
    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("A empresa foi cadastrada com sucesso!", "Cadastrada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("As informações da empresa foram editadas com sucesso!", "Editada!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
    toastr["success"]("A empresa foi excluída com sucesso.", "Excluída!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["warning"]("Ocorreu um erro, tente novamente!", "Erro!");
    <?php } ?>
</script>

 <!-- Funções JavaScript -->
<script type="text/javascript">

    function alterar_regime(id){
      $("#id_empresa_modal").val(id);
      $('#modalRegime').modal('show');
    }

    function salva_regime(){
      var id = $("#id_empresa_modal").val();
      var regime = $("#tipo_regime").val();

      $.post("<?php echo base_url();?>empresa/atualizar_regime",
      {
          id:id,
          regime: regime
      },
      function(data){
        if(data == 1){
          window.location = global_url;
        }
      });
    }


    function imprimir(){
      $("#tableExport").printThis();
    }

    function excel(){
      window.open('data:application/vnd.ms-excel,' +  encodeURIComponent($('#tabela_excel').html()));
    }
</script>