<style type="text/css">
  .footer{
    margin-left: 0px !important;
  }
</style>
<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Tutoriais<span></span></h2>
        </div>
        
      </div>
    </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-9 xl-60">
        <div class="row">

          <ul class="list" id="myUL">
          <?php if($resultado){ ?>

            <?php foreach ($resultado as $e) { ?>
            <li>
              <div class="col-xl-12">
                <div class="card">
                  <div class="blog-box blog-list row">
                    <div class="col-sm-5"><img class="img-fluid sm-100-w" alt=""><iframe class="b" src="<?php echo $e->link; ?>"  frameborder="0" allow="autoplay; encrypted-media" allowfullscreen height="190px" style="padding-top: 10px;" ></iframe></img></div>
                    <div class="col-sm-7">
                      <div class="blog-details">
                        <div class="blog-date digits"><span><?php echo $e->dia; ?></span> <?php echo $e->mes; ?> <?php echo $e->ano; ?></div>
                        <h6 class="name"><?php echo $e->titulo; ?></h6>
                        <div class="blog-bottom-content">
                          <ul class="blog-social">
                            <li>por: <?php echo $e->autor; ?></li>
                            <li class="digits"><?php echo $e->tempo; ?></li>
                          </ul>
                          <hr>
                          <p class="mt-0"><?php echo $e->descricao; ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <?php } ?>
          <?php } ?>
          </ul>

        </div>
      </div>

      
      <div class="col-xl-3 xl-40">
        <div class="default-according style-1 faq-accordion job-accordion" id="accordionoc">
          <?php echo form_open_multipart("tutorial/listar/", array('role'=>'form', 'tag'=>'novalidate')); ?>
          <div class="row">
            <div class="col-xl-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="mb-0">
                    <button class="btn btn-link pl-0" data-toggle="collapse" data-target="#collapseicon" aria-expanded="true" aria-controls="collapseicon">Filtros</button>
                  </h5>
                </div>
                <div class="collapse show" id="collapseicon" aria-labelledby="collapseicon" data-parent="#accordion">
                  <div class="card-body filter-cards-view animate-chk">
                    <div class="job-filter">
                      <div class="faq-form">
                        <input class="form-control" type="text" placeholder="Buscar.." id="myInput" onkeyup="buscar_tutoriais()"><i class="search-icon" data-feather="search"></i>
                      </div>
                    </div>
                    <div class="checkbox-animated">
                      <div class="learning-header"><span class="f-w-600">Categorias</span></div>
                      <label class="d-block" for="chk-ani">
                        <input name="filtro_categoria[]" <?php if (in_array("MENSAGENS", $categoria)) { echo "checked"; } ?> class="checkbox_animated" id="chk-ani" type="checkbox" value="MENSAGENS">Mensagens
                      </label>
                      <label class="d-block" for="chk-ani0">
                        <input name="filtro_categoria[]" <?php if (in_array("ESTADUAL", $categoria)) { echo "checked"; } ?> class="checkbox_animated" id="chk-ani0" type="checkbox" value="ESTADUAL">Estadual
                      </label>
                      <label class="d-block" for="chk-ani1">
                        <input name="filtro_categoria[]" <?php if (in_array("FEDERAL", $categoria)) { echo "checked"; } ?> class="checkbox_animated" id="chk-ani1" type="checkbox" value="FEDERAL">Federal
                      </label>
                      <label class="d-block" for="chk-ani2">
                        <input name="filtro_categoria[]" <?php if (in_array("CERTIDAO", $categoria)) { echo "checked"; } ?> class="checkbox_animated" id="chk-ani2" type="checkbox" value="CERTIDAO">Certidões
                      </label>
                      <label class="d-block" for="chk-ani2">
                        <input name="filtro_categoria[]" <?php if (in_array("PROCESSOS", $categoria)) { echo "checked"; } ?> class="checkbox_animated" id="chk-ani2" type="checkbox" value="PROCESSOS">Processos
                      </label>
                      <label class="d-block" for="chk-ani2">
                        <input name="filtro_categoria[]" <?php if (in_array("PARCELAMENTO", $categoria)) { echo "checked"; } ?> class="checkbox_animated" id="chk-ani2" type="checkbox" value="PARCELAMENTO">Parcelamento
                      </label>
                      <label class="d-block" for="chk-ani2">
                        <input name="filtro_categoria[]" <?php if (in_array("CADASTROS", $categoria)) { echo "checked"; } ?> class="checkbox_animated" id="chk-ani2" type="checkbox" value="CADASTROS">Cadastros
                      </label>
                    </div>

                    
                    <!-- <div class="checkbox-animated mt-0">
                      <div class="learning-header"><span class="f-w-600">Status</span></div>
                      <label class="d-block" for="chk-ani4">
                        <input name="filtro_lida[]" <?php if (in_array("0", $lida)) { echo "checked"; } ?> value="0" class="checkbox_animated" id="chk-ani4" type="checkbox">                            Não Visto
                      </label>
                      <label class="d-block" for="chk-ani5">
                        <input name="filtro_lida[]" <?php if (in_array("1", $lida)) { echo "checked"; } ?> value="1" class="checkbox_animated" id="chk-ani5" type="checkbox">                            Visto
                      </label>
                    </div> -->
                    <button class="btn btn-primary text-center" type="submit">Filtrar</button>
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
          <?php echo form_close(); ?>
        </div>

        <div class="default-according style-1">
         
          <div class="row">
            <div class="col-xl-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="mb-0">
                    <button class="btn btn-link pl-0"><i class="fa fa-book"> Documentos</i></button>
                  </h5>
                </div>
                <div class="collapse show" id="collapseicon" aria-labelledby="collapseicon" data-parent="#accordion">
                  <div class="card-body filter-cards-view animate-chk">
                    <div class="job-filter">
                      <div class="faq-form">
                      </div>
                    </div>
                    <div class="checkbox-animated">
                      <label class="d-block" for="chk-ani">
                        <a href="<?php echo base_url("assets/Tutorial-VERI-Cadastro-Certificado-Procuração.pdf"); ?>" download><i class="fa fa-book"> Cadastro da Procuração do Contador </i></a>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script type="text/javascript">
  function buscar_tutoriais() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByClassName("name")[0];
      if(a != undefined){
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          li[i].style.display = "";
        } else {
          li[i].style.display = "none";
        }
      }
      
    }
  }

  
</script>