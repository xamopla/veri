<?php  defined('BASEPATH') OR exit('No direct script access allowed');
$this->session->sess_destroy();

redirect('login', 'refresh');
