<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h4 class="page-title m-b-0">Contador Não Vinculado</h4>
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'consultageral'=>'Todas',
                  'credenciadas'=>'Credenciadas',
                  'credenciadasativas'=>'Credenciadas Ativas',
                  //'credenciadasbaixadas'=>'Credenciadas Baixadas',
                  'descredenciadas'=>'Descredenciadas',
                  'descredenciadasativas'=>'Descredenciadas Ativas',
                  'descredenciadasbaixadas'=>'Descredenciadas Baixadas',
                  'contadornaovinculado'=>'Contador Não Vinculado (Exceto MEI)',
                  'empresasregulares'=>'Empresas Regulares',
                  'empresasativas'=>'Empresas Ativas',
                  'empresasnaoativas'=>'Empresas Inativas',
                  'dteativo'=>'DTE - Ativo',
                  'dteinexistente'=>'DTE - Inexistente / Inativo',
                  'empresassemie'=>'Empresas sem Inscrição Estadual',
                  'empresasinaptas'=>'Empresas Inaptas',
                  'consultasemsenha'=>'Empresas Sem Senhas',
                  'empresasmei'=>'MEI',
                  'empresassimples'=>'Simples Nacional'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br> <br>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="example" data-toggle="datatables" >
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th width="1%">#</th>
                          <th style="font-size: 12px;">Razão Social</th>
                          <th style="font-size: 12px;">CNPJ</th>
                          <th style="font-size: 12px;">Inscrição Estadual</th>
                          <th style="font-size: 12px;">Contador</th>
                          <th style="font-size: 12px;">CRC</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $cont = 1;
                      if ($consulta){ 
                        foreach ($consulta as $c){?>
                        <tr>
                              <td style="font-size: 12px;"><strong><?php echo $cont; ?></strong></td>
                              <td style="text-align: left; font-size: 12px;"><?php echo $c->razao_social; ?></td>
                              <td style="font-size: 12px;"><?php echo $c->cnpj_completo; ?></td>
                              <td style="font-size: 12px;"><span data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Condição: <?php echo $c->condicao; ?>" data-content="Forma de Pagamento: <?php echo $c->forma_pagamento; ?>"><?php echo $c->inscricao_estadual_completo; ?></span></td>

                              <td style="font-size: 12px;"><?php echo $c->nome_contador; ?></td>
                              <td style="font-size: 12px;"><?php echo $c->crc_contador; ?></td>
                        </tr>
                    <?php } 
                    }
                    ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<!-- Modal das mensagens -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_mensagens">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagens DTE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div style="width: 200px;margin-left: 15px;" id="div_filter">
                    <p>
                        Situação: 
                        <select id="table-filter" class="form-control form-control-sm">
                            <option value="">Todas Mensagens</option>
                            <option value="Não Lida">Mensagens Não Lidas</option>
                        </select>
                    </p>
                </div>
                <table id="modal_table" class="table table-striped">                    
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Leitura da Mensagem -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ler_mensagem">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_mensagem"></h5>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body_mensagem">
            </div>
            <div class="modal-footer">
                <a id="link_pdf_mensagem" href="#" type="button" class="btn btn-secondary" target="_blank"><i class="ti-printer"></i>&nbsp;Imprimir</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('empresa'); ?>/"+filtro+"");
  });
});
</script>
<!-- Funções JavaScript -->
<script type="text/javascript">

    function get_mensagens_dte($login, $senha){
        $(document).ready(function() {
            $(document).ready(function() {
                swal({ 
                    title: "Buscando...",
                    text: 'Buscando Mensagens DTE da Empresa na Sefaz.\nPor favor, aguardar alguns segundos...',
                    type: "info" ,
                    confirmButtonText: "Cancelar",
                    confirmButtonColor: "#fff"
                },
                function(isConfirm){
                    if(isConfirm){
                        request.abort();
                    }
                });
            });
        });
        var url = "<?= base_url() ?>/Dte/get_mensagens/"+$login+"/"+$senha;
        var request = $.get(url, function(resultado){             
            response = JSON.parse(resultado);
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#modal_table').html(response['table']);
                $('#modal_mensagens').modal('show');
                $(document).ready( function () {
                    $.fn.dataTable.moment( 'DD/MM/YYYY HH:mm:ss' ); 
                    $('#modal_table').dataTable().fnDestroy();
                    var table = $('#modal_table').DataTable({
                        lengthMenu: [5, 10, 20, 50, 100],
                        "order": [[ 3, "desc" ]],
                        language: {
                            "sEmptyTable": "Nenhum registro encontrado",
                            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sInfoThousands": ".",
                            "sLengthMenu": "_MENU_ resultados por página",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sZeroRecords": "Nenhum registro encontrado",
                            "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sNext": "Próximo",
                                "sPrevious": "Anterior",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                            },
                            "oAria": {
                                "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                            },
                            "select": {
                                "rows": {
                                    "_": "Selecionado %d linhas",
                                    "0": "Nenhuma linha selecionada",
                                    "1": "Selecionado 1 linha"
                                }
                            }
                        }
                    });
                    $("#div_filter select").val("Não Lida");
                    table.search('Não Lida').draw();

                    $('#table-filter').on('change', function(){
                        table.search(this.value).draw();   
                    });
                });    
            }        
        });
    }

    function ler_mensagem($id, $login, $senha){        
        $('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/Dte/ler_mensagem/"+$login+"/"+$senha+"/"+encodeURIComponent($id);
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#modal_mensagens').modal('show');
                response['body'] = response['body'].replace(/(?:\\[rn])+/g, "<br>");
                response['body'] = response['body'].replace('<br>            ', '');
                $('#title_mensagem').html(response['title']);
                $('#body_mensagem').html(response['body']);
                $('#modal_ler_mensagem').modal('show');

                $('#link_pdf_mensagem').attr('href', "<?= base_url() ?>/Dte/get_pdf/"+$login+"/"+$senha+"/"+encodeURIComponent($id));
            } 
        });
    }
</script>

<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );

});
</script>