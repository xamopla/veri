<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  
    var global_url_2 = '';  

    function preencher_url(url){
    global_url = url;
    }

    function preencher_url_2(url_2){
    global_url_2 = url_2;
    }
</script> 

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Empresas Intimadas</h5>
          </div>
          <div class="card-body">
            <br>
            <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th width="1%">#</th>
                      <th style="font-size: 12px;">Razão Social</th>
                      <th style="font-size: 12px;">CNPJ</th>
                      <th style="font-size: 12px;">Inscrição Estadual</th>
                      <th style="font-size: 12px;">Situação</th>
                      <th style="font-size: 12px;">Data da Intimação</th> 
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $cont = 1;
                      if ($consulta){ 
                        foreach ($consulta as $c){?>
                        <tr>
                              <td><strong><?php echo $cont; ?></strong></td>
                              <td style="text-align: left;"><?php echo $c->razao_social; ?></td>
                              <td><?php echo $c->cnpj_completo; ?></td>
                              <td style="text-align: center;"><span data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Condição: <?php echo $c->condicao; ?>" data-content="Forma de Pagamento: <?php echo $c->forma_pagamento; ?>"><?php echo $c->inscricao_estadual_completo; ?></span></td>

                              <td style="text-align: center;">
                              <div class="badge badge-danger"><?php
                                echo "<span class='label label-warning'  data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo da Intimação' data-content='$c->motivo_situacao_cadastral'>$c->situacao_cadastral</span>";
                              
                              ?></div>
                              </td>  
                              <td style="text-align: center;"><?php echo "<span class='label label-warning'  data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Tempo para Descredenciar' data-content=''>$c->data_intimacao</span>"; ?></td> 

                              <td style="width: 2%; font-size: 12px;">
                                <?php  
                                  if(!empty($c->login_sefaz) && !empty($c->senha_sefaz)){ ?>

                                    <a href="<?php echo base_url("consulta_dte/consultar/$c->id"); ?>" data-toggle="tooltip" data-placement="top" title="Consultar DTE" target="_blank" class="btn btn-default b"><i class="ti-search" aria-hidden="true"></i></a> 

                                    <a onclick="get_mensagens_dte('<?php echo $c->login_sefaz ?>','<?php echo $c->senha_sefaz ?>');" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens do DTE" target="_blank" class="btn btn-default b"><i class="ti-email" aria-hidden="true"></i></a> 
                                  
                                <?php } ?> 
                              </td>                       
                        </tr>
                    <?php } 
                    }
                    ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->
