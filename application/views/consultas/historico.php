<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>
<div class="main-content">
  <section class="section">
    <ul class="breadcrumb breadcrumb-style ">
      <li class="breadcrumb-item">
        <h4 class="page-title m-b-0">Histórico de Atualizações</h4>
      </li>
    </ul>
    
    <div class="section-body">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body">
              <div class="ibox">
                <div class="ibox-body">
                    <h5 class="font-strong mb-4">IE: <?php  echo $empresa->inscricao_estadual_completo; ?> | CNPJ: <?php  echo $empresa->cnpj_completo; ?></h5>
                    <div class="table-responsive row">
                        <table class="table table-bordered table-hover" id="example" data-toggle="datatables">
                            <thead class="thead-default thead-lg">
                                <tr>
                                  <th>Data / Hora</th>
                                  <th>Status</th>
                                  <th>Motivo Status</th>
                                  <th>Situação</th>
                                  <th>Motivo Situação</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            
                            if ($consulta){
                                foreach ($consulta as $c){?>
                                <tr>
                                
                                  <td><?php echo date("d/m/Y H:i:s", strtotime($c->data_atualizacao)); ?></td>
                                  
                                  <td>
                                  <?php
                                  
                                  if ($c->situacao_cadastral == 'ATIVO'){
                                    echo "<span class='badge badge-success badge-shadow' data-container='body' style='background-color:#2ecc71!important;'>$c->situacao_cadastral</span>";
                                  } else if ($c->situacao_cadastral == 'BAIXADO'){
                                    echo "<span class='badge badge-danger badge-shadow' data-container='body'>$c->situacao_cadastral</span>";
                                  } else {
                                    echo "<span class='badge badge-warning badge-shadow'>$c->situacao_cadastral</span>";
                                  }
                                  
                                  ?>
                                  </td>
                                  
                                  <td><?php echo $c->motivo_situacao_cadastral; ?></td>
                                  
                                  <td>
                                  <?php 
                                  if ($c->situacao == 'Descredenciado'){
                                        echo "<span class='badge badge-danger badge-shadow'>$c->situacao</span>";
                                  } else {
                                        echo "<span class='badge badge-success badge-shadow' style='background-color:#2ecc71!important;'>$c->situacao</span>";
                                  }
                                  ?>
                                  </td>
                                  
                                  <td><?php echo $c->motivo; ?></td>
                                  
                                </tr>
                            <?php 
                                }
                            } 
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
</div>

<script>
$(document).ready(function() {
$('#example').DataTable({
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });


});
</script>

