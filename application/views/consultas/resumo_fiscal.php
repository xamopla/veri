<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
z
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 600px;
        max-width : 600px;
    }

    div.dataTables_wrapper {
        position: relative;
         font-family: unset; 
    }

    .table-responsive {
        display: block;
        width: 100%;
        overflow-x: visible;
        -webkit-overflow-scrolling: touch;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }

    .badge{
      /*width: 60px !important;*/
    }


    #header-fixed {
      position: fixed;
      top: 0px; display:none;
      background-color:white;
  }

  th {
    background: white !important;
    position: sticky !important;
    top: 0 !important; /* Don't forget this, required for the stickiness */
    box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4) !important;
  }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2>Resumo Fiscal</h2>
          </div>
          <div class="card-body">
            <div class="form-group pull-right">
            <label id="passo20">Filtro:</label>
            <?php
              $valores_filtro = array(
                  'consultageral'=>'Todas',
                  'socioirregular'=>'Sócio Irregular',
                  'omissodma'=>'Omisso DMA',
                  'omissoefd'=>'Omisso EFD',
                  'parcelamentoatraso'=>'Parcelamento em atraso ou interrompido',
                  'divergencia1'=>'Divergência entre ICMS normal informado na DMA e o recolhido',
                  'divergencia2'=>'Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido',
                  'divergencia3'=>'Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido',
                  'os1'=>'O.S. de Monitoramento ativa',
                  'os2'=>'O.S. de Auditoria ativa',
                  'pafativo'=>'PAFs ativos',
                  'simples_nacional'=>'Simples Nacional'

              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro);
            ?>
            </div><br>
            <br>
            <div class="table-responsive">
              <table class="table table-striped" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                  <tr>
                      <th style="font-size: 12px;" id="passo17">Razão Social</th>
                      <th style="font-size: 12px;">CNPJ</th>
                      <th style="font-size: 12px;">Sócio Irregular</th>
                      <th style="font-size: 12px;">Omisso DMA</th>
                      <th style="font-size: 12px;">Omisso EFD</th>
                      <th style="font-size: 12px;">Parc. em Atraso</th>
                      <th style="font-size: 12px;" id="passo18">Div. ICMS normal e o recolhido</th>
                      <th style="font-size: 12px;">Div. ICMS Subs. Trib. por Antec. e o recolhido</th>
                      <th style="font-size: 12px;">Div. ICMS Subs. Trib. por Retenção e o recolhido</th>
                      <th style="font-size: 12px;">O.S. de Monitoramento ativa</th>
                      <th style="font-size: 12px;">O.S. de Auditoria ativa</th>                      
                      <th  id="passo19">PAFs com ciclo de vida ativo</th>
                      <th style="font-size: 12px;" >É Beneficiário do Decreto 7.799/00?</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $cont = 1;
                  if ($consulta){
                      foreach ($consulta as $c){?>
                          <?php
                          if($c->razaoSocial != ""){
                              ?>
                              <tr>
                                  <td style="text-align: left; font-size: 12px;">

                                      <a target="_blank" data-toggle="tooltip" data-placement="top" title="Consultar DTE" href="<?php echo base_url("consulta_dte/consultar/$c->id"); ?>"><?php echo $c->razaoSocial; ?></a>
                                  </td>

                                  <td style="font-size: 12px; text-align: center;"><span data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Colaboradores Responsáveis' data-content='<?php echo $c->colaboradores; ?>'><?php echo $c->cnpjcpf; ?></span></td>

                                  <td style=" text-align: center;"><?php
                                      if ($c->pergunta1 == '  Não  '){
                                          echo "<span class='badge badge-pill badge-success' style=' '
data-toggle='popover' data-trigger='hover' data-placement='top' title='Sócio Irregular'>NÃO</span>";

                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Sócio Irregular' onclick='openModalDetalhesSocio($c->inscricaoEstadual)' >SIM</span>";
                                      }
                                      ?></td>
                                  <td style=" text-align: center;"><?php
                                      if ($c->pergunta2 == "   Não  "){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='Omisso DMA' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Omisso DMA' onclick='openModalDetalhesDMA($c->inscricaoEstadual)' >SIM</span>";
                                      }
                                      ?></td>
                                  <td style=" text-align: center;"><?php
                                      if ($c->pergunta3 == "    Não  "){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='Omisso EFD' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Omisso EFD' onclick='openModalDetalhesEFD($c->inscricaoEstadual)' >SIM</span>";
                                      }
                                      ?></td>
                                  <td style=" text-align: center;"><?php
                                      if (strpos($c->pergunta4, 'Não') == true){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='Parcelamento em atraso ou interrompido' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Parcelamento em atraso ou interrompido' onclick='openModalDetalhesParcelamentoAtraso($c->inscricaoEstadual)' >SIM</span>";
                                      }
                                      ?></td>
                                  <td style=" text-align: center;"><?php
                                      if (trim($c->pergunta7) == "Não" || trim($c->pergunta7) == "|||" || trim($c->pergunta7) == ""){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='Divergência entre ICMS normal informado na DMA e o recolhido'>NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Divergência entre ICMS normal informado na DMA e o recolhido' onclick='openModalDetalhes($c->inscricaoEstadual,7)' >SIM</span>";
                                      }
                                      ?></td>

                                  <td style=" text-align: center;"><?php
                                      if (trim($c->pergunta8) == "Não" || trim($c->pergunta8) == "|||" || trim($c->pergunta8) == ""){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido' onclick='openModalDetalhes($c->inscricaoEstadual,8)' >SIM</span>";
                                      }
                                      ?></td>

                                  <td style=" text-align: center;"><?php
                                      if (trim($c->pergunta9) == "Não" || trim($c->pergunta9) == "|||" || trim($c->pergunta9) == ""){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido' onclick='openModalDetalhes($c->inscricaoEstadual,9)' >SIM</span>";
                                      }
                                      ?></td>

                                  <td style=" text-align: center;"><?php
                                      if ($c->pergunta18 == "    Não  "){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='O.S. de Monitoramento ativa' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='O.S. de Monitoramento ativa' onclick='openModalDetalhesOsMonitoramento($c->inscricaoEstadual)' >SIM</span>";
                                      }
                                      ?></td>

                                  <td style=" text-align: center;"><?php
                                      if ($c->pergunta19 == "    Não  "){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='O.S. de Auditoria ativa' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='O.S. de Auditoria ativa' onclick='openModalDetalhesOsAuditoria($c->inscricaoEstadual)' >SIM</span>";
                                      }
                                      ?></td>


                                  <td style=" text-align: center;"><?php
                                      if (trim($c->pergunta6) == "Não" || trim($c->pergunta6) == ""){
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='PAFs com ciclo de vida ativo' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-danger' style=' border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='PAFs com ciclo de vida ativo' onclick='openModalDetalhesPaf($c->inscricaoEstadual)' >SIM</span>";
                                      }
                                      ?></td>

                                  <td style=" text-align: center;"><?php
                                      if ($c->pergunta25 == "   Não  "){
                                          echo "<span class='badge badge-pill badge-info' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='É Beneficiário do Decreto 7.799/00?' >NÃO</span>";
                                      } else {
                                          echo "<span class='badge badge-pill badge-success' style=' ' data-toggle='popover' data-trigger='hover' data-placement='top' title='É Beneficiário do Decreto 7.799/00?' >SIM</span>";
                                      }
                                      ?></td>
                              </tr>
                              <?php
                          }
                          ?>
                          <?php
                      }
                  }
                  ?>

                  </tbody>
              </table>

          </div>

          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<form action="https://www.sefaz.ba.gov.br/scripts/cadastro/socio/result.asp" method="post" id="frm_socio" name="frm_socio" target="TheWindow">
    <input type="hidden" name="B3" value="CPF  ->" />
    <input type="hidden" name="CGC" value="" />
    <input type="hidden" name="CPF" value="" />
    <input type="hidden" name="r1" value="V1" />
</form>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }
</style>

<div class="modal fade" id="detalhe-modal">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title" id="title"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="flexbox mb-4">
                    <div class="flex-1 d-flex">
                        <div class="flex-1">
                            <div id="conteudo"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
    $(document).ready(function() {

    $('[data-toggle="popover"]').popover({
      boundary:'window',
      html: true
    });

    var table = $('#example').DataTable({
            "iDisplayLength": 50,
            lengthChange: false,
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'EXCEL',
                    exportOptions: {
                        modifier: {
                            page: 'all'
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
                {
                    extend: 'colvis',
                    text: 'COLUNAS'
                }
            ],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "bDestroy": true
        });

        table.search('<?php echo $razao_social_filtro; ?>').draw();
        table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
    //.buttons().container()
            //.appendTo( '#example_wrapper .col-md-6:eq(0)' );

        $('#filtro').change(function(){
            var filtro = $('#filtro option:selected').val();
            window.location.replace("<?php echo base_url('resumofiscal'); ?>/"+filtro+"");
        });


        var tableOffset = $("#example").offset().top;

        $(window).bind("scroll", function() {
            var offset = $(this).scrollTop();
            
            if (offset >= tableOffset) {
              $("th").css("padding-top", "100px");
            }
            else if (offset < tableOffset) {
                $("th").css("padding-top", "0px");
            }
        });

    });


    function consultaDTE(id){
        $("#btn-submit"+id).click();
        //window.close();
        setTimeout(function() {
            //window.close();
            $("#btn-submit"+id+"Enter").click();
        }, 2000);
    }

    function openModalDetalhes(inscricaoEstadual, numeroPergunta){
        $.post("<?php echo base_url();?>resumofiscal/findPergunta",
            {
                inscricaoEstadual:inscricaoEstadual,
                pergunta:numeroPergunta
            },
            function(data){

                if(numeroPergunta == 7){
                    $("#title").html("Divergência entre ICMS normal informado na DMA e o recolhido");
                }else if(numeroPergunta == 8){
                    $("#title").html("Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido");
                }else{
                    $("#title").html("Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido");
                }
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }


    function openModalDetalhesPaf(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findPaf",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("PAFs com ciclo de vida ativo");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesSocio(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findSocioIrregular",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Sócio(s) Irregular(es)");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesEFD(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findEFD",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Omisso EFD");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesDMA(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findDMA",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Omisso DMA");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesOsMonitoramento(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findOsMonitoramento",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("O.S de Monitoramento Ativa");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesOsAuditoria(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findOsAuditoria",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("O.S de Auditoria Ativa");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesParcelamentoAtraso(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findParcelamentoEmAtraso",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Parcelamento em Atraso");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

</script>

<!--script abrir socio irregular-->
<script type="text/javascript">
    function openWindowWithPost(cpf) {
        var f = document.getElementById('frm_socio');
        f.CPF.value = cpf;
        window.open('', 'TheWindow');
        f.submit();
    }


</script>