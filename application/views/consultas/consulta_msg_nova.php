<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }

    .swal2-confirm{
      background-color: #24a0e9 !important;
      border-left-color: #24a0e9 !important;
      border-right-color: #24a0e9 !important;
    }

    .badge-info, .label-info {
      background-color: #2cc4cb;
    }
    .badge, .label {
        padding: 3px 6px;
        line-height: inherit;
        font-size: 11px;
        font-weight: 600;
        color: #fff;
        -webkit-border-radius: 2px;
        border-radius: 2px;
    }
    .icon-envelope{
      display: none !important;
    }
    .icon-zoom-in{
      display: none !important;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h4 class="page-title m-b-0">Mensagens DTE</h4>
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'TODAS'=>'Todas',
                  'LIDAS'=>'Lidas',
                  'NAO_LIDAS'=>'Não Lidas'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro', 'onchange' => 'loadAjax()'), $valores_filtro, $filtro); 
              ?>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table table-striped" id="example">
                <thead>
                  <tr>
                    <th>Razão Social</th>
                    <th >CNPJ</th>
                    <th >Inscrição Estadual</th>
                    <th>Data Da Mensagem</th>
                    <th>Lida</th>
                    <th width="15%"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }

</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
  loadAjax();
});


function loadAjax(){
  var valor_filtro = $("#filtro").val();

  $.ajax({
      type: "POST",
      url: '<?php echo base_url('caixadeemail/listar_msgs_dte'); ?>',
      data: {
          filtro: valor_filtro
      },
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          if(data == "") {
              var array = [];
              popularDataTable(array);
              jQuery('[data-toggle="tooltip"]').tooltip();                
          }else{
            var array = json2array(data);
            popularDataTable(array);
            jQuery('[data-toggle="tooltip"]').tooltip();

            
          }
      }
  });
}

function popularDataTable(json){
  $.fn.dataTable.moment('DD/MM/YYYY');
    var table = $('#example').DataTable({
        "order": [[ 3, "desc" ]],
        "iDisplayLength": 50,dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
}

function json2array(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];
    var id = '';
    var id_contabilidade = '';
    var cnpjSemFormat;

    for(var i in data){
      array = [];
      id = data[i].id;
      id_contabilidade = data[i].id_contabilidade;

      if (data[i].razao_social != "" && data[i].id_empresa != 0){
        array.push('<td>'+data[i].razao_social+'</td>'); 
        array.push('<td>'+data[i].cnpj+'</td>'); 
        array.push('<td>'+data[i].inscricao_estadual_completo+'</td>');
        array.push('<td>'+data[i].dataemail+'</td>');
      } else {
        array.push('<td>Empresa não Cadastrada'+data[i].razao_social+'</td>'); 
        cnpjSemFormat = apenasNumeros(data[i].cnpj);
        array.push('<td>'+cnpjSemFormat+'</td>'); 
        array.push('<td></td>');
        array.push('<td>'+data[i].dataemail+'</td>');
      }

      if(data[i].status == 1){
        array.push("<span class='badge badge-pill badge-danger' style=' border: white;'>NÃO</span>");
        array.push("<a href='https://efisc.sefaz.ba.gov.br/efiscalizacao/jsp/login/login.jsf' target='_blank' data-toggle='tooltip' data-placement='top' title='Ver Mensagem' class='btn btn-default b' style='color: #1b6dc1;'><i class='material-icons' aria-hidden='true'>email</i></a><a href='<?php echo base_url('caixadeemail/marcar_como_lido_aux/');?>"+data[i].id+"/"+data[i].id_contabilidade+"' data-toggle='tooltip' data-placement='top' title='Marcar como Lido no sistema' class='btn btn-default b'><i class='fa fa-eye' aria-hidden='true'></i></a>");

        // array.push("<a href='<?php echo base_url('caixadeemail/marcar_como_lido_aux/');?>"+data[i].id+"/"+data[i].id_contabilidade+"' data-toggle='tooltip' data-placement='top' title='Marcar como Lido no sistema' class='btn btn-default b'><i class='fa fa-eye' aria-hidden='true'></i></a>");
      }else{
        array.push("<span class='badge badge-pill badge-success' style=' border: white;'>SIM</span>");
        array.push("<a href='https://efisc.sefaz.ba.gov.br/efiscalizacao/jsp/login/login.jsf' target='_blank' data-toggle='tooltip' data-placement='top' title='Ver Mensagem' class='btn btn-default b' style='color: #1b6dc1;'><i class='material-icons' aria-hidden='true'>email</i></a>");

        // array.push("<a class='btn btn-default b' data-toggle='modal' data-target='#modalexcluir' onclick='excluir(&quot;"+data[i].titulo+"&quot;, &quot;<?php echo base_url('caixadeemail/excluir/');?>"+id+"/"+id_contabilidade+"&quot;)'><i data-toggle='tooltip' data-placement='top' title='Excluir Email' class='fa fa-trash-o' aria-hidden='true'></i></a>");
      }
      

      arrayMultiple.push(array);
    }
    
    return arrayMultiple;
}

function apenasNumeros(string){
    var numsStr = string.replace(/[^0-9]/g,'');
    return numsStr;
}
</script>


