<div class="main-content">
  <section class="section">
    <ul class="breadcrumb breadcrumb-style ">
      <li class="breadcrumb-item">
        <h4 class="page-title m-b-0">Consulta de Inscrição Estadual do Contribuinte</h4>
      </li>
    </ul>
    
    <div class="section-body">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body">
              <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
                <div class="page-content fade-in-up">
                    <div class="ibox">

                    <?php
                    if ($this->session->flashdata('msg_conteudo')){
                        echo "<div class='alert alert-{$this->session->flashdata('msg_tipo')} alert-dismissible fade show' role='alert'>
                        <button class='close' data-dismiss='alert' aria-label='Close'></button>
                        <strong>{$this->session->flashdata('msg_titulo')} </strong>{$this->session->flashdata('msg_conteudo')}
                        </div>";
                    }
                    ?>

                            <div class="ibox-head">
                                <div class="ibox-title"></div>
                            </div>
                            <div class="ibox-body">
                                <?php echo form_open('empresa/consultar', array('role'=>'form')); ?>
                                <div class="form-group col-md-4 mx-auto">
                                  <label>Inscrição Estadual</label>
                                  <?php echo form_input(array('name'=>'num_inscricao', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual', 'style'=>'border-color: #5c6bc0;'), '', 'required'); ?>
                                </div>
                            </div>
                            <div class="ibox-footer text-center">
                                <?php echo form_submit(array('name'=>'btn_pesquisar', 'class'=>'btn btn-primary mr-2'), 'Localizar'); ?>
                            </div>
                            <?php echo form_close(); ?>

                            </div>

                        </div>
                            <!-- APRESENTAÇÃO DO RESULTADO -->

                        <div class="ibox">
                            <?php if ($consulta){ ?>
                              <div class="row">
                              
                                  <div class="col-lg-12">
                                      <!-- Widget: user widget style 1 -->
                                      <div class="card has-cup card-air centered mb-4">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="card-cup bg-gradient-blue" style="color: #fff;">
                                        <div class="card-body">
                                          <h3 class="widget-user-username"><?php echo $consulta['razao_social']; ?></h3>
                                          <h5 class="widget-user-desc" style="font-size: 15px;"><?php echo $consulta['nome_fantasia']; ?></h5>
                                          <h5 class="widget-user-desc" style="font-size: 15px;"><?php echo $consulta['ie']; ?></h5>
                                          <h5 class="widget-user-desc" style="font-size: 15px;"><?php echo $consulta['cnpj']; ?></h5>              
                                        </div>                                    
                                          <div class="card-avatar text-center mb-4">
                                            <div class="img-circle"><i class="fa fa-building-o fa-4x" aria-hidden="true"></i></div>
                                          </div>  
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-4 border-right">
                                          <div class="card-body">
                                            <h5 class="widget-user-username"><b>SITUAÇÃO CADASTRAL</b></h5>
                                            <span class="description-text"><?php echo $consulta['situacao_cadastral']; ?></span>
                                          </div>
                                        </div>

                                        <div class="col-sm-4 border-right  text-center">
                                          <div class="card-body">
                                            <h5 class="widget-user-username"><b>SITUAÇÃO</b></h5>
                                            <span class="description-text"><?php 
                                            if ($consulta['credenciado'] == 'Descredenciado'){
                                                echo "<span class='label label-danger'>".$consulta['credenciado']."</span>";
                                            } else {
                                                echo "<span class='label label-success' style='background-color:#2ecc71!important;'>".$consulta['credenciado']."</span>";
                                            }
                                            ?>
                                            </span>
                                          </div>
                                        </div>

                                        <div class="col-sm-4">
                                          <div class="card-body">
                                            <h5 class="widget-user-username text-center"><b>MOTIVO</b></h5>
                                            <span class="description-text"><?php echo $consulta['motivo']; ?></span>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  <!-- /.widget-user -->
                                </div>
                          <?php } ?>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
</div>

