<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<style type="text/css">
  .buttons-excel{
    width: 100px;
  }
  .swal2-styled{
    background-color: #dc2727 !important;
 } 
  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }

    .swal2-confirm{
      background-color: #24a0e9 !important;
      border-left-color: #24a0e9 !important;
      border-right-color: #24a0e9 !important;
    }

    .badge-info, .label-info {
      background-color: #468847;
    }

    .badge-alert{
      background-color: #f89406;
    }

    .badge-normal{
      background-color: #3a6eb0;
    }
    .badge, .label {
        padding: 3px 6px;
        line-height: inherit;
        font-size: 11px;
        font-weight: 600;
        color: #fff;
        -webkit-border-radius: 2px;
        border-radius: 2px;
    }
    .icon-envelope{
      display: none !important;
    }
    .icon-zoom-in{
      display: none !important;
    }

    .badge_dte {
        font-size: 10px;
        max-width: 165px;
        white-space: normal;
        overflow: hidden;
        text-overflow: ellipsis;
        padding-left: 9px;
      padding-right: 9px;
      -webkit-border-radius: 9px;
      -moz-border-radius: 9px;
      border-radius: 9px;

          display: inline-block;
    padding: 2px 4px;
    font-size: 11.844px;
    font-weight: bold;
    line-height: 14px;
    color: #ffffff;
    vertical-align: baseline;
    white-space: nowrap;
    text-shadow: 0 -1px 0 rgb(0 0 0 / 25%);
    background-color: #999999;

    }

</style>
<style type="text/css">
  
  .dte_space-top {
  overflow: hidden;
  margin: 15px 0;
}

.dte_title {
  padding: 20px 20px 15px;
  margin: 0;
  background: #f5f5f5;
}

.dte_title-caixa-entrada .dte_title-icon {
  color: #393939;
  padding: 0px 10px;
  border-left: 5px solid #51a351;
}

.dte_title-caixa-saida .dte_title-icon {
  color: #393939;
  padding: 0px 10px;
  border-left: 5px solid #0b87cb;
}

.dte_actions {
  padding: 8px 0px 15px 10px;
  background: #fdfdfd;
}

.dte_box {
  background: #fdfdfd;
  padding: 15px;
  border: 1px solid #c8c8c8;
  border-top: 0px;  
    border-radius: 3px;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
}

.dte_actions div {
  margin-right: 10px;
}

.dte_table {
  background: #ffffff;
}

.dte_actions-select-right {
  float: right;
  margin: 0 5px;
}

.dte_actions-select-left {
  float: left;
  margin: 0 5px;
}

.dte_actions .dte_actions-input {
  display: inline-block;
  height: 32px;
  padding: 4px 6px;
  margin-bottom: 10px;
  font-size: 14px;
  line-height: 20px;
  color: #555;
  vertical-align: middle;
}

.dte_box .rich-datascr {
  margin: 0;
}

.dte_box .rich-datascr-act {
  color: #222;
}

.dte_mensagem-lida {
  color: #393939;
}

.dte_mensagem-nao-lida {
  color: #393939;
}

.dte_legenda {
  padding: 5px 20px 20px 2px;
}

.dte_legenda h4 {
  border-top: 1px solid rgb(204, 204, 204);
  padding: 5px;
  margin: 0px 0px 5px;
}

.dte_legenda span {
  font-size: 13px;
  margin: 5px 5px 0px 5px;
}

.dte_legenda span i {
  font-size: 14px;
}

.dte_msg-warning {
    padding: 20px;
    text-align: center;
    border: 1px solid #BBB;
    background: #f5f5f5;
    margin: 0px 0px 10px;
    border-radius: 2px;
    font-weight: bold;
}

.dte_actions .dte_span-filtro {
  display: inline-block;
  vertical-align: middle;
  margin: 3px 5px;
  width: auto;
}

.dte_fieldset-box {
  padding: 0;
  background-color: #f1f1f1;
  border: 1px solid #BBB;
}

.dte_fieldset-box * {
  box-sizing: border-box;
}

.dte_fieldset-block {
  border: 0;
  padding: 10px;
  margin: 0;
  min-width: 0;
  overflow: auto;
  zoom: 1;
  border-bottom: 1px solid #BBB;
}

.dte_fieldset-block h3 {
  padding: 2px 0 12px;
  margin: 5px 0 10px;
  border-bottom: 1px solid #ddd;
}

.dte_input-date input {
  min-height: 30px;
}

.dte_input-checkbox input {
  margin-right: 10px;
  margin-top: 2px;
}

.dte_item-inativo, .dte_item-ativo {
  font-size: 22px;
  cursor: default;
}

.dte_item-ativo, .dte_item-ativo:hover {
  color: #138913;
}

.dte_item-inativo, .dte_item-inativo:hover {
  color: #BBB; /*#b10000;*/
}

.dte_btn-add-superior {
  float: right;
  margin-bottom: 10px;
  margin-top: -5px;
}

.dte_btn-close {
  color: #999;
}

.dte_btn-close:hover {
  color: #777;
}

.dte_row-border-bottom {
  border-bottom: 1px solid #ddd;
  margin-bottom: 10px;
}

.badge.dte_badge-sample {
  height: 20px;
  width: 20px;
  display: block;
  padding: 0;
  margin: 0 auto;
}

.dte_modal-large {
  width: 80%;
  margin-left: -40%;
  overflow: inherit !important;
}

.dte_modal-large .modal-body {
  overflow-y: auto;
}

.dte_box .badge, .dte_badge-ellipsis {
  font-size: 10px;
  max-width: 165px;
  white-space: normal;
  overflow: hidden;
  text-overflow: ellipsis;
}

.dte_tabs {
  margin: 10px 0px 0px;
  border-color: #c8c8c8;
}

.nav-tabs.dte_tabs>li>a {
  margin: 0 2px;
}

.nav-tabs.dte_tabs>.active>a, .nav-tabs.dte_tabs>.active>a:hover {
  background-color: #fdfdfd !important;
  border: solid 1px #c8c8c8;
  border-bottom: solid 1px #fdfdfd;
  font-size: 14px;
}

/** CAIXA DE LEITURA **/
.dte_modal-large h4 {
  margin: 10px 0px;
}

.dte_leitura-box * {
  box-sizing: border-box;
}

.dte_leitura-box .dte_leitura-box-linha:first-child {
  border-top: 1px solid #CCC;
}

.dte_leitura-box-linha {
  display: table;
  height: 100%;
  width: 100%;
  background: white;
  border: 1px solid #CCC;
  border-top: none;
}

.dte_leitura-box-linha-conteudo {
  display: table-row;
  width: 100%;
  min-height: 20px;
  line-height: 20px;
}

.dte_leitura-box-linha-conteudo .dte_leitura-box-coluna:first-child {
  border-left: none;
}

.dte_leitura-box-coluna {
  border-left: 1px solid #CCC;
  display: table-cell;
  padding: 5px 8px;
}

.dte_leitura-box-titulo {
  width: 16%;
  background: #F4F4F4;
  font-weight: bold;
}

.dte_leitura-box-conteudo {
  width: 34%;
}

.dte_leitura-box-conteudo-80 {
  width: 83%;
}

.dte_leitura-box-titulo-grande {
  width: 100%;
  background: #F4F4F4;
  font-weight: bold;
}

.dte_leitura-box-conteudo-grande {
  width: 100%;
  height: 150px;
}

.dte_leitura-box-greater {
  box-shadow: 2px 2px 2px #DDD;
}

.dte_leitura-box-toggle {
  border: 1px solid #CCC;
  box-shadow: 2px 2px 2px #DDD;
}

.dte_leitura-box-toggle * {
  box-sizing: border-box;
}

.dte_leitura-box-toggle-titulo {
  width: 100%;
  display: block;
  position: relative;
  border-bottom: 1px solid #CCC;
  background: #EEE;
  font-weight: bold;
  padding: 10px;
  margin: 0 !important;
  cursor: pointer;
  transition: all linear 0.2s;
}

.dte_leitura-box-toggle-titulo:hover {
  background: #DDD;
}

.dte_leitura-box-toggle-btn {
  float: right;
  margin-top: -4px;
}

.dte_leitura-box-toggle-conteudo {
  display: none;
  padding: 10px;
  background: #FBFBFB;
}

/** UTILS **/
.u_center {
  text-align: center;
}

.u_block {
  display: block;
  display: block;
  padding: 10px 20px;
}

.u_vertical_center {
  vertical-align: middle;
}

.u_clearfix {
  overflow: auto;
  zoom: 1;
}

.u_btn-disabled-false, .u_btn-disabled-false:hover {
  background: #ddd !important;
  color: #aaa !important;
}

.btn .btn-icon {
  margin-right: 5px;
}

@media ( max-width : 766px) {
  .dte_modal-large {
    width: 95%;
    margin-left: 0;
  }
}
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h4 class="page-title m-b-0">Mensagens DEC</h4>
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'TODAS'=>'Todas',
                  'LIDAS'=>'Lidas',
                  'NAO_LIDAS'=>'Não Lidas',
                  'INTIMACAO'=>'Intimação',
                  'AVISO'=>'Aviso',
                  'COMUNICADO'=>'Comunicação',
                  'CIENTIFICACAO'=>'Cientificação'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro', 'onchange' => 'loadAjax()'), $valores_filtro, $filtro); 
              ?>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table table-striped" id="example">
                <thead>
                  <tr>
                    <th></th>
                    <th>Razão Social</th>
                    <th>CNPJ</th>
                    <th>Situação da Ciência</th>
                    <th>Código/Tipo</th>
                    <th>Destinatário</th>
                    <th>Remetente</th>
                    <th>Data De Emissão</th>
                    <th>Assunto</th>
                    <th>Data de Leitura</th>
                    <th>Data de Ciência</th>
                    <th>Lida</th>
                    <th width="15%"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<div class="modal fade" id="modalMensagem" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- <div class="modal-body"> -->
         

        <div class="modal-body">
          <div class="accordion" id="accordionMensagem">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a ><i class="icon-chevron-down"></i> Mensagem Recebida</a>
              </div>
              <div id="collapse2" class="accordion-body">
                <div class="accordion-inner">
                  <div style="display: inline-block; width: 100%">
                    <div class="dte_leitura-box-greater">
                      <div class="row-fluid">
                        <div class="span12">
                          <div class="dte_leitura-box">
                            <div class="dte_leitura-box-linha">
                              <div class="dte_leitura-box-linha-conteudo">
                                <div class="dte_leitura-box-coluna dte_leitura-box-titulo">Tipo de Mensagem:</div>
                                <div class="dte_leitura-box-coluna dte_leitura-box-conteudo" id="tipo"><span id="span_tipo"></span></div>
                                <div class="dte_leitura-box-coluna dte_leitura-box-titulo">Data de Leitura:</div>
                                <div class="dte_leitura-box-coluna dte_leitura-box-conteudo" id="data_leitura"></div>
                              </div>
                            </div>
                            <div class="dte_leitura-box-linha">
                              <div class="dte_leitura-box-linha-conteudo">
                                <div class="dte_leitura-box-coluna dte_leitura-box-titulo">Data de Emissão:</div>
                                <div class="dte_leitura-box-coluna dte_leitura-box-conteudo" id="data_emissao"></div>
                                <div class="dte_leitura-box-coluna dte_leitura-box-titulo">Data de Vencimento:</div>
                                <div class="dte_leitura-box-coluna dte_leitura-box-conteudo" id="data_vencimento"></div>
                              </div>
                            </div>
                            <div class="dte_leitura-box-linha">
                              <div class="dte_leitura-box-linha-conteudo">
                              <div class="dte_leitura-box-coluna dte_leitura-box-titulo">Remetente:</div>
                              <div class="dte_leitura-box-coluna dte_leitura-box-conteudo" id="remetente"></div><div class="dte_leitura-box-coluna dte_leitura-box-titulo">Destinatário:</div>
                              <div class="dte_leitura-box-coluna dte_leitura-box-conteudo" id="destinatario"></div>
                            </div>
                          </div>
                          <div class="dte_leitura-box-linha">
                            <div class="dte_leitura-box-linha-conteudo">
                              <div class="dte_leitura-box-coluna dte_leitura-box-titulo">Assunto:</div>
                              <div class="dte_leitura-box-coluna dte_leitura-box-conteudo" id="assunto"></div>
                              <div class="dte_leitura-box-coluna dte_leitura-box-titulo">CNPJ/CPF</div>
                              <div class="dte_leitura-box-coluna dte_leitura-box-conteudo-80" id="cnpj"></div>
                            </div>
                          </div>
                          <div class="dte_leitura-box-linha">
                            <div class="dte_leitura-box-linha-conteudo">
                              <div class="dte_leitura-box-coluna dte_leitura-box-titulo-grande">Mensagem:</div>
                            </div>
                          </div>
                          <div class="dte_leitura-box-linha">
                            <div class="dte_leitura-box-linha-conteudo">
                              <div style="height: 100px;" class="dte_leitura-box-coluna dte_leitura-box-conteudo-grande" id="conteudo_msg">

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- </div> -->
    </div>
  </div>

</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }

</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
  loadAjax();
});


function loadAjax(){
  var valor_filtro = $("#filtro").val();

  $.ajax({
      type: "POST",
      url: '<?php echo base_url('caixadeemail/listar_msgs_dte_novo'); ?>',
      data: {
          filtro: valor_filtro
      },
      async: true,
      success: function(result){
          var data = JSON.parse(result);
          if(data == "") {
              var array = [];
              popularDataTable(array);
              jQuery('[data-toggle="tooltip"]').tooltip();                
          }else{
            var array = json2array(data);
            popularDataTable(array);
            jQuery('[data-toggle="tooltip"]').tooltip();

            
          }
      }
  });
}

function popularDataTable(json){
  $.fn.dataTable.moment('DD/MM/YYYY');
    var table = $('#example').DataTable({
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        "iDisplayLength": 50,dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
}

function json2array(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];
    var id = '';
    var id_contabilidade = '';
    var cnpjSemFormat;
    var status;
    var tipo;
    for(var i in data){
      array = [];
      id = data[i].id;

      array.push('<td>'+data[i].data_teste+'</td>')
      array.push('<td>'+data[i].razao_social+'</td>'); 
      array.push('<td>'+data[i].cnpj+'</td>'); 
      array.push('<td>'+data[i].situacao_da_ciencia+'</td>');

      tipo = data[i].codigo_tipo;
      if(tipo.includes("53") || tipo.includes("1")){
        array.push("<span class='badge_dte' style='background-color: #3a6eb0;'>"+data[i].codigo_tipo+"</span>");
      }else if(tipo.includes("54")){
        array.push("<span class='badge_dte' style='background-color: #f89406;'>"+data[i].codigo_tipo+"</span>");
      }else if(tipo.includes("52")){
        array.push("<span class='badge_dte' style='background-color: #468847;'>"+data[i].codigo_tipo+"</span>");
      }else{
        array.push("<span class='badge_dte' style='background-color: #3a6eb0;'>"+data[i].codigo_tipo+"</span>");
      }

      array.push('<td>'+data[i].destinatario+'</td>');
      array.push('<td>'+data[i].remetente+'</td>');
      array.push('<td>'+data[i].data_de_emissao+'</td>');
      array.push('<td>'+data[i].assunto+'</td>');
      array.push('<td>'+data[i].data_de_leitura+'</td>');
      array.push('<td>'+data[i].data_de_ciencia+'</td>');

      status = data[i].status;
      if(data[i].conteudo_mensagem == "" || data[i].conteudo_mensagem == null){
        array.push("<span class='badge badge-pill badge-danger' style=' border: white;'>NÃO</span>");
        array.push("<a href='javascript:void(0)' onclick='ler_msg(&quot;"+data[i].status+"&quot;, &quot;"+id+"&quot;, &quot;"+data[i].cnpj+"&quot;)' data-toggle='tooltip' data-placement='top' title='Ver Mensagem' class='btn btn-default b' style='color: #1b6dc1;'><i class='material-icons' aria-hidden='true'>email</i></a>");
      }else{
        array.push("<span class='badge badge-pill badge-success' style=' border: white;'>SIM</span>");
        array.push("<a href='javascript:void(0)' onclick='buscar_conteudo(&quot;"+id+"&quot;)' data-toggle='tooltip' data-placement='top' title='Ver Mensagem' class='btn btn-default b' style='color: #1b6dc1;'><i class='material-icons' aria-hidden='true'>email</i></a>");
      }
      

      arrayMultiple.push(array);
    }
    
    return arrayMultiple;
}

function apenasNumeros(string){
    var numsStr = string.replace(/[^0-9]/g,'');
    return numsStr;
}

function ler_msg(msg, id, cnpj){
  var id_mensagem = msg.replace(/\D/g, "");

  swal({ 
      title: "Buscando...",
      text: 'Buscando conteúdo da mensagem do DTE.\nPor favor aguarde, esse processo pode levar algum tempo...',
      type: "info" ,
      confirmButtonText: "Cancelar",
      confirmButtonColor: "#fff"
  },function(isConfirm){
      if(isConfirm){
          request.abort();
      }
  });

  var banco = <?php echo "'".$banco."'"; ?>;
  url = "<?php echo base_url("crons-api/dte/buscar_msg/"); ?>";
  // url = "https://veri.com.br/crons-api/dctf_declaracao/buscar_declaracao/";
  var request = $.post(url, { cnpj: cnpj, banco : banco, id: id, id_mensagem: id_mensagem }, function(data, status){

      if(data.includes("ERRO")){

        $(document).ready(function() {                
            swal({
                title: "Erro ao buscar mensagem !",
                text: "Erro",
                type: "error"
            });
        });

      }else{
        swal.close();
        buscar_conteudo(id);
        //exibir_conteudo(id);
        // window.open(data, '_blank');
        //setTimeout(reload_page, 5000);
      }
  });
    
}

function buscar_conteudo(id){

  $("#tipo").html("");
  $("#data_leitura").html("");
  $("#data_emissao").html("");
  $("#data_vencimento").html("");
  $("#remetente").html("");
  $("#destinatario").html("");
  $("#assunto").html("");
  $("#cnpj").html("");
  $("#conteudo_msg").html("");

  $.ajax({
      type: "POST",
      url: '<?php echo base_url('caixadeemail/buscar_conteudo'); ?>',
      async: true,
      data: { 
          'id': id
      },
      success: function(result){
        var resultado = JSON.parse(result);
        if(resultado != ''){
          
          // $("#tipo").html(resultado.codigo_tipo);

          tipo = resultado.codigo_tipo;
          if(tipo.includes("53") || tipo.includes("1")){
            $("#tipo").html("<span class='badge badge-normal'>"+tipo+"</span>");
          }else if(tipo.includes("54")){
            $("#tipo").html("<span class='badge badge-alert'>"+tipo+"</span>");
          }else if(tipo.includes("52")){
            $("#tipo").html("<span class='badge badge-info'>"+tipo+"</span>");
          }else{
            $("#tipo").html("<span class='badge badge-normal'>"+tipo+"</span>");
          }

          $("#data_leitura").html(resultado.data_de_leitura);
          $("#data_emissao").html(resultado.data_de_emissao);
          $("#data_vencimento").html(resultado.data_de_vencimento);
          $("#remetente").html(resultado.remetente);
          $("#destinatario").html(resultado.destinatario);
          $("#assunto").html(resultado.assunto);
          $("#cnpj").html(resultado.cnpj);
          $("#conteudo_msg").html('<p>'+resultado.conteudo_mensagem+'</p>');
        }
      },
  });

  $('#modalMensagem').modal();

}
</script>


