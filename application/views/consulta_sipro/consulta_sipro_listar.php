<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url('assets/css/veri/select2.min.css') ?>" rel="stylesheet" />

<script type="text/javascript">  
    var global_url = '';  
    var global_url_2 = '';  

    function preencher_url(url){
    global_url = url;
    }

    function preencher_url_2(url_2){
    global_url_2 = url_2;
    }
</script> 

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2>Acompanhamento de Processos SIPRO</h2>
            <div class="card-header-action" style="margin-left: auto;">
            <a class="btn btn-default btn-rounded pull-right text-right" style="color: #FFFFFF; background-color: #24a0e9;" data-toggle="modal" data-target="#modal_cadastro" href="javascript:;">Novo Processo</a>
            </div>
          </div>
          <div class="card-body">
            <br>
            <div class="table-responsive">
              <table class="table table-bordered table-hover stripe row-border" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                  <tr>
                      <th style="font-size: 12px;">Processo</th>
                      <th style="font-size: 12px;">Nome da Empresa</th>
                      <th style="font-size: 12px;">Cadastramento</th>
                      <th style="font-size: 12px;">Situação</th>
                      <th style="font-size: 12px;">Envio</th>
                      <th style="font-size: 12px;">Recepção</th>
                      <th style="font-size: 12px;">Objetivo</th>
                      <th style="font-size: 12px;">Ações</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  if ($consulta){
                      foreach ($consulta as $c){?>
                          <tr>
                              <td><?php echo $c->numero_processo_formatado ?></td>
                              <td><?php echo $c->razao_social ?></td>
                              <td><?php echo $c->data_cadastramento ?></td>
                              <td><?php echo $c->situacao_processo ?></td>
                              <td><span data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Local" data-content="<?php echo $c->envio_local; ?>"><?php echo $c->envio_data; ?></span></td>
                              <td><span data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Local" data-content="<?php echo $c->recepcao_local; ?>">

                              <?php if (DateTime::createFromFormat('Y-m-d', $c->recepcao_data) !== FALSE) {
                                echo $c->recepcao_data;
                              } ?>
                                

                              </span></td> 
                              <td><?php echo $c->objetivo ?></td>
                              <td style="width: 14%; font-size: 5px;">
                                  <a href="<?php echo 'https://www.sefaz.ba.gov.br/scripts/sipro/res_consulta_final_sipro.asp?cod_processo='."$c->numero_processo"; ?>" data-toggle="tooltip" data-placement="top" title="Consultar Processo" target="_blank" class="btn btn-default b editar" style="padding: 1px !important;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">search</i></a>
                                  <?php
                                  echo '<a href="#modalEditar" data-toggle="modal" class="btn btn-default b editar" idLancamento="'.$c->id.'" protocoloEditar="'.$c->razao_social.'" style="padding: 1px !important;" data-placement="top" title="Editar Processo" ><i class="material-icons" aria-hidden="true" style="color: #24a0e9">edit</i></a>';
                                  ?>
                                  <a href="#" data-toggle="tooltip" data-placement="top" title="Excluir" onclick='preencher_url("<?php echo base_url("consulta_sipro/excluir/$c->id"); ?>")'><span class="btn sweet-7" style="padding: 1px 4px;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">delete</i></span></a>
                                  <a href="javascript:openModalDocumento(<?php echo $c->id ?>)"  title="Documentos" class="btn btn-default b editar" style="padding: 1px !important;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">description</i></a>
                              </td>
                          </tr>
                      <?php }
                  }
                  ?>
                  </tbody>
              </table>

            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modal_cadastro">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header p-4">
              <h5 class="modal-title">Cadastrar Novo Protocolo</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body p-4">
            <?php echo form_open("consulta_sipro/cadastrar"); ?> 
              <div class="form-group mb-4">
                <label>Nº do Processo*:</label>
                  <input class="form-control" type="text" name="processo" placeholder="Digite o nº do processo" required="">
              </div>  

              <div class="form-group mb-4">
                  <label for="nome">Empresa*:</label>
                  <?php echo form_dropdown(array('name'=>'id_empresa', 'class'=>'select2_demo_1 form-control',  'style'=>'width: 100% !important;', 'placeholder'=>''), $lista_empresas, '', 'required'); ?>
              </div> 
          </div>
          <div class="modal-footer  bg-primary-50">           
              <button class="btn btn-danger btn-rounded mr-3" data-dismiss="modal" >Cancelar</button>
              <?php echo form_submit(array('name'=>'btn_cadastrar', 'class'=>'btn btn-primary btn-rounded mr-3'), 'Cadastrar'); ?> 
          </div>
          <?php echo form_close(); ?> 
      </div>
  </div>
</div>

<div class="modal fade" id="modalEditar">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header p-4">
              <h5 class="modal-title">Editar Processo</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body p-4">
            <?php echo form_open("consulta_sipro/editar"); ?>
              <div class="form-group mb-4">
                  <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""  />
                  <?php echo form_dropdown(array('name'=>'id_empresa', 'id'=>'protocoloEditarValor', 'class'=>'select2_demo_1 form-control',  'style'=>'width: 100% !important;', 'placeholder'=>''), $lista_empresas, '', 'required'); ?> 
                  <input type="hidden" id="idEditar" name="id" value="" />
              </div>  
          </div>
          <div class="modal-footer  bg-primary-50">           
              <button class="btn btn-warning btn-rounded mr-3" data-dismiss="modal" >Cancelar</button>
              <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary btn-rounded mr-3'), 'Salvar'); ?> 
          </div>
          <?php echo form_close(); ?> 
      </div>
  </div>
</div>

<div id="modal-div-new-entity"></div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
        toastr["success"]("O Processo foi cadastrado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["error"]("Ocorreu um erro ao cadastrar o processo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["success"]("O processo foi editado com sucesso!", "Editado!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["error"]("Ocorreu um erro ao editar o processo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 5) { ?>
        toastr["success"]("O processo foi excluído com sucesso!", "Excluído!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 6) { ?>
        toastr["error"]("Ocorreu um erro ao excluir o processo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 7) { ?>
        toastr["error"]("Processo já cadastrado.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 10) { ?>
        toastr["error"]("Processo não encontrado na Sefaz.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 12) { ?>
        toastr["success"]("O arquivo foi excluído com sucesso!", "Excluído!");    
    <?php } ?>
</script>

<script>
$(document).ready(function() {
var table = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );

    $(document).on('click', '.editar', function(event) {
      $("#idEditar").val($(this).attr('idLancamento'));
      $("#protocoloEditarValor").val($(this).attr('protocoloEditar'));
      $("#urlAtualEditar").val($(location).attr('href'));
    }); 

});

$(".select2_demo_1").select2();

</script>

<script type="text/javascript">
    function openModalDocumento(id_tramitacao_processo_sipro) {
        $.ajax({
            type: "GET",
            url: 'consulta_sipro/documento_modal?id_tramitacao_processo_sipro='+id_tramitacao_processo_sipro,
            success: function (data) {
                $("#modal-div-new-entity").html(data);
            },
        });
        return false;
    }
</script>