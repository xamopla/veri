<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<div class="modal fade" id="modalDocumentos">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title">Documentos</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <?php echo form_open("consulta_sipro/cadastrar_documento", ['id' => 'frm_sipro_documento', 'enctype' => 'multipart/form-data']); ?>
                <input name="id_tramitacao_processo_sipro" type="hidden"  value="<?php echo $id_tramitacao_processo_sipro ?>"/>
                <div class="form-group mb-4">
                    <label>Nome</label>
                    <?php echo form_input(array('name'=>'nome', 'id'=>'nome', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), ''); ?>
                </div>
                <div class="form-group mb-4">
                    <label for="logo">Anexo</label>
                    <br>
                    <input name="anexo1" id="anexo1" class="form-control" type="file" accept="image/doc*" />
                    <br>
                    <small class="text-muted">Extensões permitidas: | gif | jpeg | jpg | png | doc | docx | txt | xls | xlsx | pdf | Tamanho máximo de 512kb.</small>
                </div>
                <div class="form-group mb-4 pull-right">
                    <button type="submit" class="btn btn-primary btn-rounded mr-3" >Salvar</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-body p-4">
                <div class="form-group mb-4">
                    <table class="table table-bordered table-hover" id="table-documentos">
                        <thead class="thead-default table-sm">
                        <tr>
                            <th>Nome</th>
                            <th>Anexo</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($lista as $value): ?>
                                <tr id="modal-tr-<?php echo $value->id ?>">
                                    <td><?php echo $value->nome; ?></td>
                                    <td><a target="_blank" href="<?php echo $value->anexo_path; ?>"> <?php echo $value->anexo_nome; ?> </a></td>
                                    <td>
                                        <a href='#' onclick='preencher_url("<?php echo base_url("consulta_sipro/remover_documento/$value->id"); ?>")' data-toggle="tooltip" data-placement="top" title="Excluir"><span class="btn sweet-7" style="padding: 1px 4px;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">delete</i></span></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script type="text/javascript">
    $('#modalDocumentos').modal();

    function remover(id){
        $.ajax({
            type: "POST",
            url: 'consulta_sipro/remover_documento',
            data: {id: id},
            cache: false,
            success: function () {
                $('#modal-tr-' + id).remove();
            },
        });
    }

    $("#frm_sipro_documento").submit(function(e) {
        e.preventDefault();
        if ($('#anexo1').get(0).files.length > 0 && $('#nome').val() !== '') {
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if(JSON.parse(data) != ''){
                        adicionarTemplate(JSON.parse(data))
                        $('#anexo1').val('')
                        $('#nome').val('')
                    }else{
                        alert('Erro');
                    }
                    
                },
            });
        }
    });

    function adicionarTemplate(data){
        var templateTr = ` <tr id="modal-tr-${data.id}">
    <td>${data.nome}</td>
    <td><a target="_blank" href="${data.anexo_path}"> ${data.anexo_nome} </a></td>
    <td>
        <a href="javascript:remover(${data.id})" data-toggle="tooltip" data-placement="top" title="Excluir"><span class="btn sweet-7" style="padding: 1px 4px;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">delete</i></span></a>
    </td>
</tr>`;
        $('#table-documentos > tbody:last-child').append(templateTr);
    }
</script>