<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Alterar Foto</h2>
                <br>
                <div class="ibox-head">
                        <a href="<?php echo base_url("logo/atualizarfoto/1") ?>" class="btn btn-info btn-fix btn-animated fade-button pull-left">
                                <span class="visible-content">Alterar Logo</span>
                                <span class="hidden-content" style="font-size: 10px;">Tamanho máx:<small> 100x100</small></span></a>
             
                    <div class="col-lg-2 pull-right">
                        <a href="<?php echo base_url("usuario/alterarsenha/").$this->session->userdata['userprimesession']['id']; ?>" class='btn btn-primary btn-fix'>
                                <span class="visible-content">Alterar Senha</span>
                        </a>
                    </div>
                </div>
              </div>
                <div class="card-body">
                  <div class="ibox">
                        <div class="ibox-body">
                            <?php echo form_open("usuario/perfil/".$this->session->userdata['userprimesession']['id'], array('role'=>'form')); ?> 

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-4">
                                        <label for="nome">Email:</label>
                                        <small><span class="fa fa-question-circle" data-toggle="tooltip" title="Obs.: Este é apenas um e-mail para contato, alterando este campo, o e-mail de login continuará o mesmo do cadastro inicial."></span></small>
                                        <?php echo form_input(array('type'=>'email', 'name'=>'email', 'class'=>'form-control', 'placeholder'=>'E-mail', 'style'=>'border-color: #5c6bc0;'), $usuario->email, 'required'); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Razão Social:</label>
                                         <?php echo form_input(array('name'=>'razao_social', 'class'=>'form-control', 'placeholder'=>'Nome Fantasia', 'style'=>'border-color: #5c6bc0;'), $usuario->razao_social, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Nome Fantasia:</label>
                                         <?php echo form_input(array('name'=>'nome_fantasia', 'class'=>'form-control', 'placeholder'=>'Nome Fantasia', 'style'=>'border-color: #5c6bc0;'), $usuario->nome_fantasia, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>CNPJ:</label>
                                         <?php echo form_input(array('name'=>'cnpj', 'maxlength'=>'18', 'onKeyPress'=>'MascaraCNPJ(this)', 'class'=>'form-control', 'placeholder'=>'Nome Fantasia', 'style'=>'border-color: #5c6bc0;'), $usuario->cnpj, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>CEP:</label>
                                         <?php echo form_input(array('name'=>'cep', 'maxlength'=>'9', 'onKeyPress'=>'MascaraCep(this)', 'onBlur'=>'ValidarCep(this)', 'class'=>'form-control', 'placeholder'=>'CEP', 'style'=>'border-color: #5c6bc0;'), $usuario->cep, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                        <label>Logradouro:</label>
                                        <?php echo form_input(array('name'=>'logradouro', 'class'=>'form-control', 'placeholder'=>'Logradouro', 'style'=>'border-color: #5c6bc0;'), $usuario->logradouro, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Número:</label>
                                         <?php echo form_input(array('name'=>'numero', 'class'=>'form-control', 'placeholder'=>'Número', 'style'=>'border-color: #5c6bc0;'), $usuario->numero, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Complemento:</label>
                                         <?php echo form_input(array('name'=>'complemento', 'class'=>'form-control', 'placeholder'=>'Complemento', 'style'=>'border-color: #5c6bc0;'), $usuario->complemento, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Bairro/Distrito:</label>
                                         <?php echo form_input(array('name'=>'bairro', 'class'=>'form-control', 'placeholder'=>'Bairro', 'style'=>'border-color: #5c6bc0;'), $usuario->bairro, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Cidade:</label>
                                         <?php echo form_input(array('name'=>'cidade', 'class'=>'form-control', 'placeholder'=>'Cidade', 'style'=>'border-color: #5c6bc0;'), $usuario->cidade, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group mb-4">
                                         <label>UF:</label>
                                         <?php echo form_input(array('name'=>'uf', 'class'=>'form-control', 'placeholder'=>'UF', 'style'=>'border-color: #5c6bc0;'), $usuario->uf, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group mb-4">
                                         <label>Telefone:</label>
                                         <?php echo form_input(array('name'=>'telefone', 'maxlength'=>'14', 'onKeyPress'=>'MascaraTelefone(this)', 'id'=>'telefone', 'class'=>'form-control', 'placeholder'=>'Telefone', 'style'=>'border-color: #5c6bc0;'), $usuario->telefone, ''); ?>
                                    </div>
                                </div>

                                <div style="display: none;">
                                <div class="col-lg-12">
                                 <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div> 
                                <div class="col-lg-12" style="font-size: 28px; text-align: center;">Notificações DTE</div>
                                <br><br><br>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Email Notificação DTE:</label>
                                         <small><span class="fa fa-question-circle" data-toggle="tooltip" title="Email em que a contabilidade recebe as notificações DTE enviadas pela Sefaz."></span></small>
                                         <?php echo form_input(array('name'=>'email_sefaz', 'class'=>'form-control', 'placeholder'=>'Email Sefaz', 'style'=>'border-color: #5c6bc0;'), $usuario->email_sefaz, ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group mb-4">
                                         <label>Senha:</label>
                                         <small><span class="fa fa-question-circle" data-toggle="tooltip" title="Senha do email em que recebe essas notificações."></span></small>
                                         <?php echo form_input(array('name'=>'senha_sefaz', 'class'=>'form-control', 'type'=>'password', 'placeholder'=>'Senha Sefaz', 'style'=>'border-color: #5c6bc0;'), $usuario->senha_sefaz, ''); ?>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                  <label>Selecione o Servidor de email:</label> <span class="fa fa-question-circle" data-toggle="tooltip" title="Selecione o Servidor de email (gmail, yahoo, hotmail...)"></span>
                                </div>

                                <div class="col">
                                  <div class="form-group m-t-15 custom-radio-ml">
                                    <div class="radio radio-primary">
                                      <input id="radio11" type="radio" name="servidor_email" value="1" <?php if ($usuario->servidor_email == '1') echo "checked"; ?> >
                                      <label for="radio11">Gmail<span class="digits"></span></label>
                                    </div>
                                    <div class="radio radio-secondary">
                                      <input id="radio22" type="radio" name="servidor_email" value="2" <?php if ($usuario->servidor_email == '2') echo "checked"; ?> >
                                      <label for="radio22">Yahoo<span class="digits"></span></label>
                                    </div>
                                    <div class="radio radio-success">
                                      <input id="radio55" type="radio" name="servidor_email" value="3" <?php if ($usuario->servidor_email == '3') echo "checked"; ?>>
                                      <label for="radio55">Hotmail/Outlook<span class="digits"></span></label>
                                    </div>
                                    <div class="radio radio-info">
                                      <input id="radio33" type="radio" name="servidor_email" value="4" <?php if ($usuario->servidor_email == '4') echo "checked"; ?>>
                                      <label for="radio33">Bol<span class="digits"></span></label>
                                    </div>
                                    <div class="radio radio-warning">
                                      <input id="radio44" type="radio" name="servidor_email" value="5" <?php if ($usuario->servidor_email == '5') echo "checked"; ?>>
                                      <label for="radio44">Uol<span class="digits"></span></label>
                                    </div>
                                    <div class="radio radio-danger">
                                      <input id="radio66" type="radio" name="servidor_email" value="6" <?php if ($usuario->servidor_email == '6') echo "checked"; ?>>
                                      <label for="radio66">Outros<span class="digits"></span></label>
                                    </div>
                                  </div>
                                </div>

                                </div>

                            </div>
                        </div>
                        <div class="ibox-footer text-right">
                            <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                            <a href="<?php echo base_url("painel"); ?>" class="btn btn-outline-secondary">Voltar</a>
                        </div>
                        <?php echo form_close(); ?>
                </div>
                               
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
 