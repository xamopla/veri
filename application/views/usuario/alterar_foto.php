<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Alterar Logo</h2>
                <label style="color:red">Ao alterar a logo será necessário deslogar e logar no Veri para que as mudanças passem a valer:</label>
              </div>
                <div class="card-body">
                  <div class="ibox-body">
                      <?php echo form_open_multipart("logo/atualizar/1"); ?>
                      <div class="row">
                          <div class="col-lg-12" style="text-align: center;">
                              <?php 
                                if ($logo->caminho){
                                  echo "<img src=".base_url("$logo->caminho")." class='img-responsive img-thumbnail'>";
                                } else {
                                  echo "<img src=".base_url('assets/img/image.png')." class='img-responsive img-thumbnail' width='200px'>";
                                }
                                ?>
                                <span class="hidden-content" style="font-size: 14px;">Tamanho máx:<small> 100x100</small></span></a>
                          </div>

                          <div class="col-md-4 mx-auto" style="margin-top: 20px; border: 1px dashed #D2D6DE;">
                          <div class="form-group">
                            <label for="usr">Selecionar Logo:</label>
                            <input id="userfile" name="userfile" class="file" type="file" required accept="image/*" />
                          </div>
                        </div>
                        <?php echo form_open("logo/atualizar/1", array('role'=>'form')); ?>
                      </div>
                  </div>
                  <div class="ibox-footer text-right">
                      <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                  </div>
                  <?php echo form_close(); ?>
                               
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>

<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
 

<script type="text/javascript">  
    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("A empresa foi cadastrada com sucesso!", "Cadastrada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("As informações da empresa foram editadas com sucesso!", "Editada!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
    toastr["success"]("A empresa foi excluída com sucesso.", "Excluída!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["warning"]("Ocorreu um erro ao salvar, verifique o tamanho máximo do arquivo permitido e tente novamente!", "Erro!");
    <?php } ?>
</script>