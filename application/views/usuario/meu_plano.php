<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link href="<?php echo base_url('assets/css/c3.min.css'); ?>" rel="stylesheet" />

<style>
  .pricing-item {
      text-align: center;
      background-color: #fff;
      margin-bottom: 26px;
  }

  .pricing-header {
      position: relative;
      padding-bottom: 30px;
  }

  .pricing-header:after {
      content: '';
      border-color: transparent;
      border-style: solid;
      border-width: 10px 10px 0 10px;
      position: absolute;
      top: 100%;
      left: 50%;
      margin-left: -10px;
  }

  .pricing-item-1 .pricing-header:after {
      border-top-color: #5c6bc0;
  }

  .pricing-item-2 .pricing-header:after {
      border-top-color: #18c5a9;
  }

  .pricing-item-3 .pricing-header:after {
      border-top-color: #ff4081;
  }

  .pricing-item-4 .pricing-header:after {
      border-top-color: #3498DB;
  }

  .pricing-item-5 .pricing-header:after {
      border-top-color: #7536e6;
  }

  .pricing-item-6 .pricing-header:after {
      border-top-color: #34495f;
  }

  .pricing-title {
      padding: 15px;
      background-color: rgba(0, 0, 0, .1);
      color: #fff;
  }

  .pricing-item .price {
      color: #fff;
  }

  .pricing-item .price .price-number {
      font-size: 60px;
  }

  .pricing-item .price sup {
      margin-right: 3px;
      font-size: 35px;
      top: -.5em;
  }

  .pricing-features {
      display: flex;
      flex-wrap: wrap;
      border-right: 1px solid rgba(0, 0, 0, .1);
  }

  .pricing-features .feature-item {
      flex: 50%;
      height: 140px;
      border-bottom: 1px solid rgba(0, 0, 0, .1);
      border-left: 1px solid rgba(0, 0, 0, .1);
  }

  .feature-item .feature-icon {
      display: block;
      font-size: 44px;
      padding: 20px;
  }

  .pricing-item-btn {
      background-color: transparent;
      color: #fff;
      padding: .75rem;
      width: 170px;
      border: 2px solid #fff;
      border-radius: 50px;
      box-shadow: none !important;
  }

  .pricing-item-btn:hover {
      background-color: #fff;
      color: inherit;
  }
</style>

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Meu Plano</h2>
              </div>
              <div class="card-body">

                <div class="ibox">
                    <div class="ibox-head">
                        <!-- INFORMAÇÃO DO PLANO CONTRATADO + LOGO -->

                        <div class="row">
                          <div class="col-5">
                            <div class="card bg-success">
                              <div class="card-body">
                                  <h2 class="text-white"><?php echo $qtd_max_empresas; ?><i class="fa fa-building float-right"></i></h2>
                                  <div class="text-white">Empresas que podem ser cadastradas</div>
                                  <div class="text-white mt-1"><i class="ti-stats-up mr-1"></i><small>Plano Contratado - <?php echo $nome_do_plano; ?></small></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-2" >
                            <?php         
                            echo "<img src=".base_url('assets/img/logo/logo-sidebar.png')." class='img-responsive' style='width: 150px;'>";                        
                            ?>
                          </div>

                          <div class="col-5">
                            <div class="card bg-primary">
                              <div class="card-body">
                                  <h2 class="text-white"><?php echo $qtd_max_usuarios; ?><i class="fa fa-user float-right"></i></h2>
                                  <div class="text-white">Usuários que podem ser cadastrados</div>
                                  <div class="text-white mt-1"><i class="ti-stats-up mr-1"></i><small>Plano Contratado - <?php echo $nome_do_plano; ?></small></div>
                              </div>
                            </div>
                          </div>
                        </div>
                          

                          
                        <!-- FIM DA INFORMAÇÃO DO PLANO CONTRATADO + LOGO -->

                    </div>
                    <br>
                    <div class="row">
                    <!-- GRÁFICO 1 -->
                    <div class="col-md-6">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Empresas Cadastradas Atualmente - <?php echo $qtd_empresas; ?></div>
                            </div>
                            <div class="ibox-body">
                                <div>
                                    <div id="gaugeChart" style="max-height: 160px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIM DO GRÁFICO 1 -->

                    <!-- GRÁFICO 2 -->
                    <div class="col-md-6">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Usuários Cadastrados Atualmente - <?php echo $qtd_usuarios; ?></div>
                            </div>
                            <div class="ibox-body">
                                <div>
                                    <div id="gaugeChart2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIM DO GRÁFICO 2 -->
                    </div>
                        
                    <div class="ibox-footer text-right">
                        <a href="<?php echo base_url("painel"); ?>" class="btn btn-outline-secondary">Voltar</a>
                    </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/d3.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/c3.min.js'); ?>"></script>

<script type="text/javascript">

  $(document).ready(function() {
      $('.js-example-basic-multiple').select2();
  });
</script>

<script type="text/javascript">
$(function() {
    
    c3.generate({
        bindto: '#gaugeChart',
        data:{
            columns: [
                ['Empresas', <?php echo $porcentagemEmpresas; ?>]
            ],

            type: 'gauge'
        },
        color:{
            pattern: ['#18C5A9', '#bdc3c7']

        }
    });

    c3.generate({
        bindto: '#gaugeChart2',
        data:{
            columns: [
                ['Usuários', <?php echo $porcentagemUsuarios; ?>]
            ],

            type: 'gauge'
        },
        color:{
            pattern: ['#5c6bc0', '#bdc3c7']

        }
    });

});
</script>