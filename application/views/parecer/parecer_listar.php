<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
    .buttons-excel{
        width: 100px;
    }

    .dataTables_filter{
        float: right;
    }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Lista de Processos Cadastrados<</h2> 
            <a class="btn btn-default btn-rounded pull-right" style="color: #FFFFFF; background-color: #5c6bc0;" data-toggle="modal" data-target="#modal_cadastro" href="javascript:;">Novo Processo</a> <br> <br> 
              <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  ''=>'Todos',
                  'consultaaguardando'=>'Aguardando',
                  'consultafinalizadas'=>'Finalizado'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br><br>
            
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-hover" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr> 
                        <th style="font-size: 12px;">Empresa</th>
                        <th style="font-size: 12px;">CNPJ</th>
                        <th style="font-size: 12px;">Nº Processo</th>
                        <th style="font-size: 12px;">Data de Inclusão</th>
                        <th style="font-size: 12px;">Ciência do Parecer Final</th>
                        <th style="font-size: 12px;">Ações</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php  
                    if ($consulta){ 
                      foreach ($consulta as $c){?>
                      <tr>                          
                            <td><?php echo $c->razao_social ?></td>
                            <td><?php echo $c->cnpj_completo ?></td>
                            <td><?php echo $c->numero_processo ?></td>
                            <td><?php echo date('d/m/Y H:i:s', strtotime($c->data_inclusao)); ?></td>
                            <td style="font-size: 12px; text-align: center;">
                              <?php if ($c->status_parecer == 1){ ?>   

                                <span class="badge badge-danger badge-shadow">AGUARDANDO</span>
                              
                              <?php } else { ?>

                                <span class="badge badge-success badge-shadow">FINALIZADO</span>

                              <?php } ?>
                            </td>
                            <td style="width: 10%; font-size: 12px;">
                             <a href="<?php echo base_url("empresa/visualizar/$c->id"); ?>" data-toggle="tooltip" data-placement="top" title="Consultar Processo" class="btn btn-default b"><i class="fa fa-address-card-o" aria-hidden="true"></i></a>
                             <a href="#" data-toggle="tooltip" data-placement="top" title="Excluir" onclick='preencher_url("<?php echo base_url("parecer/remover/$c->id"); ?>")'><span class="btn sweet-7" style="padding: 1px 4px;"><i class="ti ti-trash"></i></span></a>
                            </td>
                      </tr>
                  <?php } 
                  }
                  ?>
                  </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modal_cadastro">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header p-4">
              <h5 class="modal-title">Cadastrar Novo Processo</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body p-4">
            <?php echo form_open("parecer/cadastrar"); ?>
              <div class="flexbox mb-4">
                  <span class="btn-icon-only btn-circle bg-primary-50 text-primary mr-3"><i class="fa fa-calendar"></i></span>
                  <div class="flex-1 d-flex">
                      <div class="flex-1">
                          <span class="text-muted mr-2">Empresa:</span> 
                      </div> 
                  </div>
              </div> 
              <div class="form-group mb-4"> 
                  <?php echo form_dropdown(array('name'=>'id_empresa', 'class'=>'form-control form-control-line select2_demo_1', 'style'=>'width: 100%'), $lista_empresas, '', 'required'); ?>
              </div> 
              <div class="form-group mb-4">
                  <input class="form-control" type="text" name="numero_processo" placeholder="Digite o nº do processo">
              </div> 
              <div class="form-group mb-4">
                  <textarea class="form-control" rows="4" name="mensagem" placeholder="Observações"></textarea>
              </div>
          </div>
          <div class="modal-footer  bg-primary-50">           
              <button class="btn btn-warning btn-rounded mr-3" data-dismiss="modal" >Cancelar</button>
              <?php echo form_submit(array('name'=>'btn_cadastrar', 'class'=>'btn btn-primary btn-rounded mr-3'), 'Cadastrar'); ?> 
          </div>
          <?php echo form_close(); ?> 
      </div>
  </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('parecer'); ?>/"+filtro+"");
  });
});
</script>
<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
        toastr["success"]("O Processo foi cadastrado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["error"]("Ocorreu um erro ao cadastrar o processo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["success"]("O Processo foi excluído com sucesso!", "Excluído!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["error"]("Ocorreu um erro ao excluir o processo, por favor tente novamente.", "Erro!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );

});

$(".select2_demo_1").select2();
</script>