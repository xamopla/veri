<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Mensagens DEC</h2> 
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'TODAS'=>'Todas',
                  'LIDAS'=>'Lidas',
                  'NAO_LIDAS'=>'Não Lidas'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br>
            <div class="table-responsive row">
              <table class="table table-striped" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr>
                        <th style="font-size: 12px;">Razão Social</th>
                        <th style="font-size: 12px;">CNPJ</th>
                        <th style="font-size: 12px;">Mensagens</th>
                        <th></th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $cont = 1;
                    if ($consulta){ 
                      foreach ($consulta as $c){?> 
                      <tr>
                            <td style="text-align: left;"><?php echo $c->razao_social; ?></td>
                            <td ><?php echo $c->cnpj; ?></td>
                            <td style="text-align: center;">
                              <?php if($c->qtd_nao_lida != 0){
                                echo '<span class="badge badge-warning badge-shadow"><i class="ti-alert">&nbsp;</i>'.$c->qtd_nao_lida.' não lidas</span>';
                              }else{
                                echo '<span class="badge badge-success badge-shadow"><i class="ti-alert">&nbsp;</i>Todas lidas</span>';
                              }
                              
                              ?>
                            </td>

                            <td style="font-size: 12px;">  

                            <?php if($c->qtd_nao_lida != 0){
                              echo '<a href="javascript:void(0);" onclick="javascript:ler_mensagem(\''.$c->cnpj.'\')" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens no DEC" class="btn btn-default b" style="color: #1b6dc1; text-align: center;"><i class="material-icons" aria-hidden="true">email</i></a>';
                            }else{
                              echo '<a href="javascript:void(0)" onclick="javascript:abreModalEmpresa(&quot;'.$c->cnpj.'&quot;);" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens no DEC" class="btn btn-default b" style="color: #1b6dc1; text-align: center;"><i class="material-icons" aria-hidden="true">email</i></a>';
                            }
                            
                            ?>

                                        
                            </td> 
                            
                      </tr>
                  <?php $cont++; } 
                  }
                  ?>
                  </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_mensagens_empresa">
    <div class="modal-dialog" role="document" style="width:1000px !important">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagens</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <input type="hidden" style="display:none" name="banco" id="banco">
                <input type="hidden" style="display:none" name="cnpj" id="cnpj">
            </div>
            <div class="modal-body">
                <table id="modal_table" class="table table-striped" style="width: 100% !important">    
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th></th>
                          <th></th>
                          <th>Assunto</th>
                          <th>Tipo</th>
                          <th>Data de Envio</th>
                          <th>Data de Ciência</th>
                          <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>                
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('dec/listar_dec_recebidas'); ?>/"+filtro+"");
  });
});
</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
var table = $('#example').DataTable({
        "order": [[ 0, "desc" ]],
        "iDisplayLength": 50,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );


});


function ler_mensagem(cnpj){
  // abreModalEmpresa(cnpj);
  swal({
        title: 'Buscar Mensagens no DEC ?',
        text: 'Ao realizar essa operação será informado todas as mensagens não lidas e dado ciência na mensagens!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#0CC27E',
        cancelButtonColor: '#FF586B',
        confirmButtonText: 'Sim!',
        cancelButtonText: 'Não, cancelar!',
        confirmButtonClass: 'btn btn-success btn-raised mr-5',
        cancelButtonClass: 'btn btn-danger btn-raised',
        buttonsStyling: false
      }).then(function () {       
      
        swal({ 
            title: "Buscando...",
            text: 'Buscando Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });

        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/Dec/cron_caixapostal_procuracao_individual/";
        var request = $.post(url, { cnpj: cnpj, banco: banco }, function(data, status){

            if(data.includes("ERRO")){

              $(document).ready(function() {                
                  swal({
                      title: "Erro ao buscar mensagem !",
                      text: "Erro",
                      type: "error"
                  });
              });

            }else{
              swal.close();
              abreModalEmpresa(cnpj);
            }
        });
                
      }, function (dismiss) {
        if (dismiss === 'cancel') {
          swal(
            'Cancelado',
            'Processo cancelado!',
            'error'
          ).done();
        }
      }).done();
}
</script>

<script type="text/javascript">
  function abreModalEmpresa(cnpj){

    var variaveis = {cnpj : cnpj};

      $.ajax({
        type: "POST",
        url: '<?php echo base_url('dec/buscar_mensagens_dec_modal'); ?>',
        data: variaveis,
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if(data == "") {
                var array = [];
                popularDataTable(array);  
                jQuery('[data-toggle="popover"]').popover();   
                jQuery('[data-toggle="tooltip"]').tooltip();    

                $('#modal_mensagens_empresa').modal('show');          
            }else{
              var array = json2array(data.consulta);
              popularDataTable(array);
              jQuery('[data-toggle="popover"]').popover();
              jQuery('[data-toggle="tooltip"]').tooltip(); 

               $('#modal_mensagens_empresa').modal('show');
            }
        }
      });

  }

function popularDataTable(json){
      $.fn.dataTable.moment('DD/MM/YYYY');
      $('#modal_table').DataTable({
            "iDisplayLength": 50,
            
            lengthChange: false,
            "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
          });
    }


    function json2array(data){
      var string = "";
      var array = [];
      var arrayMultiple = [];

      for(var i in data){
        array = [];

        if(data[i].tipo == 1){
          if(data[i].lida_por == undefined){
            data[i].lida_por = "";
          }

          if(data[i].lida == 1){
            array.push('<td style="text-align: left;"><img title="Mensagem lida" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/aaMsgLida.gif'); ?>" style="border-width:0px;"></td>');
            array.push('<td><span class="badge badge-success badge-shadow" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Lida" data-content="'+data[i].lida_por+'" ><i class="ti-alert">&nbsp;</i>Lida</span> </td>');
            
          }else{
            array.push('<td style="text-align: left;"><img title="Mensagem não lida" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/aaMsgNaoLida.gif'); ?>" style="border-width:0px;"></td>');
            array.push('<td><span class="badge badge-warning badge-shadow" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Não Lida" data-content="'+data[i].lida_por+'" ><i class="ti-alert">&nbsp;</i>Não lida</span> </td>');
          }

          array.push('<td style="text-align: left;">'+data[i].assunto+'</td>');

          if(data[i].tipo == 1){
            array.push('<span class="badge badge-info badge-shadow" ><i class="ti-alert">&nbsp;</i>'+data[i].codigo_tipo+'</span>');
          }else{
            array.push('<span class="badge badge-success badge-shadow" style="background-color:#cf30009e"><i class="ti-alert">&nbsp;</i>'+data[i].codigo_tipo+'</span>');
          }

          array.push('<td style="text-align: left;">'+data[i].data+'</td>');
          array.push('<td style="text-align: left;">'+data[i].data_ciencia+'</td>');


          if(data[i].path_anexo != null && data[i].path_anexo != "" && data[i].path_anexo != undefined){
            array.push('<td ><a href="https://veri-sp.com.br/'+data[i].path_anexo+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens no DEC" class="btn btn-default b" style="color: #1b6dc1; text-align: center;"><i class="material-icons" aria-hidden="true">email</i></a> </td>');
          }else{
            array.push('<td><a onclick="javascript:ler_mensagem_final(&quot;'+data[i].cnpj+'&quot;, &quot;'+data[i].id+'&quot;, &quot;'+data[i].tipo+'&quot;)" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens no DEC" class="btn btn-default b" style="color: #1b6dc1; text-align: center;"><i class="material-icons" aria-hidden="true">email</i></a></td>');
            
          }

          arrayMultiple.push(array);
        }
        
      }
      
      return arrayMultiple;
    }
</script>

<script type="text/javascript">
  function ler_mensagem_final(cnpj, id, tipo){
    swal({ 
              title: "Buscando...",
              text: 'Buscando Mensagem.\nPor favor, aguardar alguns segundos...',
              type: "info" ,
              confirmButtonText: "Cancelar",
              confirmButtonColor: "#fff"
          },function(isConfirm){
              if(isConfirm){
                  request.abort();
              }
          });

          var banco = <?php echo "'".$banco."'"; ?>;
          url = "<?php echo base_url("crons-api/dec_ler/ler_function/"); ?>";
          // url = "https://veri.com.br/crons-api/dctf_declaracao/buscar_declaracao/";
          var request = $.post(url, { cnpj: cnpj, id : id, tipo: tipo, banco: banco }, function(data, status){

              if(data.includes("ERRO")){

                $(document).ready(function() {                
                    swal({
                        title: "Erro ao buscar mensagem !",
                        text: "Erro",
                        type: "error"
                    });
                });

              }else{
                swal.close();
                window.open(""+data, '_blank');
                // setTimeout(reload_page, 5000);
              }
          });
  }
</script>



