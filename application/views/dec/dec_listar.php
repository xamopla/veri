<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Mensagens DEC</h2> 
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'TODAS'=>'Todas',
                  'LIDAS'=>'Lidas',
                  'NAO_LIDAS'=>'Não Lidas',
                  'COMUNICADOS'=>'Comunicados',
                  'MENSAGENS'=>'Mensagens'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br>
            <div class="table-responsive row">
              <table class="table table-striped" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr>
                        <th></th>
                        <th style="font-size: 12px;">Razão Social</th>
                        <th style="font-size: 12px;">CNPJ</th>
                        <th style="font-size: 12px;">Assunto</th>
                        <th style="font-size: 12px;">Tipo</th>
                        <th style="font-size: 12px;">Data de Envio</th>
                        <th style="font-size: 12px;">Data de Ciência</th>
                        <th style="font-size: 12px;">Status</th>
                        <th></th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $cont = 1;
                    if ($consulta){ 
                      foreach ($consulta as $c){?> 
                      <tr>
                            <td ><?php echo $c->dataemail; ?></td>
                            <td style="text-align: left;"><?php echo $c->razao_social; ?></td>
                            <td ><?php echo $c->cnpj; ?></td>

                            <td ><?php echo $c->assunto; ?></td>
                            <td >
                              <?php if($c->tipo == 1){
                                echo '<span class="badge badge-info badge-shadow" ><i class="ti-alert">&nbsp;</i>'.$c->codigo_tipo.'</span>';
                              }else{
                                echo '<span class="badge badge-success badge-shadow" style="background-color:#cf30009e"><i class="ti-alert">&nbsp;</i>'.$c->codigo_tipo.'</span>';
                              }
                              
                              ?>
                              
                                
                            </td>
                            <td ><?php echo $c->data; ?></td>
                            <td ><?php echo $c->data_ciencia; ?></td>

                            <td style="text-align: center;">
                              <?php if($c->lida == 0){
                                echo '<span class="badge badge-danger badge-shadow"><i class="ti-alert">&nbsp;</i>Não lida</span>';
                              }else{
                                echo '<span class="badge badge-success badge-shadow"><i class="ti-alert">&nbsp;</i>Lida</span>';
                              }
                              
                              ?>
                            </td>

                            <?php if(empty($c->path_anexo)){ ?>
                              <td style="font-size: 12px;">
                             
                               <a onclick="javascript:ler_mensagem('<?php echo $c->cnpj; ?>', '<?php echo $c->id; ?>', <?php echo $c->tipo; ?>)" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens no DEC" class="btn btn-default b" style="color: #1b6dc1; text-align: center;"><i class="material-icons" aria-hidden="true">email</i></a> 

            
                              </td>
                            <?php }else{ ?>
                              <td style="font-size: 12px;">
                             
                               <a href="<?php echo 'https://veri-sp.com.br/'.$c->path_anexo; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Consultar Mensagens no DEC" class="btn btn-default b" style="color: #1b6dc1; text-align: center;"><i class="material-icons" aria-hidden="true">email</i></a> 

            
                              </td>
                            <?php } ?>
                            
                      </tr>
                  <?php $cont++; } 
                  }
                  ?>
                  </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('dec/listar'); ?>/"+filtro+"");
  });
});
</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
var table = $('#example').DataTable({
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        "iDisplayLength": 50,
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );


});


function ler_mensagem(cnpj, id, tipo){
  swal({ 
            title: "Buscando...",
            text: 'Buscando Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });

        var banco = <?php echo "'".$banco."'"; ?>;
        url = "<?php echo base_url("crons-api/dec_ler/ler_function/"); ?>";
        // url = "https://veri.com.br/crons-api/dctf_declaracao/buscar_declaracao/";
        var request = $.post(url, { cnpj: cnpj, id : id, tipo: tipo, banco: banco }, function(data, status){

            if(data.includes("ERRO")){

              $(document).ready(function() {                
                  swal({
                      title: "Erro ao buscar mensagem !",
                      text: "Erro",
                      type: "error"
                  });
              });

            }else{
              swal.close();
              window.open("https://veri-sp.com.br/"+data, '_blank');
              // setTimeout(reload_page, 5000);
            }
        });
}
</script>



