<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>

<!-- EXPORT TABLE -->
<script src="<?php echo base_url('assets/plugins/datatable/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/buttons.colVis.min.js'); ?>"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.20/dataRender/ellipsis.js"></script>

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<?php
$links_notas_detalhadas_destinatario = "";
if(isset($notas_destinatario)){
    foreach ($notas_destinatario as $value){
        $links_notas_detalhadas_destinatario .= $value['link_nota_detalhada'] . ';';
    }

}
?>

<div class="page-body">
    <div class="container-fluid">
        <br>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Extrato de Notas Fiscais</h5>
                    </div>
                    <div class="card-body">

                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Escolha o extrato correspondente e clique em consultar:</div>
                                <br>
                            </div>
                            <div class="ibox-body">
                                <!-- TABS -->
                                <ul class="nav nav-pills nav-pills-rounded nav-pills-air nav-pills-success">
                                    <!-- <li class="nav-item">
                                        <a class="nav-link active" href="#tab-1-1" data-toggle="tab">Extrato NFC-e (consumidor final)</a>
                                    </li> -->

                                    <div style="display: none;">
                                        <form id="submit3" action="https://nfe.sefaz.ba.gov.br/servicos/nfce/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfce%2fModulos%2fAutenticado%2fNFCEC_consulta_nfce.aspx" method="post" target="_blank">
                                            <input type="hidden" name="ctl00$ScriptManager2" value="ctl00$updPanel|ctl00$PHCentro$btnLogin">
                                            <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
                                            <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
                                            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTc5NjIwMjMwNg9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFBU5GQ0VDZAIFDxYCHwAFQk5vdGEgRmlzY2FsIGRlIENvbnN1bWlkb3IgRWxldHImIzI0NDtuaWNhIC0gQXV0ZW50aWNhJiMyMzE7JiMyMjc7b2QCCQ9kFgJmD2QWAgIBD2QWBgIDDw8WCB4IQ3NzQ2xhc3MFGCBtb2RhbCBmYWRlICBtb2RhbCBmYWRlIB4wX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUc8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgRXJybx4yX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHgRfIVNCAgIWCh4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljZAIHD2QWCgIBDxYCHghkaXNhYmxlZGRkAgMPFgIfCmRkAgcPFgIfAGVkAgkPDxYCHgdWaXNpYmxlZ2RkAgsPDxYCHwtnZGQCCQ8WAh8ABa8EPGgxPkluc3RydcOnw7Vlczo8L2gxPg0KPGJyIC8+DQogICAgDQo8dWw+DQogICAgPGxpPg0KICAgICAgICBTZXLDoSBwZXJtaXRpZG8gYXBlbmFzIG8gYWNlc3NvIGNvbSBsb2dpbiBlIHNlbmhhOw0KICAgIDwvbGk+DQogICAgPGxpPg0KICAgICAgICBBcMOzcyAwMyB0ZW50YXRpdmFzIGludsOhbGlkYXMgYSBww6FnaW5hIGRlIExvZ2luIGlyw6EgYmxvcXVlYXIgbyBhY2Vzc28gZG8gSVAgZGUgb3JpZ2VtIHBvciAxMCBtaW51dG9zOw0KICAgIDwvbGk+DQogICAgPGxpPg0KICAgICAgICBDYXNvIG7Do28gbGVtYnJlIGEgc2VuaGEsIGNsaXF1ZSBlbSAiZXNxdWVjZXUgYSBzZW5oYT8iIGUgYXDDs3MgaW5mb3JtYcOnw6NvIGRvcyBkYWRvcyBuZWNlc3PDoXJpb3MsIG8gbGVtYnJldGUgc2Vyw6EgZW52aWFkbyBwYXJhIG8gZW1haWwgY2FkYXN0cmFkby4NCiAgICA8L2xpPg0KPC91bD4NCg0KPGJyIC8+DQo8aDU+RW0gY2FzbyBkZSBkw7p2aWRhcyBlbnRyZSBlbSBjb250YXRvIGF0cmF2w6lzIGRvIGZhbGVjb25vc2NvQHNlZmF6LmJhLmdvdi5iciBvdSAwODAwIDA3MTAwNzE8L2g1PmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFGCBtb2RhbCBmYWRlICBtb2RhbCBmYWRlIB8EAgIWCh8FBQItMR8GBQZkaWFsb2cfBwUEdHJ1ZR8IBQVmYWxzZR8JBQZzdGF0aWNkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFDkFTTElCOiAyLjIuMC4wZGRqxB+SWPWDuKMcj6mrBS2LEoqy6BpmrDh6qC2e7pnPig==">
                                            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="90F7DD38">
                                            <input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="jLwj-VQytxJwrpXCvPrGB0lezh-MS7_1FR0SKQ3ZuiNj13JrFcjVexIDBH2KIC47wZRsAcTgHIhc_ZAoqiP_cq5nlrxNI2Q6G94JnbGpx-dTX6BuI_ISO980fbIuJEO9n7a7GhS4rKg5bMqePdqNIMdLPGnp2AXfiBZ8uD5HKbk1">
                                            <input type="hidden" name="__EVENTVALIDATION" value="/wEdAAoWkNB48YT+5az/Q/bWna+MIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV22ePxwj+z1HBvcACeFv2P9vvHnxbAWqAg5IOPbQ8N8U=">
                                            <input type="hidden" name="__ASYNCPOST" value="false">
                                            <input type="hidden" name="ctl00$PHCentro$userLogin" is="PHCentro_userLogin" value="<?php echo $empresa->login_sefaz; ?>" >
                                            <input type="hidden" name="ctl00$PHCentro$userPass" value="<?php echo $empresa->senha_sefaz; ?>" >
                                            <input type="submit" id="btn-submit3" class="btn btn-primary" name="ctl00$PHCentro$btnLogin" value="Entrar">
                                        </form>
                                    </div>

                                    <div style="display: none;">
                                        <form id="submit_alternativo" action="https://nfe.sefaz.ba.gov.br/servicos/nfce/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfce%2fModulos%2fAutenticado%2fNFCEC_consulta_nfce.aspx" method="post" target="_blank">
                                            <input type="hidden" name="ctl00$ScriptManager2" value="ctl00$updPanel|ctl00$PHCentro$btnLogin">
                                            <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
                                            <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
                                            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTc5NjIwMjMwNg9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFBU5GQy1lZAIFDxYCHwAFQk5vdGEgRmlzY2FsIGRlIENvbnN1bWlkb3IgRWxldHImIzI0NDtuaWNhIC0gQXV0ZW50aWNhJiMyMzE7JiMyMjc7b2QCCQ9kFgJmD2QWAgIBD2QWBgIDDw8WCB4IQ3NzQ2xhc3MFGCBtb2RhbCBmYWRlICBtb2RhbCBmYWRlIB4wX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUc8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgRXJybx4yX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHgRfIVNCAgIWCh4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljZAIHD2QWCgIBDxYCHghkaXNhYmxlZGRkAgMPFgIfCmRkAgcPFgIfAGVkAgkPDxYCHgdWaXNpYmxlZ2RkAgsPDxYCHwtnZGQCCQ8WAh8ABa8EPGgxPkluc3RydcOnw7Vlczo8L2gxPg0KPGJyIC8+DQogICAgDQo8dWw+DQogICAgPGxpPg0KICAgICAgICBTZXLDoSBwZXJtaXRpZG8gYXBlbmFzIG8gYWNlc3NvIGNvbSBsb2dpbiBlIHNlbmhhOw0KICAgIDwvbGk+DQogICAgPGxpPg0KICAgICAgICBBcMOzcyAwMyB0ZW50YXRpdmFzIGludsOhbGlkYXMgYSBww6FnaW5hIGRlIExvZ2luIGlyw6EgYmxvcXVlYXIgbyBhY2Vzc28gZG8gSVAgZGUgb3JpZ2VtIHBvciAxMCBtaW51dG9zOw0KICAgIDwvbGk+DQogICAgPGxpPg0KICAgICAgICBDYXNvIG7Do28gbGVtYnJlIGEgc2VuaGEsIGNsaXF1ZSBlbSAiZXNxdWVjZXUgYSBzZW5oYT8iIGUgYXDDs3MgaW5mb3JtYcOnw6NvIGRvcyBkYWRvcyBuZWNlc3PDoXJpb3MsIG8gbGVtYnJldGUgc2Vyw6EgZW52aWFkbyBwYXJhIG8gZW1haWwgY2FkYXN0cmFkby4NCiAgICA8L2xpPg0KPC91bD4NCg0KPGJyIC8+DQo8aDU+RW0gY2FzbyBkZSBkw7p2aWRhcyBlbnRyZSBlbSBjb250YXRvIGF0cmF2w6lzIGRvIGZhbGVjb25vc2NvQHNlZmF6LmJhLmdvdi5iciBvdSAwODAwIDA3MTAwNzE8L2g1PmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFGCBtb2RhbCBmYWRlICBtb2RhbCBmYWRlIB8EAgIWCh8FBQItMR8GBQZkaWFsb2cfBwUEdHJ1ZR8IBQVmYWxzZR8JBQZzdGF0aWNkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFDkFTTElCOiAyLjIuMC4wZGQBzya8KmhOAnMz9DlZvCBUfcq7EdJHzmRRoQQS4sqUDA==">
                                            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="90F7DD38">
                                            <input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="IcgkfHEKJWIvSCpzDzPf3RBR4OIJD8qURFb4Visg01tMagc0R1i7a19RmD78gFWsFGGKcCk4Vepx4tlS6vYQNIQJjsqd2EgoVTqOFZAPqL7WNL7nzkd-Qi2NsNb8ZgaLAM8uHdILpzQXw3f_Gz6AAqpxy4g7ItSv-2QlVEyGiaw1">
                                            <input type="hidden" name="__EVENTVALIDATION" value="/wEdAAqCUEA7NOYtWQddMygwSrHVIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV3CQDkITdJYDN7roIMu9PqOP00yaNykodS/4Xw1GmPRc=">
                                            <input type="hidden" name="__ASYNCPOST" value="false">
                                            <input type="hidden" name="ctl00$PHCentro$userLogin" is="PHCentro_userLogin" value="<?php echo $empresa->login_sefaz; ?>" >
                                            <input type="hidden" name="ctl00$PHCentro$userPass" value="<?php echo $empresa->senha_sefaz; ?>" >
                                            <input type="submit" id="btn-submit_alternativo" class="btn btn-primary" name="ctl00$PHCentro$btnLogin" value="Entrar">
                                        </form>
                                    </div>

                                    <li class="nav-item">
                                        <a class="nav-link active" href="#tab-1-3" id="link_compras" data-toggle="tab">Extrato NF-e - Compras</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab-1-2" id="link_vendas" data-toggle="tab">Extrato NF-e - Vendas</a>
                                    </li>

                                    

                                </ul>
                                <!-- FIM DA SELEÇÃO DAS TABS -->

                                <!-- CONTEÚDO DAS TABS -->
                                <div class="tab-content">

                                    <!-- TAB 1 -->
                                    <div class="tab-pane fade" id="tab-1-1">
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-4">
                                                    <label for="nome">Empresa:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->razao_social, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label for="nome">CNPJ:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->cnpj_completo, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label for="nome">Inscrição Estadual:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ibox-footer text-center">
                                            <h1 class="my-3" style="color: red; font-size: 14px;"><b>Se "Consulta 1" não funcionar por favor, tente "Consulta 2".</b></h1>
                                            <?php echo '<a href="#" data-toggle="tooltip" data-placement="top" title="Consultar Notas para Consumidor Final" class="btn btn-primary mr-2" onclick="consultaNotasConsumidorFinal()">Consulta 1</a>'; ?>
                                            <?php echo '<a href="#" data-toggle="tooltip" data-placement="top" title="Consultar Notas para Consumidor Final" class="btn btn-primary mr-2" onclick="consultaNotasConsumidorFinalAlternativo()">Consulta 2</a>'; ?>


                                        </div>
                                    </div>
                                    <!-- FIM DA TAB 1 -->

                                    <!-- TAB 2 -->
                                    <div class="tab-pane fade" id="tab-1-2">
                                        <br>
                                        <div class="text-center" >
                                            <h1 style="font-size: 28px;">Consulta Manual</h1>
                                            <h1 class="my-3" style="color: red; font-size: 14px;"><b>Se "Consulta 1" não funcionar por favor, tente "Consulta 2".</b></h1>
                                            <a href="<?php echo base_url("consulta_nfe/consultar_emitidas_nfe/$empresa->id"); ?>" data-toggle="tooltip" data-placement="top" title="Consultar Notas Emitidas" class="btn btn-primary mr-2" target="_blank">Consulta 1</a>
                                            <a href="<?php echo base_url("consulta_nfe/consultar_emitidas_nfe_alternativo/$empresa->id"); ?>" data-toggle="tooltip" data-placement="top" title="Consultar Notas Emitidas" class="btn btn-primary mr-2" target="_blank">Consulta 2</a>
                                        </div>
                                        <br>
                                        <br>
                                        <hr>
                                        <div class="text-center" >
                                            <h1 style="font-size: 28px;">Consulta Automática</h1>
                                            <br>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-4">
                                                    <label for="nome">Empresa:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->razao_social, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group mb-4">
                                                    <label for="nome">CNPJ:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->cnpj_completo, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group mb-4">
                                                    <label for="nome">Inscrição Estadual:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group mb-4">
                                                    <?php if ($empresa->situacao == 'Credenciado'): ?>

                                                        <label for="nome">Situação Ant. Parcial: <?php echo "<span class='fa fa-check' data-toggle='tooltip' data-html='true' title='$empresa->motivo'></span>"; ?></label>
                                                        <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual', 'style'=>'background-color:#1fb51f;color:white;'), $empresa->situacao, 'readonly'); ?>

                                                    <?php else: ?>

                                                        <label for="nome">Situação Ant. Parcial: <?php echo "<span class='fa fa-warning' data-toggle='tooltip' data-html='true' title='MOTIVO <br> $empresa->motivo'></span>"; ?></label>

                                                        <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual', 'style'=>'background-color:#dc4012;color:white;'), $empresa->situacao, 'readonly'); ?>

                                                    <?php endif ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <?php echo form_open_multipart("http://191.252.193.202/sistema/nfe_consulta/consultar_sefaz_emitente", array('role'=>'form', 'target'=>'_blank')); ?> -->

                                        <?php echo form_open_multipart("nfe_consulta/consultar_sefaz_emitente", array('role'=>'form')); ?>

                                        <input type="hidden" name="id_empresa" value="<?php echo $empresa->id; ?>">
                                        <input type="text" name="login" id="login" value="<?php echo $empresa->login_sefaz; ?>" hidden>
                                        <input type="text" name="pass" id="pass" value="<?php echo $empresa->senha_sefaz; ?>" hidden>
                                        <input type="text" name="cnpj" value="<?php echo $empresa->cnpj; ?>" hidden>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label for="logo">Período Inicial*:</label>
                                                    <br>
                                                    <input type="text" name="data_ini" value="" id="date_1" class="datepicker-here form-control digits" data-language="en" placeholder="" style="border-color: #5c6bc0;" required="true">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group mb-4">
                                                    <label for="nome">Período Final*:</label>
                                                    <input type="text" name="data_fim" value="" id="date_2" class="datepicker-here form-control digits" data-language="en" placeholder="" style="border-color: #5c6bc0;" required="true">
                                                </div>
                                            </div>

                                            <div class="col-md-3" style="display: none">
                                                <div class="form-group mb-4">
                                                    <label for="logo">Anexo*: <span class="fa fa-question-circle" data-toggle="tooltip" title="Você pode anexar um documento txt com as Chaves de Acesso para que o sistema verifique se a nota foi encontrada."></span></label>
                                                    <br>

                                                    <label class=" file-input mr-3">
                                                        <input name="csv_notas" class="file" type="file" accept=".txt" id="arquivo_saida">
                                                    </label>
                                                    <script type="text/javascript">
                                                        $('#arquivo_saida').change(function() {
                                                            document.getElementById("span_arquivo_saida").innerHTML = '<i class="la la-upload"></i>'+$('#arquivo_saida')[0].files[0].name;
                                                        });
                                                    </script>
                                                    <small class="text-muted">Extensões permitidas: .txt</small>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="ibox-footer text-center">
                                            <button type="submit" class="btn btn-primary mr-2">Consultar Notas</button>
                                        </div>
                                        <?php echo form_close(); ?>

                                        <?php if(isset($notas_emitentes)){ ?>

                                            <div>
                                                <div class="ibox-head">
                                                    <div class="ibox-title">Notas Encontradas</div>
                                                    <div>
                                                        <span class="btn btn-success btn-icon-only btn-circle" data-toggle="button" style="height: 25px;;width: 25px;margin-right: 8px;"></span>
                                                        NF-e conferida
                                                        <span class="fa fa-question-circle" data-toggle="tooltip" title="" data-original-title="Você pode filtrar a tabela com o termo (Conferida) para encontrá-las."></span>&nbsp;
                                                        <span class="btn btn-warning btn-icon-only btn-circle" data-toggle="button" style="height: 25px;;width: 25px;margin-right: 8px;"></span>
                                                        NF-e não localizada
                                                        <span class="fa fa-question-circle" data-toggle="tooltip" title="" data-original-title="Você pode filtrar a tabela com o termo (Não Localizada) para encontrá-las."></span>&nbsp;
                                                        <br>
                                                        <div id="totalNotas" style="margin-top: 7px; font-size: 18px;"><strong>Nº Notas: </strong></div>
                                                        <div id="totalValor" style="margin-top: 7px; font-size: 18px;"><strong>Valor Total: </strong></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="table-responsive">
                                                <table id="tabela_saida" class="table table-bordered table-hover table-striped" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nº NFE</th>
                                                        <th>Localizada</th>
                                                        <th>CNPJ/CPF</th>
                                                        <th>Razão Social</th>
                                                        <th>Emissão</th>
                                                        <th>Valor</th>
                                                        <th>Chave Acesso</th>
                                                        <th>Situação</th>
                                                        <th>Tipo Operação</th>
                                                        <th>Usuário Relatório</th>
                                                        <th>Data Relatório</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $n = 0;
                                                    foreach ($notas_emitentes as $nota){
                                                        if($nota['status']=='found_sefaz_csv' || $nota['status']=='not_found_csv') {
                                                            $n++;
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php if($nota['status']=='found_sefaz_csv'){ ?>
                                                                        <span class="notas badge badge-success badge-pill"><?= $nota['doc_sefaz']; ?></span>
                                                                    <?php }elseif ($nota['status']=='not_found_csv'){ ?>
                                                                        <span class="notas badge badge-warning badge-pill"><?= $nota['doc_sefaz']; ?></span>
                                                                    <?php } ?>
                                                                </td>
                                                                <td>
                                                                    <?php if($nota['status']=='found_sefaz_csv'){ ?>
                                                                        Conferida
                                                                    <?php }elseif ($nota['status']=='not_found_csv'){ ?>
                                                                        Não Localizada
                                                                    <?php } ?>
                                                                </td>
                                                                <td><?= $nota['CNPJ/CPF']; ?></td>
                                                                <td><?= $nota['razao_social_dest']; ?></td>
                                                                <td><?= $nota['emissao']; ?></td>
                                                                <td class="valor"><?= $nota['valor']; ?></td>
                                                                <td><?= $nota['chave_acesso']; ?></td>
                                                                <td><?= $nota['situacao']; ?></td>
                                                                <td><?= $nota['tipo_operacao']; ?></td>
                                                                <td><?= $nota['user_report']; ?></td>
                                                                <td><?= $nota['data_report']; ?></td>
                                                            </tr>
                                                        <?php }} ?>

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Nº NFE</th>
                                                        <th>Localizada</th>
                                                        <th>CNPJ/CPF</th>
                                                        <th>Razão Social</th>
                                                        <th>Emissão</th>
                                                        <th>Valor</th>
                                                        <th>Chave Acesso</th>
                                                        <th>Situação</th>
                                                        <th>Tipo Operação</th>
                                                        <th>Usuário Relatório</th>
                                                        <th>Data Relatório</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>

                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="ibox">
                                                        <div class="ibox-head">
                                                            <div class="ibox-title">Notas Não Encontradas</div>
                                                        </div>
                                                        <div class="ibox-body">
                                                            <table class="table table-bordered">
                                                                <tbody>

                                                                <?php foreach ($notas_emitentes as $nota){ if($nota['status']=='not_found_sefaz'){ ?>
                                                                    <tr>
                                                                        <td><?= $nota['doc']; ?></td>
                                                                    </tr>
                                                                <?php } } ?>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <script type="text/javascript">

                                                //document.querySelectorAll('a.nav-link')[8].click();

                                                function numberToReal(numero) {
                                                    var numero = numero.toFixed(2).split('.');
                                                    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
                                                    return numero.join(',');
                                                }

                                                var valor = parseFloat(0);
                                                var valores = document.getElementsByClassName('valor');
                                                for(var i=0; i<valores.length; i++){
                                                    valor += parseFloat(document.getElementsByClassName('valor')[i].textContent);
                                                }
                                                document.getElementById('totalNotas').innerHTML = "<strong>Nº Notas: </strong>"+document.getElementsByClassName('notas').length;
                                                document.getElementById('totalValor').innerHTML = "<strong>Valor Total: </strong>R$ "+numberToReal(valor);

                                                $('#tabela_saida thead tr').clone(true).appendTo( '#tabela_saida thead' );
                                                $('#tabela_saida thead tr:eq(1) th').each( function (i) {
                                                    var title = $(this).text();
                                                    $(this).html( '<input type="text" placeholder="'+title+'" />' );

                                                    $( 'input', this ).on( 'keyup change', function () {
                                                        if ( table.column(i).search() !== this.value ) {
                                                            table
                                                                .column(i)
                                                                .search( this.value )
                                                                .draw();
                                                        }
                                                    } );
                                                } );
                                                var table = $('#tabela_saida').DataTable({
                                                    "language": {
                                                        "sEmptyTable": "Nenhum registro encontrado",
                                                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                                                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                                        "sInfoPostFix": "",
                                                        "sInfoThousands": ".",
                                                        "sLengthMenu": "_MENU_ resultados por página&nbsp;&nbsp;",
                                                        "sLoadingRecords": "Carregando...",
                                                        "sProcessing": "Processando...",
                                                        "sZeroRecords": "Nenhum registro encontrado",
                                                        "sSearch": "Pesquisar",
                                                        "oPaginate": {
                                                            "sNext": "Próximo",
                                                            "sPrevious": "Anterior",
                                                            "sFirst": "Primeiro",
                                                            "sLast": "Último"
                                                        },
                                                        "oAria": {
                                                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                                                            "sSortDescending": ": Ordenar colunas de forma descendente"
                                                        }
                                                    },
                                                    "Columns":[
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        {"type": "date-br"}
                                                    ],
                                                    "columnDefs": [
                                                        {
                                                            "targets": [1],
                                                            "visible": false,
                                                        },
                                                        {
                                                            targets: 3,
                                                            render: $.fn.dataTable.render.ellipsis( 15, true )
                                                        },
                                                        {
                                                            "targets": [9],
                                                            "visible": false,
                                                        },
                                                        {
                                                            "targets": [10],
                                                            "visible": false,
                                                        }
                                                    ],
                                                    dom: 'Blfrtip',
                                                    buttons: [
                                                        {
                                                            extend: 'excelHtml5',
                                                            filename: 'Extrato NF-e Saída',
                                                            title: null,
                                                            text: 'Excel',
                                                            exportOptions: {
                                                                format: {
                                                                    body: function (data, row, column, node ) {
                                                                        var column_nome = "";
                                                                        if(column == 3) {
                                                                            try{
                                                                                column_nome = $(data).attr('title');
                                                                            }catch (e) {
                                                                                column_nome = data;
                                                                            }
                                                                        }
                                                                        return column === 6 ? "\0" + data : (column === 0 ? $(data).text(): (column === 3 ? column_nome : data));
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        {
                                                            extend: 'colvis',
                                                            text: 'Exibir/Ocultar Colunas'
                                                        }
                                                    ],
                                                    orderCellsTop: true,
                                                    fixedHeader: true,
                                                    "paging": true,
                                                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todas"]]
                                                });

                                            </script>
                                            <style type="text/css">
                                                thead input {
                                                    width: 100%;
                                                    padding: 3px;
                                                    box-sizing: border-box;
                                                }

                                                .buttons-excel{
                                                    margin-left: 10px;
                                                }
                                            </style>
                                        <?php } ?>
                                    </div>
                                    <!-- FIM DA TAB 2 -->

                                    <!-- TAB 3 -->
                                    <div class="tab-pane fade show active" id="tab-1-3">
                                        <br>
                                        <div class="text-center" >
                                            <h1 style="font-size: 28px;">Consulta Manual</h1>
                                            <h1 class="my-3" style="color: red; font-size: 14px;"><b>Se "Consulta 1" não funcionar por favor, tente "Consulta 2".</b></h1>
                                            <a href="<?php echo base_url("consulta_nfe/consultar_adquiridas_nfe/$empresa->id"); ?>" data-toggle="tooltip" data-placement="top" title="Consultar Notas Adquiridas" class="btn btn-primary mr-2" target="_blank">Consulta 1</a>
                                            <a href="<?php echo base_url("consulta_nfe/consultar_adquiridas_nfe_alternativo/$empresa->id"); ?>" data-toggle="tooltip" data-placement="top" title="Consultar Notas Adquiridas" class="btn btn-primary mr-2" target="_blank">Consulta 2</a>
                                        </div>
                                        <br>
                                        <br>
                                        <hr>
                                        <div class="text-center" >
                                            <h1 style="font-size: 28px;">Consulta Automática</h1>
                                            <br>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-4">
                                                    <label for="nome">Empresa:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->razao_social, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group mb-4">
                                                    <label for="nome">CNPJ:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->cnpj_completo, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group mb-4">
                                                    <label for="nome">Inscrição Estadual:</label>
                                                    <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group mb-4">
                                                    <?php if ($empresa->situacao == 'Credenciado'): ?>

                                                        <label for="nome">Situação Ant. Parcial: <?php echo "<span class='fa fa-check' data-toggle='tooltip' data-html='true' title='$empresa->motivo'></span>"; ?></label>
                                                        <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual', 'style'=>'background-color:#1fb51f;color:white;'), $empresa->situacao, 'readonly'); ?>

                                                    <?php else: ?>

                                                        <label for="nome">Situação Ant. Parcial: <?php echo "<span class='fa fa-warning' data-toggle='tooltip' data-html='true' title='MOTIVO <br> $empresa->motivo'></span>"; ?></label>

                                                        <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual', 'style'=>'background-color:#dc4012;color:white;'), $empresa->situacao, 'readonly'); ?>

                                                    <?php endif ?>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <?php echo form_open_multipart("http://191.252.193.202/sistema/nfe_consulta/consultar_sefaz_destinatario", array('role'=>'form', 'target'=>'_blank')); ?> -->

                                        <form action="<?= base_url() ?>nfe_consulta/consultar_sefaz_destinatario" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                                            <input type="hidden" name="id_empresa" value="<?php echo $empresa->id; ?>">
                                            <input type="text" name="login" value="<?php echo $empresa->login_sefaz; ?>" hidden>
                                            <input type="text" name="pass" value="<?php echo $empresa->senha_sefaz; ?>" hidden>
                                            <input type="text" name="cnpj" value="<?php echo $empresa->cnpj; ?>" hidden>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group mb-4">
                                                        <label for="logo">Período Inicial*:</label>
                                                        <br>
                                                        <input type="text" name="data_ini" value="" id="date_3" class="datepicker-here form-control digits" data-language="en" placeholder="" style="border-color: #5c6bc0;" required="true">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group mb-4">
                                                        <label for="nome">Período Final*:</label>
                                                        <input type="text" name="data_fim" value="" id="date_4" class="datepicker-here form-control digits" data-language="en" placeholder="" style="border-color: #5c6bc0;" required="true">
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="display: none">
                                                    <div class="form-group mb-4">
                                                        <label for="logo">Anexo*: <span class="fa fa-question-circle" data-toggle="tooltip" title="Você pode anexar um documento txt com as Chaves de Acesso para que o sistema verifique se a nota foi encontrada."></span></label>
                                                        <br>

                                                        <label class=" file-input mr-2" for="arquivo_compra">
                                                            <input name="csv_notas" class="file" type="file" accept=".txt" id="arquivo_compra">
                                                        </label>
                                                        <script type="text/javascript">
                                                            $('#arquivo_compra').change(function() {
                                                                document.getElementById("span_arquivo_compra").innerHTML = '<i class="la la-upload"></i>'+$('#arquivo_compra')[0].files[0].name;
                                                            });
                                                        </script>
                                                        <small class="text-muted" id="arquivo_txt_entrada">Extensões permitidas: .txt</small>

                                                    </div>
                                                </div>
                                            </div>


                                            <!--                                            <div class="text-center">-->
                                            <!--                                                <input type="checkbox" id="consultar_mdfe" name="consultar_mdfe" value="true">-->
                                            <!--                                                <label for="consultar_mdfe" style="color: red;">Consultar MDF-e de notas fora do Estado da BA e Intermunicipais</label>-->
                                            <!--                                            </div>-->

                                            <div class="ibox-footer text-center">
                                                <button type="submit" class="btn btn-primary mr-2">Consultar Notas</button>
                                                <?php if(isset($notas_destinatario)){ ?>
                                                    <button type="button" onclick="processar_mdfe_todas()" class="btn btn-secondary mr-2">Processar MDF-e (TODAS)</button>
                                                <?php } ?>
                                            </div>
                                        </form>

                                        <?php if(isset($notas_destinatario)){ ?>

                                            <div>
                                                <div class="ibox-head">
                                                    <div class="ibox-title">Notas Encontradas</div>
                                                    <div>
                                                        <span class="btn btn-success btn-icon-only btn-circle" data-toggle="button" style="height: 18px;;width: 18px;margin-right: 8px;"></span>
                                                        NF-e conferida
                                                        <span class="btn btn-warning btn-icon-only btn-circle" data-toggle="button"style="height: 18px;;width: 18px;margin-right: 8px;"></span>
                                                        NF-e não localizada
                                                        <span class="btn btn-danger btn-icon-only btn-circle" data-toggle="button"style="height: 18px;;width: 18px;margin-right: 8px;"></span>
                                                        MDF-e inexistente
                                                        <br>
                                                        <div id="totalNotas" style="margin-top: 7px; font-size: 18px;"><strong>Nº Notas: </strong></div>
                                                        <div id="totalValor" style="margin-top: 7px; font-size: 18px;"><strong>Valor Total: </strong></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="table-responsive">
                                                <table id="tabela_entrada" class="table table-bordered table-hover table-striped" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nº NFE</th>
                                                        <th>Localizada</th>
                                                        <th>CNPJ/CPF</th>
                                                        <th>Razão Social</th>
                                                        <th>Emissão</th>
                                                        <th style="width: 10px;">UF</th>
                                                        <th style="display: none">MDF-e</th>
                                                        <th>Data MDF-e</th>
                                                        <th>Valor</th>
                                                        <th>Chave Acesso</th>
                                                        <th>Situação</th>
                                                        <th>Tipo Operação</th>
                                                        <th>Usuário Relatório</th>
                                                        <th>Data Relatório</th>
                                                        <th>Cidade Emitente</th>
                                                        <th>Cidade Destinatário</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $n = 0;
                                                    foreach ($notas_destinatario as $nota){
                                                        if($nota['status']=='found_sefaz_csv' || $nota['status']=='not_found_csv') {
                                                            $n++;
                                                            ?>
                                                            <tr>
                                                                <input type="hidden" class="link_nota_detalhada" id="link-<?= $nota['chave_acesso']; ?>" value="<?= $nota['link_nota_detalhada']; ?>">
                                                                <input type="hidden" class="mdfe-status" id="mdfe-status-<?= $nota['chave_acesso']; ?>" value="<?= $nota['mdf_status']; ?>">
                                                                <td>
                                                                    <?php if($nota['status']=='found_sefaz_csv'){ ?>
                                                                        <span class="notas badge badge-success badge-pill"><?= $nota['doc_sefaz']; ?></span>
                                                                    <?php }elseif ($nota['status']=='not_found_csv'){ ?>
                                                                        <span class="notas badge badge-warning badge-pill"><?= $nota['doc_sefaz']; ?></span>
                                                                    <?php } ?>
                                                                </td>
                                                                <td>
                                                                    <?php if($nota['status']=='found_sefaz_csv'){ ?>
                                                                        Conferida
                                                                    <?php }elseif ($nota['status']=='not_found_csv'){ ?>
                                                                        Não Localizada
                                                                    <?php } ?>
                                                                </td>
                                                                <td><?= $nota['CNPJ/CPF']; ?></td>
                                                                <td><?= $nota['razao_social_dest']; ?></td>
                                                                <td><?= $nota['emissao']; ?></td>

                                                                <td id="uf-<?= $nota['chave_acesso']; ?>" class="
                                                                    <?php if(intval($nota['mdf_status']) == 2){ ?>
                                                                        uf_nfe badge badge-danger badge-shadow
                                                                    <?php }else{ ?>
                                                                        uf_nfe
                                                                    <?php } ?>"><?= $nota['uf']; ?>
                                                                </td>
                                                                <td style="display: none" id="mdfe-<?= $nota['chave_acesso']; ?>">
                                                                    <?php
                                                                        if($nota['mdf_status'] == 1){
                                                                            echo "MDF-e Encontrado ";
                                                                        }

                                                                        if($nota['mdf_status'] == 2){
                                                                            echo "MDF-e Inexistente ";
                                                                        }

                                                                        if($nota['uf'] == "BA"){
                                                                            echo " Aquisição dentro do estado ";
                                                                        }
                                                                    ?>
                                                                </td>
                                                                <td id="data_mdf-<?= $nota['chave_acesso']; ?>" class="data_mdf">
                                                                    <?php if(intval($nota['mdf_status']) == 1){ ?>
                                                                        <?= $nota['mdf_data']; ?>
                                                                    <?php }else{ ?>
                                                                        <a href="javascript:processar_mdfe('<?= $nota['chave_acesso'] ?>')">Processar MDF-e</a>
                                                                    <?php } ?>
                                                                </td>
                                                                <td class="valor"><?= $nota['valor']; ?></td>
                                                                <td>
                                                                    <form action="<?= base_url() ?>nfe_consulta/nota_destinatario" method="POST" target="_blank">
                                                                        <input type="text" name="login" value="<?php echo $empresa->login_sefaz; ?>" hidden>
                                                                        <input type="text" name="pass" value="<?php echo $empresa->senha_sefaz; ?>" hidden>
                                                                        <input type="text" name="chave_nfe" value="<?= $nota['chave_acesso']; ?>" hidden>
                                                                        <button class="tim"><?= $nota['chave_acesso']; ?></button>
                                                                    </form>
                                                                </td>
                                                                <td><?= $nota['situacao']; ?></td>
                                                                <td><?= $nota['tipo_operacao']; ?></td>
                                                                <td><?= $nota['user_report']; ?></td>
                                                                <td><?= $nota['data_report']; ?></td>
                                                                <td id="c_emitente-<?= $nota['chave_acesso']; ?>" class="c_emitente">
                                                                    <?= $nota['cidade_emitente']; ?>
                                                                </td>
                                                                <td id="c_destinatario-<?= $nota['chave_acesso']; ?>" class="c_destinatario">
                                                                    <?= $nota['cidade_destinatario']; ?>
                                                                </td>
                                                            </tr>
                                                        <?php }} ?>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Nº NFE</th>
                                                        <th>Localizada</th>
                                                        <th>CNPJ/CPF</th>
                                                        <th>Razão Social</th>
                                                        <th>Emissão</th>
                                                        <th>UF</th>
                                                        <th style="display: none">MDF-e</th>
                                                        <th>Data MDF-e</th>
                                                        <th>Valor</th>
                                                        <th>Chave Acesso</th>
                                                        <th>Situação</th>
                                                        <th>Tipo Operação</th>
                                                        <th>Usuário Relatório</th>
                                                        <th>Data Relatório</th>
                                                        <th>Cidade Emitente</th>
                                                        <th>Cidade Destinatário</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>

                                            <script type="text/javascript">

                                                function numberToReal(numero) {
                                                    var numero = numero.toFixed(2).split('.');
                                                    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
                                                    return numero.join(',');
                                                }

                                                var valor = parseFloat(0);
                                                var valores = document.getElementsByClassName('valor');
                                                for(var i=0; i<valores.length; i++){
                                                    valor += parseFloat(document.getElementsByClassName('valor')[i].textContent);
                                                }
                                                document.getElementById('totalNotas').innerHTML = "<strong>Nº Notas: </strong>"+document.getElementsByClassName('notas').length;
                                                document.getElementById('totalValor').innerHTML = "<strong>Valor Total: </strong>R$ "+numberToReal(valor);

                                                $('#tabela_entrada thead tr').clone(true).appendTo( '#tabela_entrada thead' );
                                                $('#tabela_entrada thead tr:eq(1) th').each( function (i) {
                                                    var title = $(this).text();
                                                    $(this).html( '<input type="text" placeholder="'+title+'" />' );

                                                    $( 'input', this ).on( 'keyup change', function () {
                                                        if ( table.column(i).search() !== this.value ) {
                                                            table
                                                                .column(i)
                                                                .search( this.value )
                                                                .draw();
                                                        }
                                                    } );
                                                } );


                                                //document.querySelectorAll('a.nav-link')[9].click();
                                                var ufs      = document.getElementsByClassName('uf_nfe');
                                                var data_mdf = document.getElementsByClassName('data_mdf');
                                                var emitentes = document.getElementsByClassName('c_emitente');
                                                var destinatarios = document.getElementsByClassName('c_destinatario');

                                                for(var i=0; i<ufs.length; i++){
                                                    if(ufs[i].textContent != "BA" && data_mdf[i].textContent.length == 0){
                                                        ufs[i].setAttribute("class", "uf_nfe badge badge-danger badge-shadow");
                                                    }

                                                    if( (emitentes[i].textContent != destinatarios[i].textContent) && data_mdf[i].textContent.length == 0){
                                                        ufs[i].setAttribute("class", "uf_nfe badge badge-danger badge-shadow");
                                                    }
                                                }

                                                var table = $('#tabela_entrada').DataTable({
                                                    "pageLength": -1,
                                                    "language": {
                                                        "sEmptyTable": "Nenhum registro encontrado",
                                                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                                                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                                        "sInfoPostFix": "",
                                                        "sInfoThousands": ".",
                                                        "sLengthMenu": "_MENU_ resultados por página&nbsp;&nbsp;",
                                                        "sLoadingRecords": "Carregando...",
                                                        "sProcessing": "Processando...",
                                                        "sZeroRecords": "Nenhum registro encontrado",
                                                        "sSearch": "Pesquisar",
                                                        "oPaginate": {
                                                            "sNext": "Próximo",
                                                            "sPrevious": "Anterior",
                                                            "sFirst": "Primeiro",
                                                            "sLast": "Último"
                                                        },
                                                        "oAria": {
                                                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                                                            "sSortDescending": ": Ordenar colunas de forma descendente"
                                                        }
                                                    },
                                                    "Columns":[
                                                        null,
                                                        null,
                                                        null,
                                                        null,
                                                        {"type": "date-br"},
                                                        null,
                                                        null,
                                                        {"type": "date-br"}
                                                    ],
                                                    "columnDefs": [
                                                        {
                                                            "targets": [1],
                                                            "visible": false,
                                                        },
                                                        {
                                                            targets: 3,
                                                            render: $.fn.dataTable.render.ellipsis( 15, true )
                                                        },
                                                        {
                                                            "targets": [6],
                                                            "visible": true,
                                                        },
                                                        {
                                                            "targets": [12],
                                                            "visible": false,
                                                        },
                                                        {
                                                            "targets": [13],
                                                            "visible": false,
                                                        },
                                                        {
                                                            "targets": [14],
                                                            "visible": false,
                                                        },
                                                        {
                                                            "targets": [15],
                                                            "visible": false,
                                                        },
                                                    ],
                                                    dom: 'Blfrtip',
                                                    buttons: [
                                                        {
                                                            extend: 'excelHtml5',
                                                            filename: 'Extrato NF-e Compras',
                                                            title: null,
                                                            text: 'Excel',
                                                            exportOptions: {
                                                                trim: true,
                                                                format: {
                                                                    body: function (data, row, column, node ) {
                                                                        try {
                                                                            return column === 6 ? "\0" + data :
                                                                                (column === 0 ? $(data).text():
                                                                                    (column === 3 ? $(data).attr('title') : column === 9 ?
                                                                                        '\0'+(($(data).text()).replace(/[\r\n]\s*[\r\n]/g, "")).replace(/\s/g, "") :
                                                                                        (column === 7 && data.indexOf("Processar") != -1) ? ""  : data));
                                                                        }catch(e){
                                                                            return column === 6 ? "\0" + data :
                                                                                (column === 0 ? $(data).text():
                                                                                    (column === 3 ? data : column === 9 ?
                                                                                        '\0'+(($(data).text()).replace(/[\r\n]\s*[\r\n]/g, "")).replace(/\s/g, "") :
                                                                                        (column === 7 && data.indexOf("Processar") != -1) ? ""  : data));
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        {
                                                            extend: 'colvis',
                                                            text: 'Exibir/Ocultar Colunas'
                                                        },
                                                        {
                                                            text: 'Todas NF-e',
                                                            action: function ( e, dt, node, config ) {
                                                                table
                                                                    .search( '' )
                                                                    .columns().search( '' )
                                                                    .draw();
                                                            }
                                                        },
                                                        {
                                                            text: 'Notas Bahia',
                                                            action: function ( e, dt, node, config ) {
                                                                table.columns(6).search('Aquisição dentro do estado').draw();
                                                            },
                                                            className: 'botao_nota'
                                                        },
                                                        {
                                                            text: 'MDF-e Inexistente',
                                                            action: function ( e, dt, node, config ) {
                                                                table.columns(6).search('MDF-e Inexistente').draw();
                                                            }
                                                        },
                                                        {
                                                            text: 'MDF-e Existente',
                                                            action: function ( e, dt, node, config ) {
                                                                table.columns(6).search('MDF-e Encontrado').draw();
                                                            }
                                                        }
                                                    ],
                                                    orderCellsTop: true,
                                                    fixedHeader: true,
                                                    "paging": false,
                                                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todas"]]
                                                });

                                            </script>
                                            <style type="text/css">

                                                .tim {
                                                    border: 0;
                                                    padding: 0;
                                                    display: inline;
                                                    background: none;
                                                    text-decoration: underline;
                                                    color: blue;
                                                }

                                                .tim:hover {
                                                    cursor: pointer;
                                                }


                                                thead input {
                                                    width: 100%;
                                                    padding: 3px;
                                                    box-sizing: border-box;
                                                }

                                                .buttons-excel{
                                                    margin-left: 10px;
                                                }

                                                .botao_nota{
                                                    margin-left: 10px;
                                                }
                                            </style>
                                        <?php } ?>
                                    </div>
                                    <!-- FIM DA TAB 3 -->
                                </div>
                                <!-- FIM DO CONTEÚDO DAS TABS -->
                            </div>
                            <br>
                            <div class="ibox-footer text-right">
                                <a href="<?php echo base_url("empresa/compras_e_vendas"); ?>" class="btn btn-info">Voltar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>




<div class="modal fade" data-backdrop="static" id="modal_cadastro">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title" style="text-align: center !important;">Processando Requisição</h5>
            </div>
            <div class="modal-body p-4">
                <div class="progress mb-4">
                    <div class="progress-bar progress-bar-striped active progress-bar-animated" id="ex-progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
                Por favor aguarde
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="modal_mdfe">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title" style="text-align: center !important;">Processando Requisição</h5>
            </div>
            <div class="modal-body p-4">
                <div class="progress mb-4">
                    <div class="progress-bar progress-bar-striped active progress-bar-animated" id="ex-progress" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
                Por favor aguarde
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var today = new Date();

    $('#date_1').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy",
        endDate: "today",
        maxDate: today
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });

    $('#date_2').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy",
        endDate: "today",
        maxDate: today
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });

    $('#date_3').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy",
        endDate: "today",
        maxDate: today
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });

    $('#date_4').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy",
        endDate: "today",
        maxDate: today
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });

    $(".select2_demo_1").select2();

    function consultaNotasConsumidorFinal(){
        $("#btn-submit3").click();
    }

    function consultaNotasConsumidorFinalAlternativo(){
        $("#btn-submit_alternativo").click();
    }

</script>

<!--Script MDFE-->
<script type="text/javascript">

    function processar_mdfe_todas(){

        $('#modal_mdfe').modal('toggle')
        var link_string = "<?php echo $links_notas_detalhadas_destinatario ?>";

        var obj = {
            links: link_string,
            login: $('#login').val(),
            pass: $('#pass').val()
        };

        $.ajax({
            url:"processar_mdfe_todas",
            method:"POST",
            data: obj,
            dataType:"json",
            success:function(data)
            {
                if (data){
                    data.forEach(function(item){
                        setar_dados(item, item.chave_acesso)
                    })
                }
                $('#modal_mdfe').modal('hide')
                swal(
                    "Sucesso!",
                    "Notas processadas com sucesso!",
                    "success"
                )
                //para funcionar os filtros necessita dar um reset no data table
                $('#tabela_entrada').DataTable().rows()
                    .invalidate()
                    .draw()
            }
        });
    }

    function processar_mdfe(chave){
        $('#modal_mdfe').modal('toggle')

        var obj = {
            link: $('#link-' + chave).val(),
            login: $('#login').val(),
            pass: $('#pass').val()
        };
        $.ajax({
            url:"processar_mdfe",
            method:"POST",
            data: obj,
            dataType:"json",
            success:function(data)
            {
                setar_dados(data, chave)
                $('#modal_mdfe').modal('hide')
                swal(
                    "Sucesso!",
                    "Nota processada com sucesso!",
                    "success"
                )
                //para funcionar os filtros necessita dar um reset no data table
                $('#tabela_entrada').DataTable().rows()
                    .invalidate()
                    .draw()
            }
        });
    }

    function setar_dados(data, chave){
        var mdf_data = data.mdf_data
        var uf = $('#uf-'+ chave).html()
        var cidade_emitente = so_numero(data.cidade_emitente)
        var cidade_destinatario = so_numero(data.cidade_destinatario)
        var modalidade_frete = so_numero(data.modalidade_frete)

        // SETA DATA DO MDF-d

        setar_data_mdfe(data, chave)

        // SETA O STATUS DO MDF-e
        setar_status_mdfe(chave, uf, modalidade_frete, cidade_emitente, cidade_destinatario, mdf_data )

        // SETA CIDADES
        setar_cidades(data, chave)
    }

    function setar_status_mdfe(chave, uf, modalidade_frete, cidade_emitente, cidade_destinatario, mdf_data  ){
        var element = document.getElementById('uf-'+chave)
        if( element ){
            var string_texto = ''
            if (uf != 'BA' && ! mdf_data){
                element.setAttribute("class", "uf_nfe badge badge-danger badge-shadow");
                string_texto = 'MDF-e Inexistente';
            }
            if (uf != 'BA' && mdf_data)
                string_texto = 'MDF-e Encontrado'
            if (uf == 'BA'){
                string_texto = 'Aquisição dentro do estado ';
                var modalidade_frete_obrigatorio = (modalidade_frete === 0 || modalidade_frete === 2 || modalidade_frete === 4);
                var cidade_diferente = cidade_emitente !== cidade_destinatario;

                if( modalidade_frete_obrigatorio && cidade_diferente && !mdf_data){
                    string_texto += 'MDF-e Inexistente '
                    element.setAttribute("class", "uf_nfe badge badge-danger badge-shadow");
                }
                else{
                    string_texto += 'Aquisição dentro da mesma cidade'
                }
            }
            var elem = $('#mdfe-'+ chave)
            elem.html(string_texto);
        }
    }

    function setar_cidades(data, chave){
        // SETA A CIDADE DO EMITENTE
        if ( data.cidade_emitente )
            $('#c_emitente-'+ chave).html(data.cidade_emitente);

        // SETA A CIDADE DO DESTINATARIO
        if ( data.cidade_destinatario)
            $('#c_destinatario-'+ chave).html(data.cidade_destinatario);
    }

    function setar_data_mdfe(data, chave){
        if ( data.mdf_data )
            $('#data_mdf-'+ chave).html(data.mdf_data);
    }

    function so_numero(string){
        return parseInt( string.replace(/[^0-9]/g,'') )
    }

</script>
