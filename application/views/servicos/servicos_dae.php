<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Relação de DAE´s</h2>
              </div>
              <div class="card-body"> 
                <div class="ibox-head">
                    <div class="ibox-title">Confira os dados da empresa e clique em gerar PDF</div>
                </div>

                <div class="ibox-body">  
                  <!-- CONTEÚDO DAS TABS -->
                  <div class="tab-content">

                      <!-- TAB 1 -->
                      <div class="tab-pane fade" id="tab-1-2" hidden="">
                          <br>
                          <div class="row">                                
                              <div class="col-md-6">
                                  <div class="form-group mb-4">
                                      <label for="nome">Empresa:</label>
                                      <?php echo form_input(array('name'=>'razao_social', 'class'=>'form-control', 'placeholder'=>'Razão Social'), $empresa->razao_social, 'readonly'); ?>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="form-group mb-4">
                                      <label for="nome">CNPJ:</label>
                                      <?php echo form_input(array('name'=>'cnpj', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->cnpj_completo, 'readonly'); ?>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="form-group mb-4">
                                      <label for="nome">Inscrição Estadual:</label>
                                      <?php echo form_input(array('name'=>'ie_empresa', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              
                              <div class="form-group col-md-4 mx-auto">
                                <label>Ano:</label>
                                <input type="text" name="ano" required="true" value="" id="ano_sefaz_input" maxlength="4" minlength="4" class="form-control" placeholder="Ano" style="border-color: #5c6bc0;">
                              </div>
                          
                          </div>

                          <div class="ibox-footer text-center">
                               <?php echo '<a href="#" data-toggle="tooltip" data-placement="top" title="Consultar relação de DAE" class="btn btn-primary mr-2" onclick="consultaDAE()">Consultar</a>'; ?>
                          </div>
                      
                          <?php if(isset($dados_tabela)){ ?>
                          <div class="row">

                              <div class="col-xl-12">
                                  <div class="ibox">
                                      <div class="ibox-head">
                                          <div class="ibox-title">Resultado DAE Sefaz</div>
                                      </div>
                                      <div class="ibox-body">
                                          <table class="table table-hover">
                                              <thead>
                                                  <tr>
                                                      <th>Nº DE SÉRIE</th>
                                                      <th>Referencia</th>
                                                      <th>Cod. Receita</th>
                                                      <th>Valor</th>
                                                      <th>Vencimento</th>
                                                      <th>Pagamento</th>
                                                      <th>Val. Principal</th>
                                                      <th>Val. Total</th>
                                                      <th>Status</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php foreach ($dados_tabela as $linha){ ?>
                                                  <tr>
                                                      <td><?= $linha['num_dae']; ?></td>
                                                      <td><?= $linha['referencia']; ?></td>
                                                      <td><?= $linha['codigo_receita']; ?></td>
                                                      <td><?= $linha['valor']; ?></td>
                                                      <td><?= $linha['vencimento']; ?></td>
                                                      <td><?= $linha['pagamento']; ?></td>
                                                      <td><?= $linha['val_princ']; ?></td>
                                                      <td><?= $linha['val_total']; ?></td>
                                                      <td><?= $linha['status']; ?></td>
                                                  </tr>
                                                  <?php } ?>
                                              </tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <?php } ?>
                      </div>
                      <!-- FIM DA TAB 1 -->

                      <!-- TAB 2 -->
                      <div class="tab-pane fade show active" id="tab-1-1">
                          <b>Para gerar o PDF deverá informar o ano e clicar na opção "Processar", após isso basta selecionar a opção "Gerar PDF".</b>
                          <br><br>
                          <div class="row">                                
                              <div class="col-md-6">
                                  <div class="form-group mb-4">
                                      <label for="nome">Empresa:</label>
                                      <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Razão Social'), $empresa->razao_social, 'readonly'); ?>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="form-group mb-4">
                                      <label for="nome">CNPJ:</label>
                                      <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->cnpj_completo, 'readonly'); ?>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="form-group mb-4">
                                      <label for="nome">Inscrição Estadual:</label>
                                      <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                                  </div>
                              </div>
                          </div>

                          <div class="row">
                              <div class="form-group col-md-4 mx-auto">
                                <label>Ano:</label>
                                <?php echo form_input(array('name'=>'ano', 'id'=>'ano_sefaz_input2', 'onkeypress'=>'return isNumber(event)', 'maxlength'=>'4', 'onkeydown'=>'desbloqueiaCampo(this)' ,'class'=>'form-control', 'placeholder'=>'Ano', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                              </div>
                          </div>

                          <div class="ibox-footer text-center" >
                              <a id='campoPDF' disabled style='cursor:not-allowed' href='#' data-toggle='tooltip' data-placement='top' title='Processar PDF' onclick='downloadPDF(<?php echo '"'.$empresa->inscricao_estadual.'","'.$empresa->login_sefaz.'","'.$empresa->senha_sefaz.'"' ?>);' class='btn btn-primary mr-2' >Processar</a>
                          </div>
                          <br>
                          <div class="ibox-footer text-center" id="divPDF">
                              
                          </div>
                      </div>
                      <!-- FIM DA TAB 2 -->  
                  </div>
                  <!-- FIM DO CONTEÚDO DAS TABS -->   
                  <div class="ibox-footer text-right">
                    <a href="<?php echo base_url("empresa/listar"); ?>" class="btn btn-secondary">Voltar</a>
                  </div>                        
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used--> 

<script type="text/javascript">

function consultaDAE(id){
    if($("#ano_sefaz_input").val() == ""){
        alert("Campo ano é obrigatório");
    }else{
        document.getElementById('ano_sefaz').value = document.getElementById('ano_sefaz_input').value;
        $("#btn-submitDAE").click();
    }
    
}

function downloadPDF(inscricao_estadual, login, senha){
    var ano = $('#ano_sefaz_input2').val();
    if(ano.length == 4){
        var ahref = "<a id='btnPdf' href='<?php echo base_url('servicos/download/');?>"+ano+"/"+inscricao_estadual+"/"+login+"/"+senha+"'"+ " download='DAE-"+ano+"'"+" data-toggle='tooltip' data-placement='top' class='btn btn-primary mr-2' >Gerar PDF</a>";

        $("#divPDF").append(ahref);
    }
    
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function desbloqueiaCampo(el) {
    if(el.value.length == 3){
        $("#campoPDF").css("cursor","");
    }else{
        $("#campoPDF").css("cursor","not-allowed");
    }
}

</script>
