<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/

  .media-body{
    padding-left: 0px !important; 
  }
</style>

<style type="text/css">
  .welcome-popup3 .modal-content2 {
  background-image: url(<?php echo base_url("assets/img/proc_vencida.png"); ?>);
  /*background-image: url(../images/dashboard/popup.png);*/
  background-repeat: no-repeat;
  background-position: top center;
  border-radius: 30px;
  -webkit-box-shadow: 2px 12px 26px 3px rgba(47, 46, 46, 0.3);
          box-shadow: 2px 12px 26px 3px rgba(47, 46, 46, 0.3); }

  .welcome-popup3 .close {
    z-index: 9;
    position: absolute;
    background-color: #fff;
    color: #24a0e9;
    opacity: 1;
    border-radius: 15px;
    padding: 10px 15px;
    left: -20px;
    top: -10px;
    -webkit-box-shadow: -2px 6px 11px 3px rgba(126, 55, 216, 0.3);
            box-shadow: -2px 6px 11px 3px rgba(126, 55, 216, 0.3); }

  .welcome-popup3 .modal-header {
    height: 360px;
    border: none; 
    width: 650px !important;}

  .welcome-popup3 .contain {
    /*padding-top: 80px;*/
   }

  .btn3 {
    position: absolute;
    top: 87%;
    left: 50%; 
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    background-color: #ffffff;
    color: black !important;
    font-size: 13px;
    padding: 12px 24px;
    border: none;
    cursor: pointer;
    border-radius: 5px;
  }

</style>

<style type="text/css">

  .welcome-popup2 .modal-content {
  background-image: url(<?php echo base_url("assets/img/cnd.jpeg"); ?>);
  /*background-image: url(../images/dashboard/popup.png);*/
  background-repeat: no-repeat;
  background-position: top center;
  border-radius: 30px;
  -webkit-box-shadow: 2px 12px 26px 3px rgba(47, 46, 46, 0.3);
          box-shadow: 2px 12px 26px 3px rgba(47, 46, 46, 0.3); }


  .welcome-popup2 .close {
    z-index: 9;
    position: absolute;
    background-color: #fff;
    color: #24a0e9;
    opacity: 1;
    border-radius: 15px;
    padding: 10px 15px;
    left: -20px;
    top: -10px;
    -webkit-box-shadow: -2px 6px 11px 3px rgba(126, 55, 216, 0.3);
            box-shadow: -2px 6px 11px 3px rgba(126, 55, 216, 0.3); }

  .welcome-popup2 .modal-header {
    height: 350px;
    border: none; 
    width: 630px !important;}

  .welcome-popup2 .contain {
    /*padding-top: 80px;*/
   }

   .btn2 {
    position: absolute;
    top: 80%;
    left: 58%; 
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    background-color: #ffffff;
    color: black !important;
    font-size: 13px;
    padding: 12px 24px;
    border: none;
    cursor: pointer;
    border-radius: 5px;
  }

</style>

<?php
  $hostCompleto = $_SERVER['HTTP_HOST'];
  $server = explode('.', $hostCompleto);
  $server = $server[0];

  $banco = $server;

  if($ha_certificado_vencido == false || $banco == "demo"){
    $link = base_url("assets/CND.pdf");
    $link2 = base_url("certificado/listar_certificados_vencidos");
    echo '
          <div class="welcome-popup2 modal fade" id="loadModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content" style="width: 700px !important">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <div class="modal-header"></div>
                  <div class="">
                    <div class="text-center">

                      <a href="'.$link.'" download class="btn2" >Ver</a>


                    </div>
                  </div>
                </div>
                <div style="text-align: center;">
                  
                </div>

              </div>
            </div>
          </div>';
  }

  else{
    $link = base_url("assets/SUBLIMITE.pdf");
    $link2 = base_url("certificado/listar_certificados_vencidos");
    echo '
          <div class="welcome-popup3 modal fade" id="loadModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content2" style="width: 700px !important">
                <div class="modal-body">
                  <div class="modal-header"></div>
                  <div class="">
                    <div class="text-center">

                      <a href="'.$link2.'" class="btn3" >Ver</a>


                    </div>
                  </div>
                </div>
                <div style="text-align: center;">
                  
                </div>

              </div>
            </div>
          </div>';
  }
?>

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Home</h2> 
        </div> 
      </div>
    </div>
  </div>

<div class="container-fluid general-widget">

  <div class="row">

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("mensagens"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Mensagens</span>
              <i class="icon-bg" data-feather="mail"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="#" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Em desenvolvimento !">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body"><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Estadual</span>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="360.000000pt" height="360.000000pt" viewBox="0 0 360.000000 360.000000" preserveAspectRatio="xMidYMid meet"><metadata></metadata><g transform="translate(0.000000,360.000000) scale(0.100000,-0.100000)" style="fill: white;" fill="#000000" stroke="none"><path d="M1720 2908 c-80 -60 -379 -311 -467 -392 -64 -58 -63 -59 -63 64 l0 110 -170 0 -170 0 0 -264 0 -263 -195 -173 c-107 -95 -197 -181 -200 -191 -11 -34 28 -79 69 -79 8 0 122 93 253 206 131 113 276 238 323 279 47 40 224 192 393 338 l309 266 315 -272 c999 -861 947 -817 972 -817 28 0 61 34 61 64 0 24 -30 55 -180 186 -40 36 -119 105 -174 154 -56 49 -114 101 -131 115 -16 14 -86 76 -155 136 -69 61 -155 135 -190 165 -36 30 -83 71 -105 91 -203 180 -385 319 -416 319 -13 0 -49 -19 -79 -42z"/><path d="M1679 2583 c-58 -48 -266 -223 -463 -388 -197 -165 -376 -313 -397 -330 l-38 -30 0 -585 c-1 -352 3 -591 9 -600 8 -13 64 -15 400 -15 376 0 392 1 406 19 11 16 14 74 14 303 l0 283 195 0 195 0 0 -295 c0 -272 1 -295 18 -304 22 -12 748 -12 780 0 l22 8 0 593 0 593 -497 414 c-273 228 -506 416 -517 418 -15 3 -52 -21 -127 -84z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div> -->

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("painel_receita"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Federal</span>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certidao_negativa/dashboard"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Certidões</span>
              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M985 5106 c-141 -45 -243 -146 -285 -284 -20 -63 -20 -101 -20 -2098 l0 -2034 -299 0 c-303 0 -342 -4 -367 -38 -21 -27 -19 -294 3 -358 43 -130 148 -235 278 -278 47 -15 200 -16 1940 -14 l1890 3 65 31 c119 57 193 142 230 262 20 63 20 101 20 2098 l0 2034 299 0 c303 0 342 4 367 38 21 27 19 294 -3 358 -32 96 -93 174 -178 229 -110 70 57 65 -2024 64 -1486 0 -1884 -3 -1916 -13z m3345 -171 c0 -3 -11 -27 -24 -53 -13 -26 -29 -74 -35 -107 -8 -41 -11 -707 -11 -2229 l0 -2171 -24 -50 c-92 -195 -367 -192 -455 5 -10 23 -17 81 -21 172 -5 126 -7 139 -28 160 l-23 23 -1424 3 -1425 2 0 2028 c0 2018 1 2027 21 2072 27 60 64 98 124 126 l50 24 1638 0 c900 0 1637 -2 1637 -5z m444 -6 c52 -14 141 -103 155 -155 6 -21 11 -67 11 -101 l0 -63 -251 0 -252 0 6 75 c14 186 157 292 331 244z m-1194 -4493 c0 -77 26 -184 54 -221 9 -11 16 -23 16 -27 0 -13 -3259 -10 -3304 3 -52 14 -141 103 -155 155 -6 21 -11 67 -11 102 l0 62 1700 0 1700 0 0 -74z" style="fill: white;"/>
              <path d="M1960 4180 l0 -90 560 0 560 0 0 90 0 90 -560 0 -560 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3670 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3160 l0 -90 945 0 945 0 0 90 0 90 -945 0 -945 0 0 -90z" style="fill: white;"/>
              <path d="M3410 3160 l0 -90 260 0 260 0 0 90 0 90 -260 0 -260 0 0 -90z" style="fill: white;"/>
              <path d="M1190 2650 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M3330 2303 c-101 -17 -208 -74 -287 -154 -93 -94 -143 -217 -143 -354 0 -137 41 -247 130 -348 l40 -45 0 -251 0 -252 25 -24 c42 -43 78 -35 206 49 l112 74 111 -74 c113 -75 141 -85 189 -63 43 19 47 45 47 300 l0 240 46 57 c83 101 124 213 124 337 0 232 -172 446 -402 500 -60 14 -143 17 -198 8z m220 -209 c213 -96 263 -371 98 -536 -63 -64 -127 -92 -218 -96 -105 -5 -176 22 -251 97 -260 260 35 686 371 535z m-135 -809 c61 0 122 3 138 8 l27 7 0 -95 0 -96 -42 29 c-62 42 -104 62 -130 62 -13 0 -54 -20 -91 -45 l-67 -45 0 95 0 95 28 -7 c15 -5 77 -8 137 -8z" style="fill: white;"/>
              <path d="M1190 2130 l0 -90 515 0 515 0 0 90 0 90 -515 0 -515 0 0 -90z" style="fill: white;"/>
              <path d="M1190 1710 l0 -90 220 0 220 0 0 90 0 90 -220 0 -220 0 0 -90z" style="fill: white;"/>
              <path d="M1960 1710 l0 -90 130 0 130 0 0 90 0 90 -130 0 -130 0 0 -90z" style="fill: white;"/>
              </g>
              </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>


    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("processos/dashboard"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body"><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Processos</span>
              <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="1200.000000pt" height="1200.000000pt" viewBox="0 0 1200.000000 1200.000000" preserveAspectRatio="xMidYMid meet" class="icon-bg"><metadata></metadata><g style="fill: white" transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M7110 8776 c-225 -81 -1411 -563 -1440 -586 -111 -85 -113 -258 -5 -348 39 -31 94 -52 142 -52 28 0 31 -4 77 -120 26 -66 49 -120 51 -120 7 0 1461 583 1481 594 22 11 22 12 -26 131 l-48 120 25 25 c93 91 79 262 -27 335 -73 51 -133 56 -230 21z"/><path d="M7380 8074 c-8 -3 -320 -127 -692 -276 -673 -268 -677 -270 -722 -318 -62 -66 -79 -109 -79 -200 0 -89 17 -133 76 -197 52 -57 110 -83 200 -92 l69 -6 83 -208 84 -209 -52 -22 c-29 -13 -422 -171 -874 -351 l-822 -328 -58 30 c-124 66 -274 85 -395 49 -76 -22 -2299 -910 -2363 -944 -27 -15 -79 -56 -115 -92 -295 -296 -148 -804 260 -900 68 -16 190 -16 250 0 92 25 2345 933 2394 965 102 66 182 168 221 284 l22 66 869 347 c478 192 872 348 875 348 7 0 169 -398 169 -415 0 -6 -18 -28 -40 -50 -59 -57 -83 -116 -84 -200 0 -118 54 -208 159 -263 41 -21 64 -26 125 -27 l75 0 682 273 c606 243 687 278 722 312 56 55 83 113 89 191 7 95 -17 159 -82 224 -59 59 -121 85 -205 85 l-56 0 -278 696 -278 696 40 38 c67 65 86 112 86 210 0 76 -3 91 -30 138 -52 95 -130 143 -240 148 -38 2 -77 1 -85 -2z"/><path d="M7705 5293 c-407 -163 -742 -298 -744 -300 -3 -2 17 -57 44 -124 l47 -121 -30 -31 c-36 -37 -62 -103 -62 -156 0 -56 42 -135 90 -170 52 -38 134 -56 187 -42 21 6 357 139 748 295 503 201 720 293 746 314 91 76 104 223 28 315 -34 42 -86 67 -151 76 l-49 6 -47 114 c-26 63 -52 116 -57 117 -6 2 -343 -130 -750 -293z"/><path d="M6330 3816 l0 -183 -321 -214 -321 -214 1186 -3 c652 -1 1720 -1 2374 0 l1187 3 -322 215 -323 215 0 182 0 183 -1730 0 -1730 0 0 -184z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    
    </div>

    <div class="row">
      <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("processos/dashboard"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body"><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Processos</span>
              <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="1200.000000pt" height="1200.000000pt" viewBox="0 0 1200.000000 1200.000000" preserveAspectRatio="xMidYMid meet" class="icon-bg"><metadata></metadata><g style="fill: white" transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M7110 8776 c-225 -81 -1411 -563 -1440 -586 -111 -85 -113 -258 -5 -348 39 -31 94 -52 142 -52 28 0 31 -4 77 -120 26 -66 49 -120 51 -120 7 0 1461 583 1481 594 22 11 22 12 -26 131 l-48 120 25 25 c93 91 79 262 -27 335 -73 51 -133 56 -230 21z"/><path d="M7380 8074 c-8 -3 -320 -127 -692 -276 -673 -268 -677 -270 -722 -318 -62 -66 -79 -109 -79 -200 0 -89 17 -133 76 -197 52 -57 110 -83 200 -92 l69 -6 83 -208 84 -209 -52 -22 c-29 -13 -422 -171 -874 -351 l-822 -328 -58 30 c-124 66 -274 85 -395 49 -76 -22 -2299 -910 -2363 -944 -27 -15 -79 -56 -115 -92 -295 -296 -148 -804 260 -900 68 -16 190 -16 250 0 92 25 2345 933 2394 965 102 66 182 168 221 284 l22 66 869 347 c478 192 872 348 875 348 7 0 169 -398 169 -415 0 -6 -18 -28 -40 -50 -59 -57 -83 -116 -84 -200 0 -118 54 -208 159 -263 41 -21 64 -26 125 -27 l75 0 682 273 c606 243 687 278 722 312 56 55 83 113 89 191 7 95 -17 159 -82 224 -59 59 -121 85 -205 85 l-56 0 -278 696 -278 696 40 38 c67 65 86 112 86 210 0 76 -3 91 -30 138 -52 95 -130 143 -240 148 -38 2 -77 1 -85 -2z"/><path d="M7705 5293 c-407 -163 -742 -298 -744 -300 -3 -2 17 -57 44 -124 l47 -121 -30 -31 c-36 -37 -62 -103 -62 -156 0 -56 42 -135 90 -170 52 -38 134 -56 187 -42 21 6 357 139 748 295 503 201 720 293 746 314 91 76 104 223 28 315 -34 42 -86 67 -151 76 l-49 6 -47 114 c-26 63 -52 116 -57 117 -6 2 -343 -130 -750 -293z"/><path d="M6330 3816 l0 -183 -321 -214 -321 -214 1186 -3 c652 -1 1720 -1 2374 0 l1187 3 -322 215 -323 215 0 182 0 183 -1730 0 -1730 0 0 -184z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div> -->

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("calendario/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Tarefas</span>
              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="473.000000pt" height="528.000000pt" viewBox="0 0 473.000000 528.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,528.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M1223 5054 c-78 -39 -92 -83 -93 -296 l0 -166 -287 -4 -288 -3 -63 -29 c-70 -32 -123 -85 -155 -154 l-22 -47 0 -1730 0 -1730 22 -47 c32 -69 85 -122 155 -154 l63 -29 1810 0 1810 0 62 29 c75 34 133 93 159 162 18 47 19 101 19 1769 0 1668 -1 1722 -19 1769 -26 69 -84 128 -159 162 l-62 29 -327 3 -328 4 0 176 c0 156 -2 180 -20 216 -28 57 -63 79 -130 84 -71 4 -116 -19 -147 -78 -21 -40 -23 -55 -23 -222 l0 -178 -870 0 -870 0 0 169 c0 176 -7 219 -44 259 -47 52 -129 67 -193 36z m-70 -940 c67 -127 235 -120 293 13 9 21 14 70 14 139 0 105 0 107 19 90 31 -29 65 -116 65 -171 0 -185 -195 -303 -362 -219 -134 67 -173 239 -85 367 l28 40 6 -114 c3 -78 10 -124 22 -145z m2072 -13 c33 -56 68 -76 135 -76 70 0 103 20 137 85 20 37 23 58 23 154 l1 111 34 -38 c81 -93 68 -250 -30 -335 -56 -50 -100 -64 -181 -60 -64 3 -78 7 -127 41 -44 30 -60 50 -81 96 -42 94 -29 200 32 264 l27 28 5 -118 c4 -94 9 -125 25 -152z m953 -1860 c1 -782 -1 -1186 -8 -1212 -13 -46 -51 -93 -94 -117 -29 -16 -142 -17 -1691 -17 l-1661 0 -48 25 c-27 14 -63 42 -80 63 l-31 39 -3 1199 -2 1199 1807 -2 1808 -3 3 -1174z" style="fill: white;"/>
              <path d="M1049 3121 l-29 -29 0 -192 c0 -257 -17 -240 239 -240 186 0 190 0 218 24 l28 24 3 175 c4 194 -5 239 -53 257 -15 6 -105 10 -201 10 l-176 0 -29 -29z" style="fill: white;"/>
              <path d="M1802 3140 c-44 -18 -54 -66 -50 -255 3 -168 4 -174 27 -199 l24 -26 194 0 c261 0 243 -17 243 240 l0 192 -29 29 -29 29 -179 -1 c-98 0 -188 -4 -201 -9z" style="fill: white;"/>
              <path d="M2519 3121 l-29 -29 0 -192 c0 -257 -18 -240 244 -240 l194 0 26 31 c26 30 26 32 26 205 0 185 -7 221 -49 244 -12 6 -96 10 -201 10 l-182 0 -29 -29z" style="fill: white;"/>
              <path d="M3275 3138 c-42 -24 -44 -36 -45 -240 l0 -197 26 -20 c25 -20 40 -21 216 -21 254 0 238 -16 238 234 0 197 -6 226 -47 245 -33 15 -361 14 -388 -1z" style="fill: white;"/>
              <path d="M1798 2405 c-42 -23 -50 -69 -46 -257 l3 -168 28 -27 27 -28 191 0 191 0 24 28 c24 28 24 32 24 217 0 185 0 189 -24 217 l-24 28 -184 2 c-148 2 -188 0 -210 -12z" style="fill: white;"/>
              <path d="M1045 2385 c-25 -24 -25 -26 -25 -215 l0 -192 29 -29 29 -29 177 0 c207 0 228 6 245 72 7 25 10 104 8 204 -3 156 -4 163 -27 188 l-24 26 -194 0 c-193 0 -194 0 -218 -25z" style="fill: white;"/>
              <path d="M2519 2391 c-24 -19 -24 -23 -27 -215 l-3 -196 30 -31 31 -30 190 3 c253 4 235 -15 235 242 0 192 0 195 -24 220 l-24 26 -193 0 c-174 0 -194 -2 -215 -19z" style="fill: white;"/>
              <path d="M3252 2387 c-21 -22 -22 -32 -22 -214 0 -105 3 -199 6 -208 15 -37 56 -45 234 -45 173 0 201 5 227 39 9 11 12 73 13 209 0 260 18 242 -243 242 -189 0 -194 0 -215 -23z" style="fill: white;"/>
              <path d="M1088 1653 c-15 -2 -37 -16 -48 -30 -18 -24 -20 -40 -20 -214 0 -184 0 -188 24 -216 l24 -28 180 -3 c149 -3 186 0 211 12 45 24 53 70 49 262 -3 163 -3 164 -31 191 l-27 28 -168 1 c-92 1 -179 0 -194 -3z" style="fill: white;"/>
              <path d="M1824 1653 c-12 -2 -32 -15 -45 -29 -23 -24 -24 -33 -27 -192 -6 -277 -7 -275 260 -270 l180 3 24 28 c24 28 24 32 24 215 0 211 -4 224 -68 242 -35 9 -303 12 -348 3z" style="fill: white;"/>
              <path d="M2550 1647 c-54 -19 -60 -40 -60 -244 0 -178 1 -182 24 -210 l24 -28 177 -3 c125 -2 187 0 208 9 48 20 57 57 57 239 0 193 -8 223 -67 239 -52 15 -319 13 -363 -2z" style="fill: white;"/>
              </g>
              </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("parcelamentos/dashboard"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 30px">Parcelamentos</span>
              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="225.000000pt" height="225.000000pt" viewBox="0 0 225.000000 225.000000" preserveAspectRatio="xMidYMid meet">
                <metadata>
                Created by potrace 1.16, written by Peter Selinger 2001-2019
                </metadata>
                <g transform="translate(0.000000,225.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                <path d="M92 2234 l-22 -15 0 -953 c0 -806 2 -955 14 -972 14 -18 34 -19 514 -24 l499 -5 35 -45 c45 -60 158 -144 235 -175 224 -89 473 -37 643 134 122 124 176 267 167 446 -6 123 -31 200 -99 301 -75 113 -184 193 -318 235 l-55 17 -2 518 c-3 515 -3 519 -24 537 -20 16 -73 17 -793 17 -682 0 -774 -2 -794 -16z m1478 -579 c0 -364 -3 -466 -12 -470 -7 -2 -37 -6 -65 -10 l-53 -6 0 130 c0 123 -1 131 -22 145 -20 14 -85 16 -518 16 -272 0 -505 -3 -518 -6 -50 -14 -52 -32 -52 -466 0 -349 2 -407 16 -426 15 -22 16 -22 334 -22 l319 0 11 -57 c7 -32 15 -63 17 -70 4 -10 -78 -13 -411 -13 l-416 0 0 860 0 860 685 0 685 0 0 -465z m-980 -395 l0 -70 -65 0 -65 0 0 70 0 70 65 0 65 0 0 -70z m725 0 l0 -65 -295 0 -295 0 -3 68 -3 67 298 -2 298 -3 0 -65z m-725 -260 l0 -70 -65 0 -65 0 0 65 0 64 63 4 c34 2 63 4 65 5 1 1 2 -29 2 -68z m627 58 c-1 -3 -28 -34 -60 -67 l-58 -61 -190 0 -189 0 0 63 c0 35 2 66 4 68 7 7 496 4 493 -3z m498 -20 c100 -25 205 -104 266 -199 141 -221 57 -536 -175 -653 -184 -93 -378 -67 -527 72 -353 326 -32 900 436 780z m-1123 -303 l3 -65 -67 0 -68 0 0 65 0 65 64 0 64 0 4 -65z m438 58 c-1 -5 -7 -33 -14 -63 l-12 -55 -142 -3 -142 -3 0 66 0 65 155 0 c85 0 155 -3 155 -7z" style="fill: white;"/>
                <path d="M366 1975 c-32 -16 -43 -58 -22 -89 l16 -26 294 0 c271 0 294 1 309 18 21 23 22 66 1 86 -8 9 -18 16 -22 17 -87 6 -561 1 -576 -6z" style="fill: white;"/>
                <path d="M350 1700 c-28 -28 -25 -64 6 -89 26 -21 36 -21 525 -21 486 0 500 1 519 20 25 25 26 72 2 93 -17 15 -68 17 -525 17 -494 0 -508 -1 -527 -20z" style="fill: white;"/>
                <path d="M1545 955 c-14 -13 -25 -36 -25 -50 0 -17 -7 -28 -22 -34 -30 -11 -78 -60 -89 -89 -14 -36 -11 -110 6 -141 23 -45 67 -76 150 -106 84 -31 102 -50 79 -83 -27 -39 -114 -18 -114 28 0 19 -44 60 -65 60 -7 0 -25 -10 -40 -22 -24 -19 -27 -26 -22 -65 5 -51 41 -101 91 -128 28 -15 36 -26 36 -47 0 -35 32 -68 65 -68 33 0 65 33 65 68 0 21 8 31 37 47 98 53 117 178 40 261 -22 23 -60 45 -117 67 -80 31 -85 35 -85 62 0 23 6 32 27 39 38 13 74 -6 92 -49 19 -44 56 -63 91 -47 69 32 31 163 -60 206 -28 14 -35 23 -35 46 0 35 -31 70 -60 70 -12 0 -32 -11 -45 -25z" style="fill: white;"/>
                </g>
              </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>


    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("diagnostico_veri/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 30px;">Diagnóstico Veri</span>
              <svg class="icon-bg" version="1.0" xmlns="http://www.w3.org/2000/svg"
               width="670.000000pt" height="373.000000pt" viewBox="0 0 670.000000 373.000000"
               preserveAspectRatio="xMidYMid meet" style="width: 200px; right: -50px !important;;">

              <g transform="translate(0.000000,373.000000) scale(0.100000,-0.100000)"
              fill="#000000" stroke="none">
              <path d="M3840 3719 c-167 -11 -289 -44 -451 -124 -342 -168 -598 -501 -675
              -875 -8 -41 -18 -131 -21 -200 -12 -271 57 -525 201 -739 31 -46 56 -89 56
              -96 0 -12 -142 -157 -159 -163 -5 -2 -37 15 -72 37 -35 23 -71 41 -79 41 -15
              0 -160 -138 -160 -153 0 -4 101 -110 225 -235 l224 -227 71 70 c54 53 70 76
              70 98 0 29 -32 101 -50 112 -5 3 -10 18 -10 33 0 19 18 43 71 94 40 39 79 68
              91 68 11 0 42 -16 69 -35 145 -104 356 -186 543 -211 145 -19 218 -18 356 5
              265 43 492 156 684 340 325 312 455 767 346 1211 -116 469 -497 832 -980 934
              -89 18 -206 23 -350 15z m375 -328 c155 -43 296 -129 419 -255 141 -144 240
              -339 261 -516 3 -30 9 -63 11 -72 5 -17 -16 -18 -339 -18 l-345 0 -27 88 c-14
              48 -31 100 -36 116 -20 63 -69 85 -103 47 -9 -10 -24 -47 -32 -82 -8 -35 -27
              -109 -41 -164 -14 -55 -29 -116 -33 -135 -5 -19 -16 -66 -25 -105 -9 -38 -21
              -88 -25 -110 -11 -54 -20 -75 -31 -75 -5 0 -11 6 -13 13 -2 6 -11 37 -20 67
              -9 30 -23 78 -31 105 -8 28 -26 91 -40 140 -14 50 -39 135 -56 190 -16 55 -35
              118 -41 140 -35 133 -51 154 -105 143 -18 -3 -75 -81 -112 -153 -12 -22 -45
              -80 -74 -130 l-53 -90 -169 -3 -168 -2 6 63 c7 73 41 184 90 289 109 236 348
              441 593 509 151 41 390 41 539 0z m-631 -748 c9 -32 23 -80 31 -108 8 -27 51
              -174 95 -325 44 -151 89 -301 99 -334 35 -113 105 -113 131 1 7 32 24 99 36
              148 13 50 26 101 29 115 4 14 10 41 15 60 5 19 23 94 40 165 38 160 41 166 49
              140 15 -47 35 -74 62 -86 21 -8 124 -10 378 -8 192 2 353 1 357 -3 10 -11 -13
              -135 -43 -233 -45 -151 -121 -276 -237 -390 -92 -91 -171 -149 -266 -192 -162
              -74 -224 -87 -410 -87 -177 -1 -263 17 -399 80 -292 137 -484 383 -549 704
              -28 137 -42 127 173 122 207 -5 215 -3 250 64 58 111 127 224 137 224 3 0 14
              -26 22 -57z" style="fill: white;"/>
              <path d="M1912 887 c-235 -235 -422 -430 -422 -440 0 -84 180 -296 341 -402
              102 -67 66 -93 549 390 l425 425 -225 225 c-124 124 -229 225 -235 225 -6 0
              -200 -190 -433 -423z" style="fill: white;"/>
              </g>
              </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("suporte/listar_cliente"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget" style="text-align: center;">
            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 30px;">Suporte</span>
              <i class="pe-7s-headphones icon-bg" style="font-size: 90px;"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>


    </div>

    

    </div>

  </div>
</div> 

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->

<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->


<script type="text/javascript">
  $(document).ready(function() {
      
      if($('#loadModal') != null){
        $('#loadModal').modal('show');
      }
      
      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>