<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Caixa de Email<span></span></h2> 
        </div> 
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="email-wrap">
      <div class="row">
        <div class="col-xl-3 col-md-6 box-col-6">
          <div class="email-left-aside">
            <div class="card">
              <div class="card-body">
                <div class="email-app-sidebar">
                  <div class="media">
                    <div class="media-size-email"><img class="mr-3 rounded-circle" src="../assets/images/user/user.png" alt=""></div>
                    <div class="media-body">
                      <!-- DEFININDO VARIÁVEIS DE SESSÃO -->
                      <?php
                          $login = $this->session->userdata['userprimesession']['email_sefaz'];
                          $senha = $this->session->userdata['userprimesession']['senha_sefaz'];
                      ?>

                      <?php 
                        if ($this->session->userdata['userprimesession']['nivel'] == 1){ ?>
                          <h6 class="f-w-600"><?php echo $this->session->userdata['userprimesession']['razao_social']; ?></h6>  
                        <?php  } else { ?>
                          <h6 class="f-w-600"><?php echo $this->session->userdata['userprimesession']['nome']; ?></h6>  
                       <?php } ?>                       
                      <p><?php echo $login; ?></p>
                    </div>
                  </div>
                  <ul class="nav main-menu" role="tablist">
                    <?php if(($login != NULL) && ($senha != NULL)){ ?>
                      <?php if ($this->session->userdata['userprimesession']['nivel'] != 2){ ?>
                        <li class="nav-item"><a class="btn-primary btn-pill btn-block btn-mail" id="passo14" data-toggle="pill" href="<?php echo base_url("caixadeemail/sincronizar"); ?>" onclick="openModalBloqueio()" role="tab" aria-controls="pills-darkhome" aria-selected="true"><i class="icofont icofont-envelope mr-2"></i> Buscar Mensagens</a></li>
                      <?php } ?>

                    <?php }  else { ?>

                      <?php if ($this->session->userdata['userprimesession']['nivel'] != 2){ ?>
                        <li class="nav-item"><a class="btn-primary btn-pill btn-block btn-mail" iid="passo14" disabled style="cursor:not-allowed" data-toggle="tooltip" data-placement="top" title="Primeiramente deve-se configurar o email no seu perfil" href="#" data-toggle="modal" data-target="#modalalerta" onclick="msgalerta()"><i class="icofont icofont-envelope mr-2"></i> Buscar Mensagens</a></li>
                      <?php } ?>
                    <?php } ?>

                    <!-- BOTÃO DE EXCLUIR TODOS OS EMAILS (EXCLUSIVO PARA USUÁRIOS MASTER) -->
                    <?php if($this->session->userdata['userprimesession']['nivel'] != 2){ ?>
                    <li class="nav-item"><a class="btn-danger btn-pill btn-block btn-mail" href="#" data-toggle="tooltip" data-placement="top" title="Excluir todos os emails lidos" onclick='preencher_url("<?php echo base_url("caixadeemail/excluir_todos_lidos"); ?>")'><span class="btn sweet-7" style="padding: 1px 4px;"><i class="icofont icofont-trash mr-2"></i> Apagar todos os emails Lidos</a></li>
                    <?php }  else { ?>
                    <li class="nav-item"><a class="btn-danger btn-pill btn-block btn-mail" disabled style="cursor:not-allowed" data-toggle="tooltip" data-placement="top" title="Seu perfil não possui permissões para essa funcionalidade" href="#"><i class="icofont icofont-trash mr-2"></i> Apagar todos os emails Lidos</a></li>
                    <?php } ?>
                    <!-- FIM DO BOTÃO DE EXCLUSÃO -->

                    <li class="nav-item"><a class="show" id="pills-darkprofile-tab" data-toggle="pill" href="#pills-darkprofile" role="tab" aria-controls="pills-darkprofile" aria-selected="false"><span class="title"><i class="icon-import"></i> Novos Emails</span><span class="badge pull-right digits">(<?php echo "$qtd_novos_emails->valor"; ?>)</span></a></li> 


                    <li>
                      <hr>
                      Legenda
                      <br>
                    </li>

                    <li><a href="#"><span class="title"><i class="icon-email" style="color: red;"></i> Não Lido</span></a></li>
                    <li><a href="#"><span class="title"><i class="icon-email" style="color: green;"></i> Lido</span></a></li> 
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-9 col-md-12 box-col-12">
          <div class="email-right-aside">
            <div class="card email-body">
              <div class="pr-0 b-r-light">
                <div class="email-top">
                  <div class="row">
                    <div class="col">
                      <h5>Caixa de Email</h5>
                    </div>
                    <div class="col text-right"> 
                    </div>
                  </div>
                </div>
                <div class="inbox">
                  <br>
                  <div class="table-responsive">
                    <table class="table table-hover table-inbox" id="example" data-toggle="datatables">
                      <thead class="thead-default thead-lg">
                          <tr>
                            <th width="1%"></th>
                            <th style="font-size: 12px;"></th>
                            <th style="font-size: 12px;"></th>
                            <th style="font-size: 12px;"></th>
                            <th style="font-size: 12px;"></th>
                            <th width="10%"></th>
                          </tr>
                      </thead>
                        <tbody class="rowlinkx" data-link="row">
                            <tr data-id="1">
                                
                        </tbody>
                    </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div> 
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- Modal -->
  <div class="modal fade" id="modalexcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button class="close" data-dismiss="modal" aria-label="Close"></button>
          <div class="text-center mt-3 mb-4"><i class="ti-warning"></i></div>
          <div class="text-center h4 mb-3">Deseja Excluir o Email?</div>
          <p class="text-center mb-4" id="textoexcluir"></p>
          <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancelar</button>
          <a href="" id="urlexcluir" class="btn btn-info btn-flat">Confirmar</a>
        </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalalerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Configuração do Email é Necessária</h4>
        </div>
        <div class="modal-body">
          <p id="textalerta" style="text-transform: uppercase; text-align: center;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalbloqueio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content1">
        <div class="modal-body1">
          <h4 style="text-align: center; color:white; margin-top: 40%; padding-top: 14%;" id="h4Texto">Aguarde...  Procurando por novas mensagens</h4>
        </div>
      </div>
    </div>
  </div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>

<script>
$(document).ready(function() {
    $('#btn_leremail').click(function(){
    $.ajax({
            url: '<?php echo base_url('caixadeemail/visualizar'); ?>',
            type: 'POST',
            dataType : "json",
            success: function(data){
              console.log(data);
              if(data == true){
          $('#qtd_novos_emails').html('');
                }
            }
        });
  });

$.ajax({
    type: "POST",
    url: '<?php echo base_url('caixadeemail/listarAjax'); ?>',
    async: true,
    success: function(result){
        var data = JSON.parse(result);
        if(data == "") {
            var array = [];
            popularDataTable(array);
            jQuery('[data-toggle="tooltip"]').tooltip();                
        }else{
          var array = json2array(data);
          popularDataTable(array);
          jQuery('[data-toggle="tooltip"]').tooltip();
        }
    }
});

function popularDataTable(json){
    $('#example').DataTable({
            "iDisplayLength": 20,
            "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json
    });
  }

  function json2array(data){
      var string = "";
      var array = [];
      var arrayMultiple = [];
      var id = '';
      var id_contabilidade = '';
      var cnpjSemFormat;

      for(var i in data){
        array = [];
        id = data[i].id;
        id_contabilidade = data[i].id_contabilidade;

        array.push('<td class="check-cell rowlink-skip"><label class="checkbox checkbox-primary check-single"><input class="mail-check" type="checkbox"><span class="input-span"></span></label></td>');

        if (data[i].razao_social != "" && data[i].id_empresa != 0){
          if(data[i].login_sefaz != "" && data[i].senha_sefaz != ""){
            array.push('<a target="_blank" href="<?php echo base_url('consulta_dte/consultar/');?>'+data[i].id_empresa+'">'+data[i].razao_social+'</a>');
          }else{
            array.push('<a href="<?php echo base_url('caixadeemail/redirecionarparaempresa/');?>'+id+'">'+data[i].razao_social+'</a>');
          }

        array.push('<a class="d-block" data-toggle="modal" data-target="#mail-view-modal"><b>'+data[i].cnpj+'</b></a>');
        } else {
          cnpjSemFormat = apenasNumeros(data[i].cnpj);
          array.push('<b style="color: #f39c12;">EMPRESA NÃO CADASTRADA</b>');

          <?php if ($e->valor >= $emax->valor) { ?>
          array.push('<a data-toggle="tooltip" data-placement="top" title="Você atingiu o limite de empresas cadastradas em seu plano, você pode solicitar um aumento da quantidade de empresas entrando em contado conosco."><b>'+data[i].cnpj+'</b></a>');
          <?php } else { ?>
          array.push('<a data-toggle="tooltip" data-placement="top" title="Clique para cadastrar a empresa" href="<?php echo base_url('empresa/cadastrar_via_email/');?>'+cnpjSemFormat+'" target="_blank"><b>'+data[i].cnpj+'</b></a>');
          <?php } ?>
        }

        if (data[i].status == 0){
        array.push("<i class='icon-email' style='color: green;' data-toggle='tooltip' data-placement='top' title='Email Lido'></i>");
        } else {
        array.push("<i class='icon-email' style='color: red;' data-toggle='tooltip' data-placement='top' title='Email não Lido'></i>");
        }

        array.push('<b>'+data[i].dataemail+'</b>');
        
        array.push("<a target='_blank' data-toggle='tooltip' data-placement='top' title='Consultar Mensagens do DTE' onclick='get_mensagens_dte(&quot;"+data[i].login_sefaz+"&quot;,&quot;"+data[i].senha_sefaz+"&quot;);'><i class='fa fa-envelope' aria-hidden='true'></i></a><a href='<?php echo base_url('caixadeemail/marcar_como_lido/');?>"+data[i].id+"/"+data[i].id_contabilidade+"' data-toggle='tooltip' data-placement='top' title='Marcar como Lido' class='btn btn-default b'><i class='fa fa-eye' aria-hidden='true'></i></a><a class='btn btn-default b' data-toggle='modal' data-target='#modalexcluir' onclick='excluir(&quot;"+data[i].titulo+"&quot;, &quot;<?php echo base_url('caixadeemail/excluir/');?>"+id+"/"+id_contabilidade+"&quot;)'><i data-toggle='tooltip' data-placement='top' title='Excluir Email' class='fa fa-trash-o' aria-hidden='true'></i></a><a href='<?php echo base_url("caixadeemail/marcar_como_nao_lido/"); ?>"+id+"/"+id_contabilidade+"' data-toggle='tooltip' data-placement='top' title='Marcar como não Lido' class='btn btn-default b'><i class='fa fa-eye-slash' aria-hidden='true'></i></a>");

        

        arrayMultiple.push(array);
      }
      
      return arrayMultiple;
  }


});

function apenasNumeros(string){
    var numsStr = string.replace(/[^0-9]/g,'');
    return numsStr;
}
function excluir(email, url){
  $("#urlexcluir").attr("href", url);
  $('#textoexcluir').html('Deseja realmente <strong>excluir</strong> o email: <strong>'+ email +'</strong>?');
}

function msgalerta(){
  $('#textalerta').html('Primeiramente deve-se configurar o email no perfil do usuário');
}

function openModalBloqueio(){
  $('#modalbloqueio').modal({backdrop: 'static', keyboard: false});
}

function consultaDTE(login,senha){
  $("#loginDTE").val(login);
  $("#senhaDTE").val(senha);
  $("#btn-submitDTE").click();
} 
</script>

<!-- Modal das mensagens -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_mensagens">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagens DTE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div style="width: 200px;margin-left: 15px;" id="div_filter">
                    <p>
                        Situação: 
                        <select id="table-filter" class="form-control form-control-sm">
                            <option value="">Todas Mensagens</option>
                            <option value="Não Lida">Mensagens Não Lidas</option>
                        </select>
                    </p>
                </div>
                <table id="modal_table" class="table table-striped">                    
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Leitura da Mensagem -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ler_mensagem">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_mensagem"></h5>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body_mensagem">
            </div>
            <div class="modal-footer">
                <a id="link_pdf_mensagem" href="#" type="button" class="btn btn-primary" target="_blank"><i class="ti-printer"></i>&nbsp;Imprimir</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div> 

<!-- Funções JavaScript -->
<script type="text/javascript">

    function get_mensagens_dte($login, $senha){
        $(document).ready(function() {
            $(document).ready(function() {
                swal({ 
                    title: "Buscando...",
                    text: 'Buscando Mensagens DTE da Empresa na Sefaz.\nPor favor, aguardar alguns segundos...',
                    type: "info" ,
                    confirmButtonText: "Cancelar",
                    confirmButtonColor: "#000000"
                },
                function(isConfirm){
                    if(isConfirm){
                        request.abort();
                    }
                });
            });
        });
        var url = "<?= base_url() ?>/Dte/get_mensagens/"+$login+"/"+$senha;
        var request = $.get(url, function(resultado){             
            response = JSON.parse(resultado);
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#modal_table').html(response['table']);
                $('#modal_mensagens').modal('show');
                $(document).ready( function () {
                    $.fn.dataTable.moment( 'DD/MM/YYYY HH:mm:ss' ); 
                    $('#modal_table').dataTable().fnDestroy();
                    var table = $('#modal_table').DataTable({
                        lengthMenu: [5, 10, 20, 50, 100],
                        "order": [[ 3, "desc" ]],
                        language: {
                            "sEmptyTable": "Nenhum registro encontrado",
                            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sInfoThousands": ".",
                            "sLengthMenu": "_MENU_ resultados por página",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sZeroRecords": "Nenhum registro encontrado",
                            "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sNext": "Próximo",
                                "sPrevious": "Anterior",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                            },
                            "oAria": {
                                "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                            },
                            "select": {
                                "rows": {
                                    "_": "Selecionado %d linhas",
                                    "0": "Nenhuma linha selecionada",
                                    "1": "Selecionado 1 linha"
                                }
                            }
                        }
                    });
                    $("#div_filter select").val("Não Lida");
                    table.search('Não Lida').draw();

                    $('#table-filter').on('change', function(){
                        table.search(this.value).draw();   
                    });
                });    
            }        
        });
    }

    function ler_mensagem($id, $login, $senha){        
        $('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#000000"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/Dte/ler_mensagem/"+$login+"/"+$senha+"/"+encodeURIComponent($id);
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#modal_mensagens').modal('show');
                response['body'] = response['body'].replace(/(?:\\[rn])+/g, "<br>");
                response['body'] = response['body'].replace('<br>            ', '');
                $('#title_mensagem').html(response['title']);
                $('#body_mensagem').html(response['body']);
                $('#modal_ler_mensagem').modal('show');

                $('#link_pdf_mensagem').attr('href', "<?= base_url() ?>/Dte/get_pdf/"+$login+"/"+$senha+"/"+encodeURIComponent($id));
            } 
        });
    }
</script>