
<!-- Responsive css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">

<style type="text/css">
  .d-block{font-size: 17px;
            font-weight: bold;}
</style>
<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>

<div class="page-body">
  <div class="container-fluid">
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12">
        <div class="default-according style-1 faq-accordion job-accordion" id="accordionoc">
          <div class="row">
            <div class="col-xl-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="mb-0">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title" style="font-size: 35px;">Diagnóstico Veri</h4>
                    </div>
                  </h5>
                </div>

                <?php echo form_open("Diagnostico_veri/gerar", array('class'=>'form-material', 'target' => '_blank')); ?> 
                <div class="collapse show" id="collapseicon" aria-labelledby="collapseicon" data-parent="#accordion">
                  <div class="card-body filter-cards-view animate-chk">
                    <div class="job-filter">
                      <div class="faq-form">
                       <label style="font-size: 18px;font-weight: bold;background-color: yellow;color: red;">Ao selecionar mais de uma empresa o relatório ficará restriro a até 5 Pendências Customizadas</label>
                       <div class="form-group col-lg-12">
                          <label style="font-size: 18px;font-weight: bold;">Empresa(s):</label>
                          <?php echo form_dropdown('id_empresa[]', $empresas, '', 'class="js-example-basic-multiple col-sm-12" multiple="multiple" required id="id_empresa" style="width:100%" placeholder="Selecione a empresa" onchange="limita_qtd(this)" ');?>
                        </div>
                        <!-- <input class="form-control" type="text" placeholder="Empresas.."><i class="search-icon" data-feather="search"></i> -->
                      </div>
                    </div>
                    <div class="checkbox-animated">

                      <label class="d-block" for="chk-ani" style="color:#24a0e9;">
                        <input class="checkbox_animated" id="todas_pendencias" type="checkbox" onclick="marca_todas_pendencias(this)" style="font-size:25px !important;"> Todas as Pendências
                      </label>
                      <br>
                      <!-- MENSAGENS -->
                      <label class="d-block" for="chk-ani" style="color:#24a0e9;">
                        <input class="checkbox_animated" id="mensagem" type="checkbox" onclick="marca_mensagens(this)"> Mensagens
                      </label>
                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input name="mensagem1" class="checkbox_animated" id="mensagem1" type="checkbox"  onclick="verifica_limite(this)">e-CAC
                      </label>
                      <label class="d-block" for="chk-ani2" style="margin-left:15px">
                        <input name="mensagem2" class="checkbox_animated" id="mensagem2" type="checkbox" onclick="verifica_limite(this)">DEC SP
                      </label>

                      <br>

                      <!-- FEDERAL -->
                      <label class="d-block" for="chk-ani3" style="color:#24a0e9;">
                        <input class="checkbox_animated" id="federal" type="checkbox" onclick="marca_fedaral(this)">Federal
                      </label>

                      <div class="row">
                        
                        <div class="col-xl-3">
                          <label class="d-block" for="chk-ani4" style="margin-left:15px">
                            <input name="federal1" class="checkbox_animated" id="federal1" type="checkbox" onclick="verifica_limite(this)">Diagnóstico Fiscal
                          </label>
                          <label class="d-block" for="chk-ani5" style="margin-left:15px">
                            <input name="federal2" class="checkbox_animated" id="federal2" type="checkbox" onclick="verifica_limite(this)">Divergência GFIP x GPS
                          </label>
                          <label class="d-block" for="chk-ani5" style="margin-left:15px">
                            <input name="federal3" class="checkbox_animated" id="federal3" type="checkbox" onclick="verifica_limite(this)">DCTF
                          </label>
                          <label class="d-block" for="chk-ani5" style="margin-left:15px">
                            <input name="federal4" class="checkbox_animated" id="federal4" type="checkbox" onclick="verifica_limite(this)">PGDAS
                          </label>
                          <label class="d-block" for="chk-ani5" style="margin-left:15px">
                            <input name="federal5" class="checkbox_animated" id="federal5" type="checkbox" onclick="verifica_limite(this)">DAS Não Pagos
                          </label>
                          <label class="d-block" for="chk-ani5" style="margin-left:15px">
                            <input name="federal6" class="checkbox_animated" id="federal6" type="checkbox" onclick="verifica_limite(this)">Pronampe
                          </label>
                        </div>

                        <div class="col-xl-3">
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal7" class="checkbox_animated" id="federal7" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - ECF
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal8" class="checkbox_animated" id="federal8" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - GFIP
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal9" class="checkbox_animated" id="federal9" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - DCTF
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal10" class="checkbox_animated" id="federal10" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - PGDAS
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal11" class="checkbox_animated" id="federal11" type="checkbox" onclick="verifica_limite(this)">CADIN
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal12" class="checkbox_animated" id="federal12" type="checkbox" onclick="verifica_limite(this)">Malha Fiscal
                          </label>
                        </div>

                        <div class="col-xl-3">
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal13" class="checkbox_animated" id="federal13" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - DEFIS
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal14" class="checkbox_animated" id="federal14" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - EFD-CONTRIB
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal15" class="checkbox_animated" id="federal15" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - DIRF
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal16" class="checkbox_animated" id="federal16" type="checkbox" onclick="verifica_limite(this)">Ausên. de Decl. - DASN SIMEI
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal17" class="checkbox_animated" id="federal17" type="checkbox" onclick="verifica_limite(this)">Procurações
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal18" class="checkbox_animated" id="federal18" type="checkbox" onclick="verifica_limite(this)">Termo de Exlusão do Simples
                          </label>
                        </div>

                        <div class="col-xl-3">

                          <div style="display:none">
                            
                            <label class="d-block" for="chk-ani5" >
                              <input name="federal19" class="checkbox_animated" id="federal19" type="checkbox" onclick="verifica_limite(this)">Dívida Ativa
                            </label>
                          </div>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal20" class="checkbox_animated" id="federal20" type="checkbox" onclick="verifica_limite(this)">Sublimite Simples
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal21" class="checkbox_animated" id="federal21" type="checkbox" onclick="verifica_limite(this)">Exclusão do Simples
                          </label>
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal22" class="checkbox_animated" id="federal22" type="checkbox" onclick="verifica_limite(this)">Exclusão do MEI
                          </label>

                          <div style="display:none">
                            
                            <label class="d-block" for="chk-ani5" >
                              <input name="federal23" class="checkbox_animated" id="federal23" type="checkbox" onclick="verifica_limite(this)">Certificados
                            </label>
                          </div>

                          <!-- <label class="d-block" for="chk-ani5" >
                            <input name="federal23" class="checkbox_animated" id="federal23" type="checkbox" onclick="verifica_limite(this)">Certificados
                          </label> -->
                          <label class="d-block" for="chk-ani5" >
                            <input name="federal24" class="checkbox_animated" id="federal24" type="checkbox" onclick="verifica_limite(this)">Termo de Intimação
                          </label>
                        </div>

                      </div>
                      
                      <br>

                      <!-- CERTIDÕES -->
                      <!-- <label class="d-block" for="chk-ani3" style="color:#24a0e9;">
                        <input class="checkbox_animated" id="certidao" type="checkbox" onclick="marca_certidao(this)">Certidões
                      </label>
                      
                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input name="certidao1" class="checkbox_animated" id="certidao1" type="checkbox" onclick="verifica_limite(this)">CND Estadual
                      </label>
                      <label class="d-block" for="chk-ani2" style="margin-left:15px">
                        <input name="certidao2" class="checkbox_animated" id="certidao2" type="checkbox" onclick="verifica_limite(this)">CND Trabalhista
                      </label>
                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input name="certidao3" class="checkbox_animated" id="certidao3" type="checkbox" onclick="verifica_limite(this)">CND RFB/PGFN
                      </label>
                      <label class="d-block" for="chk-ani2" style="margin-left:15px">
                        <input name="certidao4" class="checkbox_animated" id="certidao4" type="checkbox" onclick="verifica_limite(this)">CND FGTS
                      </label>
                      <label class="d-block" for="chk-ani2" style="margin-left:15px">
                        <input name="certidao5" class="checkbox_animated" id="certidao5" type="checkbox" onclick="verifica_limite(this)">CND MUNICIPAL
                      </label>


                      <br> -->
                      <!-- PROCESSOS -->
                      <label class="d-block" for="chk-ani3" style="color:#24a0e9;">
                        <input class="checkbox_animated" id="processos" type="checkbox" onclick="marca_processos(this)">Processos
                      </label>
                      
                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input name="processos1" class="checkbox_animated" id="processos1" type="checkbox" onclick="verifica_limite(this)">E-processos
                      </label>


                      <br>
                      <!-- PARCELAMENTOS -->
                      <label class="d-block" for="chk-ani3" style="color:#24a0e9;">
                        <input class="checkbox_animated" id="parcelamento" type="checkbox" onclick="marca_parcelamentos(this)">Parcelamentos
                      </label>
                      
                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input class="checkbox_animated" name="parcelamento1" id="parcelamento1" type="checkbox" onclick="verifica_limite(this)">Parcelamento DAS
                      </label>
                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input class="checkbox_animated" name="parcelamento2" id="parcelamento2" type="checkbox" onclick="verifica_limite(this)">Parcelamento PERT-SN
                      </label>

                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input class="checkbox_animated" name="parcelamento3" id="parcelamento3" type="checkbox" onclick="verifica_limite(this)">Parcelamento Não Previdenciário
                      </label>

                      <label class="d-block" for="chk-ani1" style="margin-left:15px">
                        <input class="checkbox_animated" name="parcelamento4" id="parcelamento4" type="checkbox" onclick="verifica_limite(this)">Parcelamento Lei 12.996
                      </label>

                    </div>
                    <?php echo form_submit(array('name'=>'btn_gerar', 'class'=>'btn btn-primary text-center', 'style'=>'width:100%'), 'Gerar Diagnóstico Veri'); ?>

                    <!-- <button class="btn btn-primary text-center" type="button">Gerar Diagnóstico</button> -->
                  </div>
                </div>

                <?php echo form_close(); ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js') ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url("assets/bundles/jquery-ui/jquery-ui.min.js"); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js') ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js') ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js') ?>"></script>
<script src="<?php echo base_url('assets/js/config.js') ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/jquery.ui.min.js') ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js') ?>"></script>
<script src="<?php echo base_url('assets/js/app.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>

<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/core/main.min.js') ?>'></script>
<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/interaction/main.min.js') ?>'></script>
<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/daygrid/main.min.js') ?>'></script>
<script src='<?php echo base_url('assets/bundles/fullcalendar/packages/timegrid/main.min.js') ?>'></script>


<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>


<script type="text/javascript">
  var qtd_total = 0;
  var qtd_selecionada = 0;

  function marca_todas_pendencias(source){
    marca_mensagens(source);
    marca_fedaral(source);
    marca_certidao(source);
    marca_processos(source);
    marca_parcelamentos(source);
  }

  function marca_mensagens(source) {
    var qtd_itens = $('[id^=mensagem]').length;
    if(qtd_total != 5 ){
      for(i=1;i<qtd_itens;i++){
        $("#mensagem"+i)[0].checked = source.checked;

        if(source.checked == true){
          qtd_selecionada++;
        }else{
          qtd_selecionada--;
        }
      }
    }
  }

  function marca_fedaral(source) {
    var qtd_itens = $('[id^=federal]').length;
    if(qtd_total != 5){
      for(i=1;i<qtd_itens;i++){
        $("#federal"+i)[0].checked = source.checked;

        if(source.checked == true){
          qtd_selecionada++;
        }else{
          qtd_selecionada--;
        }
      }
    }
  }

  function marca_certidao(source){
    var qtd_itens = $('[id^=certidao]').length;
    if(qtd_total != 5){
      for(i=1;i<qtd_itens;i++){
        $("#certidao"+i)[0].checked = source.checked;

        if(source.checked == true){
          qtd_selecionada++;
        }else{
          qtd_selecionada--;
        }
      }
    }
    
  }

  function marca_processos(source){
    var qtd_itens = $('[id^=processos]').length;
    if(qtd_total != 5){
      for(i=1;i<qtd_itens;i++){
        $("#processos"+i)[0].checked = source.checked;

        if(source.checked == true){
          qtd_selecionada++;
        }else{
          qtd_selecionada--;
        }
      }
    }
    
  }

  function marca_parcelamentos(source){
    var qtd_itens = $('[id^=parcelamento]').length;
    if(qtd_total != 5){
      for(i=1;i<qtd_itens;i++){
        $("#parcelamento"+i)[0].checked = source.checked;

        if(source.checked == true){
          qtd_selecionada++;
        }else{
          qtd_selecionada--;
        }
      }
    }
    
  }

  function limita_qtd(item){
    var options = $("#id_empresa :selected").length;

    var itens_selecionados = item.selectedOptions;
    var qtd = 0;
    qtd_total = options;
    for (var i = 0; i < options; i++) {
      if(itens_selecionados[i].outerText == "TODAS" || options > 1){
        qtd_total = 5;
        qtd_selecionada = 0;

        $("[id^=mensagem]").each(function(){
          $(this)[0].checked = false;
        });

        $("[id^=federal]").each(function(){
          $(this)[0].checked = false;
        });

        $("[id^=certidao]").each(function(){
          $(this)[0].checked = false;
        });

        $("[id^=processos]").each(function(){
          $(this)[0].checked = false;
        });

        $("[id^=parcelamento]").each(function(){
          $(this)[0].checked = false;
        });

        break;
      }
      qtd++;
    }
  }
  function verifica_limite(item){
    if(item.checked == true){
      qtd_selecionada++;
    }else{
      qtd_selecionada--;
    }
    if(qtd_total >=5){
      if(qtd_selecionada > 5){
        if(item.checked == true){
          item.checked = false;
          qtd_selecionada--;
        }
      }
    }
  }
</script>