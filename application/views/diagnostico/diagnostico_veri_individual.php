<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<style>
.datepicker{z-index:1151 !important;}

.badge-blue{
  background-color: #0a97fb;
  color: #fff;
}
</style>
<style type="text/css">

.swal2-styled{
    background-color: #dc2727 !important;
 } 

.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.left, .center, .center3, .right, .lefter, .righter {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.center2 {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  /*border-radius: 8px;*/
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.center2 {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/barcode.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
}
.center3 {
    opacity: 1;
    background-image: url(<?php echo base_url('assets/css/fab/images/recalcular.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
}
.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}


.modal-dialog {
    width: 1000px;
    max-width : 1000px;
}
</style>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div> 

<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title">Diagnóstico Veri</h4>
                    </div>


                </div>

                <!-- <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="excel()">EXCEL</button>
                <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="imprimir()">PDF</button> -->

                <div class="card-body">
                  <div class="form-group col-lg-4 col-xs-12 pull-right">
                    
                  </div>
                    <div class="card-block">
                        <table class="table table-striped table-hover dataTable no-footer text-center" id="example" data-toggle="datatables">

                            <thead>
                                <tr>
                                  <?php foreach ($empresas_detalhes as $e){ 
                                    $razao_social = $e->razao_social;
                                    $cnpj = $e->cnpj;
                                    ?>
                                  <?php } ?>
                                  <th width="60%" >Razão Social - <?php echo $razao_social ?></th>
                                  <th width="80%">CNPJ - <?php echo $cnpj ?></th>
                                </tr>
                                <tr>
                                  <th colspan="6">Pendência</th>
                                  <th colspan="6">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($hash_pendencias[$cnpj]){ 
                                    foreach ($hash_pendencias[$cnpj] as $p){
                                     
                                      ?>
                                <tr>
                                   
                                    <td colspan="6" ><?php echo $p->nome; ?></td>
                                    <td colspan="6">
                                        <div>
                                            <?php if($p->is_ok != 'alert'){ ?>

                                            <span onclick="openInNewTab('<?php echo $p->link; ?>');"  target="_blank" data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title="<?php echo $p->titulo; ?>" data-content='<?php echo $p->detalhes; ?> <br> Clique para ver detalhes' ><img src="<?php echo base_url('assets/css/fab/images/'.$p->is_ok.'.png'); ?>" style="width: <?php echo $p->tam; ?>"></span>
                                          <?php }else{ ?>
                                            <span onclick="openInNewTab('<?php echo $p->link; ?>');" class="badge badge-blue" target="_blank" data-container='body'  data-toggle='popover' data-trigger='hover' data-placement='top' title="<?php echo $p->titulo; ?>" data-content='<?php echo $p->detalhes; ?> <br> Clique para ver detalhes' >Nada Consta</span>
                                          <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } 
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<input type="hidden" name="imagem" id="logo_relatorio" value="<?php echo $this->session->userdata['logo']; ?>">



<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>


<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  })
var table = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                customize: function(xlsx) {
                  var sheet = xlsx.xl.worksheets['sheet1.xml'];
                  var texto = "";
                  $('row c', sheet).each(function() {
                      texto = $('is t', this).text();
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Regular">', '');
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Irregular">', '');
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Regime Normal">', '');
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Nada Consta">Nada Consta', '');
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Nada consta">Nada Consta', '');
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Simples Nacional">','');
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Todas as mensagens lidas">','');
                      texto = texto.replace('Clique para ver detalhes" data-original-title="Em análise">','');
                      $('is t', this).text(texto);
                  });
                },
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                customize: function(doc) {
                    var texto = "";
                    doc.content[1].table.widths = 
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');

                    for (var i=1;i<doc.content[1].table.body.length;i++) {
                        texto = doc.content[1].table.body[i][1].text;
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Regular">', '');
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Irregular">', '');
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Regime Normal">', '');
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Nada Consta">Nada Consta', '');
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Nada consta">Nada Consta', '');
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Simples Nacional">','');
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Todas as mensagens lidas">','');
                        texto = texto.replace('Clique para ver detalhes" data-original-title="Em análise">','');
                        doc.content[1].table.body[i][1].text = texto;
                    }

                    doc.content.splice( 1, 0, {
                        margin: [ 0, -50, 0, 12 ],
                        alignment: 'left',
                        image:  'data:text/html;base64,'+$("#logo_relatorio").val()
                    } );
                },
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'Imprimir',
                action: function ( e, dt, node, config ) {
                    imprimir();
                }
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});

function imprimir(){
  $("#example").printThis();
}

function excel(){
  window.open('data:application/vnd.ms-excel,' +  encodeURIComponent($('#example').html()));
}

function openInNewTab(url) {
 window.open(url, '_blank');
}
</script>

