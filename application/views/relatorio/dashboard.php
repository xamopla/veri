<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/
</style>

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Relatórios</h2> 
        </div> 
      </div>
    </div>
  </div>

<div class="container-fluid general-widget">

  <div class="row">

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("relatorio/pendencia_empresa"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Pendências por Empresa</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("relatorio/timeline"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important; ">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Timeline de Alterações</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("empresa/relatoriogeral"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Empresas Geral</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("empresa/contadornaovinculado"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important; ">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Contador Não Vinculado</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    
  </div>

  <div class="row">

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("relatorioempresacolaborador/gerar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important; ">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Empresas por Colaborador</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("relatorioempresacolaborador/gerarEmpresaSemColaborador"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important; ">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Empresas sem Colaborador</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("ecac/listar_empresas_com_certificado"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Empresas com Certificado</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("ecac/listar_empresas_sem_certificado"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Empresas sem Certificado</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Visão geral das Pendências das empresas em todos os níveis' data-original-title='' title=''> </div></h4><i class="pe-7s-graph2 icon-bg" style="font-size: 90px"></i>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>


  </div>

    </div>

  </div>
</div> 

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->

<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->


<script type="text/javascript">
  $(document).ready(function() {
    

      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>