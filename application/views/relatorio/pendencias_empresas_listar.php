<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }

    td {
        font-size: 16px !important;
        text-align: center;
    }

    .swal2-confirm{
      background-color: #24a0e9 !important;
      border-left-color: #24a0e9 !important;
      border-right-color: #24a0e9 !important;
    }

    .badge-info, .label-info {
      background-color: #2cc4cb;
    }
    .badge, .label {
        padding: 3px 6px;
        line-height: inherit;
        font-size: 11px;
        font-weight: 600;
        color: #fff;
        -webkit-border-radius: 2px;
        border-radius: 2px;
    }
    .icon-envelope{
      display: none !important;
    }
    .icon-zoom-in{
      display: none !important;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Pendências da Empresa - <?php echo $empresa->razao_social; ?></h2> 
          </div>
          <div class="card-body">
            <div class="ibox">
                <div class="ibox-body">
                  <ul class="nav nav-pills nav-pills-rounded nav-pills-air nav-pills-success">
                      <li class="nav-item">
                          <a class="nav-link active" href="#tab-1-1" data-toggle="tab">Pendências Federais</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link block" href="#tab-1-2" data-toggle="tab">Pendências Estaduais</a>
                      </li> 

                      <li class="nav-item">
                          <a class="nav-link block" href="#tab-1-3" data-toggle="tab">Situação Fiscal</a>
                      </li> 

                  </ul>

                  <!-- CONTEÚDO DAS TABS -->
                  <div class="tab-content">

                      <!-- TAB 1 -->
                      <div class="tab-pane fade show active" id="tab-1-1">
                          <br>                               
                              
                          <div class="table-responsive">
                            <table class="table table-striped" id="federais" data-toggle="datatables">
                                <thead class="thead-default thead-lg">
                                    <tr>
                                      <th>Razão Social</th>
                                      <th>CNPJ</th>
                                      <th>Inscrição Estadual</th>
                                      <th>Mensagens não lidas e-CAC</th>
                                      <th>Pendência Fiscal</th>
                                      <th>Parcelamento DAS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php if($possui_certificado){ ?>
                                    <tr>
                                      <td><?php echo $empresa->razao_social; ?></td>
                                      <td><?php echo $empresa->cnpj_completo; ?></td>
                                      <td><?php echo $empresa->inscricao_estadual; ?></td>
                                      <td style="text-align: center;">
                                              <?php if($msg_ecac != 0){
                                                echo '<a href="javascript:void(0)" class="badge badge-warning badge-shadow" onclick="get_mensagens_ecac('.$id_caixa_postal.');" ><i class="ti-alert" >&nbsp;</i>'.$msg_ecac.' não lidas</a>';
                                              }else{
                                                echo '<span class="badge badge-success badge-shadow"><i class="ti-alert" >&nbsp;</i>'.$msg_ecac.' não lidas</span>';
                                              }
                                              
                                              ?>
                                      </td>
                                      <td style="text-align: center;">
                                              <?php if($pendencia_fiscal != 0){
                                                echo '<a href="'.$caminho_download.'" target="_blank" class="badge badge-danger badge-shadow"><i class="ti-alert">&nbsp;</i>SIM</a>';
                                              }else{
                                                echo '<a href="'.$caminho_download.'" target="_blank" class="badge badge-success badge-shadow"><i class="ti-alert">&nbsp;</i>NÃO</a>';
                                              }
                                              
                                              ?>
                                      </td>

                                      <td style="text-align: center;">
                                              <?php if($parcelamento_das != 0){
                                                echo '<a href="javascript:void(0)" class="badge badge-info badge-shadow" onclick="mostrar_parcelas_das(&quot;'.$cnpj.'&quot;)" ><i class="ti-alert">&nbsp;</i>SIM</a>';
                                              }else{
                                                echo '<span class="badge badge-success badge-shadow"><i class="ti-alert">&nbsp;</i>NÃO</span>';
                                              }
                                              
                                              ?>
                                      </td>

                                  </tr>
                                  <?php }else{ ?>
                                    Empresa não possui certificado digital cadastrado
                                    
                                  <?php } ?>
                                  
                                </tbody>
                            </table>
                        </div>
                      </div>
                      <!-- FIM DA TAB 1 -->

                      <!-- TAB 1 -->
                      <div class="tab-pane fade" id="tab-1-2">
                          <br>                               
                          
                          <div class="table-responsive">
                            <table class="table table-striped" id="estaduais" data-toggle="datatables">
                                <thead class="thead-default thead-lg">
                                    <tr>
                                      <th>Mensagens não lidas DTE</th>
                                      <th >Sócio Irregular</th>
                                      <th >Omisso DMA</th>
                                      <th >Omisso EFD</th>
                                      <th >Parc. em Atraso</th>
                                      <th >Div. ICMS normal e o recolhido</th>
                                      <th >Div. ICMS Subs. Trib. por Antec. e o recolhido</th>
                                      <th >Div. ICMS Subs. Trib. por Retenção e o recolhido</th>
                                      <th >O.S. de Monitoramento ativa</th>
                                      <th >O.S. de Auditoria ativa</th>
                                      <th >É Beneficiário do Decreto 7.799/00?</th>
                                      <th >PAFs com ciclo de vida ativo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    
                                      <?php if(isset($resumo_fiscal)){ ?>

                                        <td>

                                        <?php if($qtd_msg_dte != 0){
                                        echo "<span class='badge badge-warning badge-shadow' data-toggle='popover' data-trigger='hover' data-placement='top' title='Mensagens não lidas' data-content='Clique para ver detalhes' onclick='get_mensagens_dte(&quot;".$empresa->login_sefaz."&quot;,&quot;".$empresa->senha_sefaz."&quot;);' >".$qtd_msg_dte." não lidas</span>";
                                      }else{
                                        echo "<span href='javascript:void(0)' class='badge badge-success badge-shadow' data-toggle='popover' data-trigger='hover' data-placement='top' title='Mensagens não lidas' onclick='get_mensagens_dte(&quot;".$empresa->login_sefaz."&quot;,&quot;".$empresa->senha_sefaz."&quot;);' );'>NÃO</span>";
                                      }
                                      ?>
                                    </td>
                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if ($resumo_fiscal->pergunta1 == '  Não  '){
                                            echo "<span  class='badge badge-success badge-shadow' data-toggle='popover' data-trigger='hover' data-placement='top' title='Sócio Irregular'>NÃO</span>";

                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Sócio Irregular' onclick='openModalDetalhesSocio($resumo_fiscal->inscricaoEstadual)' >SIM</span>";
                                        }
                                        ?></td>
                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if ($resumo_fiscal->pergunta2 == "   Não  "){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='Omisso DMA' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Omisso DMA' onclick='openModalDetalhesDMA($resumo_fiscal->inscricaoEstadual)' >SIM</span>";
                                        }
                                        ?></td>
                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if ($resumo_fiscal->pergunta3 == "    Não  "){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='Omisso EFD' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Omisso EFD' onclick='openModalDetalhesEFD($resumo_fiscal->inscricaoEstadual)' >SIM</span>";
                                        }
                                        ?></td>
                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if (strpos($resumo_fiscal->pergunta4, 'Não') == true){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='Parcelamento em atraso ou interrompido' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Parcelamento em atraso ou interrompido' onclick='openModalDetalhesParcelamentoAtraso($resumo_fiscal->inscricaoEstadual)' >SIM</span>";
                                        }
                                        ?></td>
                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if (trim($resumo_fiscal->pergunta7) == "Não" || trim($resumo_fiscal->pergunta7) == "|||" || trim($resumo_fiscal->pergunta7) == ""){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='Divergência entre ICMS normal informado na DMA e o recolhido'>NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Divergência entre ICMS normal informado na DMA e o recolhido' onclick='openModalDetalhes($resumo_fiscal->inscricaoEstadual,7)' >SIM</span>";
                                        }
                                        ?></td>

                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if (trim($resumo_fiscal->pergunta8) == "Não" || trim($resumo_fiscal->pergunta8) == "|||" || trim($resumo_fiscal->pergunta8) == ""){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido' onclick='openModalDetalhes($resumo_fiscal->inscricaoEstadual,8)' >SIM</span>";
                                        }
                                        ?></td>

                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if (trim($resumo_fiscal->pergunta9) == "Não" || trim($resumo_fiscal->pergunta9) == "|||" || trim($resumo_fiscal->pergunta9) == ""){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido' onclick='openModalDetalhes($resumo_fiscal->inscricaoEstadual,9)' >SIM</span>";
                                        }
                                        ?></td>

                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if ($resumo_fiscal->pergunta18 == "    Não  "){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='O.S. de Monitoramento ativa' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='O.S. de Monitoramento ativa' onclick='openModalDetalhesOsMonitoramento($resumo_fiscal->inscricaoEstadual)' >SIM</span>";
                                        }
                                        ?></td>

                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if ($resumo_fiscal->pergunta19 == "    Não  "){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='O.S. de Auditoria ativa' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='O.S. de Auditoria ativa' onclick='openModalDetalhesOsAuditoria($resumo_fiscal->inscricaoEstadual)' >SIM</span>";
                                        }
                                        ?></td>

                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if ($resumo_fiscal->pergunta25 == "   Não  "){
                                            echo "<span class='badge badge-info badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='É Beneficiário do Decreto 7.799/00?' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='É Beneficiário do Decreto 7.799/00?' >SIM</span>";
                                        }
                                        ?></td>

                                    <td style="font-size: 8px; text-align: center;"><?php
                                        if (trim($resumo_fiscal->pergunta6) == "Não" || trim($resumo_fiscal->pergunta6) == ""){
                                            echo "<span class='badge badge-success badge-shadow' style='' data-toggle='popover' data-trigger='hover' data-placement='top' title='PAFs com ciclo de vida ativo' >NÃO</span>";
                                        } else {
                                            echo "<span class='badge badge-danger badge-shadow' style='border: white;' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes' data-original-title='' title='PAFs com ciclo de vida ativo' onclick='openModalDetalhesPaf($resumo_fiscal->inscricaoEstadual)' >SIM</span>";
                                        }
                                        ?></td>

                                      <?php }else{ echo 'Empresa não possui senha DTE cadastrada';} ?>
                                      
                                </tr>
                                </tbody>
                            </table>
                          </div> 
                          
                      </div>
                      <!-- FIM DA TAB 1 -->

                      <!-- TAB 1 -->
                      <div class="tab-pane fade" id="tab-1-3">
                          <br>                               
                              
                          <div class="table-responsive">
                            <table class="table table-striped" id="municipais" data-toggle="datatables">
                                <thead class="thead-default thead-lg">
                                    <tr>
                                      <th>Inscrição Estadual</th>
                                      <th>Conta DTE</th>
                                      <th>Situação Cadastral</th>
                                      <th>Antec. Parcial</th>
                                      <th>Contador Vinculado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td style=""><span data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Condição: <?php echo $situacao_fiscal->condicao; ?>" data-content="Forma de Pagamento: <?php echo $situacao_fiscal->forma_pagamento; ?>"><?php echo $situacao_fiscal->inscricao_estadual_completo; ?></span></td>

                                    <td style="text-align: center;">
                                    <?php if ($situacao_fiscal->situacao_conta_dte) { ?>
                                    <span <?php if ($situacao_fiscal->situacao_conta_dte == 'INEXISTENTE'){echo "class='badge badge-danger badge-shadow'";} elseif ($situacao_fiscal->situacao_conta_dte == 'INATIVA') {echo "class='badge badge-warning badge-shadow'";} else {echo "class='badge badge-success badge-shadow' style='background-color:#2ecc71!important;'";}?> data-container='body' data-toggle='popover' data-trigger="hover" data-placement='top' title='Situação DTE' data-content="<?php echo $situacao_fiscal->situacao_dte; ?>">
                                      <?php echo $situacao_fiscal->situacao_conta_dte; ?>
                                    </span>
                                    <?php } ?>
                                    </td>

                                    <td style="text-align: center;">
                                    <?php
                                    
                                    if ($situacao_fiscal->situacao_cadastral == 'ATIVO' || $situacao_fiscal->situacao_cadastral == 'ATIVA'){
                                      echo "<span class='badge badge-success badge-shadow' data-container='body' style='background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo desta Situação Cadastral' data-content='$situacao_fiscal->motivo_situacao_cadastral'>$situacao_fiscal->situacao_cadastral</span>";
                                    } else if ($situacao_fiscal->situacao_cadastral == 'BAIXADO'){
                                      echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo desta Situação Cadastral' data-content='$situacao_fiscal->motivo_situacao_cadastral'>$situacao_fiscal->situacao_cadastral</span>";
                                    } else {
                                      echo "<span class='badge badge-warning badge-shadow'  data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo desta Situação Cadastral' data-content='$situacao_fiscal->motivo_situacao_cadastral'>$situacao_fiscal->situacao_cadastral</span>";
                                    }
                                    
                                    ?>
                                    </td>

                                    <td style="text-align: center;"><?php 
                                    if ($situacao_fiscal->situacao == 'Descredenciado'){
                                        echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo' data-content='$situacao_fiscal->motivo'>$situacao_fiscal->situacao</span>";
                                    } else if($situacao_fiscal->situacao != ''){
                                        echo "<span class='badge badge-success badge-shadow' style='background-color:#2ecc71!important;' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo' data-content='$situacao_fiscal->motivo'>$situacao_fiscal->situacao</span>";
                                    }else{
                                      echo "<span>$situacao_fiscal->situacao</span>";
                                    }
                                    ?>                            
                                    </td>

                                    <td style="text-align: center;"><?php 
                                    if ($situacao_fiscal->vinculo_contador == 0){
                                        echo "<span class='badge badge-danger badge-shadow'>NÃO</span>";
                                    } else {
                                      echo "<span class='badge badge-success badge-shadow' style='background-color:#2ecc71!important;'>SIM</span>";
                                    }
                                    ?></td>
                                </tr>
                                </tbody>
                            </table>
                          </div>
                      </div>
                      <!-- FIM DA TAB 1 -->
                  </div>                          
                  
                    
                </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>



<!-- MODAL PARCELAS -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_parcelas">
    <div class="modal-dialog" role="document" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Parcelamento do DAS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="modal_table_parcelas" class="table table-bordered table-hover" style="width: 100% !important">    
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th>Valor</th>
                          <th>Data da Parcela</th>
                        </tr>
                    </thead>
                    <tbody id="body_parcelas">
                    </tbody>                
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- FIM MODAL PARCELAS -->


<div class="modal fade" id="detalhe-modal">
    <div class="modal-dialog" role="document" style="width:500px;">
        <form class="modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title" id="title"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="flexbox mb-4">
                    <div class="flex-1 d-flex">
                        <div class="flex-1">
                            <div id="conteudo"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal das mensagens -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="modal_mensagens">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagens DTE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div style="width: 200px;margin-left: 15px;" id="div_filter">
                    <p>
                        Situação: 
                        <select id="table-filter" class="form-control form-control-sm">
                            <option value="">Todas Mensagens</option>
                            <option value="Não Lida">Mensagens Não Lidas</option>
                        </select>
                    </p>
                </div>
                <table id="modal_table" class="table table-striped">                    
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Leitura da Mensagem -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ler_mensagem">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_mensagem"></h5>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body_mensagem">
            </div>
            <div class="modal-footer">
                <a id="link_pdf_mensagem" href="#" type="button" class="btn btn-secondary" target="_blank"><i class="ti-printer"></i>&nbsp;Imprimir</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal das mensagens -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_mensagens_ecac">
    <div class="modal-dialog" role="document" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensagens e-CAC</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div style="width: 200px;margin-left: 15px;" id="div_filter_ecac">
                    <p>
                        Situação: 
                        <select id="table-filter_ecac" class="form-control form-control-sm">
                            <option value="">Todas Mensagens</option>
                            <option value="Não Lida">Mensagens Não Lidas</option>
                        </select>
                    </p>
                </div>
                <table id="modal_table_ecac" class="table table-striped" style="width: 100% !important">    
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th></th>
                          <th width="8%"></th>
                          <th width="8%"></th>
                          <th>Remetente</th>
                          <th>Assunto da Mensagem</th>
                          <th>Enviada em</th>
                          <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>                
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Leitura da Mensagem -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ler_mensagem_ecac">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudo_modal_ecac">
            <div class="modal-header">
                <h5 class="modal-title" id="title_mensagem_ecac"></h5>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body_mensagem_ecac">
            </div>
            <div class="modal-footer">
                <button id="link_pdf_mensagem" onclick="imprimir();" type="button" class="btn btn-secondary" ><i class="ti-printer"></i>&nbsp;Imprimir</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <div class="printable"></div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script type="text/javascript">

    function get_mensagens_dte($login, $senha){
        $(document).ready(function() {
            $(document).ready(function() {
                swal({ 
                    title: "Buscando...",
                    text: 'Buscando Mensagens DTE da Empresa na Sefaz.\nPor favor, aguardar alguns segundos...',
                    type: "info" ,
                    confirmButtonText: "Cancelar",
                    confirmButtonColor: "#fff"
                },
                function(isConfirm){
                    if(isConfirm){
                        request.abort();
                    }
                });
            });
        });
        var url = "<?= base_url() ?>/Dte/get_mensagens/"+$login+"/"+$senha;
        var request = $.get(url, function(resultado){             
            response = JSON.parse(resultado);
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#modal_table').html(response['table']);
                $('#modal_mensagens').modal('show');
                $(document).ready( function () {
                    $.fn.dataTable.moment( 'DD/MM/YYYY HH:mm:ss' ); 
                    $('#modal_table').dataTable().fnDestroy();
                    var table = $('#modal_table').DataTable({
                        lengthMenu: [5, 10, 20, 50, 100],
                        "order": [[ 3, "desc" ]],
                        language: {
                            "sEmptyTable": "Nenhum registro encontrado",
                            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sInfoThousands": ".",
                            "sLengthMenu": "_MENU_ resultados por página",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sZeroRecords": "Nenhum registro encontrado",
                            "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sNext": "Próximo",
                                "sPrevious": "Anterior",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                            },
                            "oAria": {
                                "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                            },
                            "select": {
                                "rows": {
                                    "_": "Selecionado %d linhas",
                                    "0": "Nenhuma linha selecionada",
                                    "1": "Selecionado 1 linha"
                                }
                            }
                        }
                    });
                    $("#div_filter select").val("Não Lida");
                    table.search('Não Lida').draw();

                    $('#table-filter').on('change', function(){
                        table.search(this.value).draw();   
                    });
                });    
            }        
        });
    }

    function ler_mensagem($id, $login, $senha){        
        $('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/Dte/ler_mensagem/"+$login+"/"+$senha+"/"+encodeURIComponent($id);
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#modal_mensagens').modal('show');
                response['body'] = response['body'].replace(/(?:\\[rn])+/g, "<br>");
                response['body'] = response['body'].replace('<br>            ', '');
                $('#title_mensagem').html(response['title']);
                $('#body_mensagem').html(response['body']);
                $('#modal_ler_mensagem').modal('show');

                $('#link_pdf_mensagem').attr('href', "<?= base_url() ?>/Dte/get_pdf/"+$login+"/"+$senha+"/"+encodeURIComponent($id));
            } 
        });
    }
</script>

<script type="text/javascript">

    function get_mensagens_ecac($id_caixa_postal){
        var url = "<?= base_url() ?>/Ecac/listar_mensagens_by_empresa/"+$id_caixa_postal;
        var request = $.get(url, function(resultado){             
            response = JSON.parse(resultado);
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                $('#modal_mensagens_ecac').modal('show');
                $(document).ready( function () {
                    var array = json2array(response['table']);
                    
                    $.fn.dataTable.moment( 'DD/MM/YYYY' ); 
                    $('#modal_table_ecac').dataTable().fnDestroy();
                    var table = $('#modal_table_ecac').DataTable({
                        "columnDefs": [
                            {
                                "targets": [ 0 ],
                                "visible": false,
                                "searchable": false
                            }
                        ],
                        lengthMenu: [10, 20, 50, 100],
                        "order": [[ 5, "desc" ]],
                        language: {
                            "sEmptyTable": "Nenhum registro encontrado",
                            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sInfoThousands": ".",
                            "sLengthMenu": "_MENU_ resultados por página",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sZeroRecords": "Nenhum registro encontrado",
                            "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sNext": "Próximo",
                                "sPrevious": "Anterior",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                            },
                            "oAria": {
                                "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                            },
                            "select": {
                                "rows": {
                                    "_": "Selecionado %d linhas",
                                    "0": "Nenhuma linha selecionada",
                                    "1": "Selecionado 1 linha"
                                }
                            }
                        },
                        data: array
                    });
                    $("#div_filter_ecac select").val("Não Lida");
                    table.search('Não Lida').draw();

                    $('#table-filter_ecac').on('change', function(){
                        table.search(this.value).draw();   
                    });
                });    
            }        
        });
    }

    function json2array(data){
        var string = "";
        var array = [];
        var arrayMultiple = [];

        for(var i in data){
          array = [];

          array.push('<td style="display:none;">'+data[i].id+'</td>');
          if(data[i].importante == 1){
            array.push('<td style="text-align: left;"><img title="Mensagem Relevante" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/exclamation.gif'); ?>" style="border-width:0px;"></td>');
          }else{
            array.push('<td style="text-align: left;"></td>');
          }

          if(data[i].lida == 1){
            array.push('<td style="text-align: left;"><img title="Mensagem lida" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/aaMsgLida.gif'); ?>" style="border-width:0px;"></td>');
          }else{
            array.push('<td style="text-align: left;"><img title="Mensagem não lida" viewstatemode="Enabled" src="<?php echo base_url('assets/img/icons/aaMsgNaoLida.gif'); ?>" style="border-width:0px;"></td>');
          }
          
          
          array.push('<td style="text-align: left;">'+data[i].remetente+'</td>');
          array.push('<td style="text-align: left;">'+data[i].assunto+'</td>');

          data[i].data = convertDate2(data[i].recebida_em);
          array.push('<td style="text-align: left;">'+data[i].data+'</td>');

          if(data[i].lida == 1){
            array.push('<td width="5%"><center><a title="Ler Mensagem" class="badge badge-info badge-shadow" rel="tooltip" href="javascript:ler_mensagem2('+data[i].id+', &quot;'+data[i].assunto+'&quot;)"><i class="icon-zoom-in "></i> Ler</a></center></td>');
          }else{
            array.push('<td width="5%"><center><a title="Ler Mensagem" class="badge badge-info badge-shadow" rel="tooltip" href="javascript:ler_mensagem_ecac('+data[i].caixa_postal_id+', '+data[i].id+', &quot;'+data[i].assunto+'&quot;)"><i class="icon-zoom-in "></i> Ler</a></center></td>');
          }
          
          arrayMultiple.push(array);
        }
        
        return arrayMultiple;
    }

    function ler_mensagem2($id, $assunto){        
        //$('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/Ecac/ler_mensagem/"+$id;
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem_ecac').html($assunto);
                $('#body_mensagem_ecac').html(response['body']);
                $('#modal_ler_mensagem_ecac').modal('show');
            } 
        });
    }

    function ler_mensagem_ecac(id, id_mensagem,assunto){        
        //$('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/Ecac/ler_mensagem_ecac/"+id+"/"+id_mensagem;
        var request = $.get(url, function(data, status){

            var response;
            if(data.includes("PFX")){
                var string_data = data.substring(data.indexOf("{"));
                response = JSON.parse(string_data); 
            }else{
              response = JSON.parse(data);  
            }
            
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Erro ao buscar mensagem !",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem_ecac').html(assunto);
                $('#body_mensagem_ecac').html(response['mensagem']);
                $('#modal_ler_mensagem_ecac').modal('show');
            } 
        });
    }

    function imprimir(){
        $(".printable").html($("#conteudo_modal_ecac").html());
        $(".printable").printThis();
    }

    function convertDate2(a){
      var day = a.split('-')[2];
      day = day.split(' ')[0];
      var month = a.split('-')[1];
      var year = a.split('-')[0];

      var date = day + "/" + month + "/" + year;
      return date;
    }
</script>

<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  })
$('#example').DataTable({
        "iDisplayLength": 50,
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

  $('#estaduais').DataTable({
        "iDisplayLength": 50,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "searching":false,
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

  $('#municipais').DataTable({
        "iDisplayLength": 50,
        "paging":   false,
        "ordering": false,
        "info":     true,
        "searching":false,
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });


});


</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable({
            "iDisplayLength": 50,
            lengthChange: false,
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'EXCEL',
                    exportOptions: {
                        modifier: {
                            page: 'all'
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
                {
                    extend: 'colvis',
                    text: 'COLUNAS'
                }
            ],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "bDestroy": true
        }).buttons().container()
            .appendTo( '#example_wrapper .col-md-6:eq(0)' );

        $('#filtro').change(function(){
            var filtro = $('#filtro option:selected').val();
            window.location.replace("<?php echo base_url('resumofiscal'); ?>/"+filtro+"");
        });
    });


    function consultaDTE(id){
        $("#btn-submit"+id).click();
        //window.close();
        setTimeout(function() {
            //window.close();
            $("#btn-submit"+id+"Enter").click();
        }, 2000);
    }

    function openModalDetalhes(inscricaoEstadual, numeroPergunta){
        $.post("<?php echo base_url();?>resumofiscal/findPergunta",
            {
                inscricaoEstadual:inscricaoEstadual,
                pergunta:numeroPergunta
            },
            function(data){

                if(numeroPergunta == 7){
                    $("#title").html("Divergência entre ICMS normal informado na DMA e o recolhido");
                }else if(numeroPergunta == 8){
                    $("#title").html("Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido");
                }else{
                    $("#title").html("Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido");
                }
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }


    function openModalDetalhesPaf(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findPaf",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("PAFs com ciclo de vida ativo");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesSocio(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findSocioIrregular",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Sócio(s) Irregular(es)");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesEFD(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findEFD",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Omisso EFD");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesDMA(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findDMA",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Omisso DMA");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesOsMonitoramento(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findOsMonitoramento",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("O.S de Monitoramento Ativa");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesOsAuditoria(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findOsAuditoria",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("O.S de Auditoria Ativa");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function openModalDetalhesParcelamentoAtraso(inscricaoEstadual){
        $.post("<?php echo base_url();?>resumofiscal/findParcelamentoEmAtraso",
            {
                inscricaoEstadual:inscricaoEstadual
            },
            function(data){
                $("#title").html("Parcelamento em Atraso");
                $("#conteudo").html(data);
                $('#detalhe-modal').modal();
            });
    }

    function mostrar_parcelas_das(cnpj){
      $.post("<?php echo base_url();?>relatorio/mostrar_parcelamento",
      {
          cnpj:cnpj
      },
      function(data){
          var parcelas = JSON.parse(data);

          for(var i in parcelas){

            $("#body_parcelas").append('<tr><td>R$ '+parcelas[i].valor+'</td><td>'+parcelas[i].data_parcela+'</td><tr>');
          }
          //$("#modal_table_parcelas").html(data);
          $('#modal_parcelas').modal();
      });
    }
</script>

<!--script abrir socio irregular-->
<script type="text/javascript">
    function openWindowWithPost(cpf) {
        var f = document.getElementById('frm_socio');
        f.CPF.value = cpf;
        window.open('', 'TheWindow');
        f.submit();
    }
</script>
