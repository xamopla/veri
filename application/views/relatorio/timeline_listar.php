<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<!-- Font Awesome-->
<link rel="stylesheet" type="text/css" href="../assets/css/fontawesome.css">
<!-- ico-font-->
<link rel="stylesheet" type="text/css" href="../assets/css/icofont.css">
<!-- Themify icon-->
<link rel="stylesheet" type="text/css" href="../assets/css/themify.css">
<!-- Flag icon-->
<link rel="stylesheet" type="text/css" href="../assets/css/flag-icon.css">
<!-- Feather icon-->
<link rel="stylesheet" type="text/css" href="../assets/css/feather-icon.css">
<link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
<!-- Plugins css start-->
<link rel="stylesheet" type="text/css" href="../assets/css/pe7-icon.css">
<!-- Plugins css Ends-->
<!-- Bootstrap css-->
<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
<!-- App css-->
<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<link id="color" rel="stylesheet" href="../assets/css/light-1.css" media="screen">
<!-- Responsive css-->
<link rel="stylesheet" type="text/css" href="../assets/css/responsive.css">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2>Timeline de Alterações</h2>
          </div>
          <div class="card-body">
            <!-- cd-timeline Start-->
            <section class="cd-container" id="cd-timeline">

              <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-location bg-info bounce-in"><i class="icon-pencil-alt"></i></div>
                <div class="cd-timeline-content">
                  <h5>Empresa cadastrada no sistema</h5><p class="m-0"></p><span class="cd-date"><?php echo date('d/m/Y H:i:s', strtotime($criacao)); ?></span>
                </div>
              </div>

              <?php if ($consulta){ 
                foreach ($consulta as $e){ $titulo = ""; ?>

                  <div class="cd-timeline-block">
                    <!-- <div class="cd-timeline-img cd-picture bg-secondary"> -->
                      <?php if($e->is_resumo_fiscal == 0){
                        echo '<div class="cd-timeline-img cd-picture bg-success bounce-in">';
                        $titulo = "Situação Fiscal";
                        echo '<i class="icon-bell"></i>';
                        echo '</div>';
                      }else{
                        echo '<div class="cd-timeline-img cd-movie bg-danger bounce-in">';
                        $titulo = "Resumo Fiscal";
                        echo '<i class="icon-stamp"></i>';
                        echo '</div>';
                      }
                      ?>

                      <div class="cd-timeline-content">
                        <h5><?php echo $titulo; ?></h5>
                        <p class="m-0"><?php echo $e->descricao; ?></p><span class="cd-date"><?php echo date('d/m/Y H:i:s', strtotime($e->data)); ?></span>
                      </div>
                    </div>

                <?php } 
              }
              ?>

              

            </section>
            <!-- cd-timeline Ends-->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid ends                    -->
</div>

<!-- latest jquery-->
<script src="../assets/js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap js-->
<script src="../assets/js/bootstrap/popper.min.js"></script>
<script src="../assets/js/bootstrap/bootstrap.js"></script>
<!-- feather icon js-->
<script src="../assets/js/icons/feather-icon/feather.min.js"></script>
<script src="../assets/js/icons/feather-icon/feather-icon.js"></script>
<!-- Sidebar jquery-->
<script src="../assets/js/sidebar-menu.js"></script>
<script src="../assets/js/config.js"></script>
<!-- Plugins JS start-->
<script src="../assets/js/timeline/timeline-v-1/main.js"></script>
<script src="../assets/js/chat-menu.js"></script>
<script src="../assets/js/modernizr.js"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="../assets/js/script.js"></script>
<script src="../assets/js/theme-customizer/customizer.js"></script>