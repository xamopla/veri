<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/
</style>

<style type="text/css">
  .card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details {
    position: absolute;
    top: 0;
    left: 0;
    width: 70%;/*100%*/
    height: 100%;
    transition: .5s;
    transform-origin: left;
    transform: perspective(2000px) rotateY(-90deg);
}
.card:hover .details {
    transform: perspective(2000px) rotateY(0deg);
}
.card .details .center {
    width: 100% !important;
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details .center p {
    margin: 10px 0;
    padding: 0;
    color: #262626;
}
.card .details .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details .center ul li a:hover {
    background: #ff3636;
}


.div_hover { 
  background: white;
  border-radius: 50px; 
}

.div_hover a:hover { 
  background-color: #24a0e9; 
  color: white !important;
}

.tag_a_hover {
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.span_hover:hover{
  background-color: #24a0e9; 
  color: white !important;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.media-body{
  padding-left: 0px !important; 
}
</style>

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Parcelamentos</h2> 
        </div> 
      </div>
    </div>
  </div>

<div class="container-fluid general-widget">

  <div class="row">

    <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("resumofiscal/parcelamentoatraso"); ?>">
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
              </div>
              <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">SEFAZ - Parcelamento em Atraso</span>
                <h4 class="mb-0 text-white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de empresas que possuem Parcelamento Atrasado segundo o Resumo Fiscal' data-original-title='' title=''><?php echo $qtd_parcatraso->valor; ?></div></h4><i class="pe-7s-copy-file icon-bg" style="font-size: 90px"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div> -->

    <!-- inicio do card DAS -->
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("parcelamento_das/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 18px;color: #24a0e9" class="span_hover">Parcelamento DAS</span>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("parcelamento_das/listar/EM_PARCELAMENTO"); ?>" style="font-size: 16px;" class="tag_a_hover">Em parcelamento: <?php 
                        if($parcelamento_das_qtd_em_parcelamento){echo $parcelamento_das_qtd_em_parcelamento;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("parcelamento_das/listar/ENCERRADO_RECISAO"); ?>" style="font-size: 16px;" class="tag_a_hover">Encerrado Recisão: <?php 
                        if($parcelamento_das_qtd_encerrado_recisao){echo $parcelamento_das_qtd_encerrado_recisao;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("parcelamento_das/listar/ENCERRADO_CONTRIBUINTE"); ?>" style="font-size: 16px;" class="tag_a_hover">Encerrado Contribuinte: <?php 
                        if($parcelamento_das_qtd_encerrado_contibuinte){echo $parcelamento_das_qtd_encerrado_contibuinte;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </teste>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">DAS - Parcelamentos</span>
              <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Parcelamento no DAS' data-original-title='' title=''><?php echo $parcelamento_das_qtd; ?></div></h4>
              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="225.000000pt" height="225.000000pt" viewBox="0 0 225.000000 225.000000" preserveAspectRatio="xMidYMid meet">
                <metadata>
                Created by potrace 1.16, written by Peter Selinger 2001-2019
                </metadata>
                <g transform="translate(0.000000,225.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                <path d="M92 2234 l-22 -15 0 -953 c0 -806 2 -955 14 -972 14 -18 34 -19 514 -24 l499 -5 35 -45 c45 -60 158 -144 235 -175 224 -89 473 -37 643 134 122 124 176 267 167 446 -6 123 -31 200 -99 301 -75 113 -184 193 -318 235 l-55 17 -2 518 c-3 515 -3 519 -24 537 -20 16 -73 17 -793 17 -682 0 -774 -2 -794 -16z m1478 -579 c0 -364 -3 -466 -12 -470 -7 -2 -37 -6 -65 -10 l-53 -6 0 130 c0 123 -1 131 -22 145 -20 14 -85 16 -518 16 -272 0 -505 -3 -518 -6 -50 -14 -52 -32 -52 -466 0 -349 2 -407 16 -426 15 -22 16 -22 334 -22 l319 0 11 -57 c7 -32 15 -63 17 -70 4 -10 -78 -13 -411 -13 l-416 0 0 860 0 860 685 0 685 0 0 -465z m-980 -395 l0 -70 -65 0 -65 0 0 70 0 70 65 0 65 0 0 -70z m725 0 l0 -65 -295 0 -295 0 -3 68 -3 67 298 -2 298 -3 0 -65z m-725 -260 l0 -70 -65 0 -65 0 0 65 0 64 63 4 c34 2 63 4 65 5 1 1 2 -29 2 -68z m627 58 c-1 -3 -28 -34 -60 -67 l-58 -61 -190 0 -189 0 0 63 c0 35 2 66 4 68 7 7 496 4 493 -3z m498 -20 c100 -25 205 -104 266 -199 141 -221 57 -536 -175 -653 -184 -93 -378 -67 -527 72 -353 326 -32 900 436 780z m-1123 -303 l3 -65 -67 0 -68 0 0 65 0 65 64 0 64 0 4 -65z m438 58 c-1 -5 -7 -33 -14 -63 l-12 -55 -142 -3 -142 -3 0 66 0 65 155 0 c85 0 155 -3 155 -7z" style="fill: white;"/>
                <path d="M366 1975 c-32 -16 -43 -58 -22 -89 l16 -26 294 0 c271 0 294 1 309 18 21 23 22 66 1 86 -8 9 -18 16 -22 17 -87 6 -561 1 -576 -6z" style="fill: white;"/>
                <path d="M350 1700 c-28 -28 -25 -64 6 -89 26 -21 36 -21 525 -21 486 0 500 1 519 20 25 25 26 72 2 93 -17 15 -68 17 -525 17 -494 0 -508 -1 -527 -20z" style="fill: white;"/>
                <path d="M1545 955 c-14 -13 -25 -36 -25 -50 0 -17 -7 -28 -22 -34 -30 -11 -78 -60 -89 -89 -14 -36 -11 -110 6 -141 23 -45 67 -76 150 -106 84 -31 102 -50 79 -83 -27 -39 -114 -18 -114 28 0 19 -44 60 -65 60 -7 0 -25 -10 -40 -22 -24 -19 -27 -26 -22 -65 5 -51 41 -101 91 -128 28 -15 36 -26 36 -47 0 -35 32 -68 65 -68 33 0 65 33 65 68 0 21 8 31 37 47 98 53 117 178 40 261 -22 23 -60 45 -117 67 -80 31 -85 35 -85 62 0 23 6 32 27 39 38 13 74 -6 92 -49 19 -44 56 -63 91 -47 69 32 31 163 -60 206 -28 14 -35 23 -35 46 0 35 -31 70 -60 70 -12 0 -32 -11 -45 -25z" style="fill: white;"/>
                </g>
              </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    <!-- fim do card DAS -->
    <!-- inicio do card PERT -->
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("parcelamento_pert/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 18px;color: #24a0e9" class="span_hover">Parcelamento PERT-SN</span>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("parcelamento_pert/listar/EM_PARCELAMENTO"); ?>" style="font-size: 16px;" class="tag_a_hover">Em parcelamento: <?php 
                        if($parcelamento_pert_qtd_em_parcelamento){echo $parcelamento_pert_qtd_em_parcelamento;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("parcelamento_pert/listar/ENCERRADO_RECISAO"); ?>" style="font-size: 16px;" class="tag_a_hover">Encerrado Recisão: <?php 
                        if($parcelamento_pert_qtd_encerrado_recisao){echo $parcelamento_pert_qtd_encerrado_recisao;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("parcelamento_pert/listar/ENCERRADO_CONTRIBUINTE"); ?>" style="font-size: 16px;" class="tag_a_hover">Encerrado Contribuinte: <?php 
                        if($parcelamento_pert_qtd_encerrado_contibuinte){echo $parcelamento_pert_qtd_encerrado_contibuinte;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </teste>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">PERT-SN - Parcelamentos</span>
             <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Parcelamento no PERT' data-original-title='' title=''><?php echo $parcelamento_pert_qtd; ?></div></h4>
                <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="225.000000pt" height="225.000000pt" viewBox="0 0 225.000000 225.000000" preserveAspectRatio="xMidYMid meet">
                    <metadata>
                        Created by potrace 1.16, written by Peter Selinger 2001-2019
                    </metadata>
                    <g transform="translate(0.000000,225.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                        <path d="M92 2234 l-22 -15 0 -953 c0 -806 2 -955 14 -972 14 -18 34 -19 514 -24 l499 -5 35 -45 c45 -60 158 -144 235 -175 224 -89 473 -37 643 134 122 124 176 267 167 446 -6 123 -31 200 -99 301 -75 113 -184 193 -318 235 l-55 17 -2 518 c-3 515 -3 519 -24 537 -20 16 -73 17 -793 17 -682 0 -774 -2 -794 -16z m1478 -579 c0 -364 -3 -466 -12 -470 -7 -2 -37 -6 -65 -10 l-53 -6 0 130 c0 123 -1 131 -22 145 -20 14 -85 16 -518 16 -272 0 -505 -3 -518 -6 -50 -14 -52 -32 -52 -466 0 -349 2 -407 16 -426 15 -22 16 -22 334 -22 l319 0 11 -57 c7 -32 15 -63 17 -70 4 -10 -78 -13 -411 -13 l-416 0 0 860 0 860 685 0 685 0 0 -465z m-980 -395 l0 -70 -65 0 -65 0 0 70 0 70 65 0 65 0 0 -70z m725 0 l0 -65 -295 0 -295 0 -3 68 -3 67 298 -2 298 -3 0 -65z m-725 -260 l0 -70 -65 0 -65 0 0 65 0 64 63 4 c34 2 63 4 65 5 1 1 2 -29 2 -68z m627 58 c-1 -3 -28 -34 -60 -67 l-58 -61 -190 0 -189 0 0 63 c0 35 2 66 4 68 7 7 496 4 493 -3z m498 -20 c100 -25 205 -104 266 -199 141 -221 57 -536 -175 -653 -184 -93 -378 -67 -527 72 -353 326 -32 900 436 780z m-1123 -303 l3 -65 -67 0 -68 0 0 65 0 65 64 0 64 0 4 -65z m438 58 c-1 -5 -7 -33 -14 -63 l-12 -55 -142 -3 -142 -3 0 66 0 65 155 0 c85 0 155 -3 155 -7z" style="fill: white;"/>
                        <path d="M366 1975 c-32 -16 -43 -58 -22 -89 l16 -26 294 0 c271 0 294 1 309 18 21 23 22 66 1 86 -8 9 -18 16 -22 17 -87 6 -561 1 -576 -6z" style="fill: white;"/>
                        <path d="M350 1700 c-28 -28 -25 -64 6 -89 26 -21 36 -21 525 -21 486 0 500 1 519 20 25 25 26 72 2 93 -17 15 -68 17 -525 17 -494 0 -508 -1 -527 -20z" style="fill: white;"/>
                        <path d="M1545 955 c-14 -13 -25 -36 -25 -50 0 -17 -7 -28 -22 -34 -30 -11 -78 -60 -89 -89 -14 -36 -11 -110 6 -141 23 -45 67 -76 150 -106 84 -31 102 -50 79 -83 -27 -39 -114 -18 -114 28 0 19 -44 60 -65 60 -7 0 -25 -10 -40 -22 -24 -19 -27 -26 -22 -65 5 -51 41 -101 91 -128 28 -15 36 -26 36 -47 0 -35 32 -68 65 -68 33 0 65 33 65 68 0 21 8 31 37 47 98 53 117 178 40 261 -22 23 -60 45 -117 67 -80 31 -85 35 -85 62 0 23 6 32 27 39 38 13 74 -6 92 -49 19 -44 56 -63 91 -47 69 32 31 163 -60 206 -28 14 -35 23 -35 46 0 35 -31 70 -60 70 -12 0 -32 -11 -45 -25z" style="fill: white;"/>
                    </g>
                </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    <!-- fim do card PERT -->


    <!-- inicio do card parcelamento nao previdenciario -->
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("parcelamento_nao_previdenciario/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 16px;color: #24a0e9" class="span_hover">Parcelamento  Não Previdenciário</span>
                
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("parcelamento_nao_previdenciario/listar/PARCELADO"); ?>" style="font-size: 16px;" class="tag_a_hover">Em parcelamento: <?php 
                        if($parcelamento_nao_previdenciario_qtd){echo $parcelamento_nao_previdenciario_qtd_em_parcelamento;}else{
                          echo "0";
                        }  ?>
                    </a>
                  </div>
                </teste>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Parcelamento <br/> Não Previdenciário</span>
              <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Parcelamento Não Previdenciário' data-original-title='' title=''><?php echo $parcelamento_nao_previdenciario_qtd; ?></div></h4>
               <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="225.000000pt" height="225.000000pt" viewBox="0 0 225.000000 225.000000" preserveAspectRatio="xMidYMid meet">
                    <metadata>
                        Created by potrace 1.16, written by Peter Selinger 2001-2019
                    </metadata>
                    <g transform="translate(0.000000,225.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                        <path d="M92 2234 l-22 -15 0 -953 c0 -806 2 -955 14 -972 14 -18 34 -19 514 -24 l499 -5 35 -45 c45 -60 158 -144 235 -175 224 -89 473 -37 643 134 122 124 176 267 167 446 -6 123 -31 200 -99 301 -75 113 -184 193 -318 235 l-55 17 -2 518 c-3 515 -3 519 -24 537 -20 16 -73 17 -793 17 -682 0 -774 -2 -794 -16z m1478 -579 c0 -364 -3 -466 -12 -470 -7 -2 -37 -6 -65 -10 l-53 -6 0 130 c0 123 -1 131 -22 145 -20 14 -85 16 -518 16 -272 0 -505 -3 -518 -6 -50 -14 -52 -32 -52 -466 0 -349 2 -407 16 -426 15 -22 16 -22 334 -22 l319 0 11 -57 c7 -32 15 -63 17 -70 4 -10 -78 -13 -411 -13 l-416 0 0 860 0 860 685 0 685 0 0 -465z m-980 -395 l0 -70 -65 0 -65 0 0 70 0 70 65 0 65 0 0 -70z m725 0 l0 -65 -295 0 -295 0 -3 68 -3 67 298 -2 298 -3 0 -65z m-725 -260 l0 -70 -65 0 -65 0 0 65 0 64 63 4 c34 2 63 4 65 5 1 1 2 -29 2 -68z m627 58 c-1 -3 -28 -34 -60 -67 l-58 -61 -190 0 -189 0 0 63 c0 35 2 66 4 68 7 7 496 4 493 -3z m498 -20 c100 -25 205 -104 266 -199 141 -221 57 -536 -175 -653 -184 -93 -378 -67 -527 72 -353 326 -32 900 436 780z m-1123 -303 l3 -65 -67 0 -68 0 0 65 0 65 64 0 64 0 4 -65z m438 58 c-1 -5 -7 -33 -14 -63 l-12 -55 -142 -3 -142 -3 0 66 0 65 155 0 c85 0 155 -3 155 -7z" style="fill: white;"/>
                        <path d="M366 1975 c-32 -16 -43 -58 -22 -89 l16 -26 294 0 c271 0 294 1 309 18 21 23 22 66 1 86 -8 9 -18 16 -22 17 -87 6 -561 1 -576 -6z" style="fill: white;"/>
                        <path d="M350 1700 c-28 -28 -25 -64 6 -89 26 -21 36 -21 525 -21 486 0 500 1 519 20 25 25 26 72 2 93 -17 15 -68 17 -525 17 -494 0 -508 -1 -527 -20z" style="fill: white;"/>
                        <path d="M1545 955 c-14 -13 -25 -36 -25 -50 0 -17 -7 -28 -22 -34 -30 -11 -78 -60 -89 -89 -14 -36 -11 -110 6 -141 23 -45 67 -76 150 -106 84 -31 102 -50 79 -83 -27 -39 -114 -18 -114 28 0 19 -44 60 -65 60 -7 0 -25 -10 -40 -22 -24 -19 -27 -26 -22 -65 5 -51 41 -101 91 -128 28 -15 36 -26 36 -47 0 -35 32 -68 65 -68 33 0 65 33 65 68 0 21 8 31 37 47 98 53 117 178 40 261 -22 23 -60 45 -117 67 -80 31 -85 35 -85 62 0 23 6 32 27 39 38 13 74 -6 92 -49 19 -44 56 -63 91 -47 69 32 31 163 -60 206 -28 14 -35 23 -35 46 0 35 -31 70 -60 70 -12 0 -32 -11 -45 -25z" style="fill: white;"/>
                    </g>
                </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    <!-- fim do card parcelamento nao previdenciario -->
    
    <!-- inicio do card parcelamento lei 12996 -->
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("parcelamento_lei_12996/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 16px;color: #24a0e9" class="span_hover">Parcelamento<br>Lei 12.996</span>
                
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("parcelamento_lei_12996/listar/EM_PARCELAMENTO"); ?>" style="font-size: 16px;" class="tag_a_hover">Em parcelamento: <?php 
                        if($parcelamento_lei_12996_qtd > 0){
                          echo $parcelamento_lei_12996_qtd_em_parcelamento;
                        } else{
                          echo "0";
                        } ?>
                    </a>
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("parcelamento_nao_previdenciario/listar/REJEITADA"); ?>" style="font-size: 14px;" class="tag_a_hover">Rejeitada na Consolidação: <?php 
                        if($parcelamento_nao_previdenciario_qtd_rejeitada){echo $parcelamento_nao_previdenciario_qtd_rejeitada;}else{
                          echo "0";
                        }  ?>
                    </a>
                  </div>
                </teste>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Parcelamento<br>Lei 12.996</span>
              <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Parcelamento Lei 12.996' data-original-title='' title=''><?php echo $parcelamento_lei_12996_qtd; ?></div></h4>
               <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="225.000000pt" height="225.000000pt" viewBox="0 0 225.000000 225.000000" preserveAspectRatio="xMidYMid meet">
                    <metadata>
                        Created by potrace 1.16, written by Peter Selinger 2001-2019
                    </metadata>
                    <g transform="translate(0.000000,225.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                        <path d="M92 2234 l-22 -15 0 -953 c0 -806 2 -955 14 -972 14 -18 34 -19 514 -24 l499 -5 35 -45 c45 -60 158 -144 235 -175 224 -89 473 -37 643 134 122 124 176 267 167 446 -6 123 -31 200 -99 301 -75 113 -184 193 -318 235 l-55 17 -2 518 c-3 515 -3 519 -24 537 -20 16 -73 17 -793 17 -682 0 -774 -2 -794 -16z m1478 -579 c0 -364 -3 -466 -12 -470 -7 -2 -37 -6 -65 -10 l-53 -6 0 130 c0 123 -1 131 -22 145 -20 14 -85 16 -518 16 -272 0 -505 -3 -518 -6 -50 -14 -52 -32 -52 -466 0 -349 2 -407 16 -426 15 -22 16 -22 334 -22 l319 0 11 -57 c7 -32 15 -63 17 -70 4 -10 -78 -13 -411 -13 l-416 0 0 860 0 860 685 0 685 0 0 -465z m-980 -395 l0 -70 -65 0 -65 0 0 70 0 70 65 0 65 0 0 -70z m725 0 l0 -65 -295 0 -295 0 -3 68 -3 67 298 -2 298 -3 0 -65z m-725 -260 l0 -70 -65 0 -65 0 0 65 0 64 63 4 c34 2 63 4 65 5 1 1 2 -29 2 -68z m627 58 c-1 -3 -28 -34 -60 -67 l-58 -61 -190 0 -189 0 0 63 c0 35 2 66 4 68 7 7 496 4 493 -3z m498 -20 c100 -25 205 -104 266 -199 141 -221 57 -536 -175 -653 -184 -93 -378 -67 -527 72 -353 326 -32 900 436 780z m-1123 -303 l3 -65 -67 0 -68 0 0 65 0 65 64 0 64 0 4 -65z m438 58 c-1 -5 -7 -33 -14 -63 l-12 -55 -142 -3 -142 -3 0 66 0 65 155 0 c85 0 155 -3 155 -7z" style="fill: white;"/>
                        <path d="M366 1975 c-32 -16 -43 -58 -22 -89 l16 -26 294 0 c271 0 294 1 309 18 21 23 22 66 1 86 -8 9 -18 16 -22 17 -87 6 -561 1 -576 -6z" style="fill: white;"/>
                        <path d="M350 1700 c-28 -28 -25 -64 6 -89 26 -21 36 -21 525 -21 486 0 500 1 519 20 25 25 26 72 2 93 -17 15 -68 17 -525 17 -494 0 -508 -1 -527 -20z" style="fill: white;"/>
                        <path d="M1545 955 c-14 -13 -25 -36 -25 -50 0 -17 -7 -28 -22 -34 -30 -11 -78 -60 -89 -89 -14 -36 -11 -110 6 -141 23 -45 67 -76 150 -106 84 -31 102 -50 79 -83 -27 -39 -114 -18 -114 28 0 19 -44 60 -65 60 -7 0 -25 -10 -40 -22 -24 -19 -27 -26 -22 -65 5 -51 41 -101 91 -128 28 -15 36 -26 36 -47 0 -35 32 -68 65 -68 33 0 65 33 65 68 0 21 8 31 37 47 98 53 117 178 40 261 -22 23 -60 45 -117 67 -80 31 -85 35 -85 62 0 23 6 32 27 39 38 13 74 -6 92 -49 19 -44 56 -63 91 -47 69 32 31 163 -60 206 -28 14 -35 23 -35 46 0 35 -31 70 -60 70 -12 0 -32 -11 -45 -25z" style="fill: white;"/>
                    </g>
                </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    <!-- fim do card parcelamento lei 12996 -->

      <?php
      $hostCompleto = $_SERVER['HTTP_HOST'];
      $server = explode('.', $hostCompleto);
      $server = $server[0];
      if($server == 'demo'){
      ?>
    <!-- inicio do card PERT RFB-->
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("parcelamento_pert_rfb/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 18px;color: #24a0e9" class="span_hover">Parcelamento PERT-RFB</span>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("parcelamento_pert_rfb/listar/ATIVO"); ?>" style="font-size: 16px;" class="tag_a_hover">Ativo: <?php 
                        if($parcelamento_pert_rfb_qtd_em_parcelamento){echo $parcelamento_pert_rfb_qtd_em_parcelamento;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("parcelamento_pert_rfb/listar/EXCLUIDO"); ?>" style="font-size: 16px;" class="tag_a_hover">Excluído: <?php 
                        if($parcelamento_pert_rfb_qtd_encerrado_recisao){echo $parcelamento_pert_rfb_qtd_encerrado_recisao;}else{
                          echo "0";
                        }  ?>
                      </a>  
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("parcelamento_pert_rfb/listar/CANCELADO"); ?>" style="font-size: 16px;" class="tag_a_hover">Cancelado: <?php 
                        if($parcelamento_pert_rfb_qtd_encerrado_contibuinte){echo $parcelamento_pert_rfb_qtd_encerrado_contibuinte;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </teste>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">PERT-RFB - Parcelamentos</span>
             <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Parcelamento no PERT-RFB' data-original-title='' title=''><?php echo $parcelamento_pert_rfb_qtd; ?></div></h4>
                <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="225.000000pt" height="225.000000pt" viewBox="0 0 225.000000 225.000000" preserveAspectRatio="xMidYMid meet">
                    <metadata>
                        Created by potrace 1.16, written by Peter Selinger 2001-2019
                    </metadata>
                    <g transform="translate(0.000000,225.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                        <path d="M92 2234 l-22 -15 0 -953 c0 -806 2 -955 14 -972 14 -18 34 -19 514 -24 l499 -5 35 -45 c45 -60 158 -144 235 -175 224 -89 473 -37 643 134 122 124 176 267 167 446 -6 123 -31 200 -99 301 -75 113 -184 193 -318 235 l-55 17 -2 518 c-3 515 -3 519 -24 537 -20 16 -73 17 -793 17 -682 0 -774 -2 -794 -16z m1478 -579 c0 -364 -3 -466 -12 -470 -7 -2 -37 -6 -65 -10 l-53 -6 0 130 c0 123 -1 131 -22 145 -20 14 -85 16 -518 16 -272 0 -505 -3 -518 -6 -50 -14 -52 -32 -52 -466 0 -349 2 -407 16 -426 15 -22 16 -22 334 -22 l319 0 11 -57 c7 -32 15 -63 17 -70 4 -10 -78 -13 -411 -13 l-416 0 0 860 0 860 685 0 685 0 0 -465z m-980 -395 l0 -70 -65 0 -65 0 0 70 0 70 65 0 65 0 0 -70z m725 0 l0 -65 -295 0 -295 0 -3 68 -3 67 298 -2 298 -3 0 -65z m-725 -260 l0 -70 -65 0 -65 0 0 65 0 64 63 4 c34 2 63 4 65 5 1 1 2 -29 2 -68z m627 58 c-1 -3 -28 -34 -60 -67 l-58 -61 -190 0 -189 0 0 63 c0 35 2 66 4 68 7 7 496 4 493 -3z m498 -20 c100 -25 205 -104 266 -199 141 -221 57 -536 -175 -653 -184 -93 -378 -67 -527 72 -353 326 -32 900 436 780z m-1123 -303 l3 -65 -67 0 -68 0 0 65 0 65 64 0 64 0 4 -65z m438 58 c-1 -5 -7 -33 -14 -63 l-12 -55 -142 -3 -142 -3 0 66 0 65 155 0 c85 0 155 -3 155 -7z" style="fill: white;"/>
                        <path d="M366 1975 c-32 -16 -43 -58 -22 -89 l16 -26 294 0 c271 0 294 1 309 18 21 23 22 66 1 86 -8 9 -18 16 -22 17 -87 6 -561 1 -576 -6z" style="fill: white;"/>
                        <path d="M350 1700 c-28 -28 -25 -64 6 -89 26 -21 36 -21 525 -21 486 0 500 1 519 20 25 25 26 72 2 93 -17 15 -68 17 -525 17 -494 0 -508 -1 -527 -20z" style="fill: white;"/>
                        <path d="M1545 955 c-14 -13 -25 -36 -25 -50 0 -17 -7 -28 -22 -34 -30 -11 -78 -60 -89 -89 -14 -36 -11 -110 6 -141 23 -45 67 -76 150 -106 84 -31 102 -50 79 -83 -27 -39 -114 -18 -114 28 0 19 -44 60 -65 60 -7 0 -25 -10 -40 -22 -24 -19 -27 -26 -22 -65 5 -51 41 -101 91 -128 28 -15 36 -26 36 -47 0 -35 32 -68 65 -68 33 0 65 33 65 68 0 21 8 31 37 47 98 53 117 178 40 261 -22 23 -60 45 -117 67 -80 31 -85 35 -85 62 0 23 6 32 27 39 38 13 74 -6 92 -49 19 -44 56 -63 91 -47 69 32 31 163 -60 206 -28 14 -35 23 -35 46 0 35 -31 70 -60 70 -12 0 -32 -11 -45 -25z" style="fill: white;"/>
                    </g>
                </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    <!-- fim do card PERT RFB-->

    <!-- inicio do card MEI -->
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("parcelamento_mei/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 18px;color: #24a0e9" class="span_hover">Parcelamento MEI</span>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("parcelamento_mei/listar/EM_PARCELAMENTO"); ?>" style="font-size: 16px;" class="tag_a_hover">Em parcelamento: <?php 
                        if($parcelamento_mei_qtd_em_parcelamento){echo $parcelamento_mei_qtd_em_parcelamento;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                      <a href="<?php echo base_url("parcelamento_mei/listar/CONCLUIDO"); ?>" style="font-size: 16px;" class="tag_a_hover">Concluído: <?php 
                        if($parcelamento_mei_qtd_concluido){echo $parcelamento_mei_qtd_concluido;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </teste>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">MEI - Parcelamentos</span>
             <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Parcelamento no MEI' data-original-title='' title=''><?php echo $parcelamento_mei_qtd; ?></div></h4>
                <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="225.000000pt" height="225.000000pt" viewBox="0 0 225.000000 225.000000" preserveAspectRatio="xMidYMid meet">
                    <metadata>
                        Created by potrace 1.16, written by Peter Selinger 2001-2019
                    </metadata>
                    <g transform="translate(0.000000,225.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                        <path d="M92 2234 l-22 -15 0 -953 c0 -806 2 -955 14 -972 14 -18 34 -19 514 -24 l499 -5 35 -45 c45 -60 158 -144 235 -175 224 -89 473 -37 643 134 122 124 176 267 167 446 -6 123 -31 200 -99 301 -75 113 -184 193 -318 235 l-55 17 -2 518 c-3 515 -3 519 -24 537 -20 16 -73 17 -793 17 -682 0 -774 -2 -794 -16z m1478 -579 c0 -364 -3 -466 -12 -470 -7 -2 -37 -6 -65 -10 l-53 -6 0 130 c0 123 -1 131 -22 145 -20 14 -85 16 -518 16 -272 0 -505 -3 -518 -6 -50 -14 -52 -32 -52 -466 0 -349 2 -407 16 -426 15 -22 16 -22 334 -22 l319 0 11 -57 c7 -32 15 -63 17 -70 4 -10 -78 -13 -411 -13 l-416 0 0 860 0 860 685 0 685 0 0 -465z m-980 -395 l0 -70 -65 0 -65 0 0 70 0 70 65 0 65 0 0 -70z m725 0 l0 -65 -295 0 -295 0 -3 68 -3 67 298 -2 298 -3 0 -65z m-725 -260 l0 -70 -65 0 -65 0 0 65 0 64 63 4 c34 2 63 4 65 5 1 1 2 -29 2 -68z m627 58 c-1 -3 -28 -34 -60 -67 l-58 -61 -190 0 -189 0 0 63 c0 35 2 66 4 68 7 7 496 4 493 -3z m498 -20 c100 -25 205 -104 266 -199 141 -221 57 -536 -175 -653 -184 -93 -378 -67 -527 72 -353 326 -32 900 436 780z m-1123 -303 l3 -65 -67 0 -68 0 0 65 0 65 64 0 64 0 4 -65z m438 58 c-1 -5 -7 -33 -14 -63 l-12 -55 -142 -3 -142 -3 0 66 0 65 155 0 c85 0 155 -3 155 -7z" style="fill: white;"/>
                        <path d="M366 1975 c-32 -16 -43 -58 -22 -89 l16 -26 294 0 c271 0 294 1 309 18 21 23 22 66 1 86 -8 9 -18 16 -22 17 -87 6 -561 1 -576 -6z" style="fill: white;"/>
                        <path d="M350 1700 c-28 -28 -25 -64 6 -89 26 -21 36 -21 525 -21 486 0 500 1 519 20 25 25 26 72 2 93 -17 15 -68 17 -525 17 -494 0 -508 -1 -527 -20z" style="fill: white;"/>
                        <path d="M1545 955 c-14 -13 -25 -36 -25 -50 0 -17 -7 -28 -22 -34 -30 -11 -78 -60 -89 -89 -14 -36 -11 -110 6 -141 23 -45 67 -76 150 -106 84 -31 102 -50 79 -83 -27 -39 -114 -18 -114 28 0 19 -44 60 -65 60 -7 0 -25 -10 -40 -22 -24 -19 -27 -26 -22 -65 5 -51 41 -101 91 -128 28 -15 36 -26 36 -47 0 -35 32 -68 65 -68 33 0 65 33 65 68 0 21 8 31 37 47 98 53 117 178 40 261 -22 23 -60 45 -117 67 -80 31 -85 35 -85 62 0 23 6 32 27 39 38 13 74 -6 92 -49 19 -44 56 -63 91 -47 69 32 31 163 -60 206 -28 14 -35 23 -35 46 0 35 -31 70 -60 70 -12 0 -32 -11 -45 -25z" style="fill: white;"/>
                    </g>
                </svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    <!-- fim do card MEI -->

          <?php
      }
      ?>
    
  </div>


    </div>

  </div>
</div> 

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->

<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->


<script type="text/javascript">
  $(document).ready(function() {
    

      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>