<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">
    var global_url = '';

    function preencher_url(url) {
        global_url = url;
    }
</script>
<style type="text/css">
    .buttons-excel {
        width: 100px;
    }

    .dataTables_filter {
        float: right;
    }

    .modal-dialog {
        width: 1200px;
        max-width: 1300px;
    }

    .bg-green {
        background-color: #51b754
    }
</style>

<style type="text/css">
    .swal2-styled {
        background-color: #dc2727 !important;
    }

    .all {
        display: flex;
        perspective: 10px;
        transform: perspective(300px) rotateX(20deg);
        will-change: perspective;
        perspective-origin: center center;
        transition: all 0.5s ease-out;
        justify-content: center;
        transform-style: preserve-3d;
    }

    .all:hover {
        perspective: 1000px;
        transition: all 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);

        .text {
            opacity: 1;
        }

        &>div {
            opacity: 1;
            transition-delay: 0s;
        }

        .explainer {
            opacity: 0;
        }
    }

    .all:hover .lefter {
        opacity: 1;
        margin-right: 10px;
        perspective: 2000px;
        transition: left 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);

        .text {
            opacity: 1;
        }
    }

    .all:hover .left {
        opacity: 1;
        margin-right: 10px;
        perspective: 2000px;
        transition: left 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);

        .text {
            opacity: 1;
        }
    }

    .all:hover .right {
        opacity: 1;
        margin-left: 10px;
        perspective: 2000px;
        transition: right 0.5s ease-in;
        transform: perspective(10000px) rotateX(0deg);

        .text {
            opacity: 1;
        }
    }

    .left,
    .center,
    .right,
    .lefter,
    .righter {
        width: 25px;
        height: 25px;
        transform-style: preserve-3d;
        border-radius: 10px;
        border: 1px solid #fff;
        box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
        opacity: 0;
        transition: all .3s ease;
        transition-delay: 1s;
        position: relative;
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
        background-color: white;
        cursor: pointer;
        /*background-blend-mode: color-burn;*/


    }

    .center2 {
        width: 25px;
        height: 25px;
        transform-style: preserve-3d;
        /*border-radius: 8px;*/
        border: 1px solid #fff;
        box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
        opacity: 0;
        transition: all .3s ease;
        transition-delay: 1s;
        position: relative;
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
        background-color: white;
        cursor: pointer;
        /*background-blend-mode: color-burn;*/


    }

    .text {
        transform: translateY(30px);
        opacity: 0;
        transition: all .3s ease;
        bottom: 0;
        left: 5px;
        position: absolute;
        will-change: transform;
        color: #fff;
        text-shadow: 0 0 5px rgba(100, 100, 255, .6)
    }

    .lefter {
        transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
        background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
    }

    .left {
        transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
        background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
    }

    .center2 {
        opacity: 1;
        background-image: url(<?php echo base_url('assets/css/fab/images/barcode.png'); ?>);
    }

    .center {
        opacity: 1;
        background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
    }

    .right {
        transform: translateX(30px) translateZ(-25px) rotateY(5deg);
        background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
    }

    .righter {
        transform: translateX(60px) translateZ(-50px) rotateY(10deg);
        background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
    }

    .explainer {
        font-weight: 300;
        font-size: 2rem;
        color: #fff;
        transition: all .6s ease;
        width: 100%;
        height: 100%;
        background-color: transparent;
        background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
        border-radius: 10px;
        text-shadow: 0 0 10px rgba(255, 255, 255, .8);

        display: flex;
        justify-content: center;
        align-items: center;
    }


    .ref {
        background-color: #000;
        background-image: linear-gradient(to bottom, #d80, #c00);
        border-radius: 3px;
        padding: 7px 10px;
        position: absolute;
        font-size: 16px;
        bottom: 10px;
        right: 10px;
        color: #fff;
        text-decoration: none;
        text-shadow: 0 0 3px rgba(0, 0, 0, .4);

        &::first-letter {
            font-size: 12px;
        }
    }
</style>

<style type="text/css">
    /* Tabelas**/
    table {
        border-collapse: collapse;
    }

    .dataGrid {
        border: 1px solid #c0d8e8;
        text-align: left;
    }

    .dataGrid caption {
        color: #f7941f;
        text-transform: uppercase;
    }

    /*.dataGrid th {background:#e1edf5;border:1px solid #c0d8e8;color:#446aa6;font-weight:bold;text-align:center;padding:3px 5px; }*/
    .dataGrid td {
        border-left: 1px solid #c0d8e8;
        border-right: 1px solid #c0d8e8;
        padding: 3px 10px;
    }

    .dataGrid td.pag {
        vertical-align: top;
    }

    .dataGrid .odd {
        background: #fff;
    }

    .dataGrid .even {
        background: #f1f6fa;
    }

    .dataGrid tfoot {
        background: #c0d7e8;
    }

    .dataGrid tfoot td {
        border: 1px solid #96bad6;
        *padding: 0 !important;
    }

    .dataGrid tbody {
        border-bottom: 1px solid #C0D8E8;
    }

    .dataGrid th {
        background-color: #E1E2D5;
        color: #0A4C62;
        padding: 2px 5px 2px 5px;
        width: auto;
        font-weight: bold;
        text-align: center;
        padding: 2px;
    }

    .dataGrid td {
        color: #0A4C62;
    }

    /*******************************************************************/
    /*******************************************************************/
    .app .bd {
        width: 670px;
    }

    .app .hd {
        _text-align: left;
        border-bottom: 1px solid #c0d8e8;
        background: #f1f6fa;
    }

    .app .hd h1 {
        margin-bottom: 0;
        padding-left: 34px;
        height: 20px;
        line-height: 68px;
        _padding-top: 20px;
        _height: 28px;
        _line-height: 28px;
    }

    .context-menu {
        float: right;
        margin: 5px 20px 0 0;
        *margin: 5px 20px 0 0;
        _margin: 18px 10px 0 0;
        *line-height: 2;
    }

    .context-menu .cntBack {
        padding: 0 8px 0 16px;
        background: url(images/icons/arrows/duasSetasEsqOff.png) no-repeat;
    }

    .context-menu .cntBack:hover {
        background: url(images/icons/arrows/duasSetasEsqOn.png) no-repeat;
    }

    .context-menu .cntClose {
        padding: 0 8px 0 16px;
        background: url(images/icons/arrows/fecharOff.png) no-repeat;
    }

    .context-menu .cntClose:hover {
        background: url(images/icons/arrows/fecharOn.png) no-repeat;
    }

    fieldset {
        color: #000;
        position: relative;
        padding-top: 20px;
        background-color: #F3F1E5;
    }

    /*legend {position:absolute;top:5px;left:18px;}​*/

    .quadro_botoes {
        background-color: #6D9928;
        border: solid 1px #8DC16E;
        color: white;
        font-size: 12px;
        padding: 2px 9px;
        cursor: pointer;
        font-weight: bold;
    }

    #Conteudo input[type="submit"][disabled] {
        /*background-color: #f0f0f0;*/
        color: #a9a9a9;
        cursor: default;
        border: thin groove;
    }

    strong,
    b,
    strong *,
    b * {
        font-weight: bold !important;
    }

    #loadingPagar {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0px;
        left: 1px;
        background-color: White;
        z-index: 10000;
        text-align: center;
        opacity: 0.70;
        -moz-opacity: 0.70;
        filter: alpha(opacity=70);
    }

    #mensagemPagar {
        width: 100%;
        text-align: center;
        position: absolute;
        top: 50%;
        left: 1px;
    }
</style>

<div class="page-body">
    <div class="container-fluid">
        <br>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h2 class="font-strong mb-4">Parcelamento PERT-RFB</h2>

                        <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="excel()">EXCEL</button>
                        <button class="btn btn-primary buttons-html5" style="border-radius: 20px;" onclick="imprimir()">PDF</button>
                    </div>
                    <div class="card-body" id="card-body-impressao">

                        <h5 style="text-align: center;">Detalhes do Parcelamento</h5>

                        <div class="line">
                            <fieldset>
                                <label>Identificação</label>

                                <div>
                                    <table class="dataGrid" cellspacing="0" rules="all" border="1" id="ctl00_contentPlaceH_wcParc_gdv" style="width:100%;border-collapse:collapse;">
                                        <tbody>
                                            <tr>
                                                <th scope="col">Razão Social</th>
                                                <th scope="col">CNPJ</th>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $parcelamento->razao_social; ?></td>
                                                <td style="text-align: center;"><?php echo $parcelamento->cnpj; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br>
                                <div>
                                    <table class="dataGrid" cellspacing="0" rules="all" border="1" id="ctl00_contentPlaceH_wcParc_gdv" style="width:100%;border-collapse:collapse;">
                                        <tbody>
                                            <tr>
                                                <th scope="col">Modalidade</th>
                                                <th scope="col">Parcelamento</th>
                                                <th scope="col">Data da Consolidadeção</th>
                                                <th scope="col">Situação</th>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $parcelamento->nome_modalidade; ?></td>
                                                <td style="text-align: center;"><?php echo $parcelamento->id_formatado; ?></td>
                                                <td style="text-align: center;"><?php echo $parcelamento->data_consolidacao; ?></td>
                                                <td style="text-align: center;"><?php echo $parcelamento->situacao; ?></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>

                        <br>

                        <div class="line">
                            <fieldset>
                            <label>Demonstrativo de Pagamentos</label>

                                <div>
                                    <table class="dataGrid" cellspacing="0" rules="all" border="1" id="ctl00_contentPlaceH_wcParc_gdv" style="width:100%;border-collapse:collapse;">
                                        <tbody>
                                            <tr>
                                                <th scope="col">Data da Arrecadação</th>
                                                <th scope="col">Valor Arrecadado (BRL)</th>
                                                <th scope="col">Dívida Amortizada (BRL)</th>
                                                <th scope="col">Juros Amortizado (BRL)</th>
                                                <th scope="col">Documento</th>
                                            </tr>
                                            <?php foreach ($pagamentos as $pagamento) { ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $pagamento->data_arrecadacao; ?></td>
                                                    <td style="text-align: center;"><?php echo $pagamento->valor_arrecadado; ?></td>
                                                    <td style="text-align: center;"><?php echo $pagamento->principal_utilizado; ?></td>
                                                    <td style="text-align: center;"><?php echo $pagamento->juros_utilizado; ?></td>
                                                    <td style="text-align: center;"><?php echo $pagamento->numero_documento; ?></td>
                                                </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>

                        <br>

                        <div class="line">
                            <fieldset>
                            <label>Demonstrativo das Parcelas</label>

                                <div>
                                    <table class="dataGrid" cellspacing="0" rules="all" border="1" id="ctl00_contentPlaceH_wcParc_gdv" style="width:100%;border-collapse:collapse;">
                                        <tbody>
                                            <tr>
                                                <th scope="col">Parcela</th>
                                                <th scope="col">vencimento</th>
                                                <th scope="col">Valor Originário (BRL)</th>
                                                <th scope="col">Saldo Atualizado (BRL)</th>
                                                <th scope="col">Situação</th>
                                            </tr>
                                            <?php foreach ($parcelas as $parcela) { ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $parcela->numero_parcela; ?></td>
                                                    <td style="text-align: center;"><?php echo $parcela->data_vencimento; ?></td>
                                                    <td style="text-align: center;"><?php echo $parcela->valor_originario; ?></td>
                                                    <td style="text-align: center;"><?php echo $parcela->saldo_atualizado; ?></td>
                                                    <td style="text-align: center;"><?php echo $parcela->situacao; ?></td>
                                                </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>


<div class="modal fade" id="modalDebitos" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="line">


                    <fieldset>
                        <label>Consolidação original</label>

                        <div id="ctl00_contentPlaceH_wcConsol_dvPrimeiraParcela" style="margin-top:5px; font-size:small">
                            <div><label>Informações válidas a partir da publicação da Instrução Normativa RFB nº 1.981/2020, de 13/10/2020:</label></div>
                            <div><label>Débito com histórico de inclusão em apenas um parcelamento anterior condiciona o valor da primeira parcela a 10% (dez por cento) do valor total da dívida consolidada.</label></div>
                            <div style="margin-top:5px"><label>Débito com histórico de inclusão em mais de um um parcelamento anterior condiciona o valor da primeira parcela a 20% (vinte por cento) do valor total da dívida consolidada.</label></div>
                        </div>
                        <div id="ctl00_contentPlaceH_wcConsol_pnlWcDeb">

                            <br>
                            <div>
                                <table class="dataGrid" cellspacing="0" rules="all" border="1" id="ctl00_contentPlaceH_wcConsol_wcListaDeb_gdv" style="width:100%;border-collapse:collapse;">

                                    <tbody>
                                        <tr>
                                            <th scope="col">Período de Apuração</th>
                                            <th scope="col">Vencimento</th>
                                            <th scope="col">Número do Processo</th>
                                            <th scope="col">Saldo Devedor Original</th>
                                            <th scope="col">Valor Atualizado</th>
                                        </tr>

                                        <?php if ($consulta) {
                                            foreach ($consulta as $e) { ?>
                                                <tr>
                                                    <td align="center" style="text-align: center;">
                                                        <?php echo $e->periodo_apuracao; ?>
                                                    </td>
                                                    <td style="text-align: center;"><?php echo $e->vencimento; ?></td>
                                                    <td style="text-align: center;">
                                                        <?php echo $e->numero_processo; ?>
                                                    </td>
                                                    <td style="text-align: center;"><?php echo $e->saldo_devedor_original; ?></td>
                                                    <td style="text-align: center;"><?php echo $e->valor_atualizado; ?></td>
                                                </tr>
                                        <?php }
                                        }
                                        ?>

                                    </tbody>

                                </table>
                            </div>

                        </div>
                    </fieldset>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<style type="text/css">
    .modal-dialog {
        width: 1200px;
        max-width: 1300px;
    }
</style>

<script type="text/javascript">
    var global_url = '';

    function preencher_url(url) {
        global_url = url;
    }

    function openModalDebitos() {
        $('#modalDebitos').modal();
    }
</script>


<script>
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover({
            boundary: 'window',
            html: true
        });

        $('#filtro').change(function() {
            var filtro = $('#filtro option:selected').val();
            window.location.replace("<?php echo base_url('limite_simples/listar'); ?>/" + filtro + "");
        });

        var table = $('#example').DataTable({
            "iDisplayLength": 50,
            lengthChange: false,
            //buttons: ['excel', 'pdf', 'colvis' ],
            buttons: [{
                    extend: 'excelHtml5',
                    text: 'EXCEL',
                    exportOptions: {
                        modifier: {
                            page: 'all'
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
                {
                    extend: 'colvis',
                    text: 'COLUNAS'
                }
            ],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "bDestroy": true
        });

        table.search('<?php echo $razao_social_filtro; ?>').draw();
        table.buttons().container()
            .appendTo('#example_wrapper .col-md-6:eq(0)');
    });
</script>

<script type="text/javascript">
    toastr.options = {
        "closeButton": true,
        "preventDuplicates": true,
        "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
        toastr["success"]("A empresa foi cadastrada com sucesso!", "Cadastrada!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("As informações da empresa foram editadas com sucesso!", "Editada!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["success"]("A empresa foi excluída com sucesso.", "Excluída!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["warning"]("Ocorreu um erro, tente novamente!", "Erro!");
    <?php } ?>


    function imprimir() {
        $("#card-body-impressao").printThis();
    }

    function excel() {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#card-body-impressao').html()));
    }
</script>