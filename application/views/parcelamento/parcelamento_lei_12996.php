<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">
  var global_url = '';

  function preencher_url(url) {
    global_url = url;
  }
</script>
<style type="text/css">
  .buttons-excel {
    width: 100px;
  }

  .dataTables_filter {
    float: right;
  }

  .modal-dialog {
    width: 1200px;
    max-width: 1300px;
  }

  .bg-green {
    background-color: #51b754
  }
</style>

<style type="text/css">
  .swal2-styled {
    background-color: #dc2727 !important;
  }

  .all {
    display: flex;
    perspective: 10px;
    transform: perspective(300px) rotateX(20deg);
    will-change: perspective;
    perspective-origin: center center;
    transition: all 0.5s ease-out;
    justify-content: center;
    transform-style: preserve-3d;
  }

  .all:hover {
    perspective: 1000px;
    transition: all 0.5s ease-in;
    transform: perspective(10000px) rotateX(0deg);

    .text {
      opacity: 1;
    }

    &>div {
      opacity: 1;
      transition-delay: 0s;
    }

    .explainer {
      opacity: 0;
    }
  }

  .all:hover .lefter {
    opacity: 1;
    margin-right: 10px;
    perspective: 2000px;
    transition: left 0.5s ease-in;
    transform: perspective(10000px) rotateX(0deg);

    .text {
      opacity: 1;
    }
  }

  .all:hover .left {
    opacity: 1;
    margin-right: 10px;
    perspective: 2000px;
    transition: left 0.5s ease-in;
    transform: perspective(10000px) rotateX(0deg);

    .text {
      opacity: 1;
    }
  }

  .all:hover .right {
    opacity: 1;
    margin-left: 10px;
    perspective: 2000px;
    transition: right 0.5s ease-in;
    transform: perspective(10000px) rotateX(0deg);

    .text {
      opacity: 1;
    }
  }

  .left,
  .center,
  .right,
  .lefter,
  .righter {
    width: 25px;
    height: 25px;
    transform-style: preserve-3d;
    border-radius: 10px;
    border: 1px solid #fff;
    box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
    opacity: 0;
    transition: all .3s ease;
    transition-delay: 1s;
    position: relative;
    background-position: center center;
    background-size: contain;
    background-repeat: no-repeat;
    background-color: white;
    cursor: pointer;
    /*background-blend-mode: color-burn;*/


  }

  .center2 {
    width: 25px;
    height: 25px;
    transform-style: preserve-3d;
    /*border-radius: 8px;*/
    border: 1px solid #fff;
    box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
    opacity: 0;
    transition: all .3s ease;
    transition-delay: 1s;
    position: relative;
    background-position: center center;
    background-size: contain;
    background-repeat: no-repeat;
    background-color: white;
    cursor: pointer;
    /*background-blend-mode: color-burn;*/


  }

  .text {
    transform: translateY(30px);
    opacity: 0;
    transition: all .3s ease;
    bottom: 0;
    left: 5px;
    position: absolute;
    will-change: transform;
    color: #fff;
    text-shadow: 0 0 5px rgba(100, 100, 255, .6)
  }

  .lefter {
    transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
    background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
  }

  .left {
    transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
    background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
  }

  .center2 {
    opacity: 1;
    background-image: url(<?php echo base_url('assets/css/fab/images/barcode.png'); ?>);
  }

  .center {
    opacity: 1;
    background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
  }

  .right {
    transform: translateX(30px) translateZ(-25px) rotateY(5deg);
    background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
  }

  .righter {
    transform: translateX(60px) translateZ(-50px) rotateY(10deg);
    background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
  }

  .explainer {
    font-weight: 300;
    font-size: 2rem;
    color: #fff;
    transition: all .6s ease;
    width: 100%;
    height: 100%;
    background-color: transparent;
    background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
    border-radius: 10px;
    text-shadow: 0 0 10px rgba(255, 255, 255, .8);

    display: flex;
    justify-content: center;
    align-items: center;
  }


  .ref {
    background-color: #000;
    background-image: linear-gradient(to bottom, #d80, #c00);
    border-radius: 3px;
    padding: 7px 10px;
    position: absolute;
    font-size: 16px;
    bottom: 10px;
    right: 10px;
    color: #fff;
    text-decoration: none;
    text-shadow: 0 0 3px rgba(0, 0, 0, .4);

    &::first-letter {
      font-size: 12px;
    }
  }
</style>



<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Parcelamento Lei 12.996</h2>
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label id="passo20">Filtro:</label>
              <?php
              $valores_filtro = array(
                'TODAS' => 'Todas',
                'EM_PARCELAMENTO' => 'Em Parcelamento',
                'REJEITADA' => 'Rejeitada na Consolidação'
              );
              echo form_dropdown(array('class' => 'form-control', 'id' => 'filtro'), $valores_filtro, $filtro);
              ?>
            </div>

            <div class="table-responsive">
              <table class="table table-striped  text-center" id="example">
                <thead>
                  <tr>
                    <th>Razão Social</th>
                    <th>CNPJ</th>
                    <th>Número</th>
                    <th>Situação <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone de Parcelamentos localizado na parte superior da tela, sempre que alterada a situação do parcelamento em questão"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                    <th>Parcela</th>
                    <th>Data de Vencimento</th>
                    <th>Gerar Parcela</th>
                    <th>Pago <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone de Parcelamentos localizado na parte superior da tela, sempre que alterado o status do pagamento da parcela entre PAGO e NÃO PAGO "><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                    <th>Qtd de parcelas</th>
                    <th>Parcelamentos em Atraso <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone de Parcelamentos localizado na parte superior da tela sempre que houver um parcelamento em atraso do mês anterior. Constando SIM na coluna, é possível clicar sobre 'SIM' relacionando os parcelamentos em atrasos e gerar a parcela"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                    <th>Extrato do Parcelamento</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($dividas_consolidadas) {
                    $index = 0;
                    foreach ($dividas_consolidadas as $divida_consolidada) { ?>
                      <tr>
                        <td><?php echo $divida_consolidada->razao_social; ?></td>
                        <td><?php echo $divida_consolidada->cnpj; ?></td>
                        <td><?php echo $divida_consolidada->cod_modalidade; ?></td>
                        <td>
                          <?php if ($divida_consolidada->nome_situacao == "Em Parcelamento") { ?>
                            <?php echo '<span class="badge badge-success">' . $divida_consolidada->nome_situacao . '</i></span>' ?>
                          <?php } elseif ($divida_consolidada->nome_situacao != "Rejeitada na Consolidação") { ?>
                            <?php echo '<span class="badge badge-danger">' . $divida_consolidada->nome_situacao . '</span>' ?>
                          <?php } else { ?>
                            <?php echo '<span class="badge badge-info" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Pago" data-content="Todas as parcelas foram pagas">' . $divida_consolidada->nome_situacao . '</span>' ?>
                          <?php } ?>
                        </td>
                        <td><?php echo intval($divida_consolidada->parcela_atual->parcela_id) . '°'; ?></td>
                        <td> <?php echo $divida_consolidada->parcela_atual->vencimento; ?>
                        </td>
                        <td>
                          <?php if (is_null($divida_consolidada->parcela_atual->path_download_parcela)) { ?>
                            <div class="all">
                              <div class="center2">
                                <?php echo '<a href="javascript:void(0)" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Gerar Parcela" onclick="gerar_parcela(&quot;' . $divida_consolidada->parcela_atual->id . '&quot;, &quot;' . $divida_consolidada->parcela_atual->vencimento . '&quot;, &quot;' . $divida_consolidada->razao_social . '&quot;)" ><div class="explainer"></div><div class="text"></div></a>'; ?>
                              </div>
                            </div>
                          <?php } else { ?>
                            <div class="all">

                              <div class="lefter">
                                <a href="mailto:?subject=Notificação Sistema VERI&body=A parcela <?= $divida_consolidada->parcela_atual->vencimento ?> da empresa <?= $divida_consolidada->razao_social ?> está disponível no link <?= $divida_consolidada->parcela_atual->path_download_parcela ?> " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                  <div class="explainer"></div>
                                  <div class="text"></div>
                                </a>
                              </div>

                              <div class="left">
                                <a href="https://api.whatsapp.com/send?text=A parcela <?= $divida_consolidada->parcela_atual->vencimento ?> da empresa <?= $divida_consolidada->razao_social ?> está disponível no link <?= $divida_consolidada->parcela_atual->path_download_parcela ?> " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                  <div class="explainer"></div>
                                  <div class="text">Whatsapp</div>
                                </a>
                              </div>

                              <div class="center">
                                <a href="<?= $divida_consolidada->parcela_atual->path_download_parcela ?>" download data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                  <div class="explainer"></div>
                                  <div class="text"></div>
                                </a>
                              </div>

                              <div class="right">
                                <a href="<?= $divida_consolidada->parcela_atual->path_download_parcela ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                  <div class="explainer"></div>
                                  <div class="text"></div>
                                </a>
                              </div>

                            </div>
                          <?php } ?>
                        </td>
                        <td>
                          <?php if ($divida_consolidada->parcela_atual->indicador_situacao_parcela == 'P') {
                            echo '<span class="badge badge-success"> Sim </span>';
                          } else {
                            echo '<span class="badge badge-danger"> Não </span>';
                          }
                          ?>
                        </td>
                        <td><?php echo $divida_consolidada->qtd_parcelas_a_vencer; ?></td>
                        <td>
                          <?php if ($divida_consolidada->parcelas_atrasadas > 0) {
                            echo '<div style="display: none;"> Sim </div> <a href="javascript:void(0)" style="color:white" class="badge badge-danger" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Clique para ver" onclick="abrir_modal_parcelas_nao_paga(&quot;' . $divida_consolidada->id . '&quot;, &quot;' . $divida_consolidada->razao_social . '&quot;)"> Sim </i></a>';
                          } else {
                            echo '<span class="badge badge-success"> Não </span>';
                          } ?>
                        </td>
                        <td>
                          <div style="display: none;"><?php echo base_url('parcelamento_lei_12996/listar_detalhes/' . $divida_consolidada->id); ?></div>
                          <a href="<?php echo base_url('parcelamento_lei_12996/listar_detalhes/' . $divida_consolidada->id); ?>" target="_blank" class='badge badge-info badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Clique para ver detalhes do parcelamento'>Detalhes</a>
                        </td>

                      </tr>
                  <?php $index++;
                    }
                  } ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<div class="modal fade" id="modalDebitos" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


        <div class="panel panel-success">
          <div class="panel-heading title-menu">
            <h4 class="panel-title">
              <strong id="title_modal"></strong>
            </h4>
          </div>
          <div>
            <table class="table table-striped  text-center">
              <tbody id="body_modal">


              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
  </div>

</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<style type="text/css">
  .modal-dialog {
    width: 1200px;
    max-width: 1300px;
  }
</style>

<script type="text/javascript">
  var global_url = '';

  function preencher_url(url) {
    global_url = url;
  }
</script>


<script>
  $(document).ready(function() {
    $('[data-toggle="popover"]').popover({
      boundary: 'window',
      html: true
    });

    $('#filtro').change(function() {
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('parcelamento_lei_12996/listar'); ?>/" + filtro + "");
    });

    var table = $('#example').DataTable({
      "iDisplayLength": 50,
      lengthChange: false,
      //buttons: ['excel', 'pdf', 'colvis' ],
      buttons: [{
          extend: 'excelHtml5',
          text: 'EXCEL',
          exportOptions: {
            modifier: {
              page: 'all'
            }
          }
        },
        {
          extend: 'pdfHtml5',
          orientation: 'landscape',
          pageSize: 'LEGAL'
        },
        {
          extend: 'csvHtml5',
          text: 'CSV',
          exportOptions: {
            modifier: {
              search: 'none'
            }
          }
        },
        {
          extend: 'colvis',
          text: 'COLUNAS'
        }
      ],
      "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
          "sNext": "Próximo",
          "sPrevious": "Anterior",
          "sFirst": "Primeiro",
          "sLast": "Último"
        },
        "oAria": {
          "sSortAscending": ": Ordenar colunas de forma ascendente",
          "sSortDescending": ": Ordenar colunas de forma descendente"
        }
      },
      "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
      .appendTo('#example_wrapper .col-md-6:eq(0)');
  });
</script>

<script type="text/javascript">
  toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
  }

  <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("A empresa foi cadastrada com sucesso!", "Cadastrada!");
  <?php } ?>

  <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
    toastr["info"]("As informações da empresa foram editadas com sucesso!", "Editada!");
  <?php } ?>

  <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
    toastr["success"]("A empresa foi excluída com sucesso.", "Excluída!");
  <?php } ?>

  <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
    toastr["warning"]("Ocorreu um erro, tente novamente!", "Erro!");
  <?php } ?>
</script>

<script type="text/javascript">
  function abrir_modal_parcelas_nao_paga(id_divida_consolidada, razao_social) {
    $("#body_modal").html('');
    $("#title_modal").html('Parcelas não pagas');

    $.ajax({
      type: "POST",
      url: '<?= base_url('parcelamento_lei_12996/buscar_parcelas_nao_pagas_for_modal'); ?>',
      async: true,
      data: {
        'id_divida_consolidada': id_divida_consolidada,
      },
      success: function(result) {
        var resultado = JSON.parse(result);

        if (resultado != '') {

          $("#body_modal").append('<tr><th>Data do Vencimento</th><th>Valor da Prestação</th><th>Pagamentos</th></tr>');

          resultado.forEach(parcela => {
            $("#body_modal").append('<tr>');
            $("#body_modal").append('<td  class="text-center">' + parcela.data_vencimento + '</td>');
            $("#body_modal").append('<td  class="text-center">' + parcela.valor_prestacao + '</td>');
            if (parcela.path_download_parcela != null) {
              $("#body_modal").append(
                `<td class="text-center">
                  <div class="all">

                    <div class="lefter">
                      <a href="mailto:?subject=Notificação Sistema VERI&body=A parcela ${parcela.data_vencimento} da empresa ${razao_social} está disponível no link ${parcela.path_download_parcela} " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                        <div class="explainer"></div>
                        <div class="text"></div>
                      </a>
                    </div>

                    <div class="left">
                      <a href="https://api.whatsapp.com/send?text=A parcela ${parcela.data_vencimento} da empresa ${razao_social} está disponível no link ${parcela.path_download_parcela} " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                        <div class="explainer"></div>
                        <div class="text">Whatsapp</div>
                      </a>
                    </div>

                    <div class="center">
                      <a href="${parcela.path_download_parcela}" download data-toggle="tooltip" data-placement="top" title="Imprimir" class="">
                        <div class="explainer"></div>
                        <div class="text"></div>
                      </a>
                    </div>

                    <div class="right">
                      <a href="${parcela.path_download_parcela}" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                        <div class="explainer"></div>
                        <div class="text"></div>
                      </a>
                    </div>

                  </div>
                </td>`
              );
            } else {
              $("#body_modal").append(`
                    <td class="text-center">
                      <div class="all">
                        <div class="center2">
                          <a href="javascript:void(0);" onclick="gerar_parcela_nao_paga(&quot;${parcela.id}&quot;, &quot;${parcela.data_vencimento}&quot;, &quot;${razao_social}&quot;, &quot;${parcela.id_divida_consolidada}&quot;)" data-trigger="hover" data-placement="top" title="Gerar Boleto" class="">
                            <div class="explainer"></div>
                            <div class="text"></div>
                          </a>
                        </div>
                      </div>
                    </td>
                  `);


            }
            $("#body_modal").append('</tr>');
          });

          for (var i in resultado) {

          }
        }
      },
    });

    $('#modalDebitos').modal();
  }

  function gerar_parcela(id_parcela, data_vencimento, razao_social) {
    swal({
      title: `Gerar parcela?`,
      text: `Deseja gerar a parcela ${data_vencimento} da empresa ${razao_social}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Sim!',
      cancelButtonText: 'Não, cancelar!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then(function() {
      gerar(id_parcela);
    }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelado',
          'Geração da Parcela cancelada!',
          'error'
        ).done();
      }
    }).done();
  }

  function gerar(id_parcela) {
    swal({
      title: "Gerando...",
      text: 'Gerando Parcela.\nPor favor, aguardar alguns segundos...',
      type: "info",
      confirmButtonText: "Cancelar",
      confirmButtonColor: "#fff"
    }, function(isConfirm) {
      if (isConfirm) {
        request.abort();
      }
    });

    //var banco = <?= "'" . $banco . "'"; ?>;
    var banco = 'mtzcontabilidade';

    //url = "https://veri-sp.com.br/crons-api/Simplesnacional_emitir_parcela_geral/emitir_function/";
    url = '<?= base_url('crons-api/parcelamento_lei_12996_emitir_darf/emitir_function/') ?>'

    var request = $.post(url, {
      banco: banco,
      id_parcela: id_parcela,
    }, function(data, status) {

      if (data.includes("ERROECAC")) {
        $(document).ready(function() {
          swal({
            title: "Ocorreu um erro ao gerar a parcela devido a uma instabilidade no Portal e-CAC. Por favor, tente novamente em alguns instantes.",
            text: "Erro",
            type: "error"
          });
        });
      } else if (data.includes("ERRO")) {

        $(document).ready(function() {
          swal({
            title: "Erro ao gerar a parcela !",
            text: "Erro",
            type: "error"
          });
        });
      } else {
        swal.close();
        window.open(data, '_blank');
        setTimeout(reload_page, 1000);
      }
    });
  }

  function reload_page() {
    window.location = global_url;
  }

  function gerar_parcela_nao_paga(id_parcela, data_vencimento, razao_social, divida_consolidada_id) {
    swal({
      title: `Gerar parcela?`,
      text: `Deseja gerar a parcela ${data_vencimento} da empresa ${razao_social}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Sim!',
      cancelButtonText: 'Não, cancelar!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then(function() {
      gerar_nao_paga(id_parcela, divida_consolidada_id, razao_social);
    }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelado',
          'Geração da Parcela cancelada!',
          'error'
        ).done();
      }
    }).done();
  }

  function gerar_nao_paga(id_parcela, divida_consolidada_id, razao_social) {
    swal({
      title: "Gerando...",
      text: 'Gerando Parcela.\nPor favor, aguardar alguns segundos...',
      type: "info",
      confirmButtonText: "Cancelar",
      confirmButtonColor: "#fff"
    }, function(isConfirm) {
      if (isConfirm) {
        request.abort();
      }
    });

    //var banco = <?= "'" . $banco . "'"; ?>;
    var banco = 'mtzcontabilidade';

    //url = "https://veri-sp.com.br/crons-api/Simplesnacional_emitir_parcela_geral/emitir_function/";
    url = '<?= base_url('crons-api/parcelamento_lei_12996_emitir_darf/emitir_function/') ?>'

    var request = $.post(url, {
      banco: banco,
      id_parcela: id_parcela,
    }, function(data, status) {

      if (data.includes("ERROECAC")) {
        $(document).ready(function() {
          swal({
            title: "Ocorreu um erro ao gerar a parcela devido a uma instabilidade no Portal e-CAC. Por favor, tente novamente em alguns instantes.",
            text: "Erro",
            type: "error"
          });
        });
      } else if (data.includes("ERRO")) {

        $(document).ready(function() {
          swal({
            title: "Erro ao gerar a parcela !",
            text: "Erro",
            type: "error"
          });
        });
      } else {
        swal.close();
        abrir_modal_parcelas_nao_paga(divida_consolidada_id, razao_social);
      }
    });
  }
</script>