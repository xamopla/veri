<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?> 
<!doctype html>
<html lang="en">

<head>
<title>VERI - Você no controle</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="VERI - Você no controle">
<meta name="author" content="DataByte Tecnologia LTDA">

<link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon.png'); ?>" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/login/assets/vendor/bootstrap/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/login/assets/vendor/font-awesome/css/font-awesome.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/login/assets/vendor/animate-css/vivify.min.css'); ?>">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/login/assets/css/site.min.css'); ?>">

</head>

<style type="text/css">
body {
background-image: url("<?php echo base_url('assets/login/assets/images/background-image.png'); ?>");
position: relative;
width: 100%;
height: 100%;
background-repeat: no-repeat;
 -webkit-background-size: cover;
-moz-background-size: cover;
background-size: cover;
-o-background-size: cover;
}
</style>

<body class="theme-orange" style="background-color: #59005f">
    <div class="pattern">
        <span class="red"></span>
        <span class="indigo"></span>
        <span class="blue"></span>
        <span class="green"></span>
        <span class="orange"></span>
    </div>
    <div class="auth-main particles_js">        
        <div class="auth_div vivify popIn"> 
            <div class="card">
                <div class="body" style="background-color: #f9f9f9">
                    <p class="lead"><img src="<?php echo base_url('assets/img/logos/logo.png'); ?>"><br><b>&nbsp;&nbsp;&nbsp;&nbsp; Você no Controle!</b></p><br> 
                    <p>Digite o seu email para solicitar uma nova senha.</p>

                    <form class="form-auth-small">
                        <div class="form-group">                                    
                            <input type="email" class="form-control round" id="signup-password" placeholder="Email">
                        </div>
                        <button type="submit" class="btn btn-round btn-primary btn-lg btn-block">SOLICITAR SENHA</button>
                        <div class="bottom">
                            <span class="helper-text">Lembrou a senha? <a href="<?php echo base_url("login"); ?>">Acessar</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="particles-js"></div>
    </div>
    <!-- END WRAPPER -->
    
<script src="<?php echo base_url('assets/login/assets/bundles/libscripts.bundle.js'); ?>"></script>    
<script src="<?php echo base_url('assets/login/assets/bundles/vendorscripts.bundle.js'); ?>"></script>
<script src="<?php echo base_url('assets/login/assets/bundles/mainscripts.bundle.js'); ?>"></script>
</body>
</html>
