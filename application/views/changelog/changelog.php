<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<style type="text/css">
.card {
    margin: 1rem 0 1rem 0;
}
.card {
    position: relative;
    margin: .5rem 0 1rem 0;
    -webkit-transition: box-shadow .25s;
    transition: box-shadow .25s;
    border-radius: 2px;
    background-color: #fff;
}
.z-depth-1, nav, .card-panel, .card, .toast, .btn, .btn-large, .btn-small, .btn-floating, .dropdown-content, .collapsible, .sidenav {
    box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 12%), 0 1px 5px 0 rgb(0 0 0 / 20%);
}
.card .card-content {
    padding: 24px;
    border-radius: 0 0 2px 2px;
}
.card .card-content .card-title {
    line-height: 32px;
    display: block;
    margin-bottom: 8px;
}
.card .card-title {
    font-size: 18px;
    font-weight: 400;
    margin: 0;
}
span.badge {
    color: #fff;
}
span.badge {
    font-size: 1rem;
    line-height: 22px;
    float: right;
    box-sizing: border-box;
    min-width: 3rem;
    height: 22px;
    margin-left: 14px;
    padding: 0 6px;
    text-align: center;
    color: #fff;
}
.light-blue {
    background-color: #03a9f4 !important;
}
.green {
    background-color: #4caf50 !important;
}
.purple {
    background-color: #9c27b0 !important;
}
.red {
    background-color: #f44336 !important;
}
.float-left {
    float: left !important;
}
.ml-0 {
    margin-left: 0 !important;
}
.mt-1 {
    margin-top: 1% !important;
}
.clear-both {
    clear: both;
}
ul.list-type-bullet {
    padding-left: 40px;
    list-style-type: inherit;
}
ul.list-type-bullet li {
    list-style-type: inherit;
}
</style>
<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 xl-50 box-col-12">
        <div class="card">
          <div class="card-header">
            <h5>Alterações Sistema VERI</h5>
          </div>
          <div class="card-body">
            <div class="col s12">
              <!-- v5.1 -->
              <div class="card">
                  <div class="card-content">
                      <h5 class="card-title">v 6.1.2 - 10 de Fevereiro, 2021</h5>
                      <span class="badge purple float-left ml-0 mt-1">Melhoria</span>
                      <div class="clear-both"></div><br>
                      <ul class="list-type-bullet">
                          <li>Campo de observação nas certidões negativas</li> 
                      </ul>
                  </div>
              </div>

              <div class="card">
                  <div class="card-content">
                      <h5 class="card-title">v 6.1.1 - 04 de Fevereiro, 2021</h5>
                      <span class="badge green float-left ml-0 mt-1">Novo</span>
                      <div class="clear-both"></div><br>
                      <ul class="list-type-bullet">
                          <li>Filtro de Empresas do Simples Nacional</li>
                          <li>Relação de Colaboradores vinculados a empresa no relatório do resumo fiscal</li>
                      </ul>
                  </div>
              </div>

              <!-- v5.0 -->
              <div class="card">
                  <div class="card-content">
                      <h5 class="card-title">v 6.1.0 - 03 de Fevereiro, 2021</h5>

                      <span class="badge green float-left ml-0 mt-1">Novo</span>
                      <div class="clear-both"></div><br>
                      <ul class="list-type-bullet">
                          <li>Certidão Negativa FGTS</li>
                          <li>Certidão Negativa Trabalhista</li> 
                      </ul>
                      <span class="badge purple float-left ml-0 mt-1">Melhoria</span>
                      <div class="clear-both"></div><br>
                      <ul class="list-type-bullet">
                          <li>Relatório de DMA e EFD</li>
                          <li>Card de O.S de Monitoramento e O.S de auditoria agora estão em 1 só</li>
                      </ul>
                      <span class="badge light-blue float-left ml-0 mt-1">Correções</span>
                      <div class="clear-both"></div><br>
                      <ul class="list-type-bullet">
                          <li>Contador dashboard dívida ativa</li>
                      </ul>
                      <span class="badge red float-left ml-0 mt-1">Removido</span>
                      <div class="clear-both"></div><br>
                      <ul class="list-type-bullet">
                          <li>Card O.S monitoramento</li>
                          <li>Card O.S auditoria</li> 
                      </ul>
                  </div>
              </div> 
          </div>
        </div>
      </div>
    </div> 
  </div>
</div>
<!-- Container-fluid Ends-->
</div> 

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS --> 