<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  
    var global_url_2 = '';  

    function preencher_url(url){
    global_url = url;
    }

    function preencher_url_2(url_2){
    global_url_2 = url_2;
    }
</script> 

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Consulta de Alvarás SSA</h5>
          </div>
          <div class="card-body">
            <div class="ibox">
                <div class="ibox-body">
                  <div class="form-group col-lg-6 col-xs-12 pull-right">
                      <label>Filtro:</label>
                      <?php 
                      $valores_filtro = array(
                          'listar'=>'Todas',
                          'vencidos'=>'Vencidos',
                          'vencidosPublicidade'=>'Alvarás de Publicidade Vencidos',
                          'vencidosFuncionamento'=>'Alvarás de Funcionamento Vencidos',
                          'ativaprovisoria'=>'Ativa Provisória',
                          'ativaregular'=>'Ativa Regular',
                          'ativatemporaria'=>'Ativa Temporária',
                          'suspensaRenovacao'=>'Suspensa por Falta de Renovação',
                          'suspensaRecadastramento'=>'Suspensa por Falta de Recadastramento',
                          'semCpfRepresentante'=>'CPF do Representante não Cadastrado'
                          
                      );
                      echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
                      ?>
                    </div><br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="example" data-toggle="datatables">
                            <thead class="thead-default thead-lg">
                                <tr>
                                  <th>Razão Social</th>
                                  <th>CNPJ</th>
                                  <th>Inscrição Municipal (CGA)</th>
                                  <th>Nº DO TVL</th>
                                  <th>Validade TVL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                  <th>Validade Alvará Publ.</th>
                                  <th>Validade Alvará Func.</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if ($consulta){ 
                              
                                foreach ($consulta as $c){
                            ?>
                                <tr>
                                  <td style="text-align: left;"><?php echo $c->razao_social; ?></td>
                                  <td><?php echo $c->cnpj; ?></td>
                                  <td><?php echo substr($c->inscricao_municipal,0,14); ?></td>
                                  <td><?php echo $c->numero_tvl; ?></td>
                                  <td><?php 
                                  if(trim($c->validade != "")){
                                    $var = $c->validade;
                                    $date = str_replace('/', '-', $var);
                                    $date = date('Y-m-d', strtotime($date));

                                    if (trim($c->validade) == "Definitivo" || strtotime($date) > strtotime(date("Y-m-d"))){
                                      echo "<span class='badge badge-success badge-shadow' style='font-size: 10px; background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' >$c->validade</span>";
                                    } else {
                                      echo "<span class='badge badge-danger badge-shadow' style='font-size: 10px' data-toggle='popover' data-trigger='hover' data-placement='top'>$c->validade</span>";
                                    }  
                                  }else{
                                    echo "<span class='badge badge-warning badge-shadow' style='font-size: 10px' data-toggle='tooltip' data-trigger='hover' title='Não Possui Data' data-placement='top'>Não Possui</span>";
                                  }
                                  
                                  ?>                  

                                  <a href="<?php echo base_url("alvara/download/$c->cnpj_sem_formatacao"); ?>" data-toggle="tooltip" data-placement="top" title="Download Alvará" class="btn btn-default b"><i class="fa fa-download" aria-hidden="true"></i></a> 
                                  </td>

                                  <td>
                                  <?php 
                                  if(trim($c->data_validade_publicidade != "")){
                                    $var = $c->data_validade_publicidade;
                                    $date = str_replace('/', '-', $var);
                                    $date = date('Y-m-d', strtotime($date));

                                    if (trim($c->data_validade_publicidade) == "Definitivo" || strtotime($date) > strtotime(date("Y-m-d"))){
                                      echo "<span class='badge badge-success badge-shadow' style='font-size: 10px; background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' >$c->data_validade_publicidade</span>";
                                    } else {
                                      echo "<span class='badge badge-danger badge-shadow' style='font-size: 10px' data-toggle='popover' data-trigger='hover' data-placement='top'>$c->data_validade_publicidade</span>";
                                    }  
                                  }else{
                                    echo "<span class='badge badge-warning badge-shadow' style='font-size: 10px' data-toggle='tooltip' data-trigger='hover' title='Não Possui Alvará' data-placement='top'>Não Possui</span>";
                                  }
                                  
                                  ?>                  
                                  <?php 
                                    $valorCGA = substr($c->inscricao_municipal,0,14); 
                                    $valorCGA = str_replace(".", "", $valorCGA);
                                    $valorCGA = str_replace("-", "", $valorCGA);
                                    $valorCGA = str_replace("/", "", $valorCGA);
                                  ?>
                                  <?php if(trim($c->data_validade_publicidade != "")){ ?>

                                  <a href="<?php echo base_url("alvara/download_alvara_publicidade/$valorCGA"); ?>" data-toggle="tooltip" data-placement="top" title="Download Alvará Publicidade" class="btn btn-default b"><i class="fa fa-download" aria-hidden="true"></i></a> 

                                  <?php } else { ?>

                                  <a disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Não existe alvará" class="btn btn-default b"><i class="fa fa-download" aria-hidden="true"></i></a>

                                  <?php } ?>
                                  </td>

                                  <td>
                                  <?php 
                                  if(trim($c->data_validade_funcionamento != "")){
                                    $var = $c->data_validade_funcionamento;
                                    $date = str_replace('/', '-', $var);
                                    $date = date('Y-m-d', strtotime($date));

                                     if (trim($c->data_validade_funcionamento) == "Definitivo" || strtotime($date) > strtotime(date("Y-m-d"))){
                                      echo "<span class='badge badge-success badge-shadow' style='font-size: 10px; background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' >$c->data_validade_funcionamento</span>";
                                    } else {
                                      echo "<span class='badge badge-danger badge-shadow' style='font-size: 10px' data-toggle='popover' data-trigger='hover' data-placement='top'>$c->data_validade_funcionamento</span>";
                                    }  
                                  }else{
                                    if (!$c->login_mei) {
                                     echo "<span class='badge badge-warning badge-shadow' style='font-size: 10px' data-toggle='tooltip' data-trigger='hover' title='O cpf do representante não foi cadastrado' data-placement='top'>Não Possui</span>";
                                    } else {  
                                      echo "<span class='badge badge-warning badge-shadow' style='font-size: 10px' data-toggle='tooltip' data-trigger='hover' title='Não Possui Alvará' data-placement='top'>Não Possui</span>";
                                    }
                                  }
                                  
                                  ?>                  

                                 <?php if(trim($c->data_validade_funcionamento != "")){ ?>

                                  <a href="<?php echo base_url("alvara/download_alvara_funcionamento/$c->cnpj_sem_formatacao/$valorCGA/$c->login_mei"); ?>" data-toggle="tooltip" data-placement="top" title="Download Alvará Funcionamento" class="btn btn-default b"><i class="fa fa-download" aria-hidden="true"></i></a> 

                                  <?php } else { ?>

                                  <a disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Não existe alvará" class="btn btn-default b"><i class="fa fa-download" aria-hidden="true"></i></a>

                                  <?php } ?>
                                  </td>
                                </tr>
                            <?php } 
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.5.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>

<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );

  $('#filtro').change(function(){
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('alvara'); ?>/"+filtro+"");
  });
});
</script>