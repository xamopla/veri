<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link href="<?php echo base_url("assets/css/aviso.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script>
  window.setTimeout("history.back(-2)", 16000);
  window.onload = setInterval(contador,1000);

  var tempo = 15;
  function contador(){

  if(document.getElementById("contador") != null){
    document.getElementById("contador").innerHTML = tempo;  
    tempo--;
  }  
}
</script> 

<body>
    <div class="error-content flexbox">
        <span class="error-icon"></span>
        <div class="flex-1">
            <h1 class="error-code"></h1>
            <h3 class="font-strong" style="font-size: 40px;">CÓDIGO E0001A</h3>
            <p class="mb-4">Por favor informe o código para nossa equipe e iremos resolver esse problema para você.</p>
            <div>
                você será redirecionado em <span id="contador" style="color: red;"></span> segundos.
                <br><a class="text-primary" href="<?php echo base_url('painel'); ?>">Ir para a tela inicial</a>
            </div>
        </div>
    </div>
</body>