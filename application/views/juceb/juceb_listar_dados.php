<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Consulta de dados JUCEB</h2> 
          </div>
          <div class="card-body">
          <div class="form-group col-lg-3 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'listar_dados'=>'Todas',
                  'registrosAtivos'=>'Registros Ativos',
                  'registrosCancelados'=>'Registros Cancelados',
                  'registrosExtintos'=>'Registros Extintos',
                  'registrosProxVencimento'=>'Próximos de Vencer',
                  'registrosVencidos'=>'Vencidos'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br><br>
            <div class="table-responsive">
              <table class="table table-striped" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr>
                        <th style="text-align: center;">Razão Social</th>
                        <th style="text-align: center;">CNPJ</th>
                        <th style="text-align: center;">Nire</th>
                        <th style="text-align: center;">Situação/Status</th>
                        <th style="text-align: center;">Capital Social</th>
                        <th style="text-align: center;">Data do Ato Constitutivo</th> 
                        <th style="text-align: center;">Data do Último Arquivamento</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php 
                  if ($consulta){ 
                    
                      foreach ($consulta as $c){
                  ?>
                      <tr>
                        <td style="text-align: left;"><?php echo $c->razao_social; ?></td>
                        <td><?php echo $c->cnpj_completo; ?></td>
                        <td><?php echo $c->juceb_nire; ?></td>
                        <td>
                          <p class="example-popover" data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Status" data-content="<?php echo $c->juceb_status; ?>"><?php echo rtrim( $c->juceb_situacao, '\\/' ); ?></p>
                        </td>
                        <td>
                          <p class="example-popover" data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Capital Integralizado" data-content="<?php echo $c->juceb_capital_integralizado; ?>"><?php echo $c->juceb_capital_social; ?></p>
                        </td>
                        <td style="text-align: center;">
                          <p class="example-popover" data-container='body' data-toggle='popover' data-placement='top' data-trigger="hover" title="Início das Atividades" data-content="<?php echo $c->juceb_data_inicio_das_atividades; ?>"><?php echo $c->juceb_data_do_ato_constitutivo; ?></p>
                        </td> 

                        <!-- VALIDAÇÃO -->
                        <?php
                          $df = new DateTime($c->juceb_data_evento_expirar);
                          $da = new DateTime(date(''));
                          $intervalo = $df->diff($da);
                        ?>

                        <?php if (strstr($c->cnpj_completo, '0002')): ?>
                          <td style="text-align: center;" >
                            <?php  
                            echo "<p class='example-popover' data-html='true' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Informações Gerais' data-content='<b>Nome do Evento:</b> $c->juceb_historico_nome_do_evento <br><br><b>Nº Arquivamento:</b> $c->juceb_historico_numero_arquivamento <br><br><b>Descrição:</b> $c->juceb_historico_descricao_ato'>$c->juceb_historico_data_ultimo_arquivamento</p>";
                          ?>
                          </td> 
                        <?php else: ?>

                        <?php if (strtotime($c->juceb_data_evento_expirar) < strtotime(date('Y-m-d'))){ ?>
                         <td style="text-align: center;background-color: #f71818;color: #ffffff;" >
                          <?php  
                            echo "<p class='example-popover' data-html='true' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Informações Gerais' data-content='<b>Nome do Evento:</b> $c->juceb_historico_nome_do_evento <br><br><b>Nº Arquivamento:</b> $c->juceb_historico_numero_arquivamento <br><br><b>Descrição:</b> $c->juceb_historico_descricao_ato'>$c->juceb_historico_data_ultimo_arquivamento</p>";
                          ?>
                         </td> 
                        <?php } elseif ($intervalo->format('%a') <= 90) { ?>
                          <td style="text-align: center;background-color: #f7b218;" >
                            <?php  
                            echo "<p class='example-popover' data-html='true' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Informações Gerais' data-content='<b>Nome do Evento:</b> $c->juceb_historico_nome_do_evento <br><br><b>Nº Arquivamento:</b> $c->juceb_historico_numero_arquivamento <br><br><b>Descrição:</b> $c->juceb_historico_descricao_ato'>$c->juceb_historico_data_ultimo_arquivamento</p>";
                          ?>
                          </td> 
                        <?php } else { ?> 
                          <td style="text-align: center;" >
                            <?php  
                            echo "<p class='example-popover' data-html='true' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Informações Gerais' data-content='<b>Nome do Evento:</b> $c->juceb_historico_nome_do_evento <br><br><b>Nº Arquivamento:</b> $c->juceb_historico_numero_arquivamento <br><br><b>Descrição:</b> $c->juceb_historico_descricao_ato'>$c->juceb_historico_data_ultimo_arquivamento</p>";
                          ?>
                          </td> 
                        <?php } ?>                         
                        <?php endif ?>
                      </tr>
                  <?php } 
                  }
                  ?>
                </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>
 
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->
<script src="<?php echo base_url('assets/js/popover-custom.js'); ?>"></script>
<style type="text/css">
    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );

  $('#filtro').change(function(){
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('juceb'); ?>/"+filtro+"");
  });
});
</script>