<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>



<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Acompanhamento de Viabilidade e Legalização</h2> 
          </div>
          <div class="card-body">
          <a class="btn btn-default btn-rounded pull-right" style="color: #FFFFFF; background-color: #24a0e9;" data-toggle="modal" data-target="#modal_cadastro" href="javascript:;">Novo Protocolo</a> <br> <br> 
              <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'listar_acompanhamento_viabilidade'=>'Todos',
                  'consultaproabertos'=>'Processos Abertos',
                  'consultaprofinalizados'=>'Processos Finalizados'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br><br>
            <div class="table-responsive">
                  <table class="table table-striped" id="example" data-toggle="datatables">
                      <thead class="thead-default thead-lg">
                          <tr> 
                            <th style="">Protocolo</th>
                            <th style="">Nome da Empresa / Solicitante</th>
                            <th style="">Estado do Processo</th>
                            <th style="">Status envio RFB</th>
                            <th style="">Dt. de Atualização</th>
                            <th style="">Dt. de incorporação do protocolo</th>
                            <th style="">Ações</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php  
                        if ($consulta){ 
                          foreach ($consulta as $c){?>
                          <tr>  
                            <?php if ($c->estado_do_processo == "PROCESSO FINALIZADO "): ?>
                                <td style="background-color: #afe6af;"><?php echo $c->protocolo ?></td>
                                <td style="background-color: #afe6af;"><?php echo $c->nome_empresa_mostrar ?></td>
                                <td style="background-color: #afe6af;"><?php echo $c->estado_do_processo ?></td>
                                <td style="background-color: #afe6af;"><?php echo $c->status_de_envio ?></td>
                                <td style="background-color: #afe6af;"><?php echo $c->data_atualizacao ?></td>
                                <td style="background-color: #afe6af;"><?php echo $c->data_incorporacao ?></td> 
                                <td style="width: 10%;  background-color: #afe6af;">
                                 <a href="<?php echo 'http://regin.juceb.ba.gov.br/regin.ba/CON_DadosIdentificacion.aspx?id='."$c->protocolo".'&pTipo=3&pTipoTela=CONSULTA'; ?>" data-toggle="tooltip" data-placement="top" title="Consultar Protocolo" target="_blank"><i class="material-icons" style="color: #24a0e9;">search</i></a>

                                 <a href="<?php echo 'http://regin.juceb.ba.gov.br/regin.ba/CON_InstituicaoProtocolo.aspx?id='."$c->protocolo".'&pTipoTela=CONSULTA'; ?>" data-toggle="tooltip" data-placement="top" title="Consultar Pendências" target="_blank"><i class="material-icons" aria-hidden="true" style="color: #24a0e9;">warning</i></a>

                                 <?php
                                    echo '<a href="#modalEditar" data-toggle="modal" class="b editar" idLancamento="'.$c->id.'" protocoloEditar="'.$c->nome_empresa_mostrar.'"><i class="material-icons" aria-hidden="true" style="color: #24a0e9" data-toggle="tooltip" data-placement="top" title="Editar">create</i></a>';
                                  ?> 
                                 <a href="#" data-toggle="tooltip" data-placement="top" title="Excluir" onclick='preencher_url("<?php echo base_url("juceb/remover_protocolo/$c->id"); ?>")'><span class="sweet-7" style="padding: 1px 4px;"><i class="material-icons" style="color: #24a0e9">delete</i></span></a>
                                </td>

                              <?php else: ?>
                                <td><?php echo $c->protocolo ?></td>
                                <td><?php echo $c->nome_empresa_mostrar ?></td>
                                <td><?php echo $c->estado_do_processo ?></td>
                                <td><?php echo $c->status_de_envio ?></td>
                                <td><?php echo $c->data_atualizacao ?></td>
                                <td><?php echo $c->data_incorporacao ?></td> 
                                <td style="width: 10%; ">
                                 <a href="<?php echo 'http://regin.juceb.ba.gov.br/regin.ba/CON_DadosIdentificacion.aspx?id='."$c->protocolo".'&pTipo=3&pTipoTela=CONSULTA'; ?>" data-toggle="tooltip" data-placement="top" title="Consultar Protocolo" target="_blank"><i class="material-icons" aria-hidden="true" style="color: #24a0e9;">search</i></a>

                                 <a href="<?php echo 'http://regin.juceb.ba.gov.br/regin.ba/CON_InstituicaoProtocolo.aspx?id='."$c->protocolo".'&pTipoTela=CONSULTA'; ?>" data-toggle="tooltip" data-placement="top" title="Consultar Pendências" target="_blank"><i class="material-icons" aria-hidden="true" style="color: #24a0e9;">warning</i></a>

                                 <?php
                                    echo '<a href="#modalEditar" data-toggle="modal" class="b editar" idLancamento="'.$c->id.'" protocoloEditar="'.$c->nome_empresa_mostrar.'"><i class="material-icons" aria-hidden="true" style="color: #24a0e9" data-toggle="tooltip" data-placement="top" title="Editar">create</i></a>';
                                  ?> 
                                 <a href="#" data-toggle="tooltip" data-placement="top" title="Excluir" onclick='preencher_url("<?php echo base_url("juceb/remover_protocolo/$c->id"); ?>")'><span class="sweet-7" style="padding: 1px 4px;"><i class="material-icons" style="color: #24a0e9">delete</i></span></a>
                                </td>
                              <?php endif ?>
                          </tr>
                      <?php } 
                      }
                      ?>
                      </tbody>
                  </table>
              </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modal_cadastro">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header p-4">
              <h5 class="modal-title">Cadastrar Novo Protocolo</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body p-4">
            <?php echo form_open("juceb/cadastrar_protocolo_acompanhamento"); ?> 
              <div class="form-group mb-4">
                <label>Digite o nº do protocolo</label>
                  <input class="form-control" type="text" name="protocolo" placeholder="Ex: BAP1234567890">
              </div>  
          </div>
          <div class="modal-footer  bg-primary-50">           
              <button class="btn btn-warning btn-rounded mr-3" data-dismiss="modal" >Cancelar</button>
              <?php echo form_submit(array('name'=>'btn_cadastrar', 'class'=>'btn btn-primary btn-rounded mr-3'), 'Cadastrar'); ?> 
          </div>
          <?php echo form_close(); ?> 
      </div>
  </div>
</div>

<div class="modal fade" id="modalEditar">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header p-4">
              <h5 class="modal-title">Editar Protocolo</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body p-4">
            <?php echo form_open("juceb/editar_protocolo_acompanhamento/$id"); ?> 
              <div class="form-group mb-4">
                  <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""  />
                  <input class="form-control" type="text" id="protocoloEditarValor" name="nome_empresa_mostrar" placeholder="Digite nome da Empresa" value="">
                  <input type="hidden" id="idEditar" name="id" value="" />
              </div>  
          </div>
          <div class="modal-footer  bg-primary-50">           
              <button class="btn btn-warning btn-rounded mr-3" data-dismiss="modal" >Cancelar</button>
              <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary btn-rounded mr-3'), 'Salvar'); ?> 
          </div>
          <?php echo form_close(); ?> 
      </div>
  </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('juceb'); ?>/"+filtro+"");
  });
});
</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
        toastr["success"]("O Protocolo foi cadastrado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["error"]("Ocorreu um erro ao cadastrar o protocolo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["success"]("O Protocolo foi editado com sucesso!", "Excluído!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["error"]("Ocorreu um erro ao editar o protocolo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 5) { ?>
        toastr["success"]("O Protocolo foi excluído com sucesso!", "Excluído!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 6) { ?>
        toastr["error"]("Ocorreu um erro ao excluir o protocolo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 10) { ?>
        toastr["error"]("Protocolo não encontrado na Juceb.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 12) { ?>
        toastr["error"]("Protocolo já cadastrado.", "Erro!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );



    $(document).on('click', '.editar', function(event) {
      $("#idEditar").val($(this).attr('idLancamento'));
      $("#protocoloEditarValor").val($(this).attr('protocoloEditar'));
      $("#urlAtualEditar").val($(location).attr('href'));
    }); 

});


</script>