<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/
</style>

<style type="text/css">
  .card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details {
    position: absolute;
    top: 0;
    left: 0;
    width: 70%;/*100%*/
    height: 100%;
    transition: .5s;
    transform-origin: left;
    transform: perspective(2000px) rotateY(-90deg);
}
.card:hover .details {
    transform: perspective(2000px) rotateY(0deg);
}
.card .details .center {
    width: 100% !important;
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details .center p {
    margin: 5px 0;
    padding: 0;
    color: #262626;
}
.card .details .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details .center ul li a:hover {
    background: #ff3636;
}


.div_hover { 
  background: white;
  border-radius: 50px; 
}

.div_hover a:hover { 
  background-color: #24a0e9; 
  color: white !important;
}

.tag_a_hover {
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.span_hover:hover{
  background-color: #24a0e9; 
  color: white !important;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.media-body{
  padding-left: 0px !important; 
}
</style>

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Processos</h2> 
        </div> 
      </div>
    </div>
  </div>

<div class="container-fluid general-widget">

  <div class="row">

    <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("juceb/listar_acompanhamento_viabilidade"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <h1><br><span style="font-size: 20px;color: #24a0e9" class="span_hover">JUCEB</span></h1>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("juceb/consultaproabertos"); ?>" style="font-size: 18px;" class="tag_a_hover">Processos Abertos: <?php echo $qtd_juceb->qtd; ?></a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("juceb/consultaprofinalizados"); ?>" style="font-size: 18px;" class="tag_a_hover">Processos Finalizados: <?php echo $qtd_juceb_finalizados->qtd; ?></a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">JUCEB - Cons. Viabilidade</span>
              <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com processos abertos' data-original-title='' title=''><?php echo $qtd_juceb->qtd; ?></div></h4>
              <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="1200.000000pt" height="1200.000000pt" viewBox="0 0 1200.000000 1200.000000" preserveAspectRatio="xMidYMid meet" class="icon-bg"><metadata></metadata><g style="fill: white" transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M7110 8776 c-225 -81 -1411 -563 -1440 -586 -111 -85 -113 -258 -5 -348 39 -31 94 -52 142 -52 28 0 31 -4 77 -120 26 -66 49 -120 51 -120 7 0 1461 583 1481 594 22 11 22 12 -26 131 l-48 120 25 25 c93 91 79 262 -27 335 -73 51 -133 56 -230 21z"/><path d="M7380 8074 c-8 -3 -320 -127 -692 -276 -673 -268 -677 -270 -722 -318 -62 -66 -79 -109 -79 -200 0 -89 17 -133 76 -197 52 -57 110 -83 200 -92 l69 -6 83 -208 84 -209 -52 -22 c-29 -13 -422 -171 -874 -351 l-822 -328 -58 30 c-124 66 -274 85 -395 49 -76 -22 -2299 -910 -2363 -944 -27 -15 -79 -56 -115 -92 -295 -296 -148 -804 260 -900 68 -16 190 -16 250 0 92 25 2345 933 2394 965 102 66 182 168 221 284 l22 66 869 347 c478 192 872 348 875 348 7 0 169 -398 169 -415 0 -6 -18 -28 -40 -50 -59 -57 -83 -116 -84 -200 0 -118 54 -208 159 -263 41 -21 64 -26 125 -27 l75 0 682 273 c606 243 687 278 722 312 56 55 83 113 89 191 7 95 -17 159 -82 224 -59 59 -121 85 -205 85 l-56 0 -278 696 -278 696 40 38 c67 65 86 112 86 210 0 76 -3 91 -30 138 -52 95 -130 143 -240 148 -38 2 -77 1 -85 -2z"/><path d="M7705 5293 c-407 -163 -742 -298 -744 -300 -3 -2 17 -57 44 -124 l47 -121 -30 -31 c-36 -37 -62 -103 -62 -156 0 -56 42 -135 90 -170 52 -38 134 -56 187 -42 21 6 357 139 748 295 503 201 720 293 746 314 91 76 104 223 28 315 -34 42 -86 67 -151 76 l-49 6 -47 114 c-26 63 -52 116 -57 117 -6 2 -343 -130 -750 -293z"/><path d="M6330 3816 l0 -183 -321 -214 -321 -214 1186 -3 c652 -1 1720 -1 2374 0 l1187 3 -322 215 -323 215 0 182 0 183 -1730 0 -1730 0 0 -184z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div> -->

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("ecac/listar_eprocessos_todos"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">e-Processos</span>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ecac/listar_eprocessos_ativos"); ?>" style="font-size: 18px;" class="tag_a_hover">Ativos: <?php echo $qtd_eprocessos->valor; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ecac/listar_eprocessos_inativos"); ?>" style="font-size: 18px;" class="tag_a_hover">Inativos: <?php echo $qtd_eprocessos_inativos->valor;; ?></a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">e-Processos</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com e-Processos ativos' data-original-title='' title=''><?php echo $qtd_eprocessos->valor; ?></div></h4>
                <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="1200.000000pt" height="1200.000000pt" viewBox="0 0 1200.000000 1200.000000" preserveAspectRatio="xMidYMid meet" class="icon-bg"><metadata></metadata><g style="fill: white" transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M7110 8776 c-225 -81 -1411 -563 -1440 -586 -111 -85 -113 -258 -5 -348 39 -31 94 -52 142 -52 28 0 31 -4 77 -120 26 -66 49 -120 51 -120 7 0 1461 583 1481 594 22 11 22 12 -26 131 l-48 120 25 25 c93 91 79 262 -27 335 -73 51 -133 56 -230 21z"/><path d="M7380 8074 c-8 -3 -320 -127 -692 -276 -673 -268 -677 -270 -722 -318 -62 -66 -79 -109 -79 -200 0 -89 17 -133 76 -197 52 -57 110 -83 200 -92 l69 -6 83 -208 84 -209 -52 -22 c-29 -13 -422 -171 -874 -351 l-822 -328 -58 30 c-124 66 -274 85 -395 49 -76 -22 -2299 -910 -2363 -944 -27 -15 -79 -56 -115 -92 -295 -296 -148 -804 260 -900 68 -16 190 -16 250 0 92 25 2345 933 2394 965 102 66 182 168 221 284 l22 66 869 347 c478 192 872 348 875 348 7 0 169 -398 169 -415 0 -6 -18 -28 -40 -50 -59 -57 -83 -116 -84 -200 0 -118 54 -208 159 -263 41 -21 64 -26 125 -27 l75 0 682 273 c606 243 687 278 722 312 56 55 83 113 89 191 7 95 -17 159 -82 224 -59 59 -121 85 -205 85 l-56 0 -278 696 -278 696 40 38 c67 65 86 112 86 210 0 76 -3 91 -30 138 -52 95 -130 143 -240 148 -38 2 -77 1 -85 -2z"/><path d="M7705 5293 c-407 -163 -742 -298 -744 -300 -3 -2 17 -57 44 -124 l47 -121 -30 -31 c-36 -37 -62 -103 -62 -156 0 -56 42 -135 90 -170 52 -38 134 -56 187 -42 21 6 357 139 748 295 503 201 720 293 746 314 91 76 104 223 28 315 -34 42 -86 67 -151 76 l-49 6 -47 114 c-26 63 -52 116 -57 117 -6 2 -343 -130 -750 -293z"/><path d="M6330 3816 l0 -183 -321 -214 -321 -214 1186 -3 c652 -1 1720 -1 2374 0 l1187 3 -322 215 -323 215 0 182 0 183 -1730 0 -1730 0 0 -184z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

    <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("consulta_sipro"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">SIPRO</span>
              <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Processos SIPRO' data-original-title='' title=''><?php echo $qtd_sipro->qtd; ?></div></h4>
              <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="1200.000000pt" height="1200.000000pt" viewBox="0 0 1200.000000 1200.000000" preserveAspectRatio="xMidYMid meet" class="icon-bg"><metadata></metadata><g style="fill: white" transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M7110 8776 c-225 -81 -1411 -563 -1440 -586 -111 -85 -113 -258 -5 -348 39 -31 94 -52 142 -52 28 0 31 -4 77 -120 26 -66 49 -120 51 -120 7 0 1461 583 1481 594 22 11 22 12 -26 131 l-48 120 25 25 c93 91 79 262 -27 335 -73 51 -133 56 -230 21z"/><path d="M7380 8074 c-8 -3 -320 -127 -692 -276 -673 -268 -677 -270 -722 -318 -62 -66 -79 -109 -79 -200 0 -89 17 -133 76 -197 52 -57 110 -83 200 -92 l69 -6 83 -208 84 -209 -52 -22 c-29 -13 -422 -171 -874 -351 l-822 -328 -58 30 c-124 66 -274 85 -395 49 -76 -22 -2299 -910 -2363 -944 -27 -15 -79 -56 -115 -92 -295 -296 -148 -804 260 -900 68 -16 190 -16 250 0 92 25 2345 933 2394 965 102 66 182 168 221 284 l22 66 869 347 c478 192 872 348 875 348 7 0 169 -398 169 -415 0 -6 -18 -28 -40 -50 -59 -57 -83 -116 -84 -200 0 -118 54 -208 159 -263 41 -21 64 -26 125 -27 l75 0 682 273 c606 243 687 278 722 312 56 55 83 113 89 191 7 95 -17 159 -82 224 -59 59 -121 85 -205 85 l-56 0 -278 696 -278 696 40 38 c67 65 86 112 86 210 0 76 -3 91 -30 138 -52 95 -130 143 -240 148 -38 2 -77 1 -85 -2z"/><path d="M7705 5293 c-407 -163 -742 -298 -744 -300 -3 -2 17 -57 44 -124 l47 -121 -30 -31 c-36 -37 -62 -103 -62 -156 0 -56 42 -135 90 -170 52 -38 134 -56 187 -42 21 6 357 139 748 295 503 201 720 293 746 314 91 76 104 223 28 315 -34 42 -86 67 -151 76 l-49 6 -47 114 c-26 63 -52 116 -57 117 -6 2 -343 -130 -750 -293z"/><path d="M6330 3816 l0 -183 -321 -214 -321 -214 1186 -3 c652 -1 1720 -1 2374 0 l1187 3 -322 215 -323 215 0 182 0 183 -1730 0 -1730 0 0 -184z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div> -->

    
    
  </div>


    </div>

  </div>
</div> 

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->

<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->


<script type="text/javascript">
  $(document).ready(function() {
    

      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>