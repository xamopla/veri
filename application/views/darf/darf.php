<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<style>
.datepicker{z-index:1151 !important;}
</style>
<style type="text/css">

.swal2-styled{
    background-color: #dc2727 !important;
 } 

.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.left, .center, .right, .lefter, .righter {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.center2 {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  /*border-radius: 8px;*/
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.center2 {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/barcode.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
}
.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}


.modal-dialog {
    width: 1000px;
    max-width : 1000px;
}
</style>

<style type="text/css">
  .title-menu {
    cursor: pointer;
    color: #0A4C62 !important;
    background-color: #def0c1 !important;
    border-color: #def0c1 !important;
}
.div-menu {
    padding:0px;
}
.item-menu {
    cursor: pointer;
    padding-left: 15px;
}
.item-menu:hover {
    cursor: pointer;
    /*padding-left: 15px;*/
    background-color: #f8f8f8;
    font-weight: bold;
}
.item-menu.active {
    background-color: #eeeeee !important;
    font-weight: bolder;
}
.table-menu {
    margin-bottom: 0px;
}
.glyphicon-menu {
    margin-right:10px;
}
.text-menu {
    color: #0A4C62;
}
.text-menu:hover, .text-menu:active, .text-menu:visited, .text-menu:focus {
    text-decoration: none;
}
.text-title-menu:hover, .text-title-menu:active, .text-title-menu:visited, .text-title-menu:focus {
    text-decoration: none;
}


.table-contribuinte {
    width: 100%;
    margin-bottom: 6px;
}

.mensagem-inicial {
    margin-top: 20px;
    text-align: center;
}

.mensagem-inicial p {
    color: #0A4C62;
    font-size: 12px;
}

.form-ano-calendario {
    margin-top: 20px;
}

.consulta-emissao, .consulta-extrato {
    text-align: center;
}

/* Tabela */

table.trabalhar td {
    font-size: 11px;
}


table.consulta {
    margin: 0 auto;
    margin-top: 10px;
}

table.consulta th.pa {
    vertical-align:middle !important; 
    text-align: left; 
    padding: 4px !important; 
    border: 2px solid white;
    color: #0A4C62;
}

table.consulta th.header {
    vertical-align:middle !important; 
    text-align: center; 
    padding: 4px !important; 
    border: 1px solid white;
    color: #0A4C62;
}

table.consulta td.header 
{
    color: #555555;
    padding: 1px;
    font-weight:bold;
    font-size: 10px !important;
    text-align:center;
}

table.emissao th.detalhe 
{
    padding: 0px !important;
    width: 1%; /* !important;*/
    border: 1px solid #99BBE8 !important;
    background-color: #D6E3F2;
    cursor: pointer;
}

table.emissao th.check {
    width: 2%;
}

table.emissao th.periodo {
    width: 20%;
}

table.emissao th.apurado {
    width: 8%;
}

table.emissao th.situacao, table.emissao th.beneficio {
    width: 10%;
}

table.emissao th.resumo {
    width: 45%;
}

table.emissao th.principal, table.emissao th.multa, table.emissao th.juros, table.emissao th.total  {
    width: 9%;
    white-space: nowrap;
}

table.emissao td {
    padding: 2px !important;
    padding-left: 6px !important;
    vertical-align: middle !important;
}

table.emissao td.pa {
    font-weight: bold;
    color:#0A4C62; 
}

table.emissao td.selecionar, table.emissao td.beneficio {
    text-align: center;
}

table.emissao td.checkall {
    color:#0A4C62; 
    /*padding-top: 4px;*/
    border: 0px !important;
}

table.emissao td.simnao a
{
    font-size: 12px;
    text-decoration: underline;
    color: #0A4C62;
}

table.emissao td.situacaoPa 
{
    font-weight: bold;
    color:#0A4C62; 
}

table.emissao td.pago, table.emissao td.pago a
{
    color: #1caa12;
}

table.emissao td.devedor
{
    color: #FF0000;
}

/*  Detalhes do DAS pago */

div.modal-body p span:first-child 
{
    font-weight: bold;
    color: #0A4C62;
}

</style>
<script type="text/javascript">
<?php
switch ($m) {
        case "01":    $mes = Janeiro;     break;
        case "02":    $mes = Fevereiro;   break;
        case "03":    $mes = Marco;       break;
        case "04":    $mes = Abril;       break;
        case "05":    $mes = Maio;        break;
        case "06":    $mes = Junho;       break;
        case "07":    $mes = Julho;       break;
        case "08":    $mes = Agosto;      break;
        case "09":    $mes = Setembro;    break;
        case "10":    $mes = Outubro;     break;
        case "11":    $mes = Novembro;    break;
        case "12":    $mes = Dezembro;    break; 
 } 

?>
</script>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div> 

<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <!-- <a class="btn mr-1 shadow-z-2 btn-round btn-info pull-right" data-toggle="modal" data-target="#modal_filtro" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Filtro"><span class="fa fa-filter"></span></a> -->
                     
                      <h4 class="card-title">DARF - Comprovante de Arrecadação</h4>
                      <h6 class="card-title" style="color: red"></h6>
                    </div>

                    <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_1"> 

                        <br>
                        <div class="card-block text-center col-">
                          <div class="form-body">
                            <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("darf/index/anterior/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                              <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="block-button">
                                  <a class="btn btn-outline-success btn-lg btn-block"><?php if($mes == "Marco"){
                                    echo "Março";
                                  }else{
                                    echo $mes;
                                  }?> </a>
                                   <?php echo $ano ?>
                                </div>
                              </div>

                              <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="<?php echo base_url("darf/index/proximo/$mes/$ano"); ?>">                             
                                </a> 
                              </span>
                            </div>
                          </div>
                        </div>

                    </div>

                    <!-- OPÇÃO 02 - -->
                    <div class="card-title-wrap bar-success" id="mostrar_meses_opcao_2" style="display: none"> 
                        <br>
                        <?php echo form_open("darf/index", array('role'=>'form')); ?>
                        <div class="card-block text-center col-">
                          <div class="form-body">
                            <div class="row justify-content-center">
                              <span class="dropleft mr-2"> 
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:diminuiAno()">                             
                                </a> 
                              </span>
                              <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="block-button">
                                  <input type="text" name="ano_selecionado" id="ano_selecionado" value="<?php echo $ano ?>" class="btn btn-outline-success btn-lg btn-block" placeholder="<?php echo $ano ?>" />
                                </div>
                              </div>

                              <span class="dropright mr-2">
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:aumentaAno()">                             
                                </a> 
                              </span>
                            </div>
                          </div>
                        </div>

                        <div class="card-block text-center col-" style="margin-top: 5px;">
                          <div class="form-body">
                            <div class="row justify-content-center"> 
                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Dezembro')" name="mes_selecionado" id="mes_selecionado" value="01">JAN.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Janeiro')" name="mes_selecionado" id="mes_selecionado" value="02">FEV.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Fevereiro')" name="mes_selecionado" id="mes_selecionado" value="03">MAR.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Março')" name="mes_selecionado" id="mes_selecionado" value="04">ABR.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Abril')" name="mes_selecionado" id="mes_selecionado" value="05">MAI.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Maio')" name="mes_selecionado" id="mes_selecionado" value="06">JUN.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Junho')" name="mes_selecionado" id="mes_selecionado" value="07">JUL.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Julho')" name="mes_selecionado" id="mes_selecionado" value="08">AGO.</button>  
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Agosto')" name="mes_selecionado" id="mes_selecionado" value="09">SET.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Setembro')" name="mes_selecionado" id="mes_selecionado" value="10">OUT.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Outubro')" name="mes_selecionado" id="mes_selecionado" value="11">NOV.</button> 
                                </div>
                              </div> 

                              <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="block-button">
                                  <button class="btn btn-outline-success" type="button" onclick="envia_form('Novembro')" name="mes_selecionado" id="mes_selecionado" value="12">DEZ.</button>  
                                </div>
                              </div> 
                            </div>
                          </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- OPÇÃO 02 - -->

                </div>

                <div class="card-body">
                  <div class="form-group col-lg-4 col-xs-12 pull-right">
                    <label>Filtro:</label>
                    <?php 
                    $valores_filtro = array(
                        'TODAS'=>'Todas',
                        'REGULAR'=>'Pagos',
                        'PENDENTE'=>'Não Pagos',
                        'DEBITOS'=>'Débitos'
                        
                    );
                    echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
                    ?>
                  </div><br> <br>
                    <div class="card-block">
                        <table class="table table-bordered table-responsive-md text-center" id="example" data-toggle="datatables">
                            <thead>
                                <tr style="background-color:#DEF0C1">
                                  <th class="pa" colspan="12"><?php echo $m.'/'.$ano; ?></th>
                                </tr>
                                <tr>
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Nº Documento</th>
                                    <th>Período de Apuração</th>
                                    <th>Data de Arrecadação</th>
                                    <th>Data de Vencimento</th>
                                    <th>Código de Receita</th>
                                    <th>Valor Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($consulta){ 
                                    foreach ($consulta as $e){?>
                                <tr>
                                   
                                    <td><?php echo $e->razao_social; ?></td>
                                    <td><?php echo $e->cnpj_completo; ?></td>
                                    
                                    <td><?php echo $e->numero_documento; ?></td>
                                    <td><?php echo $e->periodo_apuracao; ?></td>
                                    <td><?php echo $e->data_arrecadacao; ?></td>
                                    <td><?php echo $e->data_vencimento; ?></td>
                                    <td><?php echo $e->codigo_receita; ?></td>
                                    <td><?php echo $e->valor_total; ?></td>
                                    <td>
                                        <div class="all">
                                        <div class="lefter">
                                          <a href="mailto:?subject=Notificação Sistema VERI&body=O extrato do DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> está disponível no link <?php echo $e->caminho_download_extrato; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="left">
                                           <a href="https://api.whatsapp.com/send?text=O extrato do DAS nº <?php echo $e->numero_das; ?> da empresa <?php echo $e->razao_social; ?> está disponível no link <?php echo $e->caminho_download_extrato; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Whatsapp</div>
                                          </a>
                                        </div>
                                        <div class="center">
                                          <a href="<?php echo $e->caminho_download_extrato; ?>" download  data-toggle="tooltip" data-placement="top" title="Download" class="">
                                            <div class="explainer"></div>
                                            <div class="text"></div>
                                          </a>
                                        </div>
                                        <div class="right">
                                          <a href="<?php echo $e->caminho_download_extrato; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class="">
                                            <div class="explainer"></div>
                                            <div class="text">Visualizar</div>
                                          </a>
                                        </div>
                                        <!-- <div class="righter">
                                          <div class="text">SEO</div>
                                        </div> -->
                                      </div>
                                    </td>

                                </tr>
                            <?php } 
                            }
                            ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Razão Social</th>
                                    <th>CNPJ</th>
                                    <th>Nº Documento</th>
                                    <th>Período de Apuração</th>
                                    <th>Data de Arrecadação</th>
                                    <th>Data de Vencimento</th>
                                    <th>Código de Receita</th>
                                    <th>Valor Total</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<input type="hidden" name="imagem" id="logo_relatorio" value="<?php echo $this->session->userdata['logo']; ?>">
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    var url = window.location.href;
    url = url.substring(0,url.indexOf('?'));
    window.location.replace(url+"?filtro="+filtro+"");
  });
});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) { 

// BOTÃO DE BUSCAR ANOS E MESES
$('#mostrar_meses_opcao_1').click(function(event) {     
  $('#mostrar_meses_opcao_2').show(); 
  $('#mostrar_meses_opcao_1').hide();
});


});

</script>

<script type="text/javascript">
  
</script>
<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  });

var table = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                customize: function ( doc ) {
                  doc.content[1].table.widths = 
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.defaultStyle.alignment = 'center';
                    doc.content.splice( 1, 0, {
                        margin: [ 0, -50, 0, 12 ],
                        alignment: 'left',
                        image:  'data:text/html;base64,'+$("#logo_relatorio").val()
                    } );
                    
                }
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});

</script>

<script type="text/javascript">
    function diminuiAno(){
      var ano = $("#ano_selecionado").val();
      ano = parseInt(ano);
      $("#ano_selecionado").val(ano - 1);
    }
    function aumentaAno(){
      var ano = $("#ano_selecionado").val();
      ano = parseInt(ano);
      $("#ano_selecionado").val(ano + 1);
    }

    function envia_form(mes){
      var mes_selecionado = $("#mes_selecionado").val();
      var ano_selecionado = $("#ano_selecionado").val();

      if(mes == 'Dezembro'){
        ano_selecionado = ano_selecionado - 1;
      }
      

      window.location = '<?php echo base_url("darf/index/proximo"); ?>'+"/"+mes+"/"+ano_selecionado;
    }

    function reload_page(){
      window.location = global_url;
    }
</script>