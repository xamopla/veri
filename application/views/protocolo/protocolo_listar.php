<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
    .buttons-excel{
        width: 100px;
    }

    .dataTables_filter{
        float: right;
    }

    .table.dataTable th, table.dataTable td {
        vertical-align: middle;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2>Protocolo de Mensagens</h2>
          </div>
          <div class="card-body">
            <div class="form-group pull-right">
              <label>Filtro:</label>
              <select class="form-control" id="filtro" onchange="loadAjax()">
                <option value="Todas">Todas</option>
                <option value="Verificadas">Verificadas</option>
                <option value="Não Verificadas">Não Verificadas</option>
              </select>
            </div><br><br>
            <div class="table-responsive">
                <table class="table table-striped" id="example" data-toggle="datatables">
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th>Razão Social</th>
                          <th>CNPJ</th>
                          <th width="20%">Situação</th>
                          <th width="8%">Data da Notificação</th>
                          <th>Status</th> 
                          <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<div class="modal fade" id="new-mail-modal">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header p-4">
              <h5 class="modal-title">Protocolar Notificação</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body p-4">
            <?php echo form_open("protocolo/salvarNewPopUp"); ?>
              <div class="flexbox mb-4">
                  <span class="btn-icon-only btn-circle bg-primary-50 text-primary mr-3"><i class="fa fa-building"></i></span>
                  <div class="flex-1 d-flex">
                      <div class="flex-1">
                          <span class="text-muted mr-2">Empresa:</span>
                          <div id="nomeEmpresa"></div>
                      </div> 
                  </div>
              </div>
              <div class="form-group mb-4">
                  <input class="form-control form-control-line" type="text" name="usuario" id="usuario" placeholder="Usuário" readonly="true">
              </div>
              <div class="form-group mb-4">
                  <input class="form-control form-control-line" type="text" name="data" id="data" placeholder="Data" readonly="true">
              </div>
              <div class="form-group mb-4">
                  <textarea class="form-control form-control-line" rows="4" name="mensagem" id="mensagem" placeholder="Mensagem"></textarea>
              </div>
              <input type="text" name="isnotificaoresumo" id="isnotificaoresumo" style="visibility: hidden;">
              <input type="text" name="idnotificacao" id="idnotificacao" style="visibility: hidden;">
          </div>
          <div class="modal-footer justify-content-between bg-primary-50">           
              <button class="btn btn-primary btn-rounded mr-3" type="submit" id="btnSalvar" >Salvar</button>
          </div>
          <?php echo form_close(); ?> 
      </div>
  </div>
</div>

<div class="modal fade" id="detalhe-modal">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header p-4">
                <h5 class="modal-title" id="title"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="flexbox mb-4">
                    <div class="flex-1 d-flex">
                        <div class="flex-1">
                            <div id="conteudo"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {

  loadAjax();

});

function loadAjax(){
  var filtro = $('#filtro option:selected').text();
  
  var variaveis = {filtros : filtro};

  $.ajax({
    type: "POST",
    url: '<?php echo base_url('protocolo/listarAjax'); ?>',
    data: variaveis,
    async: true,
    success: function(result){
        var data = JSON.parse(result);
        if(data == "") {
            var array = [];
            popularDataTable(array);  
            jQuery('[data-toggle="popover"]').popover();   
            jQuery('[data-toggle="tooltip"]').tooltip();              
        }else{
          var array = json2array(data);
          popularDataTable(array);
          jQuery('[data-toggle="popover"]').popover();
          jQuery('[data-toggle="tooltip"]').tooltip(); 
        }
    }
  });

}

function popularDataTable(json){
  $.fn.dataTable.moment('DD/MM/YYYY');
  $('#example').DataTable({
        "order": [[ 3, "desc" ]],
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    data: json,
    "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
}

function openModalDetalhes(ie, descricao){
  if(ie.startsWith("0")){
    ie = ie.substring(1);
  }
  if(descricao == "Sócio Irregular"){
    openModalDetalhesSocio(ie);
  }else if(descricao == "Omisso DMA"){
    openModalDetalhesDMA(ie);
  }else if(descricao == "Omisso EFD"){
    openModalDetalhesEFD(ie);
  }else if(descricao == "Parcelamento em atraso ou interrompido"){
    openModalDetalhesParcelamentoAtraso(ie);
  }else if(descricao == "Diver. ICMS normal informado na DMA e o recolhido"){
    openModalDetalhesAux(ie,7);
  }else if(descricao == "Diver. ICMS Subs. por Antecipação informado na DMA e o recolhido"){
    openModalDetalhesAux(ie,8);
  }else if(descricao == "Diver. ICMS Subs. por Retenção informado na DMA e o recolhido"){
    openModalDetalhesAux(ie,9);
  }else if(descricao == "O.S. de Monitoramento ativa"){
    openModalDetalhesOsMonitoramento(ie);
  }else if(descricao == "O.S. de Auditoria ativa"){
    openModalDetalhesOsAuditoria(ie);
  }else if(descricao == "Outros PAFs com ciclo de vida ativo"){
    openModalDetalhesPaf(ie);
  }

}

function openModalDetalhesAux(inscricaoEstadual, numeroPergunta){
  $.post("<?php echo base_url();?>resumofiscal/findPergunta",
  {
      inscricaoEstadual:inscricaoEstadual,
      pergunta:numeroPergunta
  },
  function(data){

    if(numeroPergunta == 7){
      $("#title").html("Divergência entre ICMS normal informado na DMA e o recolhido");
    }else if(numeroPergunta == 8){
      $("#title").html("Divergência entre ICMS Substituição Tributária por Antecipação informado na DMA e o recolhido");
    }else{
      $("#title").html("Divergência entre ICMS Substituição Tributária por Retenção informado na DMA e o recolhido");
    }
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}


function openModalDetalhesPaf(inscricaoEstadual){
  $.post("<?php echo base_url();?>resumofiscal/findPaf",
  {
      inscricaoEstadual:inscricaoEstadual
  },
  function(data){
    $("#title").html("PAFs com ciclo de vida ativo");
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}

function openModalDetalhesSocio(inscricaoEstadual){
  $.post("<?php echo base_url();?>resumofiscal/findSocioIrregular",
  {
      inscricaoEstadual:inscricaoEstadual
  },
  function(data){
    $("#title").html("Sócio(s) Irregular(es)");
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}

function openModalDetalhesEFD(inscricaoEstadual){
  $.post("<?php echo base_url();?>resumofiscal/findEFD",
  {
      inscricaoEstadual:inscricaoEstadual
  },
  function(data){
    $("#title").html("Omisso EFD");
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}

function openModalDetalhesDMA(inscricaoEstadual){
  $.post("<?php echo base_url();?>resumofiscal/findDMA",
  {
      inscricaoEstadual:inscricaoEstadual
  },
  function(data){
    $("#title").html("Omisso DMA");
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}

function openModalDetalhesOsMonitoramento(inscricaoEstadual){
  $.post("<?php echo base_url();?>resumofiscal/findOsMonitoramento",
  {
      inscricaoEstadual:inscricaoEstadual
  },
  function(data){
    $("#title").html("O.S de Monitoramento Ativa");
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}

function openModalDetalhesOsAuditoria(inscricaoEstadual){
  $.post("<?php echo base_url();?>resumofiscal/findOsAuditoria",
  {
      inscricaoEstadual:inscricaoEstadual
  },
  function(data){
    $("#title").html("O.S de Auditoria Ativa");
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}

function openModalDetalhesParcelamentoAtraso(inscricaoEstadual){
  $.post("<?php echo base_url();?>resumofiscal/findParcelamentoEmAtraso",
  {
      inscricaoEstadual:inscricaoEstadual
  },
  function(data){
    $("#title").html("Parcelamento em Atraso");
    $("#conteudo").html(data);
    $('#detalhe-modal').modal();     
  });
}


function json2array(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];

    for(var i in data){
      array = [];
      data[i].data = convertDate2(data[i].data);
      if(data[i].usuario == null){
        data[i].usuario = "";
      }
      if(data[i].dataresolucao == null){
        data[i].dataresolucao = "";
      }

      if(data[i].situacao == null){
        data[i].situacao = "";
      }

      if(data[i].cnpj_completo == null){
        data[i].cnpj_completo = "";
      }
      
      //onclick='openModalDetalhesOsAuditoria($c->inscricaoEstadual)'

      if(data[i].login_sefaz != "" && data[i].senha_sefaz != ""){
        array.push('<a target="_blank" href="<?php echo base_url('consulta_dte/consultar/');?>'+data[i].idEmpresa+'">'+data[i].razao_social+'</a>');
      }else{
        array.push('<td style="text-align: left;">'+data[i].razao_social+'</td>');
      }

      array.push('<td style="text-align: left;">'+data[i].cnpj_completo+'</td>');

      if (data[i].motivo == null) {
        var ie = data[i].ie;
        if(ie != null){
          if(ie.toString().startsWith('0')){
            data[i].ie = ie.substring(1);
          }
        }
        
        array.push('<a href="javascript:openModalDetalhes(&quot;'+data[i].ie+'&quot;,&quot;'+data[i].situacao+'&quot;)">'+data[i].situacao+'</a>');
      //array.push('<a target="_blank" href="<?php echo base_url('protocolo/listarResumoFiscalEmpresa/');?>'+data[i].idEmpresa+'">'+data[i].situacao+'</a>');
      } else {
      array.push("<td><span data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo desta Situação Cadastral' data-content='"+data[i].motivo+"'>"+data[i].situacao+"</span></td>"); 
      } 
      array.push('<td>'+data[i].data+'</td>');

      if(data[i].status == "1"){
        if(data[i].idprotocolo != null){
          array.push("<td><span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-html='true' data-trigger='hover' data-placement='top' title='Dados do Fechamento' data-content='Colaborador: "+data[i].usuario+"<br>Data de Fechamento: "+data[i].dataresolucao+"'>NÃO VERIFICADO</span></td>");

          array.push("<td><a href='javascript:openPopUp(&quot;"+data[i].razao_social+"&quot;, &quot;"+data[i].usuario+"&quot;, &quot;"+data[i].dataresolucao+"&quot;, &quot;"+data[i].descricao+"&quot;);' class='bc-share-def bc-prot' data-toggle='tooltip' data-placement='top' title='Protocolar Notificação'></a><a class='bc-share-def bc-wha2' href='https://api.whatsapp.com/send?text="+data[i].razao_social+" está com a situação: "+data[i].situacao+" em "+data[i].data+". O Status está como: NÃO VERIFICADO' id='bc-whats' rel='nofollow' target='_blank' data-toggle='tooltip' data-placement='top' title='Enviar notificação pelo WhatsApp'></a><a class='bc-share-def bc-mail2' href='mailto:?subject=Notificação Sistema VERI&body=A empresa "+data[i].razao_social+" recebeu a notificação: "+data[i].situacao+" em "+data[i].data+". O Status está como: NÃO VERIFICADO' id='bc-sharemail' rel='nofollow' data-toggle='tooltip' data-placement='top' title='Enviar notificação por e-mail'></a></td>");
        }else{
          array.push("<td><span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-html='true' data-trigger='hover' data-placement='top' title='Dados do Fechamento' data-content='Colaborador: "+data[i].usuario+"<br>Data de Fechamento: "+data[i].dataresolucao+"'>NÃO VERIFICADO</span></td>");

          array.push("<td><a href='javascript:openNewPopUp(&quot;"+data[i].razao_social+"&quot;, &quot;<?php echo $nomeusuario; ?>&quot;, &quot;"+data[i].isnotificaoresumo+"&quot;, &quot;"+data[i].idnotificacao+"&quot;);' class='bc-share-def bc-prot' data-toggle='tooltip' data-placement='top' title='Protocolar Notificação'></a><a class='bc-share-def bc-wha2' href='https://api.whatsapp.com/send?text="+data[i].razao_social+" está com a situação: "+data[i].situacao+" em "+data[i].data+". O Status está como: NÃO VERIFICADO' id='bc-whats' rel='nofollow' target='_blank' data-toggle='tooltip' data-placement='top' title='Enviar notificação pelo WhatsApp'></a><a class='bc-share-def bc-mail2' href='mailto:?subject=Notificação Sistema VERI&body=A empresa "+data[i].razao_social+" recebeu a notificação: "+data[i].situacao+" em "+data[i].data+". O Status está como: NÃO VERIFICADO' id='bc-sharemail' rel='nofollow' data-toggle='tooltip' data-placement='top' title='Enviar notificação por e-mail'></a></td>");
        }
        
      }else{
        if(data[i].idprotocolo != null){
          array.push("<td><span class='badge badge-success badge-shadow' style='background-color:#2ecc71!important;' data-container='body' data-toggle='popover' data-html='true' data-trigger='hover' data-placement='top' title='Dados do Fechamento' data-content='Colaborador: "+data[i].usuario+"<br>Data de Fechamento: "+data[i].dataresolucao+"'>VERIFICADO</span></td>");

          array.push("<td><a href='javascript:openPopUp(&quot;"+data[i].razao_social+"&quot;, &quot;"+data[i].usuario+"&quot;, &quot;"+data[i].dataresolucao+"&quot;, &quot;"+data[i].descricao+"&quot;);' class='bc-share-def bc-prot' data-toggle='tooltip' data-placement='top' title='Protocolar Notificação'></a><a class='bc-share-def bc-wha2' href='https://api.whatsapp.com/send?text="+data[i].razao_social+" está com a situação: "+data[i].situacao+" em "+data[i].data+". O Status está como: VERIFICADO' id='bc-whats' rel='nofollow' target='_blank' data-toggle='tooltip' data-placement='top' title='Enviar notificação pelo WhatsApp'></a><a class='bc-share-def bc-mail2' href='mailto:?subject=Notificação Sistema VERI&body=A empresa "+data[i].razao_social+" recebeu a notificação: "+data[i].situacao+" em "+data[i].data+". O Status está como: VERIFICADO' id='bc-sharemail' rel='nofollow' data-toggle='tooltip' data-placement='top' title='Enviar notificação por e-mail'></a></td>");
        }else{
          array.push("<td><span class='badge badge-success badge-shadow' style='background-color:#2ecc71!important;' data-container='body' data-toggle='popover' data-html='true' data-trigger='hover' data-placement='top' title='Dados do Fechamento' data-content='Colaborador: "+data[i].usuario+"<br>Data de Fechamento: "+data[i].dataresolucao+"'>VERIFICADO</span></td>");

          array.push("<td><a href='javascript:openNewPopUp(&quot;"+data[i].razao_social+"&quot;, &quot;<?php echo $nomeusuario; ?>&quot;, &quot;"+data[i].isnotificaoresumo+"&quot;, &quot;"+data[i].idnotificacao+"&quot;);' class='bc-share-def bc-prot' data-toggle='tooltip' data-placement='top' title='Protocolar Notificação'></a><a class='bc-share-def bc-wha2' href='https://api.whatsapp.com/send?text="+data[i].razao_social+" está com a situação: "+data[i].situacao+" em "+data[i].data+". O Status está como: VERIFICADO' id='bc-whats' rel='nofollow' target='_blank' data-toggle='tooltip' data-placement='top' title='Enviar notificação pelo WhatsApp'></a><a class='bc-share-def bc-mail2' href='mailto:?subject=Notificação Sistema VERI&body=A empresa "+data[i].razao_social+" recebeu a notificação: "+data[i].situacao+" em "+data[i].data+". O Status está como: VERIFICADO' id='bc-sharemail' rel='nofollow' data-toggle='tooltip' data-placement='top' title='Enviar notificação por e-mail'></a></td>");
        }

      }

      arrayMultiple.push(array);
    }
    
    return arrayMultiple;
}

function openNewPopUp(razao_social, nomeusuario, isnotificaoresumo, idnotificacao){
  var data = retornaDataHoraAtual();

  $('#nomeEmpresa').html(razao_social);
  $('#usuario').val(nomeusuario);
  $('#data').val(data);
  $('#mensagem').val("");
  $('#mensagem').attr('readonly', false);
  $('#idnotificacao').val(idnotificacao);
  $('#isnotificaoresumo').val(isnotificaoresumo);
  $('#btnSalvar').show();
  $('#new-mail-modal').modal();
}

function openPopUp(razao_social, usuario, data, mensagem){
  $('#nomeEmpresa').html(razao_social);
  $('#usuario').val(usuario);
  $('#data').val(data);
  $('#mensagem').val(mensagem);
  $('#mensagem').attr('readonly', true);
  $('#btnSalvar').hide();
  $('#new-mail-modal').modal();
}

function retornaDataHoraAtual(){
  var dNow = new Date();
  var hora = dNow.getHours();
  if(hora < 10){
    hora = "0" + hora;
  }
  var minutos = dNow.getMinutes();
  if(minutos < 10){
    minutos = "0" + minutos;
  }
  var localdate = dNow.getDate() + '/' + (dNow.getMonth()+1) + '/' + dNow.getFullYear() + ' ' + hora + ':' + minutos
  return localdate;
}

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
}

function convertDate2(a){
  var day = a.split('-')[2];
  day = day.split(' ')[0];
  var month = a.split('-')[1];
  var year = a.split('-')[0];

  var date = day + "/" + month + "/" + year;
  return date;
}

function consultaDTE(login,senha){
  $("#loginDTE").val(login);
  $("#senhaDTE").val(senha);
  $("#btn-submitDTE").click();
} 
//document.getElementById('bc-whats').href = 'https://api.whatsapp.com/send?text=' + title;

//document.getElementById('bc-sharemail').href = 'mailto:?subject=' + title + '&body=' + descript;
</script>