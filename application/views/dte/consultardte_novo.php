<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login SEFAZ-BA</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
</head>
<body>
	<header class="page-header">
		<div class="container-fluid">
			<h1 class="text-center">Login SEFAZ-BA</h1>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<table class="table">
					<thead>
						<tr>
							<th>CNPJ</th>
							<th colspan="2">Login</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								07.204.158/0001-22
							</td>
							<td>
								6548556500
							</td>
							<td>
								<form action="https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login?ReturnUrl=%2fsistemas%2fDTE%2fContribuinte%2fDefault.aspx" method="post" target="_self">
									<input type="hidden" name="ctl00$ScriptManager2" value="ctl00$updPanel|ctl00$PHCentro$btnLogin">
									<input type="hidden" name="__EVENTTARGET" value="">
									<input type="hidden" name="__EVENTARGUMENT" value="">
									<input type="hidden" name="__VIEWSTATE" value="/wEPDwUKMTYwMTM5MTE2OA9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFA0RURWQCBQ8WAh8ABS5Eb21pYyYjMjM3O2xpbyBUcmlidXQmIzIyNTtyaW8gRWxldHImIzI0NDtuaWNvZAIJD2QWAmYPZBYCAgEPZBYGAgMPDxYIHjBfX1BIQ2VudHJvX21zZ0Vycm9Mb2dpbl9WaWV3U3RhdGVBU01vZGFsVGl0dWxvSUQFEVNlc3PDo28gRW5jZXJyYWRhHjJfX1BIQ2VudHJvX21zZ0Vycm9Mb2dpbl9WaWV3U3RhdGVBU01vZGFsQ29udGV1ZG9JRGUeCENzc0NsYXNzBQptb2RhbCBmYWRlHgRfIVNCAgIWDB4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljHglkcmFnZ2FibGUFBHRydWVkAgUPZBYEAgcPFgIfAGVkAgsPDxYCHgdWaXNpYmxlZ2RkAgcPFgIfAAX9Bzx0YWJsZT4NCg0KPHRyPg0KDQogICAgPHRkPg0KICAgIA0KICAgICAgICA8ZGl2IGNsYXNzPSJjb2wxIHNwYW41Ij4NCiAgICAgICAgICAgIDxpbWcgc3JjPSIvZHRlL2R0X2VfZmluYWwucG5nIiBhbHQ9IkxvZ28gRFRFIi8+DQogICAgICAgICAgICA8YnIgLz48YnIgLz48YnIgLz4NCiAgICAgICAgPC9kaXY+DQogICAgPC90ZD4NCjwvdHI+DQo8dHI+DQogICAgPHRkPg0KICAgDQogICAgICA8ZGl2Pg0KDQogICAgICAgIDxwPkZlcnJhbWVudGEgZXh0ZXJuYSBwYXJhIGdlcmVuY2lhbWVudG8gZGEgQ29udGEgbm8gRG9taWPDrWxpbyBUcmlidXTDoXJpbyBFbGV0csO0bmljbyAtIERURTwvcD4NCiAgICANCiAgICAgICAgPGgxPg0KICAgIA0KICAgICAgICA8L2gxPg0KICAgICAgICA8aDI+SW5mb3JtYcOnw7Vlcy48L2gyPg0KICAgIA0KICAgICAgICA8cD4NCiAgICAgICAgICAgIFByb2pldGFkbyBjb20gYSBpZGVpYSBkZSBkZXNlbnZvbHZlciBkZSBmb3JtYSBtYWlzIGludHVpdGl2YSwgZWZpY2llbnRlLCDDunRpbCBlIHNpbXBsZXMuIA0KICAgICAgICAgICAgPGJyIC8+DQogICAgICAgICAgICBDb20gYSBmZXJyYW1lbnRhIHBvZGVyw6EgY2FkYXN0cmFyIHVtYSBDb250YSBEb21pY8OtbGlvIFRyaWJ1dMOhcmlvLCBFbmRlcmXDp29zIEVsZXRyw7RuaWNvcywgVGVsZWZvbmVzIENlbHVsYXJlcywgTGVyIE1lbnNhZ2VucyBSZWNlYmlkYXMgZSBWaXN1YWxpemFyIFJlbGF0w7NyaW9zLg0KICAgICAgICA8L3A+DQogICAgICAgIDx1bD4NCiAgICAgICAgICAgIDxsaT4NCiAgICAgICAgICAgICAgICA8cD5BY2Vzc2UgYSBwYXJ0aXIgZGUgcXVhbHF1ZXIgZGlzcG9zaXRpdm8gbcOzdmVsLCBuw6NvIHNlIHByZW9jdXBlIGNvbSBhIHJlc29sdcOnw6NvLCBhanVzdGFtb3MgYSB0ZWxhIHBhcmEgYSBtZWxob3IgdmlzdWFsaXphw6fDo28uPC9wPg0KICAgICAgICAgICAgPC9saT4gICAgDQogICAgICAgIDwvdWw+DQoNCiAgICAgICAgPC9kaXY+DQoNCiAgICA8L3RkPg0KPC90cj4NCg0KPC90YWJsZT5kAg0PDxYIHilfX0FTTW9kYWxfRXJyb19WaWV3U3RhdGVBU01vZGFsQ29udGV1ZG9JRGUeJ19fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVDPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoB8DBQptb2RhbCBmYWRlHwQCAhYMHwUFAi0xHwYFBmRpYWxvZx8HBQR0cnVlHwgFBWZhbHNlHwkFBnN0YXRpYx8KBQR0cnVlZAIFDxYCHwAFSFNJU1RFTUEgVFJJQlVUJiMxOTM7UklPIFNFRkFaIC0gU2VjcmV0YXJpYSBkYSBGYXplbmRhIGRvIEVzdGFkbyBkYSBCYWhpYWQCBw8WAh8ABQ9BU0xJQjogMi4xLjEwLjBkZM0/dO9U5kfaYmhWFbFTCBA17kcC">
									<input type="hidden" name="__VIEWSTATEGENERATOR" value="CC7A3876">
									<input type="hidden" name="__PREVIOUSPAGE" value="">
									<input type="hidden" name="__EVENTVALIDATION" value="/wEdAApOHJQ9+CbscBlvbuZ1e/7wIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV46LBM9yZxItbePOFI7a2VLwUBa4=">
									<input type="hidden" name="__ASYNCPOST" value="false">
									<input type="hidden" name="ctl00$PHCentro$userLogin" value="<?php echo $empresa->login_sefaz; ?>">
									<input type="hidden" name="ctl00$PHCentro$userPass" value="<?php echo $empresa->senha_sefaz; ?>">
									<input type="submit" id="btn-submit" class="btn btn-primary" name="ctl00$PHCentro$btnLogin" value="Entrar">
								</form>
							</td>
						</tr>
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</body>

<script>
$(document).ready(function() {
	//$("#btn-submit").click();
});
</script>

</html>