<?php
session_start();

if(isset($_REQUEST['logout'])){

unlink('cookie.txt');
echo "<script>window.location.href='index.php';</script>";

}


if(isset($_REQUEST['consulta'])){

$usuario = $_REQUEST['usuario'];
$senha = $_REQUEST['senha'];
$cnpj  = $_REQUEST['cnpj'];

if(empty($usuario) || empty($senha) || empty($cnpj)){
 
 echo "<script>alert('Campos são Obrigatorios');</script>";

}else{

?>
<!DOCTYPE html>
<html>
<!--  Desenvolvedor    
      | Eduardo Silva |
      > Especialista em Tecnologia da InformaÃ§Ã£o
      >> suporte@equipe.site
      >>> www.equipe.site -->
<head>
<title>Modulo Consulta DTE</title>
<meta charset='utf-8'>

    <!-- Bootstrap -->
    <link href="/layout/frameworks/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- CSS FONTES -->
    <link href="/layout/frameworks/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
   <!-- jQuery -->
    <script src="/layout/frameworks/jquery/jquery.min.js"></script>
<style>
.table{text-align: center;}
.table>thead>tr>th{text-align: center;}
.label-primary,.label-info{font-size: 13px;}
#lblDescricao,#MenuPerfil,#formCadastroTelefoneCelular,#buorg{display:none !important;}
footer,header,#menu,.breadcrumb{display:none !important;}
</style>
</head>
<body style='font-family:arial;color:#555;font-size: 12px;padding:20px;padding-top:0px;'>
<div style='width:100%;max-width:600px; margin:0 auto'>
<h1><a href='index.php?logout=sim'>Sair desta sessão</a></h1>
<?php



$login= $usuario;
$password = $senha;
$cnpj = $cnpj;
$campos = "";
$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";

/* ACESSO PEGO AS HIDDENS */

$options = array(
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $campos,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER         => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_USERAGENT      => "spider",
        CURLOPT_AUTOREFERER    => true,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT        => 120,
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_SSL_VERIFYPEER => false
    );

    $ch = curl_init($url);
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    curl_close( $ch );
   

$divs = explode('"',$content);


$data = array('__EVENTTARGET'            => '',
              '__EVENTARGUMENT'          => '',
              '__VIEWSTATE'              => $divs['131'],
              '__VIEWSTATEGENERATOR'     => $divs['143'],
              '__PREVIOUSPAGE'           => $divs['151'],
              '__EVENTVALIDATION'        => $divs['159'],
              'ctl00$PHCentro$userLogin' => $login,
              'ctl00$PHCentro$userPass'  => $password,
              'ctl00$PHCentro$btnLogin'  => $divs['387']);





$options = array(
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => http_build_query($data),
        CURLOPT_RETURNTRANSFER => true,     // pagina web
        CURLOPT_HEADER         => false,    // sem headers
        CURLOPT_FOLLOWLOCATION => true,     // com redimensionamento
        CURLOPT_COOKIEJAR      => dirname(__FILE__) . '/cookie.txt',
        CURLOPT_COOKIEFILE     => dirname(__FILE__). '/cookie.txt',
        CURLOPT_COOKIESESSION  => false,
        CURLOPT_ENCODING       => "",       //  encodifico os codigos no envio mão
        CURLOPT_USERAGENT      => "spider", // quem sou?
        CURLOPT_AUTOREFERER    => true,     // refencia de onde vim
        CURLOPT_CONNECTTIMEOUT => 120,      //  tempo conexao
        CURLOPT_TIMEOUT        => 120,      //  tempo de resposta
        CURLOPT_MAXREDIRS      => 10,       // pare depois 10 tentativas
        CURLOPT_SSL_VERIFYPEER => false     // sem SSL por favor
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;




/* PARTE 2 APOS LOGIN*/

$url_two= "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Contribuinte/LeituraCaixaMensagem/LeituraCaixaMensagem.aspx?numeroCnpjCpfCompleto=$cnpj";

 $others = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER         => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_COOKIEJAR      => dirname(__FILE__) . '/cookie.txt',
        CURLOPT_COOKIEFILE     => dirname(__FILE__). '/cookie.txt',
        CURLOPT_ENCODING       => "",
        CURLOPT_USERAGENT      => "spider",
        CURLOPT_AUTOREFERER    => true,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT        => 120,
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_SSL_VERIFYPEER => false
    );

    $ch      = curl_init( $url_two );
    curl_setopt_array( $ch, $others  );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header1['errno']   = $err;
    $header1['errmsg']  = $errmsg;
    $header1['content'] = $content;


echo $header1['content'];

echo "</div></body></html>";
exit;
}
}

?>
<!DOCTYPE html>
<html>
<!--  Desenvolvedor    
      | Eduardo Silva |
      > Especialista em Tecnologia da InformaÃ§Ã£o
      >> suporte@equipe.site
      >>> www.equipe.site -->
<head>
<title>Modulo Consulta DTE</title>
<meta charset='utf-8'>
</head>
<body style='font-family:arial;color:#555;font-size: 12px;padding:20px;padding-top:0px;'>
<!--<form action='' method='POST'>
<table style='font-size: 12px;width: 100%;max-width: 250px;height: 170px;'>
<tr><td>USUÃRIO<br><input type='text' name='usuario' value='6548556500' style='border:1px solid #ddd;padding:8px;border-radius:4px;outline:0;width: 100%;' required></td></tr>
<tr><td>SENHA<br><input type='text' name='senha' value='JBI0291' style='border:1px solid #ddd;padding:8px;border-radius:4px;outline:0;width: 100%;' required></td></tr>
<tr><td>CNPJ<br><input type='text' name='cnpj' value='07.204.158/0001-22' style='border:1px solid #ddd;padding:8px;border-radius:4px;outline:0;width: 100%;' required></td></tr>
<tr><td><input type='submit' name='consulta' value='Consulta' style='width: 100%;background: rgb(84, 145, 199);border: 1px solid;color: #fff;border-radius: 4px;padding: 8px;cursor: pointer;max-width: 100px;outline:0;'></td></tr>
</table>
</form>-->
<a href='http://equipe.site/dte/index.php?consulta=sim&usuario=6548556500&senha=JBI0291&cnpj=07.204.158/0001-22'><input type='submit' name='consulta' value='Consulta' style='width: 100%;background: rgb(84, 145, 199);border: 1px solid;color: #fff;border-radius: 4px;padding: 8px;cursor: pointer;max-width: 100px;outline:0;'></a>
</body>
</html>
