<?php

if(isset($_REQUEST['logout'])){

unlink('cookie.txt');
redirect('empresa/consultageral');

}

if(empty($empresa->login_sefaz) || empty($empresa->senha_sefaz)){
 
 echo "<script>alert('Campos login e senha da Sefaz são obrigatorios');</script>";
 echo "<script>window.location.href='http://localhost/vheri2.2/empresa/consultageral';</script>";
 //redirect('empresa/consultageral');
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Modulo Consulta DTE</title>
<meta charset='utf-8'>

    <!-- Bootstrap -->
    <link href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <!-- CSS FONTES -->
    <link href="<?php echo base_url("assets/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
   <!-- jQuery -->
    <script src="/layout/frameworks/jquery/jquery.min.js"></script>
<style>
.table{text-align: center;}
.table>thead>tr>th{text-align: center;}
.label-primary,.label-info{font-size: 13px;}
#lblDescricao,#MenuPerfil,#formCadastroTelefoneCelular,#buorg{display:none !important;}
footer,header,#menu,.breadcrumb{display:none !important;}
</style>
</head>
<body style='font-family:arial;color:#555;font-size: 12px;padding:20px;padding-top:0px;'>
<div style='width:100%;max-width:600px; margin:0 auto'>
<h1><a href='consultardte.php?logout=sim'>Sair desta sessão</a></h1>
<?php



$login= $empresa->login_sefaz;
$password = $empresa->senha_sefaz;
$cnpj = $empresa->cnpj_completo;
$campos = "";
$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";

/* ACESSO PEGO AS HIDDENS */

$options = array(
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $campos,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER         => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_USERAGENT      => "spider",
        CURLOPT_AUTOREFERER    => true,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT        => 120,
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_SSL_VERIFYPEER => false
    );

    $ch = curl_init($url);
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    curl_close( $ch );
   

$divs = explode('"',$content);


$data = array('__EVENTTARGET'            => '',
              '__EVENTARGUMENT'          => '',
              '__VIEWSTATE'              => $divs['131'],
              '__VIEWSTATEGENERATOR'     => $divs['143'],
              '__PREVIOUSPAGE'           => $divs['151'],
              '__EVENTVALIDATION'        => $divs['159'],
              'ctl00$PHCentro$userLogin' => $login,
              'ctl00$PHCentro$userPass'  => $password,
              'ctl00$PHCentro$btnLogin'  => $divs['387']);





$options = array(
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => http_build_query($data),
        CURLOPT_RETURNTRANSFER => true,     // pagina web
        CURLOPT_HEADER         => false,    // sem headers
        CURLOPT_FOLLOWLOCATION => true,     // com redimensionamento
        CURLOPT_COOKIEJAR      => dirname(__FILE__) . '/cookie.txt',
        CURLOPT_COOKIEFILE     => dirname(__FILE__). '/cookie.txt',
        CURLOPT_COOKIESESSION  => false,
        CURLOPT_ENCODING       => "",       //  encodifico os codigos no envio mão
        CURLOPT_USERAGENT      => "spider", // quem sou?
        CURLOPT_AUTOREFERER    => true,     // refencia de onde vim
        CURLOPT_CONNECTTIMEOUT => 120,      //  tempo conexao
        CURLOPT_TIMEOUT        => 120,      //  tempo de resposta
        CURLOPT_MAXREDIRS      => 10,       // pare depois 10 tentativas
        CURLOPT_SSL_VERIFYPEER => false     // sem SSL por favor
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;




/* PARTE 2 APOS LOGIN*/

$url_two= "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Contribuinte/LeituraCaixaMensagem/LeituraCaixaMensagem.aspx?numeroCnpjCpfCompleto=$cnpj";

 $others = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER         => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_COOKIEJAR      => dirname(__FILE__) . '/cookie.txt',
        CURLOPT_COOKIEFILE     => dirname(__FILE__). '/cookie.txt',
        CURLOPT_ENCODING       => "",
        CURLOPT_USERAGENT      => "spider",
        CURLOPT_AUTOREFERER    => true,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT        => 120,
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_SSL_VERIFYPEER => false
    );

    $ch      = curl_init( $url_two );
    curl_setopt_array( $ch, $others  );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header1['errno']   = $err;
    $header1['errmsg']  = $errmsg;
    $header1['content'] = $content;


echo $header1['content'];

echo "</div></body></html>";
exit;

?>
