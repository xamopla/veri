<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">
<style type="text/css">
    #example_wrapper{
      width: 100% !important;
    }
</style>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Empresas com Senhas DTE Inválidas</h5>
          </div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="example" data-toggle="datatables">
                    <thead class="thead-default thead-lg">
                        <tr  id="passo11">
                            <th style="font-size: 12px;">Razão Social</th>
                            <th style="font-size: 12px;">Nome Fantasia</th>
                            <th style="font-size: 12px;">CNPJ</th>
                            <th style="font-size: 12px;">Inscrição Estadual</th>
                            <th style="font-size: 12px;">Conta DTE</th>
                            <th style="font-size: 12px;">Situação Cadastral</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ($consulta){ 
                        foreach ($consulta as $e){?>
                        <tr>
                              <td style="font-size: 12px;"><?php echo $e->razao_social; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->nome_fantasia; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->cnpj_completo; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->inscricao_estadual_completo; ?></td>
                              
                              <td style="font-size: 12px; text-align: center;">
                              <?php if ($e->situacao_conta_dte) { ?>
                              <span <?php if ($e->situacao_conta_dte == 'INEXISTENTE'){echo "class='badge badge-danger badge-shadow'";} elseif ($e->situacao_conta_dte == 'INATIVA') {echo "class='badge badge-warning badge-shadow'";} else {echo "class='badge badge-success badge-shadow' style='background-color:#2ecc71!important;'";}?> data-container='body' data-toggle='popover' data-trigger="hover" data-placement='top' title='Situação DTE' data-content="<?php echo $e->situacao_dte; ?>">
                                <?php echo $e->situacao_conta_dte; ?>
                              </span>
                              <?php } ?>
                              </td>

                              <td style="font-size: 12px; text-align: center;">
                              <?php
                              
                              if ($e->situacao_cadastral == 'ATIVO' || $e->situacao_cadastral == 'ATIVA'){
                                echo "<span class='badge badge-success badge-shadow' data-container='body' style='background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo desta Situação Cadastral' data-content='$e->motivo_situacao_cadastral'>$e->situacao_cadastral</span>";
                              } else if ($e->situacao_cadastral == 'BAIXADO'){
                                echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo desta Situação Cadastral' data-content='$e->motivo_situacao_cadastral'>$e->situacao_cadastral</span>";
                              } else {
                                echo "<span class='badge badge-warning badge-shadow'  data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Motivo desta Situação Cadastral' data-content='$e->motivo_situacao_cadastral'>$e->situacao_cadastral</span>";
                              }
                              
                              ?>
                              </td>

                              <td>    
                                <a href="<?php echo base_url("verificador_senhas/alterar_senha_sefaz/$e->id"); ?>" data-toggle="tooltip" data-placement="top" title="Alterar Senha" class="btn btn-default b"><i class="bc-share-def bc-alterar-senha" aria-hidden="true"></i></a>   
                              </td>
                        </tr>
                    <?php } 
                    }
                    ?>
                    </tbody>
                </table>

            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>


<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->
<script src="<?php echo base_url('assets/js/page/toastr.js'); ?>"></script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
        toastr["success"]("O Processo foi cadastrado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["error"]("Ocorreu um erro ao cadastrar o processo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["success"]("O processo foi editado com sucesso!", "Editado!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["error"]("Ocorreu um erro ao editar o processo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 5) { ?>
        toastr["success"]("O processo foi excluído com sucesso!", "Excluído!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 6) { ?>
        toastr["error"]("Ocorreu um erro ao excluir o processo, por favor tente novamente.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 7) { ?>
        toastr["error"]("Processo já cadastrado.", "Erro!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 10) { ?>
        toastr["error"]("Processo não encontrado na Sefaz.", "Erro!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
$('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );



    $(document).on('click', '.editar', function(event) {
      $("#idEditar").val($(this).attr('idLancamento'));
      $("#protocoloEditarValor").val($(this).attr('protocoloEditar'));
      $("#urlAtualEditar").val($(location).attr('href'));
    }); 

});

$(".select2_demo_1").select2();

</script>

<script type="text/javascript">
    function openModalDocumento(id_tramitacao_processo_sipro) {
        $.ajax({
            type: "GET",
            url: 'consulta_sipro/documento_modal?id_tramitacao_processo_sipro='+id_tramitacao_processo_sipro,
            success: function (data) {
                $("#modal-div-new-entity").html(data);
            },
        });
        return false;
    }
</script>