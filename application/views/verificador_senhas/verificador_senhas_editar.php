<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Alterar Senha DTE</h2>
              </div>
              <div class="card-body">
                <div class="ibox-body">                         
                    <br>
                    <div class="row">                                
                        <div class="col-md-6">
                            <div class="form-group mb-4">
                                <label for="nome">Empresa:</label>
                                <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->razao_social, 'readonly'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group mb-4">
                                <label for="nome">CNPJ:</label>
                                <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->cnpj_completo, 'readonly'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group mb-4">
                                <label for="nome">Inscrição Estadual:</label>
                                <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual'), $empresa->inscricao_estadual_completo, 'readonly'); ?>
                            </div>
                        </div>
                      </div>

                      <br>

                      <div>
                        <div class="text-center" >
                        <h1 style="font-size: 28px;">Senha</h1> 
                        </div>
                      </div>

                      <br>
                      
                     <div class="row">     
                        <div class="col-md-3">
                            <div class="form-group mb-4">    
                                <label for="nome">Situação da Senha:</label>               
                                    <?php if ($status_senha->valor != 1): ?>
                                        <?php if($empresa->senha_sefaz == null && $empresa->login_sefaz == null): ?> 
                                            <input type="text" value="Vazia" class="form-control" style="background-color: red;color:white;" readonly=""> 
                                        <?php else: ?>
                                            <input type="text" value="Correta" class="form-control" style="background-color: green;color:white;" readonly=""> 
                                        <?php endif ?> 
                                <?php else: ?>
                                    <input type="text" value="Errada" class="form-control" style="background-color: red;color:white;" readonly=""> 
                                <?php endif ?>   
                            </div>
                        </div>                               
                        <div class="col-md-6">
                           <div class="form-group mb-4" style="display: inline-grid;">
                                <label>Alterar Senha</label>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fullCalModal">
                                    <span class="btn-icon"><i class="la la-cog"></i>Configurar</span>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>  
                <div class="ibox-footer text-right">                            
                    <a href="<?php echo base_url("verificador_senhas"); ?>" class="btn btn-primary">Voltar</a>
                </div>
                <?php echo form_close(); ?>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="fullCalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Alterar Senha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="certificado_form" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-4">
                            <label>Login Sefaz:</label>
                            <input type="text" name="login_sefaz" class="form-control" style="border-color: #5c6bc0;"/>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group mb-4">
                            <label>Senha Sefaz:</label>
                            <input type="text" name="senha_sefaz" class="form-control" style="border-color: #5c6bc0;" />
                        </div>
                    </div>               
                    <input type="text" name="id_empresa" value="<?php echo $empresa->id ?>" hidden/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelarModalA1">Cancelar</button>
                <button type="button" class="btn btn-success" onclick="submit_certificado();">Alterar Senha</button>       

                <script type="text/javascript">

                    function submit_certificado(){
                        var form = $('#certificado_form')[0];
                        var data = new FormData(form);

                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: "<?= base_url() ?>verificador_senhas/testar_senha", /*Alterar aqui*/
                            data: data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            timeout: 600000,
                            success: function (data) {
                                var jsonResponse = JSON.parse(data);
                                if(jsonResponse["error"]){
                                    swal_certificado_error(jsonResponse["msg"]);
                                }else{
                                    swal_certificado_success(jsonResponse["msg"]);
                                }
                            },
                            error: function (e) {
                                alert("error");
                            }
                        });
                    }

                    function swal_certificado_success($msg){
                        document.getElementById('btnCancelarModalA1').click();
                        $(document).ready(function(){
                            swal({ 
                                title: 'Senha Alterada!',
                                text: $msg,
                                type: "success" 
                            },
                            function(){
                                document.location.reload(true);
                            });
                        });
                    }

                    function swal_certificado_error($msg){
                        document.getElementById('btnCancelarModalA1').click();
                        $(document).ready(function() {
                            swal({ 
                                title: "Ops, Algo Aconteceu!",
                                text: $msg,
                                type: "error" 
                            },
                            function(){
                                $('#fullCalModal').modal('show'); 
                            });
                    });
                    }
                </script>

                <style type="text/css">
                    .sweet-alert .sa-icon {
                        margin-bottom: 40px;
                    }
                </style>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A senha foi alterada com sucesso!", "Senha Alterada!");
    <?php } ?>
</script>

<script type="text/javascript">

$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>