<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/
</style>

<style type="text/css">
  .card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details {
    position: absolute;
    top: 0;
    left: 0;
    width: 70%;/*100%*/
    height: 100%;
    transition: .5s;
    transform-origin: left;
    transform: perspective(2000px) rotateY(-90deg);
}
.card:hover .details {
    transform: perspective(2000px) rotateY(0deg);
}
.card .details .center {
    width: 100% !important;
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details .center p {
    margin: 10px 0;
    padding: 0;
    color: #262626;
}
.card .details .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details .center ul li a:hover {
    background: #ff3636;
}


.div_hover { 
  background: white;
  border-radius: 50px; 
}

.div_hover a:hover { 
  background-color: #24a0e9; 
  color: white !important;
}

.tag_a_hover {
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.span_hover:hover{
  background-color: #24a0e9; 
  color: white !important;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.media-body{
  padding-left: 0px !important; 
}
</style>

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Certidões</h2> 
        </div> 
      </div>
    </div>
  </div>

<div class="container-fluid general-widget">

  <div class="row">
        <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #03a9f3">ESTADUAIS</h5></div>

        <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #03a9f3">FEDERAIS</h5></div>

        <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #03a9f3">MUNICIPAIS</h5></div>
      </div>

  <div class="row">

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certidao_negativa/listar_estadual"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">CND Estadual</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certidao_negativa/listar_estadual/Regulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php 
                        if($qtd_estadual_regular){echo $qtd_estadual_regular;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                      <a href="<?php echo base_url("certidao_negativa/listar_estadual/Irregulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Alerta: <?php 
                        if($qtd_estadual_irregular){echo $qtd_estadual_irregular;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </p>
              </div>
            </div>


            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">CND Estadual - Irregular</span>
              <h4 class="mb-0" style="color: white"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Certidões Estaduais Irregulares ' data-original-title='' title=''><?php 
              if($qtd_estadual_irregular){echo $qtd_estadual_irregular;}else{
                echo "0";
              }  ?></div></h4>

              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M985 5106 c-141 -45 -243 -146 -285 -284 -20 -63 -20 -101 -20 -2098 l0 -2034 -299 0 c-303 0 -342 -4 -367 -38 -21 -27 -19 -294 3 -358 43 -130 148 -235 278 -278 47 -15 200 -16 1940 -14 l1890 3 65 31 c119 57 193 142 230 262 20 63 20 101 20 2098 l0 2034 299 0 c303 0 342 4 367 38 21 27 19 294 -3 358 -32 96 -93 174 -178 229 -110 70 57 65 -2024 64 -1486 0 -1884 -3 -1916 -13z m3345 -171 c0 -3 -11 -27 -24 -53 -13 -26 -29 -74 -35 -107 -8 -41 -11 -707 -11 -2229 l0 -2171 -24 -50 c-92 -195 -367 -192 -455 5 -10 23 -17 81 -21 172 -5 126 -7 139 -28 160 l-23 23 -1424 3 -1425 2 0 2028 c0 2018 1 2027 21 2072 27 60 64 98 124 126 l50 24 1638 0 c900 0 1637 -2 1637 -5z m444 -6 c52 -14 141 -103 155 -155 6 -21 11 -67 11 -101 l0 -63 -251 0 -252 0 6 75 c14 186 157 292 331 244z m-1194 -4493 c0 -77 26 -184 54 -221 9 -11 16 -23 16 -27 0 -13 -3259 -10 -3304 3 -52 14 -141 103 -155 155 -6 21 -11 67 -11 102 l0 62 1700 0 1700 0 0 -74z" style="fill: white;"/>
              <path d="M1960 4180 l0 -90 560 0 560 0 0 90 0 90 -560 0 -560 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3670 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3160 l0 -90 945 0 945 0 0 90 0 90 -945 0 -945 0 0 -90z" style="fill: white;"/>
              <path d="M3410 3160 l0 -90 260 0 260 0 0 90 0 90 -260 0 -260 0 0 -90z" style="fill: white;"/>
              <path d="M1190 2650 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M3330 2303 c-101 -17 -208 -74 -287 -154 -93 -94 -143 -217 -143 -354 0 -137 41 -247 130 -348 l40 -45 0 -251 0 -252 25 -24 c42 -43 78 -35 206 49 l112 74 111 -74 c113 -75 141 -85 189 -63 43 19 47 45 47 300 l0 240 46 57 c83 101 124 213 124 337 0 232 -172 446 -402 500 -60 14 -143 17 -198 8z m220 -209 c213 -96 263 -371 98 -536 -63 -64 -127 -92 -218 -96 -105 -5 -176 22 -251 97 -260 260 35 686 371 535z m-135 -809 c61 0 122 3 138 8 l27 7 0 -95 0 -96 -42 29 c-62 42 -104 62 -130 62 -13 0 -54 -20 -91 -45 l-67 -45 0 95 0 95 28 -7 c15 -5 77 -8 137 -8z" style="fill: white;"/>
              <path d="M1190 2130 l0 -90 515 0 515 0 0 90 0 90 -515 0 -515 0 0 -90z" style="fill: white;"/>
              <path d="M1190 1710 l0 -90 220 0 220 0 0 90 0 90 -220 0 -220 0 0 -90z" style="fill: white;"/>
              <path d="M1960 1710 l0 -90 130 0 130 0 0 90 0 90 -130 0 -130 0 0 -90z" style="fill: white;"/>
              </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certidao_negativa/listar_trabalhista"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">CND Trabalhista</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certidao_negativa/listar_trabalhista/Regulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php 
                        if($qtd_trabalhista_regular){echo $qtd_trabalhista_regular;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                      <a href="<?php echo base_url("certidao_negativa/listar_trabalhista/Irregulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Irregulares: <?php 
                        if($qtd_trabalhista_irregular){echo $qtd_trabalhista_irregular;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">CND Trabalhista - Irregular</span>
              <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Certidões Trabalhistas Irregulares' data-original-title='' title=''><?php 
              if($qtd_trabalhista_irregular){echo $qtd_trabalhista_irregular;}else{
                echo "0";
              }  ?></div></h4>

              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M985 5106 c-141 -45 -243 -146 -285 -284 -20 -63 -20 -101 -20 -2098 l0 -2034 -299 0 c-303 0 -342 -4 -367 -38 -21 -27 -19 -294 3 -358 43 -130 148 -235 278 -278 47 -15 200 -16 1940 -14 l1890 3 65 31 c119 57 193 142 230 262 20 63 20 101 20 2098 l0 2034 299 0 c303 0 342 4 367 38 21 27 19 294 -3 358 -32 96 -93 174 -178 229 -110 70 57 65 -2024 64 -1486 0 -1884 -3 -1916 -13z m3345 -171 c0 -3 -11 -27 -24 -53 -13 -26 -29 -74 -35 -107 -8 -41 -11 -707 -11 -2229 l0 -2171 -24 -50 c-92 -195 -367 -192 -455 5 -10 23 -17 81 -21 172 -5 126 -7 139 -28 160 l-23 23 -1424 3 -1425 2 0 2028 c0 2018 1 2027 21 2072 27 60 64 98 124 126 l50 24 1638 0 c900 0 1637 -2 1637 -5z m444 -6 c52 -14 141 -103 155 -155 6 -21 11 -67 11 -101 l0 -63 -251 0 -252 0 6 75 c14 186 157 292 331 244z m-1194 -4493 c0 -77 26 -184 54 -221 9 -11 16 -23 16 -27 0 -13 -3259 -10 -3304 3 -52 14 -141 103 -155 155 -6 21 -11 67 -11 102 l0 62 1700 0 1700 0 0 -74z" style="fill: white;"/>
              <path d="M1960 4180 l0 -90 560 0 560 0 0 90 0 90 -560 0 -560 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3670 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3160 l0 -90 945 0 945 0 0 90 0 90 -945 0 -945 0 0 -90z" style="fill: white;"/>
              <path d="M3410 3160 l0 -90 260 0 260 0 0 90 0 90 -260 0 -260 0 0 -90z" style="fill: white;"/>
              <path d="M1190 2650 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M3330 2303 c-101 -17 -208 -74 -287 -154 -93 -94 -143 -217 -143 -354 0 -137 41 -247 130 -348 l40 -45 0 -251 0 -252 25 -24 c42 -43 78 -35 206 49 l112 74 111 -74 c113 -75 141 -85 189 -63 43 19 47 45 47 300 l0 240 46 57 c83 101 124 213 124 337 0 232 -172 446 -402 500 -60 14 -143 17 -198 8z m220 -209 c213 -96 263 -371 98 -536 -63 -64 -127 -92 -218 -96 -105 -5 -176 22 -251 97 -260 260 35 686 371 535z m-135 -809 c61 0 122 3 138 8 l27 7 0 -95 0 -96 -42 29 c-62 42 -104 62 -130 62 -13 0 -54 -20 -91 -45 l-67 -45 0 95 0 95 28 -7 c15 -5 77 -8 137 -8z" style="fill: white;"/>
              <path d="M1190 2130 l0 -90 515 0 515 0 0 90 0 90 -515 0 -515 0 0 -90z" style="fill: white;"/>
              <path d="M1190 1710 l0 -90 220 0 220 0 0 90 0 90 -220 0 -220 0 0 -90z" style="fill: white;"/>
              <path d="M1960 1710 l0 -90 130 0 130 0 0 90 0 90 -130 0 -130 0 0 -90z" style="fill: white;"/>
              </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certidao_negativa/listar_federal_pgfn"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
               <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">CND RFB/PGFN</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certidao_negativa/listar_federal_pgfn/Regulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php 
                        if($qtd_pgfn_regular){echo $qtd_pgfn_regular;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                      <a href="<?php echo base_url("certidao_negativa/listar_federal_pgfn/Irregulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Irregulares: <?php 
                        if($qtd_pgfn_irregular){echo $qtd_pgfn_irregular;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">CND RFB/PGFN - Irregular</span>
              <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Certidões da PGFN Irregulares' data-original-title='' title=''><?php 
              if($qtd_pgfn_irregular){echo $qtd_pgfn_irregular;}else{
                echo "0";
              }  ?></div></h4>

              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M985 5106 c-141 -45 -243 -146 -285 -284 -20 -63 -20 -101 -20 -2098 l0 -2034 -299 0 c-303 0 -342 -4 -367 -38 -21 -27 -19 -294 3 -358 43 -130 148 -235 278 -278 47 -15 200 -16 1940 -14 l1890 3 65 31 c119 57 193 142 230 262 20 63 20 101 20 2098 l0 2034 299 0 c303 0 342 4 367 38 21 27 19 294 -3 358 -32 96 -93 174 -178 229 -110 70 57 65 -2024 64 -1486 0 -1884 -3 -1916 -13z m3345 -171 c0 -3 -11 -27 -24 -53 -13 -26 -29 -74 -35 -107 -8 -41 -11 -707 -11 -2229 l0 -2171 -24 -50 c-92 -195 -367 -192 -455 5 -10 23 -17 81 -21 172 -5 126 -7 139 -28 160 l-23 23 -1424 3 -1425 2 0 2028 c0 2018 1 2027 21 2072 27 60 64 98 124 126 l50 24 1638 0 c900 0 1637 -2 1637 -5z m444 -6 c52 -14 141 -103 155 -155 6 -21 11 -67 11 -101 l0 -63 -251 0 -252 0 6 75 c14 186 157 292 331 244z m-1194 -4493 c0 -77 26 -184 54 -221 9 -11 16 -23 16 -27 0 -13 -3259 -10 -3304 3 -52 14 -141 103 -155 155 -6 21 -11 67 -11 102 l0 62 1700 0 1700 0 0 -74z" style="fill: white;"/>
              <path d="M1960 4180 l0 -90 560 0 560 0 0 90 0 90 -560 0 -560 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3670 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3160 l0 -90 945 0 945 0 0 90 0 90 -945 0 -945 0 0 -90z" style="fill: white;"/>
              <path d="M3410 3160 l0 -90 260 0 260 0 0 90 0 90 -260 0 -260 0 0 -90z" style="fill: white;"/>
              <path d="M1190 2650 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M3330 2303 c-101 -17 -208 -74 -287 -154 -93 -94 -143 -217 -143 -354 0 -137 41 -247 130 -348 l40 -45 0 -251 0 -252 25 -24 c42 -43 78 -35 206 49 l112 74 111 -74 c113 -75 141 -85 189 -63 43 19 47 45 47 300 l0 240 46 57 c83 101 124 213 124 337 0 232 -172 446 -402 500 -60 14 -143 17 -198 8z m220 -209 c213 -96 263 -371 98 -536 -63 -64 -127 -92 -218 -96 -105 -5 -176 22 -251 97 -260 260 35 686 371 535z m-135 -809 c61 0 122 3 138 8 l27 7 0 -95 0 -96 -42 29 c-62 42 -104 62 -130 62 -13 0 -54 -20 -91 -45 l-67 -45 0 95 0 95 28 -7 c15 -5 77 -8 137 -8z" style="fill: white;"/>
              <path d="M1190 2130 l0 -90 515 0 515 0 0 90 0 90 -515 0 -515 0 0 -90z" style="fill: white;"/>
              <path d="M1190 1710 l0 -90 220 0 220 0 0 90 0 90 -220 0 -220 0 0 -90z" style="fill: white;"/>
              <path d="M1960 1710 l0 -90 130 0 130 0 0 90 0 90 -130 0 -130 0 0 -90z" style="fill: white;"/>
              </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certidao_negativa/listar_municipal"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">CND Municipal</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certidao_negativa/listar_municipal/Regulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php 
                        if($qtd_municipal_regular){echo $qtd_municipal_regular;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                      <a href="<?php echo base_url("certidao_negativa/listar_municipal/Irregulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Irregulares: <?php 
                        if($qtd_municipal_irregular){echo $qtd_municipal_irregular;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">CND Municipal - Irregular</span>
              <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Certidões Municipais Irregulares' data-original-title='' title=''><?php 
              if($qtd_municipal_irregular){echo $qtd_municipal_irregular;}else{
                echo "0";
              }  ?></div></h4>

              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M985 5106 c-141 -45 -243 -146 -285 -284 -20 -63 -20 -101 -20 -2098 l0 -2034 -299 0 c-303 0 -342 -4 -367 -38 -21 -27 -19 -294 3 -358 43 -130 148 -235 278 -278 47 -15 200 -16 1940 -14 l1890 3 65 31 c119 57 193 142 230 262 20 63 20 101 20 2098 l0 2034 299 0 c303 0 342 4 367 38 21 27 19 294 -3 358 -32 96 -93 174 -178 229 -110 70 57 65 -2024 64 -1486 0 -1884 -3 -1916 -13z m3345 -171 c0 -3 -11 -27 -24 -53 -13 -26 -29 -74 -35 -107 -8 -41 -11 -707 -11 -2229 l0 -2171 -24 -50 c-92 -195 -367 -192 -455 5 -10 23 -17 81 -21 172 -5 126 -7 139 -28 160 l-23 23 -1424 3 -1425 2 0 2028 c0 2018 1 2027 21 2072 27 60 64 98 124 126 l50 24 1638 0 c900 0 1637 -2 1637 -5z m444 -6 c52 -14 141 -103 155 -155 6 -21 11 -67 11 -101 l0 -63 -251 0 -252 0 6 75 c14 186 157 292 331 244z m-1194 -4493 c0 -77 26 -184 54 -221 9 -11 16 -23 16 -27 0 -13 -3259 -10 -3304 3 -52 14 -141 103 -155 155 -6 21 -11 67 -11 102 l0 62 1700 0 1700 0 0 -74z" style="fill: white;"/>
              <path d="M1960 4180 l0 -90 560 0 560 0 0 90 0 90 -560 0 -560 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3670 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3160 l0 -90 945 0 945 0 0 90 0 90 -945 0 -945 0 0 -90z" style="fill: white;"/>
              <path d="M3410 3160 l0 -90 260 0 260 0 0 90 0 90 -260 0 -260 0 0 -90z" style="fill: white;"/>
              <path d="M1190 2650 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M3330 2303 c-101 -17 -208 -74 -287 -154 -93 -94 -143 -217 -143 -354 0 -137 41 -247 130 -348 l40 -45 0 -251 0 -252 25 -24 c42 -43 78 -35 206 49 l112 74 111 -74 c113 -75 141 -85 189 -63 43 19 47 45 47 300 l0 240 46 57 c83 101 124 213 124 337 0 232 -172 446 -402 500 -60 14 -143 17 -198 8z m220 -209 c213 -96 263 -371 98 -536 -63 -64 -127 -92 -218 -96 -105 -5 -176 22 -251 97 -260 260 35 686 371 535z m-135 -809 c61 0 122 3 138 8 l27 7 0 -95 0 -96 -42 29 c-62 42 -104 62 -130 62 -13 0 -54 -20 -91 -45 l-67 -45 0 95 0 95 28 -7 c15 -5 77 -8 137 -8z" style="fill: white;"/>
              <path d="M1190 2130 l0 -90 515 0 515 0 0 90 0 90 -515 0 -515 0 0 -90z" style="fill: white;"/>
              <path d="M1190 1710 l0 -90 220 0 220 0 0 90 0 90 -220 0 -220 0 0 -90z" style="fill: white;"/>
              <path d="M1960 1710 l0 -90 130 0 130 0 0 90 0 90 -130 0 -130 0 0 -90z" style="fill: white;"/>
              </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div>
    
  </div>

  <div class="row">
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certidao_negativa/listar_caixa"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">CND FGTS</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certidao_negativa/listar_caixa/Regulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php 
                        if($qtd_fgts_regular){echo $qtd_fgts_regular;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                      <a href="<?php echo base_url("certidao_negativa/listar_caixa/Irregulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Irregulares: <?php 
                        if($qtd_fgts_irregular){echo $qtd_fgts_irregular;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">CND FGTS - Irregular </span>
              <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Certidões do FGTS Irregulares' data-original-title='' title=''><?php 
              if($qtd_fgts_irregular){echo $qtd_fgts_irregular;}else{
                echo "0";
              }  ?></div></h4>

              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M985 5106 c-141 -45 -243 -146 -285 -284 -20 -63 -20 -101 -20 -2098 l0 -2034 -299 0 c-303 0 -342 -4 -367 -38 -21 -27 -19 -294 3 -358 43 -130 148 -235 278 -278 47 -15 200 -16 1940 -14 l1890 3 65 31 c119 57 193 142 230 262 20 63 20 101 20 2098 l0 2034 299 0 c303 0 342 4 367 38 21 27 19 294 -3 358 -32 96 -93 174 -178 229 -110 70 57 65 -2024 64 -1486 0 -1884 -3 -1916 -13z m3345 -171 c0 -3 -11 -27 -24 -53 -13 -26 -29 -74 -35 -107 -8 -41 -11 -707 -11 -2229 l0 -2171 -24 -50 c-92 -195 -367 -192 -455 5 -10 23 -17 81 -21 172 -5 126 -7 139 -28 160 l-23 23 -1424 3 -1425 2 0 2028 c0 2018 1 2027 21 2072 27 60 64 98 124 126 l50 24 1638 0 c900 0 1637 -2 1637 -5z m444 -6 c52 -14 141 -103 155 -155 6 -21 11 -67 11 -101 l0 -63 -251 0 -252 0 6 75 c14 186 157 292 331 244z m-1194 -4493 c0 -77 26 -184 54 -221 9 -11 16 -23 16 -27 0 -13 -3259 -10 -3304 3 -52 14 -141 103 -155 155 -6 21 -11 67 -11 102 l0 62 1700 0 1700 0 0 -74z" style="fill: white;"/>
              <path d="M1960 4180 l0 -90 560 0 560 0 0 90 0 90 -560 0 -560 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3670 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3160 l0 -90 945 0 945 0 0 90 0 90 -945 0 -945 0 0 -90z" style="fill: white;"/>
              <path d="M3410 3160 l0 -90 260 0 260 0 0 90 0 90 -260 0 -260 0 0 -90z" style="fill: white;"/>
              <path d="M1190 2650 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M3330 2303 c-101 -17 -208 -74 -287 -154 -93 -94 -143 -217 -143 -354 0 -137 41 -247 130 -348 l40 -45 0 -251 0 -252 25 -24 c42 -43 78 -35 206 49 l112 74 111 -74 c113 -75 141 -85 189 -63 43 19 47 45 47 300 l0 240 46 57 c83 101 124 213 124 337 0 232 -172 446 -402 500 -60 14 -143 17 -198 8z m220 -209 c213 -96 263 -371 98 -536 -63 -64 -127 -92 -218 -96 -105 -5 -176 22 -251 97 -260 260 35 686 371 535z m-135 -809 c61 0 122 3 138 8 l27 7 0 -95 0 -96 -42 29 c-62 42 -104 62 -130 62 -13 0 -54 -20 -91 -45 l-67 -45 0 95 0 95 28 -7 c15 -5 77 -8 137 -8z" style="fill: white;"/>
              <path d="M1190 2130 l0 -90 515 0 515 0 0 90 0 90 -515 0 -515 0 0 -90z" style="fill: white;"/>
              <path d="M1190 1710 l0 -90 220 0 220 0 0 90 0 90 -220 0 -220 0 0 -90z" style="fill: white;"/>
              <path d="M1960 1710 l0 -90 130 0 130 0 0 90 0 90 -130 0 -130 0 0 -90z" style="fill: white;"/>
              </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certidao_negativa/listar_previdenciaria"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">CND Previdenciária</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certidao_negativa/listar_previdenciaria/Regulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php 
                        if($qtd_previdenciaria_regular){echo $qtd_previdenciaria_regular;}else{
                          echo "0";
                        }  ?>
                          
                        </a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                      <a href="<?php echo base_url("certidao_negativa/listar_previdenciaria/Irregulares"); ?>" style="font-size: 18px;" class="tag_a_hover">Irregulares: <?php 
                        if($qtd_previdenciaria_irregular){echo $qtd_previdenciaria_irregular;}else{
                          echo "0";
                        }  ?>
                        
                      </a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" style="padding-left: 15px !important;"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">CND Previdenciária - Irregular </span>
              <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Certidões do FGTS Irregulares' data-original-title='' title=''><?php 
              if($qtd_previdenciaria_irregular){echo $qtd_previdenciaria_irregular;}else{
                echo "0";
              }  ?></div></h4>

              <svg xmlns="http://www.w3.org/2000/svg" class="icon-bg" version="1.0" width="512.000000pt" height="512.000000pt" viewBox="0 0 512.000000 512.000000" preserveAspectRatio="xMidYMid meet">
              <metadata>
              </metadata>
              <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
              <path d="M985 5106 c-141 -45 -243 -146 -285 -284 -20 -63 -20 -101 -20 -2098 l0 -2034 -299 0 c-303 0 -342 -4 -367 -38 -21 -27 -19 -294 3 -358 43 -130 148 -235 278 -278 47 -15 200 -16 1940 -14 l1890 3 65 31 c119 57 193 142 230 262 20 63 20 101 20 2098 l0 2034 299 0 c303 0 342 4 367 38 21 27 19 294 -3 358 -32 96 -93 174 -178 229 -110 70 57 65 -2024 64 -1486 0 -1884 -3 -1916 -13z m3345 -171 c0 -3 -11 -27 -24 -53 -13 -26 -29 -74 -35 -107 -8 -41 -11 -707 -11 -2229 l0 -2171 -24 -50 c-92 -195 -367 -192 -455 5 -10 23 -17 81 -21 172 -5 126 -7 139 -28 160 l-23 23 -1424 3 -1425 2 0 2028 c0 2018 1 2027 21 2072 27 60 64 98 124 126 l50 24 1638 0 c900 0 1637 -2 1637 -5z m444 -6 c52 -14 141 -103 155 -155 6 -21 11 -67 11 -101 l0 -63 -251 0 -252 0 6 75 c14 186 157 292 331 244z m-1194 -4493 c0 -77 26 -184 54 -221 9 -11 16 -23 16 -27 0 -13 -3259 -10 -3304 3 -52 14 -141 103 -155 155 -6 21 -11 67 -11 102 l0 62 1700 0 1700 0 0 -74z" style="fill: white;"/>
              <path d="M1960 4180 l0 -90 560 0 560 0 0 90 0 90 -560 0 -560 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3670 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M1190 3160 l0 -90 945 0 945 0 0 90 0 90 -945 0 -945 0 0 -90z" style="fill: white;"/>
              <path d="M3410 3160 l0 -90 260 0 260 0 0 90 0 90 -260 0 -260 0 0 -90z" style="fill: white;"/>
              <path d="M1190 2650 l0 -90 1370 0 1370 0 0 90 0 90 -1370 0 -1370 0 0 -90z" style="fill: white;"/>
              <path d="M3330 2303 c-101 -17 -208 -74 -287 -154 -93 -94 -143 -217 -143 -354 0 -137 41 -247 130 -348 l40 -45 0 -251 0 -252 25 -24 c42 -43 78 -35 206 49 l112 74 111 -74 c113 -75 141 -85 189 -63 43 19 47 45 47 300 l0 240 46 57 c83 101 124 213 124 337 0 232 -172 446 -402 500 -60 14 -143 17 -198 8z m220 -209 c213 -96 263 -371 98 -536 -63 -64 -127 -92 -218 -96 -105 -5 -176 22 -251 97 -260 260 35 686 371 535z m-135 -809 c61 0 122 3 138 8 l27 7 0 -95 0 -96 -42 29 c-62 42 -104 62 -130 62 -13 0 -54 -20 -91 -45 l-67 -45 0 95 0 95 28 -7 c15 -5 77 -8 137 -8z" style="fill: white;"/>
              <path d="M1190 2130 l0 -90 515 0 515 0 0 90 0 90 -515 0 -515 0 0 -90z" style="fill: white;"/>
              <path d="M1190 1710 l0 -90 220 0 220 0 0 90 0 90 -220 0 -220 0 0 -90z" style="fill: white;"/>
              <path d="M1960 1710 l0 -90 130 0 130 0 0 90 0 90 -130 0 -130 0 0 -90z" style="fill: white;"/>
              </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      </a>
    </div> -->
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      
    </div>


  </div>

    </div>

  </div>
</div> 

<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->

<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->


<script type="text/javascript">
  $(document).ready(function() {
    

      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>