<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
 
 <style type="text/css">

.swal2-styled{
    background-color: #dc2727 !important;
    display: none !important;
} 

.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .righter{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .righter2{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .center-regular{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.left, .center, .right, .lefter, .righter, .righter2, .center-historico, .center-regular {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/edit.png'); ?>);
}
.righter2 {
  transform: translateX(90px) translateZ(-75px) rotateY(15deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/refresh.png'); ?>);
}
.center-historico {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/img/icons/images.png'); ?>);
}
.center-regular {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(<?php echo base_url('assets/img/icons/testeicon.jpg'); ?>);
}

.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}

</style>

 <div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h4 class="page-title m-b-0">Certidão de Débitos Relativos a Créditos Tributários Federais e à Dívida Ativa da União</h4>
          </div>
          <div class="card-body">
            <?php 
              // VERIFICAR O NOME DA BASE
              $hostCompleto = $_SERVER['HTTP_HOST'];
              $server = explode('.', $hostCompleto);
              $server = $server[0];
              // VERIFICAÇÃO CONCLUÍDA 
               ?>
            <div class="card-header-action">
              <?php 
              echo '<a class="btn btn-lg btn-default btn-rounded text-right"  style="color: #FFFFFF; background-color: #24a0e9;" href="javascript:abrirModal2(&quot;'.$server.'&quot;)">Baixar Todas as Certidões</a>'
               ?>
            </div>
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'Todas'=>'Todas',
                  'Irregulares'=>'Irregular',
                  'Regulares'=>'Regulares',
                  'Matriz'=>'Matriz',
                  'Filial'=>'Filial'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro', 'onchange' => 'loadAjax()'), $valores_filtro, $filtro); 
              ?>
            </div><br>
            <br>
            <div class="table-responsive">
                <table class="table table-striped" id="federais" data-toggle="datatables">
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th style="width:8%">Razão Social</th>
                          <th >CNPJ</th>
                          <th >Município</th>                           
                          <th >Situação  <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone de Certidões localizado na parte superior da tela sempre que alterar a situação da certidão entre Regular e Irregular"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                          <!-- <th >Validade</th> -->
                          <th style="text-align: center;">Histórico de CND</th>
                          <th >Última Consulta</th>
                          <th >Próxima Consulta</th>
                          <th style="text-align: center;">Acompanhamento</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div> 
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Adicionar Observação</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <?php echo form_open("certidao_negativa/adicionar_observacao_federal"); ?> 
          <div class="form-group">
            <label class="col-form-label" for="recipient-name">Empresa:</label>
            <input class="form-control" type="text" id="nomeEmpresaValor" readonly="">
          </div>
          <div class="form-group">
            <label class="col-form-label" for="message-text">Observação:</label>
            <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""  />
            <input type="hidden" id="idEditar" name="id"/>
            <textarea class="form-control" id="observacaoEditarValor" name="observacao" rows="5"></textarea>
          </div> 
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning" type="button" data-dismiss="modal">Fechar</button>
        <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary'), 'Salvar'); ?>
      </div>
      <?php echo form_close(); ?> 
    </div>
  </div>
</div> 

<div class="modal fade" id="modal_historico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Histórico de CND</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table id="modal_table" class="table table-striped" data-toggle="datatables">
              <thead class="thead-default thead-lg">
                  <tr>
                    <th>Razão Social</th>
                    <th>Data da Consulta</th>
                    <th>Status</th>
                    <th>Segunda via</th>
                  </tr>
              </thead>
              <tbody>                
              </tbody>
          </table>
      </div> 
      </div>
      <div class="modal-footer">
        <button class="btn btn-warning" type="button" data-dismiss="modal">Fechar</button> 
      </div>
      <?php echo form_close(); ?> 
    </div>
  </div>
</div>  

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
        toastr["success"]("Observação adicionada com sucesso!", "Adicionada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["error"]("Ocorreu um erro ao adicionar a observação, por favor tente novamente.", "Erro!");
    <?php } ?> 

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["success"]("Certidão atualizada com sucesso!", "Atualizada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["error"]("Ocorreu um erro ao atualizar a certidão, por favor tente novamente.", "Erro!");
    <?php } ?> 
</script>

<script>
$(document).ready(function() {

  loadAjax();

  $(document).on('click', '.editar', function(event) {
      $("#idEditar").val($(this).attr('idLancamento'));
      $("#observacaoEditarValor").val($(this).attr('observacaoEditar'));
      $("#nomeEmpresaValor").val($(this).attr('nomeEmpresa'));
      $("#urlAtualEditar").val($(location).attr('href'));
    }); 

});

function loadAjax(){
    var valor_filtro = $("#filtro").val();

    $.ajax({
        type: "POST",
        url: '<?php echo base_url('certidao_negativa/listarFederalAjax'); ?>',
        data: {
            filtro: valor_filtro
        },
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if(data == "") {
                var array = [];
                popularDataTableFederal(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }else{
                var array = json2arrayFederal(data);
                popularDataTableFederal(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }
        }
    });
}

function popularDataTableFederal(json){
    $.fn.dataTable.moment('DD/MM/YYYY');
    var table = $('#federais').DataTable({
        "order": [[ 3, "desc" ]],
        "iDisplayLength": 50,dom: 'Bfrtip',
        drawCallback: function() {
          $('[data-toggle="tooltip"]').tooltip({
              boundary:'window',
              html: true
            });
        }, 
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
}

function json2arrayFederal(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];
    var im = "";

    for(var i in data){
        array = [];

        if(data[i].cnpj_completo == null){
            data[i].cnpj_completo = "";
        } 

        if(data[i].observacao == null){
            data[i].observacao = "";
        }

        array.push('<td style="text-align: left;">'+data[i].razao_social+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cnpj_completo+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cidade+'</td>');

        im = data[i].inscricao_municipal.replace(/[^0-9]/g,'');

        <?php if($server_name != 'demo'){ ?>

        if(data[i].status == "1") {

            // SITUAÇÃO
            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');

            // VALIDADE
            // array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            // HISTORICO DE CND
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar E-mail última regular" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar Whatsapp última regular" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center-historico"><a href="javascript:void(0)" onclick="openModalHistorico('+data[i].cnpj_completo+')" class=""><div class="explainer" data-toggle="tooltip" data-placement="top" title="Histórico"></div><div class="text"></div></a></div><div class="center-regular"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Última Regular" class=""><div class="explainer"></div><div class="text">Visualizar</div></a> </div> <div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div>');

            // ULTIMA CONSULTA
            array.push('<td style="text-align: center;"><a style="text-align: center;">'+data[i].data_execucao_novo+'</a></td>');

            // PROXIMA CONSULTA
            array.push('<td><a style="text-align: center;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            // ACOMPANHAMENTO
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="'+data[i].caminho_download+'" download  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter" data-toggle="tooltip" data-placement="top" title="Adicionar Observação"><a href="#modalEditar" data-toggle="modal" class="btn btn-default b editar" idLancamento="'+data[i].cnpj_sem_formato+'" nomeEmpresa="'+data[i].razao_social+'" observacaoEditar="'+data[i].observacao+'" ><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj_sem_formato+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');

        } if(data[i].status == "2") {

            // SITUAÇÃO
            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-warning" data-toggle="tooltip" data-placement="top" title="A certidão foi emitida para o CNPJ da matriz">Informação</span></td>');

            // VALIDADE
            // array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            // HISTORICO DE CND
            array.push('<div class="all"><div class="center-historico"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  class=""><div class="explainer" data-toggle="tooltip" data-placement="top" title="Disponível apenas para a matriz"></div><div class="text"></div></a></div></div>');

            // ULTIMA CONSULTA
            array.push('<td style="text-align: center;"><a style="text-align: center;">'+data[i].data_execucao_novo+'</a></td>');

            // PROXIMA CONSULTA
            array.push('<td><a style="text-align: center;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            // ACOMPANHAMENTO
            array.push('<div class="all"><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Disponível apenas para a matriz" class=""><div class="explainer"></div><div class="text"></div></a></div></div>');

        } else {

          if (data[i].disponivel == "2") {

            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-info" data-toggle="tooltip" data-placement="top" title="Sistema da Receita Federal Indisponível no momento da consulta, será realizada outra tentativa">Indisponível</span></td>');

            // VALIDADE
            // array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            // HISTORICO DE CND
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar E-mail última regular" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar Whatsapp última regular" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center-historico"><a href="javascript:void(0)" onclick="openModalHistorico('+data[i].cnpj_completo+')" class=""><div class="explainer"></div><div data-toggle="tooltip" data-placement="top" title="Histórico" class="text"></div></a></div><div class="center-regular"><a href="#modalHistorico" data-toggle="tooltip" data-placement="top" title="Última Regular" class=""><div class="explainer"></div><div class="text">Visualizar</div></a> </div> <div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div>');

            // ULTIMA CONSULTA
            array.push('<td style="text-align: center;"><a style="text-align: center;">'+data[i].data_execucao_novo+'</a></td>');

            // PROXIMA CONSULTA
            array.push('<td><a style="text-align: center;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            // ACOMPANHAMENTO
            array.push('<div class="all"><div class="lefter"><a  disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a  disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter" data-toggle="tooltip" data-placement="top" title="Adicionar Observação"><a href="#modalEditar" data-toggle="modal" class="btn btn-default b editar" idLancamento="'+data[i].cnpj_sem_formato+'" nomeEmpresa="'+data[i].razao_social+'" observacaoEditar="'+data[i].observacao+'" ><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj_sem_formato+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');

          } else {

            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-danger">Irregular</span></td>');

            // VALIDADE
            // array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            // HISTORICO DE CND
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar E-mail última regular" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar Whatsapp última regular" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center-historico"><a href="javascript:void(0)" onclick="openModalHistorico('+data[i].cnpj_completo+')" class=""><div data-toggle="tooltip" data-placement="top" title="Histórico" class="explainer"></div><div class="text"></div></a></div><div class="center-regular"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Última Regular" class=""><div class="explainer"></div><div class="text">Visualizar</div></a> </div> <div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div>');

            // ULTIMA CONSULTA
            array.push('<td style="text-align: center;"><a style="text-align: center;">'+data[i].data_execucao_novo+'</a></td>');

            // PROXIMA CONSULTA
            array.push('<td><a style="text-align: center;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            // ACOMPANHAMENTO
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está IRREGULAR " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está IRREGULAR " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:ver_situacao(&quot;'+data[i].cnpj+'&quot;)" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter" data-toggle="tooltip" data-placement="top" title="Adicionar Observação"><a href="#modalEditar" data-toggle="modal" class="btn btn-default b editar" idLancamento="'+data[i].cnpj_sem_formato+'" nomeEmpresa="'+data[i].razao_social+'" observacaoEditar="'+data[i].observacao+'" ><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj_sem_formato+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
          }
        }

        <?php }else{ ?>

        if(data[i].status == "1") {
            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');

            // VALIDADE
            // array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            // HISTORICO DE CND
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar E-mail última regular" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar Whatsapp última regular" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center-historico"><a href="javascript:void(0)" onclick="openModalHistorico('+data[i].cnpj_completo+')" class=""><div data-toggle="tooltip" data-placement="top" title="Histórico" class="explainer"></div><div class="text"></div></a></div><div class="center-regular"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Última Regular" class=""><div class="explainer"></div><div class="text">Visualizar</div></a> </div> <div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div>');

            // ULTIMA CONSULTA
            array.push('<td style="text-align: center;"><a style="text-align: center;">'+data[i].data_execucao_novo+'</a></td>');

            // PROXIMA CONSULTA
            array.push('<td><a style="text-align: center;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            // ACOMPANHAMENTO
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="'+data[i].caminho_download+'" download  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter" data-toggle="tooltip" data-placement="top" title="Adicionar Observação"><a href="#modalEditar" data-toggle="modal" class="btn btn-default b editar" idLancamento="'+data[i].cnpj_sem_formato+'" nomeEmpresa="'+data[i].razao_social+'" observacaoEditar="'+data[i].observacao+'" ><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');

        } if(data[i].status == "2") {

            // SITUAÇÃO
            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-warning" data-toggle="tooltip" data-placement="top" title="A certidão foi emitida para o CNPJ da matriz">Informação</span></td>');

            // VALIDADE
            // array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            // HISTORICO DE CND
            array.push('<div class="all"><div class="center-historico"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  class=""><div class="explainer" data-toggle="tooltip" data-placement="top" title="Disponível apenas para a matriz"></div><div class="text"></div></a></div></div>');

            // ULTIMA CONSULTA
            array.push('<td style="text-align: center;"><a style="text-align: center;">'+data[i].data_execucao_novo+'</a></td>');

            // PROXIMA CONSULTA
            array.push('<td><a style="text-align: center;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            // ACOMPANHAMENTO
            array.push('<div class="all"><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Disponível apenas para a matriz" class=""><div class="explainer"></div><div class="text"></div></a></div></div>');

        } else {
            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-danger">Pendência</span></td>');

            // VALIDADE
            // array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            // HISTORICO DE CND
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar E-mail última regular" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de Regularidade da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar Whatsapp última regular" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center-historico"><a href="javascript:void(0)" onclick="openModalHistorico('+data[i].cnpj_completo+')" class=""><div data-toggle="tooltip" data-placement="top" title="Histórico" class="explainer"></div><div class="text"></div></a></div><div class="center-regular"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Última Regular" class=""><div class="explainer"></div><div class="text">Visualizar</div></a> </div> <div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div>');

            // ULTIMA CONSULTA
            array.push('<td style="text-align: center;"><a style="text-align: center;">'+data[i].data_execucao_novo+'</a></td>');

            // PROXIMA CONSULTA
            array.push('<td><a style="text-align: center;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            // ACOMPANHAMENTO
            array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está IRREGULAR " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está IRREGULAR " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:ver_situacao(&quot;'+data[i].cnpj+'&quot;)" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter" data-toggle="tooltip" data-placement="top" title="Adicionar Observação"><a href="#modalEditar" data-toggle="modal" class="btn btn-default b editar" idLancamento="'+data[i].cnpj_sem_formato+'" nomeEmpresa="'+data[i].razao_social+'" observacaoEditar="'+data[i].observacao+'" ><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_federal/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
        }

        <?php } ?>
        

        

        arrayMultiple.push(array);
    }

    return arrayMultiple;
}

function redireciona(href) {
    window.location = href
}

function abrirModal(){
  swal({ 
        title: "Buscando...",
        text: 'Atualizando certidão.\nPor favor aguarde...',
        type: "info" ,
        confirmButtonText: "Cancelar",
        confirmButtonColor: "#fff"
    },function(isConfirm){
        if(isConfirm){
            request.abort();
        }
    }); 
} 

function abrirModal2(server_name){
  // server_name = 'testebr';

  swal({
      title: 'Baixando as certidões, por favor aguarde...',
      type: 'warning',
      showConfirmButton: false
  })
  window.location.href = 'https://arquivosveri.com.br/downloadCertidao/?dbName='+server_name+'&certidao=1';

  setTimeout(function(){ swal.close(); }, 30000);

}

function download(filename, data) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;base64,' + data);
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

// Start file download.


function ver_situacao(cnpj){
  $.ajax({
      type: "POST",
      url: '<?php echo base_url('certidao_negativa/busca_situacao_fiscal'); ?>',
      async: true,
      data: { 
          'cnpj': cnpj
      },
      success: function(result){

        if(result != ''){
          
          window.open(result, '_blank');
        }else{
          alert("Erro ao buscar");
        }
      },
  });
} 

function openModalHistorico(cnpj){
  var variaveis = {cnpj : cnpj};

  $.ajax({
    type: "POST",
    url: '<?php echo base_url('historico_certidao_negativa/buscar_historico_pgfn'); ?>',
    data: variaveis,
    async: true,
    success: function(result){
        var data = JSON.parse(result);
        if(data == "") {
            var array = [];
            popularDataTable(array);  
            jQuery('[data-toggle="popover"]').popover();   
            jQuery('[data-toggle="tooltip"]').tooltip();    

            $('#modal_historico').modal('show');          
        }else{
          var array = json2array(data);
          popularDataTable(array);
          jQuery('[data-toggle="popover"]').popover();
          jQuery('[data-toggle="tooltip"]').tooltip(); 

           $('#modal_historico').modal('show');
        }
    }
  });
}

function popularDataTable(json){
  $.fn.dataTable.moment('DD/MM/YYYY');
  $('#modal_table').DataTable({
        "iDisplayLength": 50,
        "order": [[ 0, "desc" ]],
        lengthChange: false,
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    data: json,
    "bDestroy": true
      });
}

function json2array(data){
  var string = "";
  var array = [];
  var arrayMultiple = [];

  for(var i in data){
    array = [];
    array.push('<td>'+data[i].razao_social+'</td>'); 
    array.push('<td>'+data[i].data_execucao_novo+'</td>');

    if(data[i].status == "0"){
      array.push('<td><span class="span badge badge-pill pill-badge-danger">Irregular</span></td>');
    }else {
      array.push('<td><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');
    }

    if(data[i].status == "0"){
      array.push('<td><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Certidão não disponível" class=""><div class="explainer"></div></td>');
    }else {
      array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão de débitos da PGFN da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="'+data[i].caminho_download+'" download target="_blank" data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="center-regular"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Última Regular" class=""><div class="explainer"></div>');
    }
    
    arrayMultiple.push(array);
  }
  
  return arrayMultiple;
}
</script>