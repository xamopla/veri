<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
 
<div class="page-body"> 
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2>Certidão Negativa de Débitos</h2> 
          </div>
          <div class="card-body"> 
            <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
              <li class="nav-item"><a class="nav-link active" id="top-home-tab" data-toggle="tab" href="#tab-1-1" role="tab" aria-controls="top-home" aria-selected="true"> Federais</a>
                <div class="material-border"></div>
              </li>
              <li class="nav-item"><a class="nav-link" id="profile-top-tab" data-toggle="tab" href="#tab-1-2" role="tab" aria-controls="top-profile" aria-selected="false"> Estaduais</a>
                <div class="material-border"></div>
              </li>
              <li class="nav-item"><a class="nav-link" id="contact-top-tab" data-toggle="tab" href="#tab-1-3" role="tab" aria-controls="top-contact" aria-selected="false"> Municipais</a>
                <div class="material-border"></div>
              </li>
            </ul>
            <div class="tab-content" id="top-tabContent">

              <!-- TAB FEDERAIS -->              
              <div class="tab-pane fade show active" id="tab-1-1" role="tabpanel" aria-labelledby="top-home-tab">
                <div class="table-responsive">
                    <table class="table table-striped" id="federais" data-toggle="datatables">
                        <thead class="thead-default thead-lg">
                            <tr>
                              <th>Razão Social</th>
                              <th>CNPJ</th>
                              <th>Inscrição Estadual</th>
                              <th>Município</th>                              
                              <th>Situação <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone de Certidões localizado na parte superior da tela sempre que alterar a situação da certidão entre Regular e Irregular"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                              <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div> 
              </div>
              <!-- TAB FEDERAIS FIM -->              

              <!-- TAB ESTADUAIS -->
              <div class="tab-pane fade" id="tab-1-2" role="tabpanel" aria-labelledby="profile-top-tab">
                <div class="table-responsive">
                  <table class="table table-striped" id="estaduais" data-toggle="datatables">
                      <thead class="thead-default thead-lg">
                          <tr>
                            <th>Razão Social</th>
                            <th>CNPJ</th>
                            <th>Inscrição Estadual</th>
                            <th>Município</th>
                            <th width="20%">Situação</th>
                            <th>Ações</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
                </div> 
              </div>
              <!-- TAB ESTADUAIS FIM -->

              <!-- TAB MUNICIPAIS -->
              <div class="tab-pane fade" id="tab-1-3" role="tabpanel" aria-labelledby="contact-top-tab">
                <div class="table-responsive">
                  <table class="table table-striped" id="municipais" data-toggle="datatables">
                      <thead class="thead-default thead-lg">
                          <tr>
                            <th>Razão Social</th>
                            <th>CNPJ</th>
                            <th>Inscrição Estadual</th>
                            <th>Município</th>
                            <th width="20%">Situação</th>
                            <th>Ações</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
                </div>
              </div>
              <!-- TAB MUNICIPAIS FIM -->

            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {

  loadAjax();

});

function loadAjax(){

  $.ajax({
    type: "POST",
    url: '<?php echo base_url('certidao_negativa/listarAjax'); ?>',
    async: true,
    success: function(result){
        var data = JSON.parse(result);
        if(data == "") {
            var array = [];
            popularDataTable(array);  
            jQuery('[data-toggle="popover"]').popover();   
            jQuery('[data-toggle="tooltip"]').tooltip();              
        }else{
          var array = json2array(data);
          popularDataTable(array);
          jQuery('[data-toggle="popover"]').popover();
          jQuery('[data-toggle="tooltip"]').tooltip(); 
        }
    }
  });

    $.ajax({
        type: "POST",
        url: '<?php echo base_url('certidao_negativa/listarEstadualAjax'); ?>',
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if(data == "") {
                var array = [];
                popularDataTableEstadual(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }else{
                var array = json2arrayEstadual(data);
                popularDataTableEstadual(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }
        }
    });

    $.ajax({
        type: "POST",
        url: '<?php echo base_url('certidao_negativa/listarFederalAjax'); ?>',
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if(data == "") {
                var array = [];
                popularDataTableFederal(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }else{
                var array = json2arrayFederal(data);
                popularDataTableFederal(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }
        }
    });
}

function popularDataTable(json){
  $.fn.dataTable.moment('DD/MM/YYYY');
  $('#municipais').DataTable({
        "order": [[ 3, "desc" ]],
        "iDisplayLength": 50,
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    data: json,
    "bDestroy": true
      }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
}

function popularDataTableEstadual(json){
    $.fn.dataTable.moment('DD/MM/YYYY');
    $('#estaduais').DataTable({
        "order": [[ 3, "desc" ]],
        "iDisplayLength": 50,dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
    }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
}

function popularDataTableFederal(json){
    $.fn.dataTable.moment('DD/MM/YYYY');
    $('#federais').DataTable({
        "order": [[ 3, "desc" ]],
        "iDisplayLength": 50,dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
    }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
}

function json2array(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];
    var im = "";

    for(var i in data){
      array = [];
      
      if(data[i].cnpj_completo == null){
        data[i].cnpj_completo = "";
      }

      if(data[i].inscricao_estadual == null){
        data[i].inscricao_estadual_completo = "";
      }
      
      array.push('<td style="text-align: left;">'+data[i].razao_social+'</td>');
      array.push('<td style="text-align: left;">'+data[i].cnpj_completo+'</td>');
      array.push('<td style="text-align: left;">'+data[i].inscricao_estadual_completo+'</td>');
      array.push('<td style="text-align: left;">'+data[i].cidade+'</td>');

      im = data[i].inscricao_municipal.replace(/[^0-9]/g,'');

      var motivo = data[i].motivo;

      if(motivo != null && motivo != ""){
        if(motivo.includes("Situa")){
          data[i].motivo = "Situação Cadastral não permite a emissão de certidão negativa.";
        }else if(motivo.includes("PPI")){
          data[i].motivo = "Contribuinte possui dívida no PPI em aberto.";
        }else{
          data[i].motivo = "Contribuinte possui dívida em aberto.";
        }
      }
      

      if(data[i].cidade == "Salvador"){
        if(data[i].status == "Regular"){
          array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');
          array.push('<a href="http://www.sefaz.salvador.ba.gov.br/Certidoes/CtdNegaDbtMob?Length=9"  target=“_blank” data-toggle="tooltip" data-placement="top" title="Consultar" class="btn btn-default b"><i class="fa fa-search" aria-hidden="true" style="color:#24a0e9;"></i></a>');
        }else{
          array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-danger" data-toggle="tooltip" data-placement="top" title="'+data[i].motivo+'">Irregular</span></td>');
          array.push('<td></td>');
        }
      }else if(data[i].cidade == "Feira de Santana"){
        if(data[i].status == "TRUE"){
          array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Disponível</span></td>');
          array.push('<a href="http://www.sefaz.feiradesantana.ba.gov.br/imprimir/imprimirCertidaoDebitos.php?inscricao='+im+'&parcelamento=&debitoParc=1&debitoComun="  target=“_blank” data-toggle="tooltip" data-placement="top" title="Download Certidão de Débito" class="btn btn-default b"><i class="fa fa-download" aria-hidden="true" style="color:#24a0e9;"></i></a>');
        }else{
          array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-danger">Não Disponível</span></td>');
          array.push('<td></td>');
        }
      }else if(data[i].cidade == "Luis Eduardo Magalhães"){
        if(data[i].status == "Regular"){
          array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');
          array.push('<a href="'+data[i].url+'"  target=“_blank” data-toggle="tooltip" data-placement="top" title="Consultar" class="btn btn-default b"><i class="fa fa-search" aria-hidden="true" style="color:#24a0e9;"></i></a>');
        }else{
          array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-danger" data-toggle="tooltip" data-placement="top" title="'+data[i].motivo+'">Irregular</span></td>');
          array.push('<td></td>');
        }
      }
      

      arrayMultiple.push(array);
    }
    
    return arrayMultiple;
}

function json2arrayEstadual(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];
    var im = "";

    for(var i in data){
        array = [];

        if(data[i].cnpj_completo == null){
            data[i].cnpj_completo = "";
        }

        if(data[i].inscricao_estadual == null){
            data[i].inscricao_estadual_completo = "";
        }

        array.push('<td style="text-align: left;">'+data[i].razao_social+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cnpj_completo+'</td>');
        array.push('<td style="text-align: left;">'+data[i].inscricao_estadual_completo+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cidade+'</td>');

        im = data[i].inscricao_municipal.replace(/[^0-9]/g,'');

        if(data[i].status == "Regular") {
            array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');
        } else {
            array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-warning">Alerta</span><span class="fa fa-question-circle" data-toggle="tooltip" title="Certidões em alertas são aquelas cujo status é Positiva ou Positiva com efeito de Negativa." style="color: black;"></td>');
        }

        array.push(`<a href="${data[i].caminho_download}" target=“_blank” data-toggle="tooltip" data-placement="top" title="Consultar" class="btn btn-default b"><i class="fa fa-search" aria-hidden="true" style="color:#24a0e9;"></i></a>`);

        arrayMultiple.push(array);
    }

    return arrayMultiple;
}

function json2arrayFederal(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];
    var im = "";

    for(var i in data){
        array = [];

        if(data[i].cnpj_completo == null){
            data[i].cnpj_completo = "";
        }

        if(data[i].inscricao_estadual == null){
            data[i].inscricao_estadual_completo = "";
        }

        array.push('<td style="text-align: left;">'+data[i].razao_social+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cnpj_completo+'</td>');
        array.push('<td style="text-align: left;">'+data[i].inscricao_estadual_completo+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cidade+'</td>');

        im = data[i].inscricao_municipal.replace(/[^0-9]/g,'');

        if(data[i].status == "0") {
            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');
        } else {
            array.push('<td style="text-align: center;"><span class="span badge badge-pill pill-badge-danger">Pendência</span></td>');
        }

        array.push('<a href="http://servicos.receita.fazenda.gov.br/Servicos/certidao/CndConjuntaInter/EmiteCertidaoInternet.asp?ni='+data[i].cnpj+'&passagens=1&tipo=1"  target="_blank" data-toggle="tooltip" data-placement="top" title="Consultar" class="btn btn-default b"><i class="fa fa-search" aria-hidden="true" style="color:#24a0e9;"></i></a>');

        arrayMultiple.push(array);
    }

    return arrayMultiple;
}

function redireciona(href) {
    window.location = href
}

</script>