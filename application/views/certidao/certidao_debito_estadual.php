<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

 <style type="text/css">
.swal2-styled{
    background-color: #dc2727 !important;
    display: none !important;
}   
.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .righter{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .center-regular{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .righter2{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.left, .center, .right, .lefter, .righter, .righter2, .center-historico, .center-regular {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}
.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/edit.png'); ?>);
}
.righter2 {
  transform: translateX(90px) translateZ(-75px) rotateY(15deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/refresh.png'); ?>);
}
.center-historico {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/img/icons/images.png'); ?>);
}

.center-regular {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(<?php echo base_url('assets/img/icons/testeicon.jpg'); ?>);
}
.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}

</style>

 <div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h4 class="page-title m-b-0">Certidão Negativa Estadual <?php echo $filtro_uf; ?></h4>
            
            <h3 style="font-size: 18px; color: green;">Obs: <b>Clique na bandeira para filtrar pelo estado escolhido.</b></h3>
          </div>
          <div class="card-body">
          <!-- BANDEIRAS --> 
          <div style="text-align: center;">
            <!-- ACRE -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/AC"); ?>" data-toggle="tooltip" data-placement="top" title="Acre"><img src="<?php echo base_url('assets/img/estados/Acre.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Acre"><img src="<?php echo base_url('assets/img/estados/black/Acre.png'); ?>"></a> -->

            <!-- ALAGOAS -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/AL"); ?>" data-toggle="tooltip" data-placement="top" title="Alagoas"><img src="<?php echo base_url('assets/img/estados/Alagoas.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Alagoas"><img src="<?php echo base_url('assets/img/estados/black/Alagoas.png'); ?>"></a> -->

            <!-- AMAPA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/AP"); ?>" data-toggle="tooltip" data-placement="top" title="Amapá"><img src="<?php echo base_url('assets/img/estados/Amapa.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Amapá"><img src="<?php echo base_url('assets/img/estados/black/Amapa.png'); ?>"></a> -->

            <!-- AMAZONAS -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/AM"); ?>" data-toggle="tooltip" data-placement="top" title="Amazonas"><img src="<?php echo base_url('assets/img/estados/Amazonas.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Amazonas"><img src="<?php echo base_url('assets/img/estados/black/Amazonas.png'); ?>"></a> -->

            <!-- BAHIA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/BA"); ?>" data-toggle="tooltip" data-placement="top" title="Bahia"><img src="<?php echo base_url('assets/img/estados/Bahia.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Bahia"><img src="<?php echo base_url('assets/img/estados/black/Bahia.png'); ?>"></a> -->

            <!-- CEARA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/CE"); ?>" data-toggle="tooltip" data-placement="top" title="Ceará"><img src="<?php echo base_url('assets/img/estados/Ceara.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Ceará"><img src="<?php echo base_url('assets/img/estados/black/Ceara.png'); ?>"></a> -->

            <!-- DISTRITO FEDERAL -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/DF"); ?>" data-toggle="tooltip" data-placement="top" title="Distrito Federal"><img src="<?php echo base_url('assets/img/estados/Distrito Federal.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Distrito Federal"><img src="<?php echo base_url('assets/img/estados/black/Distrito Federal.png'); ?>"></a> -->

            <!-- ESPIRITO SANTO -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/ES"); ?>" data-toggle="tooltip" data-placement="top" title="Espírito Santo"><img src="<?php echo base_url('assets/img/estados/Espirito Santo.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Espírito Santo"><img src="<?php echo base_url('assets/img/estados/black/Espirito Santo.png'); ?>"></a> -->

            <!-- GOIAS -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/GO"); ?>" data-toggle="tooltip" data-placement="top" title="Goiás"><img src="<?php echo base_url('assets/img/estados/Goias.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Goiás"><img src="<?php echo base_url('assets/img/estados/black/Goias.png'); ?>"></a> -->

            <!-- MARANHÃO -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/MA"); ?>" data-toggle="tooltip" data-placement="top" title="Maranhão"><img src="<?php echo base_url('assets/img/estados/Maranhão.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Maranhão"><img src="<?php echo base_url('assets/img/estados/black/Maranhão.png'); ?>"></a> -->

            <!-- MATO GROSSO DO SUL -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/MS"); ?>" data-toggle="tooltip" data-placement="top" title="Mato Grosso do Sul"><img src="<?php echo base_url('assets/img/estados/Mato Grosso do Sul.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Mato Grosso do Sul"><img src="<?php echo base_url('assets/img/estados/black/Mato Grosso do Sul.png'); ?>"></a> -->

            <!-- MATO GROSSO -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/MT"); ?>" data-toggle="tooltip" data-placement="top" title="Mato Grosso"><img src="<?php echo base_url('assets/img/estados/Mato Grosso.png'); ?>"></a>
            <!-- <a href="" data-toggle="tooltip" data-placement="top" title="Mato Grosso"><img src="<?php echo base_url('assets/img/estados/black/Mato Grosso.png'); ?>"></a> -->

            <!-- MINAS GERAIS -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/MG"); ?>" data-toggle="tooltip" data-placement="top" title="Minas Gerais"><img src="<?php echo base_url('assets/img/estados/Minas Gerais.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Minas Gerais"><img src="<?php echo base_url('assets/img/estados/black/Minas Gerais.png'); ?>"></a> -->

            
            <br>
            <br>
            <div style="text-align: center;">
            <!-- PARA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/PA"); ?>" data-toggle="tooltip" data-placement="top" title="Pará"><img src="<?php echo base_url('assets/img/estados/Para.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Pará"><img src="<?php echo base_url('assets/img/estados/black/Para.png'); ?>"></a> -->

            <!-- PARAIBA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/PB"); ?>" data-toggle="tooltip" data-placement="top" title="Paraíba"><img src="<?php echo base_url('assets/img/estados/Paraiba.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Paraíba"><img src="<?php echo base_url('assets/img/estados/black/Paraiba.png'); ?>"></a> -->

            <!-- PARANA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/PR"); ?>" data-toggle="tooltip" data-placement="top" title="Paraná"><img src="<?php echo base_url('assets/img/estados/Parana.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Paraná"><img src="<?php echo base_url('assets/img/estados/black/Parana.png'); ?>"></a> -->

            <!-- PERNAMBUCO -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/PE"); ?>" data-toggle="tooltip" data-placement="top" title="Pernambuco"><img src="<?php echo base_url('assets/img/estados/Pernambuco.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Pernambuco"><img src="<?php echo base_url('assets/img/estados/black/Pernambuco.png'); ?>"></a> -->

            <!-- PIAU -->
            <!-- <a href="<?php echo base_url("certidao_negativa_estadual/listar/PI"); ?>" data-toggle="tooltip" data-placement="top" title="Piauí"><img src="<?php echo base_url('assets/img/estados/Piaui.png'); ?>"></a> -->
            <a href="#" data-toggle="tooltip" data-placement="top" title="Piauí"><img src="<?php echo base_url('assets/img/estados/black/Piaui.png'); ?>"></a>

            <!-- RIO DE JANEIRO -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/RJ"); ?>" data-toggle="tooltip" data-placement="top" title="Rio de Janeiro"><img src="<?php echo base_url('assets/img/estados/Rio de Janeiro.png'); ?>"></a>
            <!-- <a href="<?php echo base_url("certidao_negativa_estadual/listar/RJ"); ?>" data-toggle="tooltip" data-placement="top" title="Rio de Janeiro"><img src="<?php echo base_url('assets/img/estados/black/Rio de Janeiro.png'); ?>"></a> -->

            <!-- RIO GRANDE DO NORTE -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/RN"); ?>" data-toggle="tooltip" data-placement="top" title="Rio Grande do Norte"><img src="<?php echo base_url('assets/img/estados/Rio Grande do Norte.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Rio Grande do Norte"><img src="<?php echo base_url('assets/img/estados/black/Rio Grande do Norte.png'); ?>"></a> -->

            <!-- RIO GRANDE DO SUL -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/RS"); ?>" data-toggle="tooltip" data-placement="top" title="Rio Grande do Sul"><img src="<?php echo base_url('assets/img/estados/Rio Grande do Sul.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Rio Grande do Sul"><img src="<?php echo base_url('assets/img/estados/black/Rio Grande do Sul.png'); ?>"></a> -->

            <!-- RONDONIA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/RO"); ?>" data-toggle="tooltip" data-placement="top" title="Rondônia"><img src="<?php echo base_url('assets/img/estados/Rondonia.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Rondônia"><img src="<?php echo base_url('assets/img/estados/black/Rondonia.png'); ?>"></a> -->

            <!-- RORAIMA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/RR"); ?>" data-toggle="tooltip" data-placement="top" title="Roraima"><img src="<?php echo base_url('assets/img/estados/Roraima.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Roraima"><img src="<?php echo base_url('assets/img/estados/black/Roraima.png'); ?>"></a> -->

            <!-- SANTA CATARINA -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/SC"); ?>" data-toggle="tooltip" data-placement="top" title="Santa Catarina"><img src="<?php echo base_url('assets/img/estados/Santa Catarina.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Santa Catarina"><img src="<?php echo base_url('assets/img/estados/black/Santa Catarina.png'); ?>"></a> -->

            <!-- SAO PAULO -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/SP"); ?>" data-toggle="tooltip" data-placement="top" title="São Paulo"><img src="<?php echo base_url('assets/img/estados/São Paulo.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="São Paulo"><img src="<?php echo base_url('assets/img/estados/black/São Paulo.png'); ?>"></a> -->

            <!-- SERGIPE -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/SE"); ?>" data-toggle="tooltip" data-placement="top" title="Sergipe"><img src="<?php echo base_url('assets/img/estados/Sergipe.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Sergipe"><img src="<?php echo base_url('assets/img/estados/black/Sergipe.png'); ?>"></a> -->

            <!-- TOCANTINS -->
            <a href="<?php echo base_url("certidao_negativa_estadual/listar/TO"); ?>" data-toggle="tooltip" data-placement="top" title="Tocantins"><img src="<?php echo base_url('assets/img/estados/Tocantios.png'); ?>"></a>
            <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Tocantins"><img src="<?php echo base_url('assets/img/estados/black/Tocantios.png'); ?>"></a> -->

            </div>
          </div>
          <!-- FIM BANDEIRAS -->
          <br>
          <?php 
              // VERIFICAR O NOME DA BASE
              $hostCompleto = $_SERVER['HTTP_HOST'];
              $server = explode('.', $hostCompleto);
              $server = $server[0];
              // VERIFICAÇÃO CONCLUÍDA 
               ?>
            <div class="card-header-action">
              <?php 
              echo '<a class="btn btn-lg btn-default btn-rounded text-right" onclick"abrirModal2" style="color: #FFFFFF; background-color: #24a0e9;" href="https://arquivosveri.com.br/download?dbName='.$server.'&fonte=sp">Baixar Todas as Certidões</a>'
               ?>
            </div>
            <div class="form-group col-lg-3 col-xs-3 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'Todas'=>'Todas',
                  'Irregulares'=>'Irregulares',
                  'Regulares'=>'Regulares'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro', 'onchange' => 'loadAjaxExtra()'), $valores_filtro, $filtro); 
              ?>
            </div>
            <div class="form-group col-lg-3 col-xs-3 pull-right">
              <label>Filtro UF:</label>
              <?php 
              $valores_filtro_uf = array(
                  'Todas'=>'Todas',
                  'AC'=>'AC',                  
                  'AL'=>'AL',
                  'AP'=>'AP',
                  'AM'=>'AM',
                  'BA'=>'BA',
                  'CE'=>'CE',
                  'DF'=>'DF',
                  'ES'=>'ES',
                  'GO'=>'GO',
                  'MA'=>'MA',
                  'MT'=>'MT',
                  'MS'=>'MS',
                  'MG'=>'MG',
                  'PA'=>'PA',
                  'PB'=>'PB',
                  'PR'=>'PR',
                  'PE'=>'PE',
                  'RJ'=>'RJ',
                  'RN'=>'RN',
                  'RS'=>'RS',
                  'RO'=>'RO',
                  'RR'=>'RR',
                  'SC'=>'SC',
                  'SP'=>'SP',
                  'SE'=>'SE',
                  'TO'=>'TO'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'valor_filtro_2', 'onchange' => 'loadAjaxExtra()'), $valores_filtro_uf, $filtro_uf); 
              ?>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table table-striped" id="estaduais" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr>
                        <th style="width: 12%;">Razão Social</th>
                        <th>CNPJ</th>
                        <th>UF</th>
                        <th>Município</th>
                        <th>Situação  <a class=""  data-toggle="tooltip" data-placement="left" title="" style="color: gold;" data-original-title="É gerado um alerta no ícone de Certidões localizado na parte superior da tela sempre que alterar a situação da certidão entre Regular e Irregular"><img width="20px" height="20px" src=" <?php echo base_url('/assets/images/IconeAlerta.png'); ?>"/></th>
                        <th>Última Consulta</th>
                        <th>Próxima Consulta</th>
                        <th>Acompanhamento</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            </div> 
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
        toastr["success"]("Observação adicionada com sucesso!", "Adicionada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["error"]("Ocorreu um erro ao adicionar a observação, por favor tente novamente.", "Erro!");
    <?php } ?> 

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["success"]("Certidão atualizada com sucesso!", "Atualizada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["error"]("Ocorreu um erro ao atualizar a certidão, por favor tente novamente.", "Erro!");
    <?php } ?> 
</script>

<script>
$(document).ready(function() {

  loadAjax();
  loadAjaxExtra()
});

function loadAjax(){
    var valor_filtro = $("#filtro").val();
    var valor_filtro_uf = <?php echo "'".$filtro_uf."'"; ?>;

    $.ajax({
        type: "POST",
        url: '<?php echo base_url('certidao_negativa_estadual/listarEstadualAjax'); ?>',
        data: {
            filtro: valor_filtro,
            filtro_uf: valor_filtro_uf
        },
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if(data == "") {
                var array = [];
                popularDataTableEstadual(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }else{
                var array = json2arrayEstadual(data);
                popularDataTableEstadual(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }
        }
    });
}

function loadAjaxExtra(){
    var valor_filtro_2 = $("#valor_filtro_2").val();
    var valor_filtro_uf_2 = <?php echo "'".$filtro_uf."'"; ?>;

    $.ajax({
        type: "POST",
        url: '<?php echo base_url('certidao_negativa/listarEstadualAjax'); ?>',
        data: {
            filtro: valor_filtro_2,
            filtro_uf: valor_filtro_uf_2
        },
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if(data == "") {
                var array = [];
                popularDataTableEstadual(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }else{
                var array = json2arrayEstadual(data);
                popularDataTableEstadual(array);
                jQuery('[data-toggle="popover"]').popover();
                jQuery('[data-toggle="tooltip"]').tooltip();
            }
        }
    });
}

function popularDataTableEstadual(json){
    $.fn.dataTable.moment('DD/MM/YYYY');
    var table = $('#estaduais').DataTable({
        "order": [[ 3, "desc" ]],
        "iDisplayLength": 50,dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
    });
    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
}

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function getFormattedDate(date) {
  var year = date.getFullYear();

  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;

  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  
  return month + '/' + day + '/' + year;
}

function json2arrayEstadual(data){
    var string = "";
    var array = [];
    var arrayMultiple = [];
    var im = "";

    for(var i in data){
        array = [];

        if(data[i].cnpj == null){
            data[i].cnpj = "";
        }

        if(data[i].inscricao_estadual == null){
            data[i].inscricao_estadual_completo = "";
        }

        array.push('<td style="text-align: left;">'+data[i].razao_social+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cnpj+'</td>');
        array.push('<td style="text-align: left;">'+data[i].uf+'</td>');
        array.push('<td style="text-align: left;">'+data[i].cidade+'</td>');

        if(data[i].inscricao_municipal != undefined){
          im = data[i].inscricao_municipal.replace(/[^0-9]/g,'');
        }else{
          im = "";
        }

        if(data[i].status == 1) {
            array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-success" style="background-color:#2ecc71!important;">Regular</span></td>');

             array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

             array.push('<td><a style="text-align: center; !important;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

             array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está REGULAR e disponível no link '+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="'+data[i].caminho_download+'" download  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="'+data[i].caminho_download+'" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Manutenção"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" class="btn btn-default b editar" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');


             // <a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a>
        } else {
            array.push('<td style="text-align: left;"><span class="span badge badge-pill pill-badge-danger">Irregular</span></td>');

            array.push('<td><a style="text-align: center; !important;">'+data[i].data_execucao_novo+'</a></td>');

            array.push('<td><a style="text-align: center; !important;" data-toggle="tooltip" data-placement="top" title="Restam '+data[i].intervalo+' dias para a próxima execução.">'+data[i].proxima_execucao+'</a></td>');

            if (data[i].uf == "SP") {
              array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            } 
            // else if (data[i].uf == "SE") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "RS") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');   
            // } else if (data[i].uf == "CE") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "MG") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "SC") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "MT") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "AL") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "AP") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "MS") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "PE") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } else if (data[i].uf == "TO") {
            //   array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualizar Certidão"><a href="<?php echo base_url('consultar_certidoes_tempo_real/consultar_certidao_stadual_saopaulo/'); ?>'+data[i].cnpj+'" class="btn btn-default b editar" onclick="abrirModal()" ><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            // } 
            else {
              array.push('<div class="all"><div class="lefter"><a href="mailto:?subject=Notificação Sistema VERI&body=A certidão estadual da empresa '+data[i].razao_social+' está Irregular e indisponível" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="left"><a href="https://api.whatsapp.com/send?text=A certidão estadual da empresa '+data[i].razao_social+' está Irregular indisponível " target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class=""><div class="explainer"></div> <div class="text">Whatsapp</div></a></div><div class="center"><a href="javascript:void(0)" disabled style="cursor:not-allowed;"  data-toggle="tooltip" data-placement="top" title="Download" class=""><div class="explainer"></div><div class="text"></div></a></div><div class="right"><a href="javascript:void(0)" disabled style="cursor:not-allowed;" data-toggle="tooltip" data-placement="top" title="Visualizar" class=""><div class="explainer"></div><div class="text">Visualizar</div></a></div><div class="righter2" data-toggle="tooltip" data-placement="top" title="Atualização em tempo real não disponível para esse estado."><a href="#" class="btn btn-default b editar"><div class="explainer"></div><div class="text">Visualizar</div></a></div></div>');
            }
            

        }

        

        arrayMultiple.push(array);
    }

    return arrayMultiple;
}

function redireciona(href) {
    window.location = href
}

function abrirModal(){
  swal({ 
        title: "Buscando...",
        text: 'Atualizando certidão.\nPor favor aguarde...',
        type: "info" ,
        confirmButtonText: "Cancelar",
        confirmButtonColor: "#fff"
    },function(isConfirm){
        if(isConfirm){
            request.abort();
        }
    }); 
} 


function abrirModal2(){
  let timerInterval
  swal({
    title: 'Baixando as certidões',
    html: 'Por favor aguarde...',
    timer: 10000,
    timerProgressBar: true,
    didOpen: () => {
      Swal.showLoading()
      const b = Swal.getHtmlContainer().querySelector('b')
      timerInterval = setInterval(() => {
        b.textContent = Swal.getTimerLeft()
      }, 100)
    },
    willClose: () => {
      clearInterval(timerInterval)
    }
  }).then((result) => {
    /* Read more about handling dismissals below */
    if (result.dismiss === Swal.DismissReason.timer) {
      console.log('I was closed by the timer')
    }
  })
}

</script>