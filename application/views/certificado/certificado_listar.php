<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2><?php echo $nome_tela; ?></h2>
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'listar_certificados_todos'=>'Todas',
                  'listar_certificados_vencidos'=>'Vencidas',
                  'listar_certificados_proximos'=>'Próximos ao Vencimento',
                  'listar_certificados_regulares'=>'Regulares'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br> <br>
            <div class="table-responsive row">
                <table class="table table-striped" id="example" data-toggle="datatables">
                    <thead class="thead-default thead-lg">
                        <tr  id="passo11">
                            <th></th>
                            <th style="font-size: 12px;">Razão Social</th>
                            <th style="font-size: 12px;">CNPJ</th>
                            <th style="font-size: 12px;">Inscrição Estadual</th>
                            <th style="font-size: 12px;">Tipo do Certificado</th>
                            <th style="font-size: 12px;">Data de Vencimento</th>
                            <th style="font-size: 12px;">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ($consulta){ 
                        foreach ($consulta as $e){?>
                        <tr>
                              <?php 
                               $data_v = date('Ymd', $e->data_validade);
                               $data_for = date('Y-m-d', strtotime($data_v));

                              ?>
                              <td><?php echo $data_for; ?></td>

                              <td style="font-size: 12px;"><?php echo $e->razao_social; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->cnpj_completo; ?></td>
                              <td style="font-size: 12px;"><?php echo $e->inscricao_estadual_completo; ?></td>
                              <?php if(!$e->is_procuracao){ ?>
                                <td style="font-size: 15px;"><span class="badge badge-pill badge-info" style="" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-original-title="Certificado da Empresa">Empresa</span>
                                </td>
                              <?php }else{ ?>
                                <td style="font-size: 15px;"><span class="badge badge-pill badge-warning" style="" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-original-title="Certificado do Contador">Contador</span>
                                </td>
                              <?php } ?>

                              <td>
                                <?php 

                                $data_vencimento = date('Ymd', $e->data_validade);
                                $data_hoje = date('Ymd');

                                $diasNotificacao = 30;
                                $df = new DateTime($data_vencimento);
                                $da = new DateTime(date(''));

                                $intervalo = $df->diff($da);

                                if (date('Ymd', $e->data_validade) < date('Ymd')) {
                                  echo "<span style='font-size: 13px;' class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Vencido' data-content='Vencido à ".$intervalo->format('%a')." dia(s)'>".date('d/m/Y', strtotime($data_vencimento))."</span>";
                                } else if ($intervalo->format('%a') <= $diasNotificacao) {
                                  echo "<span  style='font-size: 13px;' class='badge badge-warning badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Próximo de Vencer' data-content='Faltam ".$intervalo->format('%a')." dia(s) para vencer'>".date('d/m/Y', strtotime($data_vencimento))."</span>";
                                } else{
                                  echo "<span style='font-size: 13px;' class='badge badge-success badge-shadow'  data-container='body' style='background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' title='Certificado dentro do prazo' data-content='Faltam ".$intervalo->format('%a')." dia(s) para vencer'>".date('d/m/Y', strtotime($data_vencimento))."</span>";
                                }

                                ?>                        
                              </td>

                              <td>   
                                <?php if(!$e->is_procuracao){ ?>
                                <a href="<?php echo base_url("certificado_empresa/cadastrar/$e->id"); ?>" data-toggle="tooltip" data-placement="top" title="Renovar certificado no sistema" class="btn btn-default b" style="padding: 1px !important;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">today</i></a>
                                <?php }else{ ?>
                                  <a href="<?php echo base_url("certificado_contador/cadastrar/$e->id"); ?>" data-toggle="tooltip" data-placement="top" title="Renovar certificado no sistema" class="btn btn-default b" style="padding: 1px !important;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">today</i></a>
                                <?php } ?>
                              </td>
                        </tr>
                    <?php } 
                    }
                    ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('certificado'); ?>/"+filtro+"");
  });
});
</script>
<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  });

$('#example').DataTable({
        "order": [[ 0, "asc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        "iDisplayLength": 50,
        lengthChange: false,
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    }).buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});

</script>