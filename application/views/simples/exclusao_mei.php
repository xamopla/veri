<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4"><?php echo $titulo; ?></h2> 
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
            <label id="passo20">Filtro:</label>
            <?php
              $valores_filtro = array(
                  'TODAS'=>'Todas',
                  'EXCLUIDAS'=>'Excluídas',
                  'OPTANTES'=>'Optantes'

              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro);
            ?>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center" id="example" >
                  <thead>
                    <tr style="background-color:#F0F0F0">
                        <th colspan="2" class="header">Dados</th>
                        <th colspan="2" class="header">Última Exclusão</th>
                        <th colspan="1" class="header">Situação</th>
                    </tr>
                    <tr>
                      <th >Razão Social</th>
                      <th >CNPJ</th>
                      <th>Data Inicial</th>
                      <th>Data Final</th>
                      <th>Data da Exclusão</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($consulta){ 
                      foreach ($consulta as $e){?>
                      <tr>

                        <td ><?php echo $e->razao_social; ?></td>
                        <td ><?php echo $e->cnpj; ?></td>

                        <td ><?php echo $e->data_inicio; ?></td>
                        <td ><?php echo $e->data_fim; ?></td>

                        <?php if($e->is_excluida == 0){ 
                          $situacao = "Empresa optante desde ".$e->data_inicio;
                          ?>

                          <td ><?php echo "<span class='badge badge-success badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Optante' data-content='$situacao'>Optante</span>"; ?></td>
                        <?php }else{ 
                           $situacao = "Empresa excluída desde ".$e->data_fim;
                          ?>
                          <td ><?php echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Data da Exclusão' data-content='$situacao'>Excluída</span>"; ?></td>
                        <?php } ?>
                        
                      </tr>
                  <?php } 
                  }
                  ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modalRegime" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tipoRegimeModal">Alterar Regime</h5>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
            <div class="form-group col-lg-4 col-xs-4">
              <input type="hidden" name="id_empresa_modal" id="id_empresa_modal">
              <label>Selecione o Regime:</label>
              <?php 
              $valores_filtro = array(
                  'SIMPLES'=>'Simples Nacional ',
                  'LUCRO_REAL'=>'Lucro Real',
                  'LUCRO_PRESUMIDO'=>'Lucro Presumido'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'name'=>'tipo_regime', 'id'=>'tipo_regime'), $valores_filtro, $filtro); 
              ?>
            </div>

      </div>
      <div class="modal-footer">
          <button type="button" onclick="salva_regime()" class="btn btn-success">Salvar</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>

</div>




<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<style type="text/css">
    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }

</script>


<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  });

  $('#filtro').change(function(){
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('mei/listar'); ?>/"+filtro+"");
  });

var table = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});
</script>

<script type="text/javascript">  
    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("A empresa foi cadastrada com sucesso!", "Cadastrada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("As informações da empresa foram editadas com sucesso!", "Editada!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
    toastr["success"]("A empresa foi excluída com sucesso.", "Excluída!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["warning"]("Ocorreu um erro, tente novamente!", "Erro!");
    <?php } ?>
</script>
