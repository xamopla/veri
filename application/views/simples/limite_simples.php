<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link href="<?php echo base_url("assets/css/icones.css"); ?>" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }

  .bg-green{
    background-color: #51b754
  }

  .bg-warning2{
    background-color: #f5630e
  }

  .bg-danger{
    background-color: #ef1005 !important
  }

  
</style>

<style type="text/css">

.swal2-styled{
    background-color: #dc2727 !important;
 } 

.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.left, .center, .center2, .right, .lefter, .righter {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.center2 {
  width: 25px;
  height: 25px;
  transform-style: preserve-3d;
  /*border-radius: 8px;*/
  border: 1px solid #fff;
  box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.center2 {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/printer.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
}
.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}

</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Sublimite de Faturamento Simples Nacional</h2> 
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
            <label id="passo20">Filtro:</label>
            <?php
              $valores_filtro = array(
                  'TODAS'=>'Todas',
                  'PROXIMAS'=>'Próximas ao sublimite (RBA - PJ)',
                  'LIMITE'=>'Extrapolou o sublimite (RBA - PJ)',
                  'PROXIMAS_ACUMULADA'=>'Próximas ao sublimite acumulado (SÓCIOS/PJ)',
                  'LIMITE_ACUMULADA'=>'Extrapolou o sublimite acumulado (SÓCIOS/PJ)'

              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro);
            ?>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center" id="example" >
                  <thead>
                    <tr>
                      <th style="width:12%">Razão Social</th>
                      <th style="width:8%">CNPJ</th>
                      <th style="width:8%">Receita bruta acumulada (RBA) PJ</th>
                      <th style="width:12%" >Percentual PJ</th>
                      <th style="width:8%" ></th>
                      <th style="width:12%">Sócios e Administradores</th>
                      <th style="width:8%">Acumulado Simples Nacional (Sócios/PJ)</th>
                      <th style="width:12%">% Acumulado Simples Nacional (Sócios/PJ)</th>
                      <th style="width:8%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($consulta){ 
                      foreach ($consulta as $e){
                        $razao_aux = str_replace("'", "", $e->razao_social);
                        $razao_aux = str_replace("&", "", $razao_aux);

                        $e->razao_social = $razao_aux;

                        ?>
                      <tr>
                        <td ><?php echo $e->razao_social; ?></td>
                        <td ><?php echo $e->cnpj; ?></td>
                        <td ><?php echo "R$ ".number_format($e->valor_atual, 2, ',', '.'); ?></td>

                        <td>
                          <div class="progress mb-2">
                              <?php if($e->percentual < 70){ ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-green" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual.'%'; ?>"><?php echo $e->percentual.'%'; ?></a>
                              <?php } elseif ($e->percentual >= 100) { ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-danger" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual.'%'; ?>" ><?php echo $e->percentual.'%'; ?></a>
                              <?php } else if ($e->percentual < 100 && $e->percentual >= 90){ ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-warning2" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual.'%'; ?>"><?php echo $e->percentual.'%'; ?></a>
                              <?php } else if ($e->percentual >= 70 && $e->percentual < 90){ ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-warning" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual.'%'; ?>"><?php echo $e->percentual.'%'; ?></a>
                              <?php } ?>
                          </div>
                        </td>

                        <td>
                          <div class="all">
                            <div class="lefter">
                              <a href="mailto:?subject=Notificação Sistema VERI&body=A empresa <?php echo $e->razao_social; ?> atingiu <?php echo $e->percentual ?>% do sublimite do Simples Nacional" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                <div class="explainer"></div>
                                <div class="text"></div>
                              </a>
                            </div>
                            <div class="left">
                               <a href="https://api.whatsapp.com/send?text=A empresa <?php echo $e->razao_social; ?> atingiu <?php echo $e->percentual ?>% do sublimite do Simples Nacional" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                <div class="explainer"></div>
                                <div class="text">Whatsapp</div>
                              </a>
                            </div>
                            <div class="center2">
                              <a href="<?php echo $e->path; ?>" target="_blank" data-trigger="hover" data-placement="top" title="Consultar última declaração PJ" class="">
                                <div class="explainer"></div>
                                <div class="text"></div>
                              </a>
                            </div>
                          </div>
                        </td>

                        <td style="font-size: 12px;"><?php echo $e->socios_empresas_string; ?></td>
                        <td ><?php echo "R$ ".number_format($e->total_acumulado, 2, ',', '.'); ?></td>

                        <td>
                          <div class="progress mb-2">
                              <?php if($e->percentual_acumulado < 70){ ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-green" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual_acumulado.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual_acumulado.'%'; ?>"><?php echo $e->percentual_acumulado.'%'; ?></a>
                              <?php } elseif ($e->percentual_acumulado >= 100) { ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-danger" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual_acumulado.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual_acumulado.'%'; ?>" ><?php echo $e->percentual_acumulado.'%'; ?></a>
                              <?php } else if ($e->percentual_acumulado < 100 && $e->percentual_acumulado >= 90){ ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-warning2" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual_acumulado.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual_acumulado.'%'; ?>"><?php echo $e->percentual_acumulado.'%'; ?></a>
                              <?php } else if ($e->percentual_acumulado >= 70 && $e->percentual_acumulado < 90){ ?>
                                  <a href="javascript:void(0)" class="progress-bar progress-bar-striped bg-warning" role="progressbar" aria-valuenow="25" aria-valuemin="20" aria-valuemax="100" style="width:<?php echo $e->percentual_acumulado.'%'; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $e->percentual_acumulado.'%'; ?>"><?php echo $e->percentual_acumulado.'%'; ?></a>
                              <?php } ?>
                          </div>
                        </td>

                        <td>
                          <div class="all">
                            <div class="lefter">
                              <a href="mailto:?subject=Notificação Sistema VERI&body=O total acumulado entre os sócios da empresa <?php echo $e->razao_social; ?> atingiu <?php echo $e->percentual_acumulado ?>% do sublimite do Simples Nacional" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                <div class="explainer"></div>
                                <div class="text"></div>
                              </a>
                            </div>
                            <div class="left">
                               <a href="https://api.whatsapp.com/send?text=O total acumulado entre os sócios da empresa <?php echo $e->razao_social; ?> atingiu <?php echo $e->percentual_acumulado ?>% do sublimite do Simples Nacional" target="_blank" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                <div class="explainer"></div>
                                <div class="text">Whatsapp</div>
                              </a>
                            </div>
                            <div class="center2">
                              <a href="javascript:abrirModalComposicao('<?php echo $e->cnpj; ?>','<?php echo $e->razao_social; ?>')" data-trigger="hover" data-placement="top" title="Ver os detalhes" class="">
                                <div class="explainer"></div>
                                <div class="text"></div>
                              </a>
                            </div>
                          </div>
                        </td>
                        
                      </tr>
                  <?php } 
                  }
                  ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="modalDebitos" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <h5><label id="razao_social_modal" style="font-weight: bold;"></label>
        </h5>
        <input type="hidden" name="id_usuario_modal" id="id_usuario_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         
        <div class="panel panel-success">
          <div class="panel-heading title-menu">
              <h5 class="panel-title">
                Sócios e faturamento
              </h5>
              <label style="color: red; font-size: 20px; background-color: yellow;" >Atenção ! São listadas apenas empresas cadastradas no Veri</label>

          </div>
          <div id="collapse2018" >
              <table class="table consulta" style="margin-top: 0px;">
                  <tbody id="body_modal">
                    
                      
                  </tbody>
                  <br>
                  <tbody id="body_modal2">
                    
                      
                  </tbody>
              </table>
          </div>
        </div>

      </div>
    </div>
  </div>

</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<style type="text/css">
    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }

</script>


<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  });

  $('#filtro').change(function(){
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('limite_simples/listar'); ?>/"+filtro+"");
  });

var table = $('#example').DataTable({
        "iDisplayLength": 50,
        lengthChange: false,
        "order": [[ 7, "desc" ]],
        //buttons: ['excel', 'pdf', 'colvis' ],
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
});
</script>

<script type="text/javascript">  
    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("A empresa foi cadastrada com sucesso!", "Cadastrada!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("As informações da empresa foram editadas com sucesso!", "Editada!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
    toastr["success"]("A empresa foi excluída com sucesso.", "Excluída!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["warning"]("Ocorreu um erro, tente novamente!", "Erro!");
    <?php } ?>
</script>

<script type="text/javascript">
  function abrirModalComposicao(cnpj, razao_social){
    $("#body_modal").html('');
    $("#body_modal2").html('');
    $("#razao_social_modal").text(razao_social);

    $.ajax({
        type: "POST",
        url: '<?php echo base_url('limite_simples/abrirModalComposicao'); ?>',
        async: true,
        data: { 
            'cnpj': cnpj
        },
        success: function(result){
          var resultado = JSON.parse(result);
          if(resultado != ''){
            $("#body_modal").append('<tr><th style="color:#24a0e9 !important" >Sócio(s)</th><th style="color:#24a0e9 !important">CPF/CNPJ</th><th></th><th></th></tr>');

            for(var i in resultado.socios){
              $("#body_modal").append('<tr>');
              $("#body_modal").append('<td style="text-center; font-weight:bold">'+resultado.socios[i].nome_socio+'</td>');
              $("#body_modal").append('<td style="text-center">'+resultado.socios[i].cpf_cnpj+'</td>');
              $("#body_modal").append('<td></td>');
              $("#body_modal").append('<td></td>');
              $("#body_modal").append('</tr>');
            }

            $("#body_modal2").append('<tr><th style="color:#24a0e9 !important">Empresa(s)</th><th style="color:#24a0e9 !important">CNPJ</th><th style="color:#24a0e9 !important">Faturamento</th><th style="color:#24a0e9 !important">Última Declaração</th></tr>');
            for(var i in resultado.empresas){
              $("#body_modal2").append('<tr>');
              $("#body_modal2").append('<td style="text-center; font-weight:bold">'+resultado.empresas[i].razao_social+'</td>');
              $("#body_modal2").append('<td style="text-center">'+resultado.empresas[i].cnpj_empresa+'</td>');
              $("#body_modal2").append('<td style="text-center">'+resultado.empresas[i].limite+'</td>');
              $("#body_modal2").append('<td style="text-center"><div class="all"> <div class="center"><a href="'+resultado.empresas[i].documento+'" target="_blank" data-trigger="hover" data-placement="top" title="Ver" class=""><div class="explainer"></div><div class="text"></div></a></div></div></td>');
              $("#body_modal2").append('</tr>');
            }

            $("#body_modal2").append('<tr><th ></th><th ></th><th>Total '+resultado.total+'</th></tr>');
          }
        },
    });

    $('#modalDebitos').modal();
  }
</script>
