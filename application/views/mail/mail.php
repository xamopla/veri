<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                
              </div>
                <div class="card-body">
                  <div class="ibox">
                        <div class="ibox-body">
                            <?php echo form_open("mail/salvar/".$id_mail, array('role'=>'form')); ?> 

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-4">
                                        <label for="nome">Email destinatário:</label>
                                        <?php echo form_input(array('type'=>'email', 'name'=>'email', 'class'=>'form-control', 'placeholder'=>'E-mail', 'style'=>'border-color: #5c6bc0;'), $mail->email, 'required'); ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group mb-4">
                                        <label for="nome">Assunto (Cada assunto deve ser separado por ';' ex: DCTF;MALHA FISCAL; ) </label>
                                        
                                        <?php echo form_input(array('type'=>'textarea', 'name'=>'assunto', 'class'=>'form-control', 'placeholder'=>'Assunto', 'style'=>'border-color: #5c6bc0;'), $mail->assunto); ?>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                        <div class="ibox-footer text-right">
                            <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                        </div>
                        <?php echo form_close(); ?>
                </div>
                               
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
 