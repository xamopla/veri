<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">

<style type="text/css">

  .hot thead th:last-child{
    background-color: #38b529;
    color: white;
  }

  .hot thead th:first-child{
    background-color: #ffffff;
  }

  .hot tbody th:first-child{
    background-color: #9b9b9b;
    color: white;
  }
</style>
<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Cadastrar Empresa</h2>

                <label style="color: red; font-size: 16px; background-color: yellow;" >Atenção, para cadastrar várias empresas basta copiar e colar os cnpjs das empresas a serem cadastradas no campo abaixo sem limite de quantidade. Ao finalizar clique no botão na parte inferior da tela 'Cadastrar empresas'</label>

              </div>
              <div class="card-body">

                <?php
                if ($this->session->flashdata('msg_conteudo')){
                    echo "<div class='alert alert-{$this->session->flashdata('msg_tipo')} alert-dismissible show fade' role='alert'>
                    <button class='close' data-dismiss='alert'>
                          <span>×</span>
                        </button>
                    <strong>{$this->session->flashdata('msg_titulo')} </strong>{$this->session->flashdata('msg_conteudo')}
                    </div>";
                }
                ?>

                <div class="ibox-body">
                  <ul class="nav nav-pills nav-pills-rounded nav-pills-air nav-pills-success  col-md-4 mx-auto">
                    

                    <li class="nav-item">
                        <a class="nav-link active" href="#tab-2" data-toggle="tab" id="btn-varias">Cadastrar várias empresas.</a>
                    </li>  

                    <li class="nav-item">
                        <a class="nav-link " href="#tab-1" data-toggle="tab" id="btn-uma">Cadastrar apenas uma empresa.</a>
                    </li> 

                  </ul>

                  <!-- CONTEÚDO DAS TABS -->

                  <!-- TAB 2 -->
                  <div id="tab-1" style="display: none">  
                      <br>                                      
                      <?php echo form_open('empresa/cadastrar_empresa_servico_novatela', array('role'=>'form')); ?>
                          <div class="form-group col-md-4 mx-auto">  
                            <label>CNPJ</label>
                            <?php echo form_input(array('name'=>'cnpj', 'class'=>'form-control', 'placeholder'=>'CNPJ', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div> 
                      <div class="ibox-footer text-center">
                          <?php echo form_submit(array('name'=>'btn_localizar', 'class'=>'btn btn-primary mr-2'), 'Localizar'); ?>
                      </div>
                      <?php echo form_close(); ?>                                  
                  </div>
                  <!-- FIM DA TAB 2 -->  

                  <!-- TAB 2 -->
                  <div id="tab-2" style="display: show">  
                      <div class="form-group col-xl-12 mx-auto"> 
                        <br>
                        <div id="principal" class="hot"></div> 
                      </div>
                      <div class="ibox-footer text-center">
                        <button class="btn btn-primary" type="button" id="salvar">Cadastrar Empresas</button>
                      </div>
                  </div>
                  <!-- FIM DA TAB 2 -->  
                </div>
                <!-- FIM DO CONTEÚDO DAS TABS -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js"></script>
      
<script type="text/javascript">
  const container = document.querySelector('#principal');
  const salvar = document.querySelector('#salvar');

  
  document.getElementById('btn-uma').addEventListener('click', function(){
    document.getElementById('tab-1').style.display= '';
    document.getElementById('tab-2').style.display= 'none';
  })

  document.getElementById('btn-varias').addEventListener('click', function(){
    document.getElementById('tab-1').style.display= 'none';
    document.getElementById('tab-2').style.display= '';
  })
  


  Handsontable.renderers.registerRenderer('customRenderer', (hotInstance, TD, ...rest) => {
    Handsontable.renderers.getRenderer('text')(hotInstance, TD, ...rest);
    TD.style.color = 'black';
    TD.style.background = 'white';
  });

  Handsontable.renderers.registerRenderer('success', (hotInstance, TD, ...rest) => {
    Handsontable.renderers.getRenderer('text')(hotInstance, TD, ...rest);
    TD.style.fontWeight = 'bold';
    TD.style.color = 'green';
    TD.style.background = 'lightgreen';
  });

  Handsontable.renderers.registerRenderer('fail', (hotInstance, TD, ...rest) => {
    Handsontable.renderers.getRenderer('text')(hotInstance, TD, ...rest);
    TD.style.fontWeight = 'bold';
    TD.style.color = 'red';
    TD.style.background = 'lightpink';
  });

  Handsontable.renderers.registerRenderer('exceeded', (hotInstance, TD, ...rest) => {
    Handsontable.renderers.getRenderer('text')(hotInstance, TD, ...rest);
    TD.style.fontWeight = 'bold';
    TD.style.color = 'darkgoldenrod';
    TD.style.background = 'gold';
  });

  const table = new Handsontable(container, {
    data: Handsontable.helper.createSpreadsheetData(20, 1),
    nestedHeaders: [['CNPJs das empresas a serem monitoradas', { label: 'A1'}]],
    columns: [{ data: 1, type: "text", renderer: "customRenderer" }],
    rowHeaders: true,
    colHeaders: true,
    width: '100%',
    height: 'auto',
    stretchH: 'all',
    className: 'custom-table',
    manualColumnResize: false,
    licenseKey: "non-commercial-and-evaluation"
  });

  Handsontable.dom.addEvent(salvar, 'click', () => {
    swal({ 
      title: "Cadastrando...",
      text: 'Cadastrando as empresas.\nPor favor, aguardar alguns segundos...',
      type: "info",
      showConfirmButton: false
    },function(isConfirm){
      if(isConfirm){
          request.abort();
      }
    });
    salvar.disabled = true;
    var dados = [];
    for (i = 0; i < table.countSourceRows(); i++) {
      dados.push(table.getSourceDataAtCell(i,0));    
    }
    cadastrar(dados);
  });


  function cadastrar(dados){
    var isErro = false;
    var isExceeded = false;
    var url = '<?= base_url("/empresa/novo_importador/") ?>';
    var request = $.post(url, { dados: dados }, function(dados){
      var cnpjs = JSON.parse(dados)


      for (i = 0; i < table.countSourceRows(); i++) {

        if (cnpjs.erro.includes(table.getSourceDataAtCell(i,0))) {
          setColor(i,table,'fail');
          isErro = true;  
        } else if (cnpjs.exceeded.includes(table.getSourceDataAtCell(i,0))) {
          setColor(i,table,'exceeded');
          isExceeded = true;  
        }else {
          setColor(i,table,'success');  
        }
      }

      if (isExceeded) {
        swal_exceeded();
      } else if (isErro) {
        swal_error();
      }else {
        swal_success();
      }
    });
  }

  function setColor(row, table, color){
    table.setCellMeta(row, 0, 'renderer', color);
    table.render();
  }

  function swal_success(){
    swal({
      title: 'Empresas Cadastradas',
      text: 'Todas empresas foram cadastradas com sucesso!',
      type: 'success',
      showCancelButton: false,
      confirmButtonColor: '#0CC27E',
      confirmButtonText: 'Continuar',
      confirmButtonClass: 'btn btn-success btn-raised',
      buttonsStyling: false
    }).then(function () {
      window.location='<?= base_url("/empresa/listar/") ?>';   
    }).done();
  }

  function swal_error(){
    swal({
      title: 'Empresas Não Cadastradas',
      text: 'Algumas empresas não foram cadastradas, ao continuar, poderá ver os CNPJ destacados em vermelho.',
      type: 'error',
      showCancelButton: false,
      confirmButtonColor: '#fff',
      confirmButtonText: 'Continuar',
      confirmButtonClass: 'btn btn-success btn-raised',
      buttonsStyling: false
    }).done();
  }

  function swal_exceeded(){
    swal({
      title: 'Limite do plano excedido!',
      text: 'Seu limite de cadastro de empresas foi atingido e os CNPJ destacados em amarelo não foram cadastrados. Para realizar a contratação de mais empresas entre em contato conosco no telefone: (11) 3539-4865 (ramal 1)',
      type: 'warning',
      html: 'Seu limite de cadastro de empresas foi atingido e os CNPJ destacados em amarelo não foram cadastrados. <br> Para realizar a contratação de mais empresas entre em contato conosco no telefone: (11) 3539-4865 (ramal 1)',
      showCancelButton: false,
      confirmButtonColor: '#fff',
      confirmButtonText: 'Continuar',
      confirmButtonClass: 'btn btn-success btn-raised',
      buttonsStyling: false
    }).done();
  }

</script>