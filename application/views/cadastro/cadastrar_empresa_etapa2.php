<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Cadastrar Empresa</h2>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <label style="color: red;">Verifique as informações corretamente, ao pular de etapa não será possível voltar a anterior!</label>
          </div>
          <div class="card-body">
            <div class="f1" >
              <div class="f1-steps" >
                <div class="f1-progress">
                  <div class="f1-progress-line" style="width: 25%!important;" data-now-value="16.66" data-number-of-steps="2"></div>
                </div>
                <div class="f1-step active" style="width: 50%;">
                  <div class="f1-step-icon" style="background-color:green"><i class="fa fa-user"></i></div>
                  <p>Etapa 1 - Confirmar Dados</p>
                </div>
                <div class="f1-step" style="width: 50%;">
                  <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                  <p>Etapa 2 - Certificado Digital</p>
                </div>
              </div>

              <!-- <fieldset style="border: none !important;"> -->
                <div class="card-body">
                  <div class="page-content fade-in-up">
                    <div class="ibox">
                        <div class="ibox-head">
                            <ul class="nav nav-tabs tabs-line">
                              <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Identificação</a></li>
                              <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Endereço</a></li>
                              <!-- <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Domicílio Tributário Eletrônico - DTE</a></li> -->
                              <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">Informações Complementares</a></li>
                              <!-- <li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab">Contador</a></li> -->
                              <li class="nav-item"><a class="nav-link" href="#tab_6" data-toggle="tab">Contatos&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li> 
                              <!-- <li class="nav-item"><a class="nav-link" href="#tab_7" data-toggle="tab">Senhas&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li>  -->
                              <li class="nav-item"><a class="nav-link" href="#tab_8" data-toggle="tab">Anotações&nbsp;<i class="fa fa-plus-circle text-success" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="ibox-body">
                            <?php echo form_open("empresa/cadastrar_empresa_etapa2/$tipo/$valor", array('role'=>'form')); ?>
                            <div class="tab-content">
                                
                            <!-- TAB 1 -->
                            <div class="tab-pane fade show active" id="tab_1">
                                <br>
                                <h3 class="box-title">Identificação</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>    
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Inscrição Estadual:</label>
                                             <?php echo form_input(array('name'=>'ie', 'class'=>'form-control', 'placeholder'=>'Inscrição Estadual', 'style'=>'border-color: #5c6bc0;'), trim($result['ie']), 'required readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>CNPJ:</label>
                                             <?php echo form_input(array('name'=>'cnpj', 'class'=>'form-control', 'placeholder'=>'CNPJ', 'style'=>'border-color: #5c6bc0;'), trim($result['cnpj']), 'required readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Razão Social:</label>
                                             <?php echo form_input(array('name'=>'razao_social', 'style'=>'font-weight:bold; color: #000000;', 'class'=>'form-control', 'placeholder'=>'Razão Social', 'style'=>'border-color: #5c6bc0;'), trim($result['razao_social']), 'required readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Micro Empreendedor Individual - MEI:</label>
                                        <?php echo form_input(array('name'=>'mei', 'class'=>'form-control', 'placeholder'=>'MEI', 'style'=>'border-color: #5c6bc0;'), utf8_decode(trim($result['mei'])), 'readonly'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Nome Fantasia:</label>
                                             <?php echo form_input(array('name'=>'nome_fantasia', 'class'=>'form-control', 'placeholder'=>'Nome Fantasia', 'style'=>'border-color: #5c6bc0;'), trim($result['nome_fantasia']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Natureza Jurídica:</label>
                                             <?php echo form_input(array('name'=>'natureza_juridica', 'class'=>'form-control', 'placeholder'=>'Natureza Jurídica', 'style'=>'border-color: #5c6bc0;'), trim($result['natureza_juridica']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Unidade de Atendimento:</label>
                                             <?php echo form_input(array('name'=>'unidade_atendimento', 'class'=>'form-control', 'placeholder'=>'Unidade de Atendimento', 'style'=>'border-color: #5c6bc0;'), trim($result['unidade_atendimento']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Unidade de Fiscalização:</label>
                                             <?php echo form_input(array('name'=>'unidade_fiscalizacao', 'class'=>'form-control', 'placeholder'=>'Unidade de Fiscalização', 'style'=>'border-color: #5c6bc0;'), trim($result['unidade_fiscalizacao']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group mb-4">
                                             <label>Atividade Econômica Principal:</label>
                                             <?php echo form_input(array('name'=>'atividade_principal', 'class'=>'form-control', 'placeholder'=>'Atividade Econômica Principal', 'style'=>'border-color: #5c6bc0;'), trim($result['atividade_principal']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group mb-4" style="color:red">
                                        <label>Selecione o Regime:</label>
                                          <?php 
                                          $valores_filtro = array(
                                              'SIMPLES'=>'Simples Nacional ',
                                              'LUCRO_REAL'=>'Lucro Real',
                                              'LUCRO_PRESUMIDO'=>'Lucro Presumido'
                                              
                                          );
                                          echo form_dropdown(array('class'=>'form-control', 'name'=>'tipo_regime', 'id'=>'tipo_regime', 'style'=>'color: white; background-color: #ef3636;'), $valores_filtro, $filtro);
                                          ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- TAB 2 -->
                            <div class="tab-pane fade" id="tab_2">
                                <br>
                                <h3 class="box-title">Endereço</h3>
                                <div class="col-lg-12">
                                    <hr align="center" width="100%" size="1" color=#e4e9f0>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>CEP</label>
                                             <?php echo form_input(array('name'=>'cep', 'class'=>'form-control', 'placeholder'=>'CEP', 'style'=>'border-color: #5c6bc0;'), trim($result['cep']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Logradouro</label>
                                             <?php echo form_input(array('name'=>'logradouro', 'class'=>'form-control', 'placeholder'=>'Logradouro', 'style'=>'border-color: #5c6bc0;'), trim($result['logradouro']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Número</label>
                                             <?php echo form_input(array('name'=>'numero', 'class'=>'form-control', 'placeholder'=>'Número', 'style'=>'border-color: #5c6bc0;'), trim($result['numero']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Complemento</label>
                                             <?php echo form_input(array('name'=>'complemento', 'class'=>'form-control', 'placeholder'=>'Complemento', 'style'=>'border-color: #5c6bc0;'), trim($result['complemento']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Bairro/Distrito</label>
                                             <?php echo form_input(array('name'=>'bairro', 'class'=>'form-control', 'placeholder'=>'Bairro', 'style'=>'border-color: #5c6bc0;'), trim($result['bairro']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Cidade</label>
                                             <?php echo form_input(array('name'=>'cidade', 'class'=>'form-control', 'placeholder'=>'Cidade', 'style'=>'border-color: #5c6bc0;'), trim($result['cidade']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>UF</label>
                                             <?php echo form_input(array('name'=>'uf', 'class'=>'form-control', 'placeholder'=>'UF', 'style'=>'border-color: #5c6bc0;'), trim($result['uf']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Referência</label>
                                             <?php echo form_input(array('name'=>'referencia', 'class'=>'form-control', 'placeholder'=>'Referência', 'style'=>'border-color: #5c6bc0;'), trim($result['referencia']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Localização</label>
                                             <?php echo form_input(array('name'=>'localizacao', 'class'=>'form-control', 'placeholder'=>'Localização', 'style'=>'border-color: #5c6bc0;'), trim($result['localizacao']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-4">
                                             <label>Telefone</label>
                                             <?php echo form_input(array('name'=>'telefone', 'class'=>'form-control', 'placeholder'=>'Telefone', 'style'=>'border-color: #5c6bc0;'), trim($result['telefone']), 'readonly'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group mb-4">
                                             <label>E-mail</label>
                                             <?php echo form_input(array('name'=>'email', 'class'=>'form-control', 'placeholder'=>'E-mail', 'style'=>'border-color: #5c6bc0;'), trim($result['email']), 'readonly'); ?>
                                        </div>
                                    </div>
                                </div>                              
                            </div>

                            <!-- TAB 3 -->
                            <div class="tab-pane fade" id="tab_3">
                                <br>
                                <h3 class="box-title">Domicílio Tributário Eletrônico - DTE</h3>
                                    <div class="col-lg-12">
                                        <hr align="center" width="100%" size="1" color=#e4e9f0>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Situação do DTE</label>
                                                <?php echo form_input(array('name'=>'situacao_dte', 'class'=>'form-control', 'placeholder'=>'Situação do DTE', 'style'=>'border-color: #5c6bc0;'), trim($result['situacao_dte']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Situação da Conta</label>
                                                <?php echo form_input(array('name'=>'situacao_conta_dte', 'class'=>'form-control', 'placeholder'=>'Situação da Conta', 'style'=>'border-color: #5c6bc0;'), trim($result['situacao_conta_dte']), 'readonly'); ?>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <!-- TAB 4 -->
                            <div class="tab-pane fade" id="tab_4">
                                <br>
                                <h3 class="box-title">Informações Complementares</h3>
                                    <div class="col-lg-12">
                                        <hr align="center" width="100%" size="1" color=#e4e9f0>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Situação Cadastral</label>
                                                <?php echo form_input(array('name'=>'situacao_cadastral', 'class'=>'form-control', 'placeholder'=>'Situação Cadastral', 'style'=>'border-color: #5c6bc0;'), trim($result['situacao_cadastral']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Situação</label>
                                                <?php echo form_input(array('name'=>'situacao', 'class'=>'form-control', 'placeholder'=>'Situação', 'style'=>'border-color: #5c6bc0;'), trim($result['credenciado']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Motivo do Credenciamento / Descredenciamento</label>
                                                <?php echo form_input(array('name'=>'motivo', 'class'=>'form-control', 'placeholder'=>'Motivo', 'style'=>'border-color: #5c6bc0;'), trim($result['motivo']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Condição</label>
                                                <?php echo form_input(array('name'=>'condicao', 'class'=>'form-control', 'placeholder'=>'Condição', 'style'=>'border-color: #5c6bc0;'), trim($result['condicao']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Forma de Pagamento</label>
                                                <?php echo form_input(array('name'=>'forma_pagamento', 'class'=>'form-control', 'placeholder'=>'Forma de Pagamento', 'style'=>'border-color: #5c6bc0;'), trim($result['forma_pagamento']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>NIRE</label>
                                                <?php echo form_input(array('name'=>'nire', 'class'=>'form-control', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), $empresa->nire, ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group mb-4">
                                                <label>Motivo da Situação Cadastral</label>
                                                <?php echo form_input(array('name'=>'motivo_situacao_cadastral', 'class'=>'form-control', 'placeholder'=>'Motivo desta Situação Cadastral', 'style'=>'border-color: #5c6bc0;'), trim($result['motivo_situacao_cadastral']), 'readonly'); ?>
                                            </div>
                                        </div>
                                    </div>       
                            </div>

                            <!-- TAB 5 -->
                            <div class="tab-pane fade" id="tab_5">
                                <br>
                                <h3 class="box-title">Informações do Contador</h3>
                                    <div class="col-lg-12">
                                        <hr align="center" width="100%" size="1" color=#e4e9f0>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Nome do Contador</label>
                                                <?php echo form_input(array('name'=>'nome_contador', 'class'=>'form-control', 'placeholder'=>'Nome do Contador', 'style'=>'border-color: #5c6bc0;'), trim($result['nome_contador']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>CRC do Contador</label>
                                                <?php echo form_input(array('name'=>'crc_contador', 'class'=>'form-control', 'placeholder'=>'CRC do Contador', 'style'=>'border-color: #5c6bc0;'), trim($result['crc_contador']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Nome do Responsável</label>
                                                <?php echo form_input(array('name'=>'nome_responsavel', 'class'=>'form-control', 'placeholder'=>'Nome do Responsável', 'style'=>'border-color: #5c6bc0;'), trim($result['nome_responsavel']), 'readonly'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>CRC do Responsável</label>
                                                <?php echo form_input(array('name'=>'crc_responsavel', 'class'=>'form-control', 'placeholder'=>'CRC do Responsável', 'style'=>'border-color: #5c6bc0;'), trim($result['crc_responsavel']), 'readonly'); ?>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <!-- TAB 6 -->
                            <div class="tab-pane fade" id="tab_6">
                                <br>
                                <h3 class="box-title">Adicionar Contatos</h3>
                                    <div class="col-lg-12">
                                        <hr align="center" width="100%" size="1" color=#e4e9f0>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Telefone Alternativo</label>
                                                <?php echo form_input(array('type'=>'tel', 'maxlength'=>'14', 'onKeyPress'=>'MascaraTelefone(this)', 'name'=>'telefone_alternativo', 'id'=>'telefone_alternativo', 'class'=>'form-control', 'placeholder'=>'Telefone Alternativo', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Celular Alternativo</label>
                                                <?php echo form_input(array('type'=>'tel', 'maxlength'=>'15', 'onKeyPress'=>'MascaraCelular(this)', 'name'=>'celular_alternativo', 'id'=>'celular_alternativo', 'class'=>'form-control', 'placeholder'=>'Celular Alternativo', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>E-mail Alternativo</label>
                                                <?php echo form_input(array('type'=>'email', 'name'=>'email_alternativo', 'class'=>'form-control', 'placeholder'=>'E-mail Alternativo', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Colaborador Principal</label>
                                                <?php echo form_dropdown(array('name'=>'id_funcionario', 'class'=>'js-example-basic-single col-sm-6 form-control', 'data-live-search'=>'true', 'placeholder'=>'Selecione o Funcionário'), $lista_funcionarios, '', ''); ?>
                                            </div>
                                        </div>  

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Colaboradores Auxiliares</label>
                                                <?php echo form_dropdown('id_usuario[]', $lista_funcionarios2, '', 'class="form-control js-example-basic-multiple" id="id_empresa_usuario" multiple="multiple" style="width: 500px;"');?>
                                            </div>
                                        </div> 

                                    </div>
                            </div>

                            <!-- TAB 7 -->
                            <div class="tab-pane fade" id="tab_7">
                                <br>
                                <h3 class="box-title">Senhas</h3>
                                    <div class="col-lg-12">
                                        <hr align="center" width="100%" size="1" color=#e4e9f0>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Login DTE Sefaz</label>
                                                <?php echo form_input(array('name'=>'login_sefaz', 'class'=>'form-control', 'placeholder'=>'Login Sefaz', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>Senha DTE Sefaz</label>
                                                <?php echo form_input(array('name'=>'senha_sefaz', 'class'=>'form-control', 'placeholder'=>'Senha Sefaz', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group mb-4">
                                                <label>CPF do Representante</label>
                                                <?php echo form_input(array('name'=>'login_mei', 'class'=>'form-control', 'id'=>'cpf', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6" style="display: none;">
                                            <div class="form-group mb-4">
                                                <label>Código de Acesso - Simples Nacional</label>
                                                <?php echo form_input(array('name'=>'senha_mei', 'class'=>'form-control', 'id'=>'codigodeacesso', 'placeholder'=>'Código de Acesso - Simples Nacional', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6" style="display: none;">
                                            <div class="form-group mb-4">
                                                <label></label>
                                                <?php echo form_input(array('name'=>'flag_empresa_sem_ie', 'class'=>'form-control', 'placeholder'=>'Flag empresa sem ie'), trim($result['flag_empresa_sem_ie']), 'readonly'); ?>
                                            </div>
                                        </div>

                                    </div>
                            </div>

                            <!-- TAB 8 -->
                            <div class="tab-pane fade" id="tab_8">
                                <br>
                                <h3 class="box-title">Anotações</h3>
                                    <div class="col-lg-12">
                                        <hr align="center" width="100%" size="1" color=#e4e9f0>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" hidden="true">
                                            <div class="form-group mb-4">
                                                <label>CPF do Representante</label>
                                                <?php echo form_input(array('name'=>'cpf_alvara', 'class'=>'form-control', 'placeholder'=>'Digite sem pontuação', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div> 

                                        <div class="col-md-12">
                                            <div class="form-group mb-4">
                                                <label>Anotações</label>
                                                <?php echo form_textarea(array('name'=>'anotacoes', 'class'=>'form-control', 'placeholder'=>'Anotações', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                                            </div>
                                        </div>

                                    </div>
                            </div>

                            </div>
                            </div>
                            <div class="ibox-footer text-right">
                                <?php echo form_submit(array('name'=>'btn_cadastrar', 'class'=>'btn btn-primary mr-2'), 'Próxima Etapa'); ?>
                                <a href="<?php echo base_url("empresa/cadastrar_empresa_etapa1"); ?>" class="btn btn-outline-secondary">Cancelar</a>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>

                
              <!-- </fieldset> -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>



<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/form-wizard/form-wizard-three.js'); ?>"></script>