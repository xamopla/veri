<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Cadastrar Procuração</h2>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <label style="color: red;">Verifique as informações corretamente, ao pular de etapa não será possível voltar a anterior!</label>
          </div>
          <div class="card-body">
            <div class="f1" >
              <div class="f1-steps" >
                <div class="f1-progress">
                  <div class="f1-progress-line" style="width: 85%!important;" data-now-value="16" data-number-of-steps="2"></div>
                </div>
                <div class="f1-step activated" style="width: 33%;">
                  <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                  <p>Etapa 1 - Dados</p>
                </div>
                <div class="f1-step activated" style="width: 33%;">
                  <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                  <p>Etapa 2 - Procuração Digital</p>
                </div>
                <div class="f1-step active" style="width: 33%;">
                  <div class="f1-step-icon" style="background-color:green"><i class="fa fa-sitemap"></i></div>
                  <p>Etapa 3 - Vincular Empresas</p>
                </div>
              </div>

              <!-- <fieldset style="border: none !important;"> -->
                <div class="card-body">
                    <?php echo form_open("contador/cadastrar_novo_etapa3/$id/", array('role'=>'form')); ?>

                    <div class="row">
                        <a href="<?php echo base_url("empresa/cadastrar_empresa_etapa1"); ?>" target="_blank" class="btn btn-primary mr-2">Cadastrar Empresa</a> 
                        <a href="javascript:location.reload();" class="btn btn-secondary mr-2">Atualizar Lista de Empresas</a> 
                    </div>
                    <br>
                    <br>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-4">
                                <a id="select-all" href="#" class="btn btn-success">Vincular Todas</a>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group mb-4">
                                <a id="deselect-all" href="#" style="float: right;" class="btn btn-danger">Desvincular Todas</a>
                            </div>
                        </div>

                        <div class="col-md-12" style="width: inherit !important;">
                            <select class="searchable" id="custom-headers" multiple="multiple" name="multi_select[]">
                                <?php foreach ($empresas as $row) : ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->selected; ?>><?php echo $row->razao_social.' - '.$row->cnpj_completo; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    
                <br>
                <div class="ibox-footer text-right">
                    <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Finalizar'); ?>
                </div>
                <?php echo form_close(); ?>
                </div>

                
              <!-- </fieldset> -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>



<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/form-wizard/form-wizard-three.js'); ?>"></script>



<script src="<?php echo base_url('assets/plugins/multiselect/js/jquery.multi-select.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/multiselect/js/jquery.quicksearch.js'); ?>"></script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
        toastr["warning"]("É necessário vincular ao menos uma empresa a procuração!", "Erro!");
    <?php } ?>
</script>

<script type="text/javascript">
    //$('#custom-headers').multiSelect();

    $('.searchable').multiSelect({
        selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Buscar...'>",
        selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Buscar...'>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    $('#select-all').click(function(){
        $('#custom-headers').multiSelect('select_all');
        return false;
    });
    $('#deselect-all').click(function(){
        $('#custom-headers').multiSelect('deselect_all');
        return false;
    });
</script>