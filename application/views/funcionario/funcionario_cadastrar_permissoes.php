<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Cadastrar Permissões</h2><span>Marque ou Desmarque as opções abaixo:</span>
              </div>
              <div class="card-body">
                <?php echo form_open("funcionario/cadastrar_permissoes/$id", array('role'=>'form')); ?>
                  <div class="row">
                      

                    <div class="col-md-2">
                        <div class="form-group mb-4">
                          <label style="font-size: 25px; color: #dca21e;">Certidão Negativa:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mCertidaoNegativa" value="1" checked="checked"><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>  

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Federal:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mFederal" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Dashboard:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mDashboardFederal" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Msg e-CAC:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mMsgEcac" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Situação Fiscal:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mSituacaoFiscalEcac" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Parcelamento DAS:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mParcelamentoDasEcac" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Dívida Ativa:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mDividaAtiva" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Cadastros:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mCadastros" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Empresas:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mEmpresas" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Usuários:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mUsuarios" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Contadores:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mContadores" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Documentos:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mDocumentos" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Tipos de Doc.:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mTiposDeDocumentos" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div>

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Calendário:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mCalendario" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div>

                  <!-- <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Protocolos:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mProtocolos" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div> -->

                  <hr>
                  <!-- <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Relatórios:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorios" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Pendências por Empresa:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioPorEmpresas" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Timeline:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioTimeline" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Geral:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioGeral" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Empresas por Colaborador:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioPorColaborador" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Empresas sem Colaborador:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioSemColaborador" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Empresas com Certificado:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioComCertificado" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Empresas sem Certificado:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioSemCertificado" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div> -->

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Painel:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mPainel" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Registros de Acesso:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRegistroDeAcesso" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Plano:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mPlano" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <label style="font-size: 25px; color: #dca21e;">Painel do Cliente:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mPainelCliente" value="1" checked="checked"><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                          </div>
                      </div> 
                  </div>

               <div class="ibox-footer text-right">
                  <?php echo form_submit(array('name'=>'btn_cadastrar', 'class'=>'btn btn-primary mr-2'), 'Cadastrar'); ?> 
              </div>
              <?php echo form_close(); ?>                
              </div>

             

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  
 