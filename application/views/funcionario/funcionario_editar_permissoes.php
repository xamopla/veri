<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Editar Permissões</h2><span>Marque ou Desmarque as opções abaixo:</span>
              </div>
              <div class="card-body">
                <?php echo form_open("funcionario/editar_permissoes/$id", array('role'=>'form')); ?>
                  <?php   
                    $x = unserialize($funcionario->permissoes_nova);
                  ?>
                  <div class="row">
                      <!-- <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mEstadual', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Estadual:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mEstadual" value="1" <?php if($x['mEstadual'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

                  <div class="row">
                    <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mDashboardSefaz', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">Dashboard Sefaz:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mDashboardSefaz" value="1" <?php if($x['mDashboardSefaz'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mMsgDte', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">Msg DTE:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mMsgDte" value="1" <?php if($x['mMsgDte'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mResumoFiscal', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">Resumo Fiscal:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mResumoFiscal" value="1" <?php if($x['mResumoFiscal'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mSituacaoFiscal', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">Situação Fiscal:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mSituacaoFiscal" value="1" <?php if($x['mSituacaoFiscal'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div> -->

                    <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mCertidaoNegativa', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">Certidão Negativa:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mCertidaoNegativa" value="1" <?php if($x['mCertidaoNegativa'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div>

                    <!-- <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mProcessosSipro', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">Processos Sipro:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mProcessosSipro" value="1" <?php if($x['mProcessosSipro'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div> -->

                    <!-- <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mCompraVendas', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">Compra X Vendas:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mCompraVendas" value="1" <?php if($x['mCompraVendas'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div> -->

                    <!-- <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mIpva', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">IPVA:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mIpva" value="1" <?php if($x['mIpva'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mJucebViabilidade', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">JUCEB - Viabilidade:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mJucebViabilidade" value="1" <?php if($x['mJucebViabilidade'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div> -->

                    <!-- <div class="col-md-2">
                        <div class="form-group mb-4">
                          <?php
                          if(array_key_exists('mJucebRegistro', $x)){ ?>
                          <label style="font-size: 25px; color: #dca21e;">JUCEB - Registro:</label>
                          <div class="media"> 
                            <div class="media-body icon-state">
                              <label class="switch">
                                <input type="checkbox" name="mJucebRegistro" value="1" <?php if($x['mJucebRegistro'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                              </label>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                    </div> -->
                  </div> 

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mFederal', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Federal:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mFederal" value="1" <?php if($x['mFederal'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mDashboardFederal', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Dashboard:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mDashboardFederal" value="1" <?php if($x['mDashboardFederal'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mMsgEcac', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Msg e-CAC:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mMsgEcac" value="1" <?php if($x['mMsgEcac'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mSituacaoFiscalEcac', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Situação Fiscal:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mSituacaoFiscalEcac" value="1" <?php if($x['mSituacaoFiscalEcac'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mParcelamentoDasEcac', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Parcelamento DAS:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mParcelamentoDasEcac" value="1" <?php if($x['mParcelamentoDasEcac'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mDividaAtiva', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Dívida Ativa:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mDividaAtiva" value="1" <?php if($x['mDividaAtiva'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>
                  </div>

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mCadastros', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Cadastros:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mCadastros" value="1" <?php if($x['mCadastros'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mEmpresas', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Empresas:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mEmpresas" value="1" <?php if($x['mEmpresas'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mUsuarios', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Usuários:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mUsuarios" value="1" <?php if($x['mUsuarios'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mContadores', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Contadores:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mContadores" value="1" <?php if($x['mContadores'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mDocumentos', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Documentos:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mDocumentos" value="1" <?php if($x['mDocumentos'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mTiposDeDocumentos', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Tipos de Doc.:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mTiposDeDocumentos" value="1" <?php if($x['mTiposDeDocumentos'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mCalendario', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Calendário:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mCalendario" value="1" <?php if($x['mCalendario'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

                  <!-- <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mProtocolos', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Protocolos:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mProtocolos" value="1" <?php if($x['mProtocolos'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div> -->

                 <!--  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorios', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Relatórios:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorios" value="1" <?php if($x['mRelatorios'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorioPorEmpresas', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Pendências por Empresa:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioPorEmpresas" value="1" <?php if($x['mRelatorioPorEmpresas'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorioTimeline', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Timeline:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioTimeline" value="1" <?php if($x['mRelatorioTimeline'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorioGeral', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Geral:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioGeral" value="1" <?php if($x['mRelatorioGeral'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorioPorColaborador', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Empresas por Colaborador:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioPorColaborador" value="1" <?php if($x['mRelatorioPorColaborador'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorioSemColaborador', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Empresas sem Colaborador:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioSemColaborador" value="1" <?php if($x['mRelatorioSemColaborador'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorioComCertificado', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Empresas com Certificado:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioComCertificado" value="1" <?php if($x['mRelatorioComCertificado'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRelatorioSemCertificado', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Empresas sem Certificado:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRelatorioSemCertificado" value="1" <?php if($x['mRelatorioSemCertificado'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>
                  </div> -->

                  <hr>
                  <div class="row">
                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mPainel', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Painel:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mPainel" value="1" <?php if($x['mPainel'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

                  <div class="row"> 
                    <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mRegistroDeAcesso', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Registros de Acesso:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mRegistroDeAcesso" value="1" <?php if($x['mRegistroDeAcesso'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mPlano', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Plano:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mPlano" value="1" <?php if($x['mPlano'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div>

                      <div class="col-md-2">
                          <div class="form-group mb-4">
                            <?php
                            if(array_key_exists('mPainelCliente', $x)){ ?>
                            <label style="font-size: 25px; color: #dca21e;">Painel do Cliente:</label>
                            <div class="media"> 
                              <div class="media-body icon-state">
                                <label class="switch">
                                  <input type="checkbox" name="mPainelCliente" value="1" <?php if($x['mPainelCliente'] == '1'){echo 'checked';}?>><span class="switch-state"></span>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                          </div>
                      </div> 
                  </div>

               <div class="ibox-footer text-right">
                  <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                  <a href="<?php echo base_url("funcionario/listar"); ?>" class="btn btn-secondary">Voltar</a> 
              </div>
              <?php echo form_close(); ?>                
              </div>

             

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  
 