<link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>" rel="stylesheet" />
<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Cadastrar Colaborador</h2><span>Preencha os campos abaixo:</span>
              </div>
              <div class="card-body">
                <?php echo form_open("funcionario/vincular_empresas/$id/", array('role'=>'form')); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-4">
                                <a id="select-all" href="#" class="btn btn-success">Vincular Todas</a>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group mb-4">
                                 <a id="deselect-all" href="#" style="float: right;" class="btn btn-danger">Desvincular Todas</a>
                            </div>
                        </div>

                        <div class="col-md-12" style="width: inherit !important;">
                            <select class="searchable" id="custom-headers" multiple="multiple" name="multi_select[]">
                                <?php foreach ($empresas as $row) : ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->selected; ?>><?php echo $row->razao_social.' - '.$row->cnpj_completo; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="ibox-footer text-right">
                    <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                    <a href="<?php echo base_url("funcionario/listar"); ?>" class="btn btn-outline-secondary">Voltar</a>
                    </div>

                </div>
                <br>
                
                <?php echo form_close(); ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  

<script src="<?php echo base_url('assets/plugins/multiselect/js/jquery.multi-select.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/multiselect/js/jquery.quicksearch.js'); ?>"></script>
<script type="text/javascript">
    //$('#custom-headers').multiSelect();

    $('.searchable').multiSelect({
  selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Buscar...'>",
  selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Buscar...'>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
  }
});

  $('#select-all').click(function(){
  $('#custom-headers').multiSelect('select_all');
  return false;
});
$('#deselect-all').click(function(){
  $('#custom-headers').multiSelect('deselect_all');
  return false;
});
</script>