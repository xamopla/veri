<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Editar Colaborador</h2><span>Preencha os campos abaixo:</span>
              </div>
              <div class="card-body">
                <?php echo form_open("funcionario/editar/$id/$id_cadastro", array('role'=>'form')); ?>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group mb-4">
                            <label>Nome *</label>
                            <?php echo form_input(array('name'=>'nome', 'class'=>'form-control', 'style'=>'border-color: #5c6bc0;'), $funcionario->nome, 'required'); ?>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group mb-4">
                              <label>Login: *</label>
                              <?php echo form_input(array('type'=>'email', 'name'=>'login', 'class'=>'form-control', 'style'=>'border-color: #5c6bc0;'), $funcionario->login, 'required'); ?>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group mb-4">
                            <label>Senha: *</label>
                            <?php echo form_input(array('name'=>'senha', 'type'=>'password', 'class'=>'form-control', 'style'=>'border-color: #5c6bc0;'), $funcionario->senha, 'required'); ?>
                          </div>
                      </div> 
                  </div>  

                  <div class="ibox-footer text-right">
                    <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                    <a href="<?php echo base_url("funcionario/listar"); ?>" class="btn btn-outline-secondary">Voltar</a>
                </div>
                <?php echo form_close(); ?>
                              
              </div>

              

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->  

<script>
    $("#form-sample-1").validate({
        rules: {                
            password_confirmation: {
                required: !0,
                equalTo: "#senha"
            }
        },
        errorClass: "help-block error",
        highlight: function(e) {
            $(e).closest(".form-group.row").addClass("has-error")
        },
        unhighlight: function(e) {
            $(e).closest(".form-group.row").removeClass("has-error")
        },
    });
</script>