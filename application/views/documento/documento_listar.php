<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>
<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1200px;
        max-width : 1300px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2>Lista de Documentos</h2>
            <div class="card-header-action" style="margin-left: auto;">
            <a href="#exampleModal" data-toggle="modal" class="btn btn-icon btn-primary pull-right"><i class="fa fa-filter"></i></a>
            <a class="btn btn-default btn-rounded pull-right" style="color: #FFFFFF; background-color: #24a0e9;margin-right: 5px !important;" href="<?php echo base_url("documento/cadastrar"); ?>" id="passo22">Novo Documento</a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="example" data-toggle="datatables">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th id="passo23">Empresa</th>
                            <th>CNPJ</th>
                            <th id="passo24">Tipo Documento</th>
                            <th>Data Emissão</th>
                            <th id="passo25">Data Validade</th>
                            <th width="5%;" id="passo26">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if ($consulta){ 
                        foreach ($consulta as $e){  
                    ?>
                     
                        <tr>
                            <td align="left"><?php echo $e->razao_social; ?></td>
                            <td align="left"><?php echo $e->cnpj_completo; ?></td>
                            <td align="left"><?php echo $e->nome; ?></td>
                            <td><?php echo date('d/m/Y', strtotime($e->dataEmissao)); ?></td>
                            <td><?php 

                            //$data_inicial = strtotime($e->dataEmissao);
                            $data_final = strtotime($e->dataValidade);
                            $data_atual = strtotime(date(''). ' + 15 days');

                            $df = new DateTime($e->dataValidade);
                            $da = new DateTime(date(''));
                            $intervalo = $df->diff($da);

                            if ($data_final < strtotime(date('Y-m-d'))) {
                              echo "<span class='badge badge-danger badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Vencido' data-content='Vencido à ".$intervalo->format('%a')." dia(s)'>".date('d/m/Y', strtotime($e->dataValidade))."</span>";
                            } else if ($intervalo->format('%a') <= $e->diasNotificacao) {
                              echo "<span class='badge badge-warning badge-shadow' data-container='body' data-toggle='popover' data-trigger='hover' data-placement='top' title='Próximo de Vencer' data-content='Faltam ".$intervalo->format('%a')." dia(s) para vencer'>".date('d/m/Y', strtotime($e->dataValidade))."</span>";
                            } else{
                              echo "<span class='badge badge-success badge-shadow'  data-container='body' style='background-color:#2ecc71!important;' data-toggle='popover' data-trigger='hover' data-placement='top' title='Documento dentro do prazo' data-content='Faltam ".$intervalo->format('%a')." dia(s) para vencer'>".date('d/m/Y', strtotime($e->dataValidade))."</span>";
                            }

                            ?>                        
                            </td>
                            <td style="width: 14%;">

                            <?php if ($e->id_tipoDocumento == '8'): ?>

                            <a disabled style="cursor:not-allowed;padding: 1px !important;" data-toggle="tooltip" data-placement="top" title="A renovação de Certificados Digitais é feita na tela de empresas" class="btn btn-default b" ><i class="material-icons" aria-hidden="true" style="color: #24a0e9;">today</i></a>
                            <a disabled style="cursor:not-allowed;padding: 1px !important;" data-toggle="tooltip" data-placement="top" title="A renovação de Certificados Digitais é feita na tela de empresas" class="btn btn-default b"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">create</i></a>    
                            <?php else: ?>
                            
                            <a href="<?php echo base_url("documento/renovardocumento/$e->id"); ?>" data-toggle="tooltip" data-placement="top" title="Renovar" class="btn btn-default b" style="padding: 1px !important;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">today</i></a>
                            <a href="<?php echo base_url("documento/editar/$e->id"); ?>" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-default b" style="padding: 1px !important;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">create</i></a>  

                            <a href="<?php echo base_url("documento/editar_aux/$e->id/"); ?>" data-toggle="tooltip" data-placement="top" title="Ver Documentos Anexados" class="btn btn-default b" style="padding: 1px !important;"><i class="fa fa-paperclip" style="color: #24a0e9; font-size: 22px;"></i></a>   
                              
                            <?php endif ?>

                            <a href="#" data-toggle="tooltip" data-placement="top" title="Alterar Tipo de Documento" onclick='modalTipoDocumento(<?php echo $e->id_tipoDocumento; ?>,<?php echo $e->id; ?>)'><span class="btn btn-default b" style="padding: 1px 4px;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">autorenew</i></span></a>

                            <a href="#" data-toggle="tooltip" data-placement="top" title="Excluir" onclick='preencher_url("<?php echo base_url("documento/excluir/$e->id"); ?>")'><span class="btn sweet-7" style="padding: 1px 4px;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">delete_forever</i></span></a>
                            </td>
                        </tr>
                    <?php }
                    } ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModal">Filtro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="">
          <div class="form-group">
            <label>Status:</label>
              <?php 
              $valores_filtro = array(
                  'listar'=>'Todas',
                  'listardocumentoproxvenci'=>'Próximos a Vencer',
                  'listardocumentosvencidos'=>'Vencidos',
                  'listardocumentosregulares'=>'Regulares'                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
          </div> 
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="tipoDocumentoModal" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="tipoDocumentoformModal">Alterar Tipo de Documento</h5>
        <input type="hidden" name="id_documento" id="id_documento">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="">
          <div class="form-group">
            <label>Tipos de Documentos:</label>
              <?php 

              echo form_dropdown(array('class'=>'form-control', 'id'=>'tipo_documento'), $lista_documentos, $filtro); 
              ?>
          </div> 
        </form>
        <div class="modal-footer justify-content-between bg-primary-50">           
              <button class="btn btn-primary btn-rounded mr-3" onclick="salva_tipo_documento()" >Salvar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('documento'); ?>/"+filtro+"");
  });
});
</script>

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }

    function iniciaTour5(){
      var tour = new Tour({
        storage: false,
        steps: [
            {
                element: "#passo21",
                title: "Tela de Documentos",
                content: "Esta é a tela de documentos, aqui você poderá cadastrar documentos. Escolha a empresa cujo documento pertence, a data de emissão/validade e se precisar anexe algum arquivo.",
                placement: 'auto',
                backdrop: true
            },

            {
                element: "#passo22",
                title: "Botão Cadastrar",
                content: "Para cadastrar um novo documento clique neste botão.",
                placement: 'auto',
                backdrop: true
            },

            {
                element: "#passo23",
                title: "Empresa",
                content: "Este campo mostrará a qual empresa o documento pertence.",
                placement: 'auto',
                backdrop: true
            },

            {
                element: "#passo24",
                title: "Tipo de documento",
                content: "Este campo mostrará qual o tipo de documento (ex: Certificado Digital, CND da Receita Federal, etc).",
                placement: 'auto',
                backdrop: true
            },

            {
                element: "#passo25",
                title: "Data de Validade",
                content: "Este campo mostrará a data de validade dos documentos e quantos dias restam para o vencimento. Esteja sempre atento as cores: (verde - dentro do prazo, amarelo - próximo de vencer, vermelho - vencido).",
                placement: 'auto',
                backdrop: true
            },

            {
                element: "#passo26",
                title: "Ações",
                content: "Nas ações você poderá editar, excluir ou renovar o documento. Para alguns documentos você terá um link personalizado para facilitar o processo de renovação.",
                placement: 'auto',
                backdrop: true
            },

            {         
            title: "Calendário",
            content: "Bem vindo ao Calendário",
              onNext: function() {
                window.location = "<?php echo base_url();?>calendario/listar/1";
              }
            }
        ]
        });

        tour.init();
        tour.start();
    }
</script>

<?php if(isset($fromTour)){
  echo '<script type="text/javascript">',
        '$(document).ready(function() { iniciaTour5(); });',
        '</script>';
;} ?>

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
    var filtro = $('#filtro option:selected').val();
    window.location.replace("<?php echo base_url('documento'); ?>/"+filtro+"");
  });
});
</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
    toastr["success"]("O documento foi excluído com sucesso!", "Excluído!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["warning"]("Ocorreu um erro, tente novamente!", "Erro!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {
$('[data-toggle="popover"]').popover({
  boundary:'window',
  html: true
});
$.fn.dataTable.moment('DD/MM/YYYY');
var table = $('#example').DataTable({
        "order": [[ 4, "asc" ]],    
        "iDisplayLength": 50,
        lengthChange: false,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
      });
  
      table.search('<?php echo $razao_social_filtro; ?>').draw();
      table.buttons().container()
      .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      


});


function modalTipoDocumento(id_tipo_documento, id_documento){
  $("#id_documento").val(id_documento);
  $("#tipo_documento").val(id_tipo_documento);
  $('#tipoDocumentoModal').modal();
}

function salva_tipo_documento(){
  var id_documento = $("#id_documento").val();
  var tipo_documento = $("#tipo_documento").val();

  $.post("<?php echo base_url();?>documento/update_tipo_documento",
  {
      id_documento:id_documento,
      tipo_documento:tipo_documento
  },
  function(data){
    if(data == 1){
      window.location = global_url;
    }
  });
}
</script>