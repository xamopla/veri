<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Editar Documento</h2><span>Preencha os campos abaixo:</span>
              </div>
              <div class="card-body">
                <?php echo form_open_multipart("documento/editar/$id", array('role'=>'form')); ?>
            <!-- TABS -->
                <ul class="nav nav-pills nav-pills-rounded nav-pills-air nav-pills-success">
                    <li class="nav-item">
                        <a class="nav-link active" href="#tab-1-1" data-toggle="tab">Documento</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tab-1-2" data-toggle="tab">Anexos</a>
                    </li>
                </ul>
                <!-- FIM DA SELEÇÃO DAS TABS -->

                <!-- CONTEÚDO DAS TABS -->
                <div class="tab-content">

                    <!-- TAB 1 -->
                    <div class="tab-pane fade show active" id="tab-1-1">
                        <br>
                        <!-- <div class="text-muted mb-4 mt-4">Status <span class="fa fa-question-circle" data-toggle="tooltip" title="Ative ou Desative o tipo de documento"></span></div>
                        <div class="form-group">
                            <div class="mb-2">
                                <label class="radio radio-inline radio-ebony">
                                    <input type="radio" name="ativo" value="Sim" <?php if ($documento->ativo == 'Sim') echo "checked"; ?>/>
                                    <span class="input-span"></span>Ativo</label>
                                <label class="radio radio-inline radio-ebony">
                                    <input type="radio" name="ativo" value="Finalizado" <?php if ($documento->ativo == 'Finalizado') echo "checked"; ?>/>
                                    <span class="input-span"></span>Inativo</label>
                            </div>
                        </div>   -->                          

                        <div class="row">                                
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="nome">Empresa*:</label>
                                    <?php echo form_dropdown(array('name'=>'id_empresa', 'class'=>'js-example-basic-single form-control', 'placeholder'=>''), $lista_empresas, $documento->id_empresa, 'required'); ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="nome">Tipo de Documento*:</label>
                                    <?php echo form_dropdown(array('name'=>'id_tipoDocumento', 'class'=>'js-example-basic-single form-control', 'placeholder'=>''), $lista_tipodedocumentos, $documento->id_tipoDocumento, 'required'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">                                
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="nome">Número do Documento:</label>
                                    <?php echo form_input(array('name'=>'numero_documento', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), $documento->numero_documento, ''); ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="nome">Número do Protocolo:</label>
                                    <?php echo form_input(array('name'=>'numero_protocolo', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), $documento->numero_protocolo, ''); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">                                
                            <div class="col-md-4">
                                <div class="form-group mb-4">
                                    <label for="nome">Data de Emissão*:</label>
                                    <?php echo form_input(array('name'=>'dataEmissao', 'id'=>'date_2', 'class'=>'form-control', 'data-language'=>'en', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), date('d/m/Y', strtotime($documento->dataEmissao)), ''); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-4">
                                    <label for="nome">Data de Validade*:</label>
                                    <?php echo form_input(array('name'=>'dataValidade', 'id'=>'date_3', 'class'=>'form-control', 'data-language'=>'en', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), date('d/m/Y', strtotime($documento->dataValidade)), ''); ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-4">
                                    <label for="nome">Dias para Notificação*: <span class="fa fa-question-circle" data-toggle="tooltip" title="Defina a quantidade de dias antes do vencimento que você deseja ser alertado ao acesar o sistema."></span></label>
                                    <?php echo form_input(array('name'=>'diasNotificacao', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), $documento->diasNotificacao, ''); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">                                
                            <div class="col-md-12">
                                <div class="form-group mb-4">
                                    <label for="observacao">Observação:</label>
                                    <?php echo form_textarea(array('name'=>'observacao', 'class'=>'form-control', 'placeholder'=>'', 'rows'=>'3', 'style'=>'border-color: #5c6bc0;'), $documento->observacoes, ''); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">                                
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <label for="logo">Anexo</label>
                                    <br> 
                                    <input name="anexo1" class="file" type="file" accept="image/*,.pdf,.xlsx,.csv,.doc,.txt" /> 
                                    <br>
                                    <small class="text-muted">Extensões permitidas: | gif | jpeg | jpg | png | doc | docx | txt | xls | xlsx | pdf | Tamanho máximo de 512kb.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIM DA TAB 1 -->

                    <!-- TAB 2 -->
                    <div class="tab-pane fade" id="tab-1-2">
                        <br>
                        <div class="with-border" style="border-bottom: 1px solid #f4f4f4; margin-bottom: 10px;">
                        <h4 style="margin-left: 10px;">Anexo</h4>
                            <div class="box-body">
                              <table id="example" class="table table-bordered table-striped">
                                <thead>
                                  
                                  <tr class="uppercase size">
                                    <th>Documento</th>
                                    <th>Data de Emissão</th>
                                    <th>Data de Validade</th>
                                    <th>Anexo</th>
                                    <th>Ações</th>
                                    <th>Remover</th>
                                  </tr>
                                </thead>
                                
                                <tbody class="size">
                                  <?php 
                                if ($consulta){ 
                                  foreach ($consulta as $e){  
                                ?>
                                  <tr>
                                    <td><?php echo $e->nomedocumento; ?></td>
                                    <?php if ($e->data_va != '0000-00-00'): ?>
                                    <td><?php echo date('d/m/Y', strtotime($e->data_va)); ?></td>   
                                    <?php else: ?>
                                    <td></td>    
                                    <?php endif ?>

                                    <?php if ($e->data_emi != '0000-00-00'): ?>
                                    <td><?php echo date('d/m/Y', strtotime( $e->data_emi)); ?></td>  
                                    <?php else: ?>
                                    <td></td>    
                                    <?php endif ?>
                                    
                                    
                                    <td><a href="<?php echo $e->path?>" download><img style="width: 25px" src="<?php echo base_url('assets/img/sistema/pdf-icon.png'); ?>"></a></td>
                                    <td><a href="<?php echo $e->path?>" download>Download</a></td>
                                    <td><a href="#" data-toggle="tooltip" data-placement="top" title="Excluir" onclick='deletar_anexo(<?php echo $e->id_anexo; ?>)' ><span class="" style="padding: 1px 4px;"><i class="material-icons" aria-hidden="true" style="color: #24a0e9">delete_forever</i></span></a></td>
                                  </tr>

                                   <?php 
                                  }
                                } 
                                ?>              
                                </tbody>

                              </table>

                          </div>
                       </div>
                    </div>
                    <!-- FIM DA TAB 2 -->    
                </div>
                <!-- FIM DO CONTEÚDO DAS TABS -->
                <div class="ibox-footer text-right">
                  <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary mr-2'), 'Salvar'); ?>
                  <a href="<?php echo base_url("documento/listar"); ?>" class="btn btn-secondary">Voltar</a>
              </div>
              
              </div>
              
              <?php echo form_close(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>


<script type="text/javascript">
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }

</script>

<script type="text/javascript">
    $('#date_2').datepicker({
        startView: 0,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy"
    });

    $('#date_3').datepicker({
        startView: 0,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy"
    }); 

    $(document).ready(function() {
    $('#example').DataTable({
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });
});
</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    } 

    <?php if ($this->session->flashdata('msg_alerta') == 3) { ?>
    toastr["success"]("O anexo foi excluído com sucesso!", "Excluído!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 4) { ?>
        toastr["warning"]("Ocorreu um erro, tente novamente!", "Erro!");
    <?php } ?>
</script>

<script type="text/javascript">
    function deletar_anexo(id_anexo){
        $.ajax({
          type: "POST",
          url: '<?php echo base_url();?>Documento/excluir_anexo_tela_editar/'+id_anexo,
          //data: {filtro : filtro.val()},
          async: true,
          success: function(result){
            location.reload();
            // var data = JSON.parse(result);
          },
        });
    }
</script>