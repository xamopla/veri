<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/date-picker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/timepicker.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2.css'); ?>">

<div class="page-body">
  <div class="container-fluid">
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xl-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h2>Cadastrar Documento</h2><span>Preencha os campos abaixo:</span>
              </div>
              <div class="card-body">
                <?php echo form_open_multipart("documento/cadastrar", array('role'=>'form')); ?>
                                  
                  <div class="row">                                
                      <div class="col-md-6">
                          <div class="form-group mb-4">
                              <label for="nome">Empresa*:</label>
                              <?php echo form_dropdown(array('name'=>'id_empresa', 'class'=>'js-example-basic-single form-control', 'placeholder'=>''), $lista_empresas, '', 'required'); ?>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group mb-4">
                              <label for="nome">Tipo de Documento*:</label>
                              <?php echo form_dropdown(array('name'=>'id_tipoDocumento', 'class'=>'js-example-basic-single form-control', 'placeholder'=>''), $lista_tipodedocumentos, '', 'required'); ?>
                          </div>
                      </div>
                  </div>

                  <div class="row">                                
                      <div class="col-md-6">
                          <div class="form-group mb-4">
                              <label for="nome">Número do Documento:</label>
                              <?php echo form_input(array('name'=>'numero_documento', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group mb-4">
                              <label for="nome">Número do Protocolo:</label>
                              <?php echo form_input(array('name'=>'numero_protocolo', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>
                  </div>
                  <div class="row">                                
                      <div class="col-md-4">
                          <div class="form-group mb-4">
                              <label for="nome">Data de Emissão*:</label>
                              <?php echo form_input(array('name'=>'dataEmissao', 'id'=>'date_2', 'class'=>'datepicker-here form-control digits', 'data-language'=>'en', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), '', 'required'); ?>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group mb-4">
                              <label for="nome">Data de Validade*:</label>
                              <?php echo form_input(array('name'=>'dataValidade', 'id'=>'date_3', 'class'=>'datepicker-here form-control digits', 'data-language'=>'en', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), '', 'required'); ?>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group mb-4">
                              <label for="nome">Dias para Notificação*: <span class="fa fa-question-circle" data-toggle="tooltip" title="Defina a quantidade de dias antes do vencimento que você deseja ser alertado ao acesar o sistema."></span></label>
                              <?php echo form_input(array('name'=>'diasNotificacao', 'class'=>'form-control', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>
                  </div>

                  <div class="row">                                
                      <div class="col-md-12">
                          <div class="form-group mb-4">
                              <label for="observacao">Observação:</label>
                              <?php echo form_textarea(array('name'=>'observacao', 'class'=>'form-control', 'placeholder'=>'', 'rows'=>'3', 'style'=>'border-color: #5c6bc0;'), '', ''); ?>
                          </div>
                      </div>
                  </div>

                  <div class="row">                                
                      <div class="col-md-6">
                          <div class="form-group mb-4">
                              <label for="logo">Anexo</label>
                              <br> 
                              <input name="anexo1" class="file" type="file" accept="image/doc*" /> 
                              <br>
                              <small class="text-muted">Extensões permitidas: | gif | jpeg | jpg | png | doc | docx | txt | xls | xlsx | pdf | Tamanho máximo de 512kb.</small>
                          </div>
                      </div>
                  </div>

                  <div class="ibox-footer text-right">
                  <?php echo form_submit(array('name'=>'btn_cadastrar', 'class'=>'btn btn-primary mr-2'), 'Cadastrar'); ?>
                  <a href="<?php echo base_url("documento/listar"); ?>" class="btn btn-secondary">Voltar</a>
              </div>

              </div>
              
              <?php echo form_close(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.en.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datepicker/date-picker/datepicker.custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/jquery-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/highlight.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/time-picker/clockpicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/select2/select2-custom.js'); ?>"></script>

<script type="text/javascript">
    $('#date_2').datepicker({
        startView: 0,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy"
    });

    $('#date_3').datepicker({
        startView: 0,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd/mm/yyyy"
    }); 
</script>