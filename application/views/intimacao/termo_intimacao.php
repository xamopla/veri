<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable-extension.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/toast/toastr.css'); ?>">

<script type="text/javascript">  
    var global_url = '';  

    function preencher_url(url){
    global_url = url;
    }
</script>

<style>
.datepicker{z-index:1151 !important;}

.badge-yellow{
  background-color: #ffc50e;
  color: #fff;
}

.badge-orange{
  background-color: #fb790a;
  color: #fff;
}

.badge-blue{
  background-color: #00cfb8;
  color: #fff;
}

</style>

<style type="text/css">

.swal2-styled{
    background-color: #dc2727 !important;
 } 

.all {
  display: flex;
  perspective: 10px;
  transform: perspective(300px) rotateX(20deg);
  will-change: perspective;
  perspective-origin: center center;
  transition: all 0.5s ease-out;
  justify-content: center;
  transform-style: preserve-3d;
}
.all:hover {
  perspective: 1000px;
  transition: all 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
  & > div {
    opacity: 1;
    transition-delay: 0s;
  }
  .explainer {
    opacity: 0;
  }
}

.all:hover .lefter{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .left{
  opacity: 1;
  margin-right: 10px; 
  perspective: 2000px;
  transition: left 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.all:hover .right{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}


.all:hover .right2{
  opacity: 1;
  margin-left: 10px; 
  perspective: 2000px;
  transition: right 0.5s ease-in;
  transform: perspective(10000px) rotateX(0deg);
  .text {
    opacity: 1;
  }
}

.left, .center, .right, .right2, .lefter, .righter,.left2 {
  width: 32px;
  height: 32px;
  transform-style: preserve-3d;
  border-radius: 10px;
  border: 1px solid #fff;
  /*box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);*/
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.center2 {
  width: 32px;
  height: 32px;
  transform-style: preserve-3d;
  /*border-radius: 8px;*/
  border: 1px solid #fff;
  /*box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);*/
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.center3 {
  width: 32px;
  height: 32px;
  transform-style: preserve-3d;
  /*border-radius: 8px;*/
  border: 1px solid #fff;
  /*box-shadow: 0 0 20px 5px rgb(6 144 243 / 21%);*/
  opacity: 0;
  transition: all .3s ease;
  transition-delay: 1s;
  position: relative;
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  background-color: white;
  cursor: pointer;
  /*background-blend-mode: color-burn;*/
  
  
}

.text {
  transform: translateY(30px);
  opacity: 0;
  transition: all .3s ease;
  bottom: 0;
  left: 5px;
  position: absolute;
  will-change: transform;
  color: #fff;
  text-shadow: 0 0 5px rgba(100, 100, 255, .6)
}
.lefter {
  transform: translateX(-60px) translateZ(-50px) rotateY(-10deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.left2 {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email11.png'); ?>);
}
.left {
  transform: translateX(-30px) translateZ(-25px) rotateY(-5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.center {
  opacity: 1;
  background-image: url(<?php echo base_url('assets/css/fab/images/email3.png'); ?>);
}
.center2 {
  opacity: 1;
  border-radius: 10px;
  background-image: url(<?php echo base_url('assets/css/fab/images/search.png'); ?>);
}
.center3 {
  opacity: 1;
  border-radius: 10px;
  background-image: url(<?php echo base_url('assets/img/icons/alterar_senha.png'); ?>);
}
.right {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/css/fab/images/whatsapp.png'); ?>);
}
.right2 {
  transform: translateX(30px) translateZ(-25px) rotateY(5deg);
  background-image: url(<?php echo base_url('assets/img/icons/history2.svg'); ?>);
}
.righter {
  transform: translateX(60px) translateZ(-50px) rotateY(10deg);
  background-image: url(https://cdn3.iconfinder.com/data/icons/other-icons/48/search-512.png);
}
.explainer {
  font-weight: 300;
  font-size: 2rem;
  color: #fff;
  transition: all .6s ease;
  width: 100%;
  height: 100%;
  background-color: transparent;
  background-image: radial-gradient(circle at center top, #eeeef300, #f7f7f700);
  border-radius: 10px;
  text-shadow: 0 0 10px rgba(255, 255, 255, .8);
  
  display: flex;
  justify-content: center;
  align-items: center;
}


.ref {
  background-color: #000;
  background-image: linear-gradient(to bottom, #d80, #c00);
  border-radius: 3px;
  padding: 7px 10px;
  position: absolute;
  font-size: 16px;
  bottom: 10px;
  right: 10px;
  color: #fff;
  text-decoration: none;
  text-shadow: 0 0 3px rgba(0, 0, 0, .4);
  &::first-letter {
    font-size: 12px;
  }
}

</style>

<style type="text/css">
  .buttons-excel{
    width: 100px;
  }

  .dataTables_filter{
    float: right;
  }

    .modal-dialog {
        width: 1700px;
        max-width : 1700px;
    }
</style>

<div class="page-body">
  <div class="container-fluid"> 
    <br>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h2 class="font-strong mb-4">Termo de Intimação</h2> 
          </div>
          <div class="card-body">
            <div class="form-group col-lg-4 col-xs-12 pull-right">
              <label>Filtro:</label>
              <?php 
              $valores_filtro = array(
                  'TODAS'=>'Todas as empresas',
                  'REGULARIZADO'=>'Regularizado',
                  'PENDENCIAS'=>'Pendências',
                  'ANALISANDO'=>'Analisando',
                  'CLIENTE' => 'Aguardando cliente'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'id'=>'filtro'), $valores_filtro, $filtro); 
              ?>
            </div><br>
            
            <div class="table-responsive row">
              <table class="table table-striped" id="example" data-toggle="datatables">
                  <thead class="thead-default thead-lg">
                      <tr>
                        <th style="display: none;"></th>
                        <th>Razão Social</th>
                        <th>CNPJ</th>
                        <th>Assunto</th>
                        <th style="text-align:center;">Termo de Intimação</th>
                        <th></th>
                        <th>Data</th>
                        <th>Situação</th>
                        <th style="text-align:center;">Acompanhamento</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $cont = 1;
                    if ($consulta){ 
                      foreach ($consulta as $c){?> 
                      <tr>  
                            <td style="display: none;"><?php echo $c->data; ?></td>
                            <td style="text-align: left;"><?php echo $c->razao_social; ?></td>
                            <td ><?php echo $c->cnpj; ?></td>
                            <td ><?php echo $c->assunto; ?></td>

                            <?php 
                            $razao_aux = str_replace("'", "", $c->razao_social);
                            $razao_aux = str_replace("&", "", $razao_aux);

                            ?>

                            <?php if($c->lida == 1 && !empty($c->conteudo)){ ?>
                                <td>
                                    <div class="all">
                                      <div class="left">
                                        <a href="javascript:void(0)" onclick="enviar_email('<?php echo $c->id_caixa_postal_mensagem ?>','<?php echo $razao_aux; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por E-mail" class="">
                                          <div class="explainer"></div>
                                          <div class="text"></div>
                                        </a>
                                      </div>
                                      <div class="center2">
                                        <a href="javascript:void(0);" onclick="openModalMensagem('<?php echo $c->id_caixa_postal_mensagem ?>','<?php echo $razao_aux; ?>','<?php echo $c->cnpj; ?>')" data-trigger="hover" data-placement="top" title="Ver Mensagem" class="">
                                          <div class="explainer"></div>
                                          <div class="text"></div>
                                        </a>
                                      </div>
                                      <div class="right">
                                         <a href="javascript:void(0)" onclick="enviar_whatsapp('<?php echo $c->id_caixa_postal_mensagem ?>','<?php echo $razao_aux; ?>','<?php echo $c->cnpj; ?>')" data-toggle="tooltip" data-placement="top" title="Enviar por Whatsapp" class="">
                                          <div class="explainer"></div>
                                          <div class="text">Whatsapp</div>
                                        </a>
                                      </div>
                                      
                                    </div>
                                </td>

                                <td><span class="badge badge-success badge-shadow" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Lida" data-content="<?php echo $c->lida_por; ?>" ><i class="ti-alert">&nbsp;</i>Lida</span> </td>
                            <?php }else{ ?>
                                <td>
                                    <?php if($c->possui_certificado){ ?>
                                        <div class="all">
                                          <div class="center4">
                                            <a href="javascript:void(0);" onclick="ler_mensagem('<?php echo $c->id_caixa_postal_mensagem ?>', 'CERTIFICADO')" data-trigger="hover" data-placement="top" title="Ler Mensagem" class="">
                                                <i class="material-icons" aria-hidden="true" style="font-size:32px !important">email</i>
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="all">
                                          <div class="center4">
                                            <a href="javascript:void(0);" onclick="ler_mensagem('<?php echo $c->id_caixa_postal_mensagem ?>', 'PROCURACAO')" data-trigger="hover" data-placement="top" title="Ler Mensagem" class="">
                                                <i class="material-icons" aria-hidden="true" style="font-size:32px !important">email</i>
                                              <div class="explainer"></div>
                                              <div class="text"></div>
                                            </a>
                                          </div>
                                        </div>
                                    <?php } ?>
                                    
                                </td>

                                <td><span class="badge badge-warning badge-shadow" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Não Lida" data-content="" ><i class="ti-alert">&nbsp;</i>Não lida</span> </td>
                            <?php } ?>

                            <td><?php echo date("d/m/Y", strtotime($c->data)); ?></td>

                            <td>
                              <?php if ($c->situacao == "REGULARIZADO"){ ?>
                                <?php 


                                echo '<span class="badge badge-success" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Regularizado"  data-content="'.$c->ultimo_historico.'">Regularizado</span>'; 

                                ?>

                              <?php } else if($c->situacao == "PENDENCIAS") { ?>
                                <?php 
                                echo '<span class="badge badge-danger" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Pendências" data-content="'.$c->ultimo_historico.'">Pendências</i></span>'; 
                                ?>

                              <?php } else if($c->situacao == "ANALISANDO") { ?>
                                <?php 
                                echo '<span class="badge badge-orange" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Analisando" data-content="'.$c->ultimo_historico.'" >Analisando</span>'; 

                                ?>

                              <?php } else if($c->situacao == "CLIENTE") { ?>
                                <?php 
                                echo '<span class="badge badge-blue" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" title="Aguardando cliente" data-content="'.$c->ultimo_historico.'" >Aguardando cliente</span>'; 

                                ?>

                              <?php } ?>
                            </td>

                            <td>
                              <div class="all">
                                <div class="center3">
                                  <a href="javascript:void(0);" onclick="openModalSituacao('<?php echo $c->id_caixa_postal_mensagem; ?>', '<?php echo $c->situacao; ?>','<?php echo $razao_aux; ?>')" data-trigger="hover" data-placement="top" title="Adicionar Observação" class="">
                                    <div class="explainer"></div>
                                    <div class="text"></div>
                                  </a>
                                </div>
                                <div class="right2">
                                   <a href="javascript:void(0)" onclick="openModalHistorico('<?php echo $c->id_caixa_postal_mensagem; ?>')" data-toggle="tooltip" data-placement="top" title="Ver Histórico de Ações" class="">
                                    <div class="explainer"></div>
                                    <div class="text">Histórico</div>
                                  </a>
                                </div>
                                
                              </div>
                            </td>
                            
                      </tr>
                  <?php $cont++; } 
                  }
                  ?>
                  </tbody>
              </table>
          </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
  <!-- Container-fluid Ends-->
</div>

<!-- Modal pronampe -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal_ler_mensagem">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudo_modal">
            <div class="modal-header">
                <h5 class="modal-title" id="title_mensagem"></h5>                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body_mensagem">
                <pre id="mensagem_corpo">
                    
                </pre>
            </div>
            <div class="modal-footer">
                <button id="link_pdf_mensagem" onclick="imprimir();" type="button" class="btn btn-secondary" ><i class="ti-printer"></i>&nbsp;Imprimir</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <div class="printable"></div>
    </div>
</div>


<div class="modal fade" id="modalSituacao" tabindex="-1" role="dialog" aria-labelledby="formModal"
          aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 500px !important; ">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tipoRegimeModal">Adicionar Observação</h5>
          
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php echo form_open("termo_intimacao/atualiza_situacao"); ?> 
            <div class="form-group col-lg-12 col-xs-12">
              <input type="hidden" name="id_mensagem_ecac" id="id_mensagem_ecac">

              <div class="form-group">
                <label class="col-form-label" for="recipient-name">Empresa:</label>
                <input class="form-control" type="text" id="nomeEmpresaValor" readonly="">
              </div>

              <label>Selecione a Situação do Acompanhamento:</label>
              <?php 
              $valores_filtro_aux = array(
                  'REGULARIZADO'=>'Regularizado',
                  'ANALISANDO'=>'Analisando',
                  'PENDENCIAS'=>'Pendências',
                  'CLIENTE' => 'Aguardando cliente'
                  
              );
              echo form_dropdown(array('class'=>'form-control', 'name'=>'situacao', 'id'=>'situacao', 'style'=>'width:100% !important'), $valores_filtro_aux, $filtro_aux); 
              ?>

               <div class="form-group">
                <label class="col-form-label" for="message-text">Observação:</label>
                <textarea class="form-control" id="descricao" name="descricao" rows="5"></textarea>
              </div> 

            </div>

      </div>
      <div class="modal-footer">
          <?php echo form_submit(array('name'=>'btn_salvar', 'class'=>'btn btn-primary'), 'Salvar'); ?>
          <!-- <button type="button" onclick="salva_situacao()" class="btn btn-success">Salvar</button> -->
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
      <?php echo form_close(); ?> 
    </div>
  </div>

</div>



<div class="modal fade" tabindex="-1" role="dialog" id="modal_historico">
    <div class="modal-dialog" role="document" style="height: 800px !important; width:800px !important">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Histórico de Ações</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="modal_table" class="table table-striped" style="width: 100% !important">    
                    <thead class="thead-default thead-lg">
                        <tr>
                          <th style="display:none"></th>
                          <th>Usuário</th>
                          <th>Data</th>
                          <th>Situação</th>
                          <th>Observação</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>                
                </table>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<!-- PLUGINS ESPECÍFICOS -->
<!-- Plugin used-->
<script src="<?php echo base_url('assets/js/prism/prism.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom-card/custom-card.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tooltip-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.colVis.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/dataTables.scroller.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datatable/datatable-extension/custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/datetime-moment.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweet-alerts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/alertas/sweetalert2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/toast/toastr.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
<!-- PLUGINS ESPECÍFICOS -->

<script>
$(document).ready(function() {  
  $('#filtro').change(function(){
      var filtro = $('#filtro option:selected').val();
      window.location.replace("<?php echo base_url('termo_intimacao/listar'); ?>/"+filtro+"");
  });
});
</script>

<script type="text/javascript">

    function ler_mensagem($id, $tipo){      
      var variaveis = {id : $id};  
      $.ajax({
        type: "POST",
        url: '<?php echo base_url('termo_intimacao/buscar_mensagem'); ?>',
        data: variaveis,
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if($tipo == "CERTIFICADO"){
              ler_mensagem_ecac(data.caixa_postal_id, data.id_caixa_postal_mensagem, data.assunto);
            }else{
              ler_mensagem_ecac_proc(data.caixa_postal_id, data.id_caixa_postal_mensagem, data.assunto);
            }
        }
      });
    }

    function ler_mensagem_ecac(id, id_mensagem,assunto){        
        //$('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/Ler_mensagem_ecac/ler_mensagem/"+id+"/"+id_mensagem+"/"+banco;
        var request = $.get(url, function(data, status){

            var response;
            if(data.includes("PFX")){
                var string_data = data.substring(data.indexOf("{"));
                response = JSON.parse(string_data); 
            }else{
              response = JSON.parse(data);  
            }
            
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Erro ao buscar mensagem !",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem').html(assunto);
                $('#mensagem_corpo').html(response['mensagem']);
                $('#modal_ler_mensagem').modal('show');

                 marcarLidaPor(id,id_mensagem);
            } 
        });
    }

    function ler_mensagem_ecac_proc(id, id_mensagem,assunto){        
        //$('#modal_mensagens').modal('hide');
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        var banco = <?php echo "'".$banco."'"; ?>;
        url = "https://veri-sp.com.br/crons-api/Ler_mensagem_ecac/ler_mensagem_proc/"+id+"/"+id_mensagem+"/"+banco;
        var request = $.get(url, function(data, status){

            var response;
            if(data.includes("PFX")){
                var string_data = data.substring(data.indexOf("{"));
                response = JSON.parse(string_data); 
            }else{
              response = JSON.parse(data);  
            }
            
            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Erro ao buscar mensagem !",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem').html(assunto);
                $('#mensagem_corpo').html(response['mensagem']);
                $('#modal_ler_mensagem').modal('show');

                 marcarLidaPor(id,id_mensagem);
            } 
        });
    }

    function imprimir(){
        $(".printable").html($("#conteudo_modal").html());
        $(".printable").printThis();
    }

    function convertDate2(a){
      var day = a.split('-')[2];
      day = day.split(' ')[0];
      var month = a.split('-')[1];
      var year = a.split('-')[0];

      var date = day + "/" + month + "/" + year;
      return date;
    }

    function openModalMensagem(id, razao_social, cnpj){
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/termo_intimacao/find_info_for_modal/"+id;
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                $('#title_mensagem').html(razao_social+' - '+cnpj);
                $('#mensagem_corpo').html(response['conteudo']);
                $('#modal_ler_mensagem').modal('show');
            } 
        });
    }

    function enviar_email(id, razao_social, cnpj){
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/termo_intimacao/find_info_for_modal/"+id;
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                var m = response['conteudo'];
                var m2 = m.replaceAll("\n", "%0a");
                window.open('mailto:?subject=Notificação Sistema VERI&body='+m2, '_blank');
            } 
        });
    }

    function enviar_whatsapp(id, razao_social, cnpj){
        swal({ 
            title: "Buscando...",
            text: 'Buscando Conteúdo da Mensagem.\nPor favor, aguardar alguns segundos...',
            type: "info" ,
            confirmButtonText: "Cancelar",
            confirmButtonColor: "#fff"
        },function(isConfirm){
            if(isConfirm){
                request.abort();
            }
        });
        url = "<?= base_url() ?>/termo_intimacao/find_info_for_modal/"+id;
        var request = $.get(url, function(data, status){

            response = JSON.parse(data);

            if(response['error']){
                $(document).ready(function() {                
                    swal({
                        title: "Ops, Algo Aconteceu!",
                        text: response['msg'],
                        type: response['type']
                    });
                });
            }else{
                swal.close();
                var m = response['conteudo'];
                var m2 = m.replaceAll("\n", "%0a");
                window.open('https://api.whatsapp.com/send?text='+m2, '_blank');
            } 
        });
    }


    function openModalSituacao(id, situacao, razao_social){
      $("#id_mensagem_ecac").val(id);
      $("#situacao").val(situacao);
      $("#nomeEmpresaValor").val(razao_social);

      $('#modalSituacao').modal('show');
    }

    function salva_situacao(){
      var id = $("#id_mensagem_ecac").val();
      var situacao = $("#situacao").val();

      $.post("<?php echo base_url();?>termo_intimacao/atualiza_situacao",
      {
          id:id,
          situacao: situacao
      },
      function(data){
        if(data == 1){
          window.location = global_url;
        }
      });
    }


    function openModalHistorico(id){
      var variaveis = {id : id};

      $.ajax({
        type: "POST",
        url: '<?php echo base_url('termo_intimacao/buscar_historico'); ?>',
        data: variaveis,
        async: true,
        success: function(result){
            var data = JSON.parse(result);
            if(data == "") {
                var array = [];
                popularDataTable(array);  
                jQuery('[data-toggle="popover"]').popover();   
                jQuery('[data-toggle="tooltip"]').tooltip();    

                $('#modal_historico').modal('show');          
            }else{
              var array = json2array(data);
              popularDataTable(array);
              jQuery('[data-toggle="popover"]').popover();
              jQuery('[data-toggle="tooltip"]').tooltip(); 

               $('#modal_historico').modal('show');
            }
        }
      });
    }

    function popularDataTable(json){
      $.fn.dataTable.moment('DD/MM/YYYY');
      $('#modal_table').DataTable({
            "iDisplayLength": 50,
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "order": [[ 0, "desc" ]],
            lengthChange: false,
            "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        data: json,
        "bDestroy": true
          });
    }


    function json2array(data){
      var string = "";
      var array = [];
      var arrayMultiple = [];

      for(var i in data){
        array = [];

        array.push('<td style="display: none;">'+data[i].data_alteracao+'</td>');
        array.push('<td>'+data[i].nome_usuario+'</td>');
        array.push('<td>'+data[i].data+'</td>');

        if(data[i].situacao == "REGULARIZADO"){
          array.push('<td><span class="badge badge-success">Regularizado</span></td>');
        }else if(data[i].situacao == "PENDÊNCIAS"){
          array.push('<td><span class="badge badge-danger">Pendências</span></td>');
        }else if(data[i].situacao == "ANALISANDO") {
          array.push('<td><span class="badge badge-orange">Analisando</span></td>');
        }else if(data[i].situacao == "CLIENTE") {
          array.push('<td><span class="badge badge-blue">Aguardando cliente</span></td>');
        }

        if(data[i].descricao == null){
          data[i].descricao = "";
        }
        array.push('<td>'+data[i].descricao+'</td>');

        arrayMultiple.push(array);
      }
      
      return arrayMultiple;
    }


    function  marcarLidaPor(id,id_mensagem){
        $.post("<?php echo base_url();?>mensagens/marcar_lida_por",
          {
              id:id,
              id_mensagem: id_mensagem
          },
          function(data){
            if(data == 1){
            }
          });
    }
</script>

<script type="text/javascript">

    toastr.options = {
    "closeButton": true,
    "preventDuplicates": true,
    "progressBar": true
    }

    <?php if ($this->session->flashdata('msg_alerta') == 1) { ?>
    toastr["success"]("O cadastro foi efetuado com sucesso!", "Cadastrado!");    
    <?php } ?>

    <?php if ($this->session->flashdata('msg_alerta') == 2) { ?>
        toastr["info"]("A edição foi realizada com sucesso!", "Editado!");
    <?php } ?>
</script>

<script>
$(document).ready(function() {

  $('[data-toggle="popover"]').popover({
    boundary:'window',
    html: true
  })

var table = $('#example').DataTable({
        "iDisplayLength": 50,
        "order": [[ 0, "desc" ]],
        lengthChange: true,
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'EXCEL',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    }
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                exportOptions: {
                    modifier: {
                        search: 'none'
                    }
                }
            },
            {
                extend: 'colvis',
                text: 'COLUNAS'
            }
        ],
        "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
     "bDestroy": true
    });

    table.search('<?php echo $razao_social_filtro; ?>').draw();
    table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );


});
</script>



