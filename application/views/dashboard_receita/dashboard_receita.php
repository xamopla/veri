<style type="text/css">
  .gradient-secondary{
    background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);
  }

  .gradient-info{
    background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);
  }
  /*background-image: linear-gradient(to bottom right, #fa6868, #ff4e4e);*/
  /*background-image: linear-gradient(to bottom right, #00d74c, #07b2dd);*/
</style>

<style type="text/css">
  .card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details {
    position: absolute;
    top: 0;
    left: 0;
    width: 70%;/*100%*/
    height: 100%;
    transition: .5s;
    transform-origin: left;
    transform: perspective(2000px) rotateY(-90deg);
}
.card:hover .details {
    transform: perspective(2000px) rotateY(0deg);
}
.card .details .center {
    width: 100% !important;
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details .center p {
    margin: 5px 0;
    padding: 0;
    color: #262626;
}
.card .details .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details .center ul li a:hover {
    background: #ff3636;
}


.div_hover { 
  background: white;
  border-radius: 50px; 
}

.div_hover a:hover { 
  background-color: #24a0e9; 
  color: white !important;
}

.tag_a_hover {
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.span_hover:hover{
  background-color: #24a0e9; 
  color: white !important;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.media-body{
  padding-left: 0px !important; 
}
</style>


<style type="text/css">
    .card .image {
    width: 100%;
    height: 100%;
    overflow: hidden;
}
.card .image img {
    width: 100%;
    transition: .5s;
}
.card:hover .image img {
    opacity: .5;
    transform: translateX(30%);/*100%*/
}
.card .details2 {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;/*100%*/
    height: 100%;
    transition: .5s;
    transform-origin: left;
    transform: perspective(2000px) rotateY(-90deg);
}

.card:hover .details2 {
    transform: perspective(2000px) rotateY(0deg);
}
.card .details2 .center {
    width: 100% !important;
    padding: 20px;
    text-align: center;
    background: #fff;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
}
.card .details2 .center h1 {
    margin: 0;
    padding: 0;
    color: #ff3636;
    line-height: 20px;
    font-size: 20px;
    text-transform: uppercase;
}
.card .details2 .center h1 span {
    font-size: 14px;
    color: #262626;
}
.card .details2 .center p {
    margin: 5px 0;
    padding: 0;
    color: #262626;
}
.card .details2 .center ul {
    margin: 10px auto 0;
    padding: 0;
    display: table;
}
.card .details2 .center ul li {
    list-style: none;
    margin: 0 5px;
    float: left;
}
.card .details2 .center ul li a {
    display: block;
    background: #262626;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    transform: .5s;
}
.card .details2 .center ul li a:hover {
    background: #ff3636;
}


.div_hover { 
  background: white;
  border-radius: 50px; 
}

.div_hover a:hover { 
  background-color: #24a0e9; 
  color: white !important;
}

.tag_a_hover {
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.span_hover:hover{
  background-color: #24a0e9; 
  color: white !important;
  padding-left: 3px;
  padding-right: 3px;
  border-radius: 10px; 
}

.media-body{
  padding-left: 0px !important; 
}
</style>
<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Federal</h2> 
        </div> 
      </div>
    </div>
  </div>

<?php 
  $hostCompleto = $_SERVER['HTTP_HOST'];
  $server = explode('.', $hostCompleto);
  $server = $server[0];

  if($server == "demo"){
   $server = 'demo';
  }else{
    $server = 'teste';
  }
?>

<div class="container-fluid general-widget">

  <!-- <div class="row">
    <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #ff4e4e">PENDÊNCIAS</h5></div>

    <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #03a9f3">DÍVIDA ATIVA - REGULARIZE</h5></div>

    <div class="col-xl-4 col-lg-4" style="text-align: center;"><h5 class="page-title m-b-0" style="color: #00d74c">VENCIMENTOS</h5></div>
  </div> -->

  <div class="row">

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("ecac/listar_situacao_fiscal_geral"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <br><span style="font-size: 16px;color: #24a0e9" class="span_hover">e-CAC - Diagnóstico Fiscal</span>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ecac/listar_situacao_fiscal_regular"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php echo $qtd_empresas_situacao_fiscal_regular->valor; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ecac/listar_situacao_fiscal_pendencia"); ?>" style="font-size: 18px;" class="tag_a_hover">Pendências: <?php echo $qtd_empresas_situacao_fiscal_pendencias->valor; ?></a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">e-CAC - Diagnóstico Fiscal</span>
                <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com Situação Fiscal no e-CAC irregular' data-original-title='' title=''><?php echo $qtd_empresas_situacao_fiscal_pendencias->valor; ?></div></h4>

                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>


              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("ausencia_ecf/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <br><span style="font-size: 17px;color: #24a0e9" class="span_hover">Ausência de Declaração - ECF</span>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_ecf/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_ecf_regular; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_ecf/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_ecf_irregular; ?></a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 17px">Ausência de Declaração - ECF</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_ecf_irregular; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div> 
    
      


      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("ausencia_defis/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <br><span style="font-size: 17px;color: #24a0e9" class="span_hover">Ausência de Declaração - DEFIS</span>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_defis/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_defis_regular; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_defis/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_defis_irregular; ?></a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 17px">Ausência de Declaração - DEFIS</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_defis_irregular; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>


      
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="javascript:void(0)">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details2">
                <div class="center">

                  <teste style="text-align: left !important;">
                    <div class="div_hover">
                      <a href="<?php echo base_url("regularize/listar_previdencia"); ?>" style="font-size: 16px;" class="tag_a_hover">Previdenciária: <?php if($qtd_empresas_divida_previdencia->valor){ echo $qtd_empresas_divida_previdencia->valor; }else{echo 0;}  ?></a>
                    </div>
                  </teste>

                  <teste style="float: right;"> 
                    <div class="div_hover">
                      <a href="<?php echo base_url("regularize/listar_demais_debitos_nao_tributarios"); ?>" style="font-size: 16px;" class="tag_a_hover">Não Tributário: <?php if($qtd_empresas_divida_debitos_nao_tributarios->valor){ echo $qtd_empresas_divida_debitos_nao_tributarios->valor; }else{echo 0;} ?></a>  
                    </div>
                  </teste>

                  <teste style="text-align: left !important;"> 
                    <div class="div_hover">
                      <a href="<?php echo base_url("regularize/listar_fgts"); ?>" style="font-size: 16px;" class="tag_a_hover">FGTS: <?php if($qtd_empresas_divida_fgts->valor){ echo $qtd_empresas_divida_fgts->valor; }else{echo 0;} ?></a>  
                    </div>
                  </teste>

                  <teste style="float: right;"> 
                    <div class="div_hover">
                      <a href="<?php echo base_url("regularize/listar_multa_criminal"); ?>" style="font-size: 16px;" class="tag_a_hover">Criminal: <?php if($qtd_empresas_divida_multacriminal->valor){ echo $qtd_empresas_divida_multacriminal->valor; }else{echo 0;} ?></a>  
                    </div>
                  </teste>

                  <teste style="text-align: left !important;">
                    <div class="div_hover">
                      <a href="<?php echo base_url("regularize/listar_multa_trabalhista"); ?>" style="font-size: 16px;" class="tag_a_hover">Trabalhista: <?php if($qtd_empresas_divida_multatrabalhista->valor){ echo $qtd_empresas_divida_multatrabalhista->valor; }else{echo 0;} ?></a>  
                    </div>
                  </teste>

                  <teste style="float: right;"> 
                    <div class="div_hover">
                      <a href="<?php echo base_url("regularize/listar_multa_eleitoral"); ?>" style="font-size: 16px;" class="tag_a_hover">Eleitoral: <?php if($qtd_empresas_divida_multaeleitoral->valor){ echo $qtd_empresas_divida_multaeleitoral->valor; }else{echo 0;} ?></a>  
                    </div>
                  </teste>

                  <teste style="text-align: left !important;"> 
                    <div class="div_hover">
                      <a href="<?php echo base_url("regularize/listar_demais_debitos_tributarios"); ?>" style="font-size: 16px;" class="tag_a_hover">Tributários: <?php if($qtd_empresas_divida_debitos_tributarios->valor){ echo $qtd_empresas_divida_debitos_tributarios->valor; }else{echo 0;} ?></a>  
                    </div>
                  </teste>

                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Dívida Ativa</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com procurações próximas ao vencimento' data-original-title='' title=''><?php echo $qtd_empresas_divida_previdencia->valor + $qtd_empresas_divida_debitos_nao_tributarios->valor + $qtd_empresas_divida_fgts->valor + $qtd_empresas_divida_multacriminal->valor + $qtd_empresas_divida_multatrabalhista->valor + $qtd_empresas_divida_multaeleitoral->valor + $qtd_empresas_divida_debitos_tributarios->valor; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>
      
      

      

    </div>

  <div class="row">


    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("gfip/listar/TODAS"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <h1><br><span style="font-size: 20px;color: #24a0e9" class="span_hover">Divergência GFIP x GPS</span></h1>
                <p>
                  <div class="div_hover">
                  <a href="<?php echo base_url("gfip/listar/REGULAR"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php echo $qtd_gfip_regular; ?></a>  
                  </div>
                </p>

                <p>
                  <div class="div_hover">
                  <a href="<?php echo base_url("gfip/listar/IRREGULAR"); ?>" style="font-size: 18px;" class="tag_a_hover">Irregulares: <?php echo $qtd_gfip_irregular; ?></a>  
                  </div>
                </p>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Divergência GFIP x GPS</span>
              <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas irregulares no CADIN' data-original-title='' title=''><?php echo $qtd_gfip_irregular; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>


    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("ausencia_gfip/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
        <div class="b-r-4 card-body"  style="color: white;">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 17px;color: #24a0e9" class="span_hover">Ausência de Declaração - GFIP</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_gfip/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_gfip_regular; ?></a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_gfip/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_gfip_irregular; ?></a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 17px">Ausência de Declaração - GFIP</span>
              <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_gfip_irregular; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("ausencia_efd/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
        <div class="b-r-4 card-body"  style="color: white;">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 16px;color: #24a0e9" class="span_hover">Ausência de Declaração - EFD-CONTRIB</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_efd/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_efd_regular; ?></a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_efd/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_efd_irregular; ?></a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 16px">Ausência de Declaração - EFD-CONTRIB</span>
              <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_efd_irregular; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("limite_simples/listar"); ?>">   
          <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
            <div class="b-r-4 card-body">
              <div class="media static-top-widget">

                <div class="details">
                  <div class="center">
                    <h1><span style="font-size: 14px;color: #24a0e9" class="span_hover">Sublimite Simples</span></h1>


                    <teste >
                      <div class="div_hover">
                      <a href="<?php echo base_url("limite_simples/listar/PROXIMAS"); ?>" style="font-size: 15px;" class="tag_a_hover">Próx Sublimite PJ: <?php echo $qtd_prox_limite; ?></a>   
                      </div>
                    </teste>
                    
                    <teste >
                      <div class="div_hover">
                      <a href="<?php echo base_url("limite_simples/listar/LIMITE"); ?>" style="font-size: 15px;" class="tag_a_hover">Atingido/Superado PJ: <?php echo $qtd_limite_atingido; ?></a>
                      </div>
                    </teste>
                    
                    <teste >
                      <div class="div_hover">
                      <a href="<?php echo base_url("limite_simples/listar/PROXIMAS_ACUMULADA"); ?>" style="font-size: 15px;" class="tag_a_hover">Próx Sublimite Sócios/PJ: <?php echo $qtd_prox_limite_acumulado; ?></a>   
                      </div>
                    </teste>

                    <teste >
                      <div class="div_hover">
                      <a href="<?php echo base_url("limite_simples/listar/LIMITE_ACUMULADA"); ?>" style="font-size: 15px;" class="tag_a_hover">Atingido/Superado Sócios/PJ: <?php echo $qtd_limite_atingido_acumulado; ?></a> 
                      </div>
                    </teste>

                  </div>
                </div>

                <div class="align-self-center text-center"></div>
                <div class="media-body"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Sublimite Simples</span>
                  <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas que atingiram o Sublimite do Simples Nacional' data-original-title='' title=''><?php echo $qtd_limite_atingido+$qtd_limite_atingido_acumulado; ?></div></h4>
                  <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    
    
  </div>

    


    <div class="row">

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("dctf/index"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="details">
                <div class="center">
                  <h1><span style="font-size: 16px;color: #24a0e9" class="span_hover">DCTF</span></h1>

                  <teste >
                    <div class="div_hover">
                    <a href="<?php if($server == "demo"){echo base_url("dctf/index/anterior/Fevereiro/2021?filtro=REGULAR");}else{echo "dctf/index?filtro=REGULAR"; } ?>" style="font-size: 15px;" class="tag_a_hover">Transmitidas: <?php if($server == "demo"){echo 1;}else{echo $qtd_dctf_transmitida; } ?> </a>  
                    </div>
                  </teste>
                  
                  <teste >
                    <div class="div_hover">
                    <a href="<?php echo "dctf/index?filtro=ANALISAR"; ?>" style="font-size: 15px;" class="tag_a_hover">Analisar: <?php echo $qtd_dctf_analise;  ?></a>
                    </div>
                  </teste>
                  
                  <teste >
                    <div class="div_hover">
                    <a href="<?php if($server == "demo"){echo base_url("dctf/index/anterior/Fevereiro/2021?filtro=PENDENTE");}else{echo "dctf/index?filtro=PENDENTE"; } ?>" style="font-size: 15px;" class="tag_a_hover">Vencidas: <?php if($server == "demo"){echo 18;}else{echo $qtd_dctf_vencidas; } ?></a>  
                    </div>
                  </teste>

                  

                  <teste >
                    <div class="div_hover">
                    <a href="<?php echo "dctf/index?filtro=SEM_MOVIMENTO";  ?>" style="font-size: 15px;" class="tag_a_hover">Sem movimento: <?php echo $qtd_dctf_sem_movimento; ?></a>  
                    </div>
                  </teste>
                  
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">DCTF</span>
                <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de DCTF Próximos a Transmitir/Não Transmitidos' data-original-title='' title=''>
                  <?php echo $qtd_dctf_vencidas + $qtd_dctf_analise;  ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

      
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("ausencia_dctf/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <br><span style="font-size: 16px;color: #24a0e9" class="span_hover">Ausência de Declaração - DCTF</span>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_dctf/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_dctf_regular; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_dctf/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_dctf_irregular; ?></a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 16px">Ausência de Declaração - DCTF</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_dctf_irregular; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

      

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("ausencia_dirf/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
                <div class="center">
                  <br><span style="font-size: 17px;color: #24a0e9" class="span_hover">Ausência de Declaração - DIRF</span>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_dirf/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_dirf_regular; ?></a>
                    </div>
                  </p>
                  <p>
                    <div class="div_hover">
                      <a href="<?php echo base_url("ausencia_dirf/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_dirf_irregular; ?></a>  
                    </div>
                  </p>
                </div>
              </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 17px">Ausência de Declaração - DIRF</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_dirf_irregular; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("simples_nacional/listar"); ?>">   
          <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
            <div class="b-r-4 card-body">
              <div class="media static-top-widget">
                <div class="details">
                  <div class="center">
                    <h1><br><span style="font-size: 15px;color: #24a0e9" class="span_hover">Exclusão do Simples</span></h1>
                    <p>
                      <div class="div_hover">
                      <a href="<?php echo base_url("simples_nacional/listar/EXCLUIDAS"); ?>" style="font-size: 18px;" class="tag_a_hover">Excluídas: <?php echo $qtd_simples_excluidas; ?></a>  
                      </div>
                    </p>

                    <p>
                      <div class="div_hover">
                      <a href="<?php echo base_url("simples_nacional/listar/OPTANTES"); ?>" style="font-size: 18px;" class="tag_a_hover">Optantes: <?php echo $qtd_simples_optantes; ?></a>   
                      </div>
                    </p>

                  </div>
                </div>

                <div class="align-self-center text-center"></div>
                <div class="media-body"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Exclusão do Simples</span>
                  <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas que foram excluídas do Simples Nacional' data-original-title='' title=''><?php echo $qtd_simples_excluidas; ?></div></h4>
                  <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
                </div>
              </div>
            </div>
          </div>
        </a>
    </div>
    <!-- Nº de Empresas com procurações próximas ao vencimento -->


    </div>


    <div class="row">

    
    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php if($server == "demo"){echo base_url("pgdas");}else{echo base_url("pgdas"); } ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="details">
              <div class="center">
                <h1><span style="font-size: 17px;color: #24a0e9" class="span_hover">PGDAS</span></h1>

                <teste>
                  <div class="div_hover">
                  <a href="<?php if($server == "demo"){echo base_url("pgdas/index/anterior/Marco/2021?filtro=REGULAR");}else{echo base_url("pgdas/index?filtro=REGULAR"); } ?>" style="font-size: 15px;" class="tag_a_hover">Transmitida: <?php if($server == "demo"){echo 117;}else{echo $qtd_pgdas_transmitidas; } ?></a>
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                  <a href="<?php echo base_url("pgdas/index?filtro=ANALISAR");  ?>" style="font-size: 15px;" class="tag_a_hover">Analisar: <?php echo $qtd_pgdas_analise; ?></a>
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                  <a href="<?php if($server == "demo"){echo base_url("pgdas/index/anterior/Marco/2021?filtro=PENDENTE");}else{echo base_url("pgdas/index?filtro=PENDENTE"); } ?>" style="font-size: 15px;" class="tag_a_hover">Vencidas: <?php if($server == "demo"){echo 10;}else{echo $qtd_pgdas_vencidas; } ?></a>
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                  <a href="<?php if($server == "demo"){echo base_url("pgdas/index/anterior/Abril/2021?filtro=SEM_MOVIMENTO");}else{echo base_url("pgdas/index?filtro=SEM_MOVIMENTO"); } ?>" style="font-size: 15px;" class="tag_a_hover">Sem movimento: <?php if($server == "demo"){echo 5;}else{echo $qtd_pgdas_sem_movimento; } ?></a>
                  </div>
                </teste>
                
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">PGDAS</span>
              <h4 class="mb-0" style="color: white !important"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com entrega do PGDAS próximas de transmissão ou não transmitidas ' data-original-title='' title=''><?php if($server == "demo"){echo 305;}else{ echo $qtd_pgdas_analise + $qtd_pgdas_vencidas; } ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("ausencia_pgdas/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
        <div class="b-r-4 card-body"  style="color: white;">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 17px;color: #24a0e9" class="span_hover">Ausência de Declaração - PGDAS</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_pgdas/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_pgdas_regular; ?></a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_pgdas/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_pgdas_irregular; ?></a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 17px">Ausência de Declaração - PGDAS</span>
              <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_pgdas_irregular; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div> 

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("ausencia_dasn/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
        <div class="b-r-4 card-body"  style="color: white;">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 16px;color: #24a0e9" class="span_hover">Ausência de Declaração - DASN SIMEI</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_dasn/listar/REGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Regulares: <?php echo $qtd_ausencia_dasn_regular; ?></a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ausencia_dasn/listar/IRREGULAR"); ?>" style="font-size: 16px;" class="tag_a_hover">Irregulares: <?php echo $qtd_ausencia_dasn_irregular; ?></a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 16px">Ausência de Declaração - DASN SIMEI</span>
              <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_ausencia_dasn_irregular; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("mei/listar"); ?>">   
          <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
            <div class="b-r-4 card-body">
              <div class="media static-top-widget">

                <div class="details">
                  <div class="center">
                    <h1><br><span style="font-size: 17px;color: #24a0e9" class="span_hover">Exclusão do MEI</span></h1>
                    <p>
                      <div class="div_hover">
                      <a href="<?php echo base_url("mei/listar/EXCLUIDAS"); ?>" style="font-size: 18px;" class="tag_a_hover">Excluídas: <?php echo $qtd_mei_excluidas; ?></a>  
                      </div>
                    </p>

                    <p>
                      <div class="div_hover">
                      <a href="<?php echo base_url("mei/listar/OPTANTES"); ?>" style="font-size: 18px;" class="tag_a_hover">Optantes: <?php echo $qtd_mei_optantes; ?></a>   
                      </div>
                    </p>

                  </div>
                </div>

                <div class="align-self-center text-center"></div>
                <div class="media-body"><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Exclusão do MEI</span>
                  <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas que foram excluídas do MEI' data-original-title='' title=''><?php echo $qtd_mei_excluidas; ?></div></h4>
                  <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
                </div>
              </div>
            </div>
          </div>
        </a>
    </div>

    </div>

    <div class="row">

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php if($server == "demo"){echo base_url("das/index");}else{echo base_url("das/index"); } ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">

              <div class="details">
              <div class="center">
                <h1><br><span style="font-size: 14px;color: #24a0e9" class="span_hover">DAS</span></h1>
                <p>
                  <div class="div_hover">
                  <a href="<?php if($server == "demo"){echo base_url("das/index/proximo/Janeiro/2021?filtro=REGULAR");}else{echo base_url("das/index?filtro=REGULAR"); } ?>" style="font-size: 13px;" class="tag_a_hover">Pagos: <?php if($server == "demo"){echo 54;}else{echo $qtd_das_pagos; } ?></a>
                  </div>
                </p>

                <p>
                  <div class="div_hover">
                  <a href="<?php if($server == "demo"){echo base_url("das/index/proximo/Janeiro/2021?filtro=PENDENTE");}else{echo base_url("das/index?filtro=PENDENTE"); } ?>" style="font-size: 13px;" class="tag_a_hover">Não Pagos: <?php if($server == "demo"){echo 15;}else{echo $qtd_das; } ?></a>  
                  </div>
                </p>

                <p>
                  <div class="div_hover">
                  <a href="<?php if($server == "demo"){echo base_url("das/index/proximo/Janeiro/2021?filtro=DEBITOS");}else{echo base_url("das/index?filtro=DEBITOS"); } ?>" style="font-size: 13px;" class="tag_a_hover">Débitos: <?php if($server == "demo"){echo 1;}else{echo $qtd_das_debitos; } ?></a>
                  </div>
                </p>

                  <p>
                  <div class="div_hover">
                      <a href="<?php if($server == "demo"){echo base_url("das/index/proximo/Janeiro/2021?filtro=DUPLICIDADE");}else{echo base_url("das/index?filtro=DUPLICIDADE"); } ?>" style="font-size: 13px;" class="tag_a_hover">Em Duplicidade: <?php if($server == "demo"){echo 1;}else{echo $qtd_das_duplicidade; } ?></a>
                  </div>
                  </p>
                
              </div>
            </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">DAS Não Pagos</span>
                <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com DAS não pagos' data-original-title='' title=''><?php if($server == "demo"){echo 15;}else{echo $qtd_das;} ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("ecac/listar_cadin"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <h1><br><span style="font-size: 20px;color: #24a0e9" class="span_hover">CADIN</span></h1>
                <p>
                  <div class="div_hover">
                  <a href="<?php echo base_url("ecac/listar_cadin_regular"); ?>" style="font-size: 18px;" class="tag_a_hover">Regulares: <?php echo $qtd_empresas_cadin_regulares->valor; ?></a>  
                  </div>
                </p>

                <p>
                  <div class="div_hover">
                  <a href="<?php echo base_url("ecac/listar_cadin_pendencia"); ?>" style="font-size: 18px;" class="tag_a_hover">Pendências: <?php echo $qtd_empresas_cadin->valor; ?></a>  
                  </div>
                </p>

              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">CADIN - Pendências</span>
              <h4 class="mb-0"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas irregulares no CADIN' data-original-title='' title=''><?php echo $qtd_empresas_cadin->valor; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("procuracao/listar"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;">
        <div class="b-r-4 card-body"  style="color: white;">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 20px;color: #24a0e9" class="span_hover">Procurações</span>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("procuracao/proximas_vencer"); ?>" style="font-size: 18px;" class="tag_a_hover">Próximas ao Vencimento: <?php echo $qtd_procuracoes_proximas_vencer; ?></a>
                  </div>
                </p>
                <p>
                  <div class="div_hover">
                    <a href="<?php echo base_url("procuracao/vencidas"); ?>" style="font-size: 18px;" class="tag_a_hover">Vencidas: <?php echo $qtd_procuracoes_vencidas; ?></a>  
                  </div>
                </p>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Procurações</span>
              <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com procurações próximas ao vencimento' data-original-title='' title=''><?php echo $qtd_procuracoes_vencidas + $qtd_procuracoes_proximas_vencer; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>

    

    <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
      <a href="<?php echo base_url("certificado/listar_certificados_todos"); ?>">     
      <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
        <div class="b-r-4 card-body"  style="color: white;">
          <div class="media static-top-widget">

            <div class="details">
              <div class="center">
                <br><span style="font-size: 18px;color: #24a0e9" class="span_hover">Certificados</span>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certificado/listar_certificados_proximos"); ?>" style="font-size: 16px;" class="tag_a_hover">Próximos ao Vencimento: <?php echo $qtd_certificados_proximos_vencer; ?></a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("certificado/listar_certificados_vencidos"); ?>" style="font-size: 16px;" class="tag_a_hover">Vencidos: <?php echo $qtd_certificados_vencidos; ?></a>  
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("ecac/listar_empresas_sem_certificado"); ?>" style="font-size: 16px;" class="tag_a_hover">Sem certificado: <?php echo $qtd_empresas_sem_certificado; ?></a>  
                  </div>
                </teste>
              </div>
            </div>

            <div class="align-self-center text-center"></div>
            <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Certificados</span>
              <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Nº de Empresas com certificado digital próximo ao vencimento' data-original-title='' title=''><?php echo $qtd_certificados_proximos_vencer + $qtd_certificados_vencidos; ?></div></h4>
              <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
            </div>
          </div>
        </div>
      </div>
      </a>
    </div>


    </div>


    <div class="row">
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("pronampe/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details" style="padding-top: 50px !important;">
                <div class="center" style="padding-top: 60px !important;">
                    <span style="font-size: 18px;color: #24a0e9; margin-bottom:25px !important" class="span_hover">PRONAMPE</span>
                    <br>
                    <p>
                      <div class="div_hover">
                        <a href="<?php echo base_url("pronampe/listar"); ?>" style="font-size: 18px;" class="tag_a_hover">Habilitadas: <?php echo $qtd_pronampe; ?></a>
                      </div>
                    </p>
                </div>
              </div>
              
              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">PRONAMPE</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Empresas habilitadas para o PRONAMPE' data-original-title='' title=''><?php echo $qtd_pronampe; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("malha_fiscal/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
              <div class="center">
                <h1><span style="font-size: 16px;color: #24a0e9" class="span_hover">Malha Fiscal</span></h1>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("malha_fiscal/listar/REGULARIZADO"); ?>" style="font-size: 15px;" class="tag_a_hover">Regularizado: <?php echo $qtd_malha_fiscal_regular; ?></a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("malha_fiscal/listar/ANALISANDO"); ?>" style="font-size: 15px;" class="tag_a_hover">Analisando: <?php echo $qtd_malha_fiscal_analise; ?></a>  
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("malha_fiscal/listar/PENDENCIAS"); ?>" style="font-size: 15px;" class="tag_a_hover">Pendências: <?php echo $qtd_malha_fiscal_pendencia; ?></a>  
                  </div>
                </teste>

                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("malha_fiscal/listar/CLIENTE"); ?>" style="font-size: 15px;" class="tag_a_hover">Aguardando cliente: <?php echo $qtd_malha_fiscal_cliente; ?></a>  
                  </div>
                </teste>

              </div>
            </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Malha Fiscal</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Empresas habilitadas para o PRONAMPE' data-original-title='' title=''><?php echo $qtd_malha_fiscal_pendencia; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>
      

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("notificacao_exclusao/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
              <div class="center">
                <h1><span style="font-size: 16px;color: #24a0e9" class="span_hover">Termo de Excl. Simples</span></h1>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("notificacao_exclusao/listar/REGULARIZADO"); ?>" style="font-size: 15px;" class="tag_a_hover">Regularizado: <?php echo $qtd_notificacao_exclusao_regular; ?></a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("notificacao_exclusao/listar/ANALISANDO"); ?>" style="font-size: 15px;" class="tag_a_hover">Analisando: <?php echo $qtd_notificacao_exclusao_analise; ?></a>  
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("notificacao_exclusao/listar/PENDENCIAS"); ?>" style="font-size: 15px;" class="tag_a_hover">Pendências: <?php echo $qtd_notificacao_exclusao_pendencia; ?></a>  
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("notificacao_exclusao/listar/CLIENTE"); ?>" style="font-size: 15px;" class="tag_a_hover">Aguardando cliente: <?php echo $qtd_notificacao_exclusao_cliente; ?></a>  
                  </div>
                </teste>
              </div>
            </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Termo de Exclusão Simples</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Empresas habilitadas para o PRONAMPE' data-original-title='' title=''><?php echo $qtd_notificacao_exclusao_pendencia; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>


      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <a href="<?php echo base_url("termo_intimacao/listar"); ?>">     
        <div class="card gradient-primary o-hidden" style="height: 130px; margin-bottom: 20px !important;" >
          <div class="b-r-4 card-body"  style="color: white;">
            <div class="media static-top-widget">

              <div class="details">
              <div class="center">
                <h1><span style="font-size: 16px;color: #24a0e9" class="span_hover">Termo de Intimação</span></h1>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("termo_intimacao/listar/REGULARIZADO"); ?>" style="font-size: 15px;" class="tag_a_hover">Regularizado: <?php echo $qtd_notificacao_intimacao_regular; ?></a>
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("termo_intimacao/listar/ANALISANDO"); ?>" style="font-size: 15px;" class="tag_a_hover">Analisando: <?php echo $qtd_notificacao_intimacao_analise; ?></a>  
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("termo_intimacao/listar/PENDENCIAS"); ?>" style="font-size: 15px;" class="tag_a_hover">Pendências: <?php echo $qtd_notificacao_intimacao_pendencia; ?></a>  
                  </div>
                </teste>
                <teste>
                  <div class="div_hover">
                    <a href="<?php echo base_url("termo_intimacao/listar/CLIENTE"); ?>" style="font-size: 15px;" class="tag_a_hover">Aguardando cliente: <?php echo $qtd_notificacao_intimacao_cliente; ?></a>  
                  </div>
                </teste>
              </div>
            </div>

              <div class="align-self-center text-center"></div>
              <div class="media-body" ><span class="m-0 text-white" style="font-weight: bold;font-size: 18px">Termo de Intimação</span>
                <h4 class="mb-0" style="color: white;"><div style="width: 50px" data-toggle='popover' data-trigger='hover' data-placement='top' data-content='Empresas habilitadas para o PRONAMPE' data-original-title='' title=''><?php echo $qtd_notificacao_intimacao_pendencia; ?></div></h4>
                <svg class="icon-bg" xmlns="http://www.w3.org/2000/svg" version="1.0" width="128.000000pt" height="128.000000pt" viewBox="0 0 128.000000 128.000000" preserveAspectRatio="xMidYMid meet" ><metadata></metadata><g transform="translate(0.000000,128.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path fill="white" d="M465 1270 c-3 -5 -16 -10 -29 -10 -12 0 -27 -5 -34 -12 -8 -8 -15 -8 -25 1 -11 9 -17 8 -27 -4 -10 -13 -10 -19 4 -34 16 -18 15 -20 -17 -40 -43 -26 -63 -27 -77 -1 -8 16 -21 20 -57 20 -40 0 -45 -2 -40 -20 3 -10 0 -21 -7 -23 -8 -3 -8 -12 0 -33 6 -17 7 -39 3 -49 -4 -11 -10 -32 -13 -46 -5 -21 -14 -29 -43 -36 -46 -11 -99 -80 -89 -117 10 -41 47 -78 72 -73 18 3 24 -1 26 -17 4 -30 59 -33 104 -7 48 28 62 27 56 -4 -6 -35 19 -61 96 -97 59 -27 63 -31 57 -55 -7 -29 9 -53 36 -53 12 0 19 -7 19 -18 0 -10 4 -22 10 -28 6 -6 6 -29 -2 -65 -14 -68 -9 -84 31 -92 26 -6 31 -11 31 -37 0 -21 5 -30 16 -30 12 0 15 -6 10 -25 -4 -14 -2 -25 4 -25 5 0 10 -11 10 -25 0 -17 -9 -29 -27 -39 -16 -8 -39 -25 -52 -39 -21 -24 -22 -28 -10 -56 8 -17 19 -31 25 -31 6 0 21 -11 34 -25 29 -31 62 -32 75 -3 6 13 20 27 31 33 35 17 72 68 79 110 9 49 19 61 59 79 53 23 70 28 109 31 31 2 49 12 88 51 28 27 53 60 56 74 3 14 12 34 19 45 7 11 16 44 20 74 6 44 14 60 50 97 23 24 46 44 51 44 20 0 73 79 73 109 0 42 -17 81 -35 81 -9 0 -38 23 -66 50 -40 40 -56 50 -84 50 -19 0 -37 5 -41 11 -3 6 -21 9 -39 7 -26 -3 -31 -1 -28 13 5 19 -60 59 -97 59 -11 0 -20 6 -20 14 0 23 -57 30 -88 11 -20 -13 -17 -8 11 18 40 39 45 52 23 60 -7 3 -16 22 -20 41 -8 40 -18 45 -38 19 -29 -39 -45 -45 -81 -32 -20 7 -42 9 -53 4 -11 -4 -38 -10 -61 -12 -34 -3 -44 1 -53 17 -7 13 -7 26 -1 38 19 36 -13 85 -34 52z"/></g></svg>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>

    </div>
    
    
  </div>
</div> 
 

<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->
<!-- latest jquery-->
<script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
<!-- Bootstrap js-->
<script src="<?php echo base_url('assets/js/bootstrap/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.js'); ?>"></script>
<!-- feather icon js-->
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/icons/feather-icon/feather-icon.js'); ?>"></script>
<!-- Sidebar jquery-->
<script src="<?php echo base_url('assets/js/sidebar-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/config.js'); ?>"></script>
<!-- Plugins JS start-->
<script src="<?php echo base_url('assets/js/counter/jquery.waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/jquery.counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counter/counter-custom.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/chat-menu.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/print.js'); ?>"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/theme-customizer/customizer.js'); ?>"></script>
<!-- login js-->
<!-- PLUGINS OBRIGATÓRIOS DA PÁGINA -->

<script type="text/javascript">
  $(document).ready(function() {
    

      $('[data-toggle="popover"]').popover({
        boundary:'window',
        html: true
      });

    });
</script>