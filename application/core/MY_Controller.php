<?php

class MY_Controller extends CI_Controller
{
	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model'); 			
		$this->load->model('consulta_sipro_model'); 
		$this->load->model('caixadeemail_model');	
		$this->load->model('notificacao_model');
		$this->load->model('usuario_model');			
    }

    public function notificacoes(){

    	//ESTADUAIS
    	// $dados['estaduais'] = $this->notificacao_model->estaduais();
    	// $dados['qtd_estaduais'] = $this->empresa_model->qtd_notificacoes()->valor + $this->resumofiscal_model->qtd_notificacoes_resumofiscal()->valor;

    	//FEDERAIS
    	// $dados['federais'] = $this->notificacao_model->federais();
    	// $dados['qtd_federais'] = $this->notificacao_model->qtd_federais()->valor;

    	//MENSAGENS
    	// $dados['mensagens'] = $this->notificacao_model->mensagens();
    	// $dados['qtd_mensagens'] = $this->notificacao_model->qtd_mensagens()->total;

		//NOTIFICAÇÕES DOS DOCUMENTOS
		$dados['notificacoes_documentos'] = $this->documento_model->notificacoes_documentos();
		$dados['qtd_notificacoes_documentos'] = $this->documento_model->qtd_notificacoes_documentos();
		
		//NOTIFICAÇÕES PROCESSOS
		// $dados['notificacoes_consulta_sipro'] = $this->consulta_sipro_model->notificacoes_consulta_sipro();
		// $dados['qtd_notificacoes_consulta_sipro'] = $this->consulta_sipro_model->qtd_notificacoes_consulta_sipro();

		//NOTIFICAÇÕES CERTIFICADOS
		// $dados['certificados'] = $this->notificacao_model->certidoes();

		//Mensagens não lidas DTE
		// $consulta = $this->resumofiscal_model->dashboard11();
		// $somaMsgNaoLidas = 0;
		// foreach ($consulta as $value) {
		// 	$valor = trim($this->soNumero($value->qtd));
		// 	$somaMsgNaoLidas = $somaMsgNaoLidas + $valor;
		// }
		
		// $dados['qtd_mensagens_dte'] = $somaMsgNaoLidas;

		// $dados['listagem_mensagens'] = $this->caixadeemail_model->qtd_msgs_dte_nova_listar();
		// $dados['msg_dte_nova'] = $this->caixadeemail_model->qtd_msgs_dte_nova()->valor;

		return $dados;
    }

    public function soNumero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	}


	public function empresas_colaborador(){
		$lista_empresa_usuario = $this->usuario_model->find_empresas_user();
	    
	    $lista = array();
	    if( sizeof($lista_empresa_usuario) == 0 ){
	    	array_push($lista, '1');
	    }else{
	    	foreach ($lista_empresa_usuario as $e) {
		    	array_push($lista, $e->cnpj);
		    }
	    }
	    
		return $lista;
		
	}
	
}
