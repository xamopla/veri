<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrodeacesso extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
    }

	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function listar() {
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->load->model('funcionario_model', 'a');
		$this->a->setIdCadastro($this->session->userdata['userprimesession']['id']);
		
		//GERAIS DIRETORES
		$dados['consulta'] = $this->a->listar_painel();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('registrodeacesso/registrodeacesso_listar', $dados);
		$this->load->view('layout/footer');
		
	}
}
