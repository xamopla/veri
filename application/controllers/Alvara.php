<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alvara extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('alvara_model');
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->listar();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function vencidos(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->vencidos();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}
	
	public function ativaprovisoria(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->ativaprovisoria();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function ativaregular(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->ativaregular();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function ativatemporaria(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->ativatemporaria();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function suspensaRecadastramento(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->suspensaRecadastramento();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function suspensaRenovacao(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->suspensaRenovacao();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function vencidosPublicidade(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->vencidosPublicidade();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function vencidosFuncionamento(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->vencidosFuncionamento();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function semCpfRepresentante(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->alvara_model->semCpfRepresentante();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('alvara/alvara_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function download(){
		$cnpj = $this->uri->segment(3);
		include ( 'PdfToText/PdfToText.phpclass' ) ;
		
		if($cnpj != ''){
			$data = array('ctl00$ScriptManagerPrincipal'=>'',//tctl00$updPopUp//'ctl00$updPanel|ctl00$PHCentro$btnLogin', 
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__LASTFOCUS'=>'',
			'__VIEWSTATE'=>'/wEPDwUKLTY0MzUxMzE1Ng9kFgJmD2QWBAIBD2QWAgIIDxUFJS9XZWJzaXRlVjIvTWVudS9idWxsZXRfYXJyb3dfZG93bi5naWYmL1dlYnNpdGVWMi9NZW51L2J1bGxldF9hcnJvd19yaWdodC5naWYiL1dlYnNpdGVWMi9Kcy9qcXVlcnktMS4xMC4yLm1pbi5qcxwvV2Vic2l0ZVYyL0pzL2pxdWVyeS5tYXNrLmpzIi9XZWJzaXRlVjIvTWVudS9qcXVlcnlzbGlkZW1lbnUuanNkAgMPZBYGAgcPZBYCZg9kFgICAQ8PFgQeB1Zpc2libGVoHgRUZXh0BSJGYXZvciBzZWxlY2lvbmFyIHVtYSBkYXMgb3DDp8O1ZXMhZGQCCQ9kFhICAQ8PFgIfAGhkZAIDDw8WBB4IQ3NzQ2xhc3MFKXBhbmVsTWVuc2FnZW0gcGFuZWxNZW5zYWdlbVF1ZXN0aW9uYW1lbnRvHgRfIVNCAgJkFgQCCg8PFgIfAGhkZAIMDxYEHg5Qb3B1cENvbnRyb2xJRAUOcG5sQ29uZmlybWFjYW8eEkJhY2tncm91bmRDc3NDbGFzcwUXcGFuZWxNZW5zYWdlbUJhY2tHcm91bmRkAgcPZBYCZg9kFgoCAQ8QDxYCHgdDaGVja2VkZ2RkZGQCAw8PFgIeCU1heExlbmd0aGZkZAIFDxYUHhFDdWx0dXJlRGF0ZUZvcm1hdAUDRE1ZHiBDdWx0dXJlQ3VycmVuY3lTeW1ib2xQbGFjZWhvbGRlcgUCUiQeFkN1bHR1cmVUaW1lUGxhY2Vob2xkZXIFAToeFkN1bHR1cmVEYXRlUGxhY2Vob2xkZXIFAS8eE092ZXJyaWRlUGFnZUN1bHR1cmVoHgtDdWx0dXJlTmFtZQUFcHQtQlIeG0N1bHR1cmVUaG91c2FuZHNQbGFjZWhvbGRlcgUBLh4WQ3VsdHVyZUFNUE1QbGFjZWhvbGRlcmUeCkFjY2VwdEFtUG1oHhlDdWx0dXJlRGVjaW1hbFBsYWNlaG9sZGVyBQEsZAIJDw8WAh8HZmRkAgsPFhQfCAUDRE1ZHwkFAlIkHwoFATofCwUBLx8MaB8NBQVwdC1CUh8OBQEuHw9lHxBoHxEFASxkAgkPPCsADQBkAgsPDxYGHwEFODxoMj5PYnM6IFBhcmEgdW1hIG5vdmEgY29uc3VsdGEgY2xpcXVlIGVtIENhbmNlbGFyLjwvaDI+HwIFEFBTX2FyZWFhQ29udGV1ZG8fAwICZGQCDQ8PFgQfAgUMc3VibWl0QWp1ZGFyHwMCAmRkAg8PDxYEHwIFDnN1Ym1pdENhbmNlbGFyHwMCAmRkAhEPDxYEHwIFD3N1Ym1pdENvbnN1bHRhch8DAgJkZAITDw8WAh8BBRhWZXJzYW8gQXNzZW1ibHk6IDEuMC4wLjFkZAIND2QWAmYPZBYCAgEPDxYEHwIFJXBhbmVsTWVuc2FnZW0gcGFuZWxNZW5zYWdlbUluZm9ybWFjYW8fAwICZBYGAggPDxYCHwBoZGQCCQ8PFgIfAGhkZAIMDxYEHwQFC3BubU1lbnNhZ2VtHwUFF3BhbmVsTWVuc2FnZW1CYWNrR3JvdW5kZBgCBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAwUrY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyUHJpbmNpcGFsJFJkQk51Q25wagUqY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyUHJpbmNpcGFsJFJkQk51Q2dhBSpjdGwwMCRDb250ZW50UGxhY2VIb2xkZXJQcmluY2lwYWwkUmRCTnVDZ2EFMGN0bDAwJENvbnRlbnRQbGFjZUhvbGRlclByaW5jaXBhbCRHclZFbmRlcmVjb0NnYQ9nZO+9U2GPAv7tL1GhYqf8JeuRlQri',
			'__VIEWSTATEGENERATOR'=>'9B9D2AAC',
			'__EVENTVALIDATION'=>'/wEWDwLcmM3RAwK4l6u0DwLiz/KpCwKs+vfiDALYsJCPBgL0prq/AQKY3pWQAwLxqN6/DwK+1teYBgKF1oKZAgKH5ZmxDwLT8P6fCgKW+7nlBgKw8tmuAgLV8fjOAyK7GfatkFols6P6dndrz5yBVf4k',
			'ctl00$ContentPlaceHolderPrincipal$BtnConsultar0'=>'Consultar',
			'ctl00$ContentPlaceHolderPrincipal$grpPesquisa'=>'RdBNuCnpj',
			'ctl00$ContentPlaceHolderPrincipal$txtNuCnpj'=>$cnpj); //18430685000171 -- 97376057000121

			$url = "http://servicosweb.sefaz.salvador.ba.gov.br/WebsiteV2/Sistemas/AlvaraCgaInternet/Modulos/Principal/AlvaraCgaEmissaoFichaResumidaFrm.aspx";
			
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			
			curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);
			curl_close($ch);

			$pdf 	=  new PdfToText () ;
			$pdf -> LoadFromString($store);

			header('Cache-Control: public'); 
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="new.pdf"');
			header('Content-Length: '.strlen($store));
			echo $store;

		}
		
	}


	function processamentoAlvara(){
		$resultado = $this->alvara_model->listarForProcessamento();

		foreach ($resultado as $row) {
			$cnpj = $row->cnpj;
			$id_empresa = $row->id_empresa;
			$idTipoDocumento = 3;

			$documento = $this->documento_model->pesquisar_documento_processamento_alvara($id_empresa, $idTipoDocumento);
			if (isset($documento)){
				$this->documento_model->setId($documento->id);
				$this->documento_model->setAtivo($documento->ativo);
				$this->documento_model->setIdEmpresa($documento->id_empresa);
				$this->documento_model->setIdTipoDocumento($documento->id_tipoDocumento);
				$this->documento_model->setNumeroDocumento($documento->numero_documento);
				$this->documento_model->setNumeroProtocolo($documento->numero_protocolo);
				$this->documento_model->setDataEmissao($documento->dataEmissao);
				$this->documento_model->setDataValidade($documento->dataValidade);
				$this->documento_model->setDiasNotificacao($documento->diasNotificacao);
				$this->documento_model->setObservacoes($documento->observacoes);
					
				$result = $this->documento_model->editar();
						
				if ($result){
					echo "Update documento da empresa - ".$cnpj;

				} else {
					
					echo "Erro Update documento da empresa - ".$cnpj;
				}
			}else{
				
				$dataAtual = date('Y-m-d');
				$dataValidadeAlvara = $row->validade;
				$dateAux = str_replace('/', '-', $dataValidadeAlvara);
				$dateAux = date('Y-m-d', strtotime($dateAux));

				$this->documento_model->setAtivo("Sim");
				$this->documento_model->setIdEmpresa($id_empresa);
				$this->documento_model->setIdTipoDocumento($idTipoDocumento);
				$this->documento_model->setNumeroDocumento("");
				$this->documento_model->setNumeroProtocolo("");
				$this->documento_model->setDataEmissao($dataAtual);
				$this->documento_model->setDataValidade($dateAux);
				$this->documento_model->setDiasNotificacao(30);
				$this->documento_model->setObservacoes("");
				$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);

				$result = $this->documento_model->cadastrar();

				if ($result){
					echo "Insert documento da empresa - ".$cnpj;

				} else {
					
					echo "Erro Insert documento da empresa - ".$cnpj;
				}
			}

		}
	}

	public function download_alvara_publicidade(){
		$valorCGA = $this->uri->segment(3);
		include ( 'PdfToText/PdfToText.phpclass' ) ;
		
		if($valorCGA != ''){
			$url  = 'http://www.sucom.ba.gov.br/servicosonline/Web/emissaoAlvaraPublicidadeView.php';
			$data = ['cgaEmpresa' => $valorCGA, 'emissaoTvl' => '1', 'acao'=>'VERIFICARALVARAPUBLICIDADE', 'Consultar'=>'Consultar'];
			
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			
			curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);
			curl_close($ch);

			$pdf 	=  new PdfToText () ;
			$pdf -> LoadFromString($store);

			header('Cache-Control: public'); 
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="new.pdf"');
			header('Content-Length: '.strlen($store));
			echo $store;

		}
		
	}

	function processamentoAlvaraPublicidade(){

		$resultado = $this->alvara_model->listarForProcessamentoAlvaraPublicidade();

		foreach ($resultado as $row) {
			$cnpj = $row->cnpj;
			$id_empresa = $row->id_empresa;
			$idTipoDocumento = 200;

			$documento = $this->documento_model->pesquisar_documento_processamento_alvara($id_empresa, $idTipoDocumento);
			if (isset($documento)){
				$this->documento_model->setId($documento->id);
				$this->documento_model->setAtivo($documento->ativo);
				$this->documento_model->setIdEmpresa($documento->id_empresa);
				$this->documento_model->setIdTipoDocumento($documento->id_tipoDocumento);
				$this->documento_model->setNumeroDocumento($documento->numero_documento);
				$this->documento_model->setNumeroProtocolo($documento->numero_protocolo);
				$this->documento_model->setDataEmissao($documento->dataEmissao);
				$this->documento_model->setDataValidade($documento->dataValidade);
				$this->documento_model->setDiasNotificacao($documento->diasNotificacao);
				$this->documento_model->setObservacoes($documento->observacoes);
					
				$result = $this->documento_model->editar();
						
				if ($result){
					echo "Update documento da empresa - ".$cnpj;

				} else {
					
					echo "Erro Update documento da empresa - ".$cnpj;
				}
			}else{
				
				$dataAtual = date('Y-m-d');
				$dataValidadeAlvara = $row->data_validade_publicidade;
				$dateAux = str_replace('/', '-', $dataValidadeAlvara);
				$dateAux = date('Y-m-d', strtotime($dateAux));

				$this->documento_model->setAtivo("Sim");
				$this->documento_model->setIdEmpresa($id_empresa);
				$this->documento_model->setIdTipoDocumento($idTipoDocumento);
				$this->documento_model->setNumeroDocumento("");
				$this->documento_model->setNumeroProtocolo("");
				$this->documento_model->setDataEmissao($dataAtual);
				$this->documento_model->setDataValidade($dateAux);
				$this->documento_model->setDiasNotificacao(30);
				$this->documento_model->setObservacoes("");
				$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);

				$result = $this->documento_model->cadastrar();

				if ($result){
					echo "Insert documento da empresa - ".$cnpj;

				} else {
					
					echo "Erro Insert documento da empresa - ".$cnpj;
				}
			}

		}
	}

	public function download_alvara_funcionamento(){

		$valorCNPJ = $this->uri->segment(3);
		$valorCGA = $this->uri->segment(4);
		$valorCPF = $this->uri->segment(5);

		include ( 'PdfToText/PdfToText.phpclass' ) ;
		
		if($valorCGA != ''){
			
			$url  = 'http://servicosweb.sefaz.salvador.ba.gov.br/WebsiteV2/Sistemas/AlvaraCgaInternet/Modulos/Principal/AlvaraCgaEmissaoFrm.aspx?opEmissao=A';
			$data = [
				'__EVENTTARGET'=>'ctl00$ContentPlaceHolderPrincipal$btnFakeAlvara',
				'__EVENTARGUMENT'=>'',
				'__LASTFOCUS'=>'',
				'__VIEWSTATE'=>'/wEPDwUKMTExNTY0ODE4NQ8WBB4IRW50aWRhZGUyk0IAAQAAAP////8BAAAAAAAAAAwCAAAAX1NlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMsIFZlcnNpb249MS4wLjAuMSwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsDAMAAABWU2VmYXouQ29ycG9yYXRpdm8uRW5kZXJlY2FtZW50bywgVmVyc2lvbj0xLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPW51bGwFAQAAAE9TZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLkNvbnRyaWJ1aW50ZUF0aXZpZGFkZUVudGlkYWRlFQAAAA5fRGF0YUluc2NyaWNhbxJfSW5zY3JpY2FvRXN0YWR1YWwOX1JlcHJlc2VudGFudGUTX0RvY3VtZW50b011bmljaXBhbApfQXRpdmlkYWRlEV9UaXBvQ29uc3RpdHVpY2FvGF9Db2RpZ29TaXR1YWNhb0NhZGFzdHJhbBtfRGVzY3JpY2FvU2l0dWFjYW9DYWRhc3RyYWwRX05hdHVyZXphSnVyaWRpY2EYX1RpcG9VbmlkYWRlRm9ybWFBdHVhY2FvB19EaXZpZGEQX0VzdGFiZWxlY2ltZW50bxVfT2J0ZXJFc3RhYmVsZWNpbWVudG8MX1RpcG9VbmlkYWRlFEVuZGVyZWNvRW50aWRhZGUrX0lkHUVuZGVyZWNvRW50aWRhZGUrX051bWVyb1BvcnRhHUVuZGVyZWNvRW50aWRhZGUrX0NvbXBsZW1lbnRvGkVuZGVyZWNvRW50aWRhZGUrX0VkaWZpY2lvIUVuZGVyZWNvRW50aWRhZGUrX1BvbnRvUmVmZXJlbmNpYR9FbmRlcmVjb0VudGlkYWRlK19DZXBMb2dyYWRvdXJvHkVuZGVyZWNvRW50aWRhZGUrX0RldGFsaGFtZW50bwMBBAQEBAABBAQEBAAAAQEBAQEEBA9TeXN0ZW0uRGF0ZVRpbWU/U2VmYXouSW50ZXJuZXQuQWx2YXJhQ2dhSW50ZXJuZXQuQ2xhc3Nlcy5FbnRpZGFkZS5Tb2Npb0VudGlkYWRlAgAAAExTZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLkRvY3VtZW50b011bmljaXBhbEVudGlkYWRlAgAAAENTZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLkF0aXZpZGFkZUVudGlkYWRlAgAAAEpTZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLlRpcG9Db25zdGl0dWljYW9FbnRpZGFkZQIAAAAISlNlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuTmF0dXJlemFKdXJpZGljYUVudGlkYWRlAgAAAFFTZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLlRpcG9VbmlkYWRlRm9ybWFBdHVhY2FvRW50aWRhZGUCAAAAQFNlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuRGl2aWRhRW50aWRhZGUCAAAASVNlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuRXN0YWJlbGVjaW1lbnRvRW50aWRhZGUCAAAAAQg+U2VmYXouQ29ycG9yYXRpdm8uRW5kZXJlY2FtZW50by5FbnRpZGFkZS5DZXBMb2dyYWRvdXJvRW50aWRhZGUDAAAARVNlZmF6LkNvcnBvcmF0aXZvLkVuZGVyZWNhbWVudG8uRW50aWRhZGUuRW5kZXJlY29EZXRhbGhhbWVudG9FbnRpZGFkZQMAAAACAAAACA0AGrodO3TTCAYEAAAAAAkFAAAACQYAAAAKCgEAAAAGBwAAAA9BdGl2YSAtIFJlZ3VsYXIJCAAAAAoKCQkAAAABAAAAAAoGCgAAAAY0NzQgICAGCwAAAAZURVJSRU8JBAAAAAYNAAAAHlBST1hJTU8gQSBGQVJNQUNJQSBQQUdVRSBNRU5PUwkOAAAACQ8AAAAMEAAAAFBTZWZhei5Db3Jwb3JhdGl2by5Db250YXRvLCBWZXJzaW9uPTEuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49bnVsbAwRAAAASVN5c3RlbSwgVmVyc2lvbj0yLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODkFBQAAAD9TZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLlNvY2lvRW50aWRhZGUFAAAAEl9UaXBvUmVwcmVzZW50YW50ZRRQZXNzb2FFbnRpZGFkZStfTm9tZRhQZXNzb2FFbnRpZGFkZStfRW5kZXJlY28XUGVzc29hRW50aWRhZGUrX0NvbnRhdG8fUGVzc29hRW50aWRhZGUrX2xpc3RhRG9jdW1lbnRvcwQBBAQES1NlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuVGlwb1JlcHJlc2VudGFudGVFbnRpZGFkZQIAAAA5U2VmYXouQ29ycG9yYXRpdm8uRW5kZXJlY2FtZW50by5FbnRpZGFkZS5FbmRlcmVjb0VudGlkYWRlAwAAACpTZWZhei5Db3Jwb3JhdGl2by5Db250YXRvLkVudGlkYWRlLkNvbnRhdG8QAAAALVN5c3RlbS5Db2xsZWN0aW9ucy5TcGVjaWFsaXplZC5MaXN0RGljdGlvbmFyeREAAAACAAAABe7///9LU2VmYXouSW50ZXJuZXQuQWx2YXJhQ2dhSW50ZXJuZXQuQ2xhc3Nlcy5FbnRpZGFkZS5UaXBvUmVwcmVzZW50YW50ZUVudGlkYWRlAQAAAAd2YWx1ZV9fAAgCAAAAAAAAAAoKCgkTAAAABQYAAABMU2VmYXouSW50ZXJuZXQuQWx2YXJhQ2dhSW50ZXJuZXQuQ2xhc3Nlcy5FbnRpZGFkZS5Eb2N1bWVudG9NdW5pY2lwYWxFbnRpZGFkZQcAAAAHX0NvZGlnbwpfRGVzY3JpY2FvCV9WYWxpZGFkZQ5fVGlwb0RvY3VtZW50bwRfVFZMB19BbHZhcmEEX0NHQQABAwQEBAQIcVN5c3RlbS5OdWxsYWJsZWAxW1tTeXN0ZW0uRGF0ZVRpbWUsIG1zY29ybGliLCBWZXJzaW9uPTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dR1NlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuVGlwb0RvY3VtZW50b0VudGlkYWRlAgAAAD1TZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLlRWTEVudGlkYWRlAgAAAEBTZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLkFsdmFyYUVudGlkYWRlAgAAAD1TZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLkNHQUVudGlkYWRlAgAAAAIAAABA+QQACgoF7P///0dTZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLlRpcG9Eb2N1bWVudG9FbnRpZGFkZQEAAAAHdmFsdWVfXwAIAgAAAAAAAAAJFQAAAAkWAAAACRcAAAAFCAAAAEpTZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLk5hdHVyZXphSnVyaWRpY2FFbnRpZGFkZQIAAAAHX0NvZGlnbwpfRGVzY3JpY2FvAAEIAgAAAAEJAAAGGAAAAERFbXByZXNhIEluZGl2aWR1YWwgZGUgUmVzcG9uc2FiaWxpZGFkZSBMaW1pdGFkYSAoZGUgTmF0LkVtcHJlc8OhcmlhKQUJAAAASVNlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuRXN0YWJlbGVjaW1lbnRvRW50aWRhZGUIAAAADV9Ob21lRmFudGFzaWEWX0NkQ29uc3RpdHVpY2FvRW1wcmVzYRZfRHNDb25zdGl0dWljYW9FbXByZXNhDF9UaXBvVW5pZGFkZRRQZXNzb2FFbnRpZGFkZStfTm9tZRhQZXNzb2FFbnRpZGFkZStfRW5kZXJlY28XUGVzc29hRW50aWRhZGUrX0NvbnRhdG8fUGVzc29hRW50aWRhZGUrX2xpc3RhRG9jdW1lbnRvcwEAAQABBAQECAg5U2VmYXouQ29ycG9yYXRpdm8uRW5kZXJlY2FtZW50by5FbnRpZGFkZS5FbmRlcmVjb0VudGlkYWRlAwAAACpTZWZhei5Db3Jwb3JhdGl2by5Db250YXRvLkVudGlkYWRlLkNvbnRhdG8QAAAALVN5c3RlbS5Db2xsZWN0aW9ucy5TcGVjaWFsaXplZC5MaXN0RGljdGlvbmFyeREAAAACAAAABhkAAAAPRE9DRSBQQU8gQlJPVEFTAQAAAAYaAAAABk1hdHJpegEAAAAGGwAAADFJTkRVU1RSSUEgREVMSUNBVEVTU0VOIEUgQ09NRVJDSU8gRE9DRSBQQU8gRUlSRUxJCgoJHAAAAAUOAAAAPlNlZmF6LkNvcnBvcmF0aXZvLkVuZGVyZWNhbWVudG8uRW50aWRhZGUuQ2VwTG9ncmFkb3Vyb0VudGlkYWRlAgAAAAtfTG9ncmFkb3VybwRfQ2VwBAQ7U2VmYXouQ29ycG9yYXRpdm8uRW5kZXJlY2FtZW50by5FbnRpZGFkZS5Mb2dyYWRvdXJvRW50aWRhZGUDAAAANFNlZmF6LkNvcnBvcmF0aXZvLkVuZGVyZWNhbWVudG8uRW50aWRhZGUuQ2VwRW50aWRhZGUDAAAAAwAAAAkdAAAACR4AAAAFDwAAAEVTZWZhei5Db3Jwb3JhdGl2by5FbmRlcmVjYW1lbnRvLkVudGlkYWRlLkVuZGVyZWNvRGV0YWxoYW1lbnRvRW50aWRhZGUDAAAACV9Db25qdW50bwVfTG90ZQdfUXVhZHJhAQEBAwAAAAoKCgUTAAAALVN5c3RlbS5Db2xsZWN0aW9ucy5TcGVjaWFsaXplZC5MaXN0RGljdGlvbmFyeQQAAAAEaGVhZAd2ZXJzaW9uBWNvdW50CGNvbXBhcmVyBAAAAzxTeXN0ZW0uQ29sbGVjdGlvbnMuU3BlY2lhbGl6ZWQuTGlzdERpY3Rpb25hcnkrRGljdGlvbmFyeU5vZGURAAAACAgcU3lzdGVtLkNvbGxlY3Rpb25zLklDb21wYXJlchEAAAAJHwAAAAEAAAABAAAACgUVAAAAPVNlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuVFZMRW50aWRhZGUBAAAAD19TdGF0dXNTZXJpZVRWTAECAAAABiAAAAABQQUWAAAAQFNlZmF6LkludGVybmV0LkFsdmFyYUNnYUludGVybmV0LkNsYXNzZXMuRW50aWRhZGUuQWx2YXJhRW50aWRhZGUCAAAAD19Db2RpZ29Db250cm9sZQ9fRGF0YVZlbmNpbWVudG8BAQIAAAAGIQAAACBGQzgxQzI1QjE0NkJBNDU0QUYxN0QwNUIzMTM0RTY3NQoFFwAAAD1TZWZhei5JbnRlcm5ldC5BbHZhcmFDZ2FJbnRlcm5ldC5DbGFzc2VzLkVudGlkYWRlLkNHQUVudGlkYWRlAQAAAA9fQ29kaWdvQ29udHJvbGUBAgAAAAkEAAAAARwAAAATAAAACSMAAAACAAAAAgAAAAoFHQAAADtTZWZhei5Db3Jwb3JhdGl2by5FbmRlcmVjYW1lbnRvLkVudGlkYWRlLkxvZ3JhZG91cm9FbnRpZGFkZQYAAAAHX0NvZGlnbwhfRXNwZWNpZQdfVGl0dWxvBV9Ob21lCl9EZXNjcmljYW8HX0JhaXJybwQBAQEBBEFTZWZhei5Db3Jwb3JhdGl2by5FbmRlcmVjYW1lbnRvLkVudGlkYWRlLkNvZGlnb0xvZ3JhZG91cm9FbnRpZGFkZQMAAAA3U2VmYXouQ29ycG9yYXRpdm8uRW5kZXJlY2FtZW50by5FbnRpZGFkZS5CYWlycm9FbnRpZGFkZQMAAAADAAAACSQAAAAKCgYlAAAAFEF2ZW5pZGEgRG9tIEpvw6NvIFZJCgkmAAAABR4AAAA0U2VmYXouQ29ycG9yYXRpdm8uRW5kZXJlY2FtZW50by5FbnRpZGFkZS5DZXBFbnRpZGFkZQEAAAAGX1ZhbG9yAAgDAAAASbNmAgwnAAAAUlNlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50bywgVmVyc2lvbj0xLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPW51bGwFHwAAADxTeXN0ZW0uQ29sbGVjdGlvbnMuU3BlY2lhbGl6ZWQuTGlzdERpY3Rpb25hcnkrRGljdGlvbmFyeU5vZGUDAAAAA2tleQV2YWx1ZQRuZXh0AwIEH1N5c3RlbS5Vbml0eVNlcmlhbGl6YXRpb25Ib2xkZXI8U3lzdGVtLkNvbGxlY3Rpb25zLlNwZWNpYWxpemVkLkxpc3REaWN0aW9uYXJ5K0RpY3Rpb25hcnlOb2RlEQAAABEAAAAJKAAAAAkpAAAACgEjAAAAHwAAAAkqAAAACSsAAAAJLAAAAAUkAAAAQVNlZmF6LkNvcnBvcmF0aXZvLkVuZGVyZWNhbWVudG8uRW50aWRhZGUuQ29kaWdvTG9ncmFkb3Vyb0VudGlkYWRlAgAAAAdfQ29kaWdvB19EaWdpdG8AAAgHAwAAAFEDAAAAAAUmAAAAN1NlZmF6LkNvcnBvcmF0aXZvLkVuZGVyZWNhbWVudG8uRW50aWRhZGUuQmFpcnJvRW50aWRhZGUDAAAAA19JZAVfTm9tZQpfTXVuaWNpcGlvAAEECDpTZWZhei5Db3Jwb3JhdGl2by5FbmRlcmVjYW1lbnRvLkVudGlkYWRlLk11bmljaXBpb0VudGlkYWRlAwAAAAMAAAC2lAAABi0AAAAGQlJPVEFTCgQoAAAAH1N5c3RlbS5Vbml0eVNlcmlhbGl6YXRpb25Ib2xkZXIDAAAABERhdGEJVW5pdHlUeXBlDEFzc2VtYmx5TmFtZQEAAQgGLgAAAChTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuQ3BmBAAAAAYvAAAAUlNlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50bywgVmVyc2lvbj0xLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPW51bGwFKQAAAChTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuQ3BmBAAAAAVfQmFzZQdfRGlnaXRvDF9UaXBvc1BhcnRlcyNEb2N1bWVudG9JZGVudGlmaWNhY2FvK19UaXBvc1BhcnRlcwMDAwMMU3lzdGVtLkludDMyDFN5c3RlbS5JbnQzMrABU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuTGlzdGAxW1tTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuVGlwb1BhcnRlRG9jdW1lbnRvLCBTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8sIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsXV2wAVN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLkxpc3RgMVtbU2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLkVudGlkYWRlLlRpcG9QYXJ0ZURvY3VtZW50bywgU2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLCBWZXJzaW9uPTEuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49bnVsbF1dJwAAAAgIwdjdAAgIPAAAAAkwAAAACTAAAAABKgAAACgAAAAGMQAAAClTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuQ25wagQAAAAJLwAAAAUrAAAAKVNlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50by5FbnRpZGFkZS5DbnBqBQAAAAVfQmFzZQdfRmlsaWFsB19EaWdpdG8MX1RpcG9zUGFydGVzI0RvY3VtZW50b0lkZW50aWZpY2FjYW8rX1RpcG9zUGFydGVzAwMDAwMMU3lzdGVtLkludDMyDFN5c3RlbS5JbnQzMgxTeXN0ZW0uSW50MzKwAVN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLkxpc3RgMVtbU2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLkVudGlkYWRlLlRpcG9QYXJ0ZURvY3VtZW50bywgU2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLCBWZXJzaW9uPTEuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49bnVsbF1dsAFTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5MaXN0YDFbW1NlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50by5FbnRpZGFkZS5UaXBvUGFydGVEb2N1bWVudG8sIFNlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50bywgVmVyc2lvbj0xLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPW51bGxdXScAAAAICDpMeQEICAEAAAAICCIAAAAJMwAAAAkzAAAAASwAAAAfAAAACTQAAAAJNQAAAAoEMAAAALABU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuTGlzdGAxW1tTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuVGlwb1BhcnRlRG9jdW1lbnRvLCBTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8sIFZlcnNpb249MS4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1udWxsXV0DAAAABl9pdGVtcwVfc2l6ZQhfdmVyc2lvbgQAADlTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuVGlwb1BhcnRlRG9jdW1lbnRvW10nAAAACAgJNgAAAAIAAAACAAAAATMAAAAwAAAACTcAAAADAAAAAwAAAAE0AAAAKAAAAAY4AAAAKFNlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50by5FbnRpZGFkZS5DZ2EEAAAACS8AAAAFNQAAAChTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuQ2dhBQAAAAVfQmFzZQdfRmlsaWFsB19EaWdpdG8MX1RpcG9zUGFydGVzI0RvY3VtZW50b0lkZW50aWZpY2FjYW8rX1RpcG9zUGFydGVzAwMDAwMMU3lzdGVtLkludDMyDFN5c3RlbS5JbnQzMgxTeXN0ZW0uSW50MzKwAVN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLkxpc3RgMVtbU2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLkVudGlkYWRlLlRpcG9QYXJ0ZURvY3VtZW50bywgU2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLCBWZXJzaW9uPTEuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49bnVsbF1dsAFTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5MaXN0YDFbW1NlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50by5FbnRpZGFkZS5UaXBvUGFydGVEb2N1bWVudG8sIFNlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50bywgVmVyc2lvbj0xLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPW51bGxdXScAAAAICNqYCAAICAEAAAAICAoAAAAJOgAAAAk6AAAABzYAAAAAAQAAAAQAAAAEN1NlZmF6LkNvcnBvcmF0aXZvLkRvY3VtZW50by5FbnRpZGFkZS5UaXBvUGFydGVEb2N1bWVudG8nAAAABcX///83U2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLkVudGlkYWRlLlRpcG9QYXJ0ZURvY3VtZW50bwEAAAAHdmFsdWVfXwAIJwAAAAEAAAABxP///8X///8DAAAAAcP////F////AAAAAAHC////xf///wAAAAAHNwAAAAABAAAABAAAAAQ3U2VmYXouQ29ycG9yYXRpdm8uRG9jdW1lbnRvLkVudGlkYWRlLlRpcG9QYXJ0ZURvY3VtZW50bycAAAABwf///8X///8BAAAAAcD////F////AgAAAAG/////xf///wMAAAABvv///8X///8AAAAAAToAAAAwAAAACUMAAAADAAAAAwAAAAdDAAAAAAEAAAAEAAAABDdTZWZhei5Db3Jwb3JhdGl2by5Eb2N1bWVudG8uRW50aWRhZGUuVGlwb1BhcnRlRG9jdW1lbnRvJwAAAAG8////xf///wEAAAABu////8X///8CAAAAAbr////F////AwAAAAG5////xf///wAAAAALHgxDb250cmlidWludGUFAUUWAmYPZBYEAgEPZBYCAggPFQUlL1dlYnNpdGVWMi9NZW51L2J1bGxldF9hcnJvd19kb3duLmdpZiYvV2Vic2l0ZVYyL01lbnUvYnVsbGV0X2Fycm93X3JpZ2h0LmdpZiIvV2Vic2l0ZVYyL0pzL2pxdWVyeS0xLjEwLjIubWluLmpzHC9XZWJzaXRlVjIvSnMvanF1ZXJ5Lm1hc2suanMiL1dlYnNpdGVWMi9NZW51L2pxdWVyeXNsaWRlbWVudS5qc2QCAw9kFgYCBw9kFgJmD2QWAgIBDw8WAh4HVmlzaWJsZWhkZAIJD2QWAgIFD2QWAmYPZBYgAgEPDxYCHwJoZBYCAgEPDxYCHgRUZXh0BRNFbWlzc8OjbyBkbyBBbHZhcsOhZGQCBw8QDxYCHwMFFkF1dMO0bm9tbyBFc3RhYmVsZWNpZG9kZGRkAgsPDxYEHglNYXhMZW5ndGhmHhBDYXVzZXNWYWxpZGF0aW9uZ2RkAg8PFhQeEUN1bHR1cmVEYXRlRm9ybWF0BQNETVkeIEN1bHR1cmVDdXJyZW5jeVN5bWJvbFBsYWNlaG9sZGVyBQJSJB4WQ3VsdHVyZVRpbWVQbGFjZWhvbGRlcgUBOh4WQ3VsdHVyZURhdGVQbGFjZWhvbGRlcgUBLx4TT3ZlcnJpZGVQYWdlQ3VsdHVyZWgeC0N1bHR1cmVOYW1lBQVwdC1CUh4bQ3VsdHVyZVRob3VzYW5kc1BsYWNlaG9sZGVyBQEuHhZDdWx0dXJlQU1QTVBsYWNlaG9sZGVyZR4KQWNjZXB0QW1QbWgeGUN1bHR1cmVEZWNpbWFsUGxhY2Vob2xkZXIFASxkAhMPDxYCHwRmZGQCFw8WFB8GBQNETVkfBwUCUiQfCAUBOh8JBQEvHwpoHwsFBXB0LUJSHwwFAS4fDWUfDmgfDwUBLGQCGw8PFgIfBGZkZAIfDxYUHwYFA0RNWR8HBQJSJB8IBQE6HwkFAS8fCmgfCwUFcHQtQlIfDAUBLh8NZR8OaB8PBQEsZAIjDw8WBB8EZh8FaGRkAicPFhQfDAUBLh8HBQJSJB8IBQE6HwkFAS8fCmgfCwUFcHQtQlIfBgUDRE1ZHw1lHw5oHw8FASxkAisPDxYGHwMFPjxoMj5PYnM6IFBhcmEgdW1hIG5vdmEgY29uc3VsdGEgY2xpcXVlIGVtIENhbmNlbGFyLjwvaDI+PGJyIC8+HghDc3NDbGFzcwUQUFNfYXJlYWFDb250ZXVkbx4EXyFTQgICZGQCLQ8PFgQfEAUMc3VibWl0QWp1ZGFyHxECAmRkAi8PDxYEHxAFDnN1Ym1pdENhbmNlbGFyHxECAmRkAjEPDxYEHxAFD3N1Ym1pdENvbnN1bHRhch8RAgJkZAIzDw8WBB8QBSlwYW5lbE1lbnNhZ2VtIHBhbmVsTWVuc2FnZW1RdWVzdGlvbmFtZW50bx8RAgJkFgQCCg8PFgIfAmhkZAIMDxYEHg5Qb3B1cENvbnRyb2xJRAUJcG5sTXNnQ0dBHhJCYWNrZ3JvdW5kQ3NzQ2xhc3MFF3BhbmVsTWVuc2FnZW1CYWNrR3JvdW5kZAI1Dw8WBB8QBSlwYW5lbE1lbnNhZ2VtIHBhbmVsTWVuc2FnZW1RdWVzdGlvbmFtZW50bx8RAgJkFgQCCg8PFgIfAmhkZAIMDxYEHxIFDHBubE1zZ0FsdmFyYR8TBRdwYW5lbE1lbnNhZ2VtQmFja0dyb3VuZGQCDQ9kFgJmD2QWAgIBDw8WBB8QBSVwYW5lbE1lbnNhZ2VtIHBhbmVsTWVuc2FnZW1JbmZvcm1hY2FvHxECAmQWBgIIDw8WAh8CaGRkAgkPDxYCHwJoZGQCDA8WBB8SBQtwbm1NZW5zYWdlbR8TBRdwYW5lbE1lbnNhZ2VtQmFja0dyb3VuZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgMFNGN0bDAwJENvbnRlbnRQbGFjZUhvbGRlclByaW5jaXBhbCRSZEJFc3RhYmVtZWNpbWVudG8FLWN0bDAwJENvbnRlbnRQbGFjZUhvbGRlclByaW5jaXBhbCRSZGJBdXRvbm9tbwUtY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyUHJpbmNpcGFsJFJkYkF1dG9ub21vBTFjdGwwMCRDb250ZW50UGxhY2VIb2xkZXJQcmluY2lwYWwkbXZFbWlzc2FvQWx2YXJhDw9kZmQZ8a11U/MwKxMwsm1EIVTd+h384w==',
				'__VIEWSTATEGENERATOR'=>'5A78BEE5',
				'__EVENTVALIDATION'=>'/wEWFwLQndhJAv7X7ZkNAoLs9IwJAoiDoJAEAtup4VUC4s/yqQsCrPr34gwC9Ka6vwECmN6VkAMC+5CqmwgCzfWkvwUC+uut6wsClvu55QYCsPLZrgIC1fH4zgMC+MHYvw0CmKPjlgYC2qeirQcC7oeb3QcC2Z3ioQICyaTAkQIC8ajevw8CvtbXmAZaQUxQj0Oejkuv67BsCF8IO3L+rA==',
				'ctl00$ContentPlaceHolderPrincipal$CONTRIBUINTE'=>'RdBEstabemecimento',
				'ctl00$ContentPlaceHolderPrincipal$txtNuCnpj'=>$valorCNPJ,
				'ctl00$ContentPlaceHolderPrincipal$meeCnpj_ClientState'=>'',
				'ctl00$ContentPlaceHolderPrincipal$txtNuCGA'=>$valorCGA,
				'ctl00$ContentPlaceHolderPrincipal$meeCga_ClientState'=>'',
				'ctl00$ContentPlaceHolderPrincipal$txtNuCpfRepresentante'=>$valorCPF,
				'ctl00$ContentPlaceHolderPrincipal$meeCpf_ClientState'=>'',
				'ctl00$ContentPlaceHolderPrincipal$meeTVL_ClientState'=>'',
				'ctl00$ContentPlaceHolderPrincipal$pnlMsgCGA$hdfFakeControl'=>'',
				'ctl00$ContentPlaceHolderPrincipal$pnlMsgAlvara$hdfFakeControl'=>'',
				'ctl00$pnmMensagem$hdfFakeControl'=>''];
			
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			
			curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);
			curl_close($ch);

			$pdf 	=  new PdfToText () ;
			$pdf -> LoadFromString($store);

			header('Cache-Control: public'); 
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="new.pdf"');
			header('Content-Length: '.strlen($store));
			echo $store;

		}
		
	}

	function processamentoAlvaraFuncionamento(){

		$resultado = $this->alvara_model->listarForProcessamentoAlvaraFuncionamento();

		foreach ($resultado as $row) {
			$cnpj = $row->cnpj;
			$id_empresa = $row->id_empresa;
			$idTipoDocumento = 201;

			$documento = $this->documento_model->pesquisar_documento_processamento_alvara($id_empresa, $idTipoDocumento);
			if (isset($documento)){
				$this->documento_model->setId($documento->id);
				$this->documento_model->setAtivo($documento->ativo);
				$this->documento_model->setIdEmpresa($documento->id_empresa);
				$this->documento_model->setIdTipoDocumento($documento->id_tipoDocumento);
				$this->documento_model->setNumeroDocumento($documento->numero_documento);
				$this->documento_model->setNumeroProtocolo($documento->numero_protocolo);
				$this->documento_model->setDataEmissao($documento->dataEmissao);
				$this->documento_model->setDataValidade($documento->dataValidade);
				$this->documento_model->setDiasNotificacao($documento->diasNotificacao);
				$this->documento_model->setObservacoes($documento->observacoes);
					
				$result = $this->documento_model->editar();
						
				if ($result){
					echo "Update documento da empresa - ".$cnpj;

				} else {
					
					echo "Erro Update documento da empresa - ".$cnpj;
				}
			}else{
				
				$dataAtual = date('Y-m-d');
				$dataValidadeAlvara = $row->data_validade_funcionamento;
				$dateAux = str_replace('/', '-', $dataValidadeAlvara);
				$dateAux = date('Y-m-d', strtotime($dateAux));

				$this->documento_model->setAtivo("Sim");
				$this->documento_model->setIdEmpresa($id_empresa);
				$this->documento_model->setIdTipoDocumento($idTipoDocumento);
				$this->documento_model->setNumeroDocumento("");
				$this->documento_model->setNumeroProtocolo("");
				$this->documento_model->setDataEmissao($dataAtual);
				$this->documento_model->setDataValidade($dateAux);
				$this->documento_model->setDiasNotificacao(30);
				$this->documento_model->setObservacoes("");
				$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);

				$result = $this->documento_model->cadastrar();

				if ($result){
					echo "Insert documento da empresa - ".$cnpj;

				} else {
					
					echo "Erro Insert documento da empresa - ".$cnpj;
				}
			}

		}
	}

}
