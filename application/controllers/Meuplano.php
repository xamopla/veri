<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meuplano extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('empresa_model', 'e');
		$this->load->model('usuario_model', 'u');
		$this->load->model('plano_model', 'p');
        $this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
    }

	public function index()
	{		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$qtd_empresas = $this->e->qtd_empresas();
		$qtd_usuarios = $this->u->qtd_usuarios();

		$qtd_max_empresas = $this->p->valor_max_empresas();
		$qtd_max_usuarios = $this->p->valor_max_usuarios();
		$nome_do_plano = $this->p->nome_do_plano();

		$dados['porcentagemEmpresas'] = ($qtd_empresas->valor/$qtd_max_empresas->valor) * 100;
		$dados['porcentagemUsuarios'] = ($qtd_usuarios->valor/$qtd_max_usuarios->valor) * 100;
			
		$dados['qtd_empresas'] = $qtd_empresas->valor;
		$dados['qtd_usuarios'] = $qtd_usuarios->valor;
		$dados['qtd_max_empresas'] = $qtd_max_empresas->valor;
		$dados['qtd_max_usuarios'] = $qtd_max_usuarios->valor;
		$dados['nome_do_plano'] = $nome_do_plano->valor;		

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('usuario/meu_plano', $dados);
		$this->load->view('layout/footer');
		
	}

	public function relatorioplano()
	{

		$qtd_empresas = $this->e->qtd_empresas();
		$qtd_usuarios = $this->u->qtd_usuarios();

		$qtd_max_empresas = $this->p->valor_max_empresas();
		$qtd_max_usuarios = $this->p->valor_max_usuarios();
		$nome_do_plano = $this->p->nome_do_plano();

		$dado1 = ($qtd_max_empresas->valor - $qtd_empresas->valor);
		$dado2 = $qtd_empresas->valor;

		$dado3 = ($qtd_max_usuarios->valor - $qtd_usuarios->valor);
		$dado4 = $qtd_usuarios->valor;
		$nomeplano = $nome_do_plano->valor;

		// INICIO DO RELATÓRIO
		$this->load->library("pdf");

		require('application/libraries/diag.php');


		$pdf = new PDF_Diag();
		$pdf->AddPage();
		$pdf->Image('assets/img/logos/logo.png', 80, 5, 55, "C");
		$pdf->Ln(15);

		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO DE CONSUMO DO PLANO',0,1,"C");
		$pdf->Cell(0, 5, $nomeplano, 0, 1);
		$pdf->Ln(2);
		$data = array('Disponível' => $dado1, 'Usado' => $dado2);

		//GRÁFICO 1
		$pdf->SetFont('Arial', 'BIU', 12);
		$pdf->Cell(0, 5, 'Empresas:', 0, 1);
		$pdf->Ln(2);

		$pdf->SetFont('Arial', '', 10);
		$valX = $pdf->GetX();
		$valY = $pdf->GetY();
		$pdf->Cell(30, 5, 'Empresas que podem ser cadastradas:');
		$pdf->Cell(42, 5, $qtd_max_empresas->valor, 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(30, 5, 'Empresas cadastradas atualmente:');
		$pdf->Cell(42, 5, $qtd_empresas->valor, 0, 0, 'R');
		$pdf->Ln();
		$pdf->Ln(8);

		$pdf->SetXY(90, $valY);
		$col1=array(100,100,255);
		$col2=array(255,100,100);
		$col3=array(255,255,100);
		$pdf->PieChart(100, 35, $data, '%l (%p)', array($col1,$col2));
		$pdf->SetXY($valX, $valY + 40);
		$pdf->Ln(12);

		//GRÁFICO 2
		$data2 = array('Disponível' => $dado3, 'Usado' => $dado4);

		$pdf->SetFont('Arial', 'BIU', 12);
		$pdf->Cell(0, 5, 'Usuários:', 0, 1);
		$pdf->Ln(2);
		$pdf->SetFont('Arial', '', 10);
		$valX = $pdf->GetX();
		$valY = $pdf->GetY();
		$pdf->Cell(30, 5, 'Usuários que podem ser cadastrados:');
		$pdf->Cell(42, 5, $qtd_max_usuarios->valor, 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(30, 5, 'Usuários cadastrados atualmente:');
		$pdf->Cell(42, 5, $qtd_usuarios->valor, 0, 0, 'R');
		$pdf->Ln();
		$pdf->Ln(8);

		$pdf->SetXY(90, $valY);
		$col1=array(100,100,255);
		$col2=array(255,100,100);
		$col3=array(255,255,100);
		$pdf->PieChart(100, 35, $data2, '%l (%p)', array($col1,$col2));
		$pdf->SetXY($valX, $valY + 40);

		$pdf->Output('Relatorio de Consumo do Plano - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}

	public function paginarelatorios(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('usuario/pagina_relatorio', $dados);
		$this->load->view('layout/footer');
	}

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['consulta'] = $this->p->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('plano_contratado/plano_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function editar_plano(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);

		if(isset($_POST['btn_salvar'])){

			$nome_do_plano = $this->input->post("nome_do_plano");
			$qtd_usuarios = $this->input->post("qtd_usuarios");
			$qtd_empresas = $this->input->post("qtd_empresas");

			$result = $this->p->editar_plano($nome_do_plano,$qtd_empresas,$qtd_usuarios);

			if ($result){
		
				$this->session->set_flashdata('msg_alerta', 3);

				redirect('meuplano/listar');

			} else {
				
				$this->session->set_flashdata('msg_alerta', 4);

				redirect('meuplano/listar');

			}

		}

		$dados['plano_contratado'] = $this->p->pesquisar_plano_id();

		$dados['id'] = $id;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('plano_contratado/plano_editar', $dados);
		$this->load->view('layout/footer');
	}
}
