<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resumofiscal extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('empresausuario_model');
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function findPergunta(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');
		$pergunta = $this->input->post('pergunta');

		$consulta = $this->resumofiscal_model->findPergunta($inscricaoEstadual,$pergunta); 

		if(strpos($consulta->pergunta, 'Sim')){
			echo $this->convertString($consulta->pergunta, $pergunta);
		}else{
			echo $this->convertStringDivIcms($consulta->pergunta);	
		}
		
		
		//$dados['consulta'] = $consulta;
	} 

	public function convertString($string, $pergunta){
		//$string = "    Sim 		ICMS InformadoICMS RecolhidoMêsAno 			8.812,960,0072017 			2.498,050,0082017  ";
		$explode = "";
		if($pergunta == 7){
			$explode = " 			";
		}else if($pergunta == 8){
			$explode = " 			";
		}else{
			$explode = " 			";
		}

		//$array = explode(" 			",$string);
		$array = explode($explode,$string);

		$tabela = "<td>
	    <div>
			<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
				<tbody>
					<tr>
						<th scope='col'>ICMS Informado</th><th scope='col'>ICMS Recolhido</th><th scope='col'>Mês</th><th scope='col'>Ano</th>
					</tr>";

		$cont = 0;
		$anoAux = "";
		foreach ($array as $value) {
			$tabela = $tabela."<tr>";

			$valorInformado = "";
			$valorRecebido = "";
			$data = "";
			if($cont != 0){
				$linha = explode(",", $value);
				$i = 0;
				foreach($linha as $coluna){
					if($i == 0){
						$valorInformado = $coluna;	
					}elseif ($i == 1) {
						$valorRecebido = $coluna;
					}elseif ($i == 2) {
						$data = $coluna;
					}	
					$i++;
				}

				//$valorInformado = $valorInformado.",".substr($valorRecebido, 0, 2);
				//$valorRecebido = substr($valorRecebido, 2).",".substr($data,0, 2);
				//$mes = substr($data, 2, 1);
				//$ano = substr($data, 3, 4);
				$valorInformado = $valorInformado.",".substr($valorRecebido, 0, 2);
				if($data == ""){
					$data = substr($valorRecebido, 3);
					$valorRecebido = "0";
					if(strlen(trim($data)) <= 5){
						$ano = substr($data, 1, 4);
						$mes = substr($data, 0, 1);
					}else{
						$ano = substr($data, 2, 4);
						$mes = substr($data, 0, 2);
					}
					
					if($ano >= 2000 && $ano <= 2999){
						$anoAux = $ano; 
					}else{
						$ano = $anoAux;
					}
					//$mes = substr($data, -4, 1);
				}else{
					$valorRecebido = substr($valorRecebido, 2).",".substr($data,0, 2);	
					if(strlen(trim($data)) == 7){
						$mes = substr($data, 2, 1);	
						$ano = substr($data, 3, 4);
					}else{
						$mes = substr($data, 2, 2);
						$ano = substr($data, 4, 4);
					}
					
					//$ano = substr($data, -4);
				}

				$tabela = $tabela."<td>".$valorInformado."</td>"."<td>".$valorRecebido."</td>"."<td>".$mes."</td>"."<td>".$ano."</td>";	
			}
			$cont++;
			$tabela = $tabela."</tr>";
		}

		$tabela = $tabela."</tbody></table></div></td>";
		return ''.$tabela.'';
	}

	public function consultageral(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		
		$dados['filtro'] = $this->uri->segment(2);

		$tour = $this->uri->segment(3);
		if($tour != ""){
			$dados['fromTour'] = "true";	
		}
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->listar();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;

		$dados['razao_social_filtro'] = $razao;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function socioirregular(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->socioirregular();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function omissodma(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->omissodma();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function omissoefd(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->omissoefd();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function parcelamentoatraso(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->parcelamentoatraso();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;

		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function divergencia1(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->divergencia1();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function divergencia2(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->divergencia2();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function divergencia3(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->divergencia3();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function os1(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->os1();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function os2(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->os2();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}

	public function pafativo(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->pafativo();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}


	public function beneficiarias() {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',20,15,40);
		$pdf->Ln(15);
			
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,"Ben. do Decreto 7.799/00",0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
	
		$result = $this->resumofiscal_model->consultaBeneficiarias();
	
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
	
		$pdf->SetFont("Courier","B",9);
	
		$pdf->Cell(100,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(40,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(40,8,"IE","TBLR",0,"C");
	
		$pdf->Ln();
	
		$pdf->SetFont("Courier","",7);
	
		foreach ($result as $r) {
			
			if($r->cnaeFiscal == " 463100 - Comércio atacadista de leite e laticínios" 
                || $r->cnaeFiscal == " 4632002 - Comércio atacadista de farinhas, amidos e féculas"
                || $r->cnaeFiscal == " 4633801 - Comércio atacadista de frutas, verduras, raízes, tubérculos, hortaliças e legumes frescos"
                || $r->cnaeFiscal == " 4633802 - Comércio atacadista de aves vivas e ovos"
                || $r->cnaeFiscal == " 4634601 - Comércio atacadista de carnes bovinas e suínas e derivados"
                || $r->cnaeFiscal == " 4634603 - Comércio atacadista de pescados e frutos do mar"
                || $r->cnaeFiscal == " 4635499 - Comércio atacadista de bebidas não especificadas anteriormente"
                || $r->cnaeFiscal == " 4637105 - Comércio atacadista de massas alimentícias"
                || $r->cnaeFiscal == " 4639701 - Comércio atacadista de produtos alimentícios em geral"
                || $r->cnaeFiscal == " 4623109 - Comércio atacadista de alimentos para animais"
                || $r->cnaeFiscal == " 4641901 - Comércio atacadista de tecidos"
                || $r->cnaeFiscal == " 4642701 - Comércio atacadista de artigos do vestuário e acessórios, exceto profissionais e de segurança"
                || $r->cnaeFiscal == " 4649401 - Comércio atacadista de equipamentos elétricos de uso pessoal e doméstico"
                || $r->cnaeFiscal == " 4649402 - Comércio atacadista de aparelhos eletrônicos de uso pessoal e doméstico"
                || $r->cnaeFiscal == " 4646001 - Comércio atacadista de cosméticos e produtos de perfumaria"
                || $r->cnaeFiscal == " 4646002 - Comércio atacadista de produtos de higiene pessoal"
                || $r->cnaeFiscal == " 4649408 - Comércio atacadista de produtos de higiene, limpeza e conservação domiciliar"
                || $r->cnaeFiscal == " 4649409 - Comércio atacadista de produtos de higiene, limpeza e conservação domiciliar, com atividadede fracionamento e acondicionamento associada"
                || $r->cnaeFiscal == " 4649499 - Comércio atacadista de outros equipamentos e artigos de uso pessoal e doméstico não especificados anteriormente"
                || $r->cnaeFiscal == " 4647801 - Comércio atacadista de artigos de escritório e de papelaria"
                || $r->cnaeFiscal == " 4649404 - Comércio atacadista de móveis e artigos de colchoaria"
                || $r->cnaeFiscal == " 4672900 - Comércio atacadista de ferragens e ferramentas"
                || $r->cnaeFiscal == " 4673700 - Comércio atacadista de material elétrico"
                || $r->cnaeFiscal == " 4679699 - Comércio atacadista de materiais de construção em geral"
                || $r->cnaeFiscal == " 4686902 - Comércio atacadista de embalagens"
                || $r->cnaeFiscal == " 4651601 - Comércio atacadista de equipamentos de informática"
                || $r->cnaeFiscal == " 4652400 - Comércio atacadista de componentes eletrônicos e equipamentos de telefonia e comunicação"
                || $r->cnaeFiscal == " 4693100 - Comércio atacadista de mercadorias em geral, sem predominância de alimentos ou de insumos agropecuários"){

				if(strpos($r->pergunta25, 'Sim')){
                    $pdf->Cell(100,8,$r->razaoSocial,"TBL",0,"L");
					$pdf->Cell(40,8,$r->cnpjcpf,"TBLR",0,"C");
					$pdf->Cell(40,8,$r->inscricaoEstadual,"TBR",0,"C");
			
					$pdf->Ln();
                    //echo "['NÃO', $l8->quantidade],";  
                  }

			}
			
	
		}
	
		$pdf->Output('Relatorio Ben. Decreto 7.799/00 - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}

	public function naobeneficiarias() {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',20,15,40);
		$pdf->Ln(15);
			
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,"Não Ben. do Decreto 7.799/00",0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
	
		$result = $this->resumofiscal_model->consultaBeneficiarias();
	
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
	
		$pdf->SetFont("Courier","B",9);
	
		$pdf->Cell(100,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(40,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(40,8,"IE","TBLR",0,"C");
	
		$pdf->Ln();
	
		$pdf->SetFont("Courier","",7);
	
		foreach ($result as $r) {
			
			if($r->cnaeFiscal == " 463100 - Comércio atacadista de leite e laticínios" 
                || $r->cnaeFiscal == " 4632002 - Comércio atacadista de farinhas, amidos e féculas"
                || $r->cnaeFiscal == " 4633801 - Comércio atacadista de frutas, verduras, raízes, tubérculos, hortaliças e legumes frescos"
                || $r->cnaeFiscal == " 4633802 - Comércio atacadista de aves vivas e ovos"
                || $r->cnaeFiscal == " 4634601 - Comércio atacadista de carnes bovinas e suínas e derivados"
                || $r->cnaeFiscal == " 4634603 - Comércio atacadista de pescados e frutos do mar"
                || $r->cnaeFiscal == " 4635499 - Comércio atacadista de bebidas não especificadas anteriormente"
                || $r->cnaeFiscal == " 4637105 - Comércio atacadista de massas alimentícias"
                || $r->cnaeFiscal == " 4639701 - Comércio atacadista de produtos alimentícios em geral"
                || $r->cnaeFiscal == " 4623109 - Comércio atacadista de alimentos para animais"
                || $r->cnaeFiscal == " 4641901 - Comércio atacadista de tecidos"
                || $r->cnaeFiscal == " 4642701 - Comércio atacadista de artigos do vestuário e acessórios, exceto profissionais e de segurança"
                || $r->cnaeFiscal == " 4649401 - Comércio atacadista de equipamentos elétricos de uso pessoal e doméstico"
                || $r->cnaeFiscal == " 4649402 - Comércio atacadista de aparelhos eletrônicos de uso pessoal e doméstico"
                || $r->cnaeFiscal == " 4646001 - Comércio atacadista de cosméticos e produtos de perfumaria"
                || $r->cnaeFiscal == " 4646002 - Comércio atacadista de produtos de higiene pessoal"
                || $r->cnaeFiscal == " 4649408 - Comércio atacadista de produtos de higiene, limpeza e conservação domiciliar"
                || $r->cnaeFiscal == " 4649409 - Comércio atacadista de produtos de higiene, limpeza e conservação domiciliar, com atividadede fracionamento e acondicionamento associada"
                || $r->cnaeFiscal == " 4649499 - Comércio atacadista de outros equipamentos e artigos de uso pessoal e doméstico não especificados anteriormente"
                || $r->cnaeFiscal == " 4647801 - Comércio atacadista de artigos de escritório e de papelaria"
                || $r->cnaeFiscal == " 4649404 - Comércio atacadista de móveis e artigos de colchoaria"
                || $r->cnaeFiscal == " 4672900 - Comércio atacadista de ferragens e ferramentas"
                || $r->cnaeFiscal == " 4673700 - Comércio atacadista de material elétrico"
                || $r->cnaeFiscal == " 4679699 - Comércio atacadista de materiais de construção em geral"
                || $r->cnaeFiscal == " 4686902 - Comércio atacadista de embalagens"
                || $r->cnaeFiscal == " 4651601 - Comércio atacadista de equipamentos de informática"
                || $r->cnaeFiscal == " 4652400 - Comércio atacadista de componentes eletrônicos e equipamentos de telefonia e comunicação"
                || $r->cnaeFiscal == " 4693100 - Comércio atacadista de mercadorias em geral, sem predominância de alimentos ou de insumos agropecuários"){

				if(strpos($r->pergunta25, 'Não')){
                    $pdf->Cell(100,8,$r->razaoSocial,"TBL",0,"L");
					$pdf->Cell(40,8,$r->cnpjcpf,"TBLR",0,"C");
					$pdf->Cell(40,8,$r->inscricaoEstadual,"TBR",0,"C");
			
					$pdf->Ln();
                    //echo "['NÃO', $l8->quantidade],";  
                  }

			}
			
	
		}
	
		$pdf->Output('Relatorio Não Ben. Decreto 7.799/00 - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}
	
	public function findPaf(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');

		$consulta = $this->resumofiscal_model->findPaf($inscricaoEstadual); 

		echo $this->convertStringPaf($consulta->pergunta);
	
	} 


	public function convertStringPaf($string){
		if(!strpos($string, 'Sim')){
			$array = explode("\n",$string);

			$tabela = "<td>
		    <div>
				<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
					<tbody>
						<tr>
							<th scope='col'>Número do Processo Administrativo-Fiscal</th>
						</tr>";

			foreach ($array as $value) {
				if(strpos($value, 'Sim') == false){
					$url = "<a target='_blank' href='https://sistemas.sefaz.ba.gov.br/sistemas/sigat/Default.Aspx?Aplicacao=SIGAT&Modulo=Credito&Tela=PAFDetalhesAcessoExterno&IdentPaf=".$value."|A|0|1&Acao=VisualizarDetalhes'>".$value."</a>";
					
					$tabela = $tabela."<tr>";
					$tabela = $tabela."<td>".$url."</td>";
					$tabela = $tabela."</tr>";
				}
			}

			$tabela = $tabela."</tbody></table></div></td>";
			return ''.$tabela.'';
		}else{
			$array = explode(" 			", $string);
			$tabela = "<td>
		    <div>
				<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
					<tbody>
						<tr>
							<th scope='col'>Número do Processo Administrativo-Fiscal</th>
						</tr>";
			
			foreach ($array as $value) {
				if(strpos($value, 'Sim') == false){
					$url = "<a target='_blank' href='https://sistemas.sefaz.ba.gov.br/sistemas/sigat/Default.Aspx?Aplicacao=SIGAT&Modulo=Credito&Tela=PAFDetalhesAcessoExterno&IdentPaf=".$value."|A|0|1&Acao=VisualizarDetalhes'>".$value."</a>";
					
					$tabela = $tabela."<tr>";
					$tabela = $tabela."<td>".$url."</td>";
					$tabela = $tabela."</tr>";
				}
			}

			$tabela = $tabela."</tbody></table></div></td>";
			return ''.$tabela.'';
		}
		
	}

	public function findSocioIrregular(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');

		$consulta = $this->resumofiscal_model->findSocioIrregular($inscricaoEstadual); 

		echo $this->convertStringSocioIrregular($consulta->pergunta);
	
	}

    public function convertStringSocioIrregular($string){
        //$string = "    Sim 		Número do Processo Administrativo-Fiscal 			7000005876182 			7000105978130  ";
        //$explode = "PHP_EOL";
        $pos = strpos($string, 'Não');

        if ($pos === false) {
            $array = explode(" 		            			",$string);

            $tabela = "<td>
		    <div>
				<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
					<tbody>
						<tr>
							<th scope='col'>Sócio(s) Irregular(es)</th>
						</tr>";

            $array = explode(" 			", $array[1]);

            foreach ($array as $value) {
                if(strpos($value, 'Sim') == false){
                    $cpf = preg_replace('/[^0-9]/', '', $value);
                    $tabela = $tabela."<tr>";
                    $tabela = $tabela."<td><a href='javascript:openWindowWithPost({$cpf})'>{$value}</a></td>";
                    $tabela = $tabela."</tr>";
                }
            }

            $tabela = $tabela."</tbody></table></div></td>";
            return ''.$tabela.'';
        } else {
            return '';
        }
    }

	public function convertStringDivIcms($string){
		$arrayPrincipal = explode("\n",$string);

		$tabela = "<td>
	    <div>
			<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
				<tbody>
					<tr>
						<th scope='col'>ICMS Informado</th><th scope='col'>ICMS Recolhido</th><th scope='col'>Mês</th><th scope='col'>Ano</th>
					</tr>";

		foreach ($arrayPrincipal as $linha) {
			if($linha != ""){
				$tabela = $tabela."<tr>";

				$arraySecundario = explode("|", $linha);

				$valorInformado = $arraySecundario[0];
				$valorRecolhido = $arraySecundario[1];
				$mes = $arraySecundario[2];
				$ano = $arraySecundario[3];

				$tabela = $tabela."<td>".$valorInformado."</td>"."<td>".$valorRecolhido."</td>"."<td>".$mes."</td>"."<td>".$ano."</td>";		
			}
			
		}

		$tabela = $tabela."</tr>";

		$tabela = $tabela."</tbody></table></div></td>";
		return ''.$tabela.'';
	}

	public function msgnaolidas(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		
		$dados['filtro'] = $this->uri->segment(2);

		$tour = $this->uri->segment(3);
		if($tour != ""){
			$dados['fromTour'] = "true";	
		}
		
		//$this->empresa_model->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->resumofiscal_model->consultaMsgNaoLidas();

		$consulta_n = $this->resumofiscal_model->dashboard11();
		$somaMsgNaoLidas = 0;
		foreach ($consulta_n as $value) {
			$valor = trim($this->soNumero($value->qtd));
			$somaMsgNaoLidas = $somaMsgNaoLidas + $valor;
		}

		$dados['msg_nao_lidas'] = $somaMsgNaoLidas;
		$dados['qtd_registros'] = $this->resumofiscal_model->qtd_registros_msg_nao_lidas();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_msg', $dados);
		$this->load->view('layout/footer');
	
	}

	public function soNumero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	}


	public function findEFD(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');

		$consulta = $this->resumofiscal_model->findEFD($inscricaoEstadual); 

		echo $this->convertStringEFD($consulta->pergunta);
	
	} 

	public function convertStringEFD($string){
		$arrayPrincipal = explode(" 			",$string);

		$tabela = "<td>
	    <div>
			<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
				<tbody>
					<tr>
						<th scope='col'>Ano&nbsp;&nbsp;&nbsp;Mes(es)</th>
					</tr>";

		foreach ($arrayPrincipal as $linha) {
			if(strpos($linha, 'Sim') == false){
				$tabela = $tabela."<tr>";
				$tabela = $tabela."<td>".$linha."</td>";
				$tabela = $tabela."</tr>";
			}
		}

		$tabela = $tabela."</tr>";

		$tabela = $tabela."</tbody></table></div></td>";
		return ''.$tabela.'';
	}


	public function findDMA(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');

		$consulta = $this->resumofiscal_model->findDMA($inscricaoEstadual); 

		echo $this->convertStringDMA($consulta->pergunta);
	
	} 

	public function convertStringDMA($string){
		$arrayPrincipal = explode(" 			",$string);

		$tabela = "<td>
	    <div>
			<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
				<tbody>
					<tr>
						<th scope='col'>Ano&nbsp;&nbsp;&nbsp;Mes(es)</th>
					</tr>";

		foreach ($arrayPrincipal as $linha) {
			if(strpos($linha, 'Sim') == false){
				$tabela = $tabela."<tr>";
				$tabela = $tabela."<td>".$linha."</td>";
				$tabela = $tabela."</tr>";
			}
		}

		$tabela = $tabela."</tr>";

		$tabela = $tabela."</tbody></table></div></td>";
		return ''.$tabela.'';
	}

	public function findOsMonitoramento(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');

		$consulta = $this->resumofiscal_model->findOsMonitoramento($inscricaoEstadual); 

		echo $this->convertStringOsMonitoramento($consulta->pergunta);
	}

	public function convertStringOsMonitoramento($string){
		 
		$array = preg_replace("/[^0-9]/", "", $string); 

		$tabela = "<td>
	    <div>
			<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
				<tbody>
					<tr>
						<th scope='col'>O.S de Monitoramento</th>
					</tr>"; 
		
		if($array != ""){
			$tabela = $tabela."<tr>";
			$tabela = $tabela."<td>".$array."</td>";
			$tabela = $tabela."</tr>";
		}

		$tabela = $tabela."</tbody></table></div></td>";
		return ''.$tabela.'';
	}

	public function findOsAuditoria(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');

		$consulta = $this->resumofiscal_model->findOsAuditoria($inscricaoEstadual); 

		echo $this->convertStringOsAuditoria($consulta->pergunta);
	}

	public function convertStringOsAuditoria($string){
		//$string = "    Sim 		Ordem de Serviço 			50320718  ";
		//$explode = "PHP_EOL"; 

		$array = preg_replace("/[^0-9]/", "", $string); 

		$tabela = "<td>
	    <div>
			<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
				<tbody>
					<tr>
						<th scope='col'>O.S de Auditoria</th>
					</tr>";  

		if($array != ""){
			$tabela = $tabela."<tr>";
			$tabela = $tabela."<td>".$array."</td>";
			$tabela = $tabela."</tr>";
		}
		 

		$tabela = $tabela."</tbody></table></div></td>";
		return ''.$tabela.'';
	}

	public function findParcelamentoEmAtraso(){
		$inscricaoEstadual = $this->input->post('inscricaoEstadual');

		$consulta = $this->resumofiscal_model->findParcelamentoEmAtraso($inscricaoEstadual); 

		echo $this->convertStringParcelamentoEmAtraso($consulta->pergunta);
	}

	public function convertStringParcelamentoEmAtraso($string){
		//$string = "    Sim 		Ordem de Serviço 			50320718  ";
		//$explode = "PHP_EOL"; 
		if(!strpos($string, 'Sim')){
		$array = preg_replace("/[^0-9]/", "", $string); 

		$tabela = "<td>
	    <div>
			<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
				<tbody>
					<tr>
						<th scope='col'>Número do Parcelamento</th>
					</tr>";  

		if(is_array($array)){
			foreach ($array as $value) {
				if(strpos($value, 'Sim') == false){
					$tabela = $tabela."<tr>";
					$tabela = $tabela."<td>".$value."</td>";
					$tabela = $tabela."</tr>";
				}
			}
		}
		 

		$tabela = $tabela."</tbody></table></div></td>";
		return ''.$tabela.'';
	}else{
			$array = explode(" 			", $string);
			$tabela = "<td>
		    <div>
				<table class='table table-striped table-bordered table-hover dataTable' cellspacing='0' rules='all' border='1' style='border-collapse:collapse;'>
					<tbody>
						<tr>
							<th scope='col'>Número do Parcelamento</th>
						</tr>";
			
			if(is_array($array)){
				foreach ($array as $value) {
					if(strpos($value, 'Sim') == false){ 
						
						$tabela = $tabela."<tr>";
						$tabela = $tabela."<td>".$value."</td>";
						$tabela = $tabela."</tr>";
					}
				}
			}
			

			$tabela = $tabela."</tbody></table></div></td>";
			return ''.$tabela.'';
		}
	}

	public function simples_nacional(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		$colaboradores_vinculados = $this->empresausuario_model->findUsuarios();
		$myhashmap = array();
    	foreach ($colaboradores_vinculados as $item) {
    		$myhashmap[$item->id_empresa] = $item->colaboradores;
    	}

		$empresas = $this->resumofiscal_model->simples_nacional();
		foreach ($empresas as $item) {
			if(isset($myhashmap[$item->id])){
				if(!empty($myhashmap[$item->id])){
					$item->colaboradores = str_replace(",", "<br>", $myhashmap[$item->id]);
				}else{
					$item->colaboradores = "Nenhum colaborador responsável por esta empresa";
				}
			}
		}

		$dados['consulta'] = $empresas;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	
	}
	
}
