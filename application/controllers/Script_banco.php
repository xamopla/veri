<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Script_banco extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function script_database(){
        $dbh = new PDO('mysql:host=localhost;', 'root', '12*bDV3r1sA0pAuL0*21', array(
            PDO::ATTR_PERSISTENT => true
        ));


        $sql = $dbh->query("SELECT DISTINCT SCHEMA_NAME AS `database`
                            FROM information_schema.SCHEMATA
                            WHERE  SCHEMA_NAME NOT IN ('information_schema', 'performance_schema', 'mysql', 'phpmyadmin')
                            ORDER BY SCHEMA_NAME");
        $getAllDbs = $sql->fetchALL(PDO::FETCH_ASSOC);

        foreach ($getAllDbs as $DB) {  

            try {
                $sql = $this->proxima_versao($DB['database']);
                $dbh->query($sql);
            } catch (Exception $e) {
                echo $e;
            }
            
        };
    }

    public function proxima_versao($banco){

        $sql = "USE `".$banco."`; ";
        $sql2 = "CREATE TABLE `dtb_certidao_alagoas` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
ALTER TABLE `dtb_certidao_alagoas`
  ADD PRIMARY KEY (`cnpj`);
COMMIT;

CREATE TABLE `dtb_certidao_amapa` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
ALTER TABLE `dtb_certidao_amapa`
  ADD PRIMARY KEY (`cnpj`);
COMMIT;";

        $sql3 = $sql.$sql2;
        return $sql3;
    }
}


