<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Certificado_contador extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
        $this->load->model('contador_model');
        $this->load->model('resumofiscal_model');
        $this->load->model('certificadocontador_model'); 
        $this->load->model('email_clientes_model'); 
        $this->load->model('gerenciar_clientes_model');
        $this->load->model('usuario_model');
    }

	public function cadastrar(){

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        $id = $this->uri->segment(3);

        $this->contador_model->setId($id);
        $dados['id'] = $id;
        $dados['contador'] = $this->contador_model->pesquisar_contador_id();
        $dados["certificado"] = $this->certificadocontador_model->get_status_certificado($id);
        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);        
        $this->load->view('contadores/contador_cadastrar_certificado_digital', $dados);
        $this->load->view('layout/footer');
	}

    public function upload(){

        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA
        
        $targetPath = 'crons-api/assets/contador/certificados/'.$server.'/';

        if (!file_exists($targetPath)) {
            mkdir($targetPath, DIR_WRITE_MODE, true);
        }
        /*
         *  o id 2 é sempre o usuario do contador onde está o cnpj da contabilidade
         */
        $this->usuario_model->setId(2);
        $usuario_contabilidade = $this->usuario_model->pesquisar_usuario_id();
        $dados_post = array(
            'senha'=>$this->input->post('certificado_senha'),
            'cnpj'=> preg_replace("/[^0-9]/", "", $usuario_contabilidade->cnpj),
            'dir_upload_cert'=>$targetPath, /*<--- Dar permissão para essa pasta, pois, ela será o caminho do upload do arquivo */
            'id_contador' => $this->input->post('id_contador'),
        );

        echo ($this->certificadocontador_model->configurar($dados_post, $_FILES['certificado_file']));        
    }
}


