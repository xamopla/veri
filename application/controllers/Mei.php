<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mei extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('mei_model');
       	$this->load->model('empresa_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";
		$titulo = "Empresas do MEI";

		if($cnpj != "EXCLUIDAS" && $cnpj != "OPTANTES" && $cnpj != "TODAS"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "TODAS";
		}else{
			if($cnpj == "EXCLUIDAS"){
				$titulo = "Empresas excluídas do MEI";
			}else if($cnpj == "OPTANTES"){
				$titulo = "Empresas optantes do MEI";
			}else{
				$cnpj = "TODAS";
			}
			$filtro = $cnpj;
		}

		$this->mei_model->setFiltro($filtro);

		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		$dados['titulo'] = $titulo;

		$resultado = $this->mei_model->listar_excluidas_simei();

		$dados['consulta'] = $resultado;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('simples/exclusao_mei', $dados);
		$this->load->view('layout/footer');
		
	}


}