<?php

use Google\Auth\Cache\Item;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Parcelamento_pert_rfb extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('parcelamento_pert_rfb_model');
        $this->load->model('empresa_model');
    }

    public function listar()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $mes_atual = date('m');
        $ano_atual = date('Y');

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ---------------- 

        $cnpj = $this->uri->segment(3);
        $razao = "";
        $filtro = "";

        if ($cnpj != "ATIVO" && $cnpj != "EXCLUIDO" && $cnpj != "TODAS" && $cnpj != "CANCELADO") {
            if ($cnpj != null) {
                $em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
                $razao = $em->razao_social;
            }
            $filtro = "TODAS";
        } else {
            $filtro = $cnpj;
        }

        $this->parcelamento_pert_rfb_model->setFiltro("TODAS");

        $dados['razao_social_filtro'] = $razao;
        $dados['filtro'] = $filtro;

        $dados['parcelamentos'] = $this->parcelamento_pert_rfb_model->buscar_parcelamentos();

        foreach ($dados['parcelamentos'] as $parcelamento) {
            //buscar parcela atual
            $parcelas = $this->parcelamento_pert_rfb_model->buscar_parcelas_a_vencer($parcelamento->cnpj, $parcelamento->id);
            $parcelamento->qtd_parcelas_a_pagar = count($parcelas);
            foreach ($parcelas as $parcela) {
                if (substr($parcela->data_vencimento, 3) == $mes_atual . '/' . $ano_atual)
                    $parcelamento->parcela_atual = $parcela;
            }
            $parcelamento->parcelas_atrasadas = $this->parcelamento_pert_rfb_model->buscar_parcelas_em_atraso($parcelamento->cnpj, $parcelamento->id);
        }

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('parcelamento/parcelamento_pert_rfb', $dados);
        $this->load->view('layout/footer');
    }

    public function listar_detalhes()
    {
        $id = $this->uri->segment(3);

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ---------------- 
        $dados['parcelamento'] = $this->parcelamento_pert_rfb_model->buscar_parcelamentos_individual($id);

        $dados['parcelas'] = $this->parcelamento_pert_rfb_model->buscar_parcelas($dados['parcelamento']->cnpj, $id);

        foreach ($dados['parcelas'] as $parcela) {
            $parcela->valor_originario = number_format($parcela->valor_originario, 2, ',', '.');
            $parcela->saldo_atualizado = number_format($parcela->saldo_atualizado, 2, ',', '.');
        }

        $dados['pagamentos'] = $this->parcelamento_pert_rfb_model->buscar_pagamentos($dados['parcelamento']->cnpj, $id);

        foreach ($dados['pagamentos'] as $pagamento) {
            $pagamento->valor_arrecadado = number_format($pagamento->valor_arrecadado, 2, ',', '.');
            $pagamento->principal_utilizado = number_format($pagamento->principal_utilizado, 2, ',', '.');
            $pagamento->juros_utilizado = number_format($pagamento->juros_utilizado, 2, ',', '.');
        }

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('parcelamento/parcelamento_pert_rfb_detalhes', $dados);
        $this->load->view('layout/footer');
    }

    //Funções para modal
    public function parcelas_atrasadas_for_modal()
    {
        $id_parcelamento = $this->input->post('id_parcelamento');
        $cnpj = $this->input->post('cnpj');

        $parcelas_atrasadas = $this->parcelamento_pert_rfb_model->buscar_parcelas_em_atraso($cnpj, $id_parcelamento);

        foreach ($parcelas_atrasadas as $parcela) {
            $parcela->valor_originario = number_format($parcela->valor_originario, 2, ',', '.');
            $parcela->saldo_atualizado = number_format($parcela->saldo_atualizado, 2, ',', '.');
        }
        
        echo json_encode($parcelas_atrasadas);
    }
}