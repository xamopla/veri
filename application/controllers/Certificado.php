<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Certificado extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('certificadocontador_model'); 
        $this->load->model('certificado_model');
    }

	public function listar_certificados_todos(){
        date_default_timezone_set('America/Sao_Paulo');
        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------
        $dados['filtro'] = $this->uri->segment(2);

        $dados['consulta'] = $this->certificado_model->listar_certificados_todos();
        
        $dados['nome_tela'] = "Certificados";

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');        
        $this->load->view('layout/header', $dados);
        $this->load->view('certificado/certificado_listar', $dados);        
        $this->load->view('layout/footer');
    }

    public function listar_certificados_vencidos(){
        date_default_timezone_set('America/Sao_Paulo');
        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------
        $dados['filtro'] = $this->uri->segment(2);

        $consulta = $this->certificado_model->listar_certificados_todos();
        $consulta_vencidos = array();

        foreach ($consulta as $item) {
            if(date('Ymd', $item->data_validade) < date('Ymd')){
                array_push($consulta_vencidos, $item);
            }
        }
        $dados['consulta'] = $consulta_vencidos;
        $dados['nome_tela'] = "Certificados Vencidos";
        
        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');        
        $this->load->view('layout/header', $dados);
        $this->load->view('certificado/certificado_listar', $dados);        
        $this->load->view('layout/footer');
    }

    public function listar_certificados_regulares(){
        date_default_timezone_set('America/Sao_Paulo');
        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------
        $dados['filtro'] = $this->uri->segment(2);
        
        $consulta = $this->certificado_model->listar_certificados_todos();
        $consulta_vencidos = array();

        foreach ($consulta as $item) {
            $data_vencimento = date('Ymd', $item->data_validade);

            $diasNotificacao = 30;
            $df = new DateTime($data_vencimento);
            $da = new DateTime(date(''));

            $intervalo = $df->diff($da);
            if(date('Ymd', $item->data_validade) > date('Ymd')){
                array_push($consulta_vencidos, $item);
            }
            // if($intervalo->format('%a') >= 0){
            //     array_push($consulta_vencidos, $item);
            // }

        }
        $dados['consulta'] = $consulta_vencidos;
        $dados['nome_tela'] = "Certificados Vencidos";
        
        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');        
        $this->load->view('layout/header', $dados);
        $this->load->view('certificado/certificado_listar', $dados);        
        $this->load->view('layout/footer');
    }

    public function listar_certificados_proximos(){
        date_default_timezone_set('America/Sao_Paulo');
        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------
        $dados['filtro'] = $this->uri->segment(2);
        
        $consulta = $this->certificado_model->listar_certificados_todos();
        $consulta_vencidos = array();

        foreach ($consulta as $item) {
            $data_vencimento = date('Ymd', $item->data_validade);

            $diasNotificacao = 30;
            $df = new DateTime($data_vencimento);
            $da = new DateTime(date(''));

            $intervalo = $df->diff($da);
            if($intervalo->format('%a') <= $diasNotificacao && date('Ymd', $item->data_validade) > date('Ymd')){
                array_push($consulta_vencidos, $item);
            }

        }
        $dados['consulta'] = $consulta_vencidos;
        $dados['nome_tela'] = "Certificados Vencidos";
        
        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');        
        $this->load->view('layout/header', $dados);
        $this->load->view('certificado/certificado_listar', $dados);        
        $this->load->view('layout/footer');
    }



    public function listar_procuracao(){

    }
    
}


