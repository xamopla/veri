<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dec extends MY_Controller {
  
  public function __construct() {
        parent::__construct();
        $this->load->model('dec_model');
    $this->load->model('empresa_model');
    }

    public function listar(){
      $id_mensagem = $this->uri->segment(3);
    $razao = "";
    $filtro = "";

    if($id_mensagem != "LIDAS" && $id_mensagem != "NAO_LIDAS" && $id_mensagem != "COMUNICADOS" && $id_mensagem != "TODAS" && $id_mensagem != "MENSAGENS"){
      if($id_mensagem != null){
                $em = $this->empresa_model->find_empresa_by_cnpj($id_mensagem);
                $razao = $em->razao_social;
            }
            $filtro = "TODAS";
    }else{
      $filtro = $id_mensagem;
    }
    
    $this->dec_model->setFiltro($filtro);

    $dados['razao_social_filtro'] = $razao;
    $dados['filtro'] = $filtro;
    
    $dados['consulta'] = $this->dec_model->listar();

    $hostCompleto = $_SERVER['HTTP_HOST'];
      $server = explode('.', $hostCompleto);
      $server = $server[0];

      $dados['banco'] = $server;
      
    $this->load->view('layout/head');
    $this->load->view('layout/sidebar');
    $this->load->view('layout/header', $dados);
    $this->load->view('dec/dec_listar', $dados);
    $this->load->view('layout/footer');
    }

    public function listar_dec_recebidas(){

      $id_mensagem = $this->uri->segment(3);
      $razao = "";
      $filtro = "";

      if($id_mensagem != "LIDAS" && $id_mensagem != "NAO_LIDAS" && $id_mensagem != "TODAS"){
        if($id_mensagem != null){
                  $em = $this->empresa_model->find_empresa_by_cnpj($id_mensagem);
                  $razao = $em->razao_social;
              }
              $filtro = "TODAS";
      }else{
        $filtro = $id_mensagem;
      }

      $this->dec_model->setFiltro($filtro);
      $dados['razao_social_filtro'] = $razao;
      $dados['filtro'] = $filtro;

      $dados['consulta'] = $this->dec_model->listar_mensagens_recebidas();

      $hostCompleto = $_SERVER['HTTP_HOST'];
      $server = explode('.', $hostCompleto);
      $server = $server[0];

      $dados['banco'] = $server;
      
      $this->load->view('layout/head');
      $this->load->view('layout/sidebar');
      $this->load->view('layout/header', $dados);
      $this->load->view('dec/dec_listar_qtd_mensagens', $dados);
      $this->load->view('layout/footer');
    } 


    public function buscar_mensagens_dec_modal(){
      $filtro = "TODAS";
      $cnpj = $this->input->post("cnpj");

      $this->dec_model->setFiltro($filtro);

      $dados['consulta'] = $this->dec_model->listar_modal($cnpj);

      $hostCompleto = $_SERVER['HTTP_HOST'];
      $server = explode('.', $hostCompleto);
      $server = $server[0];

      $dados['banco'] = $server;

      $dados['cnpj'] = $cnpj;

      echo json_encode($dados);
    }

}
