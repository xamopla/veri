<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Importador extends MY_Controller {

	public function listar(){	
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header');
		$this->load->view('importador/importador_listar');
		$this->load->view('layout/footer');
		
	}

	public function cadastrar(){
		$this->load->model('empresa_model', 'e');
		$log = "";
		$array = explode('.', $_FILES['file']['name']);
		$extension = end($array);
		//$extension = end(explode(".", $_FILES["file"]["name"])); // For getting Extension of selected file
		$allowed_extension = array("xls", "xlsx", "csv"); //allowed extension
		 if(!empty($_FILES['file']['name']) && in_array($extension,$allowed_extension)){
			$file = $_FILES["file"]["tmp_name"]; // getting temporary source of excel file
			include("PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code
  			$objPHPExcel = PHPExcel_IOFactory::load($file);

			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet){
				$highestRow = $worksheet->getHighestRow();
				for($row=2; $row<=$highestRow; $row++){
					$ie = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$senhaSefaz = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$loginSefaz = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$loginMei = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$senhaMei = $worksheet->getCellByColumnAndRow(4, $row)->getValue();


					$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
            		$ie = preg_replace("/[^0-9]/", "", $ie);
	            	$this->e->setInscricaoEstadual($ie);
	            	$verificacao = $this->e->verificar_cadastro_duplicado_for_import($ie);

	            	if($verificacao){
	            		$this->e->setId($verificacao->id);
	            		$this->e->setSenhaSefaz($senhaSefaz);
	            		$this->e->setLoginSefaz($loginSefaz);
	            		$this->e->setLoginMei($loginMei);
	            		$this->e->setSenhaMei($senhaMei);

	            		$this->e->editarForImport();
	            	}else{
	            		$retorno = $this->e->pesquisar_sefaz_for_import();
	            		if($retorno == "sucesso"){
	            			$this->e->setSenhaSefaz($senhaSefaz);
		            		$this->e->setLoginSefaz($loginSefaz);
		            		$this->e->setLoginMei($loginMei);
		            		$this->e->setSenhaMei($senhaMei);
	            			$this->e->cadastrar();
	            		}else{
	            			$log = $log."<br> IE - ".$ie." não importado devido a erro!";
	            			$qstring["status"] = $log;
	            		}
	            	}

				}
			} 

		 	if($log == ""){
            	$qstring["status"] = 'Dados importados com sucesso!';	
            }
		}else{
		  $qstring["status"] = 'Arquivo inválido';
	 	}

	 	$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
	    $this->load->view('layout/header');
		$this->load->view('importador/importador_listar',$qstring);
		$this->load->view('layout/footer');
	}

	function baixar(){
		header("Content-Type: application/octet-stream");

		$file = $_GET["file"] .".xlsm";
		header("Content-Disposition: attachment; filename=" . urlencode($file));   
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Description: File Transfer");            
		header("Content-Length: " . filesize($file));
		flush(); // this doesn't really matter.
		$fp = fopen($file, "r");
		while (!feof($fp))
		{
		    echo fread($fp, 65536);
		    flush(); // this is essential for large downloads
		} 
		fclose($fp); 
	}

	public function novo_importador(){

		$this->load->model('empresa_model', 'e'); 

		$log = "";
		$array = explode('.', $_FILES['file']['name']);
		$extension = end($array);
		//$extension = end(explode(".", $_FILES["file"]["name"])); // For getting Extension of selected file
		$allowed_extension = array("xls", "xlsx", "csv"); //allowed extension
		 if(!empty($_FILES['file']['name']) && in_array($extension,$allowed_extension)){
			$file = $_FILES["file"]["tmp_name"]; // getting temporary source of excel file
			include("PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code
  			$objPHPExcel = PHPExcel_IOFactory::load($file);

			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet){
				$highestRow = $worksheet->getHighestRow();
				
				for($row=2; $row<=$highestRow; $row++){

					$ie = "";
					$senhaSefaz = "";
					$cnpj = "";

					$cnpj = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$ie = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$senhaSefaz = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					
					$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

					// VERIFICA AUTOMATICAMENTE SE A EMPRESA É DE COMERCIO OU SERVIÇO
					if (empty($ie)) {

					   	$cnpj = preg_replace("/[^0-9]/", "", $cnpj);

					   	// COMPARA SE CNPJ DIGITADO NA PLANILHA ESTÁ COMPLETO
					   	if(strlen($cnpj) == 13){
  							$cnpj = '0'.$cnpj;
					   	} 
					   	else if (strlen($cnpj) == 12) {

					   		$cnpj = '00'.$cnpj;

					   	} else {

					   		$cnpj = $cnpj;
					   	}

		            	$this->e->setCnpj($cnpj);
		            	$verificacao = $this->e->verificar_cadastro_duplicado_for_import_cnpj($cnpj);

		            	if($verificacao){

	            			$this->e->setId($verificacao->id);  

		            	} else {
		            		$retorno = $this->e->pesquisar_cnpj_receita_importador($cnpj);
		            		if($retorno == "sucesso"){ 
		            			$this->e->cadastrar();
		            		}else{
		            			$log = $log."<br> CNPJ - ".$cnpj." não importado devido a erro!";
		            			$qstring["status"] = $log;
		            		}
		            	}

					} else {

	            		$ie = preg_replace("/[^0-9]/", "", $ie);
		            	$this->e->setInscricaoEstadual($ie);
		            	$verificacao = $this->e->verificar_cadastro_duplicado_for_import($ie);
		            	$login_sefaz = $ie.'00';

		            	if($verificacao){
		            		$this->e->setId($verificacao->id);
		            		$this->e->setLoginSefaz($login_sefaz);
		            		$this->e->setSenhaSefaz($senhaSefaz); 
	            			$this->e->setFlagEmpresaSemIe(NULL); 
	            			$this->e->setSync(1);
		            		$this->e->setVinculoContador(1);

		            		$this->e->editarForImport();
		            	}else{
		            		$retorno = $this->e->pesquisar_sefaz_for_import();
		            		if($retorno == "sucesso"){
			            		$this->e->setLoginSefaz($login_sefaz);
		            			$this->e->setSenhaSefaz($senhaSefaz); 
		            			$this->e->setFlagEmpresaSemIe(NULL); 
		            			$this->e->setSync(1);
		            			$this->e->setVinculoContador(1);

		            			$this->e->cadastrar();
		            		}else{
		            			$log = $log."<br> IE - ".$ie." não importado devido a erro!";
		            			$qstring["status"] = $log;
		            		}
		            	}
	            	 }

				}
			} 

		 	if($log == ""){
            	$qstring["status"] = 'Dados importados com sucesso!';	
            }
		}else{
		  $qstring["status"] = 'Arquivo inválido';
	 	}

	    $this->load->view('layout/head');
		$this->load->view('layout/sidebar');			
		$this->load->view('layout/header');
		$this->load->view('importador/importador_listar',$qstring);
		$this->load->view('layout/footer');
	}
}