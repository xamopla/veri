<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gerenciar_usuarios_clientes extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('gerenciar_clientes_model');
		$this->load->model('email_clientes_model');
    }

    public function listar_colaboradores(){ 

    	// VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
    	if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
    		
        	$id = $this->session->userdata['userprimesession_cliente']['id'];

    	} else {

    		$id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
    	}
    	// FIM DA VERIFICAÇÃO 

        $dados['lista_usuarios'] = $this->gerenciar_clientes_model->listar_colaboradores_por_id_cliente($id);
        $dados['id_cliente'] = $id; 

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL 

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/usuarios_listar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function cadastrar_colaborador(){ 

    	// VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
    	if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
    		
        	$id = $this->session->userdata['userprimesession_cliente']['id'];

    	} else {

    		$id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
    	}
    	// FIM DA VERIFICAÇÃO 

    	// NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $dados['id_cliente'] = $id; 
        $dados['id_empresa_origem'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->id_empresa;

		$id_usuario_master = $this->uri->segment(3);
		$id_empresa_origem = $this->uri->segment(4);

		// ALIMENTAR VARIAVEIS
		$razao_social = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->razao_social;
		$nome_fantasia = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->nome_fantasia;
		$cnpj_empresa = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->cnpj_empresa;
		$logradouro = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->logradouro;
		$numero = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->numero;
		$complemento = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->complemento;
		$bairro = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->bairro;
		$cidade = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->cidade;
		$uf = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->uf;
		$cep = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->cep;
		$telefone = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->telefone;
		$celular = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->celular;
		$logo = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->logo;
		$id_one_signal = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->id_onesignal;
		$status = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->status;
		// ALIMENTAR VARIAVEIS FIM

		if(isset($_POST['btn_cadastrar'])){

			$this->gerenciar_clientes_model->setIdEmpresa($id_empresa_origem); 
			$this->gerenciar_clientes_model->setIdUsuarioMaster($id_usuario_master); 
			$this->gerenciar_clientes_model->setRazaoSocial($razao_social); 
			$this->gerenciar_clientes_model->setNomeFantasia($nome_fantasia); 
			$this->gerenciar_clientes_model->setCnpjEmpresa($cnpj_empresa); 
			$this->gerenciar_clientes_model->setLogradouro($logradouro); 
			$this->gerenciar_clientes_model->setNumero($numero); 
			$this->gerenciar_clientes_model->setComplemento($complemento); 
			$this->gerenciar_clientes_model->setBairro($bairro); 
			$this->gerenciar_clientes_model->setCidade($cidade); 
			$this->gerenciar_clientes_model->setUf($uf); 
			$this->gerenciar_clientes_model->setCep($cep); 
			$this->gerenciar_clientes_model->setTelefone($telefone); 
			$this->gerenciar_clientes_model->setLogo($logo); 
			$this->gerenciar_clientes_model->setStatus($status); 
			$this->gerenciar_clientes_model->setIdOnesignal($id_one_signal); 
			$this->gerenciar_clientes_model->setNomeColaborador($this->input->post("nome_colaborador")); 			
			$this->gerenciar_clientes_model->setCelular($this->input->post("celular")); 
			$this->gerenciar_clientes_model->setEmail($this->input->post("email")); 
			$this->gerenciar_clientes_model->setLogin($this->input->post("login")); 
			$this->gerenciar_clientes_model->setSenha(md5($this->input->post("senha"))); 
			$this->gerenciar_clientes_model->setNivel('Colaborador'); 
			$this->gerenciar_clientes_model->setIdCadastro($this->session->userdata['userprimesession_cliente']['id']);		
			date_default_timezone_set('America/Bahia');
			$this->gerenciar_clientes_model->setDataCadastro(date('Y-m-d H:i:s'));

			$result = $this->gerenciar_clientes_model->cadastrar_colaborador(); 

			if ($result){

				$this->session->set_flashdata('msg_alerta', 1);
				redirect('gerenciar_usuarios_clientes/listar_colaboradores');

			} else {

				$this->session->set_flashdata('msg_alerta', 2);
				redirect('gerenciar_usuarios_clientes/listar_colaboradores');
			}

		} 
				
		$this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/usuarios_cadastrar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
	}

	public function editar_colaborador(){

		// VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
    	if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
    		
        	$id = $this->session->userdata['userprimesession_cliente']['id'];

    	} else {

    		$id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
    	}
    	// FIM DA VERIFICAÇÃO 

		// NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $dados['id_cliente'] = $id;

        $id_usuario = $this->uri->segment(3);

        $this->gerenciar_clientes_model->setId($id_usuario);

		if(isset($_POST['btn_salvar'])){

			$this->gerenciar_clientes_model->setNomeColaborador($this->input->post("nome_colaborador")); 			
			$this->gerenciar_clientes_model->setCelular($this->input->post("celular")); 
			$this->gerenciar_clientes_model->setEmail($this->input->post("email"));
			$this->gerenciar_clientes_model->setIdAlteracao($this->session->userdata['userprimesession_cliente']['id']);
			date_default_timezone_set('America/Bahia');
			$this->gerenciar_clientes_model->setDataAlteracao(date('Y-m-d H:i:s'));

			$result = $this->gerenciar_clientes_model->editar_colaborador();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 5);
				redirect("gerenciar_usuarios_clientes/listar_colaboradores");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 6);			
				redirect("gerenciar_usuarios_clientes/listar_colaboradores");
				
			}
		}

		$dados['usuario'] = $this->gerenciar_clientes_model->pesquisar_cliente_id();
		$dados['id_usuario'] = $id_usuario; 

		$this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/usuarios_editar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
	}

	public function inativar_cliente(){

		$id = $this->uri->segment(3);

		$this->gerenciar_clientes_model->setId($id);

		$result = $this->gerenciar_clientes_model->inativar_cliente();

		if ($result){
		
			$this->session->set_flashdata('msg_alerta', 3);
			redirect('gerenciar_usuarios_clientes/listar_colaboradores');
				
		} else {

			$this->session->set_flashdata('msg_alerta', 4);
			redirect('gerenciar_usuarios_clientes/listar_colaboradores');
				
		} 
	}

	public function ativar_cliente_novamente(){

		$id = $this->uri->segment(3);

		$this->gerenciar_clientes_model->setId($id);

		$result = $this->gerenciar_clientes_model->ativar_cliente_novamente();

		if ($result){
		
			$this->session->set_flashdata('msg_alerta', 1);
			redirect('gerenciar_usuarios_clientes/listar_colaboradores');
				
		} else {

			$this->session->set_flashdata('msg_alerta', 2);
			redirect('gerenciar_usuarios_clientes/listar_colaboradores');
				
		} 
	}	

	public function alterar_login(){

		// VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
    	if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
    		
        	$id = $this->session->userdata['userprimesession_cliente']['id'];

    	} else {

    		$id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
    	}
    	// FIM DA VERIFICAÇÃO 

		// NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $dados['id_cliente'] = $id;

        $id_usuario = $this->uri->segment(3);

        $this->gerenciar_clientes_model->setId($id_usuario);

		if(isset($_POST['btn_salvar'])){

			$this->gerenciar_clientes_model->setLogin($this->input->post("login")); 
			$this->gerenciar_clientes_model->setSenha(md5($this->input->post("senha"))); 
			$this->gerenciar_clientes_model->setIdAlteracao($this->session->userdata['userprimesession_cliente']['id']);
			date_default_timezone_set('America/Bahia');
			$this->gerenciar_clientes_model->setDataAlteracao(date('Y-m-d H:i:s'));

			$result = $this->gerenciar_clientes_model->alterar_login();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 5);
				redirect("gerenciar_usuarios_clientes/listar_colaboradores");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 6);			
				redirect("gerenciar_usuarios_clientes/listar_colaboradores");
				
			}
		}

		$dados['usuario'] = $this->gerenciar_clientes_model->pesquisar_cliente_id();
		$dados['id_usuario'] = $id_usuario; 

		$this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/usuarios_alterar_login', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
	}

}