<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suporte extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('usuario_model');
		$this->load->model('plano_model');
		$this->load->model('funcionario_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------  

		$dados['categoria'] = $this->input->post("filtro_categoria");
		// $dados['lida'] = $this->input->post("filtro_lida");

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('suporte/listar_opcoes', $dados);
		$this->load->view('layout/footer');
		
	}

	public function listar_usuarios(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
			
		$dados['consulta'] = $this->usuario_model->listar_usuarios_suporte();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('suporte/usuario_listar', $dados);
		$this->load->view('layout/footer'); 
	}

	public function editar_plano(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);

		if(isset($_POST['btn_salvar'])){

			$nome_do_plano = $this->input->post("nome_do_plano");
			$qtd_usuarios = $this->input->post("qtd_usuarios");
			$qtd_empresas = $this->input->post("qtd_empresas");

			$result = $this->plano_model->editar_plano($nome_do_plano,$qtd_empresas,$qtd_usuarios);

			if ($result){
		
				$this->session->set_flashdata('msg_alerta', 3);

				redirect('suporte/listar');

			} else {
				
				$this->session->set_flashdata('msg_alerta', 4);

				redirect('suporte/listar');

			}

		}

		$dados['plano_contratado'] = $this->plano_model->pesquisar_plano_id();

		$dados['id'] = $id;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('plano_contratado/plano_editar', $dados);
		$this->load->view('layout/footer');
	}

	public function resetar_senha(){

		$id = $this->uri->segment(3);

		$this->funcionario_model->setId($id);

		$result = $this->funcionario_model->resetar_senha();

		if ($result){
	
			$this->session->set_flashdata('msg_alerta', 7);

			redirect('suporte/listar_usuarios');

		} else {
			
			$this->session->set_flashdata('msg_alerta', 4);

			redirect('suporte/listar_usuarios');

		}
	}

	public function listar_cliente(){

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('suporte/listar_cliente', $dados);
		$this->load->view('layout/footer');
	}
}