<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendario extends MY_Controller{

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('Calendario_model', 'e');		
    }

	public function listar(){	
		
		$dados = $this->notificacoes();

		$eventos = $this->e->listar_eventos();
		
		$lista_eventos = array('0' => "Todos");

		foreach ($eventos as $value) {
			
			$lista_eventos[$value->id] = $value->nome;
		}
		$dados['lista_evento'] = $lista_eventos;
		$dados['eventos'] = $eventos;

		$tour = $this->uri->segment(3);
		if($tour != ""){
			$dados['fromTour'] = "true";	
		}
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('calendario/calendario');
		$this->load->view('layout/footer');
		
	}

	public function cadastrarEvento(){

		$this->e->setIdUsuario($this->session->userdata['userprimesession']['id']);

		$nome = $this->input->post('nome');
		$this->e->setNome($nome);

		$cor = $this->input->post('cor');
		$this->e->setCor($cor);

		$r = $this->e->cadastrar();

		$nomeusuario = $this->session->userdata['userprimesession']['nome'];
		if($nomeusuario == null || $nomeusuario == ""){
			$nomeusuario = "Administrador";
		} 

		echo $r.";".$nomeusuario;
	}

	public function cadastrarEventoCriado(){

		$this->e->setIdUsuario($this->session->userdata['userprimesession']['id']);

		$nome = $this->input->post('nome');
		$this->e->setNome($nome);

		$datainicio = $this->input->post('fecini');
		$this->e->setDataInicial($datainicio);

		$this->e->setDataFinal($this->input->post('end'));
		$this->e->setId_evento($this->input->post('id_evento'));

		$cor = $this->input->post('cor');
		$this->e->setCor($cor);

		$r = $this->e->cadastrar_eventos_criados();

		$remover = $this->input->post('remover');

		if($remover != 0){
			$this->e->remover_evento_lista($this->input->post('id_evento'));
		}
		echo $r;
	}

	public function atualizaDataInicial(){

		$param['id'] = $this->input->post('id');
		$param['fecini'] = $this->input->post('fecini');
		$r = $this->e->atualizaDataInicial($param);

		echo $r;
	}

	public function removeEventoDaLista(){

		$param['id'] = $this->input->post('id');
		$r = $this->e->removeEventoDaLista($param);

		echo $r;

	}

	public function obtemEventos(){

		$this->e->setIdUsuario($this->session->userdata['userprimesession']['id']);
		$r = $this->e->listar();
		echo json_encode($r);
	}

	public function listarForEventos(){

		$this->e->setIdUsuario($this->session->userdata['userprimesession']['id']);
		$r = $this->e->listarForEventos();
		echo json_encode($r);
	}

	public function updEvento(){

		$param['id'] = $this->input->post('id');
		$param['fecini'] = $this->input->post('fecini');
		$param['fecfin'] = $this->input->post('fecfin');

		$r = $this->e->updEvento($param);

		echo $r;
	}

	public function deleteEvento(){

		$id = $this->input->post('id');
		$r = $this->e->deleteEvento($id);
		echo $r;
	}

	public function updEvento2(){

		$param['id'] = $this->input->post('id');
		$param['nome'] = $this->input->post('nom');
		$param['web'] = $this->input->post('web');

		$r = $this->e->updEvento2($param);

		echo $r;
	}

	public function renovarDocumento(){

				//--------------- NOTIFICAÇÕES ---------------------
				$dados = $this->notificacoes();
				//-------------- FIM DAS NOTIFICAÇÕES ----------------

				$id = $this->uri->segment(3);
				$id_contabilidade = $this->uri->segment(4);

				$this->documento_model->setId($id);
				$dados['consulta'] = $this->documento_model->listaranexos();
				$dados['documento'] = $this->documento_model->pesquisar_documento_id_renovar();
					
				$dados['id'] = $id;
				$dados['id_contabilidade'] = $id_contabilidade;
					
				$this->load->view('layout/head');	
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('documento/documento_renovar', $dados);
				$this->load->view('layout/footer');
	}

	public function listar_eventos_calendario(){
		$filtro = $this->input->post('filtro');
		$this->e->setIdUsuario($this->session->userdata['userprimesession']['id']);
		$r = $this->e->listar_eventos_calendario($filtro);
		echo json_encode($r);
	}

	public function cadastrarEventoCriadoModal(){

		$this->e->setIdUsuario($this->session->userdata['userprimesession']['id']);

		$nome = $this->input->post('nome');
		$this->e->setNome($nome);
		$this->e->setDataInicial($this->input->post('fecini'));
		$this->e->setDataFinal($this->input->post('end'));
		$this->e->setId_evento($this->input->post('id_evento'));

		$cor = 'red';
		if($this->input->post('id_evento') != 0){
			$cor = $this->e->get_color_evento($this->input->post('id_evento'))->cor;
		}

		$this->e->setCor($cor);
		$this->e->setObservacao($this->input->post('detalhes'));

		if(!empty($this->input->post('frequencia'))){
			$this->e->setFrequencia($this->input->post('frequencia'));
		}else{
			$this->e->setFrequencia('NENHUMA');
		}
		

		$r = $this->e->cadastrar_eventos_criados();
		$data['cor'] = $cor;
		$data['id'] = $r;

		echo $data;
	}

	public function editarEventoCriadoModal(){

		$this->e->setIdUsuario($this->session->userdata['userprimesession']['id']);

		$this->e->setId($this->input->post('id'));
		$nome = $this->input->post('nome');
		$this->e->setNome($nome);
		$this->e->setDataInicial($this->input->post('fecini'));
		$this->e->setDataFinal($this->input->post('end'));
		$this->e->setId_evento($this->input->post('id_evento'));

		$cor = 'red';
		if($this->input->post('id_evento') != 0){
			$cor = $this->e->get_color_evento($this->input->post('id_evento'))->cor;
		}

		$this->e->setCor($cor);
		$this->e->setObservacao($this->input->post('detalhes'));

		$r = $this->e->editar_eventos_modal();
		$data['cor'] = $cor;
		$data['id'] = $r;

		echo $data;
	}

	public function excluir_tarefa(){
		$id = $this->input->post('id');

		$this->e->excluir_tarefa($id);

		echo 1;
	}
}