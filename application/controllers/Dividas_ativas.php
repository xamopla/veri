<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dividas_ativas extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('dividas_ativas_model');
		$this->load->model('usuario_model');
    }

	public function index()
	{
		$dados = $this->notificacoes();

		$dados['qtd_empresas_divida_fgts'] = $this->dividas_ativas_model->qtd_empresas_divida_fgts();
		$dados['qtd_empresas_divida_previdencia'] = $this->dividas_ativas_model->qtd_empresas_divida_previdencia();
		$dados['qtd_empresas_divida_nao_previdencia'] = $this->dividas_ativas_model->qtd_empresas_divida_nao_previdencia_ecac();
		$dados['qtd_empresas_divida_multatrabalhista'] = $this->dividas_ativas_model->qtd_empresas_divida_multatrabalhista();
		$dados['qtd_empresas_divida_multaeleitoral'] = $this->dividas_ativas_model->qtd_empresas_divida_multaeleitoral();
		$dados['qtd_empresas_divida_multacriminal'] = $this->dividas_ativas_model->qtd_empresas_divida_multacriminal();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function listar_fgts(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_fgts();
		$dados['dados_contabilidade'] = $this->usuario_model->consultar_dados_usuario_master();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_fgts_listar', $dados);
		$this->load->view('layout/footer');
	} 

	public function listar_previdencia(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_previdencia();
		$dados['dados_contabilidade'] = $this->usuario_model->consultar_dados_usuario_master();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_previdencia_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_nao_previdenciaria(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_naoprevidenciaria_ecac();
		$dados['dados_contabilidade'] = $this->usuario_model->consultar_dados_usuario_master();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_naoprevidencia_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_nao_previdenciaria_extinta(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_naoprevidenciaria_ecac_extinta();
		$dados['dados_contabilidade'] = $this->usuario_model->consultar_dados_usuario_master();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_naoprevidencia_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_nao_previdenciaria_naoextinta(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_naoprevidenciaria_ecac_naoextinta();
		$dados['dados_contabilidade'] = $this->usuario_model->consultar_dados_usuario_master();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_naoprevidencia_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_multa_trabalhista(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_multatrabalhista();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_multatrabalhista_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_multa_eleitoral(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_multaeleitoral();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_multaeleitoral_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_multa_criminal(){

		$dados = $this->notificacoes();
		
		$dados['consulta'] = $this->dividas_ativas_model->listar_empresas_divida_multacriminal();

		$dados['filtro'] = $this->uri->segment(2);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dividas_ativas/dividas_ativas_multacriminal_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function soNumero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	} 
}
