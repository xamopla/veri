<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consulta_sipro extends MY_Controller {

	public function __construct() {
    parent::__construct();
   	$this->load->model('consulta_sipro_model');
		$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
        $this->load->model('consulta_sipro_documento_model', 'documento_sipro');
    }

	public function index(){
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		// LISTA DE EMPRESAS
		$empresas = $this->consulta_sipro_model->buscar_empresas();
		$lista_empresas = array('' => "Selecione a empresa");

		foreach ($empresas as $value) {
			
			$lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
		}
		// LISTA DE EMPRESAS
		
		$dados['lista_empresas'] = $lista_empresas;

		$dados['consulta'] = $this->consulta_sipro_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consulta_sipro/consulta_sipro_listar', $dados);
		$this->load->view('layout/footer'); 
	}

  public function list(){
    $cnpj = $this->uri->segment(3);
    $razao = "";
    if($cnpj != null){
      $em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
      $razao = $em->razao_social;
    }

    //--------------- NOTIFICAÇÕES ---------------------
    $dados = $this->notificacoes();
    //-------------- FIM DAS NOTIFICAÇÕES ----------------

    // LISTA DE EMPRESAS
    $empresas = $this->consulta_sipro_model->buscar_empresas();
    $lista_empresas = array('' => "Selecione a empresa");

    foreach ($empresas as $value) {
      
      $lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
    }
    // LISTA DE EMPRESAS
    
    $dados['lista_empresas'] = $lista_empresas;

    $dados['consulta'] = $this->consulta_sipro_model->listar();
    $dados['razao_social_filtro'] = $razao;
    
    $this->load->view('layout/head');
    $this->load->view('layout/sidebar');
    $this->load->view('layout/header', $dados);
    $this->load->view('consulta_sipro/consulta_sipro_listar', $dados);
    $this->load->view('layout/footer'); 
  }

	public function cadastrar(){ 

		if(isset($_POST['btn_cadastrar'])){

			$processo = $this->input->post("processo");
			$id_empresa = $this->input->post("id_empresa");

			$verificacao = $this->consulta_sipro_model->verificar_cadastro_duplicado($processo);

			if ($verificacao){

				$this->session->set_flashdata('msg_alerta', 7);			
				redirect("consulta_sipro");

			} else {
        
			$ch = curl_init("https://www.sefaz.ba.gov.br/scripts/sipro/res_consulta_final_sipro.asp?cod_processo=".$processo);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
			curl_setopt($ch, CURLOPT_ENCODING ,"");
			$content = curl_exec($ch); 
			curl_close($ch);

			$dom = new DOMDocument;
			libxml_use_internal_errors(true);
			$dom->loadHTML($content);
			libxml_clear_errors();
			$dom->preserveWhiteSpace = false;
			$tds = $dom->getElementsByTagName('td');
			$arrayTds = array();
			foreach ($tds as $tds) {
			     $arrayTds[] = $tds->nodeValue;
			}

			$numero_processo = trim($arrayTds[3]);
			
			if ($numero_processo == "") {
				$this->session->set_flashdata('msg_alerta', 10);			
				redirect("consulta_sipro");

			} else {

			$nome_processo = trim(Encoding::fixUTF8($arrayTds[4]));
			$cadastramento = trim($arrayTds[6]);
			$nome_cadastramento = trim($arrayTds[7]);
			$interessado = trim($arrayTds[9]);
			$situacao = trim($arrayTds[10]);
			$situacao = str_replace("Situação:","", $situacao); 

			$data_envio = trim($arrayTds[18]);
			$data_recepcao = trim($arrayTds[19]);
			$objetivo = trim(Encoding::fixUTF8($arrayTds[20]));

			$nome_envio = trim($arrayTds[21]);
			$nome_recepcao = trim($arrayTds[22]);

			

			$this->consulta_sipro_model->setNumero_processo($processo); 
			$this->consulta_sipro_model->setId_empresa($id_empresa); 
			$this->consulta_sipro_model->setNumero_processo_formatado($numero_processo); 
			$this->consulta_sipro_model->setTipo_processo($nome_processo); 
			$this->consulta_sipro_model->setData_cadastramento($cadastramento); 
			$this->consulta_sipro_model->setLocal_processo($nome_cadastramento); 
			$this->consulta_sipro_model->setNome_interessado($interessado); 
			$this->consulta_sipro_model->setSituacao_processo($situacao); 
			$this->consulta_sipro_model->setEnvio_data($data_envio); 
			$this->consulta_sipro_model->setRecepcao_data($data_recepcao); 
			$this->consulta_sipro_model->setObjetivo($objetivo); 
			$this->consulta_sipro_model->setEnvio_local($nome_envio); 
			$this->consulta_sipro_model->setRecepcao_local($nome_recepcao);  
			date_default_timezone_set('America/Bahia'); 
			$this->consulta_sipro_model->setData_cadastro(date('Y-m-d H:i:s'));
			$this->consulta_sipro_model->setId_cadastro($this->session->userdata['userprimesession']['id']);

			$result = $this->consulta_sipro_model->cadastrar();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 1);
				redirect("consulta_sipro");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 2);			
				redirect("consulta_sipro");
				
			}

			} 

		}
		}
	}

	public function editar(){ 

    $id = $this->input->post("id");
		$id_empresa = $this->input->post("id_empresa");

        $this->consulta_sipro_model->setId($id); 

        if(isset($_POST['btn_salvar'])){

			$this->consulta_sipro_model->setId_empresa($id_empresa); 

            $result = $this->consulta_sipro_model->editar();
                
            if ($result){

                $this->session->set_flashdata('msg_alerta', 3);
                redirect('consulta_sipro');

            } else {

                $this->session->set_flashdata('msg_alerta', 4);
                redirect('consulta_sipro');
            }
        }  
    }

    public function excluir() {
	
		$id = $this->uri->segment(3);	
	
		$this->consulta_sipro_model->setId($id);
		$result = $this->consulta_sipro_model->excluir();

		if ($result){

			$this->session->set_flashdata('msg_alerta', 5);
			redirect('consulta_sipro');

		} else {
			
			$this->session->set_flashdata('msg_alerta', 6);
			redirect('consulta_sipro');

		}	
	}

//  Actions Documentos
	public function documento_modal(){
        $id_tramitacao_processo_sipro = $_GET['id_tramitacao_processo_sipro'];
        $this->documento_sipro->setId_tramitacao_processo_sipro($id_tramitacao_processo_sipro);
        $lista = $this->documento_sipro->listar();
        $dados = ['lista' => $lista, 'id_tramitacao_processo_sipro' =>  $_GET['id_tramitacao_processo_sipro']];
        $this->load->view('consulta_sipro/documento_modal', $dados);
    }

    public function cadastrar_documento(){

        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA
        
        $folder = 'anexos_sipro/'.$server.'/';
        // definimos o path onde o arquivo será gravado
        $path = "./uploads/".$folder;

        // verificamos se o diretório existe
        // se não existe criamos com permissão de leitura e escrita
        if ( ! is_dir($path)) {
            mkdir($path, 0777, $recursive = true);
        }

        $configUpload['upload_path']   = $path;
        // definimos - através da extensão -
        // os tipos de arquivos suportados
        $configUpload['allowed_types'] = 'jpg|png|pdf|doc|xls|txt|xml|pfx|zip';
        // definimos que o nome do arquivo
        // será alterado para um nome criptografado
        $configUpload['encrypt_name']  = TRUE;

        //$this->load->library('upload', $config);
        $this->load->library('upload');

        $this->upload->initialize($configUpload);

        if (!$this->upload->do_upload('anexo1')){
            // em caso de erro retornamos os mesmos para uma variável
            // e enviamos para a home
            $data= array('error' => $this->upload->display_errors());
            //$this->load->view('home',$data);
            $this->session->set_flashdata('msg_titulo','ERRO!');
            $this->session->set_flashdata('msg_conteudo', $data['error']);
            $this->session->set_flashdata('msg_tipo','danger');

            echo json_encode([]);

        }else{
            //se correu tudo bem, recuperamos os dados do arquivo
            $data['dadosArquivo'] = $this->upload->data();
            // definimos o path original do arquivo
            $arquivoPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
            // passando para o array '$data'
            $data['urlArquivo'] = base_url($arquivoPath);
            // definimos a URL para download
            $downloadPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
            // passando para o array '$data'
            $data['urlDownload'] = base_url($downloadPath);

            $this->documento_sipro->setAnexoNome($data['dadosArquivo']['orig_name']);
            $this->documento_sipro->setAnexoPath($data['urlDownload']);

            $this->documento_sipro->setId_tramitacao_processo_sipro($_POST['id_tramitacao_processo_sipro']);
            $this->documento_sipro->setNome($_POST['nome']);

            $id = $this->documento_sipro->cadastrar();
            $data = $this->documento_sipro->findById($id);
            echo json_encode($data, true);

        }

        
    }

    public function remover_documento(){

        $id = $this->uri->segment(3);
        $this->documento_sipro->setId($id);
        $this->documento_sipro->excluir();

        $this->session->set_flashdata('msg_alerta', 12);
        redirect('consulta_sipro');
    }
}

// ------------------------------------- CLASSE DE ENCODING ----------------------------------------
class Encoding {

  const ICONV_TRANSLIT = "TRANSLIT";
  const ICONV_IGNORE = "IGNORE";
  const WITHOUT_ICONV = "";

  protected static $win1252ToUtf8 = array(
        128 => "\xe2\x82\xac",

        130 => "\xe2\x80\x9a",
        131 => "\xc6\x92",
        132 => "\xe2\x80\x9e",
        133 => "\xe2\x80\xa6",
        134 => "\xe2\x80\xa0",
        135 => "\xe2\x80\xa1",
        136 => "\xcb\x86",
        137 => "\xe2\x80\xb0",
        138 => "\xc5\xa0",
        139 => "\xe2\x80\xb9",
        140 => "\xc5\x92",

        142 => "\xc5\xbd",


        145 => "\xe2\x80\x98",
        146 => "\xe2\x80\x99",
        147 => "\xe2\x80\x9c",
        148 => "\xe2\x80\x9d",
        149 => "\xe2\x80\xa2",
        150 => "\xe2\x80\x93",
        151 => "\xe2\x80\x94",
        152 => "\xcb\x9c",
        153 => "\xe2\x84\xa2",
        154 => "\xc5\xa1",
        155 => "\xe2\x80\xba",
        156 => "\xc5\x93",

        158 => "\xc5\xbe",
        159 => "\xc5\xb8"
  );

    protected static $brokenUtf8ToUtf8 = array(
        "\xc2\x80" => "\xe2\x82\xac",

        "\xc2\x82" => "\xe2\x80\x9a",
        "\xc2\x83" => "\xc6\x92",
        "\xc2\x84" => "\xe2\x80\x9e",
        "\xc2\x85" => "\xe2\x80\xa6",
        "\xc2\x86" => "\xe2\x80\xa0",
        "\xc2\x87" => "\xe2\x80\xa1",
        "\xc2\x88" => "\xcb\x86",
        "\xc2\x89" => "\xe2\x80\xb0",
        "\xc2\x8a" => "\xc5\xa0",
        "\xc2\x8b" => "\xe2\x80\xb9",
        "\xc2\x8c" => "\xc5\x92",

        "\xc2\x8e" => "\xc5\xbd",


        "\xc2\x91" => "\xe2\x80\x98",
        "\xc2\x92" => "\xe2\x80\x99",
        "\xc2\x93" => "\xe2\x80\x9c",
        "\xc2\x94" => "\xe2\x80\x9d",
        "\xc2\x95" => "\xe2\x80\xa2",
        "\xc2\x96" => "\xe2\x80\x93",
        "\xc2\x97" => "\xe2\x80\x94",
        "\xc2\x98" => "\xcb\x9c",
        "\xc2\x99" => "\xe2\x84\xa2",
        "\xc2\x9a" => "\xc5\xa1",
        "\xc2\x9b" => "\xe2\x80\xba",
        "\xc2\x9c" => "\xc5\x93",

        "\xc2\x9e" => "\xc5\xbe",
        "\xc2\x9f" => "\xc5\xb8"
  );

  protected static $utf8ToWin1252 = array(
       "\xe2\x82\xac" => "\x80",

       "\xe2\x80\x9a" => "\x82",
       "\xc6\x92"     => "\x83",
       "\xe2\x80\x9e" => "\x84",
       "\xe2\x80\xa6" => "\x85",
       "\xe2\x80\xa0" => "\x86",
       "\xe2\x80\xa1" => "\x87",
       "\xcb\x86"     => "\x88",
       "\xe2\x80\xb0" => "\x89",
       "\xc5\xa0"     => "\x8a",
       "\xe2\x80\xb9" => "\x8b",
       "\xc5\x92"     => "\x8c",

       "\xc5\xbd"     => "\x8e",


       "\xe2\x80\x98" => "\x91",
       "\xe2\x80\x99" => "\x92",
       "\xe2\x80\x9c" => "\x93",
       "\xe2\x80\x9d" => "\x94",
       "\xe2\x80\xa2" => "\x95",
       "\xe2\x80\x93" => "\x96",
       "\xe2\x80\x94" => "\x97",
       "\xcb\x9c"     => "\x98",
       "\xe2\x84\xa2" => "\x99",
       "\xc5\xa1"     => "\x9a",
       "\xe2\x80\xba" => "\x9b",
       "\xc5\x93"     => "\x9c",

       "\xc5\xbe"     => "\x9e",
       "\xc5\xb8"     => "\x9f"
    );

  static function toUTF8($text){
  /**
   * Function \ForceUTF8\Encoding::toUTF8
   *
   * This function leaves UTF8 characters alone, while converting almost all non-UTF8 to UTF8.
   *
   * It assumes that the encoding of the original string is either Windows-1252 or ISO 8859-1.
   *
   * It may fail to convert characters to UTF-8 if they fall into one of these scenarios:
   *
   * 1) when any of these characters:   ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß
   *    are followed by any of these:  ("group B")
   *                                    ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶•¸¹º»¼½¾¿
   * For example:   %ABREPRESENT%C9%BB. «REPRESENTÉ»
   * The "«" (%AB) character will be converted, but the "É" followed by "»" (%C9%BB)
   * is also a valid unicode character, and will be left unchanged.
   *
   * 2) when any of these: àáâãäåæçèéêëìíîï  are followed by TWO chars from group B,
   * 3) when any of these: ðñòó  are followed by THREE chars from group B.
   *
   * @name toUTF8
   * @param string $text  Any string.
   * @return string  The same string, UTF8 encoded
   *
   */

    if(is_array($text))
    {
      foreach($text as $k => $v)
      {
        $text[$k] = self::toUTF8($v);
      }
      return $text;
    }

    if(!is_string($text)) {
      return $text;
    }

    $max = self::strlen($text);

    $buf = "";
    for($i = 0; $i < $max; $i++){
        $c1 = $text[$i];
        if($c1>="\xc0"){ //Should be converted to UTF8, if it's not UTF8 already
          $c2 = $i+1 >= $max? "\x00" : $text[$i+1];
          $c3 = $i+2 >= $max? "\x00" : $text[$i+2];
          $c4 = $i+3 >= $max? "\x00" : $text[$i+3];
            if($c1 >= "\xc0" & $c1 <= "\xdf"){ //looks like 2 bytes UTF8
                if($c2 >= "\x80" && $c2 <= "\xbf"){ //yeah, almost sure it's UTF8 already
                    $buf .= $c1 . $c2;
                    $i++;
                } else { //not valid UTF8.  Convert it.
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } elseif($c1 >= "\xe0" & $c1 <= "\xef"){ //looks like 3 bytes UTF8
                if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf"){ //yeah, almost sure it's UTF8 already
                    $buf .= $c1 . $c2 . $c3;
                    $i = $i + 2;
                } else { //not valid UTF8.  Convert it.
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } elseif($c1 >= "\xf0" & $c1 <= "\xf7"){ //looks like 4 bytes UTF8
                if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf" && $c4 >= "\x80" && $c4 <= "\xbf"){ //yeah, almost sure it's UTF8 already
                    $buf .= $c1 . $c2 . $c3 . $c4;
                    $i = $i + 3;
                } else { //not valid UTF8.  Convert it.
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = ($c1 & "\x3f") | "\x80";
                    $buf .= $cc1 . $cc2;
                }
            } else { //doesn't look like UTF8, but should be converted
                    $cc1 = (chr(ord($c1) / 64) | "\xc0");
                    $cc2 = (($c1 & "\x3f") | "\x80");
                    $buf .= $cc1 . $cc2;
            }
        } elseif(($c1 & "\xc0") === "\x80"){ // needs conversion
              if(isset(self::$win1252ToUtf8[ord($c1)])) { //found in Windows-1252 special cases
                  $buf .= self::$win1252ToUtf8[ord($c1)];
              } else {
                $cc1 = (chr(ord($c1) / 64) | "\xc0");
                $cc2 = (($c1 & "\x3f") | "\x80");
                $buf .= $cc1 . $cc2;
              }
        } else { // it doesn't need conversion
            $buf .= $c1;
        }
    }
    return $buf;
  }

  static function toWin1252($text, $option = self::WITHOUT_ICONV) {
    if(is_array($text)) {
      foreach($text as $k => $v) {
        $text[$k] = self::toWin1252($v, $option);
      }
      return $text;
    } elseif(is_string($text)) {
      return static::utf8_decode($text, $option);
    } else {
      return $text;
    }
  }

  static function toISO8859($text, $option = self::WITHOUT_ICONV) {
    return self::toWin1252($text, $option);
  }

  static function toLatin1($text, $option = self::WITHOUT_ICONV) {
    return self::toWin1252($text, $option);
  }

  static function fixUTF8($text, $option = self::WITHOUT_ICONV){
    if(is_array($text)) {
      foreach($text as $k => $v) {
        $text[$k] = self::fixUTF8($v, $option);
      }
      return $text;
    }

    if(!is_string($text)) {
      return $text;
    }

    $last = "";
    while($last <> $text){
      $last = $text;
      $text = self::toUTF8(static::utf8_decode($text, $option));
    }
    $text = self::toUTF8(static::utf8_decode($text, $option));
    return $text;
  }

  static function UTF8FixWin1252Chars($text){
    // If you received an UTF-8 string that was converted from Windows-1252 as it was ISO8859-1
    // (ignoring Windows-1252 chars from 80 to 9F) use this function to fix it.
    // See: http://en.wikipedia.org/wiki/Windows-1252

    return str_replace(array_keys(self::$brokenUtf8ToUtf8), array_values(self::$brokenUtf8ToUtf8), $text);
  }

  static function removeBOM($str=""){
    if(substr($str, 0,3) === pack("CCC",0xef,0xbb,0xbf)) {
      $str=substr($str, 3);
    }
    return $str;
  }

  protected static function strlen($text){
    return (function_exists('mb_strlen') && ((int) ini_get('mbstring.func_overload')) & 2) ?
           mb_strlen($text,'8bit') : strlen($text);
  }

  public static function normalizeEncoding($encodingLabel)
  {
    $encoding = strtoupper($encodingLabel);
    $encoding = preg_replace('/[^a-zA-Z0-9\s]/', '', $encoding);
    $equivalences = array(
        'ISO88591' => 'ISO-8859-1',
        'ISO8859'  => 'ISO-8859-1',
        'ISO'      => 'ISO-8859-1',
        'LATIN1'   => 'ISO-8859-1',
        'LATIN'    => 'ISO-8859-1',
        'UTF8'     => 'UTF-8',
        'UTF'      => 'UTF-8',
        'WIN1252'  => 'ISO-8859-1',
        'WINDOWS1252' => 'ISO-8859-1'
    );

    if(empty($equivalences[$encoding])){
      return 'UTF-8';
    }

    return $equivalences[$encoding];
  }

  public static function encode($encodingLabel, $text)
  {
    $encodingLabel = self::normalizeEncoding($encodingLabel);
    if($encodingLabel === 'ISO-8859-1') return self::toLatin1($text);
    return self::toUTF8($text);
  }

  protected static function utf8_decode($text, $option = self::WITHOUT_ICONV)
  {
    if ($option == self::WITHOUT_ICONV || !function_exists('iconv')) {
       $o = utf8_decode(
         str_replace(array_keys(self::$utf8ToWin1252), array_values(self::$utf8ToWin1252), self::toUTF8($text))
       );
    } else {
       $o = iconv("UTF-8", "Windows-1252" . ($option === self::ICONV_TRANSLIT ? '//TRANSLIT' : ($option === self::ICONV_IGNORE ? '//IGNORE' : '')), $text);
    }
    return $o;
  }
}