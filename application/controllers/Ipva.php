<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ipva extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('ipva_clientes_model'); 
       	$this->load->model('usuario_model'); 
    }

	public function index(){

        $resultado = $this->ipva_clientes_model->listar();

        //--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

        $placas = array();

        foreach ($resultado as $item) {
            if ($a = explode("|", $item->placas)) {
                foreach ($a as $s) {
                    array_push($placas,$s);
                }
            }
        }

        $dados['lista_ipva'] = $resultado; 
        $dados['dados_contabilidade'] = $this->usuario_model->consultar_dados_usuario_master();

        $this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ipva/ipva_listar');
		$this->load->view('layout/footer');
    }

  //   public function index(){

  //       $resultado = $this->ipva_clientes_model->listar();

  //       //--------------- NOTIFICAÇÕES ---------------------
		// $dados = $this->notificacoes();
		// //-------------- FIM DAS NOTIFICAÇÕES ----------------

  //       $placas = array();
  //       $objeto = new \stdClass();

  //       foreach ($resultado as $item) {
  //           if ($a = explode("|", $item->placas)) {
  //               foreach ($a as $s) {
  //               	$objeto->placa = $s;
  //               	$objeto->cnpj = $item->cnpj;
  //               	$objeto->razao_social = $item->razao_social;

  //                   array_push($placas,$objeto);
  //               }
  //           }
  //       }

  //       $dados['lista_ipva'] = $resultado; 

  //       $this->load->view('layout/head');
		// $this->load->view('layout/sidebar');
		// $this->load->view('layout/header', $dados);
		// $this->load->view('ipva/ipva_listar');
		// $this->load->view('layout/footer');
  //   }
}