<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Pgdas extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('pgdas_model');
        $this->load->model('empresa_model');
    }

	public function index(){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;

		if(isset($_GET['filtro'])){
			$filtro = $_GET["filtro"];
		}else{
			$filtro = 'TODAS';
		}
		
		$this->pgdas_model->setFiltro_situacao($filtro);

		$dados['filtro'] = $filtro;
		
		$botao = $this->uri->segment(3);
		if($botao != null){

			$mes = $this->uri->segment(4);
			$mes_numero = 1;
			$ano = $this->uri->segment(5);

			switch ($mes) {
		        case "Janeiro":    $mes_numero = 1;     break;
		        case "Fevereiro":    $mes_numero = 2;   break;
		        case "Marco":    $mes_numero = 3;       break;
		        case "Abril":    $mes_numero = 4;       break;
		        case "Maio":    $mes_numero = 5;        break;
		        case "Junho":    $mes_numero = 6;       break;
		        case "Julho":    $mes_numero = 7;       break;
		        case "Agosto":    $mes_numero = 8;      break;
		        case "Setembro":    $mes_numero = 9;    break;
		        case "Outubro":    $mes_numero = 10;     break;
		        case "Novembro":    $mes_numero = 11;    break;
		        case "Dezembro":    $mes_numero = 12;    break; 
		 	} 

			if($botao == "anterior"){
				$mes_numero--;
				if($mes_numero == 0){
					$mes_numero = 12;
					$ano--;
				}
			}else{
				$mes_numero++;
				if($mes_numero == 13){
					$mes_numero = 1;
					$ano++;
				}
			}
			$this->pgdas_model->setFiltroMes($mes_numero);
			$this->pgdas_model->setFiltroAno($ano);

			if($mes_numero < 10){
				$mes_aux = '0'.$mes_numero;
			}else{
				$mes_aux = $mes_numero;
			}
			
			$ano_aux = $ano;

			$resultados = $this->pgdas_model->listarFiltro();

			$dados['m'] = $mes_numero;
			$dados['ano'] = $ano;

		} elseif (isset($_POST['mes_selecionado'])) {

			$mes_selecionado = $this->input->post("mes_selecionado");
			$ano_selecionado = $this->input->post("ano_selecionado");
			
		 	$this->pgdas_model->setFiltroMes($mes_selecionado);
			$this->pgdas_model->setFiltroAno($ano_selecionado);

			$mes_aux = $mes_selecionado;
			$ano_aux = $ano_selecionado;

			$resultados = $this->pgdas_model->listarFiltroByBotaoMes();
			$dados['m'] = $mes_selecionado;
			$dados['ano'] = $ano_selecionado;
			
		} else{

			$resultados = $this->pgdas_model->listar();

			$data_atual = date('Y-m-d');

			$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

			$arr = explode("-", $data_aux);
			$mes_ = $arr[1];
			$ano_ = $arr[0];

			$dados['m'] = $mes_;
			$dados['ano'] = $ano_;

			$mes_aux = $mes_;
			$ano_aux = $ano_;
		}
		
		// $ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

		// $data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		// $data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

		// $dados['ultimo_dia_transmitir'] = $data_aux;

		$ultimo_dia_transmitir = $this->get_days_uteis(20, $mes_aux+1, $ano_aux );
		//$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

		// $data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = $ultimo_dia_transmitir; 

		$dados['ultimo_dia_transmitir'] = $ultimo_dia_transmitir;

		//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
		$data_final = strtotime($data_aux);

        $df = new DateTime($data_aux);
        $da = new DateTime(date(''));
        $intervalo = $df->diff($da);

        $fora_prazo = false;

        if ($data_final < strtotime(date('Y-m-d'))) {
          $fora_prazo = true;
        } else{
          $fora_prazo = false;
        }

        $mes_filtro_aux = ltrim($dados['m'], "0");
        $ano_filtro_aux = $dados['ano'];

        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

  //       $sem_movimento = $this->pgdas_model->buscar_empresas_sem_movimento($data_filtro_aux);
  //       $myhashmap = array();
  //       foreach ($sem_movimento as $d) {
		// 	$myhashmap[$d->cnpj] = $d;
		// }

		$resultado_final = array();

		foreach ($resultados as $r) {
        	if($fora_prazo == false){

        		if($r->sem_pgdas == 1){
        			$r->status_geral = "TRANSMITIDA";
        			$r->sub_status_geral = "";

        			if(empty($r->pago)){
    					$r->sub_status_geral = "SEM_MOVIMENTO";
    				}

        		}elseif($r->sem_pgdas == 0){
        			$r->status_geral = "EM_ANALISE";
        			$r->sub_status_geral = "EM_ANALISE";
        		}

        	}else{

        		if($r->sem_pgdas == 1){
        			$r->status_geral = "TRANSMITIDA";
        			$r->sub_status_geral = "";

        			if(empty($r->pago)){
    					$r->sub_status_geral = "SEM_MOVIMENTO";
    				}
        		
        		}elseif($r->sem_pgdas == 0){
        			$r->status_geral = "VENCIDA";
        			$r->sub_status_geral = "VENCIDA";
        		}

        		
        	}

        	if($filtro != null && $filtro != "TODAS"){
				if($filtro == "REGULAR"){
					if($r->status_geral == "TRANSMITIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "PENDENTE") {
					if($r->status_geral == "VENCIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "ANALISAR") {
					if($r->status_geral == "EM_ANALISE"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "SEM_MOVIMENTO") {
					if($r->sub_status_geral == "SEM_MOVIMENTO"){
						array_push($resultado_final, $r);
					}
				}
			}else{
				array_push($resultado_final, $r);
			}

        }

        ///////////////////////////////////////////////////
		$dados['consulta'] = $resultado_final;
		$dados['id_usuario_logado'] = $this->session->userdata['userprimesession']['id'];

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/pgdas', $dados);
		$this->load->view('layout/footer');	
	}

	public function get_days_uteis($dia, $mes, $ano){
		if($mes == 13){
			$mes = '01';
			$ano++;
		}
		$tmpDate = $ano.'-'.$mes.'-'.$dia;


		$holidays = [$ano.'-01-01', $ano.'-02-17', $ano.'-04-02', $ano.'-04-21', $ano.'-05-01', $ano.'-06-03', $ano.'-09-07', $ano.'-10-12', $ano.'-11-02', $ano.'-11-15', $ano.'-12-25'];
		$i = 0;
		$nextBusinessDay = date('Y-m-d', strtotime($tmpDate . ' +' . $i . ' Weekday'));

		while (in_array($nextBusinessDay, $holidays)) {
		    $i++;
		    $nextBusinessDay = date('Y-m-d', strtotime($tmpDate . ' +' . $i . ' Weekday'));
		}
		return $nextBusinessDay;

	}

	public static function getDiaUtil($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
		date_default_timezone_set('America/Sao_Paulo');

		$iMes = empty($iMes) ? date('m') : $iMes;
		$iAno = empty($iAno) ? date('Y') : $iAno;
		$iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));
 
		for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
			$iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
			//inclui apenas os dias úteis
			if ($iDiaSemana < 6) {
				$aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
			}
		}
		//ignorando os feriados	
		if (sizeof($aDiasIgnorar) > 0) {
			foreach ($aDiasIgnorar as $iDia) {
				$iKey = array_search($iDia, $aDias);
				unset($aDias[$iKey]);
			}
		}
 
		if (isset($aDias[$iDia - 1])) {
			return $aDias[$iDia - 1];
		} else {
			//retorna o último dia útil
			return $aDias[count($aDias) - 1];
		}
	}


	public function list_from_notification(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;

		$resultados = $this->pgdas_model->listar();

		$data_atual = date('Y-m-d');

		$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

		$arr = explode("-", $data_aux);
		$mes_ = $arr[1];
		$ano_ = $arr[0];

		$dados['m'] = $mes_;
		$dados['ano'] = $ano_;

		$mes_aux = $mes_;
		$ano_aux = $ano_;
		
		$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;
		$dados['consulta'] = $resultados;
		$dados['razao_social_filtro'] = $razao;

		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/pgdas', $dados);
		$this->load->view('layout/footer');	
	}


	public function update_simples(){
		$id = $this->input->post('id');

		$this->pgdas_model->update_simples($id);

		echo 1;
	}

	public function busca_dados_modal(){
		$numero = $this->input->post('numero');

		$resultado = $this->pgdas_model->busca_dados_modal($numero);

		echo json_encode($resultado);
	}


	public function marcar_sem_movimento(){
		$cnpj = $this->input->post('cnpj');
		$mes = $this->input->post('mes');
		$ano = $this->input->post('ano');

		$mes_filtro_aux = ltrim($mes, "0");
		$data_aux = $mes_filtro_aux."/".$ano;

		$this->pgdas_model->marcar_sem_movimento($cnpj, $data_aux);

		$this->pgdas_model->insere_notificacao($cnpj, $data_aux);

		echo 1;
	}

	public function listar_from_notificacao(){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;
		
		$this->pgdas_model->setFiltro_situacao('TODAS');
		$filtro = "TODAS";
		
		$cnpj = $this->uri->segment(3);
		$dados['razao_social_filtro'] = $cnpj;

		if(true){

			$mes = $this->uri->segment(4);
			$mes_numero = 1;
			$ano = $this->uri->segment(5);

			switch ($mes) {
		        case "01":    $mes_numero = 1;     break;
		        case "02":    $mes_numero = 2;   break;
		        case "03":    $mes_numero = 3;       break;
		        case "04":    $mes_numero = 4;       break;
		        case "05":    $mes_numero = 5;        break;
		        case "06":    $mes_numero = 6;       break;
		        case "07":    $mes_numero = 7;       break;
		        case "08":    $mes_numero = 8;      break;
		        case "09":    $mes_numero = 9;    break;
		        case "10":    $mes_numero = 10;     break;
		        case "11":    $mes_numero = 11;    break;
		        case "12":    $mes_numero = 12;    break; 
		 	} 

			$this->pgdas_model->setFiltroMes($mes_numero);
			$this->pgdas_model->setFiltroAno($ano);

			if($mes_numero < 10){
				$mes_aux = '0'.$mes_numero;
			}else{
				$mes_aux = $mes_numero;
			}
			
			$ano_aux = $ano;

			$resultados = $this->pgdas_model->listarFiltro();

			$dados['m'] = $mes_numero;
			$dados['ano'] = $ano;

		} 
		
		$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;

		//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
		$data_final = strtotime($data_aux);

        $df = new DateTime($data_aux);
        $da = new DateTime(date(''));
        $intervalo = $df->diff($da);

        $fora_prazo = false;

        if ($data_final < strtotime(date('Y-m-d'))) {
          $fora_prazo = true;
        } else{
          $fora_prazo = false;
        }

        $mes_filtro_aux = ltrim($dados['m'], "0");
        $ano_filtro_aux = $dados['ano'];

        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

  //       $sem_movimento = $this->pgdas_model->buscar_empresas_sem_movimento($data_filtro_aux);
  //       $myhashmap = array();
  //       foreach ($sem_movimento as $d) {
		// 	$myhashmap[$d->cnpj] = $d;
		// }

		$resultado_final = array();

		foreach ($resultados as $r) {
        	if($fora_prazo == false){

        		if($r->sem_pgdas == 1){
        			$r->status_geral = "TRANSMITIDA";
        			$r->sub_status_geral = "";

        			if(empty($r->pago)){
    					$r->sub_status_geral = "SEM_MOVIMENTO";
    				}

        		}elseif($r->sem_pgdas == 0){
        			$r->status_geral = "EM_ANALISE";
        			$r->sub_status_geral = "EM_ANALISE";
        		}

        	}else{

        		if($r->sem_pgdas == 1){
        			$r->status_geral = "TRANSMITIDA";
        			$r->sub_status_geral = "";

        			if(empty($r->pago)){
    					$r->sub_status_geral = "SEM_MOVIMENTO";
    				}
        		
        		}elseif($r->sem_pgdas == 0){
        			$r->status_geral = "VENCIDA";
        			$r->sub_status_geral = "VENCIDA";
        		}

        		
        	}

        	if($filtro != null && $filtro != "TODAS"){
				if($filtro == "REGULAR"){
					if($r->status_geral == "TRANSMITIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "PENDENTE") {
					if($r->status_geral == "VENCIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "ANALISAR") {
					if($r->status_geral == "EM_ANALISE"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "SEM_MOVIMENTO") {
					if($r->sub_status_geral == "SEM_MOVIMENTO"){
						array_push($resultado_final, $r);
					}
				}
			}else{
				array_push($resultado_final, $r);
			}

        }


        ///////////////////////////////////////////////////
		$dados['consulta'] = $resultado_final;
		$dados['id_usuario_logado'] = $this->session->userdata['userprimesession']['id'];

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/pgdas', $dados);
		$this->load->view('layout/footer');	
	}
}