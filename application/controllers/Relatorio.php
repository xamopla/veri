<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Relatorio extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("empresa_model");
		$this->load->model("relatorio_model");
	}

	public function dashboard(){
		$dados = $this->notificacoes();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('relatorio/dashboard', $dados);
		$this->load->view('layout/footer');
	}

	public function pendencia_empresa(){
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$empresas = $this->relatorio_model->find_all_empresas();
		
		if(isset($_POST['btn_pesquisar'])){
			$id_empresa = $this->input->post("id_empresa");

			$empresa = $this->empresa_model->find_empresa_by_id_empresa($id_empresa);
			$this->relatorio_model->setCnpj($empresa->cnpj);
			$this->relatorio_model->setCnpj_completo($empresa->cnpj_completo);

			$qtd_msg_ecac = 0;
			$id_caixa_postal = 0;
			$retorno_msg = $this->relatorio_model->qtd_msg_nao_lidas();
			if(isset($retorno_msg)){
				$qtd_msg_ecac = $retorno_msg->nao_lidas;
				$id_caixa_postal = $retorno_msg->id;
			}

			$pendencia_fiscal = 0;
			$caminho_download = "";
			$retorno_pendecia_ecac =  $this->relatorio_model->possui_pendencia_ecac();
			if(isset($retorno_pendecia_ecac)){
				$pendencia_fiscal = $retorno_pendecia_ecac->possui_pendencia;
				$caminho_download = $retorno_pendecia_ecac->caminho_download;
			}

			$retorno_parcelamento_das = $this->relatorio_model->parcelamento_das()->qtd;

			$dados['possui_certificado'] = $this->relatorio_model->possui_certificado_digital()->valor;

			$dados['caminho_download'] = $caminho_download;
			$dados['id_caixa_postal'] = $id_caixa_postal;
			$dados['empresa'] = $empresa;
			$dados['msg_ecac'] = $qtd_msg_ecac;
			$dados['pendencia_fiscal'] = $pendencia_fiscal;
			$dados['parcelamento_das'] = $retorno_parcelamento_das;
			$dados['resumo_fiscal'] = $this->relatorio_model->resumo_fiscal();
			$dados['situacao_fiscal'] = $this->relatorio_model->situacao_fiscal();
			$qtd_msg_dte = $this->relatorio_model->qtd_registros_msg_nao_lidas_dte();
			if(isset($qtd_msg_dte)){
				$qtd_msg_dte = $this->soNumero($qtd_msg_dte->valor);
			}else{
				$qtd_msg_dte = 0;
			}
			$dados['qtd_msg_dte'] = $qtd_msg_dte;
			$dados['cnpj'] = $empresa->cnpj;

			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('relatorio/pendencias_empresas_listar', $dados);
			$this->load->view('layout/footer');
			
		} else {
			
			$empresas_lista = array();

			foreach ($empresas as $e){
				$empresas_lista[$e->id] = $e->razao_social." - ".$e->cnpj_completo;
			}
			
			$dados['empresas'] = $empresas_lista;
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('relatorio/pendencias_empresas', $dados);
			$this->load->view('layout/footer');			
		}
	}

	public function soNumero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	}

	public function mostrar_parcelamento(){
		$cnpj = $this->input->post('cnpj');
		$this->relatorio_model->setCnpj($cnpj);

		$resultado = $this->relatorio_model->obter_parcelamento_das();

		echo json_encode($resultado);
	}

	public function timeline(){
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$empresas = $this->relatorio_model->find_all_empresas();
		
		if(isset($_POST['btn_pesquisar'])){
			$id_empresa = $this->input->post("id_empresa");

			$empresa = $this->empresa_model->find_empresa_by_id_empresa($id_empresa);
			$this->relatorio_model->setCnpj($empresa->cnpj);
			$this->relatorio_model->setCnpj_completo($empresa->cnpj_completo);
			$this->relatorio_model->setId_empresa($empresa->id);

			$dados['consulta'] = $this->relatorio_model->timeline();
			$dados['criacao'] = $empresa->data_cadastro;
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('relatorio/timeline_listar', $dados);
			$this->load->view('layout/footer');
			
		} else {
			
			$empresas_lista = array();

			foreach ($empresas as $e){
				$empresas_lista[$e->id] = $e->razao_social." - ".$e->cnpj_completo;
			}
			
			$dados['empresas'] = $empresas_lista;
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('relatorio/timeline', $dados);
			$this->load->view('layout/footer');			
		}
	}

	public function pendencia_resumo_fiscal(){
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		if(isset($_POST['btn_pesquisar'])){
			$id_pendencia = $this->input->post("id_pendencia");

			if($id_pendencia == 1){
				$omisso_dma = $this->relatorio_model->omissodma();
				$this->relatorio_dma($omisso_dma);

			}else if($id_pendencia == 2){
				$omisso_efd = $this->relatorio_model->omissoefd();
				$this->relatorio_efd($omisso_efd);

			}else if($id_pendencia == 3){

			}
			
		} else {
			$pendencias = $array = array(
							    "1" => "Omisso DMA",
							    "2" => "Omisso EFD",
							);
			
			$dados['pendencias'] = $pendencias;
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('relatorio/pendencias_resumo_fiscal', $dados);
			$this->load->view('layout/footer');			
		}

	}

	/////////////////////// Relatórios em PDF //////////////////////////////////////////////////////////////
	public function relatorio_dma($result) {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
					
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO DE EMPRESAS OMISSAS DMA',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
		
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
		
		$pdf->SetFont("Courier","B",9);
		
		$pdf->Cell(80,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(30,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(85,8,"ANO MÊS(ES)","TBLR",0,"C");
		//$pdf->Cell(30,8,"MÊS(ES)","TBLR",0,"C");
		
		$pdf->Ln();
		
		$pdf->SetFont("Courier","",7);
		
		foreach ($result as $r) {
			
			$pdf->SetTextColor(0,0,0);
			$omisso = $this->convertStringDMA($r->pergunta2);
			

			$pdf->Cell(80,8,$r->razaoSocial,"TBL",0,"L");
			$pdf->Cell(30,8,$r->cnpj_completo,"TBL",0,"C");

			$pdf->SetTextColor(194,8,8);
			$pdf->MultiCell( 85, 8, $omisso, 1);

			$pdf->Ln();
			//$pdf->Cell(20,8,$r->situacao_cadastral,"TBLR",0,"C");
			
		}
		
		$pdf->Output('Relatorio Empresas Omissas DMA - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}

	public function relatorio_efd($result) {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
					
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO DE EMPRESAS OMISSAS EFD',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
		
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
		
		$pdf->SetFont("Courier","B",9);
		
		$pdf->Cell(80,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(30,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(85,8,"ANO MÊS(ES)","TBLR",0,"C");
		//$pdf->Cell(30,8,"MÊS(ES)","TBLR",0,"C");
		
		$pdf->Ln();
		
		$pdf->SetFont("Courier","",7);
		
		foreach ($result as $r) {
			
			$pdf->SetTextColor(0,0,0);
			$omisso = $this->convertStringDMA($r->pergunta3);
			

			$pdf->Cell(80,8,$r->razaoSocial,"TBL",0,"L");
			$pdf->Cell(30,8,$r->cnpj_completo,"TBL",0,"C");

			$pdf->SetTextColor(194,8,8);
			$pdf->MultiCell( 85, 8, $omisso, 1);
			//$pdf->MultiCell(85,8,$omisso,"TBLR",0,"C");

			$pdf->Ln();
			//$pdf->Cell(20,8,$r->situacao_cadastral,"TBLR",0,"C");
			
		}
		
		$pdf->Output('Relatorio Empresas Omissas EFD - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}

	public function convertStringDMA($string){
		$arrayPrincipal = explode(" 			",$string);

		$tabela = "";

		foreach ($arrayPrincipal as $linha) {
			if(strpos($linha, 'Sim') == false){
				$tabela = $tabela."".$linha."";
				$tabela = $tabela."\n";
			}
		}

		$tabela = $tabela."";

		return ''.$tabela.'';
	}

	public function convertStringEFD($string){
		$arrayPrincipal = explode(" 			",$string);

		$tabela = "";

		foreach ($arrayPrincipal as $linha) {
			if(strpos($linha, 'Sim') == false){
				$tabela = $tabela."".$linha."";
				$tabela = $tabela."\n";
			}
		}

		$tabela = $tabela."";

		return ''.$tabela.'';
	}

	
}