<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Processos extends MY_Controller {
	
	public function dashboard()
	{

		$dados = $this->notificacoes();

		$this->load->model('ecac_model');
		$this->load->model('consulta_sipro_model');
		$this->load->model('juceb_model');

		$dados['qtd_juceb'] = $this->juceb_model->qtd_listar_acompanhamento_abertos();
		$dados['qtd_juceb_finalizados'] = $this->juceb_model->qtd_listar_acompanhamento_finalizados();

		$dados['qtd_eprocessos'] = $this->ecac_model->qtd_eprocessos();
		$dados['qtd_eprocessos_inativos'] = $this->ecac_model->qtd_eprocessos_inativos();

		$dados['qtd_sipro'] = $this->consulta_sipro_model->qtd_sipro();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('processos/dashboard', $dados);
		$this->load->view('layout/footer');
		
	}

	
}