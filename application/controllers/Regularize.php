<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regularize extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
       	$this->load->model('regularize_model');
    }

	public function listar_fgts(){

		$dados = $this->notificacoes();
		
		$this->regularize_model->setFiltro('FGTS');
		$dados['consulta'] = $this->regularize_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('regularize/fgts', $dados);
		$this->load->view('layout/footer');
	} 

	public function listar_previdencia(){

		$dados = $this->notificacoes();
		
		$this->regularize_model->setFiltro('PREVIDENCIARIA');
		$dados['consulta'] = $this->regularize_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('regularize/previdenciaria', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_multa_trabalhista(){

		$dados = $this->notificacoes();
		
		$this->regularize_model->setFiltro('TRABALHISTA');
		$dados['consulta'] = $this->regularize_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('regularize/trabalhista', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_multa_eleitoral(){

		$dados = $this->notificacoes();
		
		$this->regularize_model->setFiltro('ELEITORAL');
		$dados['consulta'] = $this->regularize_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('regularize/eleitoral', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_multa_criminal(){

		$dados = $this->notificacoes();
		
		$this->regularize_model->setFiltro('CRIMINAL');
		$dados['consulta'] = $this->regularize_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('regularize/criminal', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_demais_debitos_tributarios(){

		$dados = $this->notificacoes();
		
		$this->regularize_model->setFiltro('TRIBUTARIOS');
		$dados['consulta'] = $this->regularize_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('regularize/debitos_tributarios', $dados);
		$this->load->view('layout/footer');
	}


	public function listar_demais_debitos_nao_tributarios(){

		$dados = $this->notificacoes();
		
		$this->regularize_model->setFiltro('NAO_TRIBUTARIO');
		$dados['consulta'] = $this->regularize_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('regularize/debitos_nao_tributarios', $dados);
		$this->load->view('layout/footer');
	}


	public function find_debitos_for_modal(){
		$id = $this->input->post('id');
		$tipo = $this->input->post('tipo');

		$this->regularize_model->setFiltro($tipo);
		$resultado = $this->regularize_model->detalhe($id);

		$total = 0;
		foreach ($resultado as $r) {
			$total = $total + $r->total;
		}

		$dados['resultado'] = $resultado;
		$dados['total'] = $total;

		echo json_encode($dados);
	}

}
