<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gerenciar_clientes extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('gerenciar_clientes_model');
		$this->load->model('email_clientes_model');
    }

    public function index()
	{  

		$dados = $this->notificacoes();

		$dados['consulta'] = $this->empresa_model->listar_para_gerenciamento();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('gerenciar_clientes/gerenciar_clientes_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function ativar_cliente(){
		
		$dados = $this->notificacoes();

		$id_empresa = $this->uri->segment(3);

		if(isset($_POST['btn_cadastrar'])){

			$this->gerenciar_clientes_model->setIdEmpresa($this->input->post("id_empresa")); 
			$this->gerenciar_clientes_model->setRazaoSocial($this->input->post("razao_social")); 
			$this->gerenciar_clientes_model->setNomeFantasia($this->input->post("nome_fantasia")); 
			$this->gerenciar_clientes_model->setCnpjEmpresa($this->input->post("cnpj_empresa")); 
			$this->gerenciar_clientes_model->setLogradouro($this->input->post("logradouro")); 
			$this->gerenciar_clientes_model->setNumero($this->input->post("numero")); 
			$this->gerenciar_clientes_model->setComplemento($this->input->post("complemento")); 
			$this->gerenciar_clientes_model->setBairro($this->input->post("bairro")); 
			$this->gerenciar_clientes_model->setCidade($this->input->post("cidade")); 
			$this->gerenciar_clientes_model->setUf($this->input->post("uf")); 
			$this->gerenciar_clientes_model->setCep($this->input->post("cep")); 
			$this->gerenciar_clientes_model->setTelefone($this->input->post("telefone")); 
			$this->gerenciar_clientes_model->setCelular($this->input->post("celular")); 
			$this->gerenciar_clientes_model->setLogo($this->input->post("logo")); 
			$this->gerenciar_clientes_model->setEmail($this->input->post("email")); 
			$this->gerenciar_clientes_model->setLogin($this->input->post("login")); 
			$this->gerenciar_clientes_model->setSenha(md5($this->input->post("senha"))); 
			$this->gerenciar_clientes_model->setStatus('A'); 
			$this->gerenciar_clientes_model->setNivel('Master'); 
			$this->gerenciar_clientes_model->setIdCadastro($this->session->userdata['userprimesession']['id']);			
			date_default_timezone_set('America/Bahia');
			$this->gerenciar_clientes_model->setDataCadastro(date('Y-m-d H:i:s'));

			$result = $this->gerenciar_clientes_model->ativar_cliente();
			
			// VERIFICAR O NOME DA BASE PARA CRIAR O ID EXTERNO ONE-SINGAL
	        $hostCompleto = $_SERVER['HTTP_HOST'];
	        $server = explode('.', $hostCompleto);
	        $server = $server[0];
	        // VERIFICAÇÃO CONCLUÍDA

	        $id_one_signal = $server.'-'.$result;

            $this->gerenciar_clientes_model->setIdOnesignal($id_one_signal);        
			$criar_id_one_singal = $this->gerenciar_clientes_model->criar_id_onesignal($result);

			if ($result){

				$this->session->set_flashdata('msg_alerta', 1);
				redirect('gerenciar_clientes');

			} else {

				$this->session->set_flashdata('msg_alerta', 2);
				redirect('gerenciar_clientes');
			}

		}

		$dados['cliente'] = $this->empresa_model->pesquisar_cliente_id($id_empresa);
		$dados['id'] = $id_empresa;
				
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('gerenciar_clientes/ativar_cliente');
		$this->load->view('layout/footer');
	}

	public function inativar_cliente(){

		$id = $this->uri->segment(3);

		$this->gerenciar_clientes_model->setId($id);

		$result = $this->gerenciar_clientes_model->inativar_cliente();

		if ($result){
		
			$this->session->set_flashdata('msg_alerta', 5);
			redirect('gerenciar_clientes');
				
		} else {

			$this->session->set_flashdata('msg_alerta', 6);
			redirect('gerenciar_clientes');
				
		} 
	}

	public function ativar_cliente_novamente(){

		$id = $this->uri->segment(3);

		$this->gerenciar_clientes_model->setId($id);

		$result = $this->gerenciar_clientes_model->ativar_cliente_novamente();

		if ($result){
		
			$this->session->set_flashdata('msg_alerta', 1);
			redirect('gerenciar_clientes');
				
		} else {

			$this->session->set_flashdata('msg_alerta', 2);
			redirect('gerenciar_clientes');
				
		} 
	}	

	public function editar_cliente(){

		$dados = $this->notificacoes();

		$id = $this->uri->segment(3);

		$this->gerenciar_clientes_model->setId($id);

		if(isset($_POST['btn_salvar'])){

			$this->gerenciar_clientes_model->setRazaoSocial($this->input->post("razao_social")); 
			$this->gerenciar_clientes_model->setNomeFantasia($this->input->post("nome_fantasia")); 
			$this->gerenciar_clientes_model->setCnpjEmpresa($this->input->post("cnpj_empresa")); 			
			$this->gerenciar_clientes_model->setLogradouro($this->input->post("logradouro")); 
			$this->gerenciar_clientes_model->setNumero($this->input->post("numero")); 
			$this->gerenciar_clientes_model->setComplemento($this->input->post("complemento")); 
			$this->gerenciar_clientes_model->setBairro($this->input->post("bairro")); 
			$this->gerenciar_clientes_model->setCidade($this->input->post("cidade")); 
			$this->gerenciar_clientes_model->setUf($this->input->post("uf")); 
			$this->gerenciar_clientes_model->setCep($this->input->post("cep")); 
			$this->gerenciar_clientes_model->setTelefone($this->input->post("telefone")); 
			$this->gerenciar_clientes_model->setCelular($this->input->post("celular")); 
			$this->gerenciar_clientes_model->setLogo($this->input->post("logo")); 
			$this->gerenciar_clientes_model->setEmail($this->input->post("email"));
			$this->gerenciar_clientes_model->setIdAlteracao($this->session->userdata['userprimesession']['id']);			
			date_default_timezone_set('America/Bahia');
			$this->gerenciar_clientes_model->setDataAlteracao(date('Y-m-d H:i:s'));

			$result = $this->gerenciar_clientes_model->editar_cliente();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 3);
				redirect("gerenciar_clientes");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 4);			
				redirect("gerenciar_clientes");
				
			}
		}

		$dados['cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id();
		$dados['id'] = $id; 

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('gerenciar_clientes/editar_cliente', $dados);
		$this->load->view('layout/footer');
	}

	public function alterar_login(){

		$dados = $this->notificacoes();

		$id = $this->uri->segment(3);

		$this->gerenciar_clientes_model->setId($id);

		if(isset($_POST['btn_salvar'])){

			$this->gerenciar_clientes_model->setLogin($this->input->post("login")); 
			$this->gerenciar_clientes_model->setSenha(md5($this->input->post("senha")));  
			$this->gerenciar_clientes_model->setIdAlteracao($this->session->userdata['userprimesession']['id']);			
			date_default_timezone_set('America/Bahia');
			$this->gerenciar_clientes_model->setDataAlteracao(date('Y-m-d H:i:s'));

			$result = $this->gerenciar_clientes_model->alterar_login();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 7);
				redirect("gerenciar_clientes");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 8);			
				redirect("gerenciar_clientes");
				
			}
		}

		$dados['cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id();
		$dados['id'] = $id; 

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('gerenciar_clientes/alterar_login_cliente', $dados);
		$this->load->view('layout/footer');
	}

	public function checklogin(){
		$login = $this->input->post("login");

		$resultado = $this->gerenciar_clientes_model->verificarLogin($login);
		
		if($resultado->valor > 0){
			echo json_encode(0);
		}else{
			echo json_encode(1);
		}

	}

	public function painel(){ 
		
		$id = $this->uri->segment(3);
		$dados['id_cliente'] = $id; 

		// NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id);
        // NOTIFICAÇÕES DO EMAIL

		$this->load->view('layout_clientes/head', $dados);
		$this->load->view('layout_clientes/sidebar');
		$this->load->view('layout_clientes/header');
		$this->load->view('painel_clientes/contador/dashboard');
		$this->load->view('layout_clientes/right_sidebar');
		$this->load->view('layout_clientes/footer');
	}

	public function painel_cliente(){

		$id = $this->uri->segment(3);
		$dados['id_cliente'] = $id; 

		// NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

		$this->load->view('layout_clientes/cliente/head', $dados);
		$this->load->view('layout_clientes/cliente/sidebar');
		$this->load->view('layout_clientes/cliente/header');
		$this->load->view('painel_clientes/cliente/dashboard');
		$this->load->view('layout_clientes/cliente/right_sidebar');
		$this->load->view('layout_clientes/cliente/footer');
	}
}