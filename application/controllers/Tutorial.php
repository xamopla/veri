<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tutorial extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('tutorial_model');
    }

    public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->tutorial_model->setCategoria($this->input->post("filtro_categoria"));

		// $this->tutorial_model->setLida($this->input->post("filtro_lida"));

		$dados['resultado'] = $this->tutorial_model->listar();

		$dados['categoria'] = $this->input->post("filtro_categoria");
		// $dados['lida'] = $this->input->post("filtro_lida");

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('suporte/tutorial', $dados);
		$this->load->view('layout/footer');
		
	}

}