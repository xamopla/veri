<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Nfe_consulta extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
        $this->load->model('nfe_destinatario_mdfe');

    }

    public function index(){
    	$notas_retorno = array();
    	$this->load->view('servicos/servicos_nfe', $notas_retorno);
    }
 
	public function consultar_sefaz_destinatario_ANTIGO(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id_empresa = $this->input->post('id_empresa');

		if ($this->session->userdata['userprimesession']['nivel'] == 1){
	        $nome_usuario = $this->session->userdata['userprimesession']['razao_social'];
      	} else {
	        $nome_usuario = $this->session->userdata['userprimesession']['nome']; 
        }  
        
		$dados_post = array(
			'login'=>$this->input->post('login'),
			'senha'=>$this->input->post('pass'),
			'cnpj'=>$this->input->post('cnpj'),
			'data_ini'=>$this->input->post('data_ini'),
			'data_fim'=>$this->input->post('data_fim'),
			'cookie_file'=> date('YmdHis').'.txt',
			'consultar_mdfe'=>$this->input->post('consultar_mdfe'),
			'user_report'=> $nome_usuario /* <<---- NOME DO USUÁRIO <----- */
		);

		$this->load->model('Nfe_consulta_destinatario_model');
		$notas_sefaz = array();

		#-----------------------------------------------------------------------------
		# ANTES DE CHAMAR SEFAZ, DEVEMOS CONSULTA NO BANCO SE ESSE PERÍODO E EMPRESA
		# SE ENCONTRA NA BASE DE DADOS
		#-----------------------------------------------------------------------------

		//data_ini em var Date
		$date_start = $this->input->post('data_ini');
		$date_start = implode('-', array_reverse(explode('/', substr($date_start, 0, 10)))).substr($date_start, 10);
		$date_start = new DateTime($date_start);

		//data_fim em var Date
		$date_end = $this->input->post('data_fim');
		$date_end = implode('-', array_reverse(explode('/', substr($date_end, 0, 10)))).substr($date_end, 10);
		$date_end = new DateTime($date_end);

		//Criando um range de datas entre data_ini e data_fim
		$date_range = array();
		while($date_start <= $date_end){
			$date_range[] = $date_start->format('Ymd');
			$date_start = $date_start->modify('+1day');
		}
        foreach ($date_range as $date) {
			$resultados_get_destinatario_bd = $this->Nfe_consulta_destinatario_model->get_nfe_destinatario($dados_post["cnpj"].$date);

			if(count($resultados_get_destinatario_bd) > 0){

				if($resultados_get_destinatario_bd[0]["consultou_mdfe"] == "S" || ($resultados_get_destinatario_bd[0]["consultou_mdfe"] != "S" && $dados_post["consultar_mdfe"] != "true")){
					// A consulta já possui MDF-e Consultado, logo é completa ou o usuário não quer uma consulta com mdf-e

					//Período foi encontrado no BD
					foreach ($resultados_get_destinatario_bd as $resultado_get_destinatario_bd) {

						$notas_sefaz = array_merge($notas_sefaz, array($resultado_get_destinatario_bd["chave_nfe"] => array(
								'doc_sefaz'=>$resultado_get_destinatario_bd["doc_sefaz"],
								'CNPJ/CPF'=>$resultado_get_destinatario_bd["cnpj_emit"],
								'razao_social_dest'=>$resultado_get_destinatario_bd["razao_emit"],
								'emissao'=>$resultado_get_destinatario_bd["data_emissao"],
								'valor'=>$resultado_get_destinatario_bd["valor"],
								'chave_acesso'=>$resultado_get_destinatario_bd["chave_nfe"],
								'situacao'=>$resultado_get_destinatario_bd["situacao_nfe"],
								'tipo_operacao'=>$resultado_get_destinatario_bd["tipo_operacao"],
								'uf'=>$resultado_get_destinatario_bd["uf"],
								'mdf_evento'=>$resultado_get_destinatario_bd["mdf_evento"],
								'mdf_data'=>$resultado_get_destinatario_bd["mdf_data"],
								'user_report'=>$resultado_get_destinatario_bd["user_report"],
								'data_report'=>$resultado_get_destinatario_bd["data_report"],
								'cidade_destinatario'=>$resultado_get_destinatario_bd["cidade_destinatario"],
								'cidade_emitente'=>$resultado_get_destinatario_bd["cidade_emitente"]
							))
						);
					}

				}else{

					//Período foi encontrado no banco, porém, a consulta está sem mdf-e e o cliente está requerindo o mdf-e
					//Logo devemos dropar todos os dados da tabela que já possue e fazer uma nova requisição
					$this->Nfe_consulta_destinatario_model->delete_consulta($dados_post["cnpj"].$date);

					$data_pesquisa = new DateTime($date);
					$dados_post["data_ini"] = $data_pesquisa->format('d/m/Y');
					$dados_post["data_fim"] = $data_pesquisa->format('d/m/Y');
					$notas_resultado = $this->Nfe_consulta_destinatario_model->main($dados_post);
					$notas_sefaz = array_merge($notas_sefaz, $notas_resultado);

					foreach ($notas_resultado as $nota_resultado) {
						$nota_resultado = array_merge($nota_resultado, array("cnpj_data"=>$dados_post["cnpj"].$date, "user_report" => $dados_post["user_report"], "data_report"=> date("d/m/Y"), "consultou_mdfe"=>"S"));
						$this->Nfe_consulta_destinatario_model->insert_nfe_destinatario($nota_resultado);
					}
				}

			}else{
				//Período não foi encontrado no BD
				//Fazendo requisição Sefaz e depois salvando no BD
				$data_pesquisa = new DateTime($date);
				$dados_post["data_ini"] = $data_pesquisa->format('d/m/Y');
				$dados_post["data_fim"] = $data_pesquisa->format('d/m/Y');
				$notas_resultado = $this->Nfe_consulta_destinatario_model->main($dados_post);
				$notas_sefaz = array_merge($notas_sefaz, $notas_resultado);

				foreach ($notas_resultado as $nota_resultado) {
					$nota_resultado = array_merge($nota_resultado, array("cnpj_data"=>$dados_post["cnpj"].$date, "user_report" => $dados_post["user_report"], "data_report"=> date("d/m/Y")));
					if($dados_post["consultar_mdfe"] == "true"){
						$nota_resultado = array_merge($nota_resultado, (array("consultou_mdfe"=>"S")));
					}else{
						$nota_resultado = array_merge($nota_resultado, (array("consultou_mdfe"=>"N")));
					}
					$this->Nfe_consulta_destinatario_model->insert_nfe_destinatario($nota_resultado);
				}				
			}

		}

		#-----------------------------------------------------------------------------
		#-----------------------------------------------------------------------------

		@unlink($dados_post['cookie_file']);

		$notas_retorno = array();
		$data_notas_retorno = array();
		$i = 0;

		if(strlen($_FILES['csv_notas']['name'])>0){
			$notas_cliente = ($this->Nfe_consulta_destinatario_model->ler_csv_cliente($_FILES['csv_notas']));
			foreach ($notas_cliente as $nota_cliente) {
				if (array_key_exists($nota_cliente, $notas_sefaz)) {
	    			//NOTA ENCONTRA NAS NOTAS RETORNADAS DA SEFAZ CSV-> SEFAZ
	    			array_push(
	    				$notas_retorno, 
	    				array_merge(
	    					array(
	    						"doc"=>$nota_cliente,
	    						"status"=>"found_sefaz_csv"
	    					),
	    					$notas_sefaz[$nota_cliente]
	    				)
	    			);
	    			//ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
	    			array_push($data_notas_retorno, $this->getDateISO($notas_sefaz[$nota_cliente]['emissao']).str_pad($i, 3, "0", STR_PAD_LEFT));$i++;

	    			//REMOVENDO ESSA NOTA DA RELAÇÃO DAS NOTAS SEFAZ
	    			unset($notas_sefaz[$nota_cliente]);
				}else{
					//NOTA NÃO FOI ENCONTRADA CSV -> SEFAZ
					array_push(
	    				$notas_retorno,     				
	    				array(
	    					"doc"=>$nota_cliente,
	    					"status"=>"not_found_sefaz"
	    				)
	    			);
	    			//ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
	    			array_push($data_notas_retorno, ('00000000').str_pad($i, 3, "0", STR_PAD_LEFT));$i++;
				}
			}
		}

		foreach ($notas_sefaz as $nota_sefaz) {
			array_push(
				$notas_retorno,
				array_merge(
					$nota_sefaz,
					array(
						"status"=>"not_found_csv"
					)
				)
			);
			//ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
    		array_push($data_notas_retorno, $this->getDateISO($nota_sefaz['emissao']).str_pad($i, 3, "0", STR_PAD_LEFT));$i++;
		}

		$notas_retorno = array_combine($data_notas_retorno, $notas_retorno);
		ksort($notas_retorno);	
		$notas_retorno = array("notas_destinatario"=>$notas_retorno);

		
		$this->empresa_model->setId($id_empresa);

		$notas_retorno['empresa'] = $this->empresa_model->pesquisar_empresa_id();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/servicos_nfe', $notas_retorno);
		$this->load->view('layout/footer');
	}

	public function nota_destinatario(){

		//Recebendo os dados do login
		$dados_post = array(
			'login'=>$this->input->post('login'),
			'senha'=>$this->input->post('pass'),
			'chave_nfe'=>$this->input->post('chave_nfe'),
			'cookie_file'=> date('YmdHis').'.txt',
			'retornar_consulta'=> true,

		);

		$this->load->model('Nfe_consulta_destinatario_model');
        $this->Nfe_consulta_destinatario_model->retorna_nfe2($dados_post);
	}

    public function consultar_sefaz_emitente(){

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        $id_empresa = $this->input->post('id_empresa');

        if ($this->session->userdata['userprimesession']['nivel'] == 1){
            $nome_usuario = $this->session->userdata['userprimesession']['razao_social'];
        } else {
            $nome_usuario = $this->session->userdata['userprimesession']['nome'];
        }

        $params = array(
            'usuario' => $this->input->post('login') ,
            'senha' => $this->input->post('pass'),
            'cookie_path' => date('YmdHis').'.txt',
            'data_inicial' => $this->input->post('data_ini'),
            'data_final' => $this->input->post('data_fim'),
            'user_report' => $nome_usuario,
            'data_report' => date('d/m/Y')
        );

        $this->load->library('Nfe_emitente_library', $params);

        $notas_sefaz = $this->nfe_emitente_library->obter_notas_planilha();
        $notas_retorno = array();
        $data_notas_retorno = array();
        $i = 0;

        if(isset($_FILES['csv_notas']) && strlen($_FILES['csv_notas']['name'])>0){
            $notas_cliente = ($this->Nfe_consulta_emitente_teste_model->ler_csv_cliente($_FILES['csv_notas']));
            foreach ($notas_cliente as $nota_cliente) {
                if (array_key_exists($nota_cliente, $notas_sefaz)) {
                    //NOTA ENCONTRA NAS NOTAS RETORNADAS DA SEFAZ CSV-> SEFAZ
                    array_push(
                        $notas_retorno,
                        array_merge(
                            array(
                                "doc"=>$nota_cliente,
                                "status"=>"found_sefaz_csv"
                            ),
                            $notas_sefaz[$nota_cliente]
                        )
                    );
                    //ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
                    array_push($data_notas_retorno, $this->getDateISO($notas_sefaz[$nota_cliente]['emissao']).str_pad($i, 3, "0", STR_PAD_LEFT));$i++;

                    //REMOVENDO ESSA NOTA DA RELAÇÃO DAS NOTAS SEFAZ
                    unset($notas_sefaz[$nota_cliente]);
                }else{
                    //NOTA NÃO FOI ENCONTRADA CSV -> SEFAZ
                    array_push(
                        $notas_retorno,
                        array(
                            "doc"=>$nota_cliente,
                            "status"=>"not_found_sefaz"
                        )
                    );
                    //ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
                    array_push($data_notas_retorno, ('00000000').str_pad($i, 3, "0", STR_PAD_LEFT));$i++;
                }
            }
        }

        foreach ($notas_sefaz as $nota_sefaz) {
            array_push(
                $notas_retorno,
                array_merge(
                    $nota_sefaz,
                    array(
                        "status"=>"not_found_csv"
                    )
                )
            );
            //ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
            array_push($data_notas_retorno, $this->getDateISO($nota_sefaz['emissao']).str_pad($i, 3, "0", STR_PAD_LEFT));$i++;
        }

        $notas_retorno = array_combine($data_notas_retorno, $notas_retorno);
        ksort($notas_retorno);
        $notas_retorno = array("notas_emitentes"=>$notas_retorno);

        $this->empresa_model->setId($id_empresa);

        $notas_retorno['empresa'] = $this->empresa_model->pesquisar_empresa_id();

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('servicos/servicos_nfe', $notas_retorno);
        $this->load->view('layout/footer');
        unset($this->nfe_emitente_library);
    }

	public function getDateISO($data){
		return substr($data, 6, 4).substr($data, 3, 2).substr($data, 0, 2);
	}

    public function processar_mdfe(){

        $login = $this->input->post('login');
        $pass = $this->input->post('pass');
        $link = $this->input->post('link');

        $params = array(
            'usuario' =>  $login,
            'senha' => $pass,
            'cookie_path' => date('YmdHis').'.txt',
        );
        $this->load->library('Nfe_destinatario_library', $params);
        $dados_mdfe = $this->nfe_destinatario_library->consulta_mdfe( $link );
        $url = parse_url($link);
        $cod_chave_acesso_string = explode("&",$url['query'])[0];
        $chave = $this->apenas_numero($cod_chave_acesso_string);
        $this->nfe_destinatario_mdfe->atualizar_mdfe($dados_mdfe, $chave);

        echo json_encode($dados_mdfe);
        unset($this->nfe_destinatario_library);
    }

    public function processar_mdfe_todas(){

        $login = $this->input->post('login');
        $pass = $this->input->post('pass');
        $links = $this->input->post('links');
        $array_links = explode(';', $links);
        $params = array(
            'usuario' =>  $login,
            'senha' => $pass,
            'cookie_path' => date('YmdHis').'.txt',
        );
        $this->load->library('Nfe_destinatario_library', $params);
        $dados = array();
        foreach ($array_links as $link){
            if($link != ""){
                $url = parse_url($link);
                $cod_chave_acesso_string = explode("&",$url['query'])[0];
                $chave = $this->apenas_numero($cod_chave_acesso_string);
                if( $this->nfe_destinatario_mdfe->precisa_processar($chave) ){
                    $dados_mdfe = $this->nfe_destinatario_library->consulta_mdfe( $link );
                    $this->nfe_destinatario_mdfe->atualizar_mdfe($dados_mdfe, $chave);
                    $dados[] = $dados_mdfe;
                }
            }
        }
        echo json_encode($dados);
        unset($this->nfe_destinatario_library);
    }

    public function consultar_sefaz_destinatario(){

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        $id_empresa = $this->input->post('id_empresa');

        if ($this->session->userdata['userprimesession']['nivel'] == 1){
            $nome_usuario = $this->session->userdata['userprimesession']['razao_social'];
        } else {
            $nome_usuario = $this->session->userdata['userprimesession']['nome'];
        }

        $params = array(
            'usuario' => $this->input->post('login') ,
            'senha' => $this->input->post('pass'),
            'cookie_path' => date('YmdHis').'.txt',
            'data_inicial' => $this->input->post('data_ini'),
            'data_final' => $this->input->post('data_fim'),
            'user_report' => $nome_usuario,
            'data_report' => date('d/m/Y')
        );

        $this->load->library('Nfe_destinatario_library', $params);

        $notas_sefaz = $this->nfe_destinatario_library->obter_notas_planilha();

        $notas_retorno = array();

        $data_notas_retorno = array();

        $i = 0;

        if(isset($_FILES['csv_notas']) && isset($_FILES['csv_notas']['name']) && strlen($_FILES['csv_notas']['name'])>0){
            $notas_cliente = ($this->Nfe_consulta_destinatario_model->ler_csv_cliente($_FILES['csv_notas']));
            foreach ($notas_cliente as $nota_cliente) {
                if (array_key_exists($nota_cliente, $notas_sefaz)) {
                    //NOTA ENCONTRA NAS NOTAS RETORNADAS DA SEFAZ CSV-> SEFAZ
                    array_push(
                        $notas_retorno,
                        array_merge(
                            array(
                                "doc"=>$nota_cliente,
                                "status"=>"found_sefaz_csv"
                            ),
                            $notas_sefaz[$nota_cliente]
                        )
                    );
                    //ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
                    array_push($data_notas_retorno, $this->getDateISO($notas_sefaz[$nota_cliente]['emissao']).str_pad($i, 3, "0", STR_PAD_LEFT));$i++;

                    //REMOVENDO ESSA NOTA DA RELAÇÃO DAS NOTAS SEFAZ
                    unset($notas_sefaz[$nota_cliente]);
                }else{
                    //NOTA NÃO FOI ENCONTRADA CSV -> SEFAZ
                    array_push(
                        $notas_retorno,
                        array(
                            "doc"=>$nota_cliente,
                            "status"=>"not_found_sefaz"
                        )
                    );
                    //ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
                    array_push($data_notas_retorno, ('00000000').str_pad($i, 3, "0", STR_PAD_LEFT));$i++;
                }
            }
        }

        foreach ($notas_sefaz as $nota_sefaz) {
            $registro_encontrado = $this->nfe_destinatario_mdfe->inserir_se_nao_existe( $nota_sefaz['chave_acesso'] );
            $nota_sefaz['mdf_status'] = '';
            $nota_sefaz['mdf_data'] = '';
            if($registro_encontrado && isset($registro_encontrado->status)){
                $nota_sefaz['mdf_status'] = $registro_encontrado->status;
                $nota_sefaz['mdf_data'] = $registro_encontrado->mdf_data;
            }
            array_push(
                $notas_retorno,
                array_merge(
                    $nota_sefaz,
                    array(
                        "status"=>"not_found_csv"
                    )
                )
            );
            //ADICIONANDO A DATA EM UM ARRAY PARA ORDENAR
            array_push($data_notas_retorno, $this->getDateISO($nota_sefaz['emissao']).str_pad($i, 3, "0", STR_PAD_LEFT));$i++;
        }

        $notas_retorno = array_combine($data_notas_retorno, $notas_retorno);
        ksort($notas_retorno);
        $notas_retorno = array("notas_destinatario"=>$notas_retorno);

        $this->empresa_model->setId($id_empresa);

        $notas_retorno['empresa'] = $this->empresa_model->pesquisar_empresa_id();

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('servicos/servicos_nfe', $notas_retorno);
        $this->load->view('layout/footer');
    }

    public function apenas_numero($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }

}
