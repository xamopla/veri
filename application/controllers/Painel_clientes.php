<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Painel_clientes extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
        $this->load->model('email_clientes_model');
        $this->load->model('gerenciar_clientes_model');
        $this->load->model('anexo_email_model');
        $this->load->model('ipva_clientes_model');
        $this->load->model('documentos_clientes_model');
    }

    // ----------------------------- CONTADOR ----------------------------------

    public function listar_email_contador(){

    	$id = $this->uri->segment(3); 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_contador($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_contador($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_contador($id);
        $dados['consulta'] = $this->email_clientes_model->listar_emails_contador($id);
		$dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'nao';

        // NOTIFICAÇÕES DO EMAIL
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id);
        // NOTIFICAÇÕES DO EMAIL

		$this->load->view('layout_clientes/head', $dados);
		$this->load->view('layout_clientes/sidebar');
		$this->load->view('layout_clientes/header');
		$this->load->view('painel_clientes/contador/email_listar', $dados);
		$this->load->view('layout_clientes/right_sidebar');
		$this->load->view('layout_clientes/footer');
    }

    public function listar_email_com_estrela_contador(){

        $id = $this->uri->segment(3); 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_contador($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_contador($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_contador($id);
        $dados['consulta'] = $this->email_clientes_model->listar_emails_com_estrela_contador($id);
        $dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'nao';

        // NOTIFICAÇÕES DO EMAIL
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/head', $dados);
        $this->load->view('layout_clientes/sidebar');
        $this->load->view('layout_clientes/header');
        $this->load->view('painel_clientes/contador/email_listar', $dados);
        $this->load->view('layout_clientes/right_sidebar');
        $this->load->view('layout_clientes/footer');
    }

    public function listar_email_enviados_contador(){

        $id = $this->uri->segment(3); 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_contador($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_contador($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_contador($id);
        $dados['consulta'] = $this->email_clientes_model->listar_email_enviados_contador($id);
        $dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'sim';

        // NOTIFICAÇÕES DO EMAIL
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/head', $dados);
        $this->load->view('layout_clientes/sidebar');
        $this->load->view('layout_clientes/header');
        $this->load->view('painel_clientes/contador/email_listar', $dados);
        $this->load->view('layout_clientes/right_sidebar');
        $this->load->view('layout_clientes/footer');
    }

    public function listar_email_excluidos_contador(){

        $id = $this->uri->segment(3); 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_contador($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_contador($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_contador($id);
        $dados['consulta'] = $this->email_clientes_model->listar_email_excluidos_contador($id);
        $dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'sim';

        // NOTIFICAÇÕES DO EMAIL
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/head', $dados);
        $this->load->view('layout_clientes/sidebar');
        $this->load->view('layout_clientes/header');
        $this->load->view('painel_clientes/contador/email_listar', $dados);
        $this->load->view('layout_clientes/right_sidebar');
        $this->load->view('layout_clientes/footer');
    }

    public function enviar_email_contador(){ 

        if(isset($_POST['btn_enviar'])){

            $id_cliente = $this->input->post("id_cliente");            
            $id_onesignal = $this->input->post("id_onesignal");
            $this->email_clientes_model->setIdEmpresa($id_cliente); 
            $this->email_clientes_model->setEmailDestinatario($this->input->post("email_destinatario")); 
            $this->email_clientes_model->setAssunto($this->input->post("assunto")); 
            $this->email_clientes_model->setMensagem($this->input->post("mensagem"));  
            $this->email_clientes_model->setIdCadastro($this->session->userdata['userprimesession']['id']);            
            date_default_timezone_set('America/Bahia');
            $this->email_clientes_model->setDataCadastro(date('Y-m-d H:i:s')); 

            $countfiles = count($_FILES['file']['name']);

            $abc = date('Y-m-d H:i:s');
            $data_hora = strtolower( preg_replace("/[^a-zA-Z0-9-]/", "-", strtr(utf8_decode(trim($abc)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) );
            
            // VETIFICA SE ANEXOU ALGUM ARQUIVO
            if ($countfiles > 0 && $_FILES['file']['name'][0] != null && $_FILES['file']['name'][0] != "" ) { 

                $this->email_clientes_model->setPossuiAnexo(1);  

                $result = $this->email_clientes_model->enviar_email_contador();  

                // -------------------- ANEXO DE ARQUIVOS -----------------------

                if (!empty($_FILES)){

                    // VERIFICAR O NOME DA BASE
                    $hostCompleto = $_SERVER['HTTP_HOST'];
                    $server = explode('.', $hostCompleto);
                    $server = $server[0];
                    // VERIFICAÇÃO CONCLUÍDA
                    
                    $targetPath = 'assets_clientes/arquivos/empresas/emails/'.$server.'/'.$id_cliente.'/';

                    if (!file_exists($targetPath)) {
                        mkdir($targetPath, DIR_WRITE_MODE, true);
                    }

                    // Looping all files
                    for($i=0;$i<$countfiles;$i++){

                        $tempFile = $_FILES['file']['tmp_name'][$i]; 

                        $targetFile = $targetPath . $data_hora.$_FILES['file']['name'][$i];
                        $fileName = $data_hora.$_FILES['file']['name'][$i];

                        $downloadPath = $targetFile;
                        $data['urlDownload'] = base_url($downloadPath);

                        move_uploaded_file($tempFile, $targetFile);

                        // CADASTRO DOS ANEXOS
                        $this->anexo_email_model->setAnexo($targetFile);
                        $this->anexo_email_model->setAnexoPath($data['urlDownload']);  
                        $this->anexo_email_model->setNomeAnexo($fileName);
                        $this->anexo_email_model->setIdEmail($result);
                        $this->anexo_email_model->setIdCadastro($this->session->userdata['userprimesession']['id']);         
                        date_default_timezone_set('America/Sao_Paulo');
                        $this->anexo_email_model->setDataCadastro(date('Y-m-d H:i:s'));

                        $cadastro_anexo = $this->anexo_email_model->cadastrar();

                        //$filename = $_FILES['file']['name'][$i];
                       
                       // Upload file
                       //move_uploaded_file($_FILES['file']['tmp_name'][$i],'upload/'.$filename);
                        
                    }

                } else {

                    $data= array('error' => $this->upload->display_errors());

                    $this->session->set_flashdata('msg_alerta', 9);
                    $this->session->set_flashdata('msg_conteudo', $data['error']);

                    redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
                }

                // --------------------- FIM DO ANEXO DE ARQUIVOS --------------------------------

            } else {

                $result = $this->email_clientes_model->enviar_email_contador();  
                $mensagem_onesignal = "Novo email recebido.";
                $this->sendMessage($mensagem_onesignal,$id_onesignal);
            }

            if ($result){
                
                $this->session->set_flashdata('msg_alerta', 1);
                redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
                
            } else {
                
                $this->session->set_flashdata('msg_alerta', 2);         
                redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
                
            }
        }

        // ini_set('display_errors', 1);

        // error_reporting(E_ALL);

        // $from = "suporte@sistemaveri.com.br";

        // $to = "dhiego.samorais@gmail.com";

        // $subject = "Verificando o correio do PHP";

        // $message = "O correio do PHP funciona bem";

        // $headers =  'MIME-Version: 1.0' . "\r\n"; 
        // $headers .= 'From: Your name <suporte@sistemaveri.com.br>' . "\r\n";
        // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

        // mail($to, $subject, $message, $headers);

        // echo "A mensagem de e-mail foi enviada."; 
    }

    public function ler_email_contador(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 
        
        $id_cliente = $this->uri->segment(4);

        $result = $this->email_clientes_model->ler_email_contador();

        $dados['email'] = $this->email_clientes_model->pesquisar_email_id();
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id_cliente);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_contador($id_cliente);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_contador($id_cliente);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_contador($id_cliente);
        $dados['id_cliente'] = $id_cliente; 
        $dados['id'] = $id;

        $dados['qtd_anexos'] = $this->anexo_email_model->qtd_anexos_email_contador($id);
        $dados['lista_anexos'] = $this->anexo_email_model->listar_anexos($id);
        
        // NOTIFICAÇÕES DO EMAIL
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id_cliente);
        // NOTIFICAÇÕES DO EMAIL

        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA

        $dados['host'] = $server;

        $this->load->view('layout_clientes/head', $dados);
        $this->load->view('layout_clientes/sidebar');
        $this->load->view('layout_clientes/header');
        $this->load->view('painel_clientes/contador/email_ler', $dados);
        $this->load->view('layout_clientes/right_sidebar');
        $this->load->view('layout_clientes/footer');
    }

    public function visualizar_email_contador(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 
        
        $id_cliente = $this->uri->segment(4); 

        $dados['email'] = $this->email_clientes_model->pesquisar_email_id();
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id_cliente);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_contador($id_cliente);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_contador($id_cliente);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_contador($id_cliente);
        $dados['id_cliente'] = $id_cliente; 
        $dados['id'] = $id;
        
        $dados['qtd_anexos'] = $this->anexo_email_model->qtd_anexos_email_contador($id);
        $dados['lista_anexos'] = $this->anexo_email_model->listar_anexos($id);
        
        // NOTIFICAÇÕES DO EMAIL
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id_cliente);
        // NOTIFICAÇÕES DO EMAIL

        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA

        $dados['host'] = $server;

        $this->load->view('layout_clientes/head', $dados);
        $this->load->view('layout_clientes/sidebar');
        $this->load->view('layout_clientes/header');
        $this->load->view('painel_clientes/contador/email_visualizar', $dados);
        $this->load->view('layout_clientes/right_sidebar');
        $this->load->view('layout_clientes/footer');
    }

    public function apagar_email_contador(){

        $id = $this->uri->segment(3);

        $this->email_clientes_model->setId($id);

        $id_cliente = $this->uri->segment(4);

        $result = $this->email_clientes_model->excluir_email_contador();

        if ($result){
        
            $this->session->set_flashdata('msg_alerta', 7);
            redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
                
        } else {

            $this->session->set_flashdata('msg_alerta', 8);
            redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
                
        }
    }

    public function atribuir_estrela_contador(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 

        $id_cliente = $this->uri->segment(4);

        $result = $this->email_clientes_model->definir_email_estrela_contador();

        if ($result){
            
            $this->session->set_flashdata('msg_alerta', 3);
            redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
            
        } else {
            
            $this->session->set_flashdata('msg_alerta', 4);         
            redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
            
        }
    }

    public function remover_estrela_contador(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 

        $id_cliente = $this->uri->segment(4);

        $result = $this->email_clientes_model->remover_estrela_contador();

        if ($result){
            
            $this->session->set_flashdata('msg_alerta', 5);
            redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
            
        } else {
            
            $this->session->set_flashdata('msg_alerta', 6);         
            redirect('painel_clientes/listar_email_contador/'.$id_cliente.'');
            
        }
    }

    public function listar_ipva_contador(){

        $id = $this->uri->segment(3);  
        $id_empresa = $this->ipva_clientes_model->find_id($id)->id;

        $resultado = $this->ipva_clientes_model->listar_ipva_clientes($id_empresa);

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id);
        // NOTIFICAÇÕES DO EMAIL

        $placas = array();

        foreach ($resultado as $item) {
            if ($a = explode("|", $item->placas)) {
                foreach ($a as $s) {
                    array_push($placas,$s);
                }
            }
        }

        $dados['lista_ipva'] = $placas;
        $dados['dados_envio'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['id_cliente'] = $id;  

        $this->load->view('layout_clientes/head', $dados);
        $this->load->view('layout_clientes/sidebar');
        $this->load->view('layout_clientes/header');
        $this->load->view('painel_clientes/contador/ipva_listar', $dados);
        $this->load->view('layout_clientes/right_sidebar');
        $this->load->view('layout_clientes/footer');
    }

    public function enviar_notificacao_ipva_email(){

        if(isset($_POST['btn_enviar'])){

            $id_cliente = $this->input->post("id_cliente");
            $id_onesignal = $this->input->post("id_onesignal");
            $this->email_clientes_model->setIdEmpresa($id_cliente); 
            $this->email_clientes_model->setEmailDestinatario($this->input->post("email_destinatario")); 
            $this->email_clientes_model->setAssunto($this->input->post("assunto")); 
            $this->email_clientes_model->setMensagem($this->input->post("mensagem"));  
            $this->email_clientes_model->setIdCadastro($this->session->userdata['userprimesession']['id']);            
            date_default_timezone_set('America/Bahia');
            $this->email_clientes_model->setDataCadastro(date('Y-m-d H:i:s')); 

            $result = $this->email_clientes_model->enviar_email_contador();  

            if ($result){
                
                // DISPARAR MENSAGEM NO CELULAR
                $mensagem_onesignal = "Notificação de Débito de IPVA.";
                $this->sendMessage($mensagem_onesignal,$id_onesignal);

                $this->session->set_flashdata('msg_alerta', 1);
                redirect('painel_clientes/listar_ipva_contador/'.$id_cliente.'');
                
            } else {
                
                $this->session->set_flashdata('msg_alerta', 2);         
                redirect('painel_clientes/listar_ipva_contador/'.$id_cliente.'');
                
            }

        }

        // ini_set('display_errors', 1);

        // error_reporting(E_ALL);

        // $from = "suporte@sistemaveri.com.br";

        // $to = "dhiego.samorais@gmail.com";

        // $subject = "Verificando o correio do PHP";

        // $message = "O correio do PHP funciona bem";

        // $headers =  'MIME-Version: 1.0' . "\r\n"; 
        // $headers .= 'From: Your name <suporte@sistemaveri.com.br>' . "\r\n";
        // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

        // mail($to, $subject, $message, $headers);

        // echo "A mensagem de e-mail foi enviada."; 
    }

    public function listar_documentos_contador(){

        $id = $this->uri->segment(3);  
        $id_empresa = $this->documentos_clientes_model->find_id($id)->id;

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_contador($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_contador($id);
        // NOTIFICAÇÕES DO EMAIL

        $dados['lista_documentos'] = $this->documentos_clientes_model->listar_documentos_contador($id_empresa);
        $dados['dados_envio'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['id_cliente'] = $id;  

        $this->load->view('layout_clientes/head', $dados);
        $this->load->view('layout_clientes/sidebar');
        $this->load->view('layout_clientes/header');
        $this->load->view('painel_clientes/contador/documentos_listar', $dados);
        $this->load->view('layout_clientes/right_sidebar');
        $this->load->view('layout_clientes/footer');
    }

    // ----------------------------------------------- CLIENTE ---------------------------------------------------------
    // ----------------------------------------------- CLIENTE ---------------------------------------------------------
    // ----------------------------------------------- CLIENTE ---------------------------------------------------------
    // ----------------------------------------------- CLIENTE ---------------------------------------------------------
    // ----------------------------------------------- CLIENTE ---------------------------------------------------------

    public function listar_email_cliente(){

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_cliente($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_cliente($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_cliente($id);
        $dados['consulta'] = $this->email_clientes_model->listar_emails_cliente($id);
        $dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'nao';

        // NOTIFICAÇÕES DO EMAIL 
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/email_listar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function listar_email_com_estrela_cliente(){

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_cliente($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_cliente($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_cliente($id);
        $dados['consulta'] = $this->email_clientes_model->listar_emails_com_estrela_cliente($id);
        $dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'nao';

        // NOTIFICAÇÕES DO EMAIL 
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/email_listar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function listar_email_enviados_cliente(){

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_cliente($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_cliente($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_cliente($id);
        $dados['consulta'] = $this->email_clientes_model->listar_email_enviados_cliente($id);
        $dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'sim';

        // NOTIFICAÇÕES DO EMAIL 
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/email_listar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function listar_email_excluidos_cliente(){

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO 

        $dados['dados_cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_cliente($id);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_cliente($id);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_cliente($id);
        $dados['consulta'] = $this->email_clientes_model->listar_email_excluidos_cliente($id);
        $dados['id_cliente'] = $id; 
        $dados['proibir_ler'] = 'sim';

        // NOTIFICAÇÕES DO EMAIL 
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/email_listar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function enviar_email_cliente(){ 

        if(isset($_POST['btn_enviar'])){

            $id_cliente = $this->input->post("id_cliente");
            $this->email_clientes_model->setIdEmpresa($id_cliente); 
            $this->email_clientes_model->setEmailDestinatario($this->input->post("email_destinatario")); 
            $this->email_clientes_model->setAssunto($this->input->post("assunto")); 
            $this->email_clientes_model->setMensagem($this->input->post("mensagem"));  
            $this->email_clientes_model->setIdCadastro($this->session->userdata['userprimesession_cliente']['id']);            
            date_default_timezone_set('America/Bahia');
            $this->email_clientes_model->setDataCadastro(date('Y-m-d H:i:s')); 

            $countfiles = count($_FILES['file']['name']);

            $abc = date('Y-m-d H:i:s');
            $data_hora = strtolower( preg_replace("/[^a-zA-Z0-9-]/", "-", strtr(utf8_decode(trim($abc)), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) );
             

            // VETIFICA SE ANEXOU ALGUM ARQUIVO
            if ($countfiles > 0 && $_FILES['file']['name'][0] != null && $_FILES['file']['name'][0] != "" ) { 

                $this->email_clientes_model->setPossuiAnexo(1);  

                $result = $this->email_clientes_model->enviar_email_cliente();  

                // -------------------- ANEXO DE ARQUIVOS -----------------------

                if (!empty($_FILES)){

                    // VERIFICAR O NOME DA BASE
                    $hostCompleto = $_SERVER['HTTP_HOST'];
                    $server = explode('.', $hostCompleto);
                    $server = $server[0];
                    // VERIFICAÇÃO CONCLUÍDA
                    
                    $targetPath = 'assets_clientes/arquivos/empresas/emails/'.$server.'/'.$id_cliente.'/';

                    if (!file_exists($targetPath)) {
                        mkdir($targetPath, DIR_WRITE_MODE, true);
                    }

                    // Looping all files
                    for($i=0;$i<$countfiles;$i++){

                        $tempFile = $_FILES['file']['tmp_name'][$i]; 

                        $targetFile = $targetPath . $data_hora.$_FILES['file']['name'][$i];
                        $fileName = $data_hora.$_FILES['file']['name'][$i];

                        $downloadPath = $targetFile;
                        $data['urlDownload'] = base_url($downloadPath);

                        move_uploaded_file($tempFile, $targetFile);

                        // CADASTRO DOS ANEXOS
                        $this->anexo_email_model->setAnexo($targetFile);
                        $this->anexo_email_model->setAnexoPath($data['urlDownload']);  
                        $this->anexo_email_model->setNomeAnexo($fileName);
                        $this->anexo_email_model->setIdEmail($result);
                        $this->anexo_email_model->setIdCadastro($this->session->userdata['userprimesession_cliente']['id']);         
                        date_default_timezone_set('America/Sao_Paulo');
                        $this->anexo_email_model->setDataCadastro(date('Y-m-d H:i:s'));

                        $cadastro_anexo = $this->anexo_email_model->cadastrar();

                        //$filename = $_FILES['file']['name'][$i];
                       
                       // Upload file
                       //move_uploaded_file($_FILES['file']['tmp_name'][$i],'upload/'.$filename);
                        
                    }

                } else {

                    $data= array('error' => $this->upload->display_errors());

                    $this->session->set_flashdata('msg_alerta', 9);
                    $this->session->set_flashdata('msg_conteudo', $data['error']);

                    redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
                }

                // --------------------- FIM DO ANEXO DE ARQUIVOS --------------------------------

            } else {

                $result = $this->email_clientes_model->enviar_email_cliente();  
            }

            if ($result){
                
                $this->session->set_flashdata('msg_alerta', 1);
                redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
                
            } else {
                
                $this->session->set_flashdata('msg_alerta', 2);         
                redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
                
            }
        }

        // ini_set('display_errors', 1);

        // error_reporting(E_ALL);

        // $from = "suporte@sistemaveri.com.br";

        // $to = "dhiego.samorais@gmail.com";

        // $subject = "Verificando o correio do PHP";

        // $message = "O correio do PHP funciona bem";

        // $headers =  'MIME-Version: 1.0' . "\r\n"; 
        // $headers .= 'From: Your name <suporte@sistemaveri.com.br>' . "\r\n";
        // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; 

        // mail($to, $subject, $message, $headers);

        // echo "A mensagem de e-mail foi enviada."; 
    }

    public function ler_email_cliente(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 
        
        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id_cliente = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id_cliente = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO    

        $result = $this->email_clientes_model->ler_email_cliente();

        $dados['email'] = $this->email_clientes_model->pesquisar_email_id();
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id_cliente);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_cliente($id_cliente);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_cliente($id_cliente);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_cliente($id_cliente);
        $dados['id_cliente'] = $id_cliente; 
        $dados['id'] = $id;

        $dados['qtd_anexos'] = $this->anexo_email_model->qtd_anexos_email_cliente($id);
        $dados['lista_anexos'] = $this->anexo_email_model->listar_anexos($id);
        
        // NOTIFICAÇÕES DO EMAIL 
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA

        $dados['host'] = $server;

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/email_ler', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function visualizar_email_cliente(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 
        
        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id_cliente = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id_cliente = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO    

        $dados['email'] = $this->email_clientes_model->pesquisar_email_id();
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id_cliente);
        $dados['qtd_emails_com_estrela'] = $this->email_clientes_model->qtd_emails_com_estrelas_cliente($id_cliente);
        $dados['qtd_emails_enviados'] = $this->email_clientes_model->qtd_emails_enviados_cliente($id_cliente);
        $dados['qtd_emails_excluidos'] = $this->email_clientes_model->qtd_emails_excluidos_cliente($id_cliente);
        $dados['id_cliente'] = $id_cliente; 
        $dados['id'] = $id;
        
        $dados['qtd_anexos'] = $this->anexo_email_model->qtd_anexos_email_cliente($id);
        $dados['lista_anexos'] = $this->anexo_email_model->listar_anexos($id);
        
        // NOTIFICAÇÕES DO EMAIL 
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL
        
        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA

        $dados['host'] = $server;

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/email_visualizar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function apagar_email_cliente(){

        $id = $this->uri->segment(3);

        $this->email_clientes_model->setId($id);

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id_cliente = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id_cliente = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO  

        $result = $this->email_clientes_model->excluir_email_cliente();

        if ($result){
        
            $this->session->set_flashdata('msg_alerta', 7);
            redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
                
        } else {

            $this->session->set_flashdata('msg_alerta', 8);
            redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
                
        }
    }

    public function atribuir_estrela_cliente(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id_cliente = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id_cliente = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO  

        $result = $this->email_clientes_model->definir_email_estrela_cliente();

        if ($result){
            
            $this->session->set_flashdata('msg_alerta', 3);
            redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
            
        } else {
            
            $this->session->set_flashdata('msg_alerta', 4);         
            redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
            
        }
    }

    public function remover_estrela_cliente(){

        $id = $this->uri->segment(3);
        $this->email_clientes_model->setId($id); 

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id_cliente = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id_cliente = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO  

        $result = $this->email_clientes_model->remover_estrela_cliente();

        if ($result){
            
            $this->session->set_flashdata('msg_alerta', 5);
            redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
            
        } else {
            
            $this->session->set_flashdata('msg_alerta', 6);         
            redirect('painel_clientes/listar_email_cliente/'.$id_cliente.'');
            
        }
    }

    public function listar_ipva_cliente(){

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO  

        $id_empresa = $this->ipva_clientes_model->find_id($id)->id;

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $resultado = $this->ipva_clientes_model->listar_ipva_clientes($id_empresa);

        $placas = array();

        foreach ($resultado as $item) {
            if ($a = explode("|", $item->placas)) {
                foreach ($a as $s) {
                    array_push($placas,$s);
                }
            }
        }

        $dados['lista_ipva'] = $placas;
        $dados['dados_envio'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['id_cliente'] = $id;  

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/ipva_listar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function listar_documentos_cliente(){

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO 
        
        $id_empresa = $this->documentos_clientes_model->find_id($id)->id;

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $dados['lista_documentos'] = $this->documentos_clientes_model->listar_documentos_cliente($id_empresa);
        $dados['dados_envio'] = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id);
        $dados['id_cliente'] = $id;  

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/documentos_listar', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function alterar_login_cliente(){

        // VERIFICA SE QUEM ESTÁ LOGADO É O MASTER OU COLABORADOR
        if ($this->session->userdata['userprimesession_cliente']['nivel'] == 'Master') {
            
            $id = $this->session->userdata['userprimesession_cliente']['id'];

        } else {

            $id = $this->session->userdata['userprimesession_cliente']['id_usuario_master'];
        }
        // FIM DA VERIFICAÇÃO 

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL
        
        $this->gerenciar_clientes_model->setId($id);

        if(isset($_POST['btn_salvar'])){

            $this->gerenciar_clientes_model->setLogin($this->input->post("login")); 
            $this->gerenciar_clientes_model->setSenha(md5($this->input->post("senha")));  
            $this->gerenciar_clientes_model->setIdAlteracao($this->session->userdata['userprimesession_cliente']['id']);            
            date_default_timezone_set('America/Bahia');
            $this->gerenciar_clientes_model->setDataAlteracao(date('Y-m-d H:i:s'));

            $result = $this->gerenciar_clientes_model->alterar_senha_cliente();

            if ($result){
                
                $this->session->set_flashdata('msg_alerta', 1);
                redirect('painel_clientes/alterar_login_cliente/'.$id.'');
                
            } else {
                
                $this->session->set_flashdata('msg_alerta', 2);         
                redirect('painel_clientes/alterar_login_cliente/'.$id.'');
                
            }
        }

        $dados['cliente'] = $this->gerenciar_clientes_model->pesquisar_cliente_id();
        $dados['id_cliente'] = $id;  

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/alterar_login', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    // ------------------------------ AMBOS ---------------------------------  

    // DISPARAR MENSAGEM ONE-SIGNAL
    public function sendMessage($mensagem_onesignal,$id_onesignal){
        $content = array(
            "en" => $mensagem_onesignal
            );
        
        $fields = array(
            'app_id' => "1eb8a282-cfa0-44b5-8bf5-5795a851d112",
            // 'include_external_user_ids' => $id_onesignal,
            // 'included_segments' => array('All'),
            'include_external_user_ids' => array($id_onesignal),
            'data' => array("foo" => "bar"),
            'android_led_color' => "673399",
            'android_accent_color' => "673399",
            'small_icon' =>"ic_stat_onesignal_default",
            'large_icon' =>"https://img.onesignal.com/n/80d77308-1f11-47e0-9ea7-e4e3f875ba67.png",
            'contents' => $content
        );
        
        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                       'Authorization: Basic MGM3NjhhN2QtMmFkYS00MTllLWIzZWMtNGNjOWU2ZGUxMDkx'));
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }  
}