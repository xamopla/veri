<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diagnostico_veri extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('diagnostico_veri_model');
       	$this->load->model('dctf_model');
       	$this->load->model('socios_empresas_model');
       	$this->load->model('limite_simples_model');
       	$this->load->model('parcelamento_das_model');
       	$this->load->model('parcelamento_pert_model');
       	$this->load->model('parcelamento_nao_previdenciario_model');
       	$this->load->model('parcelamento_lei_12996_model');
    }

	public function listar(){

		$empresas = array('Todas'=>'TODAS');
		$lista_empresas = $this->diagnostico_veri_model->listar_empresas();

		foreach ($lista_empresas as $e){
			$empresas[$e->cnpj] = $e->razao_social." - ".$e->cnpj;
		}

		$dados['empresas'] = $empresas;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header');
		$this->load->view('diagnostico/diagnostico_veri', $dados);
		$this->load->view('layout/footer');
		
	}

	public function gerar(){
		$empresas = $this->input->post('id_empresa');

		$pendencias = array();

		$todas_empresas = false;
		$multiplas_empresas = false;
		$qtd_empresas = 1;

		foreach ($empresas as $e) {
			if($e == "Todas"){
				$todas_empresas = true;
				$multiplas_empresas = true;
				break;
			}

			if($qtd_empresas > 1){
				$multiplas_empresas = true;
				break;
			}
			$qtd_empresas++;
		}

		$mensagem_ecac = $this->input->post('mensagem1');
		if(isset($mensagem_ecac)){
			array_push($pendencias, "Mensagens e-CAC");
		}

		$mensagem_dec = $this->input->post('mensagem2');
		if(isset($mensagem_dec)){
			array_push($pendencias, "Mensagens DEC SP");
		}

		$diagnostico_fiscal = $this->input->post('federal1');
		if(isset($diagnostico_fiscal)){
			array_push($pendencias, "Diagnóstico Fiscal");
		}

		$divergencia_gfip_gps = $this->input->post('federal2');
		if(isset($divergencia_gfip_gps)){
			array_push($pendencias, "Divergência GFIP x GPS");
		}

		$dctf = $this->input->post('federal3');
		if(isset($dctf)){
			array_push($pendencias, "DCTF");
		}

		$pgdas = $this->input->post('federal4');
		if(isset($pgdas)){
			array_push($pendencias, "PGDAS");
		}

		$das_nao_pagos = $this->input->post('federal5');
		if(isset($das_nao_pagos)){
			array_push($pendencias, "DAS Não Pagos");
		}

		$pronampe = $this->input->post('federal6');
		if(isset($pronampe)){
			array_push($pendencias, "PRONAMPE");
		}

		$ausencia_ecf = $this->input->post('federal7');
		if(isset($ausencia_ecf)){
			array_push($pendencias, "Ausência Decl. ECF");
		}

		$ausencia_gfip = $this->input->post('federal8');
		if(isset($ausencia_gfip)){
			array_push($pendencias, "Ausência Decl. GFIP");
		}

		$ausencia_dctf = $this->input->post('federal9');
		if(isset($ausencia_dctf)){
			array_push($pendencias, "Ausência Decl. DCTF");
		}

		$ausencia_pgdas = $this->input->post('federal10');
		if(isset($ausencia_pgdas)){
			array_push($pendencias, "Ausência Decl. PGDAS");
		}

		$cadin = $this->input->post('federal11');
		if(isset($cadin)){
			array_push($pendencias, "CADIN");
		}

		$malha_fiscal = $this->input->post('federal12');
		if(isset($malha_fiscal)){
			array_push($pendencias, "Malha Fiscal");
		}

		$ausencia_defis = $this->input->post('federal13');
		if(isset($ausencia_defis)){
			array_push($pendencias, "Ausência Decl. DEFIS");
		}

		$ausencia_efd = $this->input->post('federal14');
		if(isset($ausencia_efd)){
			array_push($pendencias, "Ausência Decl. EFD");
		}

		$ausencia_dirf = $this->input->post('federal15');
		if(isset($ausencia_dirf)){
			array_push($pendencias, "Ausência Decl. DIRF");
		}

		$ausencia_dasn = $this->input->post('federal16');
		if(isset($ausencia_dasn)){
			array_push($pendencias, "Ausência Decl. DASN SIMEI");
		}

		$procuracoes = $this->input->post('federal17');
		if(isset($procuracoes)){
			array_push($pendencias, "Procurações");
		}

		$termo_exclusao = $this->input->post('federal18');
		if(isset($termo_exclusao)){
			array_push($pendencias, "Termo de Exclusão");
		}

		// $divida_ativa = $this->input->post('federal19');
		// if(isset($divida_ativa)){
		// 	array_push($pendencias, "Dívida Ativa");
		// }

		$sublimite_simples = $this->input->post('federal20');
		if(isset($sublimite_simples)){
			array_push($pendencias, "Sublimite do Simples Nacional");

			array_push($pendencias, "Sublimite do Simples Nacional Acumulado Sócios");
		}

		$exclusao_simples = $this->input->post('federal21');
		if(isset($exclusao_simples)){
			array_push($pendencias, "Exclusão Simples Nacional");
		}

		$exclusao_mei = $this->input->post('federal22');
		if(isset($exclusao_mei)){
			array_push($pendencias, "Exclusão MEI");
		}

		$termo_intimacao = $this->input->post('federal24');
		if(isset($termo_intimacao)){
			array_push($pendencias, "Termo de Intimação");
		}

		$certidao_estadual = $this->input->post('certidao1');
		if(isset($certidao_estadual)){
			array_push($pendencias, "Certidão Estadual");
		}

		$certidao_trabalhista = $this->input->post('certidao2');
		if(isset($certidao_trabalhista)){
			array_push($pendencias, "Certidão Trabalhista");
		}

		$certidao_rfb = $this->input->post('certidao3');
		if(isset($certidao_rfb)){
			array_push($pendencias, "Certidão RFB/PGFN");
		}

		$certidao_fgts = $this->input->post('certidao4');
		if(isset($certidao_fgts)){
			array_push($pendencias, "Certidão FGTS");
		}

		$certidao_municipal = $this->input->post('certidao5');
		if(isset($certidao_municipal)){
			array_push($pendencias, "Certidão Municipal");
		}

		$certidao_municipal = $this->input->post('certidao5');
		if(isset($certidao_municipal)){
			array_push($pendencias, "Certidão Municipal");
		}

		$eprocessos = $this->input->post('processos1');
		if(isset($eprocessos)){
			array_push($pendencias, "e-Processos");
		}

		$parcelamentos_das = $this->input->post('parcelamento1');
		if(isset($parcelamentos_das)){
			array_push($pendencias, "Parcelamento DAS");
		}

		$parcelamentos_pert = $this->input->post('parcelamento2');
		if(isset($parcelamentos_pert)){
			array_push($pendencias, "Parcelamento PERT");
		}

		$parcelamentos_nao_prev = $this->input->post('parcelamento3');
		if(isset($parcelamentos_nao_prev)){
			array_push($pendencias, "Parcelamento Não Previdenciário");
		}

		$parcelamentos_lei = $this->input->post('parcelamento4');
		if(isset($parcelamentos_lei)){
			array_push($pendencias, "Parcelamento Lei 12.996");
		}

		$empresas_detalhes = $this->diagnostico_veri_model->detalhar_empresas($empresas, $todas_empresas);
		$hash_pendencias = array();

		foreach ($empresas_detalhes as $e) {

			$pendencias_detalhes = array();

			if(isset($mensagem_ecac)){
				$m1 = $this->mensagem_ecac($e);
				$m1->nome = "Mensagens e-CAC";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($mensagem_dec)){
				$m1 = $this->mensagem_dec($e);
				$m1->nome = "Mensagens DEC";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($diagnostico_fiscal)){
				$m1 = $this->diagnostico_fiscal($e);
				$m1->nome = "Diagnóstico Fiscal";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($divergencia_gfip_gps)){
				$m1 = $this->divergencia_gfip_gps($e);
				$m1->nome = "Divergência GFIP X GPS";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($dctf)){
				$m1 = $this->dctf($e);
				$m1->nome = "DCTF";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($pgdas)){
				$m1 = $this->pgdas($e);
				$m1->nome = "PGDAS";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($das_nao_pagos)){
				$m1 = $this->das_nao_pagos($e);
				$m1->nome = "DAS não pagos";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($pronampe)){
				$m1 = $this->pronampe($e);
				$m1->nome = "Pronampe";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_ecf)){
				$m1 = $this->ausencia_ecf($e);
				$m1->nome = "Ausência ECF";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_gfip)){
				$m1 = $this->ausencia_gfip($e);
				$m1->nome = "Ausência GFIP";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_dctf)){
				$m1 = $this->ausencia_dctf($e);
				$m1->nome = "Ausência DCTF";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_pgdas)){
				$m1 = $this->ausencia_pgdas($e);
				$m1->nome = "Ausência PGDAS";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($cadin)){
				$m1 = $this->cadin($e);
				$m1->nome = "Cadin";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($malha_fiscal)){
				$m1 = $this->malha_fiscal($e);
				$m1->nome = "Malha Fiscal";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_defis)){
				$m1 = $this->ausencia_defis($e);
				$m1->nome = "Ausência DEFIS";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_efd)){
				$m1 = $this->ausencia_efd($e);
				$m1->nome = "Ausência EFD";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_dirf)){
				$m1 = $this->ausencia_dirf($e);
				$m1->nome = "Ausência DIRF";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($ausencia_dasn)){
				$m1 = $this->ausencia_dasn($e);
				$m1->nome = "Ausência DASN";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($procuracoes)){
				$m1 = $this->procuracoes($e);
				$m1->nome = "Procurações";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($termo_exclusao)){
				$m1 = $this->termo_exclusao($e);
				$m1->nome = "Termo de Exclusão";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($sublimite_simples)){
				$m1 = $this->sublimite_simples($e);
				$m1->nome = "Sublimite do Simples";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($sublimite_simples)){
				$m1 = $this->sublimite_simples_acumulado($e);
				$m1->nome = "Sublimite do Simples Nacional Acumulado Sócios";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($exclusao_simples)){
				$m1 = $this->exclusao_simples($e);
				$m1->nome = "Exclusão do Simples";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($exclusao_mei)){
				$m1 = $this->exclusao_mei($e);
				$m1->nome = "Exclusão do MEI";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($termo_intimacao)){
				$m1 = $this->termo_intimacao($e);
				$m1->nome = "Termo de Intimação";
				array_push($pendencias_detalhes, $m1);
			}

			// if(isset($certidao_estadual)){
			// 	$m1 = $this->certidao_estadual($e);
			// 	$m1->nome = "Certidão Estadual";
			// 	array_push($pendencias_detalhes, $m1);
			// }

			if(isset($eprocessos)){
				$m1 = $this->eprocessos($e);
				$m1->nome = "e-Processos";
				array_push($pendencias_detalhes, $m1);
			}


			if(isset($parcelamentos_das)){
				$m1 = $this->parcelamentos_das($e);
				$m1->nome = "Parcelamento DAS";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($parcelamentos_pert)){
				$m1 = $this->parcelamentos_pert($e);
				$m1->nome = "Parcelamento PERT";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($parcelamentos_nao_prev)){
				$m1 = $this->parcelamentos_nao_prev($e);
				$m1->nome = "Parcelamento Não Previdenciário";
				array_push($pendencias_detalhes, $m1);
			}

			if(isset($parcelamentos_lei)){
				$m1 = $this->parcelamentos_lei($e);
				$m1->nome = "Parcelamento Lei 12.996";
				array_push($pendencias_detalhes, $m1);
			}


			$hash_pendencias[$e->cnpj] = $pendencias_detalhes;
		}

		$dados['empresas_detalhes'] = $empresas_detalhes;

		$dados['pendencias'] = $pendencias;
		$dados['hash_pendencias'] = $hash_pendencias;

		if($multiplas_empresas == false){
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('diagnostico/diagnostico_veri_individual', $dados);
			$this->load->view('layout/footer');
		}else{
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('diagnostico/diagnostico_veri_multiplos', $dados);
			$this->load->view('layout/footer');
		}
		
	}

	/*                                      FUNÇÕES ESPECÍFICAS                                     */
	// MENSAGENS ECAC *************************************************************************
	public function mensagem_ecac($empresa){
		$mensagem = $this->diagnostico_veri_model->mensagem_ecac($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($mensagem)){
			if($mensagem->nao_lidas != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Mensagens não lidas';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">'.$mensagem->nao_lidas.' mensagem(s) não lida(s)</label>';
				$objeto->link = base_url("ecac/listar_mensagens/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Todas as mensagens lidas';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Todas as mensagens lidas</label>';
				$objeto->link = base_url("ecac/listar_mensagens/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;">Nada Consta</label>';
			$objeto->link = base_url("ecac/listar_mensagens/").$empresa->cnpj;
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}

	//MENSAGENS DEC *************************************************************************
	public function mensagem_dec($empresa){
		$mensagem = $this->diagnostico_veri_model->mensagem_dec($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($mensagem)){
			if($mensagem->qtd_nao_lida != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Mensagens não lidas';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">'.$mensagem->qtd_nao_lida.' mensagem(s) não lida(s)</label>';
				$objeto->link = base_url("dec/listar_dec_recebidas/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Todas as Mensagens lidas';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Todas as mensagens lidas</label>';
				$objeto->link = base_url("dec/listar_dec_recebidas/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;">Nada Consta</label>';
			$objeto->link = base_url("dec/listar_dec_recebidas/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}

	//DIAGNOSTICO FISCAL *************************************************************************
	public function diagnostico_fiscal($empresa){
		$diagnostico = $this->diagnostico_veri_model->diagnostico_fiscal($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($diagnostico)){
			if($diagnostico->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = $diagnostico->caminho_download;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = $diagnostico->caminho_download;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;">Nada Consta</label>';
			$objeto->link = base_url("ecac/listar_situacao_fiscal_geral");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//DIVERGENCIA GFIP X GPS *************************************************************************
	public function divergencia_gfip_gps($empresa){
		$divergencia = $this->diagnostico_veri_model->divergencia_gfip_gps($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("gfip/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("gfip/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;">Nada Consta</label>';
			$objeto->link = base_url("gfip/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}



	//DCTF *************************************************************************
	public function dctf($empresa){

		if($empresa->tipo_regime == "SIMPLES NACIONAL"){
			$objeto = new stdClass();
			$objeto->tam = '35px';

			$objeto->is_ok = 'info';
			$objeto->titulo = 'Simples Nacional';
			$objeto->detalhes = '<label style="color:#0a93f5; font-weight: bold;">Empresa do Simples Nacional</label>';
			$objeto->link = base_url("dctf/index/");

			return $objeto;
		}else{
			$dctf = $this->diagnostico_veri_model->dctf($empresa);

			$objeto = new stdClass();
			$objeto->tam = '35px';
			if(isset($dctf)){
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">DCTF desse mês foi entregue no dia '.$dctf->data_recepcao.'</label>';
				$objeto->link = base_url("dctf/index?filtro=REGULAR");
			}else{

				$data_atual = date('Y-m-d');

				$data_aux = date('Y-m-d', strtotime($data_atual.' - 2 months')); 

				$arr = explode("-", $data_aux);
				$mes_ = $arr[1];
				$ano_ = $arr[0];

				$dados['m'] = $mes_;
				$dados['ano'] = $ano_;

				$mes_aux = $mes_;
				$ano_aux = $ano_;

				$mes_filtro_aux = ltrim($dados['m'], "0");
		        $ano_filtro_aux = $dados['ano'];

		        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

		        //busca empresas marcadas como sem movimento
		        $sem_movimento = $this->dctf_model->buscar_empresas_sem_movimento($data_filtro_aux);
		        $myhashmap = array();
		        foreach ($sem_movimento as $d) {
					$myhashmap[$d->cnpj] = $d;
				}


				//calculo da data de vencimento
				$ultimo_dia_transmitir = $this->getDiaUtilDCTF(15, $mes_aux+2, $ano_aux);

				$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

				$data_aux = date('Y-m-d', strtotime($data_format.' + 2 months')); 

				$dados['ultimo_dia_transmitir'] = $data_aux;

				//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
				$data_final = strtotime($data_aux);

		        $df = new DateTime($data_aux);
		        $da = new DateTime(date(''));
		        $intervalo = $df->diff($da);

		        $fora_prazo = false;

		        if ($data_final < strtotime(date('Y-m-d'))) {
		          $fora_prazo = true;
		        } else{
		          $fora_prazo = false;
		        }

				if($fora_prazo == false){
	        		if(isset($myhashmap[$empresa->cnpj])){
	        			$objeto->is_ok = 'sem_movimento';
	        			$objeto->titulo = 'Sem movimento';
	        			$s = "Analisado por: ".$myhashmap[$empresa->cnpj]->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($myhashmap[$empresa->cnpj]->data_alteracao));
						$objeto->detalhes = '<label style="color:#fb790a; font-weight: bold;">'.$s.'</label>';


						$objeto->link = base_url("dctf/index?filtro=SEM_MOVIMENTO");
						$objeto->tam = '34px';
	        		}else{
	        			$objeto->is_ok = 'em_analise';
	        			$objeto->titulo = 'Em análise';
						$objeto->detalhes = '<label style="color:#e9b100; font-weight: bold;">Veri ainda não localizou o DCTF desse mês, entretanto ainda está dentro do prazo de entrega </label>';
						$objeto->link = base_url("dctf/index?filtro=ANALISAR");
	        		}
	        	}else{
	        		if(isset($myhashmap[$empresa->cnpj])){
	        			$objeto->is_ok = 'sem_movimento';
	        			$objeto->titulo = 'Sem movimento';
	        			$s = "Analisado por: ".$myhashmap[$empresa->cnpj]->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($myhashmap[$empresa->cnpj]->data_alteracao));
						$objeto->detalhes = '<label style="color:#fb790a; font-weight: bold;">'.$s.'</label>';

						$objeto->link = base_url("dctf/index?filtro=SEM_MOVIMENTO");
						$objeto->tam = '34px';
	        		}else{
	        			$objeto->is_ok = 'pendencia';
	        			$objeto->titulo = 'Vencida';
						$objeto->detalhes = '<label style="color:red; font-weight: bold;">Entrega de DCTF não realizada dentro do prazo do mês atual</label>';
						$objeto->link = base_url("dctf/index?filtro=PENDENTE");
	        		}
	        	}
			}
			
			return $objeto;
		}
		
	}


	public static function getDiaUtilDCTF($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
		date_default_timezone_set('America/Sao_Paulo');

		$iMes = empty($iMes) ? date('m') : $iMes;
		$iAno = empty($iAno) ? date('Y') : $iAno;
		$iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));
 
		for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
			$iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
			//inclui apenas os dias úteis
			if ($iDiaSemana < 6) {
				$aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
			}
		}
		//ignorando os feriados	
		if (sizeof($aDiasIgnorar) > 0) {
			foreach ($aDiasIgnorar as $iDia) {
				$iKey = array_search($iDia, $aDias);
				unset($aDias[$iKey]);
			}
		}
 
		if (isset($aDias[$iDia - 1])) {
			return $aDias[$iDia - 1];
		} else {
			//retorna o último dia útil
			return $aDias[count($aDias) - 1];
		}
	}



	//PGDAS *************************************************************************
	public function pgdas($empresa){
		if($empresa->tipo_regime != "SIMPLES NACIONAL"){
			$objeto = new stdClass();
			$objeto->tam = '35px';

			$objeto->is_ok = 'info';
			$objeto->titulo = 'Regime Normal';
			$objeto->detalhes = '<label style="color:#0a93f5; font-weight: bold;">Empresa do Regime Normal</label>';
			$objeto->link = base_url("pgdas/index/");

			return $objeto;
		}else{

			$pgdas = $this->diagnostico_veri_model->pgdas($empresa);

			$objeto = new stdClass();
			$objeto->tam = '35px';
			if(isset($pgdas)){
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">PGDAS desse mês foi entregue no dia '.$pgdas->data_hora_transmissao.'</label>';
				$objeto->link = base_url("pgdas/index?filtro=REGULAR");
			}else{

				$data_atual = date('Y-m-d');

				$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

				$arr = explode("-", $data_aux);
				$mes_ = $arr[1];
				$ano_ = $arr[0];

				$dados['m'] = $mes_;
				$dados['ano'] = $ano_;

				$mes_aux = $mes_;
				$ano_aux = $ano_;

				$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

				$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

				$data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

				$dados['ultimo_dia_transmitir'] = $data_aux;

				//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
				$data_final = strtotime($data_aux);

		        $df = new DateTime($data_aux);
		        $da = new DateTime(date(''));
		        $intervalo = $df->diff($da);

		        $fora_prazo = false;

		        if ($data_final < strtotime(date('Y-m-d'))) {
		          $fora_prazo = true;
		        } else{
		          $fora_prazo = false;
		        }

		        $mes_filtro_aux = ltrim($dados['m'], "0");
		        $ano_filtro_aux = $dados['ano'];

		        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

				if($fora_prazo == false){
	        		$objeto->is_ok = 'em_analise';
	        		$objeto->titulo = 'Em análise';
					$objeto->detalhes = '<label style="color:#e9b100; font-weight: bold;">Veri ainda não localizou o PGDAS desse mês, entretanto ainda está dentro do prazo de entrega</label>';
					$objeto->link = base_url("pgdas/index?filtro=ANALISAR");
	        	}else{
	        		$objeto->is_ok = 'pendencia';
	        		$objeto->titulo = 'Vencida';
					$objeto->detalhes = '<label style="color:red; font-weight: bold;">Entrega de PGDAS não realizada dentro do prazo do mês atual</label>';
					$objeto->link = base_url("pgdas/index?filtro=PENDENTE");
	        	}

			}

			return $objeto;
		}
	}



	//DAS NÃO PAGOS *************************************************************************
	public function das_nao_pagos($empresa){
		if($empresa->tipo_regime != "SIMPLES NACIONAL"){
			$objeto = new stdClass();
			$objeto->tam = '35px';

			$objeto->is_ok = 'info';
			$objeto->titulo = 'Regime Normal';
			$objeto->detalhes = '<label style="color:#0a93f5; font-weight: bold;">Empresa do Regime Normal</label>';
			$objeto->link = base_url("das/index/");

			return $objeto;
		}else{

			$das = $this->diagnostico_veri_model->das_nao_pagos($empresa);

			$objeto = new stdClass();
			$objeto->tam = '35px';
			if(isset($das)){

				if($das->pago == "Sim"){
					$objeto->is_ok = 'regular';
					$objeto->titulo = 'Regular';
					$objeto->detalhes = '<label style="color:green; font-weight: bold;">PGDAS desse mês foi entregue no dia '.$das->data_hora_transmissao.'</label>';
					$objeto->link = base_url("das/index?filtro=REGULAR");
				}else{

					$data_atual = date('Y-m-d');

					$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

					$arr = explode("-", $data_aux);
					$mes_ = $arr[1];
					$ano_ = $arr[0];

					$dados['m'] = $mes_;
					$dados['ano'] = $ano_;

					$mes_aux = $mes_;
					$ano_aux = $ano_;

					$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

					$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

					$data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

					$dados['ultimo_dia_transmitir'] = $data_aux;

					//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
					$data_final = strtotime($data_aux);

			        $df = new DateTime($data_aux);
			        $da = new DateTime(date(''));
			        $intervalo = $df->diff($da);

			        $fora_prazo = false;

			        if ($data_final < strtotime(date('Y-m-d'))) {
			          $fora_prazo = true;
			        } else{
			          $fora_prazo = false;
			        }

			        $mes_filtro_aux = ltrim($dados['m'], "0");
			        $ano_filtro_aux = $dados['ano'];

			        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

			        if(empty($das->numero_das)){
			        	$objeto->is_ok = 'sem_movimento';
			        	$objeto->titulo = 'Sem Movimento';
						$objeto->detalhes = '<label style="color:#fb790a; font-weight: bold;">Sem movimento </label><br>';
						$objeto->link = base_url("das/index");
						$objeto->tam = '34px';
			        }else{
			        	if($fora_prazo == false){
			        		$objeto->is_ok = 'em_analise';
			        		$objeto->titulo = 'Em análise';
							$objeto->detalhes = '<label style="color:#e9b100; font-weight: bold;">Veri ainda não localizou o pagamento do DAS desse mês, entretanto ainda está dentro da data de vencimento</label>';
							$objeto->link = base_url("das/index?filtro=PENDENTE");
			        	}else{
			        		$objeto->is_ok = 'pendencia';
			        		$objeto->titulo = 'Vencida';
							$objeto->detalhes = '<label style="color:red; font-weight: bold;">Pagamento do DAS não realizada dentro da data de vencimento do mês atual</label>';
							$objeto->link = base_url("das/index?filtro=PENDENTE");
			        	}	
			        }
					
				}
				
			}else{

				$objeto->is_ok = 'alert';
				$objeto->titulo = 'Nada Consta';
				$objeto->detalhes = '<label style="color:blue; font-weight: bold;">Nada Consta</label>';
				$objeto->link = base_url("das/index");
				$objeto->tam = '30px';

			}

			return $objeto;
		}
	}


	//PRONAMPE *************************************************************************
	public function pronampe($empresa){
		$pronampe = $this->diagnostico_veri_model->pronampe($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(!empty($pronampe)){
			$objeto->is_ok = 'regular';
			$objeto->titulo = 'Regular';
			$objeto->detalhes = '<label style="color:green; font-weight: bold;">Existe PRONAMPE para esta empresa </label>';
			$objeto->link = base_url("pronampe/listar/").$empresa->cnpj;
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> PRONAMPE não encontrado</label>';
			$objeto->link = base_url("pronampe/listar");
			$objeto->tam = '30px';
		}

		return $objeto;
	}


	//AUSÊNCIA DECLARAÇÃO ECF *************************************************************************
	public function ausencia_ecf($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_ecf($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_ecf/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_ecf/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_ecf/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//AUSÊNCIA DECLARAÇÃO ECF *************************************************************************
	public function ausencia_gfip($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_gfip($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_gfip/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_gfip/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_gfip/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//AUSÊNCIA DECLARAÇÃO DCTF *************************************************************************
	public function ausencia_dctf($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_dctf($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_dctf/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_dctf/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_dctf/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//AUSÊNCIA DECLARAÇÃO PGDAS *************************************************************************
	public function ausencia_pgdas($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_pgdas($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_pgdas/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_pgdas/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_pgdas/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//CADIN *************************************************************************
	public function cadin($empresa){
		$diagnostico = $this->diagnostico_veri_model->cadin($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($diagnostico)){
			if($diagnostico->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = $diagnostico->caminho_download;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = $diagnostico->caminho_download;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ecac/listar_situacao_fiscal_geral");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//MALHA FISCAL
	public function malha_fiscal($empresa){
		$malha_fiscal = $this->diagnostico_veri_model->malha_fiscal($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(!empty($malha_fiscal)){
			$objeto->is_ok = 'pendencia';
			$objeto->titulo = 'Irregular';
			$objeto->detalhes = '<label style="color:red; font-weight: bold;">Existem notificações da malha fiscal para esta empresa </label>';
			$objeto->link = base_url("malha_fiscal/listar/").$empresa->cnpj;
		}else{
			$objeto->is_ok = 'regular';
			$objeto->titulo = 'Regular';
			$objeto->detalhes = '<label style="color:green; font-weight: bold;">Não existem notificações sobre malha fiscal para esta empresa </label>';
			$objeto->link = base_url("malha_fiscal/listar/").$empresa->cnpj;
		}

		return $objeto;
	}


	//AUSENCIA DECLARAÇÃO DEFIS
	public function ausencia_defis($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_defis($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_defis/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_defis/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_defis/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//AUSENCIA DECLARAÇÃO EFD
	public function ausencia_efd($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_efd($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_efd/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_efd/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_efd/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//AUSENCIA DECLARAÇÃO DIRF
	public function ausencia_dirf($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_dirf($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_dirf/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_dirf/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_dirf/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//AUSENCIA DECLARAÇÃO DASN
	public function ausencia_dasn($empresa){
		$divergencia = $this->diagnostico_veri_model->ausencia_dasn($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($divergencia)){
			if($divergencia->possui_pendencia != 0){
				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular</label>';
				$objeto->link = base_url("ausencia_dasn/listar/").$empresa->cnpj;
			}else{
				$objeto->is_ok = 'regular';
				$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("ausencia_dasn/listar/").$empresa->cnpj;
			}
		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ausencia_dasn/listar/TODAS");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//PROCURAÇÕES
	public function procuracoes($empresa){
		$procuracao = $this->diagnostico_veri_model->procuracoes($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(isset($procuracao)){


			$data_banco = explode("/", $procuracao->data_fim);
            $data_final_format = $data_banco[2]."-".$data_banco[1]."-".$data_banco[0];

            $data_final = strtotime($data_final_format);
            $data_atual = strtotime(date(''). ' + 30 days');

            $df = new DateTime($data_final_format);
            $da = new DateTime(date(''));
            $intervalo = $df->diff($da);

            //REGULAR
            if ($data_final > strtotime(date('Y-m-d'))) {
            	$objeto->is_ok = 'regular';
            	$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
				$objeto->link = base_url("procuracao/regulares/");
            } 

            //PROXIMO VENCER
            if ( ($intervalo->format('%a') <= 30) && ($data_final > strtotime(date('Y-m-d'))) ) {
            	$objeto->is_ok = 'proximo_vencer';
            	$objeto->titulo = 'Próximo a Vencer';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Restam '.$intervalo->format('%a').' dias para o vencimento da procuração </label>';
				$objeto->link = base_url("procuracao/proximas_vencer");
            }

            //VENCIDA
            if ($data_final < strtotime(date('Y-m-d'))) {
            	$objeto->is_ok = 'pendencia';
            	$objeto->titulo = 'Vencido';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Certificado vencido à '.$intervalo->format('%a').' dias </label>';
				$objeto->link = base_url("procuracao/vencidas");
            }

		}else{
			$objeto->is_ok = 'alert';
			$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("procuracao/listar");
			$objeto->tam = '30px';
		}
		
		return $objeto;
	}


	//TERMO DE EXCLUSÃO DO SIMPLES
	public function termo_exclusao($empresa){
		$termo_exclusao = $this->diagnostico_veri_model->termo_exclusao($empresa);

		$objeto = new stdClass();
		$objeto->tam = '35px';
		if(!empty($termo_exclusao)){
			$objeto->is_ok = 'pendencia';
			$objeto->titulo = 'Irregular';
			$objeto->detalhes = '<label style="color:red; font-weight: bold;">Existem termos de exclusão do Simples Nacional para esta empresa</label>';
			$objeto->link = base_url("notificacao_exclusao/listar/").$empresa->cnpj;
		}else{
			$objeto->is_ok = 'regular';
			$objeto->titulo = 'Regular';
			$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular</label>';
			$objeto->link = base_url("notificacao_exclusao/listar/").$empresa->cnpj;
		}
		
		return $objeto;
	}

	public function sublimite_simples($empresa){
		$resultado = $this->diagnostico_veri_model->sublimite_simples($empresa);

		if($empresa->tipo_regime == "SIMPLES NACIONAL"){
        	if(isset($resultado)){

        		$objeto = new stdClass();
				$objeto->tam = '35px';

        		if($resultado->percentual >= 70 && $resultado->percentual <= 100){
        			$objeto = new stdClass();
					$objeto->tam = '35px';

					$objeto->is_ok = 'alert_prox';
					$objeto->titulo = 'Próx Sublimite';
					$objeto->detalhes = '<label style="color:#0a93f5; font-weight: bold;">Próxima do Sublimite - percentual de '.$resultado->percentual.'% atingido<br>'.' Total '."R$ ".number_format($resultado->valor_atual, 2, ',', '.').'</label>';
					$objeto->link = base_url("limite_simples/listar/PROXIMAS");

					return $objeto;
        		}elseif($resultado->percentual >= 100){
        			$objeto = new stdClass();
					$objeto->tam = '35px';

					$objeto->is_ok = 'pendencia';
					$objeto->titulo = 'Sublimite Atingido';
					$objeto->detalhes = '<label style="color:red; font-weight: bold;">Sublimite Atingido - percentual de '.$resultado->percentual.'% atingido<br>'.' Total '."R$ ".number_format($resultado->valor_atual, 2, ',', '.').'</label>';
					$objeto->link = base_url("limite_simples/listar/LIMITE");

					return $objeto;
        		}else{
        			$objeto->is_ok = 'regular';
        			$objeto->titulo = 'Regular';
					$objeto->detalhes = '<label style="color:green; font-weight: bold;">Percentual de '.$resultado->percentual.'% atingido<br>'.' Total '."R$ ".number_format($resultado->valor_atual, 2, ',', '.').'</label>';
					$objeto->link = base_url("limite_simples/listar/REGULAR");

					return $objeto;
        		}
	        	

	        }else{

	        	$objeto = new stdClass();

	        	$objeto->is_ok = 'alert';
	        	$objeto->titulo = 'Nada Consta';
				$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
				$objeto->link = base_url("limite_simples/listar");
				$objeto->tam = '30px';

				return $objeto;
	        }
        }else{
        	$objeto = new stdClass();
			$objeto->tam = '35px';
			$objeto->titulo = 'Regime Normal';
			$objeto->is_ok = 'info';
			$objeto->detalhes = '<label style="color:#0a93f5; font-weight: bold;">Empresa do Regime Normal</label>';
			$objeto->link = base_url("limite_simples/listar/");

			return $objeto;
        }
	}

	//SUBLIMITE SIMPLES NACIONAL ACUMULADO
	public function sublimite_simples_acumulado($empresa){

        $resultado = $this->diagnostico_veri_model->sublimite_simples($empresa);
        $myhashmap_limites = array();

        $resultados = $this->limite_simples_model->buscar();
        $myhashmap_limites = array();
        foreach ($resultados as $r) {
            $myhashmap_limites[$r->cnpj] = $r;
        }

        if($empresa->tipo_regime == "SIMPLES NACIONAL"){
        	if(isset($resultado)){

	        	$array_final = array();

	        	$this->soma_total($resultado, $myhashmap_limites);

	            $resultado->total_acumulado = $resultado->soma_total + $resultado->valor_atual;
	            $resultado->percentual_acumulado = (int) ( ( ( (float) ($resultado->total_acumulado) ) / 4800000 ) * 100);

	            if($resultado->percentual_acumulado >= 70 && $resultado->percentual_acumulado < 100){
	            	//proximas do todal acumulado
                   	$objeto = new stdClass();
					$objeto->tam = '35px';

					$objeto->is_ok = 'alert_prox';
					$objeto->titulo = 'Próx Sublimite Acum. Sócios';
					$objeto->detalhes = '<label style="color:#0a93f5; font-weight: bold;">Próxima do Sublimite Acumulado dos Sócios - percentual de '.$resultado->percentual_acumulado.'% atingido<br>'.' Total '."R$ ".number_format($resultado->total_acumulado, 2, ',', '.').'</label>';
					$objeto->link = base_url("limite_simples/listar/PROXIMAS");
                }elseif($resultado->percentual_acumulado >= 100){
                	$objeto = new stdClass();
					$objeto->tam = '35px';
					$objeto->titulo = 'Sublimite Acum. dos Sócios Atingido';
					$objeto->is_ok = 'pendencia';
					$objeto->detalhes = '<label style="color:red; font-weight: bold;">Sublimite Acumulado dos Sócios Atingido - percentual de '.$resultado->percentual_acumulado.'% atingido<br>'.' Total '."R$ ".number_format($resultado->total_acumulado, 2, ',', '.').'</label>';
					$objeto->link = base_url("limite_simples/listar/LIMITE");

					return $objeto;
                }else{
                	$objeto = new stdClass();
                	$objeto->tam = '35px';

                	$objeto->titulo = 'Regular';
                	$objeto->is_ok = 'regular';
					$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular - percentual de '.$resultado->percentual_acumulado.'% atingido<br>'.' Total '."R$ ".number_format($resultado->total_acumulado, 2, ',', '.').'</label>';
					$objeto->link = base_url("limite_simples/listar/REGULAR");

					return $objeto;
                }

	        }else{
	        	$objeto = new stdClass();

	        	$objeto->is_ok = 'alert';
	        	$objeto->titulo = 'Nada Consta';
				$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
				$objeto->link = base_url("limite_simples/listar");
				$objeto->tam = '30px';

				return $objeto;
	        }
        }else{
        	$objeto = new stdClass();
			$objeto->tam = '35px';

			$objeto->is_ok = 'info';
			$objeto->titulo = 'Regime Normal';
			$objeto->detalhes = '<label style="color:#0a93f5; font-weight: bold;">Empresa do Regime Normal</label>';
			$objeto->link = base_url("limite_simples/listar/");

			return $objeto;
        }
    }


    public function soma_total($r, $myhashmap_limites){
        $socios = $this->socios_empresas_model->busca_socios($r->cnpj);
        $soma_total = 0;
        $r->soma_total = 0;

        $array_cpfs = array();
        $string_socios = "";
        if(!empty($socios)){

            foreach ($socios as $socio) {
                if(!empty($socio->nome_socio)){
                    if($string_socios == ""){
                        $string_socios = $socio->nome_socio." - ".$socio->cpf_cnpj;
                    }else{
                        $string_socios = $string_socios."<br>".$socio->nome_socio." - ".$socio->cpf_cnpj;
                    }
                    
                }

                array_push($array_cpfs, $socio->cpf_cnpj);
            }
            $socios_outras_empresas = $this->socios_empresas_model->busca_socios_outras_empresas($r->cnpj, $array_cpfs);
            
            foreach ($socios_outras_empresas as $s) {

                if(isset($myhashmap_limites[$s->cnpj_empresa])){
                    $a = $myhashmap_limites[$s->cnpj_empresa];
                    $soma_total = $soma_total + $a->valor_atual;
                    $r->soma_total = $soma_total;
                }
                
            }  
        }else{
          $string_socios = "QUADRO SOCIETÁRIO NÃO OBRIGATÓRIO POR SUA NATUREZA JURÍDICA";  
        }

        $r->socios_empresas_string = $string_socios;
        
    }


    //EXCLUIDAS DO SIMPLES
    public function exclusao_simples($empresa){
    	$resultado = $this->diagnostico_veri_model->exclusao_simples($empresa);

		if(isset($resultado)){
			if($resultado->is_excluida == 1){
               	$objeto = new stdClass();
				$objeto->tam = '35px';

				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Essa empresa já foi excluída do Simples Nacional - Excluída em '.$resultado->data_fim.'</label>';
				$objeto->link = base_url("simples_nacional/listar/EXCLUIDAS");

				return $objeto;
            }else{
            	$objeto = new stdClass();
            	$objeto->tam = '35px';

            	$objeto->is_ok = 'regular';
            	$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Optante pelo Simples Nacional - desde '.$resultado->data_inicio.'</label>';
				$objeto->link = base_url("simples_nacional/listar/OPTANTES");

				return $objeto;
            }
		}else{
			$objeto = new stdClass();

        	$objeto->is_ok = 'alert';
        	$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("simples_nacional/listar");
			$objeto->tam = '30px';

			return $objeto;
		}
        	
    }


    //EXCLUIDAS DO MEI
    public function exclusao_mei($empresa){
    	$resultado = $this->diagnostico_veri_model->exclusao_mei($empresa);

		if(isset($resultado)){
			if($resultado->is_excluida == 1){
               	$objeto = new stdClass();
				$objeto->tam = '35px';

				$objeto->is_ok = 'pendencia';
				$objeto->titulo = 'Irregular';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Essa empresa já foi excluída do MEI - Excluída em '.$resultado->data_fim.'</label>';
				$objeto->link = base_url("mei/listar/EXCLUIDAS");

				return $objeto;
            }else{
            	$objeto = new stdClass();
            	$objeto->tam = '35px';

            	$objeto->is_ok = 'regular';
            	$objeto->titulo = 'Regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Optante pelo MEI - desde '.$resultado->data_inicio.'</label>';
				$objeto->link = base_url("mei/listar/OPTANTES");

				return $objeto;
            }
		}else{
			$objeto = new stdClass();

        	$objeto->is_ok = 'alert';
        	$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("mei/listar");
			$objeto->tam = '30px';

			return $objeto;
		}
    }


    //TERMO DE INTIMAÇÃO
    public function termo_intimacao($empresa){
    	$resultado = $this->diagnostico_veri_model->termo_intimacao($empresa);

		if(empty($resultado)){
			$objeto = new stdClass();
        	$objeto->tam = '35px';

        	$objeto->is_ok = 'regular';
        	$objeto->titulo = 'Regular';
			$objeto->detalhes = '<label style="color:green; font-weight: bold;">Nenhum termo de intimação encontrado para esta empresa</label>';
			$objeto->link = base_url("termo_intimacao/listar/").$empresa->cnpj;

			return $objeto;

		}else{
			$objeto = new stdClass();
			$objeto->tam = '35px';

			$objeto->is_ok = 'pendencia';
			$objeto->titulo = 'Irregular';
			$objeto->detalhes = '<label style="color:red; font-weight: bold;">Foi encontrado um ou mais termo(s) de intimação para esta empresa</label>';
			$objeto->link = base_url("termo_intimacao/listar/").$empresa->cnpj;

			return $objeto;
		}
        	
    }

    //CERTIDAO
    public function certidao_estadual($empresa){
  //   	$resultado = $this->diagnostico_veri_model->certidao_estadual($empresa);

		// if(empty($resultado)){
		// 	$objeto = new stdClass();
  //       	$objeto->tam = '35px';

  //       	$objeto->is_ok = 'regular';
		// 	$objeto->detalhes = '<label style="color:green; font-weight: bold;">Regular - </label> nenhum termo de intimação encontrado para esta empresa';
		// 	$objeto->link = base_url("termo_intimacao/listar/").$empresa->cnpj;

		// 	return $objeto;

		// }else{
		// 	$objeto = new stdClass();
		// 	$objeto->tam = '35px';

		// 	$objeto->is_ok = 'pendencia';
		// 	$objeto->detalhes = '<label style="color:red; font-weight: bold;">Irregular - </label> foi encontrado um ou mais termo(s) de intimação para esta empresa';
		// 	$objeto->link = base_url("termo_intimacao/listar/").$empresa->cnpj;

		// 	return $objeto;
		// }
    }

    //E-PROCESSOS
    public function eprocessos($empresa){
    	$resultado = $this->diagnostico_veri_model->eprocessos($empresa);

    	if(empty($resultado)){
			$objeto = new stdClass();

        	$objeto->is_ok = 'alert';
        	$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("ecac/listar_eprocessos_todos");
			$objeto->tam = '30px';

			return $objeto;

		}else{
			$objeto = new stdClass();
			$objeto->tam = '35px';

			$objeto->titulo = 'e-Processos';
			$objeto->is_ok = 'eprocessos';
			$objeto->detalhes = '<label style="color:green; font-weight: bold;">Existem e-Processos ativos para esta empresa</label>';
			$objeto->link = base_url("ecac/listar_eprocessos_ativos/").$empresa->cnpj;

			return $objeto;
		}
    }

    //PARCELAMENTO DAS
    public function parcelamentos_das($empresa){
    	$resultado = $this->diagnostico_veri_model->parcelamento_das($empresa);

    	if(isset($resultado)){

    		$parcelas = $this->diagnostico_veri_model->listar_parcelas($empresa);
	        $parcelas_empresa = array();
	        $parcelas_empresas_aux = array();

	        foreach ($parcelas as $p) {
	        	$parcelas_empresas_aux = array();

	        	if(isset($parcelas_empresa[$p->cnpj])){
	        		array_push($parcelas_empresa[$p->cnpj], trim($p->data_parcela));
	        	}else{
	        		array_push($parcelas_empresas_aux, trim($p->data_parcela));
	        		$parcelas_empresa[$p->cnpj] = $parcelas_empresas_aux;
	        	}
	        }

	        //busca parcelas já pagas
	        $pagas = $this->diagnostico_veri_model->listar_parcelas_pagas($empresa);
	        $parcelas_pagas = array();
	        $parcelas_pagas_aux = array();

	        foreach ($pagas as $p) {
	        	$parcelas_pagas_aux = array();

	        	if(isset($parcelas_pagas[$p->cnpj])){
	        		array_push($parcelas_pagas[$p->cnpj], trim($p->mes_parcela));
	        	}else{
	        		array_push($parcelas_pagas_aux, trim($p->mes_parcela));
	        		$parcelas_pagas[$p->cnpj] = $parcelas_pagas_aux;
	        	}
	        }

	        $ha_parcelas_nao_pagas = false;

	        $resultado_final_listagem = array();

        	$parcelas_p = null;
        	$parcelas_np = null;
        	if(isset($parcelas_pagas[$resultado->cnpj])){
        		$parcelas_p = $parcelas_pagas[$resultado->cnpj];
        	}

        	if(isset($parcelas_empresa[$resultado->cnpj])){
        		$parcelas_np = $parcelas_empresa[$resultado->cnpj];
        	}
        	$this->verifica_se_ha_parcelas($resultado, $parcelas_p, $parcelas_np);

        	$this->calculaNumeroParcela($resultado);


        	if($resultado->parcelas_nao_pagas == "Não"){
    			$objeto = new stdClass();
	        	$objeto->tam = '35px';

	        	$objeto->titulo = 'Regular';
	        	$objeto->is_ok = 'regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Não existem parcelas em atraso para essa empresa</label>';
				$objeto->link = base_url("parcelamento_das/listar/").$empresa->cnpj;

				return $objeto;
    		}else{
    			$objeto = new stdClass();
	        	$objeto->tam = '35px';

	        	$objeto->titulo = 'Irregular';
	        	$objeto->is_ok = 'pendencia';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Existem parcelas em atraso para essa empresa</label>';
				$objeto->link = base_url("parcelamento_das/listar/").$empresa->cnpj;

				return $objeto;
    		}

    	}else{
    		$objeto = new stdClass();

    		$objeto->titulo = 'Nada Consta';
        	$objeto->is_ok = 'alert';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("parcelamento_das/listar/");
			$objeto->tam = '30px';

			return $objeto;
    	}
    }


    public function verifica_se_ha_parcelas($item, $parcelas_pagas, $parccelas_nao_pagas){
		$item->detalhes_parcelas_nao_pagas = "";

		if($parcelas_pagas == null && $parccelas_nao_pagas == null){
			$item->parcelas_nao_pagas = "Não";
		}elseif($parcelas_pagas == null && $parccelas_nao_pagas != null){
			$item->parcelas_nao_pagas = "Sim";
		}elseif($parcelas_pagas != null && $parccelas_nao_pagas == null){
			$item->parcelas_nao_pagas = "Não";
		}else{

			foreach ($parccelas_nao_pagas as $p) {
				if (in_array($p, $parcelas_pagas)) { 
				    $item->parcelas_nao_pagas = "Não";
				}else{
					$item->parcelas_nao_pagas = "Sim";

					$item->detalhes_parcelas_nao_pagas = $item->detalhes_parcelas_nao_pagas." | ".$p;
				}
			}

		}

	}

	public function calculaNumeroParcela($item){
		date_default_timezone_set('America/Sao_Paulo');
		$dia_atual_aux = date('j');
	  	$mes_atual = date('m');
	  	$ano_atual = date('Y');

		$dia_atual = date('d');

		$data_pedido = $item->data_pedido;
		$array = explode("/",$data_pedido);

		$data_final = '01'."/".$array[1]."/".$array[2];

		$data_parcelamento1 = str_replace('/', '-', $data_final);
		$data_parcelamento = date('Y-m-d', strtotime($data_parcelamento1));

		$data_atual = date('Y-m-d');

		$data1 = new DateTime( $data_parcelamento );
		$data2 = new DateTime( $data_atual );

		//Calcula o numero da parcela atual 1º, 2º....
		$intervalo = $data2->diff( $data1 );
		$item->parcela_atual_numero = $intervalo->m + ($intervalo->y * 12) + 1;

		if($item->parcela_atual_numero > $item->qtd_parcelas){
			$item->parcela_atual_numero = $item->qtd_parcelas;
			$item->mes_parcela = date('m/Y', strtotime("+".$item->qtd_parcelas." months", strtotime($data_parcelamento)));
		}else{
			$item->mes_parcela = $mes_atual."/".$ano_atual;
		}


		//Verifica se a parcela atual já foi paga
		$pagou_parcela_atual = $this->parcelamento_das_model->pagou_parcela_mes_atual($item->mes_parcela,$item->id);
		if(isset($pagou_parcela_atual)){
			$item->pagou_parcela_atual = true;
			$item->detalhes_pagamento = "Valor Pago: ".$pagou_parcela_atual->valor_pago."<br>"."Data de arrecadação: ".$pagou_parcela_atual->data_arrecadacao;
		}else{
			$item->pagou_parcela_atual = false;
			$item->parcela_atual = $this->parcelamento_das_model->get_parcela_atual($item->mes_parcela, $item->cnpj);
			$item->detalhes_pagamento = "Não Pago";
		}

		//Verifica se todas as parcelas já foram pagas
		$qtd_parcelas_pagas = $this->parcelamento_das_model->qtd_pagamentos($item->id);
		if($qtd_parcelas_pagas->qtd >= $item->qtd_parcelas){
			$item->situacao = "Concluído";
		}
	}


	//PARCELAMENTO PERT
	public function parcelamentos_pert($empresa){
		$resultado = $this->diagnostico_veri_model->parcelamento_pert($empresa);

		if(isset($resultado)){
			//busca parcelas que podem ser emitidas
	        $parcelas = $this->diagnostico_veri_model->listar_parcelas_pert($empresa);
	        $parcelas_empresa = array();
	        $parcelas_empresas_aux = array();

			
	        foreach ($parcelas as $p) {
	        	$parcelas_empresas_aux = array();

	        	if(isset($parcelas_empresa[$p->cnpj])){
	        		array_push($parcelas_empresa[$p->cnpj], trim($p->data_parcela));
	        	}else{
	        		array_push($parcelas_empresas_aux, trim($p->data_parcela));
	        		$parcelas_empresa[$p->cnpj] = $parcelas_empresas_aux;
	        	}
	        }

	        //busca parcelas já pagas
	        $pagas = $this->diagnostico_veri_model->listar_parcelas_pagas_pert($empresa);
	        $parcelas_pagas = array();
	        $parcelas_pagas_aux = array();

	        foreach ($pagas as $p) {
	        	$parcelas_pagas_aux = array();

	        	if(isset($parcelas_pagas[$p->cnpj])){
	        		array_push($parcelas_pagas[$p->cnpj], trim($p->mes_parcela));
	        	}else{
	        		array_push($parcelas_pagas_aux, trim($p->mes_parcela));
	        		$parcelas_pagas[$p->cnpj] = $parcelas_pagas_aux;
	        	}
	        }

	        $ha_parcelas_nao_pagas = false;

	        $resultado_final_listagem = array();
	    	$parcelas_p = null;
	    	$parcelas_np = null;
	    	if(isset($parcelas_pagas[$resultado->cnpj])){
	    		$parcelas_p = $parcelas_pagas[$resultado->cnpj];
	    	}

	    	if(isset($parcelas_empresa[$resultado->cnpj])){
	    		$parcelas_np = $parcelas_empresa[$resultado->cnpj];
	    	}
	    	$this->verifica_se_ha_parcelas_pert($resultado, $parcelas_p, $parcelas_np);

	    	
			$this->calculaNumeroParcela_pert($resultado);
			

	    	if($resultado->parcelas_nao_pagas == "Não"){
				$objeto = new stdClass();
	        	$objeto->tam = '35px';

	        	$objeto->titulo = 'Regular';
	        	$objeto->is_ok = 'regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Não existem parcelas em atraso para essa empresa</label>';
				$objeto->link = base_url("parcelamento_pert/listar/").$empresa->cnpj;

				return $objeto;
			}else{
				$objeto = new stdClass();
	        	$objeto->tam = '35px';

	        	$objeto->titulo = 'Irregular';
	        	$objeto->is_ok = 'pendencia';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Existem parcelas em atraso para essa empresa</label>';
				$objeto->link = base_url("parcelamento_pert/listar/").$empresa->cnpj;

				return $objeto;
			}
		}else{
			$objeto = new stdClass();

			$objeto->titulo = 'Nada Consta';
        	$objeto->is_ok = 'alert';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("parcelamento_pert/listar");
			$objeto->tam = '30px';

			return $objeto;
		}

	}


	public function verifica_se_ha_parcelas_pert($item, $parcelas_pagas, $parccelas_nao_pagas){
		$item->detalhes_parcelas_nao_pagas = "";

		if($parcelas_pagas == null && $parccelas_nao_pagas == null){
			$item->parcelas_nao_pagas = "Não";
		}elseif($parcelas_pagas == null && $parccelas_nao_pagas != null){
			$item->parcelas_nao_pagas = "Sim";
		}elseif($parcelas_pagas != null && $parccelas_nao_pagas == null){
			$item->parcelas_nao_pagas = "Não";
		}else{

			foreach ($parccelas_nao_pagas as $p) {
				if (in_array($p, $parcelas_pagas)) { 
				    $item->parcelas_nao_pagas = "Não";
				}else{
					$item->parcelas_nao_pagas = "Sim";

					$item->detalhes_parcelas_nao_pagas = $item->detalhes_parcelas_nao_pagas." | ".$p;
				}
			}

		}

	}

	public function calculaNumeroParcela_pert($item){

		date_default_timezone_set('America/Sao_Paulo');
		$dia_atual_aux = date('j');
	  	$mes_atual = date('m');
	  	$ano_atual = date('Y');

		$dia_atual = date('d');


		$data_pedido = $item->data_pedido;

		$array = explode("/",$data_pedido);

		$data_final = '01'."/".$array[1]."/".$array[2];


		$data_parcelamento1 = str_replace('/', '-', $data_final);
		$data_parcelamento = date('Y-m-d', strtotime($data_parcelamento1));

		$data_atual = date('Y-m-d');

		$data1 = new DateTime( $data_parcelamento );
		$data2 = new DateTime( $data_atual );

		//Calcula o numero da parcela atual 1º, 2º....
		$intervalo = $data2->diff( $data1 );
		$item->parcela_atual_numero = $intervalo->m + ($intervalo->y * 12) + 1;
		
		$item->mes_parcela = $mes_atual."/".$ano_atual;

		//Verifica se a parcela atual já foi paga
		$pagou_parcela_atual = $this->parcelamento_pert_model->pagou_parcela_mes_atual($item->mes_parcela,$item->id);
		if(isset($pagou_parcela_atual)){
			$item->pagou_parcela_atual = true;
			$item->detalhes_pagamento = "Valor Pago: ".$pagou_parcela_atual->valor_pago."<br>"."Data de arrecadação: ".$pagou_parcela_atual->data_arrecadacao;
		}else{
			$item->pagou_parcela_atual = false;
			$item->parcela_atual = $this->parcelamento_pert_model->get_parcela_atual($item->mes_parcela, $item->cnpj);
			$item->detalhes_pagamento = "Não Pago";
		}

		//Verifica se todas as parcelas já foram pagas
		$qtd_parcelas_pagas = $this->parcelamento_pert_model->qtd_pagamentos($item->id);
		if($qtd_parcelas_pagas->qtd >= $item->qtd_parcelas){
			$item->situacao = "Concluído";
		}
	}

	//PARCELAMENTO NÃO PREVIDENCIARIO
	public function parcelamentos_nao_prev($empresa){
		$dados['processos_negociados'] = $this->diagnostico_veri_model->listar_parcelmaneto_nao_previdenciario($empresa);

		$data_atual = date('d-m-Y');
		$parcelas_nao_pagas = array();

		$atraso = false;

		if(!empty($dados['processos_negociados'])){
			foreach ($dados['processos_negociados'] as $processo) {
				foreach ($this->parcelamento_nao_previdenciario_model->listar_tributos_negociados($processo->id) as $tributo_negociado) {
					foreach ($this->parcelamento_nao_previdenciario_model->listar_demonstrativo_das_parcelas($tributo_negociado->id) as $parcela) {
						//verifica as parcelas em atraso
						if(strtotime(str_replace('/', '-', $parcela->data_vencimento)) < strtotime($data_atual) && $parcela->situacao != 'Paga'){
							$parcela->nome_tributo = $tributo_negociado->tributo;
							$parcelas_nao_pagas[] = $parcela;
						}
						//pega data de vencimento de cada tributo do mes corrente
						if (str_replace('/', '-', substr($parcela->data_vencimento, 3 )) == date('m-Y')) {
							$parcelas_datas_vencimento[] = $parcela->data_vencimento.' ('.$tributo_negociado->tributo.')<br>';
						}
					}
				}
				$processo->parcelas_datas_vencimento = $parcelas_datas_vencimento;
				$processo->parcelas_nao_pagas = $parcelas_nao_pagas;

				if(count($processo->parcelas_nao_pagas) > 0){
					$atraso = true;
				}
			}

			if($atraso == false){
				$objeto = new stdClass();
	        	$objeto->tam = '35px';

	        	$objeto->titulo = 'Regular';
	        	$objeto->is_ok = 'regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Não existem parcelas em atraso para essa empresa </label>';
				$objeto->link = base_url("parcelamento_nao_previdenciario/listar/");

				return $objeto;
			}else{
				$objeto = new stdClass();
	        	$objeto->tam = '35px';

	        	$objeto->titulo = 'Irregular';
	        	$objeto->is_ok = 'pendencia';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Existem parcelas em atraso para essa empresa </label>';
				$objeto->link = base_url("parcelamento_nao_previdenciario/listar/");

				return $objeto;
			}
		}else{
			$objeto = new stdClass();

			$objeto->titulo = 'Nada Consta';
        	$objeto->is_ok = 'alert';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("parcelamento_nao_previdenciario/listar");
			$objeto->tam = '30px';

			return $objeto;
		}

	}


	//PARCELAMENTO LEI 12.996
	public function parcelamentos_lei($empresa){
		$dados['dividas_consolidadas'] = $this->diagnostico_veri_model->listar_parcelmaneto_lei($empresa);

		$data_atual = date('d-m-Y');
		$parcelas_nao_pagas = array();

		$atraso = false;

		if(!empty($dados['dividas_consolidadas'])){

	        //buscar parcela do mês atual e contar o total de parcelas 
	        foreach ($dados['dividas_consolidadas'] as $divida) {
	            foreach ($this->parcelamento_lei_12996_model->listar_demonstrativo_prestacoes($divida->id) as $parcela) {
	                if (substr($parcela->data_parcela, 0, -4)  == date('Y') && substr($parcela->data_parcela, 4, -2) == date('m')) {
	                    $parcela->vencimento = $this->formata_data_dia_mes_ano($parcela->data_parcela);
	                    $divida->parcela_atual = $parcela;
	                    break;
	                }
	            }
	            $divida->parcelas_atrasadas = $this->diagnostico_veri_model->get_parcelas_vencidas($empresa)->qtd;
	            if($divida->parcelas_atrasadas > 0){
	            	$atraso = true;
	            }
	        }

			if($atraso == false){
				$objeto = new stdClass();
	        	$objeto->tam = '35px';
	        	$objeto->titulo = 'Regular';
	        	$objeto->is_ok = 'regular';
				$objeto->detalhes = '<label style="color:green; font-weight: bold;">Não existem parcelas em atraso para essa empresa</label>';
				$objeto->link = base_url("parcelamento_lei_12996/listar/");

				return $objeto;
			}else{
				$objeto = new stdClass();
	        	$objeto->tam = '35px';
	        	$objeto->titulo = 'Irregular';
	        	$objeto->is_ok = 'pendencia';
				$objeto->detalhes = '<label style="color:red; font-weight: bold;">Existem parcelas em atraso para essa empresa</label>';
				$objeto->link = base_url("parcelamento_lei_12996/listar/");

				return $objeto;
			}
		}else{
			$objeto = new stdClass();

        	$objeto->is_ok = 'alert';
        	$objeto->titulo = 'Nada Consta';
			$objeto->detalhes = '<label style="color:blue; font-weight: bold;"> Nada consta </label>';
			$objeto->link = base_url("parcelamento_lei_12996/listar");
			$objeto->tam = '30px';

			return $objeto;
		}

	}

	private function formata_data_dia_mes_ano($data)
    {
        $dia = substr($data, 6);
        $ano = substr($data, 0, -4);
        $mes = substr($data, 4, -2);

        return $dia . '/' . $mes . '/' . $ano;
    }


}