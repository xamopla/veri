<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Dte extends MY_Controller {

	public function index(){
		$this->load->view('empresas');
	}

	public function get_mensagens($login, $senha){
		$this->load->model('dte_model');
		$table = $this->dte_model->main($login, $senha);
		echo $table;
	}

	public function ler_mensagem($login, $senha, $id){
		$this->load->model('dte_model');
		echo ($this->dte_model->ler_mensagem($login, $senha, $id));
	}

	public function get_pdf($login, $senha, $id){
		$this->load->model('dte_model');
		echo ($this->dte_model->get_pdf($login, $senha, $id));
	}

}