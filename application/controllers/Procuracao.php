<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procuracao extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('procuracao_model');
    }    

	public function listar(){
		$id_empresa = $this->uri->segment(3);
		$razao = "";
		if($id_empresa != null){
			$em = $this->empresa_model->find_empresa_by_id_empresa($id_empresa);
			$razao = $em->razao_social;
		}
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$consulta = $this->procuracao_model->listar();
		$empresas_cadastradas = $this->procuracao_model->empresas_cadastradas();
		$myhashmap = array();
		$resultado_final = array();

		foreach ($empresas_cadastradas as $e) {
			$myhashmap[$e->cnpj] = $e;
		}

		foreach ($consulta as $c) {
			$cnpj_numero = $this->deixarNumero($c->cnpj_outorgante);
			if(isset($myhashmap[$cnpj_numero])){
				$a = $this->procuracao_model->buscar_ultima_proc($c->cnpj_outorgante);
				array_push($resultado_final, $a);
			}
		}

		$dados['consulta'] = $resultado_final;
		$dados['filtro'] = "listar";

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');		
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/procuracao_listar', $dados);		
		$this->load->view('layout/footer');
		
	}

	public function vencidas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$resultado = $this->procuracao_model->listar();

		$empresas_cadastradas = $this->procuracao_model->empresas_cadastradas();
		$myhashmap = array();
		$resultado_final = array();

		foreach ($empresas_cadastradas as $e) {
			$myhashmap[$e->cnpj] = $e;
		}

		foreach ($resultado as $c) {
			$cnpj_numero = $this->deixarNumero($c->cnpj_outorgante);
			if(isset($myhashmap[$cnpj_numero])){
				$a = $this->procuracao_model->buscar_ultima_proc($c->cnpj_outorgante);
				array_push($resultado_final, $a);
			}
		}

		$array = [];

		foreach ($resultado_final as $c) {
			$data_banco = explode("/", $c->data_fim);
            $data_final_format = $data_banco[2]."-".$data_banco[1]."-".$data_banco[0];

            $data_final = strtotime($data_final_format);
            $data_atual = strtotime(date(''). ' + 30 days');

            $df = new DateTime($data_final_format);
            $da = new DateTime(date(''));
            $intervalo = $df->diff($da);

            if ($data_final < strtotime(date('Y-m-d'))) {
            	array_push($array, $c);
            } 
		}

		$dados['consulta'] = $array;
		$dados['filtro'] = 'vencidas';

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');		
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/procuracao_listar', $dados);		
		$this->load->view('layout/footer');
	}


	public function proximas_vencer(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$resultado = $this->procuracao_model->listar();


		$empresas_cadastradas = $this->procuracao_model->empresas_cadastradas();
		$myhashmap = array();
		$resultado_final = array();

		foreach ($empresas_cadastradas as $e) {
			$myhashmap[$e->cnpj] = $e;
		}

		foreach ($resultado as $c) {
			$cnpj_numero = $this->deixarNumero($c->cnpj_outorgante);
			if(isset($myhashmap[$cnpj_numero])){
				$a = $this->procuracao_model->buscar_ultima_proc($c->cnpj_outorgante);
				array_push($resultado_final, $a);
			}
		}
		
		$array = [];

		foreach ($resultado_final as $c) {
			$data_banco = explode("/", $c->data_fim);
            $data_final_format = $data_banco[2]."-".$data_banco[1]."-".$data_banco[0];

            $data_final = strtotime($data_final_format);
            $data_atual = strtotime(date(''). ' + 30 days');

            $df = new DateTime($data_final_format);
            $da = new DateTime(date(''));
            $intervalo = $df->diff($da);

            if ( ($intervalo->format('%a') <= 30) && ($data_final > strtotime(date('Y-m-d'))) ) {
            	array_push($array, $c);
            }

		}

		$dados['consulta'] = $array;
		$dados['filtro'] = 'proximas_vencer';
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');		
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/procuracao_listar', $dados);		
		$this->load->view('layout/footer');
	}

	function regulares(){
		$id_empresa = $this->uri->segment(3);
		$razao = "";
		if($id_empresa != null){
			$em = $this->empresa_model->find_empresa_by_id_empresa($id_empresa);
			$razao = $em->razao_social;
		}
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$consulta = $this->procuracao_model->listar();
		$empresas_cadastradas = $this->procuracao_model->empresas_cadastradas();
		$myhashmap = array();
		$resultado_final = array();

		foreach ($empresas_cadastradas as $e) {
			$myhashmap[$e->cnpj] = $e;
		}

		foreach ($consulta as $c) {
			$cnpj_numero = $this->deixarNumero($c->cnpj_outorgante);
			if(isset($myhashmap[$cnpj_numero])){
				$a = $this->procuracao_model->buscar_ultima_proc($c->cnpj_outorgante);
				array_push($resultado_final, $a);
				
			}
		}

		$array = [];

		foreach ($resultado_final as $c) {
			$data_banco = explode("/", $c->data_fim);
            $data_final_format = $data_banco[2]."-".$data_banco[1]."-".$data_banco[0];

            $data_final = strtotime($data_final_format);
            $data_atual = strtotime(date(''). ' + 30 days');

            $df = new DateTime($data_final_format);
            $da = new DateTime(date(''));
            $intervalo = $df->diff($da);

            if ($data_final > strtotime(date('Y-m-d'))) {
            	array_push($array, $c);
            } 
		}

		$dados['consulta'] = $array;
		$dados['filtro'] = "regulares";

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');		
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/procuracao_listar', $dados);		
		$this->load->view('layout/footer');
		
	}

	function deixarNumero($string){
	  return preg_replace("/[^0-9]/", "", $string);
	}
}
