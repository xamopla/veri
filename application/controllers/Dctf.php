<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Dctf extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('dctf_model');
        $this->load->model('empresa_model');
    }

	public function index(){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;

		if(isset($_GET['filtro'])){
			$filtro = $_GET["filtro"];
		}else{
			$filtro = 'TODAS';
		}
		
		$this->dctf_model->setFiltro_situacao($filtro);

		$dados['filtro'] = $filtro;
		
		$botao = $this->uri->segment(3);
		if($botao != null){

			$mes = $this->uri->segment(4);
			$mes_numero = 1;
			$ano = $this->uri->segment(5);

			switch ($mes) {
		        case "Janeiro":    $mes_numero = 1;     break;
		        case "Fevereiro":    $mes_numero = 2;   break;
		        case "Marco":    $mes_numero = 3;       break;
		        case "Abril":    $mes_numero = 4;       break;
		        case "Maio":    $mes_numero = 5;        break;
		        case "Junho":    $mes_numero = 6;       break;
		        case "Julho":    $mes_numero = 7;       break;
		        case "Agosto":    $mes_numero = 8;      break;
		        case "Setembro":    $mes_numero = 9;    break;
		        case "Outubro":    $mes_numero = 10;     break;
		        case "Novembro":    $mes_numero = 11;    break;
		        case "Dezembro":    $mes_numero = 12;    break; 
		 	} 

			if($botao == "anterior"){
				$mes_numero--;
				if($mes_numero == 0){
					$mes_numero = 12;
					$ano--;
				}
			}else{
				$mes_numero++;
				if($mes_numero == 13){
					$mes_numero = 1;
					$ano++;
				}
			}
			$this->dctf_model->setFiltroMes($mes_numero);
			$this->dctf_model->setFiltroAno($ano);

			if($mes_numero < 10){
				$mes_aux = '0'.$mes_numero;
			}else{
				$mes_aux = $mes_numero;
			}
			
			$ano_aux = $ano;

			$resultados = $this->dctf_model->listarFiltro();

			$dados['m'] = $mes_numero;
			$dados['ano'] = $ano;

		} elseif (isset($_POST['mes_selecionado'])) {

			$mes_selecionado = $this->input->post("mes_selecionado");
			$ano_selecionado = $this->input->post("ano_selecionado");
			
		 	$this->dctf_model->setFiltroMes($mes_selecionado);
			$this->dctf_model->setFiltroAno($ano_selecionado);

			$mes_aux = $mes_selecionado;
			$ano_aux = $ano_selecionado;

			$resultados = $this->dctf_model->listarFiltroByBotaoMes();
			$dados['m'] = $mes_selecionado;
			$dados['ano'] = $ano_selecionado;
			
		} else{

			$resultados = $this->dctf_model->listar();

			$data_atual = date('Y-m-d');

			$data_aux = date('Y-m-d', strtotime($data_atual.' - 2 months')); 

			$arr = explode("-", $data_aux);
			$mes_ = $arr[1];
			$ano_ = $arr[0];

			$dados['m'] = $mes_;
			$dados['ano'] = $ano_;

			$mes_aux = $mes_;
			$ano_aux = $ano_;
		}
		
		$ultimo_dia_transmitir = $this->getDiaUtil(15, $mes_aux+2, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 2 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;

		//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
		$data_final = strtotime($data_aux);

        $df = new DateTime($data_aux);
        $da = new DateTime(date(''));
        $intervalo = $df->diff($da);

        $fora_prazo = false;

        if ($data_final < strtotime(date('Y-m-d'))) {
          $fora_prazo = true;
        } else{
          $fora_prazo = false;
        }

        $mes_filtro_aux = ltrim($dados['m'], "0");
        $ano_filtro_aux = $dados['ano'];

        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

        $sem_movimento = $this->dctf_model->buscar_empresas_sem_movimento($data_filtro_aux);
        $myhashmap = array();
        foreach ($sem_movimento as $d) {
			$myhashmap[$d->cnpj] = $d;
		}

		$resultado_final = array();

		$array_empresas_sem_mov = array();

        foreach ($resultados as $r) {
        	if($fora_prazo == false){
        		if(isset($myhashmap[$r->cnpj])){
        			$r->status_geral = "SEM_MOVIMENTO";
        			$r->texto = "Analisado por: ".$myhashmap[$r->cnpj]->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($myhashmap[$r->cnpj]->data_alteracao));
        			$r->data_recepcao = date('d/m/Y', strtotime($myhashmap[$r->cnpj]->data_alteracao));
        		}elseif($r->sem_dctf == 1){
        			$r->status_geral = "TRANSMITIDA";
        		}elseif($r->sem_dctf == 0){
        			$r->status_geral = "EM_ANALISE";

        			array_push($array_empresas_sem_mov,$r);
        		}
        	}else{
        		if(isset($myhashmap[$r->cnpj])){
        			$r->status_geral = "SEM_MOVIMENTO";
        			$r->texto = "Analisado por: ".$myhashmap[$r->cnpj]->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($myhashmap[$r->cnpj]->data_alteracao));
        			$r->data_recepcao = date('d/m/Y', strtotime($myhashmap[$r->cnpj]->data_alteracao));
        		}elseif($r->sem_dctf == 1){
        			$r->status_geral = "TRANSMITIDA";
        		}elseif($r->sem_dctf == 0){
        			$r->status_geral = "VENCIDA";

        			array_push($array_empresas_sem_mov,$r);
        		}
        	}

        	if($filtro != null && $filtro != "TODAS"){
				if($filtro == "REGULAR"){
					if($r->status_geral == "TRANSMITIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "PENDENTE") {
					if($r->status_geral == "VENCIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "ANALISAR") {
					if($r->status_geral == "EM_ANALISE"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "SEM_MOVIMENTO") {
					if($r->status_geral == "SEM_MOVIMENTO"){
						array_push($resultado_final, $r);
					}
				}
			}else{
				array_push($resultado_final, $r);
			}

        }


		$dados['consulta'] = $resultado_final;

		$dados['array_empresas_sem_mov'] = $array_empresas_sem_mov;

		$dados['id_usuario_logado'] = $this->session->userdata['userprimesession']['id'];
		
		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;

		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/dctf', $dados);
		$this->load->view('layout/footer');	
	}

	public static function getDiaUtil($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
		date_default_timezone_set('America/Sao_Paulo');
		$ano = $iAno;
		switch ($iMes) {
			case '01':
				$aDiasIgnorar = ['1'];
				break;
			case '02':
				$aDiasIgnorar = ['17'];
				break;
			case '03':
				$aDiasIgnorar = array();
				break;
			case '04':
				$aDiasIgnorar = ['2','21'];
				break;
			case '05':
				$aDiasIgnorar = ['1'];
				break;
			case '06':
				$aDiasIgnorar = ['3'];
				break;
			case '07':
				$aDiasIgnorar = array();
				break;
			case '08':
				$aDiasIgnorar = array();
				break;
			case '09':
				$aDiasIgnorar = ['7'];
				break;
			case '10':
				$aDiasIgnorar = ['12'];
				break;
			case '11':
				$aDiasIgnorar = ['2','15'];
				break;
			case '12':
				$aDiasIgnorar = ['25'];
				break;
			default:
				$aDiasIgnorar = array();
				break;
		}

		$iMes = empty($iMes) ? date('m') : $iMes;
		$iAno = empty($iAno) ? date('Y') : $iAno;
		$iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));
 
		for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
			$iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
			//inclui apenas os dias úteis
			if ($iDiaSemana < 6) {
				$aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
			}
		}
		//ignorando os feriados	
		if (sizeof($aDiasIgnorar) > 0) {
			foreach ($aDiasIgnorar as $iDiaa) {
				$iKey = array_search($iDiaa, $aDias);
				unset($aDias[$iKey]);
			}
		}

		$array_aux = array_values( $aDias );
		
		if (isset($array_aux[$iDia-1])) {
			return $array_aux[$iDia-1];
		} else {
			//retorna o último dia útil
			return $aDias[count($aDias) - 1];
		}
	}


	public function list_from_notification(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;

		$resultados = $this->dctf_model->listar();

		$data_atual = date('Y-m-d');

		$data_aux = date('Y-m-d', strtotime($data_atual.' - 2 months')); 

		$arr = explode("-", $data_aux);
		$mes_ = $arr[1];
		$ano_ = $arr[0];

		$dados['m'] = $mes_;
		$dados['ano'] = $ano_;

		$mes_aux = $mes_;
		$ano_aux = $ano_;
		
		$ultimo_dia_transmitir = $this->getDiaUtil(15, $mes_aux+2, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 2 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;
		$dados['consulta'] = $resultados;
		$dados['razao_social_filtro'] = $razao;

		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/dctf', $dados);
		$this->load->view('layout/footer');	
	}


	public function update_simples(){
		$id = $this->input->post('id');

		$this->dctf_model->update_simples($id);

		echo 1;
	}

	public function buscar_declaracao(){
		$cnpj = $this->input->post('cnpj');
		$periodo = $this->input->post('periodo');

		$this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        $cerficados = $this->certificadocontador_model->get();

        foreach ($cerficados as $cerficado_contador) {

            $params = array('caminho_certificado' => 'crons-api/'.str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => '');

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()) {
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj);

            if( $empresa ){
                
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($cnpj);
                
                if(! $validado){
                    echo "ERRO2";
                    continue;
                }

                $caminho_download = $this->ecac_robo_library_eprocessos_procuracao->get_dctf_declaracao_individual($periodo);
                if($caminho_download != ""){
                	$this->dctf_model->update_caminho_download($caminho_download , $periodo, $cnpj);
                	unset($this->ecac_robo_library_eprocessos_procuracao);
                	echo $caminho_download;
                }else{
                	unset($this->ecac_robo_library_eprocessos_procuracao);
                	echo "ERRO";
                }

                break;
            }
        }

        unset($this->ecac_robo_library_eprocessos_procuracao);
	}

	public function marcar_sem_movimento(){
		$cnpj = $this->input->post('cnpj');
		$mes = $this->input->post('mes');
		$ano = $this->input->post('ano');

		$mes_filtro_aux = ltrim($mes, "0");
		$data_aux = $mes_filtro_aux."/".$ano;

		$this->dctf_model->marcar_sem_movimento($cnpj, $data_aux);

		$this->dctf_model->insere_notificacao($cnpj, $data_aux);

		echo 1;
	}


	public function listar_from_notificacao(){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;
		
		$this->dctf_model->setFiltro_situacao('TODAS');
		$filtro = "TODAS";
		
		$cnpj = $this->uri->segment(3);
		$dados['razao_social_filtro'] = $cnpj;

		if(true){

			$mes = $this->uri->segment(4);
			$mes_numero = 1;
			$ano = $this->uri->segment(5);

			switch ($mes) {
		        case "01":    $mes_numero = 1;     break;
		        case "02":    $mes_numero = 2;   break;
		        case "03":    $mes_numero = 3;       break;
		        case "04":    $mes_numero = 4;       break;
		        case "05":    $mes_numero = 5;        break;
		        case "06":    $mes_numero = 6;       break;
		        case "07":    $mes_numero = 7;       break;
		        case "08":    $mes_numero = 8;      break;
		        case "09":    $mes_numero = 9;    break;
		        case "10":    $mes_numero = 10;     break;
		        case "11":    $mes_numero = 11;    break;
		        case "12":    $mes_numero = 12;    break; 
		 	} 

			$this->dctf_model->setFiltroMes($mes_numero);
			$this->dctf_model->setFiltroAno($ano);

			if($mes_numero < 10){
				$mes_aux = '0'.$mes_numero;
			}else{
				$mes_aux = $mes_numero;
			}
			
			$ano_aux = $ano;

			$resultados = $this->dctf_model->listarFiltro();

			$dados['m'] = $mes_numero;
			$dados['ano'] = $ano;

		} 
		
		$ultimo_dia_transmitir = $this->getDiaUtil(15, $mes_aux+2, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 2 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;

		//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
		$data_final = strtotime($data_aux);

        $df = new DateTime($data_aux);
        $da = new DateTime(date(''));
        $intervalo = $df->diff($da);

        $fora_prazo = false;

        if ($data_final < strtotime(date('Y-m-d'))) {
          $fora_prazo = true;
        } else{
          $fora_prazo = false;
        }

        $mes_filtro_aux = ltrim($dados['m'], "0");
        $ano_filtro_aux = $dados['ano'];

        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

        $sem_movimento = $this->dctf_model->buscar_empresas_sem_movimento($data_filtro_aux);
        $myhashmap = array();
        foreach ($sem_movimento as $d) {
			$myhashmap[$d->cnpj] = $d;
		}

		$resultado_final = array();

        foreach ($resultados as $r) {
        	if($fora_prazo == false){
        		if(isset($myhashmap[$r->cnpj])){
        			$r->status_geral = "SEM_MOVIMENTO";
        			$r->texto = "Analisado por: ".$myhashmap[$r->cnpj]->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($myhashmap[$r->cnpj]->data_alteracao));
        			$r->data_recepcao = date('d/m/Y', strtotime($myhashmap[$r->cnpj]->data_alteracao));
        		}elseif($r->sem_dctf == 1){
        			$r->status_geral = "TRANSMITIDA";
        		}elseif($r->sem_dctf == 0){
        			$r->status_geral = "EM_ANALISE";
        		}
        	}else{
        		if(isset($myhashmap[$r->cnpj])){
        			$r->status_geral = "SEM_MOVIMENTO";
        			$r->nome_usuario = $myhashmap[$r->cnpj]->nome_usuario;
        			$r->data_usuario = $myhashmap[$r->cnpj]->data_alteracao;
        		}elseif($r->sem_dctf == 1){
        			$r->status_geral = "TRANSMITIDA";
        		}elseif($r->sem_dctf == 0){
        			$r->status_geral = "VENCIDA";
        		}
        	}

        	if($filtro != null && $filtro != "TODAS"){
				if($filtro == "REGULAR"){
					if($r->status_geral == "TRANSMITIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "PENDENTE") {
					if($r->status_geral == "VENCIDA"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "ANALISAR") {
					if($r->status_geral == "EM_ANALISE"){
						array_push($resultado_final, $r);
					}
				}elseif ($filtro == "SEM_MOVIMENTO") {
					if($r->status_geral == "SEM_MOVIMENTO"){
						array_push($resultado_final, $r);
					}
				}
			}else{
				array_push($resultado_final, $r);
			}

        }


		$dados['consulta'] = $resultado_final;

		$dados['id_usuario_logado'] = $this->session->userdata['userprimesession']['id'];
		
		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;

		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/dctf', $dados);
		$this->load->view('layout/footer');	
	}


	public function sem_movimento_lote(){
		$mes = $this->uri->segment(3);
		$ano = $this->uri->segment(4);

		$mes_filtro_aux = ltrim($mes, "0");
		$data_aux = $mes_filtro_aux."/".$ano;

		$empresas = $this->input->post('multi_select');

        $this->dctf_model->semMovimentoLote($empresas, $data_aux);

        redirect('dctf/');
	}
}