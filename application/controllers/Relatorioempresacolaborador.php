<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorioempresacolaborador extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model("empresa_model");
	}
	
	public function gerar() {
		
		$usuario = '';
		$this->load->model("usuario_model");
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$usuario = $this->usuario_model->findAll();
		
		if(isset($_POST['btn_pesquisar'])){
			
			$lista = array(); 
			$users = $this->input->post("id_usuario");

			$dados = $this->empresa_model->gerar_relatorio_empresa_colaborador($users);
			
			$this->relatoriogeral2($dados);
			
		} else {
			
			$usuarios = array();

			foreach ($usuario as $u){
				$usuarios["{$u->id}"] = $u->nome;
			}
			
			$dados['usuarios'] = $usuarios;
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('relatorio/relatorioempresacolaborador', $dados);
			$this->load->view('layout/footer');			
		}
		
	}

	public function relatoriogeral2($dados) {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
		$pdf->Ln(15);
					
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO DE EMPRESAS POR COLABORADOR',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Arial","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Arial","",7);
		
		//$this->load->model('empresa_model');
		//$result = $this->visita_model->relatorio_geral_vendas_em_andamento();
		
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
		
		$pdf->SetFont("Arial","B",9);
		
		//$pdf->Ln();
		$pdf->SetFont("Arial","",7);
		//$pdf->Write(5,'_____________________________________________________________________________________________________',0,1,"L");
		//$pdf->Ln();

		$colaboradorAux = "";
		$variavel_controle = true;
		foreach ($dados as $r) {
			
			if($variavel_controle){
				$pdf->SetFont("Arial","B",12);
				$pdf->Write(5,'COLABORADOR: '.$r->nome.'',0,1,"L");
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Write(5,'Razão Social; CNPJ; Inscrição Estadual',0,1,"L");
				$pdf->Ln();
				$pdf->Ln();
			}
			$variavel_controle = false;

			$pdf->SetFont("Arial","B",9);
			$pdf->Write(5, $r->razao_social.'; ',0,1,"L"); //$pdf->Ln();
			$pdf->Write(5, $r->cnpj_completo.'; ',0,1,"L"); //$pdf->Ln();
			$pdf->Write(5, $r->inscricao_estadual_completo.';',0,1,"L"); //$pdf->Ln();	
			
			//$pdf->Write(5,'Colaborador: '.$r->nome,0,1,"L"); $pdf->Ln();
			
			$pdf->Write(5,'_________________________________________________________________________________________________',0,1,"L");
			//$pdf->Ln();
			$pdf->Ln();			
		}
		
		$pdf->Output('Relatorio de Empresas por Colaborador - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}

	public function relatoriogeral($dados) {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
		$pdf->Ln(15);
					
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO DE EMPRESAS POR COLABORADOR',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Arial","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Arial","",7);
		
		//$this->load->model('empresa_model');
		//$result = $this->visita_model->relatorio_geral_vendas_em_andamento();
		
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
		
		$pdf->SetFont("Arial","B",9);
		
		//$pdf->Ln();
		$pdf->SetFont("Arial","",7);
		//$pdf->Write(5,'_____________________________________________________________________________________________________',0,1,"L");
		//$pdf->Ln();

		$colaboradorAux = "";
		foreach ($dados as $r) {
			
			if($colaboradorAux == ""){
				$pdf->SetFont("Arial","B",12);
				if($r->nome == null || $r->nome == ""){
					$pdf->Write(5,'COLABORADOR: '.$r->nome_aux.'',0,1,"L");
					$colaboradorAux = $r->nome_aux;
				}else{
					$pdf->Write(5,'COLABORADOR: '.$r->nome.'',0,1,"L");
					$colaboradorAux = $r->nome;
				}
				
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Write(5,'Razão Social; CNPJ; Inscrição Estadual',0,1,"L");
				$pdf->Ln();
				$pdf->Ln();
					
			}

			if($r->nome != null && $r->nome != ""){
				if($r->nome != $colaboradorAux){
					$pdf->Ln();
					$pdf->SetFont("Arial","B",12);
					if($r->nome == null || $r->nome == ""){
						$pdf->Write(5,'COLABORADOR: '.$r->nome_aux.'',0,1,"L");
						$colaboradorAux = $r->nome_aux;
					}else{
						$pdf->Write(5,'COLABORADOR: '.$r->nome.'',0,1,"L");
						$colaboradorAux = $r->nome;
					}
					$pdf->Ln();
					$pdf->Ln();
					$pdf->Write(5,'Razão Social; CNPJ; Inscrição Estadual',0,1,"L");
					$pdf->Ln();
					$pdf->Ln();
					
				}
			}else{
				if($r->nome_aux != $colaboradorAux){
					$pdf->Ln();
					$pdf->SetFont("Arial","B",12);
					if($r->nome == null || $r->nome == ""){
						$pdf->Write(5,'COLABORADOR: '.$r->nome_aux.'',0,1,"L");
						$colaboradorAux = $r->nome_aux;
					}else{
						$pdf->Write(5,'COLABORADOR: '.$r->nome.'',0,1,"L");
						$colaboradorAux = $r->nome;
					}
					$pdf->Ln();
					$pdf->Ln();
					$pdf->Write(5,'Razão Social; CNPJ; Inscrição Estadual',0,1,"L");
					$pdf->Ln();
					$pdf->Ln();
					
				}
			}
			

			$pdf->SetFont("Arial","B",9);
			$pdf->Write(5, $r->razao_social.'; ',0,1,"L"); //$pdf->Ln();
			$pdf->Write(5, $r->cnpj_completo.'; ',0,1,"L"); //$pdf->Ln();
			$pdf->Write(5, $r->inscricao_estadual_completo.';',0,1,"L"); //$pdf->Ln();	
			
			//$pdf->Write(5,'Colaborador: '.$r->nome,0,1,"L"); $pdf->Ln();
			
			$pdf->Write(5,'_________________________________________________________________________________________________',0,1,"L");
			//$pdf->Ln();
			$pdf->Ln();			
		}
		
		$pdf->Output('Relatorio de Empresas por Colaborador - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}
	
	public function gerarEmpresaSemColaborador(){
		$dados = $this->empresa_model->relatorio_empresa_sem_colaborador();

		$this->load->library("pdf");
		
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
		$pdf->Ln(15);
					
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO DE EMPRESAS SEM COLABORADOR',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Arial","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Arial","",7);
		
		//$this->load->model('empresa_model');
		//$result = $this->visita_model->relatorio_geral_vendas_em_andamento();
		
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
		
		$pdf->SetFont("Arial","B",9);
		
		//$pdf->Ln();
		$pdf->SetFont("Arial","",7);
		//$pdf->Write(5,'_____________________________________________________________________________________________________',0,1,"L");
		//$pdf->Ln();

		$colaboradorAux = "";
		foreach ($dados as $r) {
			
			if($colaboradorAux == ""){
				$pdf->SetFont("Arial","B",12);
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Write(5,'Razão Social; CNPJ; Inscrição Estadual',0,1,"L");
				$pdf->Ln();
				$pdf->Ln();	
				$colaboradorAux = "Item";
			}
			

			$pdf->SetFont("Arial","B",9);
			$pdf->Write(5, $r->razao_social.'; ',0,1,"L"); //$pdf->Ln();
			$pdf->Write(5, $r->cnpj_completo.'; ',0,1,"L"); //$pdf->Ln();
			$pdf->Write(5, $r->inscricao_estadual_completo.';',0,1,"L"); //$pdf->Ln();	
			
			//$pdf->Write(5,'Colaborador: '.$r->nome,0,1,"L"); $pdf->Ln();
			
			$pdf->Write(5,'_________________________________________________________________________________________________',0,1,"L");
			//$pdf->Ln();
			$pdf->Ln();			
		}
		
		$pdf->Output('Relatorio de Empresas sem Colaborador - '.date('Y-m-d H:i:s').'.pdf', 'I');

	}
}
