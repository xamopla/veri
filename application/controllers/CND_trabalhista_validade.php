<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CND_trabalhista_validade extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Declaracao_dirf_ausente/processar
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);
        $this->load->model('cnd_trabalhista_validade_model');

        $registros = $this->cnd_trabalhista_validade_model->busca_cnd($banco);
        $pdf    =  new PdfToText() ;

        
        foreach ($registros as $e) {

            if(!empty($e->caminho_download)){
                try{

                    $pdf->Load( $e->caminho_download ) ;

                    $cnpj = $e->cnpj;

                    $texto = nl2br($pdf -> Text) ;

                    //parte1 é a partir da string validade
                    $parte1 = explode("Validade:", $texto);

                    //parte2 é apenas a string da data de validade
                    $parte2 = explode("-", $parte1[1]);

                    $data_validade = trim($parte2[0]);

                    $this->cnd_trabalhista_validade_model->atualiza_validade_cnd_trabalhista($banco, $cnpj, $data_validade);

                }catch(Exception $x){
                    echo "ERRO AO PROCESSAR CNPJ - ".$e->cnpj;
                    echo '<br>';
                    continue;
                }
                
            }
            
        }
        
    }


    function deixarNumero($string){
      return preg_replace("/[^0-9]/", "", $string);
    }


    

}
