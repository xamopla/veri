<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notificacao extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('notificacao_model');
       	$this->load->model('dctf_model');
    }


    ////////////////////////LISTAGENS//////////////////////////////////
	public function mensagens(){
		$mensagens = $this->notificacao_model->mensagens();
		echo json_encode($mensagens);
	}

	public function estaduais(){
		$estaduais = $this->notificacao_model->estaduais();
		echo json_encode($estaduais);
	}

	public function federais(){
		$federais = $this->notificacao_model->federais();
		echo json_encode($federais);
	}

	public function certidoes(){
		$certidoes = $this->notificacao_model->certidoes();
		echo json_encode($certidoes);
	}

	public function processos(){
		$processos = $this->notificacao_model->processos();
		echo json_encode($processos);
	}

	public function calendario(){
		$eventos = $this->notificacao_model->calendario();
		echo json_encode($eventos);
	}

	public function atualizacoes(){
		$atualizacoes = $this->notificacao_model->atualizacoes();
		echo json_encode($atualizacoes);
	}

    public function parcelamentos(){
        $parcelamentos = $this->notificacao_model->parcelamentos();

        echo json_encode($parcelamentos);
    }

	////////////////////////MARCAÇÃO DE LIDA////////////////////////////
	public function marcar_msg_dte_lida() {
		$idnotificao = $this->input->post('id_notificacao');

		$this->notificacao_model->marcar_msg_dte_lida($idnotificao);

		echo 1;
	}

	public function marcar_msg_dte_lida2() {
		$idnotificao = $this->input->post('id_notificacao');

		$this->notificacao_model->marcar_msg_dte_lida2($idnotificao);

		echo 1;
	}

	public function marcar_msg_ecac_lida() {
		$idnotificao = $this->input->post('id_notificacao');

		$this->notificacao_model->marcar_msg_ecac_lida($idnotificao);

		echo 1;
	}

	public function marcar_situacao_ecac_lida(){
		$idnotificao = $this->input->post('id_notificacao');

		$this->notificacao_model->marcar_situacao_ecac_lida($idnotificao);

		echo 1;
	}

	public function marcar_situacao_cadin_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_situacao_cadin_lida($idnotificao);

		echo 1;
	}

	public function marcar_notificacao_certidao_federal(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_notificacao_certidao_federal($idnotificao);

		echo 1;
	}

	public function marcar_notificacao_certidao_estadual(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_notificacao_certidao_estadual($idnotificao);

		echo 1;
	}

	public function marcar_notificacao_certidao_sao_paulo(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_notificacao_certidao_sao_paulo($idnotificao);

		echo 1;
	}

	public function marcar_notificacao_certidao_caixa(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_notificacao_certidao_caixa($idnotificao);

		echo 1;
	}

	public function marcar_notificacao_certidao_trabalhista(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_notificacao_certidao_trabalhista($idnotificao);

		echo 1;
	}
	
	public function marcar_eprocesso_ativo_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_eprocesso_ativo_lida($idnotificao);

		echo 1;
	}

	public function marcar_eprocesso_inativo_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_eprocesso_inativo_lida($idnotificao);

		echo 1;
	}


	public function marcar_dctf_vencida_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_dctf_vencida_lida($idnotificao);

		echo 1;
	}

	public function marcar_dctf_proxima_vencer_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_dctf_proxima_vencer_lida($idnotificao);

		echo 1;
	}

	public function marcar_dctf_sem_movimento_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_dctf_sem_movimento_lida($idnotificao);

		echo 1;
	}

	public function marcar_pgdas_proxima_vencer_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_dpgdas_proxima_vencer_lida($idnotificao);

		echo 1;
	}


	public function marcar_pgdas_vencidas_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_pgdas_vencida_lida($idnotificao);

		echo 1;
	}


	public function marcar_das_nao_pagos_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_das_nao_pago_lida($idnotificao);

		echo 1;
	}

	public function marcar_sublimite_simples_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_sublimite_simples_lida($idnotificao);

		echo 1;
	}

	public function marcar_gfip_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_gfip_lida($idnotificao);

		echo 1;
	}

	public function marcar_malha_fiscal_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_malha_fiscal_lida($idnotificao);

		echo 1;
	}

	public function marcar_atualizacao_lida(){
		$idnotificao = $this->input->post('id_notificacao');
		
		$this->notificacao_model->marcar_atualizacoes_lida($idnotificao);

		echo 1;
	}

    public function marcar_parcelamento_das_status_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_das_status_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_das_pago_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_das_pago_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_pertsn_pago_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_pertsn_pago_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_pertsn_status_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_pertsn_status_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_nao_previdenciario_status_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_nao_previdenciario_status_lida($idnotificao);

        echo 1;
    }

	public function marcar_parcelamento_nao_previdenciario_em_atraso_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_nao_previdenciario_em_atraso_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_nao_previdenciario_pago_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_nao_previdenciario_pago_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_lei_12996_pago_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_lei_12996_pago_lida($idnotificao);

        echo 1;
    }

	public function marcar_parcelamento_lei_12996_em_atraso_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_lei_12996_em_atraso_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_lei_12996_status_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_lei_12996_status_lida($idnotificao);

        echo 1;
    }

	public function marcar_parcelamento_pert_rfb_pago_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_pert_rfb_pago_lida($idnotificao);

        echo 1;
    }

	public function marcar_parcelamento_pert_rfb_em_atraso_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_pert_rfb_em_atraso_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_pert_rfb_status_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_pert_rfb_status_lida($idnotificao);

        echo 1;
    }

	public function marcar_parcelamento_mei_pago_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_mei_pago_lida($idnotificao);

        echo 1;
    }

	public function marcar_parcelamento_mei_em_atraso_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_mei_em_atraso_lida($idnotificao);

        echo 1;
    }

    public function marcar_parcelamento_mei_status_lida(){
        $idnotificao = $this->input->post('id_notificacao');

        $this->notificacao_model->marcar_parcelamento_mei_status_lida($idnotificao);

        echo 1;
    }
}