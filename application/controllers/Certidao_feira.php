<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certidao_feira extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('certidao_feira_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_feira');
		$this->load->view('layout/footer');
		
	}

	public function listarAjax(){
		$certidoes = $this->certidao_feira_model->listar();
		echo json_encode($certidoes);
	}

}