<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dae extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
    }

	public function consultar_sefaz(){	

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id_empresa = $this->input->post('id_empresa');

		//RECEBENDO OS DADOS DA EMPRESA
		$dados_post = array(
			'login'=>$this->input->post('login'),
			'senha'=>$this->input->post('senha'),
			'ie'=> $this->input->post('ie'),
			'ano'=> $this->input->post('ano_sefaz')
		);

		$this->load->model('Dae_model');		

		//FAZENDO LOGIN NA SEFAZ E PEGANDO TODOS DOS DAES PAGOS E SUAS INFORMAÇÕES
		$daes_sefaz = $this->Dae_model->main($dados_post);

		//MONTANDO UM ARRAY RELACIONANDO AS INFORMAÇÕES 
		//DOS DAES COM OS SEUS NÚMEROS DE DAE
		$daes_sefaz_array = array();

		//ARRAY QUE IRÁ CONTER APENAS OS NUMEROS DOS DAES
		$daes_numeros = array();
		
		//PERCORRENDO TODO O ARRAY $daes_sefaz_array
		for($i=0; $i<($daes_sefaz->length-7) ; $i+=7){

			//APPENDANDO OS NUM DOS DAES NO ARRAY daes_numeros
			array_push($daes_numeros, $daes_sefaz[$i]->nodeValue);

			$dados_atual_dae_sefaz = array(				
				'num_doc'		=>$daes_sefaz[$i]->nodeValue,
            	'pagamento'		=>$daes_sefaz[$i+1]->nodeValue,
            	'referencia'	=>$daes_sefaz[$i+2]->nodeValue,
            	'cod_receita'	=>$daes_sefaz[$i+3]->nodeValue,
            	'receita'		=>$daes_sefaz[$i+4]->nodeValue,
            	'val_princ'		=>'R$ '.$daes_sefaz[$i+5]->nodeValue,
            	'val_total'		=>'R$ '.$daes_sefaz[$i+6]->nodeValue
			);

			//APPENDANDO OS DADOS DO DAE NO ARRAY $daes_sefaz_array
			array_push($daes_sefaz_array, $dados_atual_dae_sefaz);
		}
		
		//ARRAY COM OS DAES DO CLIENTE RECUPERADOS NO BANCO PERANTE A 
		//INSCRIÇÃO ESTADUAL DA EMPRESA NO MOMENTO DA PESQUISA
		$daes_cliente = $this->Dae_model->get_daes_bd($dados_post['ie'], $dados_post['ano']);

		//COMBINA O ARRAY DE NUMERO DOS DAES COM AS INFORMAÇOES DOS RESPECTIVOS DAES
		$combinacao = array_combine($daes_numeros, $daes_sefaz_array);

		//ARRAY QUE IRÁ RETORNAR PARA FORMAR A TABELA DE RESPOSTA
		$daes_retorno = array();
		
		//VERIFICANDO SE OS DAES RECUPERADOS NO BANCO 
		//SE ENCONTRAM NO ARRAY DOS DAES DA SEFAZ	
		//SÓ VAI EXISTIR CHAVE PAGAMENTO PARA OS QUE FORAM PAGOS
		foreach ($daes_cliente as $dae_cliente) {
			if (array_key_exists($dae_cliente->num_dae, $combinacao)) {
    			//DAE ENCONTRADO NOS DAES DA SEFAZ
    			//APPENDA AS INFORMAÇÕES DESSE DAE NO ARRAY DE RETORNO DA TABELA
    			array_push(
    				$daes_retorno, 
    				array_merge(
    					$combinacao[$dae_cliente->num_dae], 
    					(array)$dae_cliente, 
    					array("status"=>'<span class="badge badge-success badge-pill">OK</span>')
    				)
    			);
			}else{
				array_push(
					$daes_retorno, 
					array_merge(
						(array)$dae_cliente, 
						array(
							"status"=>'<span class="badge badge-primary badge-pill">Pendente</span>',
							"val_princ"=>'',
							"val_total"=>'',
							"pagamento"=>''
						)
					)
				);
			}
		}

		$daes_retorno = array('dados_tabela'=>$daes_retorno);

		$this->empresa_model->setId($id_empresa);

		$daes_retorno['empresa'] = $this->empresa_model->pesquisar_empresa_id(); 

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/servicos_dae', $daes_retorno);
		$this->load->view('layout/footer');
	}

	//VISUALIZAR FRONT UPLOAD DAES PDF
	public function upload_dae(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/dae_upload');
		$this->load->view('layout/footer');
	}

	//UPLOAD DOS DAES PDF PARA O BANCO
	public function upload(){
		$this->load->library('my_daepdf');
		$this->load->model('Dae_model');
		$dae = new My_daepdf;
		$dados_dae = $dae->read($_FILES['dae_file']);
		$this->Dae_model->insert_dae_bd($dados_dae);		
	}



}