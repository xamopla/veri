<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contador extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
        $this->load->model('contador_model');
        $this->load->model('log_model');
        $this->load->model('contadorprocuracao_model');
        $this->load->model('certificadocontador_model');
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function cadastrar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
			
		if(isset($_POST['btn_cadastrar'])){

			$this->contador_model->setNome($this->validar_input($this->input->post("nome")));
			$this->contador_model->setCrc($this->validar_input($this->input->post("crc")));
			$this->contador_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
				
			$result = $this->contador_model->cadastrar();
				
			if ($result){

				$this->session->set_flashdata('msg_alerta', 1);
				redirect('contador/listar');
			} else {

				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
				$this->session->set_flashdata('msg_tipo','danger');
			}

		}
				
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('contadores/contador_cadastrar');
		$this->load->view('layout/footer');
	
	}
	
	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->contador_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->contador_model->listar();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('contadores/contador_listar', $dados);
		$this->load->view('layout/footer');
		
	}
	
	public function excluir() {
	
		$id = $this->uri->segment(3);
		$id_contabilidade = $this->uri->segment(4);
	
		if ($id_contabilidade != '' || $id != ''){
				
			//VALIDAR SE O USUÁRIO É O DONO DA EMPRESA EM FOCO
			$validate = $this->validar_usuario($id_contabilidade);
				
			if ($validate){
	
				$this->contador_model->setId($id);
				$result = $this->contador_model->excluir();
	
				if ($result){
	
					$this->session->set_flashdata('msg_titulo','SUCESSO!');
					$this->session->set_flashdata('msg_conteudo','O contador foi excluído.');
					$this->session->set_flashdata('msg_tipo','success');
	
					redirect('contador/listar');
	
				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível excluir.');
					$this->session->set_flashdata('msg_tipo','danger');
	
					redirect('contador/listar');
	
				}
	
			} else {
				redirect('contador/listar');
			}
				
				
		}
		
		else {
			redirect('contador/listar');
		}
	
	}

	public function editar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
	
		if ($id != ''){			
				
				if(isset($_POST['btn_salvar'])){
				
					$this->contador_model->setId($id);
					$this->contador_model->setNome($this->validar_input($this->input->post("nome")));
					$this->contador_model->setCrc($this->validar_input($this->input->post("crc")));
				
					$result = $this->contador_model->editar();
						
					if ($result){

						$this->session->set_flashdata('msg_alerta', 1);						
						redirect('contador/listar');		
					} else {
						
						$this->session->set_flashdata('msg_alerta', 4);	
						redirect('contador/listar');
					}
				
				}
					
				$this->contador_model->setId($id);
				$dados['contador'] = $this->contador_model->pesquisar_contador_id();
					
				$dados['id'] = $id;
					
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('contadores/contador_editar', $dados);
				$this->load->view('layout/footer');
			
				
		} else {
			redirect("contador/listar");
		}
	
	}

    public function procuracao(){

        $id_contador = $this->uri->segment(3);

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        if(isset($_POST['btn_salvar'])){

            $this->contador_model->setId($id_contador);

            $empresas = $this->input->post('multi_select');

            $this->contador_model->atualizaListaEmpresaProcuracao($id_contador, $empresas);
        }

        $this->contador_model->setId($id_contador);
        $empresas = $this->empresa_model->buscar_para_vincular_aux($id_contador);
       

        $empresas_vinculadas = $this->contadorprocuracao_model->buscar_empresas_vinculadas($id_contador);
        $dados['empresas_vinculadas'] = $empresas_vinculadas;

        $myhashmap = array();
        foreach ($empresas_vinculadas as $item) {
        	$item->selected = 'selected';
            // $myhashmap[$item->id_empresa] = $item->id_empresa;
            array_push($empresas, $item);
        }

        // foreach ($empresas as $item) {
        //     if(isset($myhashmap[$item->id])){
        //         $item->selected = 'selected';
        //     }else{
        //         $item->selected = '';
        //     }
        // }

        $dados['empresas'] = $empresas;

        $dados['id'] = $id_contador;

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('contadores/contador_procuracao', $dados);
        $this->load->view('layout/footer');
    }

     public function cadastrar_novo(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
			
		if(isset($_POST['btn_cadastrar'])){

			$this->contador_model->setNome($this->validar_input($this->input->post("nome")));
			$this->contador_model->setCrc('1');
			$this->contador_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
				
			$result = $this->contador_model->cadastrar_novo();
				
			if ($result){

				$this->session->set_flashdata('msg_alerta', 1);
				redirect('contador/cadastrar_novo_etapa2/'.$result);
			} else {

				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
				$this->session->set_flashdata('msg_tipo','danger');
			}

		}
				
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('cadastro/cadastrar_procuracao_etapa1');
		$this->load->view('layout/footer');
	
	}


	public function cadastrar_novo_etapa2(){

		$id_contador = $this->uri->segment(3);
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
			
		if(isset($_POST['btn_cadastrar'])){

			$certificado = $this->certificadocontador_model->existe_certificado_contador($id_contador);
				
			if ($certificado->qtd > 0){
				redirect('contador/cadastrar_novo_etapa3/'.$id_contador);
			} else {

				$this->session->set_flashdata('msg_alerta', 2);
				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo','É necessário selecionar uma procuração');
				$this->session->set_flashdata('msg_tipo','danger');
				redirect('contador/cadastrar_novo_etapa2/'.$id_contador);
			}

		}
		
		$this->contador_model->setId($id_contador);
		$dados['contador'] = $this->contador_model->pesquisar_contador_id();
		$dados["certificado"] = $this->certificadocontador_model->get_status_certificado($id_contador);
		$dados['id'] = $id_contador;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('cadastro/cadastrar_procuracao_etapa2', $dados);
		$this->load->view('layout/footer');
	
	}


	public function cadastrar_novo_etapa3(){

        $id_contador = $this->uri->segment(3);

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        if(isset($_POST['btn_salvar'])){

            $this->contador_model->setId($id_contador);

            $empresas = $this->input->post('multi_select');

            if(!empty($empresas)){
            	$this->contador_model->atualizaListaEmpresaProcuracao($id_contador, $empresas);

                redirect('contador/listar/');
            }else{
            	$this->session->set_flashdata('msg_alerta',3);
            	redirect('contador/cadastrar_novo_etapa3/'.$id_contador);
            }
            
        }

        $this->contador_model->setId($id_contador);
        $empresas = $this->empresa_model->buscar_para_vincular_aux($id_contador);
       

        $empresas_vinculadas = $this->contadorprocuracao_model->buscar_empresas_vinculadas($id_contador);
        $dados['empresas_vinculadas'] = $empresas_vinculadas;

        $myhashmap = array();
        foreach ($empresas_vinculadas as $item) {
        	$item->selected = 'selected';
            // $myhashmap[$item->id_empresa] = $item->id_empresa;
            array_push($empresas, $item);
        }

        $dados['empresas'] = $empresas;

        $dados['id'] = $id_contador;

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('cadastro/cadastrar_procuracao_etapa3', $dados);
        $this->load->view('layout/footer');
    }
}
