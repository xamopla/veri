<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certidao_negativa_estadual extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('empresa_model');
        $this->load->model('documento_model');
        $this->load->model('resumofiscal_model');
        $this->load->model('certidao_negativa_estados_model');
        $this->load->model('certidao_negativa_model');
        $this->load->model('ecac_model');
    }

    public function listar(){

        $estado = $this->uri->segment(3);

        $dados['filtro_uf'] = $estado;
        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('certidao/certidao_debito_estadual', $dados);
        $this->load->view('layout/footer');       
    }

    public function listarEstadualAjax(){

        $filtro = $this->input->post('filtro_uf');
        // $filtro_uf = $estado;

        $this->certidao_negativa_estados_model->setFiltro($filtro);
        // $this->certidao_negativa_estados_model->setFiltro_uf($filtro_uf);

        switch ($filtro) {
            case 'AC':
                $estado = "AC";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_ac($filtro);
                break;

            case 'AL':
                $estado = "AL";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_al($filtro);
                break;
            
            case 'AP':
                $estado = "AP";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_ap($filtro);
                break;

            case 'AM':
                $estado = "AM";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_am($filtro);
                break;

            case 'BA':
                $estado = "BA";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_ba($filtro);
                break;

            case 'CE':
                $estado = "CE";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_ce($filtro);
                break;

            case 'DF':
                $estado = "DF";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_df($filtro);
                break;

            case 'ES':
                $estado = "ES";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_es($filtro);
                break;

            case 'GO':
                $estado = "GO";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_go($filtro);
                break;

            case 'MA':
                $estado = "MA";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_ma($filtro);
                break;

            case 'MS':
                $estado = "MS";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_ms($filtro);
                break;

            case 'MT':
                $estado = "MT";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_mt($filtro);
                break;

            case 'MG':
                $estado = "MG";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_mg($filtro);
                break;

            case 'PA':
                $estado = "PA";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_pa($filtro);
                break;

            case 'PB':
                $estado = "PB";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_pb($filtro);
                break;

            case 'PR':
                $estado = "PR";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_pr($filtro);
                break;

            case 'PE':
                $estado = "PE";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_pe($filtro);
                break;

            case 'PI':
                $estado = "PI";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_pi($filtro);
                break;

            case 'RJ':
                $estado = "RJ";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_rj($filtro);
                break;

            case 'RN':
                $estado = "RN";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_rn($filtro);
                break;

            case 'RS':
                $estado = "RS";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_rs($filtro);
                break;

            case 'RO':
                $estado = "RO";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_ro($filtro);
                break;

            case 'RR':
                $estado = "RR";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_rr($filtro);
                break;

            case 'SC':
                $estado = "SC";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_sc($filtro);
                break; 

            case 'SP':
                $estado = "SP";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_sp($filtro);
                break; 

            case 'SE':
                $estado = "SE";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_se($filtro);
                break;

            case 'TO':
                $estado = "TO";
                $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual_to($filtro);
                break; 

            default:
                $filtro = "Todas";
                $certidoes_estaduais = $this->certidao_negativa_model->listar_estadual($filtro);
                break;                 
        }      

        // $certidoes_estaduais = $this->certidao_negativa_estados_model->listar_estadual($filtro);

        date_default_timezone_set('America/Sao_Paulo');
        foreach ($certidoes_estaduais as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d')); 
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }
        echo json_encode($certidoes_estaduais);
    }
}