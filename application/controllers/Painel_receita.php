<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel_receita extends MY_Controller {

	public function index(){

		$dados = $this->notificacoes();

		$this->load->model('dividas_ativas_model');
		$this->load->model('empresa_model');
		$this->load->model('ecac_model');
		$this->load->model('certificado_model');
		$this->load->model('dctf_model');
		$this->load->model('procuracao_model');
		$this->load->model('pgdas_model');
		$this->load->model('das_model');
		$this->load->model('simples_nacional_model');
		$this->load->model('mei_model');
		$this->load->model('limite_simples_model');
		$this->load->model('regularize_model');
		$this->load->model('gfip_model');
		$this->load->model('ausencia_gfip_model');
		$this->load->model('ausencia_defis_model');
		$this->load->model('ausencia_dasn_model');
		$this->load->model('ausencia_efd_model');
		$this->load->model('ausencia_ecf_model');
		$this->load->model('ausencia_dirf_model');
		$this->load->model('ausencia_dctf_model');
		$this->load->model('ausencia_pgdas_model');
		$this->load->model('pronampe_model');
		$this->load->model('malha_fiscal_model');
		$this->load->model('notificacao_exclusao_model');
		$this->load->model('termo_intimacao_model');
		$this->load->model('socios_empresas_model');
		
		$resultado = $this->ecac_model->listar_caixa_postal();
		
		$somaMsgNaoLidas = 0;
		$qtdRegistros = 0;

		foreach ($resultado as $value) {
			$somaMsgNaoLidas = $somaMsgNaoLidas + $value->nao_lidas;
			$qtdRegistros++;
		}

		$dados['consulta'] = $resultado;
		$dados['msg_nao_lidas'] = $somaMsgNaoLidas;

		$empresas_sem_cert = $this->empresa_model->qtd_empresas_sem_certificado_digital();
		$qtd_empresas_sem_cert = 0;
		foreach ($empresas_sem_cert as $item) {
			$qtd_empresas_sem_cert++;
		}

		$dados['qtd_empresas_sem_certificado'] = $qtd_empresas_sem_cert;
		$dados['qtd_empresas_situacao_fiscal_pendencias'] = $this->ecac_model->qtd_empresas_situacao_fiscal_pendencias();
		$dados['qtd_empresas_com_parcelamento_simples_nacional'] = $this->ecac_model->qtd_empresas_com_parcelamento_simples_nacional();

		$dados['qtd_empresas_cadin'] = $this->ecac_model->qtd_empresas_cadin();
		$dados['qtd_empresas_cadin_regulares'] = $this->ecac_model->qtd_empresas_cadin_regulares();
		
		$dados['qtd_eprocessos'] = $this->ecac_model->qtd_eprocessos();
		
		$certificados = $this->certificado_model->listar_certificados_todos();
		$total_certificados_vencidos = 0;
		$total_certificados_proximos_vencer = 0;

		foreach ($certificados as $item) {
            $data_vencimento = date('Ymd', $item->data_validade);

            $diasNotificacao = 30;
            $df = new DateTime($data_vencimento);
            $da = new DateTime(date(''));

            $intervalo = $df->diff($da);
            if($intervalo->format('%a') <= $diasNotificacao && date('Ymd', $item->data_validade) > date('Ymd')){
                $total_certificados_proximos_vencer++;
            }else if(date('Ymd', $item->data_validade) < date('Ymd')){
            	$total_certificados_vencidos++;
            }

        }

		$dados['qtd_certificados_vencidos'] = $total_certificados_vencidos;
		$dados['qtd_certificados_proximos_vencer'] =  $total_certificados_proximos_vencer;


		$this->regularize_model->setFiltro('FGTS');
		$dados['qtd_empresas_divida_fgts'] = $this->regularize_model->qtd_empresas();

		$this->regularize_model->setFiltro('PREVIDENCIARIA');
		$dados['qtd_empresas_divida_previdencia'] = $this->regularize_model->qtd_empresas();

		$this->regularize_model->setFiltro('TRABALHISTA');
		$dados['qtd_empresas_divida_multatrabalhista'] = $this->regularize_model->qtd_empresas();

		$this->regularize_model->setFiltro('ELEITORAL');
		$dados['qtd_empresas_divida_multaeleitoral'] = $this->regularize_model->qtd_empresas();

		$this->regularize_model->setFiltro('CRIMINAL');
		$dados['qtd_empresas_divida_multacriminal'] = $this->regularize_model->qtd_empresas();

		$this->regularize_model->setFiltro('TRIBUTARIOS');
		$dados['qtd_empresas_divida_debitos_tributarios'] = $this->regularize_model->qtd_empresas();

		$this->regularize_model->setFiltro('NAO_TRIBUTARIO');
		$dados['qtd_empresas_divida_debitos_nao_tributarios'] = $this->regularize_model->qtd_empresas();

		// $dados['qtd_empresas_divida_fgts'] = $this->dividas_ativas_model->qtd_empresas_divida_fgts_ecac();
		// $dados['qtd_empresas_divida_previdencia'] = $this->dividas_ativas_model->qtd_empresas_divida_previdencia_ecac();
		// $dados['qtd_empresas_divida_nao_previdencia'] = $this->dividas_ativas_model->qtd_empresas_divida_nao_previdencia_ecac();
		// $dados['qtd_empresas_divida_multatrabalhista'] = $this->dividas_ativas_model->qtd_empresas_divida_multatrabalhista();
		// $dados['qtd_empresas_divida_multaeleitoral'] = $this->dividas_ativas_model->qtd_empresas_divida_multaeleitoral();
		// $dados['qtd_empresas_divida_multacriminal'] = $this->dividas_ativas_model->qtd_empresas_divida_multacriminal();

		/////////////////////Card do DCTF

		$dctf = $this->busca_card_dctf();
		
		$dados['qtd_dctf_vencidas'] = $dctf->qtd_vencidas;
		$dados['qtd_dctf_analise'] = $dctf->qtd_em_analise;
		$dados['qtd_dctf_transmitida'] = $dctf->qtd_transmitida;
		$dados['qtd_dctf_sem_movimento'] = $dctf->qtd_sem_movimento;

		////fim card DCTF


		//CARD PGDAS
		$pgdas = $this->busca_card_pgdas();

		$dados['qtd_pgdas_vencidas'] = $pgdas->qtd_vencidas;
		$dados['qtd_pgdas_transmitidas'] = $pgdas->qtd_transmitida;
		$dados['qtd_pgdas_analise'] = $pgdas->qtd_em_analise;
		$dados['qtd_pgdas_sem_movimento'] = $pgdas->qtd_sem_movimento;

		//FIM CARD PGDAS


		//CARD DAS
		$this->das_model->setFiltro_situacao('IRREGULAR');
		$dados['qtd_das'] = $this->das_model->qtd_dctf_pendente()->qtd;

		$this->das_model->setFiltro_situacao('DEBITOS');
		$dados['qtd_das_debitos'] = $this->das_model->qtd_dctf_pendente()->qtd;

		$this->das_model->setFiltro_situacao('REGULAR');
		$dados['qtd_das_pagos'] = $this->das_model->qtd_dctf_pendente()->qtd;

        $this->das_model->setFiltro_situacao('DUPLICIDADE');
        $dados['qtd_das_duplicidade'] = $this->das_model->qtd_das_duplicidade()->qtd;

        if (empty($dados['qtd_das_duplicidade']))
            $dados['qtd_das_duplicidade'] = 0;

        //FIM CARD DAS

		//////card procuracao/////////////////////////

		$procuracoes = $this->procuracao_model->listar();

		$empresas_cadastradas = $this->procuracao_model->empresas_cadastradas();
		$myhashmap_proc = array();
		$resultado_final_proc = array();

		foreach ($empresas_cadastradas as $e) {
			$myhashmap_proc[$e->cnpj] = $e;
		}

		foreach ($procuracoes as $c) {
			$cnpj_numero = $this->deixarNumero($c->cnpj_outorgante);
			if(isset($myhashmap_proc[$cnpj_numero])){
				$a = $this->procuracao_model->buscar_ultima_proc($c->cnpj_outorgante);
				array_push($resultado_final_proc, $a);
			}
		}

		$qtd_procuracoes_vencidas = 0;
		$qtd_procuracoes_proximas_vencer = 0;

		foreach ($resultado_final_proc as $c) {
			$data_banco = explode("/", $c->data_fim);
            $data_final_format = $data_banco[2]."-".$data_banco[1]."-".$data_banco[0];

            $data_final = strtotime($data_final_format);
            $data_atual = strtotime(date(''). ' + 30 days');

            $df = new DateTime($data_final_format);
            $da = new DateTime(date(''));
            $intervalo = $df->diff($da);

            if ($data_final < strtotime(date('Y-m-d'))) {
            	$qtd_procuracoes_vencidas++;
            }else if ($intervalo->format('%a') <= 30) {
            	$qtd_procuracoes_proximas_vencer++;
            } 
		}

		$dados['qtd_procuracoes_vencidas'] = $qtd_procuracoes_vencidas;
		$dados['qtd_procuracoes_proximas_vencer'] = $qtd_procuracoes_proximas_vencer;
		
		$dados['qtd_empresas_situacao_fiscal_pendencias'] = $this->ecac_model->qtd_empresas_situacao_fiscal_pendencias();
		$dados['qtd_empresas_situacao_fiscal_regular'] = $this->ecac_model->qtd_empresas_situacao_fiscal_regulares();
		//////fim card procuracao///////////////

		//card Simples Nacional
		$this->simples_nacional_model->setFiltro('EXCLUIDAS');
		$dados['qtd_simples_excluidas'] = $this->simples_nacional_model->qtd_excluidas_simples()->qtd;
		$this->simples_nacional_model->setFiltro('OPTANTES');
		$dados['qtd_simples_optantes'] = $this->simples_nacional_model->qtd_excluidas_simples()->qtd;
		//fim card Simples Nacional

		//card MEI
		$this->mei_model->setFiltro('EXCLUIDAS');
		$dados['qtd_mei_excluidas'] = $this->mei_model->qtd_excluidas_simei()->qtd;
		$this->mei_model->setFiltro('OPTANTES');
		$dados['qtd_mei_optantes'] = $this->mei_model->qtd_excluidas_simei()->qtd;
		//fim card MEI


		//CARD SUBLIMITE
		$sublimite = $this->card_limite();
		$dados['qtd_prox_limite_acumulado'] = $sublimite->qtd_proximos;
		$dados['qtd_limite_atingido_acumulado'] = $sublimite->qtd_extrapoladas;

		$this->limite_simples_model->setFiltro('PROXIMAS');
		$dados['qtd_prox_limite'] = $this->limite_simples_model->qtd()->qtd;
		$this->limite_simples_model->setFiltro('LIMITE');
		$dados['qtd_limite_atingido'] = $this->limite_simples_model->qtd()->qtd;
		//FIM CARD SUBLIMITE

		//CARD DIVERGENCIA GFIP x GPS
		$this->gfip_model->setFiltro('IRREGULAR');
		$dados['qtd_gfip_irregular'] = $this->gfip_model->qtd()->qtd;
		$this->gfip_model->setFiltro('REGULAR');
		$dados['qtd_gfip_regular'] = $this->gfip_model->qtd()->qtd;
		//FIM CARD DIVERGENCIA GFIP x GPS
		
		//CARD DIVERGENCIA GFIP x GPS
		$this->ausencia_gfip_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_gfip_irregular'] = $this->ausencia_gfip_model->qtd()->qtd;
		$this->ausencia_gfip_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_gfip_regular'] = $this->ausencia_gfip_model->qtd()->qtd;
		//FIM CARD DIVERGENCIA GFIP x GPS

		//CARD AUSENCIA DEFIS
		$this->ausencia_defis_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_defis_irregular'] = $this->ausencia_defis_model->qtd()->qtd;
		$this->ausencia_defis_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_defis_regular'] = $this->ausencia_defis_model->qtd()->qtd;
		//FIM CARD AUSENCIA DEFIS
		
		//CARD AUSENCIA DASN
		$this->ausencia_dasn_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_dasn_irregular'] = $this->ausencia_dasn_model->qtd()->qtd;
		$this->ausencia_dasn_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_dasn_regular'] = $this->ausencia_dasn_model->qtd()->qtd;
		//FIM CARD AUSENCIA DASN

		//CARD AUSENCIA EFD
		$this->ausencia_efd_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_efd_irregular'] = $this->ausencia_efd_model->qtd()->qtd;
		$this->ausencia_efd_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_efd_regular'] = $this->ausencia_efd_model->qtd()->qtd;
		//FIM CARD AUSENCIA EFD

		//CARD AUSENCIA ECF
		$this->ausencia_ecf_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_ecf_irregular'] = $this->ausencia_ecf_model->qtd()->qtd;
		$this->ausencia_ecf_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_ecf_regular'] = $this->ausencia_ecf_model->qtd()->qtd;
		//FIM CARD AUSENCIA ECF

		//CARD AUSENCIA DIRF
		$this->ausencia_dirf_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_dirf_irregular'] = $this->ausencia_dirf_model->qtd()->qtd;
		$this->ausencia_dirf_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_dirf_regular'] = $this->ausencia_dirf_model->qtd()->qtd;
		//FIM CARD AUSENCIA DIRF

		//CARD AUSENCIA DCTF
		$this->ausencia_dctf_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_dctf_irregular'] = $this->ausencia_dctf_model->qtd()->qtd;
		$this->ausencia_dctf_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_dctf_regular'] = $this->ausencia_dctf_model->qtd()->qtd;
		//FIM CARD AUSENCIA DCTF

		//CARD AUSENCIA PGDAS
		$this->ausencia_pgdas_model->setFiltro('IRREGULAR');
		$dados['qtd_ausencia_pgdas_irregular'] = $this->ausencia_pgdas_model->qtd()->qtd;
		$this->ausencia_pgdas_model->setFiltro('REGULAR');
		$dados['qtd_ausencia_pgdas_regular'] = $this->ausencia_pgdas_model->qtd()->qtd;
		//FIM CARD AUSENCIA PGDAS
		
		//PRONAMPE
		$dados['qtd_pronampe'] = $this->pronampe_model->qtd()->qtd;

		//CARD MALHA FISCAL
		$this->malha_fiscal_model->setFiltro('REGULARIZADO');
		$dados['qtd_malha_fiscal_regular'] = $this->malha_fiscal_model->qtd()->qtd;
		$this->malha_fiscal_model->setFiltro('PENDENCIAS');
		$dados['qtd_malha_fiscal_pendencia'] = $this->malha_fiscal_model->qtd()->qtd;
		$this->malha_fiscal_model->setFiltro('ANALISANDO');
		$dados['qtd_malha_fiscal_analise'] = $this->malha_fiscal_model->qtd()->qtd;
		$this->malha_fiscal_model->setFiltro('CLIENTE');
		$dados['qtd_malha_fiscal_cliente'] = $this->malha_fiscal_model->qtd()->qtd;
		//FIM MALHA FISCAL

		//CARD NOTIFICACAO EXCLUSAO SIMPLES
		$notificacao_exclusao = $this->card_notificacao_simples();
		$dados['qtd_notificacao_exclusao_regular'] = $notificacao_exclusao->qtd_regularizado;
		$dados['qtd_notificacao_exclusao_pendencia'] = $notificacao_exclusao->qtd_pendencias;
		$dados['qtd_notificacao_exclusao_analise'] = $notificacao_exclusao->qtd_analisando;
		$dados['qtd_notificacao_exclusao_cliente'] = $notificacao_exclusao->qtd_cliente;
		//FIM NOTIFICACAO EXCLUSAO SIMPLES


		//CARD TERMO DE INTIMAÇÃO
		$notificacao_intimacao = $this->card_intimacao();
		$dados['qtd_notificacao_intimacao_regular'] = $notificacao_intimacao->qtd_regularizado;
		$dados['qtd_notificacao_intimacao_pendencia'] = $notificacao_intimacao->qtd_pendencias;
		$dados['qtd_notificacao_intimacao_analise'] = $notificacao_intimacao->qtd_analisando;
		$dados['qtd_notificacao_intimacao_cliente'] = $notificacao_intimacao->qtd_cliente;
		//FIM TERMO DE INTIMAÇÃO
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dashboard_receita/dashboard_receita', $dados);
		$this->load->view('layout/footer');
	}

	public function busca_card_dctf(){
		
		$resultados = $this->dctf_model->listar();

		$data_atual = date('Y-m-d');

		$data_aux = date('Y-m-d', strtotime($data_atual.' - 2 months')); 

		$arr = explode("-", $data_aux);
		$mes_ = $arr[1];
		$ano_ = $arr[0];

		$dados['m'] = $mes_;
		$dados['ano'] = $ano_;

		$mes_aux = $mes_;
		$ano_aux = $ano_;


		$ultimo_dia_transmitir = $this->getDiaUtil(15, $mes_aux+2, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 2 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;

		//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
		$data_final = strtotime($data_aux);

        $df = new DateTime($data_aux);
        $da = new DateTime(date(''));
        $intervalo = $df->diff($da);

        $fora_prazo = false;

        if ($data_final < strtotime(date('Y-m-d'))) {
          $fora_prazo = true;
        } else{
          $fora_prazo = false;
        }

        $mes_filtro_aux = ltrim($dados['m'], "0");
        $ano_filtro_aux = $dados['ano'];

        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;
        
        $sem_movimento = $this->dctf_model->buscar_empresas_sem_movimento($data_filtro_aux);
        $myhashmap = array();
        foreach ($sem_movimento as $d) {
			$myhashmap[$d->cnpj] = $d;
		}

		$resultado_final = array();

		$qtd_transmitida = 0;
		$qtd_vencidas = 0;
		$qtd_sem_movimento = 0;
		$qtd_em_analise = 0;

        foreach ($resultados as $r) {
        	if($fora_prazo == false){
        		if(isset($myhashmap[$r->cnpj])){
        			$r->status_geral = "SEM_MOVIMENTO";
        			$qtd_sem_movimento++;
        		}elseif($r->sem_dctf == 1){
        			$r->status_geral = "TRANSMITIDA";
        			$qtd_transmitida++;
        		}elseif($r->sem_dctf == 0){
        			$r->status_geral = "EM_ANALISE";
        			$qtd_em_analise++;
        		}
        	}else{
        		if(isset($myhashmap[$r->cnpj])){
        			$r->status_geral = "SEM_MOVIMENTO";
        			$qtd_sem_movimento++;
        		}elseif($r->sem_dctf == 1){
        			$r->status_geral = "TRANSMITIDA";
        			$qtd_transmitida++;
        		}elseif($r->sem_dctf == 0){
        			$r->status_geral = "VENCIDA";
        			$qtd_vencidas++;
        		}
        	}

        }

        $objeto = new stdClass();
        $objeto->qtd_transmitida = $qtd_transmitida;
        $objeto->qtd_vencidas = $qtd_vencidas;
        $objeto->qtd_sem_movimento = $qtd_sem_movimento;
        $objeto->qtd_em_analise = $qtd_em_analise;

        return $objeto;
	}

	public function busca_card_pgdas(){
		$resultados = $this->pgdas_model->listar();

		$data_atual = date('Y-m-d');

		$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

		$arr = explode("-", $data_aux);
		$mes_ = $arr[1];
		$ano_ = $arr[0];

		$dados['m'] = $mes_;
		$dados['ano'] = $ano_;

		$mes_aux = $mes_;
		$ano_aux = $ano_;

		$ultimo_dia_transmitir = $this->get_days_uteis(20, $mes_aux+1, $ano_aux );
		//$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

		// $data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = $ultimo_dia_transmitir; 

		$dados['ultimo_dia_transmitir'] = $ultimo_dia_transmitir;

		//VALIDAÇÃO SE JÁ PASSOU DA DATA OU NÃO
		$data_final = strtotime($data_aux);

        $df = new DateTime($data_aux);
        $da = new DateTime(date(''));
        $intervalo = $df->diff($da);

        $fora_prazo = false;

        if ($data_final < strtotime(date('Y-m-d'))) {
          $fora_prazo = true;
        } else{
          $fora_prazo = false;
        }

        $mes_filtro_aux = ltrim($dados['m'], "0");
        $ano_filtro_aux = $dados['ano'];

        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

		$resultado_final = array();

		$qtd_transmitida = 0;
		$qtd_vencidas = 0;
		$qtd_sem_movimento = 0;
		$qtd_em_analise = 0;

		foreach ($resultados as $r) {
        	if($fora_prazo == false){

        		if($r->sem_pgdas == 1){
        			// $r->status_geral = "TRANSMITIDA";
        			$qtd_transmitida++;

        			if(empty($r->pago)){
    					// $r->sub_status_geral = "SEM_MOVIMENTO";
    					$qtd_sem_movimento++;
    				}

        		}elseif($r->sem_pgdas == 0){
        			// $r->status_geral = "EM_ANALISE";
        			$qtd_em_analise++;
        		}

        	}else{

        		if($r->sem_pgdas == 1){
        			// $r->status_geral = "TRANSMITIDA";
        			$qtd_transmitida++;

        			if(empty($r->pago)){
    					// $r->sub_status_geral = "SEM_MOVIMENTO";
    					$qtd_sem_movimento++;
    				}
        		
        		}elseif($r->sem_pgdas == 0){
        			// $r->status_geral = "VENCIDA";
        			$qtd_vencidas++;
        			
        		}
        	}
        }

        $objeto = new stdClass();
        $objeto->qtd_transmitida = $qtd_transmitida;
        $objeto->qtd_vencidas = $qtd_vencidas;
        $objeto->qtd_sem_movimento = $qtd_sem_movimento;
        $objeto->qtd_em_analise = $qtd_em_analise;

        return $objeto;

	}

	public function get_days_uteis($dia, $mes, $ano){
		if($mes == 13){
			$mes = '01';
			$ano++;
		}
		$tmpDate = $ano.'-'.$mes.'-'.$dia;


		$holidays = [$ano.'-01-01', $ano.'-02-17', $ano.'-04-02', $ano.'-04-21', $ano.'-05-01', $ano.'-06-03', $ano.'-09-07', $ano.'-10-12', $ano.'-11-02', $ano.'-11-15', $ano.'-12-25'];
		$i = 0;
		$nextBusinessDay = date('Y-m-d', strtotime($tmpDate . ' +' . $i . ' Weekday'));

		while (in_array($nextBusinessDay, $holidays)) {
		    $i++;
		    $nextBusinessDay = date('Y-m-d', strtotime($tmpDate . ' +' . $i . ' Weekday'));
		}
		return $nextBusinessDay;

	}
	
	public static function getDiaUtil($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
		date_default_timezone_set('America/Sao_Paulo');
		$ano = $iAno;
		switch ($iMes) {
			case '01':
				$aDiasIgnorar = ['1'];
				break;
			case '02':
				$aDiasIgnorar = ['17'];
				break;
			case '03':
				$aDiasIgnorar = array();
				break;
			case '04':
				$aDiasIgnorar = ['2','21'];
				break;
			case '05':
				$aDiasIgnorar = ['1'];
				break;
			case '06':
				$aDiasIgnorar = ['3'];
				break;
			case '07':
				$aDiasIgnorar = array();
				break;
			case '08':
				$aDiasIgnorar = array();
				break;
			case '09':
				$aDiasIgnorar = ['7'];
				break;
			case '10':
				$aDiasIgnorar = ['12'];
				break;
			case '11':
				$aDiasIgnorar = ['2','15'];
				break;
			case '12':
				$aDiasIgnorar = ['25'];
				break;
			default:
				$aDiasIgnorar = array();
				break;
		}

		$iMes = empty($iMes) ? date('m') : $iMes;
		$iAno = empty($iAno) ? date('Y') : $iAno;
		$iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));
 
		for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
			$iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
			//inclui apenas os dias úteis
			if ($iDiaSemana < 6) {
				$aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
			}
		}
		//ignorando os feriados	
		if (sizeof($aDiasIgnorar) > 0) {
			foreach ($aDiasIgnorar as $iDiaa) {
				$iKey = array_search($iDiaa, $aDias);
				unset($aDias[$iKey]);
			}
		}

		$array_aux = array_values( $aDias );
		
		if (isset($array_aux[$iDia-1])) {
			return $array_aux[$iDia-1];
		} else {
			//retorna o último dia útil
			return $aDias[count($aDias) - 1];
		}
	}

	function deixarNumero($string){
	  return preg_replace("/[^0-9]/", "", $string);
	}


	function card_notificacao_simples(){

		$resultado = $this->notificacao_exclusao_model->listar();

		$resultado_final = array();

		$qtd_analisando = 0;
		$qtd_pendencias = 0;
		$qtd_regularizado = 0;
		$qtd_cliente = 0;

		foreach ($resultado as $r) {
			
			$ultimo_historico = $this->montaHistorico($r);
			if(empty($ultimo_historico)){
				$r->situacao = "PENDENCIAS";
			}

			if($r->situacao == "REGULARIZADO"){
				$qtd_regularizado++;
			}

			if($r->situacao == "PENDENCIAS"){
				$qtd_pendencias++;
			}

			if($r->situacao == "ANALISANDO"){
				$qtd_analisando++;
			}

			if($r->situacao == "CLIENTE"){
				$qtd_cliente++;
			}

		}


		$objeto = new stdClass();
    $objeto->qtd_analisando = $qtd_analisando;
    $objeto->qtd_pendencias = $qtd_pendencias;
    $objeto->qtd_regularizado = $qtd_regularizado;
    $objeto->qtd_cliente = $qtd_cliente;

    return $objeto;

	}


	public function montaHistorico($r){
		$historico = $this->notificacao_exclusao_model->find_ultimo_historico($r);

		if(isset($historico)){
			$r->situacao = $historico->situacao;
			return "Alterado por: ".$historico->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($historico->data_alteracao));
		}else{
			return "";
		}

	}




	function card_intimacao(){

		$resultado = $this->termo_intimacao_model->listar();

		$resultado_final = array();

		$qtd_analisando = 0;
		$qtd_pendencias = 0;
		$qtd_regularizado = 0;
		$qtd_cliente = 0;

		foreach ($resultado as $r) {
			
			$ultimo_historico = $this->montaHistoricoIntimacao($r);
			if(empty($ultimo_historico)){
				$r->situacao = "PENDENCIAS";
			}

			if($r->situacao == "REGULARIZADO"){
				$qtd_regularizado++;
			}

			if($r->situacao == "PENDENCIAS"){
				$qtd_pendencias++;
			}

			if($r->situacao == "ANALISANDO"){
				$qtd_analisando++;
			}

			if($r->situacao == "CLIENTE"){
				$qtd_cliente++;
			}

		}


		$objeto = new stdClass();
    $objeto->qtd_analisando = $qtd_analisando;
    $objeto->qtd_pendencias = $qtd_pendencias;
    $objeto->qtd_regularizado = $qtd_regularizado;
    $objeto->qtd_cliente = $qtd_cliente;

    return $objeto;

	}


	public function montaHistoricoIntimacao($r){
		$historico = $this->termo_intimacao_model->find_ultimo_historico($r);

		if(isset($historico)){
			$r->situacao = $historico->situacao;
			return "Alterado por: ".$historico->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($historico->data_alteracao));
		}else{
			return "";
		}

	}



	public function card_limite(){

        $filtro = "TODAS";

        $this->limite_simples_model->setFiltro($filtro);

        $resultado = $this->limite_simples_model->buscar();
        $myhashmap_limites = array();
        foreach ($resultado as $r) {
            $myhashmap_limites[$r->cnpj] = $r;
        }

        $qtd_proximas = 0;
        $qtd_extrapoladas = 0;

        $array_final = array();
        foreach ($resultado as $r) {
            $this->soma_total_limite($r, $myhashmap_limites);

            $r->total_acumulado = $r->soma_total + $r->valor_atual;
            $r->percentual_acumulado = (int) ( ( ( (float) ($r->total_acumulado) ) / 4800000 ) * 100);

            if($r->percentual_acumulado >= 70 && $r->percentual_acumulado < 100){
              $qtd_proximas++;
            }

           	if($r->percentual_acumulado >= 100){
	            $qtd_extrapoladas++;
	          }
        }

        $objeto = new stdClass();
        $objeto->qtd_proximos = $qtd_proximas;
        $objeto->qtd_extrapoladas = $qtd_extrapoladas;

        return $objeto;
        
    }


    public function soma_total_limite($r, $myhashmap_limites){
        $socios = $this->socios_empresas_model->busca_socios($r->cnpj);
        $soma_total = 0;
        $r->soma_total = 0;

        $array_cpfs = array();
        $string_socios = "";
        if(!empty($socios)){

            foreach ($socios as $socio) {
                if(!empty($socio->nome_socio)){
                    if($string_socios == ""){
                        $string_socios = $socio->nome_socio." - ".$socio->cpf_cnpj;
                    }else{
                        $string_socios = $string_socios."<br>".$socio->nome_socio." - ".$socio->cpf_cnpj;
                    }
                    
                }

                array_push($array_cpfs, $socio->cpf_cnpj);
            }
            $socios_outras_empresas = $this->socios_empresas_model->busca_socios_outras_empresas($r->cnpj, $array_cpfs);
            
            foreach ($socios_outras_empresas as $s) {

                if(isset($myhashmap_limites[$s->cnpj_empresa])){
                    $a = $myhashmap_limites[$s->cnpj_empresa];
                    $soma_total = $soma_total + $a->valor_atual;
                    $r->soma_total = $soma_total;
                }
                
            }  
        }else{
          $string_socios = "QUADRO SOCIETÁRIO NÃO OBRIGATÓRIO POR SUA NATUREZA JURÍDICA";  
        }

        $r->socios_empresas_string = $string_socios;
        
    }

	
}