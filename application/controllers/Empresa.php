<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model', 'e');
		$this->load->model('resumofiscal_model');
		$this->load->model('plano_model');
        $this->load->model('log_model');
        $this->load->model('alvara_model');
        $this->load->model('limpa_empresa_model');
        $this->load->model('certificado_model'); 
        $this->load->model('certificadocontador_model'); 
    } 

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function cadastrar(){
		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		if(isset($_POST['btn_localizar'])){
			
			$ie = $this->input->post('inscricao_estadual');
			$cnpj = $this->input->post('cnpj');
			
			if ($ie != '' || $cnpj != ''){
				
				if ($ie){
					
					$ie = preg_replace("/[^0-9]/", "", $ie);
					
					$this->e->setInscricaoEstadual($ie);
					$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
					$verificacao = $this->e->verificar_cadastro_duplicado('ie');
					
					if ($verificacao){
						
						//msg que já está cadastrado					
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Empresa já cadastrada.');
						$this->session->set_flashdata('msg_tipo','danger');
						
					} else {
						redirect("empresa/finalizarcadastro/ie/$ie");
					}
					
				} else {
					
					$cnpj = preg_replace("/[^0-9]/", "", $cnpj);
					
					$this->e->setCnpj($cnpj);
					$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
					$verificacao = $this->e->verificar_cadastro_duplicado('cnpj');
						
					if ($verificacao){
						
						//msg que já está cadastrado
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Empresa já cadastrada.');
						$this->session->set_flashdata('msg_tipo','danger');
						
					} else {
						redirect("empresa/finalizarcadastro/cnpj/$cnpj");
					}
					
				}
				
			} else {				
				$this->session->set_flashdata('msg_titulo','ATENÇÃO!');
				$this->session->set_flashdata('msg_conteudo','Por favor informe um dos campos.');
				$this->session->set_flashdata('msg_tipo','warning');
			}
		
		}
			
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('empresa/empresa_cadastrar');
		$this->load->view('layout/footer');
	
	}

	public function cadastrar_empresa_servico(){ 

		if(isset($_POST['btn_localizar'])){

			$cnpj = $this->input->post('cnpj');
			
			if ($cnpj != ''){
					
					$cnpj = preg_replace("/[^0-9]/", "", $cnpj);
					
					$this->e->setCnpj($cnpj);
					$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
					$verificacao = $this->e->verificar_cadastro_duplicado('cnpj');
						
					if ($verificacao){
						
						//msg que já está cadastrado
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Empresa já cadastrada.');
						$this->session->set_flashdata('msg_tipo','danger');
						redirect("empresa/cadastrar");
						
					} else {
						redirect("empresa/finalizarcadastroempresaservico/cnpj/$cnpj");
					}				
				
			} else {				
				$this->session->set_flashdata('msg_titulo','ATENÇÃO!');
				$this->session->set_flashdata('msg_conteudo','Por favor informe um dos campos.');
				$this->session->set_flashdata('msg_tipo','warning');
				redirect("empresa/cadastrar");
			}
		
		} 
	}
	
	public function finalizarcadastro(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$tipo = $this->uri->segment(3);
		$valor = $this->uri->segment(4);
		
		if ($tipo != '' && $valor != ''){

			$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

			$funcionarios = $this->e->buscar_funcionarios();
			$lista_funcionarios = array('' => "Selecione o Funcionário Responsável");
			
			//LISTA AUXILIAR PARA O MULTI-SELECT
			$lista_funcionarios2 = array();				

			foreach ($funcionarios as $value) {
				
				$lista_funcionarios[$value->id] = $value->nome;
				$lista_funcionarios2[$value->id] = $value->nome;
			}

			$dados['lista_funcionarios'] = $lista_funcionarios;
			$dados['lista_funcionarios2'] = $lista_funcionarios2;

			if(isset($_POST['btn_cadastrar'])){
				
				$this->e->setInscricaoEstadualCompleto($this->validar_input($this->input->post("ie")));
				$this->e->setCnpjCompleto($this->validar_input($this->input->post("cnpj")));
				$this->e->setInscricaoEstadual($this->validar_input(preg_replace("/[^0-9]/", "", $this->input->post("ie"))));
				$this->e->setCnpj($this->validar_input(preg_replace("/[^0-9]/", "", $this->input->post("cnpj"))));
				$this->e->setRazaoSocial($this->validar_input($this->input->post("razao_social")));
				$this->e->setMei($this->validar_input($this->input->post("mei")));
				$this->e->setNomeFantasia($this->validar_input($this->input->post("nome_fantasia")));
				$this->e->setNaturezaJuridica($this->validar_input($this->input->post("natureza_juridica")));
				$this->e->setUnidadeAtendimento($this->validar_input($this->input->post("unidade_atendimento")));
				$this->e->setUnidadeFiscalizacao($this->validar_input($this->input->post("unidade_fiscalizacao")));
				$this->e->setCep($this->validar_input($this->input->post("cep")));
				$this->e->setLogradouro($this->validar_input($this->input->post("logradouro")));
				$this->e->setNumero($this->validar_input($this->input->post("numero")));
				$this->e->setComplemento($this->validar_input($this->input->post("complemento")));
				$this->e->setBairro($this->validar_input($this->input->post("bairro")));
				$this->e->setCidade($this->validar_input($this->input->post("cidade")));
				$this->e->setUf($this->validar_input($this->input->post("uf")));
				$this->e->setReferencia($this->validar_input($this->input->post("referencia")));
				$this->e->setLocalizacao($this->validar_input($this->input->post("localizacao")));
				$this->e->setTelefone($this->validar_input($this->input->post("telefone")));
				$this->e->setEmail($this->validar_input($this->input->post("email")));
				$this->e->setSituacaoCadastral($this->validar_input($this->input->post("situacao_cadastral")));
				$this->e->setSituacao($this->validar_input($this->input->post("situacao")));
				$this->e->setMotivo($this->validar_input($this->input->post("motivo")));
				$this->e->setTelefoneAlternativo($this->validar_input($this->input->post("telefone_alternativo")));
				$this->e->setCelularAlternativo($this->validar_input($this->input->post("celular_alternativo")));
				$this->e->setEmailAlternativo($this->validar_input($this->input->post("email_alternativo")));
				$this->e->setSituacaoDte($this->validar_input($this->input->post("situacao_dte")));
				$this->e->setSituacaoContaDte($this->validar_input($this->input->post("situacao_conta_dte")));
				$this->e->setAtividadePrincipal($this->validar_input($this->input->post("atividade_principal")));
				$this->e->setCondicao($this->validar_input($this->input->post("condicao")));
				$this->e->setFormaPagamento($this->validar_input($this->input->post("forma_pagamento")));
				$this->e->setMotivoSituacaoCadastral($this->validar_input($this->input->post("motivo_situacao_cadastral")));
				$this->e->setNomeContador($this->validar_input($this->input->post("nome_contador")));
				$this->e->setCrcContador($this->validar_input($this->input->post("crc_contador")));
				$this->e->setNomeResponsavel($this->validar_input($this->input->post("nome_responsavel")));
				$this->e->setCrcResponsavel($this->validar_input($this->input->post("crc_responsavel")));
				
				$this->e->setSenhaSefaz($this->validar_input($this->input->post("senha_sefaz")));
				$this->e->setLoginSefaz($this->validar_input($this->input->post("login_sefaz")));

				$this->e->setLoginMei($this->validar_input($this->input->post("login_mei")));
				$this->e->setSenhaMei($this->validar_input($this->input->post("senha_mei")));

				$this->e->setIdFuncionario($this->validar_input($this->input->post("id_funcionario")));

				$this->e->setFlagEmpresaSemIe($this->validar_input($this->input->post("flag_empresa_sem_ie")));

				$this->e->setCpfAlvara($this->validar_input($this->input->post("cpf_alvara")));
				$this->e->setNire($this->validar_input($this->input->post("nire")));
				$this->e->setAnotacoes($this->validar_input($this->input->post("anotacoes")));
				
				$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
				
				if ($this->input->post("situacao_cadastral") == 'BAIXADO'){
					$this->e->setSync(0);
				} else {
					$this->e->setSync(1);
				}
				
				date_default_timezone_set('America/Bahia');
				$this->e->setDataCadastro(date('Y-m-d H:i:s'));
				
				// 0 -> DESVINCULADO; 1 -> VINCULADO
				//CRC RESPONSÁVEL, CRC CONTADOR, ID CONTADOR
				$vinc = $this->e->validar_vinculo_contador();
				if ($vinc->valor > 0){
					$this->e->setVinculoContador(1);
				} else {
					$this->e->setVinculoContador(0);
				}
				
				if(isset($_POST["id_usuario"])){
					$listaUsuario = $_POST['id_usuario'];
					$this->e->setListaUsuario($listaUsuario);	
				}

				// IMPEDIR QUE CADASTRE EMPRESAS REPETIDAS
				$cnpj_verificacao = preg_replace("/[^0-9]/", "", $this->input->post("cnpj"));

				$verificacao = $this->e->verificar_cadastro_duplicado_by_cnpj($cnpj_verificacao);
						
				if ($verificacao){

				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo','Empresa já cadastrada.');
				$this->session->set_flashdata('msg_tipo','danger');
				redirect("empresa/cadastrar");

				} else {

					$result = $this->e->cadastrar();	
				}
					
				if ($result){

					$this->session->set_flashdata('msg_alerta', 1);				
					
					redirect("empresa/listar");
				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
					$this->session->set_flashdata('msg_tipo','danger');
					
					redirect("empresa/cadastrar");
				}
				
			} else{
				if ($tipo == 'ie'){
					$this->e->setInscricaoEstadual($valor);
					$result = $this->e->pesquisar_sefaz_ie();
					
					if ($result == 'erro'){
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Não foi possível localizar esta empresa. Talvez a mesma não possua Inscrição Estadual.');
						$this->session->set_flashdata('msg_tipo','danger');
						
						redirect('empresa/cadastrar');
					} else {
						$dados['result'] = $result;
						$dados['tipo'] = $tipo;
						$dados['valor'] = $valor;
					}
										
				} else if ($tipo == 'cnpj'){
					$this->e->setCnpj($valor);
					$result = $this->e->pesquisar_sefaz_cnpj();
					
					if ($result == 'erro'){

						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Não foi possível localizar esta empresa.');
						$this->session->set_flashdata('msg_tipo','danger');	
						redirect('empresa/cadastrar');
						// redirect('empresa/buscar_receita/'.$tipo.'/'.$valor);

						
						
					} else {
						$dados['result'] = $result;
						$dados['tipo'] = $tipo;
						$dados['valor'] = $valor;
					}
					
				} else {
					redirect("empresa/cadastrar");
				}
			}
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('empresa/empresa_finalizar_cadastro', $dados);
			$this->load->view('layout/footer');
			
		} else {
			redirect("empresa/cadastrar");
		}
		
	}

	public function finalizarcadastroempresaservico(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$tipo = $this->uri->segment(3);
		$valor = $this->uri->segment(4);
		
		if ($tipo != '' && $valor != ''){

			$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

			$funcionarios = $this->e->buscar_funcionarios();
			$lista_funcionarios = array('' => "Selecione o Funcionário Responsável");
			
			//LISTA AUXILIAR PARA O MULTI-SELECT
			$lista_funcionarios2 = array();				

			foreach ($funcionarios as $value) {
				
				$lista_funcionarios[$value->id] = $value->nome;
				$lista_funcionarios2[$value->id] = $value->nome;
			}

			$dados['lista_funcionarios'] = $lista_funcionarios;
			$dados['lista_funcionarios2'] = $lista_funcionarios2;

			if(isset($_POST['btn_cadastrar'])){
				
				$this->e->setInscricaoEstadualCompleto($this->validar_input($this->input->post("ie")));
				$this->e->setCnpjCompleto($this->validar_input($this->input->post("cnpj")));
				$this->e->setInscricaoEstadual($this->validar_input(preg_replace("/[^0-9]/", "", $this->input->post("ie"))));
				$this->e->setCnpj($this->validar_input(preg_replace("/[^0-9]/", "", $this->input->post("cnpj"))));
				$this->e->setRazaoSocial($this->validar_input($this->input->post("razao_social")));
				$this->e->setMei($this->validar_input($this->input->post("mei")));
				$this->e->setNomeFantasia($this->validar_input($this->input->post("nome_fantasia")));
				$this->e->setNaturezaJuridica($this->validar_input($this->input->post("natureza_juridica")));
				$this->e->setUnidadeAtendimento($this->validar_input($this->input->post("unidade_atendimento")));
				$this->e->setUnidadeFiscalizacao($this->validar_input($this->input->post("unidade_fiscalizacao")));
				$this->e->setCep($this->validar_input($this->input->post("cep")));
				$this->e->setLogradouro($this->validar_input($this->input->post("logradouro")));
				$this->e->setNumero($this->validar_input($this->input->post("numero")));
				$this->e->setComplemento($this->validar_input($this->input->post("complemento")));
				$this->e->setBairro($this->validar_input($this->input->post("bairro")));
				$this->e->setCidade($this->validar_input($this->input->post("cidade")));
				$this->e->setUf($this->validar_input($this->input->post("uf")));
				$this->e->setReferencia($this->validar_input($this->input->post("referencia")));
				$this->e->setLocalizacao($this->validar_input($this->input->post("localizacao")));
				$this->e->setTelefone($this->validar_input($this->input->post("telefone")));
				$this->e->setEmail($this->validar_input($this->input->post("email")));
				$this->e->setSituacaoCadastral($this->validar_input($this->input->post("situacao_cadastral")));
				$this->e->setSituacao($this->validar_input($this->input->post("situacao")));
				$this->e->setMotivo($this->validar_input($this->input->post("motivo")));
				$this->e->setTelefoneAlternativo($this->validar_input($this->input->post("telefone_alternativo")));
				$this->e->setCelularAlternativo($this->validar_input($this->input->post("celular_alternativo")));
				$this->e->setEmailAlternativo($this->validar_input($this->input->post("email_alternativo")));
				$this->e->setSituacaoDte($this->validar_input($this->input->post("situacao_dte")));
				$this->e->setSituacaoContaDte($this->validar_input($this->input->post("situacao_conta_dte")));
				$this->e->setAtividadePrincipal($this->validar_input($this->input->post("atividade_principal")));
				$this->e->setCondicao($this->validar_input($this->input->post("condicao")));
				$this->e->setFormaPagamento($this->validar_input($this->input->post("forma_pagamento")));
				$this->e->setMotivoSituacaoCadastral($this->validar_input($this->input->post("motivo_situacao_cadastral")));
				$this->e->setNomeContador($this->validar_input($this->input->post("nome_contador")));
				$this->e->setCrcContador($this->validar_input($this->input->post("crc_contador")));
				$this->e->setNomeResponsavel($this->validar_input($this->input->post("nome_responsavel")));
				$this->e->setCrcResponsavel($this->validar_input($this->input->post("crc_responsavel")));
				
				$this->e->setSenhaSefaz($this->validar_input($this->input->post("senha_sefaz")));
				$this->e->setLoginSefaz($this->validar_input($this->input->post("login_sefaz")));

				$this->e->setLoginMei($this->validar_input($this->input->post("login_mei")));
				$this->e->setSenhaMei($this->validar_input($this->input->post("senha_mei")));

				$this->e->setIdFuncionario($this->validar_input($this->input->post("id_funcionario")));

				$this->e->setFlagEmpresaSemIe($this->validar_input($this->input->post("flag_empresa_sem_ie")));

				$this->e->setCpfAlvara($this->validar_input($this->input->post("cpf_alvara")));
				$this->e->setNire($this->validar_input($this->input->post("nire")));
				$this->e->setAnotacoes($this->validar_input($this->input->post("anotacoes")));
				
				$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
				
				if ($this->input->post("situacao_cadastral") == 'BAIXADO'){
					$this->e->setSync(0);
				} else {
					$this->e->setSync(1);
				}
				
				date_default_timezone_set('America/Bahia');
				$this->e->setDataCadastro(date('Y-m-d H:i:s'));
				
				// 0 -> DESVINCULADO; 1 -> VINCULADO
				//CRC RESPONSÁVEL, CRC CONTADOR, ID CONTADOR
				$vinc = $this->e->validar_vinculo_contador();
				if ($vinc->valor > 0){
					$this->e->setVinculoContador(1);
				} else {
					$this->e->setVinculoContador(0);
				}
				
				if(isset($_POST["id_usuario"])){
					$listaUsuario = $_POST['id_usuario'];
					$this->e->setListaUsuario($listaUsuario);	
				}

				$result = $this->e->cadastrar();
					
				if ($result){

					$this->session->set_flashdata('msg_alerta', 1);				
					
					redirect("empresa/listar");
				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
					$this->session->set_flashdata('msg_tipo','danger');
					
					redirect("empresa/cadastrar");
				}
				
			} else { 	

				redirect('empresa/buscar_receita/'.$tipo.'/'.$valor); 
			}
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('empresa/empresa_finalizar_cadastro', $dados);
			$this->load->view('layout/footer');
			
		} else {
			redirect("empresa/cadastrar");
		}
		
	}
	
	public function buscar_receita(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$tipo = $this->uri->segment(3);
		$valor = $this->uri->segment(4);
		$this->e->setCnpj($valor);
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

		$funcionarios = $this->e->buscar_funcionarios();
		$lista_funcionarios = array('' => "Selecione o Funcionário Responsável");
		
		//LISTA AUXILIAR PARA O MULTI-SELECT
		$lista_funcionarios2 = array();				

		foreach ($funcionarios as $value) {
			
			$lista_funcionarios[$value->id] = $value->nome;
			$lista_funcionarios2[$value->id] = $value->nome;
		}

		$dados['lista_funcionarios'] = $lista_funcionarios;
		$dados['lista_funcionarios2'] = $lista_funcionarios2;
			
		$resultReceita = $this->e->pesquisar_cnpj_receita();

		if($resultReceita == "ERRO"){
			$this->session->set_flashdata('msg_titulo','ERRO!');
			$this->session->set_flashdata('msg_conteudo','Não foi possível localizar esta empresa.');
			$this->session->set_flashdata('msg_tipo','danger');	
			redirect('empresa/cadastrar');
		}else{
			$dados['result'] = $resultReceita;
			$dados['tipo'] = $tipo;
			$dados['valor'] = $valor;
		}

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('empresa/empresa_finalizar_cadastro', $dados);
		$this->load->view('layout/footer');

	}

	public function listar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$tour = $this->uri->segment(3);
		if($tour != ""){
			$dados['fromTour'] = "true";	
		}
		//VALIDAÇÃO DO PLANO EXCEDIDO
		$dados['e'] = $this->e->qtd_empresas();
		$dados['emax'] = $this->plano_model->valor_max_empresas();
		// FIM DA VALIDAÇÃO

		// VALIDAÇÃO DO TIPO DE PLANO
		$dados['nome_plano'] = $this->plano_model->nome_do_plano();
		//FIM DA VALIDAÇÃO
		
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->listar();
				
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('empresa/empresa_listar', $dados);
		$this->load->view('layout/footer');
		
	}
	
	public function consultar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
						
		$dados['consulta'] = '';
				
		if(isset($_POST['btn_pesquisar'])){
			
			// FALTA VALIDAR IE
			
			$ie = preg_replace("/[^0-9]/", "", $this->input->post("num_inscricao"));
			
			$this->e->setInscricaoEstadual($ie);
			$consulta = $this->e->consulta_individual();
			
			if ($consulta == 'erro'){
				
				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo','Não foi possível localizar esta empresa.');
				$this->session->set_flashdata('msg_tipo','danger');
							
			} else {
				$dados['consulta'] = $consulta;
			}
			
		}
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_nova_consulta', $dados);
		$this->load->view('layout/footer');
		
	}
	
	public function consultageral(){
		$id_empresa = $this->uri->segment(3);
		$razao = "";
		if($id_empresa != null){
			$em = $this->empresa_model->find_empresa_by_id_empresa($id_empresa);
			$razao = $em->razao_social;
		}

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->listar_consulta_geral();
		$dados['razao_social_filtro'] = $razao;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function credenciadas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->credenciadas();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function credenciadasativas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->credenciadasativas();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function credenciadasbaixadas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->credenciadasbaixadas();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function descredenciadas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->descredenciadas();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function descredenciadasativas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->descredenciadasativas();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function descredenciadasbaixadas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->descredenciadasbaixadas();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function contadornaovinculado(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->contadornaovinculado();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/contador_nao_vinculado', $dados);
		$this->load->view('layout/footer');
	
	}

	public function empresasregulares(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->empresasregulares();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}

	public function empresasativas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->empresasativas();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}

	public function empresasnaoativas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->empresasnaoativas();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}

	public function empresasinaptas(){
	
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->empresasinaptas();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function dteativo(){
	
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->dteativo();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}

	public function dteinexistente(){
	
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->dteinexistente();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function empresassemie(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->empresassemie();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	}

	public function relatoriogeral() {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
					
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO GERAL',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
		
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$result = $this->e->relatorio_geral();
		
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
		
		$pdf->SetFont("Courier","B",9);
		
		$pdf->Cell(80,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(30,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(30,8,"IE","TBLR",0,"C");
		$pdf->Cell(20,8,"STATUS","TBLR",0,"C");
		$pdf->Cell(30,8,"SITUAÇÃO","TBLR",0,"C");
		
		$pdf->Ln();
		
		$pdf->SetFont("Courier","",7);
		
		foreach ($result as $r) {
			
			$pdf->Cell(80,8,$r->razao_social,"TBL",0,"L");
			$pdf->Cell(30,8,$r->cnpj_completo,"TBL",0,"C");
			$pdf->Cell(30,8,$r->inscricao_estadual_completo,"TBL",0,"C");
			$pdf->Cell(20,8,$r->situacao_cadastral,"TBLR",0,"C");

			if ($r->situacao == "Descredenciado") {
			$pdf->SetFont('arial','B',8);
			$pdf->SetTextColor(194,8,8);
			$pdf->Cell(30,8,$r->situacao,"TBR",0,"C");
			} else {
			$pdf->SetFont('arial','B',8);
			$pdf->SetTextColor(0,113,37);
			$pdf->Cell(30,8,$r->situacao,"TBR",0,"C");	
			}
			$pdf->SetFont("Courier","",7);
			$pdf->SetTextColor(0,0,0);

			$pdf->Ln();
			$pdf->Cell(190,8,'MOTIVO: '.$r->motivo,"TBLR",1,"L");
			$pdf->SetFont("Courier","",7);
			
		}
		
		$pdf->Output('Relatorio Geral - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}
	
	public function relatorioativas() {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
			
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO EMPRESAS ATIVAS',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$result = $this->e->relatorio_ativas();
	
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
	
		$pdf->SetFont("Courier","B",9);
	
		$pdf->Cell(80,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(30,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(30,8,"IE","TBLR",0,"C");
		$pdf->Cell(20,8,"STATUS","TBLR",0,"C");
		$pdf->Cell(30,8,"SITUAÇÃO","TBLR",0,"C");
	
		$pdf->Ln();
	
		$pdf->SetFont("Courier","",7);
	
		foreach ($result as $r) {
				
			$pdf->Cell(80,8,$r->razao_social,"TBL",0,"L");
			$pdf->Cell(30,8,$r->cnpj_completo,"TBL",0,"C");
			$pdf->Cell(30,8,$r->inscricao_estadual_completo,"TBL",0,"C");
			$pdf->Cell(20,8,$r->situacao_cadastral,"TBLR",0,"C");

			if ($r->situacao == "Descredenciado") {
			$pdf->SetFont('arial','B',8);
			$pdf->SetTextColor(194,8,8);
			$pdf->Cell(30,8,$r->situacao,"TBR",0,"C");
			} else {
			$pdf->SetFont('arial','B',8);
			$pdf->SetTextColor(0,113,37);
			$pdf->Cell(30,8,$r->situacao,"TBR",0,"C");	
			}
			$pdf->SetFont("Courier","",7);
			$pdf->SetTextColor(0,0,0);

			$pdf->Ln();
			$pdf->Cell(190,8,'MOTIVO: '.$r->motivo,"TBLR",1,"L");
				
		}
	
		$pdf->Output('Relatorio Ativas - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}
	
	public function relatoriocredenciadas() {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
		$pdf->Ln(15);
			
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO EMPRESAS CREDENCIADAS',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$result = $this->e->relatorio_credenciadas();
	
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
	
		$pdf->SetFont("Courier","B",9);
	
		$pdf->Cell(80,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(30,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(30,8,"IE","TBLR",0,"C");
		$pdf->Cell(20,8,"STATUS","TBLR",0,"C");
		$pdf->Cell(30,8,"SITUAÇÃO","TBLR",0,"C");
	
		$pdf->Ln();
	
		$pdf->SetFont("Courier","",7);
	
		foreach ($result as $r) {
	
			$pdf->Cell(80,8,$r->razao_social,"TBL",0,"L");
			$pdf->Cell(30,8,$r->cnpj_completo,"TBL",0,"C");
			$pdf->Cell(30,8,$r->inscricao_estadual_completo,"TBL",0,"C");
			$pdf->Cell(20,8,$r->situacao_cadastral,"TBLR",0,"C");
			$pdf->Cell(30,8,$r->situacao,"TBR",0,"C");
	
			$pdf->Ln();
			$pdf->Cell(190,8,'MOTIVO: '.$r->motivo,"TBLR",1,"L");
	
		}
	
		$pdf->Output('Relatorio Credenciadas - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}
	
	public function relatoriodescredenciadas() {
			
		$this->load->library("pdf");
			
		$pdf = new Pdf("P","mm","A4");
		$pdf->SetMargins(10,10,10);
		$pdf->AddPage();
			
		$pdf->Image('assets/img/logos/logo.png',10,15,20);
		$pdf->Ln(15);
			
		$pdf->SetFont("Arial","B",15);
		$pdf->Cell(0,15,'RELATÓRIO EMPRESAS DESCREDENCIADAS',0,1,"C");
			
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont("Courier","",9);
		$pdf->Cell(190,10,"",0,1,"C");
		date_default_timezone_set('America/Bahia');
		$pdf->Cell(190,0,"Data da Emissão: ".date("d/m/Y H:i:s"),0,1,"C");
			
		//$pdf->Cell(190,10,"____________________________________________________________________________________________________",0,0,"L");
		//$pdf->Ln();
		$pdf->Cell(190,10,'',0,0,"L");
		$pdf->Ln();
			
		$pdf->SetFont("Courier","",7);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$result = $this->e->relatorio_descredenciadas();
	
		$pdf->SetDrawColor("0","0","0");
		$pdf->SetLineWidth(0.1);
	
		$pdf->SetFont("Courier","B",9);
	
		$pdf->Cell(80,8,"RAZÃO SOCIAL","TBRL",0,"C");
		$pdf->Cell(30,8,"CNPJ","TBLR",0,"C");
		$pdf->Cell(30,8,"IE","TBLR",0,"C");
		$pdf->Cell(20,8,"STATUS","TBLR",0,"C");
		$pdf->Cell(30,8,"SITUAÇÃO","TBLR",0,"C");
	
		$pdf->Ln();
	
		$pdf->SetFont("Courier","",7);
	
		foreach ($result as $r) {
	
			$pdf->Cell(80,8,$r->razao_social,"TBL",0,"L");
			$pdf->Cell(30,8,$r->cnpj_completo,"TBL",0,"C");
			$pdf->Cell(30,8,$r->inscricao_estadual_completo,"TBL",0,"C");
			$pdf->Cell(20,8,$r->situacao_cadastral,"TBLR",0,"C");
			$pdf->Cell(30,8,$r->situacao,"TBR",0,"C");
	
			$pdf->Ln();
			$pdf->Cell(190,8,'MOTIVO: '.$r->motivo,"TBLR",1,"L");
	
		}
	
		$pdf->Output('Relatorio Descredenciadas - '.date('Y-m-d H:i:s').'.pdf', 'I');
	}
	
	public function excluir() {
		
		$id = $this->uri->segment(3);
		$id_contador = $this->uri->segment(4); 
				
		$this->e->setId($id);
		$this->limpa_empresa_model->setId($id);

		$nome_empresa = $this->e->nome_empresa();
		$cnpj_empresa = $this->e->cnpj_empresa();

		$empresa = $this->limpa_empresa_model->find_empresa();

		$result = $this->e->excluir();

		$this->documento_model->setIdEmpresa($id);
		$result2 = $this->documento_model->excluirDocumentoByEmpresa();

		$this->resumofiscal_model->setCnpjCpf($cnpj_empresa->cnpj_completo);
		$result3 = $this->resumofiscal_model->excluirResumoByCnpj();

		$this->alvara_model->setCnpj($cnpj_empresa->cnpj_completo);
		$result4 = $this->alvara_model->excluirAlvaraByCnpj();
		
		$this->limpa_empresa_model->setCnpj($empresa->cnpj);
		$this->limpa_empresa_model->setCnpjCompleto($cnpj_empresa);
		$this->limpa_dados_empresa();

		if ($result){
		
			$this->session->set_flashdata('msg_alerta', 3);
		
			redirect('empresa/listar');
				
		} else {
			$this->session->set_flashdata('msg_alerta', 4);
		
			redirect('empresa/listar');
				
		} 
	
	}
	
	public function sync() {
	
		$id = $this->uri->segment(3);
		$valor = $this->uri->segment(4);
		$id_contador = $this->uri->segment(5);
		$filtro = $this->uri->segment(6);		
	
		if ($id != '' || $valor != '' || $id_contador != '' || $filtro != ''){
			
			//VALIDAR SE O USUÁRIO É O DONO DA EMPRESA EM FOCO
			$result = $this->validar_usuario($id_contador);
	
			if ($result){
				$this->e->setId($id);
				$this->e->setSync($valor);
				$this->e->update_sync();
				redirect("empresa/$filtro");
			} else {
				redirect('empresa/consultageral');
			}
			
		}
		else {
			redirect('empresa/consultageral');
		}
	
	}
	
	public function editar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
	
		$id = $this->uri->segment(3);

		if($this->session->userdata['userprimesession']['nivel'] == 2){

			$id_contador = $this->session->userdata['userprimesession']['id_cadastro'];
	
		} else {

			//$id_contador = $this->uri->segment(4);

			$id_contador = $this->session->userdata['userprimesession']['id'];
		}
		
	
		if ($id != '' && $id_contador != ''){
			
			if (true){//$this->validar_usuario($id_contador)){
				
				$this->load->model('empresausuario_model', 'eum');
				
				//$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
				$this->e->setIdContador($id_contador);
				
				$funcionarios = $this->e->buscar_funcionarios();
				$lista_funcionarios = array('' => "Selecione o Funcionário Responsável");
				
				//LISTA AUXILIAR PARA O MULTI-SELECT
				$lista_funcionarios2 = array();				

				foreach ($funcionarios as $value) {
					
					$lista_funcionarios[$value->id] = $value->nome;
					$lista_funcionarios2[$value->id] = $value->nome;
				}

				$dados['lista_funcionarios'] = $lista_funcionarios;
				$dados['lista_funcionarios2'] = $lista_funcionarios2;

				if(isset($_POST['btn_salvar'])){
				
					$this->e->setId($id);
					$this->e->setRazaoSocial($this->validar_input($this->input->post("razao_social")));
					$this->e->setNomeFantasia($this->validar_input($this->input->post("nome_fantasia")));
					$this->e->setInscricaoEstadualCompleto($this->validar_input($this->input->post("ie")));					
					$this->e->setTelefoneAlternativo($this->validar_input($this->input->post("telefone_alternativo")));
					$this->e->setCelularAlternativo($this->validar_input($this->input->post("celular_alternativo")));
					$this->e->setEmailAlternativo($this->validar_input($this->input->post("email_alternativo")));
					$this->e->setSenhaSefaz($this->validar_input($this->input->post("senha_sefaz")));
					$this->e->setLoginSefaz($this->validar_input($this->input->post("login_sefaz")));
					$this->e->setLoginMei($this->validar_input($this->input->post("login_mei")));
					$this->e->setSenhaMei($this->validar_input($this->input->post("senha_mei")));
					$this->e->setIdFuncionario($this->validar_input($this->input->post("id_funcionario")));
					$this->e->setCpfAlvara($this->validar_input($this->input->post("cpf_alvara")));
					$this->e->setNire($this->validar_input($this->input->post("nire")));
					$this->e->setAnotacoes($this->validar_input($this->input->post("anotacoes")));

					if(isset($_POST["id_usuario"])){
						$listaUsuario = $_POST['id_usuario'];
						$this->e->setListaUsuario($listaUsuario);	
					}
					

					$result = $this->e->editar();
						
					if ($result){
						
						$this->session->set_flashdata('msg_alerta', 2);	
						redirect('empresa/listar');

							
					} else {
						$dados['msg_titulo'] = 'ERRO!';
						$dados['msg_conteudo'] = 'Não foi possível editar.';
						$dados['msg_tipo'] = 'danger';
					}
				
				}
					
				$this->e->setId($id);
				$dados['empresa'] = $this->e->pesquisar_empresa_id();
				
				$this->eum->setIdEmpresa($id);
				$selecionados = $this->eum->findAllByIdEmpresa();
				$listaSelecionados = "";

				foreach ($selecionados as $value) {
					
					$listaSelecionados[$value->id] = $value->id;
				}

				$dados['listaSelecionados'] = $listaSelecionados;

				$dados['id'] = $id;
				$dados['id_contador'] = $id_contador;
					
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('empresa/empresa_editar', $dados);
				$this->load->view('layout/footer');
				
			} else {
				redirect("empresa/listar");
			}
				
		} else {
			redirect("empresa/listar");
		}
	
	}
	
	public function visualizar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
	
		$id = $this->uri->segment(3);
		$id_contador = $this->uri->segment(4);

		$funcionarios = $this->e->buscar_funcionarios();
		$lista_funcionarios = array('' => "Selecionar Funcionário Responsável");

		foreach ($funcionarios as $value) {
					
			$lista_funcionarios[$value->id] = $value->nome;
		}

		$dados['lista_funcionarios'] = $lista_funcionarios;
	
		if ($id != '' && $id_contador != ''){
				
			//if ($this->validar_usuario($id_contador)){
				
				$this->e->setId($id);
				$dados['empresa'] = $this->e->consultar_emopresa_aux();
				
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('empresa/empresa_visualizar', $dados);
				$this->load->view('layout/footer');
	
			//} else {
				//redirect("empresa/consultageral");
			//}
	
		} else {
			redirect("empresa/consultageral");
		}
	
	}
	
	public function historico(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
		$id_contador = $this->uri->segment(4);
		
		if ($id != '' && $id_contador != ''){
		
			if (true){//$this->validar_usuario($id_contador)){
				
				$this->e->setIdContador($id_contador);
				$this->e->setId($id);
				$dados['empresa'] = $this->e->empresa_id();
				$dados['consulta'] = $this->e->historico_empresa();
			
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('consultas/historico', $dados);
				$this->load->view('layout/footer');
		
			} else {
				redirect("empresa/consultageral");
			}
		
		} else {
			redirect("empresa/consultageral");
		}
		
	}

	public function consultarDte(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$idEmpresa = $this->uri->segment(3);
		$idContador = $this->uri->segment(4);
		
		$this->e->setIdContador($idContador);
		$this->e->setId($idEmpresa);

		$dados['empresa'] = $this->e->consultarDadosForDte();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('dte/consultardte_novo', $dados);
		$this->load->view('layout/footer');
	
	}

	public function consultaintimadas(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->listar_empresas_intimadas();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_empresas_intimadas', $dados);
		$this->load->view('layout/footer');
	}

	public function consultasemsenha(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->listar_empresas_sem_senha_dte();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	}

	public function empresasmei(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->empresas_mei();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}

	public function empresassimples(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->empresas_simples_nacional();
	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');
	
	}

	public function cadastrar_via_email(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$cnpj = $this->uri->segment(3);
		
		$this->e->setCnpj($cnpj);
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$verificacao = $this->e->verificar_cadastro_duplicado('cnpj');
			
		if ($verificacao){
			
			//msg que já está cadastrado
			$this->session->set_flashdata('msg_titulo','ERRO!');
			$this->session->set_flashdata('msg_conteudo','Empresa já cadastrada.');
			$this->session->set_flashdata('msg_tipo','danger');
			
		} else {
			redirect("empresa/finalizarcadastro/cnpj/$cnpj");
		}

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('empresa/empresa_cadastrar');
		$this->load->view('layout/footer');
				 
	}

	public function compras_e_vendas(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->listar();
				
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('empresa/compras_e_vendas', $dados);
		$this->load->view('layout/footer');
		
	}
	
	public function atualizar_regime(){
		$regime = $this->input->post("regime");
		$id = $this->input->post("id");

		if($regime == "SIMPLES"){
			$this->e->update_regime($id, 'SIMPLES NACIONAL');
		}else if($regime == "LUCRO_REAL"){
			$this->e->update_regime($id, 'LUCRO REAL');
		}else{
			$this->e->update_regime($id, 'LUCRO PRESUMIDO');
		}

		echo 1;
	}


	public function limpa_dados_empresa(){
		$this->limpa_empresa_model->excluir_certidao_sefaz();
		$this->limpa_empresa_model->excluir_certidao_barreiras();
		$this->limpa_empresa_model->excluir_certidao_caixa();
		$this->limpa_empresa_model->excluir_certidao_conquista();
		$this->limpa_empresa_model->excluir_certidao_feira();
		$this->limpa_empresa_model->excluir_certidao_luizeduardo();
		$this->limpa_empresa_model->excluir_certidao_receita();
		$this->limpa_empresa_model->excluir_certidao_trabalhista();
		$this->limpa_empresa_model->excluir_certificado();
		$this->limpa_empresa_model->excluir_contador_procuracao();
		// $this->limpa_empresa_model->excluir_documentos();
		$this->limpa_empresa_model->excluir_ecac_caixa_postal_mensagem();
		$this->limpa_empresa_model->excluir_ecac_caixa_postal();
		$this->limpa_empresa_model->excluir_ecac_das();
		$this->limpa_empresa_model->excluir_ecac_das_debitos();
		$this->limpa_empresa_model->excluir_ecac_dctf();
		$this->limpa_empresa_model->excluir_exclusao_simples();
		$this->limpa_empresa_model->excluir_ipva();
		$this->limpa_empresa_model->excluir_juceb_dados();
		$this->limpa_empresa_model->excluir_logins_validos();
		$this->limpa_empresa_model->excluir_mensagens_dte();
		$this->limpa_empresa_model->excluir_situacao_cadin();
		$this->limpa_empresa_model->excluir_situacao_fiscal();
		// $this->limpa_empresa_model->excluir_resumofiscal();
		$this->limpa_empresa_model->excluir_parcelas_emitidas();
		$this->limpa_empresa_model->excluir_tramitacao_processo_sipro();

	}


	/////////////////////////////NOVA TELA/////////////////////////////////////
	public function cadastrar_empresa_etapa1(){ 
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
			
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('cadastro/cadastrar_empresa_etapa1');
		$this->load->view('layout/footer');
	
	}



	public function cadastrar_empresa_servico_novatela(){ 

		if(isset($_POST['btn_localizar'])){

			$cnpj = $this->input->post('cnpj');
			
			if ($cnpj != ''){
					
					$cnpj = preg_replace("/[^0-9]/", "", $cnpj);
					
					$this->e->setCnpj($cnpj);
					$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
					$verificacao = $this->e->verificar_cadastro_duplicado('cnpj');
						
					if ($verificacao){
						
						//msg que já está cadastrado
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Empresa já cadastrada.');
						$this->session->set_flashdata('msg_tipo','danger');
						redirect("empresa/cadastrar_empresa_etapa1");
						
					} else {
						redirect("empresa/cadastrar_empresa_etapa2/cnpj/$cnpj");
					}				
				
			} else {				
				$this->session->set_flashdata('msg_titulo','ATENÇÃO!');
				$this->session->set_flashdata('msg_conteudo','Por favor informe um dos campos.');
				$this->session->set_flashdata('msg_tipo','warning');
				redirect("empresa/cadastrar_empresa_etapa1");
			}
		
		} 
	}



	public function cadastrar_empresa_etapa2(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$tipo = $this->uri->segment(3);
		$valor = $this->uri->segment(4);
		
		if ($tipo != '' && $valor != ''){

			$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

			$funcionarios = $this->e->buscar_funcionarios();
			$lista_funcionarios = array('' => "Selecione o Funcionário Responsável");
			
			//LISTA AUXILIAR PARA O MULTI-SELECT
			$lista_funcionarios2 = array();				

			foreach ($funcionarios as $value) {
				
				$lista_funcionarios[$value->id] = $value->nome;
				$lista_funcionarios2[$value->id] = $value->nome;
			}

			$dados['lista_funcionarios'] = $lista_funcionarios;
			$dados['lista_funcionarios2'] = $lista_funcionarios2;

			if(isset($_POST['btn_cadastrar'])){
				
				$this->e->setInscricaoEstadualCompleto($this->validar_input($this->input->post("ie")));
				$this->e->setCnpjCompleto($this->validar_input($this->input->post("cnpj")));
				$this->e->setInscricaoEstadual($this->validar_input(preg_replace("/[^0-9]/", "", $this->input->post("ie"))));
				$this->e->setCnpj($this->validar_input(preg_replace("/[^0-9]/", "", $this->input->post("cnpj"))));
				$this->e->setRazaoSocial($this->validar_input($this->input->post("razao_social")));
				$this->e->setMei($this->validar_input($this->input->post("mei")));
				$this->e->setNomeFantasia($this->validar_input($this->input->post("nome_fantasia")));
				$this->e->setNaturezaJuridica($this->validar_input($this->input->post("natureza_juridica")));
				$this->e->setUnidadeAtendimento($this->validar_input($this->input->post("unidade_atendimento")));
				$this->e->setUnidadeFiscalizacao($this->validar_input($this->input->post("unidade_fiscalizacao")));
				$this->e->setCep($this->validar_input($this->input->post("cep")));
				$this->e->setLogradouro($this->validar_input($this->input->post("logradouro")));
				$this->e->setNumero($this->validar_input($this->input->post("numero")));
				$this->e->setComplemento($this->validar_input($this->input->post("complemento")));
				$this->e->setBairro($this->validar_input($this->input->post("bairro")));
				$this->e->setCidade($this->validar_input($this->input->post("cidade")));
				$this->e->setUf($this->validar_input($this->input->post("uf")));
				$this->e->setReferencia($this->validar_input($this->input->post("referencia")));
				$this->e->setLocalizacao($this->validar_input($this->input->post("localizacao")));
				$this->e->setTelefone($this->validar_input($this->input->post("telefone")));
				$this->e->setEmail($this->validar_input($this->input->post("email")));
				$this->e->setSituacaoCadastral($this->validar_input($this->input->post("situacao_cadastral")));
				$this->e->setSituacao($this->validar_input($this->input->post("situacao")));
				$this->e->setMotivo($this->validar_input($this->input->post("motivo")));
				$this->e->setTelefoneAlternativo($this->validar_input($this->input->post("telefone_alternativo")));
				$this->e->setCelularAlternativo($this->validar_input($this->input->post("celular_alternativo")));
				$this->e->setEmailAlternativo($this->validar_input($this->input->post("email_alternativo")));
				$this->e->setSituacaoDte($this->validar_input($this->input->post("situacao_dte")));
				$this->e->setSituacaoContaDte($this->validar_input($this->input->post("situacao_conta_dte")));
				$this->e->setAtividadePrincipal($this->validar_input($this->input->post("atividade_principal")));
				$this->e->setCondicao($this->validar_input($this->input->post("condicao")));
				$this->e->setFormaPagamento($this->validar_input($this->input->post("forma_pagamento")));
				$this->e->setMotivoSituacaoCadastral($this->validar_input($this->input->post("motivo_situacao_cadastral")));
				$this->e->setNomeContador($this->validar_input($this->input->post("nome_contador")));
				$this->e->setCrcContador($this->validar_input($this->input->post("crc_contador")));
				$this->e->setNomeResponsavel($this->validar_input($this->input->post("nome_responsavel")));
				$this->e->setCrcResponsavel($this->validar_input($this->input->post("crc_responsavel")));
				
				$this->e->setTipoRegime($this->input->post("tipo_regime"));
				$this->e->setSenhaSefaz($this->validar_input($this->input->post("senha_sefaz")));
				$this->e->setLoginSefaz($this->validar_input($this->input->post("login_sefaz")));

				$this->e->setLoginMei($this->validar_input($this->input->post("login_mei")));
				$this->e->setSenhaMei($this->validar_input($this->input->post("senha_mei")));

				$this->e->setIdFuncionario($this->validar_input($this->input->post("id_funcionario")));

				$this->e->setFlagEmpresaSemIe($this->validar_input($this->input->post("flag_empresa_sem_ie")));

				$this->e->setCpfAlvara($this->validar_input($this->input->post("cpf_alvara")));
				$this->e->setNire($this->validar_input($this->input->post("nire")));
				$this->e->setAnotacoes($this->validar_input($this->input->post("anotacoes")));
				
				$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
				
				if ($this->input->post("situacao_cadastral") == 'BAIXADO'){
					$this->e->setSync(0);
				} else {
					$this->e->setSync(1);
				}
				
				date_default_timezone_set('America/Bahia');
				$this->e->setDataCadastro(date('Y-m-d H:i:s'));
				
				// 0 -> DESVINCULADO; 1 -> VINCULADO
				//CRC RESPONSÁVEL, CRC CONTADOR, ID CONTADOR
				$vinc = $this->e->validar_vinculo_contador();
				if ($vinc->valor > 0){
					$this->e->setVinculoContador(1);
				} else {
					$this->e->setVinculoContador(0);
				}
				
				if(isset($_POST["id_usuario"])){
					$listaUsuario = $_POST['id_usuario'];
					$this->e->setListaUsuario($listaUsuario);	
				}

				$result = $this->e->cadastrar_novatela();
					
				if ($result){

					$this->session->set_flashdata('msg_alerta', 1);				
					

					//VAI PARA FUNÇÃO DE ABRIR CERTIFICADO 
					redirect("empresa/cadastrar_empresa_etapa3/".$result);
				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
					$this->session->set_flashdata('msg_tipo','danger');
					
					redirect("empresa/cadastrar_empresa_etapa1");
				}
				
			} else { 	

				redirect('empresa/buscar_receita_novatela/'.$tipo.'/'.$valor); 
			}
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('cadastro/cadastrar_empresa_etapa2', $dados);
			$this->load->view('layout/footer');
			
		} else {
			redirect("empresa/cadastrar_empresa_etapa1");
		}
		
	}

	public function buscar_receita_novatela(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$tipo = $this->uri->segment(3);
		$valor = $this->uri->segment(4);
		$this->e->setCnpj($valor);
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

		$funcionarios = $this->e->buscar_funcionarios();
		$lista_funcionarios = array('' => "Selecione o Funcionário Responsável");
		
		//LISTA AUXILIAR PARA O MULTI-SELECT
		$lista_funcionarios2 = array();				

		foreach ($funcionarios as $value) {
			
			$lista_funcionarios[$value->id] = $value->nome;
			$lista_funcionarios2[$value->id] = $value->nome;
		}

		$dados['lista_funcionarios'] = $lista_funcionarios;
		$dados['lista_funcionarios2'] = $lista_funcionarios2;
			
		$resultReceita = $this->e->pesquisar_cnpj_receita();

		if($resultReceita == "ERRO"){
			$this->session->set_flashdata('msg_titulo','ERRO!');
			$this->session->set_flashdata('msg_conteudo','Não foi possível localizar esta empresa.');
			$this->session->set_flashdata('msg_tipo','danger');	
			redirect('empresa/cadastrar_empresa_etapa1');
		}else{
			$dados['result'] = $resultReceita;
			$dados['tipo'] = $tipo;
			$dados['valor'] = $valor;
		}

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('cadastro/cadastrar_empresa_etapa2', $dados);
		$this->load->view('layout/footer');

	}


	public function cadastrar_empresa_etapa3(){
		$id = $this->uri->segment(3);

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		$this->empresa_model->setId($id);
		$dados['empresa'] = $this->empresa_model->pesquisar_empresa_id();
        $dados['id'] = $id;
        
        $procuracoes = array('' => "Selecione a Procuração");
		$certificados_proc = $this->certificadocontador_model->busca_todos_certificados();
		foreach ($certificados_proc as $value) {
			
			$procuracoes[$value->id] = $value->nome;
		}

		$dados['procuracoes'] = $procuracoes;


		$dados['procuracao_empresa'] = $this->certificadocontador_model->busca_certificado_empresa_vinculada($id);

        $dados["certificado"] = $this->certificado_model->get_status_certificado($dados['empresa']->cnpj);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('cadastro/cadastrar_empresa_etapa3', $dados);
		$this->load->view('layout/footer');
	}


	public function upload_certificado_individual(){

        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA
        
        $targetPath = 'crons-api/assets/empresas/certificados/'.$server.'/';

        if (!file_exists($targetPath)) {
            mkdir($targetPath, DIR_WRITE_MODE, true);
        }

        $findme   = '0001';
        $valor_cnpj = $this->input->post('certificado_cnpj');
        $pos = strpos($valor_cnpj, $findme);
        
        $dados_post = array(
            'senha'=>$this->input->post('certificado_senha'),
            'cnpj'=>$this->input->post('certificado_cnpj'),
            'id_empresa'=>$this->input->post('id_empresa'),
            'dir_upload_cert'=>$targetPath /*<--- Dar permissão para essa pasta, pois, ela será o caminho do upload do arquivo */
        );

        if ($pos === false) {
            echo ($this->certificado_model->configurar_certificado_filial($dados_post, $_FILES['certificado_file']));  
        } else {
            echo ($this->certificado_model->configurar($dados_post, $_FILES['certificado_file']));  
        }       
    }


    public function finalizar_cadastro_novatela(){
    	$id_empresa = $this->uri->segment(3);
    	$id_procuracao = $this->input->post("id_procuracao");
    	if(!empty($id_procuracao)){
    		$this->certificadocontador_model->vicular_empresa_certificado($id_empresa, $id_procuracao);
    		
    		$this->session->set_flashdata('msg_alerta', 1);	
    		redirect('empresa/listar');
    	}else{

    		$existe_certificado_individual = $this->certificado_model->verifica_se_certificado_empresa($id_empresa);
    		if($existe_certificado_individual->qtd > 0){
    			$this->session->set_flashdata('msg_alerta', 1);	
			    redirect('empresa/listar');
    		}else{
    			$this->session->set_flashdata('msg_alerta', 3);
    			redirect('empresa/cadastrar_empresa_etapa3/'.$id_empresa);
    		}
    		
    	}
    }

	public function novo_importador(){
		$dados = $this->input->post('dados');

		//Busca a quantidade de empresas disponiveis no plano
		$qtd_max_empresa_plano = $this->plano_model->valor_max_empresas();
		$qtd_empresas = $this->e->qtd_empresas();
		$qtd_cadastro_permitido = $qtd_max_empresa_plano->valor - $qtd_empresas->valor;

		$log = "";

		$cnpj_erro = [];
		$cnpj_exceeded = [];
		$cnpj_retorno = [];

		if (!empty($dados)) {
			$contador = 0;
			foreach ($dados as $cnpj) {
				if ($contador <= $qtd_cadastro_permitido) {
					if (!empty($cnpj)) {
						$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
	
						$cnpj = preg_replace("/[^0-9]/", "", $cnpj);
	
						// COMPARA SE CNPJ DIGITADO NA PLANILHA ESTÁ COMPLETO
						if(strlen($cnpj) == 13){
							$cnpj = '0'.$cnpj;
						} 
						else if (strlen($cnpj) == 12) {
							$cnpj = '00'.$cnpj;
						} else {
							$cnpj = $cnpj;
						}
	
						$this->e->setCnpj($cnpj);
						
						$verificacao = $this->e->verificar_cadastro_duplicado_by_cnpj($cnpj);
	
						if($verificacao){
	
							$this->e->setId($verificacao->id);  
	
						} else {
							$retorno = $this->e->pesquisar_cnpj_receita_importador($cnpj);
						
							if($retorno == "sucesso"){
								$this->e->cadastrar();
								$contador++;
							}else{
								array_push($cnpj_retorno, $cnpj.' - '.$retorno);
								array_push($cnpj_erro, $cnpj);
								$log = $log."<br> CNPJ - ".$cnpj." não importado devido a erro!";
								$qstring["status"] = $log;
							}
						}
					}
				} else {
					array_push($cnpj_exceeded, $cnpj);
				}
			}

			if($log == "")
            	$qstring["status"] = 'Dados importados com sucesso!';	
            	
		} else{
			$qstring["status"] = 'Carregamento inválido';
		}

		$cnpjs = [
			'erro' => $cnpj_erro,
			'exceeded' => $cnpj_exceeded,
			'retorno' => $cnpj_retorno
		];
		
		echo json_encode($cnpjs);
		die();		
	}
}
