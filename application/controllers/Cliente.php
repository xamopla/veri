<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends MY_Controller {

	public function index() {		  

		if(isset($_POST['btn_login'])){
			$this->load->model("login_cliente_model", "login");
			
			$verificacao = 	$this->login->logar($this->input->post("login"), md5($this->input->post("senha")));
			if ($verificacao != 'invalido' && $verificacao != 'inativo' && $verificacao != 'expirado'){
				
				$this->session->set_userdata("userprimesession_cliente", $verificacao);

				//GRAVA O LOG DE ACESSO AO SISTEMA WEB
				$this->load->model('gerenciar_clientes_model');
				$this->gerenciar_clientes_model->setId($this->session->userdata['userprimesession_cliente']['id']);
				date_default_timezone_set('America/Bahia');
				$this->gerenciar_clientes_model->setDataAcesso(date('Y-m-d H:i:s'));
				$this->gerenciar_clientes_model->update_acesso(); 
				
				$id_cliente = $this->session->userdata['userprimesession_cliente']['id'];
				redirect('gerenciar_clientes/painel_cliente/'.$id_cliente.'', 'refresh');
				
			} else if ($verificacao == 'invalido'){

				//TENTATIVA DE ACESSO
				$this->load->model('tentativa_acesso_model', 'l');
				$this->l->setNomeUsuario($this->input->post("login"));
				$this->l->setSenhaUsada($this->input->post("senha"));
				date_default_timezone_set('America/Bahia');
				$this->l->setDataHora(date('Y-m-d H:i:s'));
				function getUserIP()
				{
				    $client  = @$_SERVER['HTTP_CLIENT_IP'];
				    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
				    $remote  = $_SERVER['REMOTE_ADDR'];

				    if(filter_var($client, FILTER_VALIDATE_IP))
				    {
				        $ip = $client;
				    }
				    elseif(filter_var($forward, FILTER_VALIDATE_IP))
				    {
				        $ip = $forward;
				    }
				    else
				    {
				        $ip = $remote;
				    }

				    return $ip;
				}
				$ip_do_usuario = getUserIP();
				$this->l->setIp($ip_do_usuario);

				$result = $this->l->tentativa_de_logar();
				//FIM DO LOG DE TENTATIVA DE ACESSO
				
				$dados ['msg'] = 'invalido';
				$this->session->set_flashdata('invalido', 'Dados inválidos!');
				$this->load->view('index_cliente', $dados);
			} else if ($verificacao == 'inativo') {
				$dados ['msg'] = 'inativo';
				$this->session->set_flashdata('inativo', 'Seu cadastro está inativo!');
				$this->load->view('index_cliente', $dados);
			} else if ($verificacao == 'expirado') {
				$dados ['msg'] = 'expirado';
				$this->session->set_flashdata('expirado', 'O seu período de utilização expirou, por favor entre em contato.');
				$this->load->view('index_cliente', $dados);
			}
		}
		else {
			$dados ['msg'] = '';
			$this->load->view('index_cliente', $dados);
		}
		
	}

	public function logout()
	{
		$this->load->view('logout_cliente');
	}
}