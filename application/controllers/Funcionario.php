<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Funcionario extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('funcionario_model');
		$this->load->model('empresausuario_model');
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function cadastrar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		if ($this->session->userdata['userprimesession']['nivel'] != 2){
			
				
			if(isset($_POST['btn_cadastrar'])){
	
				$this->funcionario_model->setNome($this->validar_input($this->input->post("nome")));
				$this->funcionario_model->setSenha(md5($this->input->post("senha")));
				$this->funcionario_model->setLogin($this->validar_input($this->input->post("login")));
				
				$this->funcionario_model->setIdCadastro($this->session->userdata['userprimesession']['id']);			
				date_default_timezone_set('America/Bahia');
				$this->funcionario_model->setDataCadastro(date('Y-m-d H:i:s'));

				$this->funcionario_model->setvalidade($this->session->userdata['userprimesession']['validade']);	
					
				$result = $this->funcionario_model->cadastrar();
					
				if ($result){
	
					redirect("funcionario/cadastrar_permissoes/$result");

				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
					$this->session->set_flashdata('msg_tipo','danger');
				}
	
			}
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');			
			$this->load->view('layout/header', $dados);
			$this->load->view('funcionario/funcionario_cadastrar');
			$this->load->view('layout/footer');
		
		} else {
			redirect("painel");
		}
	
	}

	public function listar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		//VALIDADAÇÃO DO PLANO EXCEDIDO
		$this->load->model('usuario_model');
		$this->load->model('plano_model');

		$dados['u'] = $this->usuario_model->qtd_usuarios();
		$dados['umax'] = $this->plano_model->valor_max_usuarios();
		// FIM DA VALIDADAÇÃO

		$this->funcionario_model->setIdCadastro($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->funcionario_model->listar();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('funcionario/funcionario_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function excluir() {
	
		$id = $this->uri->segment(3);
		$id_cadastro = $this->uri->segment(4);
	
		if ($id != ''){
				
			//VALIDAR SE O USUÁRIO É O DONO DA EMPRESA EM FOCO
			//$validate = $this->validar_usuario($id_cadastro);
				
			if (TRUE){
	
	
				$this->funcionario_model->setId($id);
				$result = $this->funcionario_model->excluir();
	
				if ($result){
	
					$this->session->set_flashdata('msg_titulo','SUCESSO!');
					$this->session->set_flashdata('msg_conteudo','O funcionário foi excluído.');
					$this->session->set_flashdata('msg_tipo','success');
	
					redirect('funcionario/listar');
	
				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível excluir.');
					$this->session->set_flashdata('msg_tipo','danger');
	
					redirect('funcionario/listar');
	
				}
	
			} else {
				redirect('funcionario/listar');
			}
				
				
		}
		else {
			redirect('funcionario/listar');
		}
	
	}

	public function editar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
		$id_cadastro = $this->uri->segment(4);
	
		if ($id != ''){
			
			if (TRUE){
				
				if(isset($_POST['btn_salvar'])){
				
					$this->funcionario_model->setId($id);
					$this->funcionario_model->setNome($this->validar_input($this->input->post("nome")));
					$this->funcionario_model->setLogin($this->validar_input($this->input->post("login")));
					$this->funcionario_model->setSenha(md5($this->input->post("senha")));					

					$this->funcionario_model->setvalidade($this->session->userdata['userprimesession']['validade']);
				
					$result = $this->funcionario_model->editar();
						
					if ($result){
						
						$this->session->set_flashdata('msg_alerta', 2);

						redirect("funcionario/listar");
							
					} else {
						$dados['msg_titulo'] = 'ERRO!';
						$dados['msg_conteudo'] = 'Não foi possível editar.';
						$dados['msg_tipo'] = 'danger';
					}
				
				}
					
				$this->funcionario_model->setId($id);
				$dados['funcionario'] = $this->funcionario_model->pesquisar_funcionario_id();

				$dados['id'] = $id;
				$dados['id_cadastro'] = $id_cadastro;
					
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('funcionario/funcionario_editar', $dados);
				$this->load->view('layout/footer');
				
			} else {
				redirect("funcionario/listar");
			}
				
		} else {
			redirect("funcionario/listar");
		}
	
	}

	public function cadastrar_permissoes(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
		
		$this->funcionario_model->setId($id);

		if(isset($_POST['btn_cadastrar'])){

			$this->funcionario_model->setId($id);

			//ATRIBUIÇÃO DE PERMISSÕES
			$permissoes_nova = array(

            	//ESTADUAL
            	'mEstadual' => $this->input->post('mEstadual'),
            	'mDashboardSefaz' => $this->input->post('mDashboardSefaz'),
            	'mMsgDte' => $this->input->post('mMsgDte'),
            	'mResumoFiscal' => $this->input->post('mResumoFiscal'),
            	'mSituacaoFiscal' => $this->input->post('mSituacaoFiscal'),
            	'mCertidaoNegativa' => $this->input->post('mCertidaoNegativa'),
            	'mProcessosSipro' => $this->input->post('mProcessosSipro'),
            	'mCompraVendas' => $this->input->post('mCompraVendas'),
            	'mIpva' => $this->input->post('mIpva'),
            	'mJucebViabilidade' => $this->input->post('mJucebViabilidade'),
            	'mJucebRegistro' => $this->input->post('mJucebRegistro'), 

            	// FEDERAL
            	'mFederal' => $this->input->post('mFederal'),
            	'mDashboardFederal' => $this->input->post('mDashboardFederal'),
            	'mMsgEcac' => $this->input->post('mMsgEcac'),
            	'mSituacaoFiscalEcac' => $this->input->post('mSituacaoFiscalEcac'),
            	'mParcelamentoDasEcac' => $this->input->post('mParcelamentoDasEcac'),
            	'mDividaAtiva' => $this->input->post('mDividaAtiva'),

            	// CALENDARIO
            	'mCalendario' => $this->input->post('mCalendario'), 

            	// PROTOCOLOS
            	'mProtocolos' => $this->input->post('mProtocolos'),

            	// CADASTROS
            	'mCadastros' => $this->input->post('mCadastros'),
            	'mEmpresas' => $this->input->post('mEmpresas'),
            	'mUsuarios' => $this->input->post('mUsuarios'),
            	'mContadores' => $this->input->post('mContadores'),
            	'mDocumentos' => $this->input->post('mDocumentos'),
            	'mTiposDeDocumentos' => $this->input->post('mTiposDeDocumentos'),

            	// RELATORIOS
            	'mRelatorios' => $this->input->post('mRelatorios'),
            	'mRelatorioPorEmpresas' => $this->input->post('mRelatorioPorEmpresas'),
            	'mRelatorioTimeline' => $this->input->post('mRelatorioTimeline'),
            	'mRelatorioGeral' => $this->input->post('mRelatorioGeral'),
            	'mRelatorioPorColaborador' => $this->input->post('mRelatorioPorColaborador'),
            	'mRelatorioSemColaborador' => $this->input->post('mRelatorioSemColaborador'),
            	'mRelatorioComCertificado' => $this->input->post('mRelatorioComCertificado'),
            	'mRelatorioSemCertificado' => $this->input->post('mRelatorioSemCertificado'),

            	// PAINEL
            	'mPainel' => $this->input->post('mPainel'),
            	'mRegistroDeAcesso' => $this->input->post('mRegistroDeAcesso'),
            	'mPlano' => $this->input->post('mPlano'),
            	'mPainelCliente' => $this->input->post('mPainelCliente'),
            );

            $permissoes_nova = serialize($permissoes_nova);

            $this->funcionario_model->setPermissoes($permissoes_nova);

            $result = $this->funcionario_model->cadastrar_permissoes();

            if ($result){
						
						$this->session->set_flashdata('msg_alerta', 1);
						redirect("funcionario/listar");
							
					} else {
						$dados['msg_titulo'] = 'ERRO!';
						$dados['msg_conteudo'] = 'Não foi possível editar.';
						$dados['msg_tipo'] = 'danger';
					}
		}

		$this->funcionario_model->setId($id);
		$dados['funcionario'] = $this->funcionario_model->pesquisar_funcionario_id();

		$dados['id'] = $id;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');			
		$this->load->view('layout/header', $dados);
		$this->load->view('funcionario/funcionario_cadastrar_permissoes', $dados);
		$this->load->view('layout/footer');
	}

	public function editar_permissoes(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);

		if(isset($_POST['btn_salvar'])){

			$this->funcionario_model->setId($id);

			//ATRIBUIÇÃO DE PERMISSÕES
			$permissoes_nova = array(

            	//ESTADUAL
            	'mEstadual' => $this->input->post('mEstadual'),
            	'mDashboardSefaz' => $this->input->post('mDashboardSefaz'),
            	'mMsgDte' => $this->input->post('mMsgDte'),
            	'mResumoFiscal' => $this->input->post('mResumoFiscal'),
            	'mSituacaoFiscal' => $this->input->post('mSituacaoFiscal'),
            	'mCertidaoNegativa' => $this->input->post('mCertidaoNegativa'),
            	'mProcessosSipro' => $this->input->post('mProcessosSipro'),
            	'mCompraVendas' => $this->input->post('mCompraVendas'),
            	'mIpva' => $this->input->post('mIpva'),
            	'mJucebViabilidade' => $this->input->post('mJucebViabilidade'),
            	'mJucebRegistro' => $this->input->post('mJucebRegistro'), 

            	// FEDERAL
            	'mFederal' => $this->input->post('mFederal'),
            	'mDashboardFederal' => $this->input->post('mDashboardFederal'),
            	'mMsgEcac' => $this->input->post('mMsgEcac'),
            	'mSituacaoFiscalEcac' => $this->input->post('mSituacaoFiscalEcac'),
            	'mParcelamentoDasEcac' => $this->input->post('mParcelamentoDasEcac'),
            	'mDividaAtiva' => $this->input->post('mDividaAtiva'),

            	// CALENDARIO
            	'mCalendario' => $this->input->post('mCalendario'), 

            	// PROTOCOLOS
            	'mProtocolos' => $this->input->post('mProtocolos'),

            	// CADASTROS
            	'mCadastros' => $this->input->post('mCadastros'),
            	'mEmpresas' => $this->input->post('mEmpresas'),
            	'mUsuarios' => $this->input->post('mUsuarios'),
            	'mContadores' => $this->input->post('mContadores'),
            	'mDocumentos' => $this->input->post('mDocumentos'),
            	'mTiposDeDocumentos' => $this->input->post('mTiposDeDocumentos'),

            	// RELATORIOS
            	'mRelatorios' => $this->input->post('mRelatorios'),
            	'mRelatorioPorEmpresas' => $this->input->post('mRelatorioPorEmpresas'),
            	'mRelatorioTimeline' => $this->input->post('mRelatorioTimeline'),
            	'mRelatorioGeral' => $this->input->post('mRelatorioGeral'),
            	'mRelatorioPorColaborador' => $this->input->post('mRelatorioPorColaborador'),
            	'mRelatorioSemColaborador' => $this->input->post('mRelatorioSemColaborador'),
            	'mRelatorioComCertificado' => $this->input->post('mRelatorioComCertificado'),
            	'mRelatorioSemCertificado' => $this->input->post('mRelatorioSemCertificado'),

            	// PAINEL
            	'mPainel' => $this->input->post('mPainel'),
            	'mRegistroDeAcesso' => $this->input->post('mRegistroDeAcesso'),
            	'mPlano' => $this->input->post('mPlano'),
            	'mPainelCliente' => $this->input->post('mPainelCliente'),     

            );

            $permissoes_nova = serialize($permissoes_nova);

            $this->funcionario_model->setPermissoes($permissoes_nova);

            $result = $this->funcionario_model->editar_permissoes();

            if ($result){
						
						$this->session->set_flashdata('msg_alerta', 3);
						redirect("funcionario/listar");
							
					} else {
						
						$this->session->set_flashdata('msg_alerta', 4);
						redirect("funcionario/listar");
					}
		}

		$this->funcionario_model->setId($id);
		$dados['funcionario'] = $this->funcionario_model->pesquisar_funcionario_id();

		$dados['id'] = $id;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');			
		$this->load->view('layout/header', $dados);
		$this->load->view('funcionario/funcionario_editar_permissoes', $dados);
		$this->load->view('layout/footer');
	}

	public function tornar_master(){

		$id = $this->uri->segment(3);

		$this->funcionario_model->setId($id);

		$result = $this->funcionario_model->tornar_master();

		if ($result){
	
			$this->session->set_flashdata('msg_alerta', 6);

			redirect('funcionario/listar');

		} else {
			
			$this->session->set_flashdata('msg_alerta', 7);

			redirect('funcionario/listar');

		}
	}

	public function remover_master(){

		$id = $this->uri->segment(3);

		$this->funcionario_model->setId($id);

		$result = $this->funcionario_model->remover_master();

		if ($result){
	
			$this->session->set_flashdata('msg_alerta', 7);

			redirect('funcionario/listar');

		} else {
			
			$this->session->set_flashdata('msg_alerta', 4);

			redirect('funcionario/listar');

		}
	}

	public function vincular_empresas(){

		$id = $this->uri->segment(3);

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		if(isset($_POST['btn_salvar'])){

			$this->funcionario_model->setId($id);

			$empresas = $this->input->post('multi_select');

            //$this->funcionario_model->setPermissoes($permissoes_nova);

            $result = $this->funcionario_model->atualizaListaEmpresaUsuario($id, $empresas);

            if ($result){
						
				$this->session->set_flashdata('msg_alerta', 5);
				redirect("funcionario/listar");
					
			} else {
				
				$this->session->set_flashdata('msg_alerta', 6);
				redirect("funcionario/listar");
			}
		}

		$this->funcionario_model->setId($id);
		$empresas = $this->empresa_model->buscar_para_vincular();
		$dados['empresas'] = $empresas;

		$empresas_vinculadas = $this->empresausuario_model->buscar_empresas_vinculadas($id);
		$dados['empresas_vinculadas'] = $empresas_vinculadas;

		$myhashmap = array();
    	foreach ($empresas_vinculadas as $item) {
    		$myhashmap[$item->id_empresa] = $item->id_empresa;
    	}

    	
    	foreach ($empresas as $item) {
    		if(isset($myhashmap[$item->id])){
    			$item->selected = 'selected';
    		}else{
    			$item->selected = '';
    		}
    	}
    		
		$dados['id'] = $id;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');			
		$this->load->view('layout/header', $dados);
		$this->load->view('funcionario/vincular_empresas', $dados);
		$this->load->view('layout/footer');
	}

}