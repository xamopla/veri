<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gfip extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('gfip_model');
       	$this->load->model('empresa_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";
		$titulo = "Todas";

		if($cnpj != "IRREGULAR" && $cnpj != "REGULAR" && $cnpj != "TODAS"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "TODAS";
		}else{
			if($cnpj == "IRREGULAR"){
				$titulo = "Irregulares";
			}else if($cnpj == "REGULAR"){
				$titulo = "Regulares";
			}

			$filtro = $cnpj;
		}

		$this->gfip_model->setFiltro($filtro);

		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		$dados['titulo'] = $titulo;

		$resultado = $this->gfip_model->listar();

		foreach($resultado as $r){
			if($r->possui_pendencia == 0){
				$r->modal = "";
			}else{
				$r->modal = $this->find_info_for_list($r->cnpj);
			}
		}

		$dados['consulta'] = $resultado;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('gfip/divergencia_gfip', $dados);
		$this->load->view('layout/footer');
		
	}

	public function find_info_for_modal(){
		$cnpj = $this->input->post('cnpj');

		$resultado = $this->gfip_model->find_info_for_modal($cnpj);

		echo json_encode($resultado);
	}


	public function find_info_for_list($cnpj){
		$resultado = $this->gfip_model->find_info_for_modal($cnpj);
		$texto_final = "";
		foreach ($resultado as $r) {
			$texto_final = $texto_final."|".$r->competencia." ".$r->fpas." ".$r->situacao." ".$r->rubrica." ".$r->valor;
		}

		return $texto_final;
	}


}