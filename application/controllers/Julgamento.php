<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Julgamento extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('julgamento_model');
    }

    public function listar(){

    	//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['consulta'] = $this->julgamento_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('julgamento/julgamento_listar', $dados);
		$this->load->view('layout/footer');
    }
}