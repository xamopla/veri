<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('usuario_model', 'e');
		$this->load->model('plano_model');
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function cadastrar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		if ($this->session->userdata['userprimesession']['nivel'] == 0){
				
			if(isset($_POST['btn_cadastrar'])){
	
				$this->e->setIe($this->validar_input($this->input->post("ie")));
				$this->e->setCnpj($this->validar_input($this->input->post("cnpj")));
				$this->e->setRazaoSocial($this->validar_input($this->input->post("razao_social")));
				$this->e->setNomeFantasia($this->validar_input($this->input->post("nome_fantasia")));
				$this->e->setCep($this->validar_input($this->input->post("cep")));
				$this->e->setLogradouro($this->validar_input($this->input->post("logradouro")));
				$this->e->setNumero($this->validar_input($this->input->post("numero")));
				$this->e->setComplemento($this->validar_input($this->input->post("complemento")));
				$this->e->setBairro($this->validar_input($this->input->post("bairro")));
				$this->e->setCidade($this->validar_input($this->input->post("cidade")));
				$this->e->setUf($this->validar_input($this->input->post("uf")));
				$this->e->setTelefone($this->validar_input($this->input->post("telefone")));
				$this->e->setEmail($this->validar_input($this->input->post("email")));
				$this->e->setCelular($this->validar_input($this->input->post("celular")));
				
				//$logo = $this->input->post("celular") ? $this->input->post("celular") : 'logos/logo.png';
				//$this->e->setLogo($logo);
				$this->e->setStatus('A');
				
				$this->e->setValidade($this->input->post("validade"));
				$this->e->setNivel($this->input->post("nivel"));
				$this->e->setSenha(md5(preg_replace("/[^0-9]/", "", $this->input->post("cnpj"))));
				$this->e->setNome($this->validar_input($this->input->post("nome")));
				$this->e->setLogin($this->validar_input($this->input->post("email")));
				
				$this->e->setIdCadastro($this->session->userdata['userprimesession']['id']);			
				date_default_timezone_set('America/Bahia');
				$this->e->setDataCadastro(date('Y-m-d H:i:s'));
					
				$result = $this->e->cadastrar();
					
				if ($result){
				
					$this->session->set_flashdata('msg_alerta', 1);
					redirect('usuario/listar');

				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
					$this->session->set_flashdata('msg_tipo','danger');
				}
	
			}
					
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('usuario/usuario_cadastrar');
			$this->load->view('layout/footer');
		
		} else {
			redirect("painel");
		}
	
	}
	
	public function listar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		if ($this->session->userdata['userprimesession']['nivel'] == 0){
			
			$dados['consulta'] = $this->e->listar();
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header', $dados);
			$this->load->view('usuario/usuario_listar', $dados);
			$this->load->view('layout/footer');
		} else {
			redirect("painel");
		}
		
	
	}
	
	public function perfil(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
	
		if ($id != ''){
				
			if ($this->validar_usuario($id)){
	
				if(isset($_POST['btn_salvar'])){
	
					$this->e->setId($id);
					
					$this->e->setCnpj($this->validar_input($this->input->post("cnpj")));
					$this->e->setRazaoSocial($this->validar_input($this->input->post("razao_social")));
					$this->e->setNomeFantasia($this->validar_input($this->input->post("nome_fantasia")));
					$this->e->setCep($this->validar_input($this->input->post("cep")));
					$this->e->setLogradouro($this->validar_input($this->input->post("logradouro")));
					$this->e->setNumero($this->validar_input($this->input->post("numero")));
					$this->e->setComplemento($this->validar_input($this->input->post("complemento")));
					$this->e->setBairro($this->validar_input($this->input->post("bairro")));
					$this->e->setCidade($this->validar_input($this->input->post("cidade")));
					$this->e->setUf($this->validar_input($this->input->post("uf")));
					$this->e->setTelefone($this->validar_input($this->input->post("telefone")));
					$this->e->setEmail($this->validar_input($this->input->post("email")));
					$this->e->setCelular($this->validar_input($this->input->post("celular")));
					$this->e->setEmailSefaz($this->validar_input($this->input->post("email_sefaz")));
					$this->e->setSenhaSefaz($this->validar_input($this->input->post("senha_sefaz")));
					$this->e->setServidorEmail($this->validar_input($this->input->post("servidor_email")));
	
					$result = $this->e->editar();
					$email_dte = $this->validar_input($this->input->post("email_sefaz"));
					$senha_dte = $this->validar_input($this->input->post("senha_sefaz"));
					$servidor_email = $this->validar_input($this->input->post("servidor_email"));

					$this->plano_model->salvar_dados_dte($email_dte, $senha_dte, $servidor_email);

					if ($result){

						$this->session->set_flashdata('msg_alerta', 2);

					} else {
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Não foi possível editar.');
						$this->session->set_flashdata('msg_tipo','danger');
					}
	
				}
					
				$this->e->setId($id);
				$dados['usuario'] = $this->e->pesquisar_usuario_id();
				
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('usuario/perfil', $dados);
				$this->load->view('layout/footer');
	
			} else {
				redirect("painel");
			}
	
		} else {
			redirect("painel");
		}
	
	}
	
	public function alterarsenha(){
	
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
	
		if ($id != ''){
	
			if ($this->validar_usuario($id)){
	
				if(isset($_POST['btn_salvar'])){
	
					$this->e->setId($id);
						
					$this->e->setSenha(md5($this->input->post("senha")));
	
					$result = $this->e->alterarsenha();
						
					if ($result){
						
						$this->session->set_flashdata('msg_alerta', 3);

					} else {
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Não foi possível alterar.');
						$this->session->set_flashdata('msg_tipo','danger');
					}
	
				}
					
				//$this->e->setId($id);
				//$dados['usuario'] = $this->e->pesquisar_usuario_id();
	
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('usuario/alterar_senha');
				$this->load->view('layout/footer');
	
			} else {
				redirect("painel");
			}
	
		} else {
			redirect("painel");
		}
	
	}
	
	private function set_upload_options() {
		//upload an image options
		$config = array();
	
		$config['upload_path'] = '/assets/img/contabilidades/';
	
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
	
		$config['encrypt_name'] = TRUE;
	
		//$config['max_size']='';
		$config['max_width']='500';
		$config['max_height']='500';
	
		return $config;
	}
	
	public function atualizarfoto(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
	
		if ($id != ''){
	
			if ($this->validar_usuario($id)){
									
				if(isset($_POST['btn_salvar'])){
					
					//UPLOAD
					$this->load->library('upload');
					
					
					// passamos as configurações para a library upload
					$this->upload->initialize($this->set_upload_options());
					
					// verificamos se o upload foi processado com sucesso
					if ( ! $this->upload->do_upload('userfile'))
					{
						// em caso de erro retornamos os mesmos para uma variável
						// e enviamos para a home
						$data= array('error' => $this->upload->display_errors());
						//$this->load->view('home',$data);
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo', $data['error']);
						$this->session->set_flashdata('msg_tipo','danger');
					}
					else
					{
						//se correu tudo bem, recuperamos os dados do arquivo
						$data['dadosArquivo'] = $this->upload->data();
						// definimos o path original do arquivo
						$arquivoPath = 'assets/img/contabilidades/'.$data['dadosArquivo']['file_name'];
						// passando para o array '$data'
						//$data['urlArquivo'] = base_url($arquivoPath);
						// definimos a URL para download
						//$downloadPath = 'assets/logos/'.$data['dadosArquivo']['file_name'];
						// passando para o array '$data'
						//$data['urlDownload'] = base_url($downloadPath);
												
						$this->e->setId($id);
						$this->e->setLogo($arquivoPath);
						
						$this->e->uploadimg();
						$this->session->userdata['userprimesession']['logo'] = $arquivoPath;
						// carregamos a view com as informações e link para download
						$this->session->set_flashdata('msg_titulo','SUCESSO!');
						$this->session->set_flashdata('msg_conteudo', 'A foto do perfil foi atualizada!');
						$this->session->set_flashdata('msg_tipo','success');
					}
					
				}
				
				$this->e->setId($id);
				$dados['usuario'] = $this->e->pesquisar_logo();
	
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('usuario/alterar_foto', $dados);
				$this->load->view('layout/footer');
	
			} else {
				redirect("painel");
			}
	
		} else {
			redirect("painel");
		}
	
	}


	public function atualizarfotoperfil(){

        $user_id = $this->uri->segment(3); 

        if ($user_id != ''){
                                    
                if(isset($_POST['btn_salvar'])){
                    
                    //UPLOAD
                    $this->load->library('upload');                 
                    
                    $hostCompleto = $_SERVER['HTTP_HOST'];
                    $server = explode('.', $hostCompleto);
                    $server = $server[0];

                    $image_upload_folder = FCPATH . 'assets/arquivos/perfil/'.$server.'/';

                    if (!file_exists($image_upload_folder)) {
                        mkdir($image_upload_folder, DIR_WRITE_MODE, true);
                    }

                    $this->upload_config = array(
                        'upload_path'   => $image_upload_folder,
                        'allowed_types' => 'png|jpg|jpeg',
                        'max_size'      => 2048,
                        'max_width'     => 1500,
                        'max_height'    => 1500,
                        'remove_space'  => TRUE,
                        'encrypt_name'  => TRUE,
                    );

                    $this->upload->initialize($this->upload_config);
                    
                    // verificamos se o upload foi processado com sucesso
                    if ( ! $this->upload->do_upload('userfile'))
                    {
                        // em caso de erro retornamos os mesmos para uma variável
                        // e enviamos para a home
                        $data= array('error' => $this->upload->display_errors());
                        //$this->load->view('home',$data); 
                        $this->session->set_flashdata('msg_conteudo', $data['error']); 
                        $this->session->set_flashdata('msg_alerta', 2);
                    }
                    else
                    {
                        //se correu tudo bem, recuperamos os dados do arquivo
                        $data['dadosArquivo'] = $this->upload->data();
                        // definimos o path original do arquivo
                        $arquivoPath = 'assets/arquivos/perfil/'.$server.'/'.$data['dadosArquivo']['file_name'];
                        // passando para o array '$data'
                        //$data['urlArquivo'] = base_url($arquivoPath);
                        // definimos a URL para download
                        //$downloadPath = 'assets/logos/'.$data['dadosArquivo']['file_name'];
                        // passando para o array '$data'
                        //$data['urlDownload'] = base_url($downloadPath);
                                                
                        $this->e->setId($user_id);
                        $this->e->setLogo($arquivoPath);
                        
                        $this->e->upload_img_perfil();
                        $this->session->userdata['userprimesession']['logo'] = $arquivoPath;
                        // carregamos a view com as informações e link para download
                        $this->session->set_flashdata('msg_alerta', 1); 
                    }
                    
                }
                
                redirect("chat");
    
        } else {
            redirect("chat");
        }
    
    }
	
}
