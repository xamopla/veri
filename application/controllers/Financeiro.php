<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Financeiro extends MY_Controller
{
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function listar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->load->model('financeiro_model');
		$this->financeiro_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->financeiro_model->listar();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('financeiro/financeiro_listar', $dados);
		$this->load->view('layout/footer');
		
	}
}