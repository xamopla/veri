<?php defined('BASEPATH') or exit('No direct script access allowed');

class Parcelamento_lei_12996 extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('parcelamento_lei_12996_model');
        $this->load->model('empresa_model');
    }

    public function listar()
    {
        date_default_timezone_set('America/Sao_Paulo');

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ---------------- 

        $cnpj = $this->uri->segment(3);
        $razao = "";
        $filtro = "";

        if ($cnpj != "EM_PARCELAMENTO" && $cnpj != "TODAS") {
            if ($cnpj != null) {
                $em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
                $razao = $em->razao_social;
            }
            $filtro = "TODAS";
        } else {
            $filtro = $cnpj;
        }

        $this->parcelamento_lei_12996_model->setFiltro("TODAS");

        $dados['razao_social_filtro'] = $razao;
        $dados['filtro'] = $filtro;

        $dados['dividas_consolidadas'] = $this->parcelamento_lei_12996_model->listar_dividas_consolidadas();

        //buscar parcela do mês atual e contar o total de parcelas 
        foreach ($dados['dividas_consolidadas'] as $divida) {
            foreach ($this->parcelamento_lei_12996_model->listar_demonstrativo_prestacoes($divida->id) as $parcela) {
                if (substr($parcela->data_parcela, 0, -4)  == date('Y') && substr($parcela->data_parcela, 4, -2) == date('m')) {
                    $parcela->vencimento = $this->formata_data_dia_mes_ano($parcela->data_parcela);
                    $divida->parcela_atual = $parcela;
                    break;
                }
            }
            $divida->parcelas_atrasadas = $this->parcelamento_lei_12996_model->get_parcelas_vencidas()->qtd;
            $divida->qtd_parcelas_a_vencer = $this->parcelamento_lei_12996_model->get_qtd_parcelas_a_vencer()->qtd;
        }

        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];

        $dados['banco'] = $server;

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('parcelamento/parcelamento_lei_12996', $dados);
        $this->load->view('layout/footer');
    }

    public function listar_detalhes()
    {
        $id = $this->uri->segment(3);

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ---------------- 

        $dados['divida_consolidada'] = $this->parcelamento_lei_12996_model->listar_divida_consolidada_individual($id);
        $dados['divida_consolidada']->data_adesao = $this->formata_data_dia_mes_ano($dados['divida_consolidada']->data_adesao);

        $dados['demonstrativo_prestacoes'] = $this->parcelamento_lei_12996_model->listar_demonstrativo_prestacoes($dados['divida_consolidada']->id);

        $prestacoes_em_atraso_valor = 0;
        $prestacoes_em_atraso_qtd = 0;

        foreach ($dados['demonstrativo_prestacoes'] as $prestacao) {
            switch ($prestacao->indicador_situacao_parcela) {
                case 'P':
                    $prestacao->indicador_situacao_parcela = 'PAGA';
                    break;

                case 'A':
                    $prestacao->indicador_situacao_parcela = 'A VENCER';
                    break;

                case 'D':
                    $prestacao->indicador_situacao_parcela = 'DEVEDORA';
                    $prestacoes_em_atraso_valor += $prestacao->saldo_parc_devedora + $prestacao->juros_parc_deverdora;
                    $prestacoes_em_atraso_qtd++;
                    break;
            }

            $prestacao->data_parcela = $this->formata_data_mes_ano($prestacao->data_parcela);
            $prestacao->valor_da_prestacao_basica = $this->formata_valor($prestacao->valor_parc_minima);
            $prestacao->saldo_devedor_atualizado = $this->formata_valor($prestacao->saldo_parc_devedora + $prestacao->juros_parc_deverdora);
        }

        if ($prestacoes_em_atraso_qtd > 0) {
            $dados['prestacoes_em_atraso'] = [
                'valor' => $this->formata_valor($prestacoes_em_atraso_valor),
                'qtd' => $prestacoes_em_atraso_qtd
            ];
        } else {
            $dados['prestacoes_em_atraso'] = [
                'valor' => '0,00',
                'qtd' => '0'
            ];
        }

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);
        $this->load->view('parcelamento/parcelamento_lei_12996_detalhes', $dados);
        $this->load->view('layout/footer');
    }

    public function buscar_parcelas_nao_pagas_for_modal()
    {
        $id_divida_consolidada = $this->input->post('id_divida_consolidada');

        $resultado_final = $this->parcelamento_lei_12996_model->listar_parcelas_nao_pagas_for_modal($id_divida_consolidada);

        foreach ($resultado_final as $parcela) {
            $parcela->data_vencimento = $this->formata_data_dia_mes_ano($parcela->data_parcela);
            $parcela->valor_prestacao = $this->formata_valor($parcela->saldo_parc_devedora + $parcela->juros_parc_deverdora);
        }

        echo json_encode($resultado_final);
    }

    private function formata_valor($valor)
    {
        $valor_formatado = substr($valor, 0, -2) . '.' . substr($valor, -2, 2);

        return number_format($valor_formatado, 2, ',', '.');
    }

    private function formata_data_dia_mes_ano($data)
    {
        $dia = substr($data, 6);
        $ano = substr($data, 0, -4);
        $mes = substr($data, 4, -2);

        return $dia . '/' . $mes . '/' . $ano;
    }

    private function formata_data_mes_ano($data)
    {
        $ano = substr($data, 0, -4);
        $mes = substr($data, 4, -2);

        switch ($mes) {
            case '01':
                return 'Jan/' . $ano;
                break;

            case '02':
                return 'Fev/' . $ano;
                break;

            case '03':
                return 'Mar/' . $ano;
                break;
            case '04':
                return 'Abr/' . $ano;
                break;

            case '05':
                return 'Mai/' . $ano;
                break;

            case '06':
                return 'Jun/' . $ano;
                break;

            case '07':
                return 'Jul/' . $ano;
                break;

            case '08':
                return 'Ago/' . $ano;
                break;

            case '09':
                return 'Set/' . $ano;
                break;

            case '10':
                return 'Out/' . $ano;
                break;

            case '11':
                return 'Nov/' . $ano;
                break;

            case '12':
                return 'Dez/' . $ano;
                break;
        }
    }
}
