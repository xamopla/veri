<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Changelog extends MY_Controller {

	public function __construct() {
        parent::__construct(); 
    }

	public function index(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('changelog/changelog');
		$this->load->view('layout/footer');
		
	} 

}