<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documento extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
        $this->load->model('log_model');
    }    

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function cadastrar(){

			//--------------- NOTIFICAÇÕES ---------------------
			$dados = $this->notificacoes();
			//-------------- FIM DAS NOTIFICAÇÕES ----------------

			$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);

			$empresas = $this->documento_model->buscar_empresas();
			$lista_empresas = array('' => "Selecionar empresa");

			foreach ($empresas as $value) {
				
				$lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
			}

			$documentos = $this->documento_model->buscar_tipodocumento();
			$lista_tipodedocumentos = array('' => "Selecionar Tipo de documento");

			foreach ($documentos as $value) {
				
				$lista_tipodedocumentos[$value->id] = $value->nome;
			}

			// $email = $this->documento_model->buscar_email();
			// $lista_email = array('' => "Selecione o Email");

			// foreach ($email as $value) {
				
			// 	$lista_email[$value->id] = $value->email;
			// }

			$dados['lista_empresas'] = $lista_empresas;
			$dados['lista_tipodedocumentos'] = $lista_tipodedocumentos;
			// $dados['lista_email'] = $lista_email;
			
		if(isset($_POST['btn_cadastrar'])){

			$this->documento_model->setAtivo($this->validar_input($this->input->post("ativo")));
			$this->documento_model->setIdEmpresa($this->validar_input($this->input->post("id_empresa")));
			$this->documento_model->setIdTipoDocumento($this->validar_input($this->input->post("id_tipoDocumento")));
			$this->documento_model->setNumeroDocumento($this->validar_input($this->input->post("numero_documento")));
			$this->documento_model->setNumeroProtocolo($this->validar_input($this->input->post("numero_protocolo")));
			$this->documento_model->setDataEmissao(implode("-", array_reverse(explode("/", $this->input->post("dataEmissao")))));
			$this->documento_model->setDataValidade(implode("-", array_reverse(explode("/", $this->input->post("dataValidade")))));
			$this->documento_model->setDiasNotificacao($this->validar_input($this->input->post("diasNotificacao")));
			$this->documento_model->setObservacoes($this->validar_input($this->input->post("observacao")));
			$this->documento_model->setAnexo1($this->validar_input($this->input->post("anexo1")));

			// VERIFICAR O NOME DA BASE
	        $hostCompleto = $_SERVER['HTTP_HOST'];
	        $server = explode('.', $hostCompleto);
	        $server = $server[0];
	        // VERIFICAÇÃO CONCLUÍDA 

			$folder = 'anexos/documentos/'.$server.'/';
	        // definimos o path onde o arquivo será gravado
	        $path = "./uploads/".$folder;
	 
	        // verificamos se o diretório existe
	        // se não existe criamos com permissão de leitura e escrita
	        if ( ! is_dir($path)) {
	        	mkdir($path, 0777, $recursive = true);
	    	}

		 	$configUpload['upload_path']   = $path;
	        // definimos - através da extensão - 
	        // os tipos de arquivos suportados
	        $configUpload['allowed_types'] = 'jpg|png|pdf|doc|xls|txt|xml|pfx|zip';
	        // definimos que o nome do arquivo
	        // será alterado para um nome criptografado
	        $configUpload['encrypt_name']  = TRUE;

            //$this->load->library('upload', $config);
            $this->load->library('upload');

            $this->upload->initialize($configUpload);

            if (!$this->upload->do_upload('anexo1')){
	            // em caso de erro retornamos os mesmos para uma variável
	            // e enviamos para a home
	            $data= array('error' => $this->upload->display_errors());
				//$this->load->view('home',$data);
				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo', $data['error']);
				$this->session->set_flashdata('msg_tipo','danger');
	        }else{
	            //se correu tudo bem, recuperamos os dados do arquivo
	            $data['dadosArquivo'] = $this->upload->data();
	            // definimos o path original do arquivo
	            $arquivoPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
	            // passando para o array '$data'
	            $data['urlArquivo'] = base_url($arquivoPath);
	            // definimos a URL para download
	            $downloadPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
	            // passando para o array '$data'
	            $data['urlDownload'] = base_url($downloadPath);
	 
	 			$this->documento_model->setAnexoNome($data['dadosArquivo']['orig_name']);
	 			$this->documento_model->setAnexoPath($data['urlDownload']);

	            // carregamos a view com as informações e link para download
	            //$this->load->view('download',$data);
	        }

			// $this->documento_model->setAnexo2($this->validar_input($this->input->post("anexo2")));
			// $this->documento_model->setAnexo3($this->validar_input($this->input->post("anexo3")));
			// $this->documento_model->setAnexo4($this->validar_input($this->input->post("anexo4")));
			//$this->documento_model->setIdEmail($this->validar_input($this->input->post("id_email")));
			$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);

				
			$result = $this->documento_model->cadastrar();
				
			if ($result){

				$this->session->set_flashdata('msg_alerta', 1);
				redirect('documento/listar');

			} else {
				
				$this->session->set_flashdata('msg_alerta', 4);				
			}

		}
				
		$this->load->view('layout/head');		
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('documento/documento_cadastrar', $dados);
		$this->load->view('layout/footer');
	
	}
	
	public function Download(){
        // recuperamos o terceiro segmento da url, que é o nome do arquivo
        $arquivo = $this->uri->segment(3);
        // recuperamos o segundo segmento da url, que é o diretório
        $diretorio = $this->uri->segment(2);
        // definimos original path do arquivo
        $arquivoPath = './uploads/'.$diretorio."/".$arquivo;
 
        // forçamos o download no browser 
        // passando como parâmetro o path original do arquivo
        force_download($arquivoPath,null);
    }

	public function listar(){
		$id_empresa = $this->uri->segment(3);
		$razao = "";
		if($id_empresa != null){
			$em = $this->empresa_model->find_empresa_by_id_empresa($id_empresa);
			$razao = $em->razao_social;
		}
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->documento_model->listar();
		
		$tour = $this->uri->segment(3);
		if($tour != ""){
			$dados['fromTour'] = "true";	
		}
		$dados['razao_social_filtro'] = $razao;

		$documentos = $this->documento_model->buscar_tipodocumento();
		$lista_tipodedocumentos = array('' => "Selecionar Tipo de documento");

		foreach ($documentos as $value) {
			
			$lista_tipodedocumentos[$value->id] = $value->nome;
		}
		$dados['lista_documentos'] = $lista_tipodedocumentos;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');		
		$this->load->view('layout/header', $dados);
		$this->load->view('documento/documento_listar', $dados);		
		$this->load->view('layout/footer');
		
	}
	
	public function excluir() {
	
		$id = $this->uri->segment(3); 
	
		if ($id != ''){ 
	
			$this->documento_model->setId($id);
			$result = $this->documento_model->excluir();

			if ($result){

				$this->session->set_flashdata('msg_alerta', 3);

				redirect('documento/listar');

			} else {
				
				$this->session->set_flashdata('msg_alerta', 4);

				redirect('documento/listar');

			}

		} else {
			redirect('documento/listar');
		} 
	
	}

	public function editar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3); 
	
		if ($id != ''){
			
			if (true){

				$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);

				$empresas = $this->documento_model->buscar_empresas();
				$lista_empresas = array('' => "Selecionar empresa");

				foreach ($empresas as $value) {
					
					$lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
				}

				$documentos = $this->documento_model->buscar_tipodocumento();
				$lista_tipodedocumentos = array('' => "Selecionar Tipo de documento");

				foreach ($documentos as $value) {
					
					$lista_tipodedocumentos[$value->id] = $value->nome;
				}				

				//$email = $this->documento_model->buscar_email();
				//$lista_email = array('' => "Selecione os Emails");

				//foreach ($email as $value) {
					
					//$lista_email[$value->id] = $value->email;
				//}

				$this->documento_model->setId($id);
				$dados['consulta'] = $this->documento_model->listaranexos();

				$dados['lista_empresas'] = $lista_empresas;
				$dados['lista_tipodedocumentos'] = $lista_tipodedocumentos;
				//$dados['lista_email'] = $lista_email;
				
				if(isset($_POST['btn_salvar'])){
				
					$this->documento_model->setId($id);
					$this->documento_model->setAtivo($this->validar_input($this->input->post("ativo")));
					$this->documento_model->setIdEmpresa($this->validar_input($this->input->post("id_empresa")));
					$this->documento_model->setIdTipoDocumento($this->validar_input($this->input->post("id_tipoDocumento")));
					$this->documento_model->setNumeroDocumento($this->validar_input($this->input->post("numero_documento")));
					$this->documento_model->setNumeroProtocolo($this->validar_input($this->input->post("numero_protocolo")));
					$this->documento_model->setDataEmissao(implode("-", array_reverse(explode("/", $this->input->post("dataEmissao")))));
					$this->documento_model->setDataValidade(implode("-", array_reverse(explode("/", $this->input->post("dataValidade")))));
					$this->documento_model->setDiasNotificacao($this->validar_input($this->input->post("diasNotificacao")));
					$this->documento_model->setObservacoes($this->validar_input($this->input->post("observacao")));
					$this->documento_model->setAnexo1($this->validar_input($this->input->post("anexo1")));
					// $this->documento_model->setAnexo2($this->validar_input($this->input->post("anexo2")));
					// $this->documento_model->setAnexo3($this->validar_input($this->input->post("anexo3")));
					// $this->documento_model->setAnexo4($this->validar_input($this->input->post("anexo4")));
					//$this->documento_model->setIdEmail($this->validar_input($this->input->post("id_email")));
				
					// VERIFICAR O NOME DA BASE
			        $hostCompleto = $_SERVER['HTTP_HOST'];
			        $server = explode('.', $hostCompleto);
			        $server = $server[0];
			        // VERIFICAÇÃO CONCLUÍDA 

					$folder = 'anexos/documentos/'.$server.'/';
			        // definimos o path onde o arquivo será gravado
			        $path = "./uploads/".$folder;
			 
			        // verificamos se o diretório existe
			        // se não existe criamos com permissão de leitura e escrita
			        if ( ! is_dir($path)) {
			        	mkdir($path, 0777, $recursive = true);
			    	}

				 	$configUpload['upload_path']   = $path;
			        // definimos - através da extensão - 
			        // os tipos de arquivos suportados
			        $configUpload['allowed_types'] = 'jpg|png|pdf|doc|xlsx|txt|xml|pfx|zip|docx|csv';
			        // definimos que o nome do arquivo
			        // será alterado para um nome criptografado
			        //$configUpload['encrypt_name']  = TRUE;

		            //$this->load->library('upload', $config);
		            $this->load->library('upload');

		            $this->upload->initialize($configUpload);

		            if (!$this->upload->do_upload('anexo1')){
			            // em caso de erro retornamos os mesmos para uma variável
			            // e enviamos para a home
			            $data= array('error' => $this->upload->display_errors());
						//$this->load->view('home',$data);
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo', $data['error']);
						$this->session->set_flashdata('msg_tipo','danger');
			        }else{
			            //se correu tudo bem, recuperamos os dados do arquivo
			            $data['dadosArquivo'] = $this->upload->data();
			            // definimos o path original do arquivo
			            $arquivoPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
			            // passando para o array '$data'
			            $data['urlArquivo'] = base_url($arquivoPath);
			            // definimos a URL para download
			            $downloadPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
			            // passando para o array '$data'
			            $data['urlDownload'] = base_url($downloadPath);
			 
			 			$this->documento_model->setAnexoNome($data['dadosArquivo']['file_name']);
			 			$this->documento_model->setAnexoPath($data['urlDownload']);

			            // carregamos a view com as informações e link para download
			            //$this->load->view('download',$data);
			        }

					$result = $this->documento_model->editar();
						
					if ($result){
						$this->session->set_flashdata('msg_alerta', 2);
						redirect("documento/listar");

					} else {
						
						$this->session->set_flashdata('msg_alerta', 4);
					}
				
				}
					
				$this->documento_model->setId($id);
				$dados['documento'] = $this->documento_model->pesquisar_documento_id();
					
				$dados['id'] = $id; 
					
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('documento/documento_editar', $dados);
				$this->load->view('layout/footer');
				
			} else {
				redirect("documento/listar");
			}
				
		} else {
			redirect("documento/listar");
		}
	
	}

	public function editar_aux(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3); 
	
		if ($id != ''){
			
			if (true){

				$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);

				$empresas = $this->documento_model->buscar_empresas();
				$lista_empresas = array('' => "Selecionar empresa");

				foreach ($empresas as $value) {
					
					$lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
				}

				$documentos = $this->documento_model->buscar_tipodocumento();
				$lista_tipodedocumentos = array('' => "Selecionar Tipo de documento");

				foreach ($documentos as $value) {
					
					$lista_tipodedocumentos[$value->id] = $value->nome;
				}				

				//$email = $this->documento_model->buscar_email();
				//$lista_email = array('' => "Selecione os Emails");

				//foreach ($email as $value) {
					
					//$lista_email[$value->id] = $value->email;
				//}

				$this->documento_model->setId($id);
				$dados['consulta'] = $this->documento_model->listaranexos();

				$dados['lista_empresas'] = $lista_empresas;
				$dados['lista_tipodedocumentos'] = $lista_tipodedocumentos;
				//$dados['lista_email'] = $lista_email;
				
				if(isset($_POST['btn_salvar'])){
				
					$this->documento_model->setId($id);
					$this->documento_model->setAtivo($this->validar_input($this->input->post("ativo")));
					$this->documento_model->setIdEmpresa($this->validar_input($this->input->post("id_empresa")));
					$this->documento_model->setIdTipoDocumento($this->validar_input($this->input->post("id_tipoDocumento")));
					$this->documento_model->setNumeroDocumento($this->validar_input($this->input->post("numero_documento")));
					$this->documento_model->setNumeroProtocolo($this->validar_input($this->input->post("numero_protocolo")));
					$this->documento_model->setDataEmissao(implode("-", array_reverse(explode("/", $this->input->post("dataEmissao")))));
					$this->documento_model->setDataValidade(implode("-", array_reverse(explode("/", $this->input->post("dataValidade")))));
					$this->documento_model->setDiasNotificacao($this->validar_input($this->input->post("diasNotificacao")));
					$this->documento_model->setObservacoes($this->validar_input($this->input->post("observacao")));
					$this->documento_model->setAnexo1($this->validar_input($this->input->post("anexo1")));
					// $this->documento_model->setAnexo2($this->validar_input($this->input->post("anexo2")));
					// $this->documento_model->setAnexo3($this->validar_input($this->input->post("anexo3")));
					// $this->documento_model->setAnexo4($this->validar_input($this->input->post("anexo4")));
					//$this->documento_model->setIdEmail($this->validar_input($this->input->post("id_email")));
				
					// VERIFICAR O NOME DA BASE
			        $hostCompleto = $_SERVER['HTTP_HOST'];
			        $server = explode('.', $hostCompleto);
			        $server = $server[0];
			        // VERIFICAÇÃO CONCLUÍDA 

					$folder = 'anexos/documentos/'.$server.'/';
			        // definimos o path onde o arquivo será gravado
			        $path = "./uploads/".$folder;
			 
			        // verificamos se o diretório existe
			        // se não existe criamos com permissão de leitura e escrita
			        if ( ! is_dir($path)) {
			        	mkdir($path, 0777, $recursive = true);
			    	}

				 	$configUpload['upload_path']   = $path;
			        // definimos - através da extensão - 
			        // os tipos de arquivos suportados
			        $configUpload['allowed_types'] = 'jpg|png|pdf|doc|xlsx|txt|xml|pfx|zip|docx|csv';
			        // definimos que o nome do arquivo
			        // será alterado para um nome criptografado
			        //$configUpload['encrypt_name']  = TRUE;

		            //$this->load->library('upload', $config);
		            $this->load->library('upload');

		            $this->upload->initialize($configUpload);

		            if (!$this->upload->do_upload('anexo1')){
			            // em caso de erro retornamos os mesmos para uma variável
			            // e enviamos para a home
			            $data= array('error' => $this->upload->display_errors());
						//$this->load->view('home',$data);
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo', $data['error']);
						$this->session->set_flashdata('msg_tipo','danger');
			        }else{
			            //se correu tudo bem, recuperamos os dados do arquivo
			            $data['dadosArquivo'] = $this->upload->data();
			            // definimos o path original do arquivo
			            $arquivoPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
			            // passando para o array '$data'
			            $data['urlArquivo'] = base_url($arquivoPath);
			            // definimos a URL para download
			            $downloadPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
			            // passando para o array '$data'
			            $data['urlDownload'] = base_url($downloadPath);
			 
			 			$this->documento_model->setAnexoNome($data['dadosArquivo']['file_name']);
			 			$this->documento_model->setAnexoPath($data['urlDownload']);

			            // carregamos a view com as informações e link para download
			            //$this->load->view('download',$data);
			        }

					$result = $this->documento_model->editar();
						
					if ($result){
						$this->session->set_flashdata('msg_alerta', 2);
						redirect("documento/listar");

					} else {
						
						$this->session->set_flashdata('msg_alerta', 4);
					}
				
				}
					
				$this->documento_model->setId($id);
				$dados['documento'] = $this->documento_model->pesquisar_documento_id();
					
				$dados['id'] = $id; 

				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('documento/documento_editar_aux', $dados);
				$this->load->view('layout/footer');
				
			} else {
				redirect("documento/listar");
			}
				
		} else {
			redirect("documento/listar");
		}
	
	}

	public function renovardocumento() {

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3); 
	
		if ($id != ''){
			
			if (true){				

				$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);


				//$email = $this->documento_model->buscar_email();
				//$lista_email = array('' => "Selecione os Emails");

				// foreach ($email as $value) {
					
				// 	$lista_email[$value->id] = $value->email;
				// }

				$this->documento_model->setId($id);
				$dados['consulta'] = $this->documento_model->listaranexos();
				//$dados['lista_email'] = $lista_email;
				
				if(isset($_POST['btn_salvar'])){
				
					$this->documento_model->setId($id);
					$this->documento_model->setAtivo($this->validar_input($this->input->post("ativo")));
					//$this->documento_model->setIdEmpresa($this->validar_input($this->input->post("id_empresa")));
					//$this->documento_model->setIdTipoDocumento($this->validar_input($this->input->post("id_tipoDocumento")));
					$this->documento_model->setNumeroDocumento($this->validar_input($this->input->post("numero_documento")));
					$this->documento_model->setNumeroProtocolo($this->validar_input($this->input->post("numero_protocolo")));
					$this->documento_model->setDataEmissao(implode("-", array_reverse(explode("/", $this->input->post("dataEmissao")))));
					$this->documento_model->setDataValidade(implode("-", array_reverse(explode("/", $this->input->post("dataValidade")))));
					$this->documento_model->setDiasNotificacao($this->validar_input($this->input->post("diasNotificacao")));
					$this->documento_model->setObservacoes($this->validar_input($this->input->post("observacoes")));
					$this->documento_model->setAnexo1($this->validar_input($this->input->post("anexo1")));
					// $this->documento_model->setAnexo2($this->validar_input($this->input->post("anexo2")));
					// $this->documento_model->setAnexo3($this->validar_input($this->input->post("anexo3")));
					// $this->documento_model->setAnexo4($this->validar_input($this->input->post("anexo4")));
					//$this->documento_model->setIdEmail($this->validar_input($this->input->post("id_email")));
				
					// VERIFICAR O NOME DA BASE
			        $hostCompleto = $_SERVER['HTTP_HOST'];
			        $server = explode('.', $hostCompleto);
			        $server = $server[0];
			        // VERIFICAÇÃO CONCLUÍDA 

					$folder = 'anexos/documentos/'.$server.'/';
			        // definimos o path onde o arquivo será gravado
			        $path = "./uploads/".$folder;
			 
			        // verificamos se o diretório existe
			        // se não existe criamos com permissão de leitura e escrita
			        if ( ! is_dir($path)) {
			        	mkdir($path, 0777, $recursive = true);
			    	}

				 	$configUpload['upload_path']   = $path;
			        // definimos - através da extensão - 
			        // os tipos de arquivos suportados
			        $configUpload['allowed_types'] = 'jpg|png|pdf|doc|xlsx|txt|xml|pfx|zip|docx|csv';
			        // definimos que o nome do arquivo
			        // será alterado para um nome criptografado
			        //$configUpload['encrypt_name']  = TRUE;

		            //$this->load->library('upload', $config);
		            $this->load->library('upload');

		            $this->upload->initialize($configUpload);

		            if (!$this->upload->do_upload('anexo1')){
			            // em caso de erro retornamos os mesmos para uma variável
			            // e enviamos para a home
			            $data= array('error' => $this->upload->display_errors());
						//$this->load->view('home',$data);
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo', $data['error']);
						$this->session->set_flashdata('msg_tipo','danger');
			        }else{
			            //se correu tudo bem, recuperamos os dados do arquivo
			            $data['dadosArquivo'] = $this->upload->data();
			            // definimos o path original do arquivo
			            $arquivoPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
			            // passando para o array '$data'
			            $data['urlArquivo'] = base_url($arquivoPath);
			            // definimos a URL para download
			            $downloadPath = 'uploads/'.$folder."/".$data['dadosArquivo']['file_name'];
			            // passando para o array '$data'
			            $data['urlDownload'] = base_url($downloadPath);
			 
			 			$this->documento_model->setAnexoNome($data['dadosArquivo']['file_name']);
			 			$this->documento_model->setAnexoPath($data['urlDownload']);

			            // carregamos a view com as informações e link para download
			            //$this->load->view('download',$data);
			        }

					$result = $this->documento_model->renovardocumento();
						
					if ($result){
						$this->session->set_flashdata('msg_alerta', 3);
						redirect("documento/listar");

					} else {
						$dados['msg_titulo'] = 'ERRO!';
						$dados['msg_conteudo'] = 'Não foi possível renovar o documento.';
						$dados['msg_tipo'] = 'danger';
					}
				
				}
					
				$this->documento_model->setId($id);
				$dados['documento'] = $this->documento_model->pesquisar_documento_id_renovar();
					
				$dados['id'] = $id; 
					
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('documento/documento_renovar', $dados);
				$this->load->view('layout/footer');
				
			} else {
				redirect("documento/listar");
			}
				
		} else {
			redirect("documento/listar");
		}
	}

	public function listardocumentoproxvenci(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->documento_model->listardocumentoproxvenci();
	
		$this->load->view('layout/head');	
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('documento/documento_listar', $dados);
		$this->load->view('layout/footer');	
	}

	public function listardocumentosvencidos() {

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
	
		$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->documento_model->listardocumentosvencidos();
	
		$this->load->view('layout/head');		
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('documento/documento_listar', $dados);
		$this->load->view('layout/footer');
	}
	
	public function listardocumentosregulares() {

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->documento_model->listardocumentosregulares();
		
		$this->load->view('layout/head');	
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('documento/documento_listar', $dados);	
		$this->load->view('layout/footer');
	}

	public function listarDocumentosEmpresas() {

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$id = $this->uri->segment(3); 

		$this->documento_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$this->documento_model->setIdEmpresa($id);
		$dados['consulta'] = $this->documento_model->listar_documentos_por_empresa();
		
		$this->load->view('layout/head');		
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('documento/documento_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function excluir_anexo_tela_editar(){

		$id = $this->uri->segment(3);  
	
		$this->documento_model->setId($id);
		$result = $this->documento_model->excluir_anexos_documentos();

		$this->session->set_flashdata('msg_alerta', 3);
		echo json_encode($result);
		 
	}

	public function excluir_anexo_tela_renovar(){

		$id = $this->uri->segment(3);  
	
		$this->documento_model->setId($id);
		$result = $this->documento_model->excluir_anexos_documentos();

		$this->session->set_flashdata('msg_alerta', 3);
		echo json_encode($result); 		 

	}

	public function update_tipo_documento(){
		$id = $this->input->post('id_documento');
		$tipo_documento = $this->input->post('tipo_documento');

		$this->documento_model->update_tipo_documento($id, $tipo_documento);

		echo 1;
	}
}
