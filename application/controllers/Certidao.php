<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

date_default_timezone_set('America/Sao_Paulo');
header ('Content-type: text/html; charset=ISO-8859-1');

class Certidao extends MY_Controller {

	public function consultar($cnpj){
		$this->load->model('M_certidao');
		print($this->M_certidao->verificar_cnp($cnpj));
	}

	public function return_html_certidao($cnpj){
		$this->load->model('M_certidao');
		print($this->M_certidao->return_certidao_html($cnpj));
	}

	public function download($cnpj){
		$this->load->model('M_certidao');
		$this->M_certidao->download($cnpj);
	}


}
