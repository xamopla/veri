<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consultar_certidoes_tempo_real extends MY_Controller {

	public function consultar_certidao_caixa(){

		// VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA 

		$banco_de_dados = $server;
		$cnpj = $this->uri->segment(3);

		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, "http://191.252.93.133/Caixa_sp/buscar_individual/".$banco_de_dados.'/'.$cnpj);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// grab URL and pass it to the browser
		// curl_exec($ch);
		$result = curl_exec($ch);
		// close cURL resource, and free up system resources
		$pos = strpos('Done Exit with code 1', $result);

		curl_close($ch);

		if ($pos === false) {

           // echo "sucesso";  
           $this->session->set_flashdata('msg_alerta', 3);
		   redirect('certidao_negativa/listar_caixa');
        } else {

    		// echo "erro";  
    		$this->session->set_flashdata('msg_alerta', 4);
			redirect('certidao_negativa/listar_caixa');
        } 

		// curl_setopt($ch, CURLOPT_URL, "http://191.252.93.133/Caixa/buscar_individual/".$banco_de_dados.'/'.$cnpj"); 

	}

	public function consultar_certidao_federal(){

		// VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA 

		$banco_de_dados = $server;
		$cnpj = $this->uri->segment(3);

		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, "http://191.252.93.133/Federal_sp/buscar_individual/".$banco_de_dados.'/'.$cnpj);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// grab URL and pass it to the browser
		// curl_exec($ch);
		$result = curl_exec($ch);
		// close cURL resource, and free up system resources
		$pos = strpos('Finalizado', $result);

		curl_close($ch);

		if ($pos === false) {

           // echo "sucesso";  
           $this->session->set_flashdata('msg_alerta', 3);
		   redirect('certidao_negativa/listar_federal_pgfn');
        } else {

    		// echo "erro";  
    		$this->session->set_flashdata('msg_alerta', 4);
			redirect('certidao_negativa/listar_federal_pgfn');
        } 
	}

	public function consultar_certidao_trabalhista(){

		// VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA 

		$banco_de_dados = $server;
		$cnpj = $this->uri->segment(3);
		
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, "http://191.252.93.133/Trabalhista_sp/buscar_individual/".$banco_de_dados.'/'.$cnpj);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// grab URL and pass it to the browser
		// curl_exec($ch);
		$result = curl_exec($ch);
		// close cURL resource, and free up system resources
		$pos = strpos('task finished', $result);

		curl_close($ch);

		if ($pos === false) {

           // echo "sucesso";  
           $this->session->set_flashdata('msg_alerta', 3);
		   redirect('certidao_negativa/listar_federal_pgfn');
        } else {

    		// echo "erro";  
    		$this->session->set_flashdata('msg_alerta', 4);
			redirect('certidao_negativa/listar_federal_pgfn');
        } 
	}

	public function consultar_certidao_ecac(){

	}

	public function consultar_certidao_cadin(){

	}

	public function consultar_certidao_stadual_saopaulo(){

		// VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA 

		$banco_de_dados = $server;
		$cnpj = $this->uri->segment(3);
		
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, "http://191.252.93.133/Saopaulo/buscar_estadual_individual/".$banco_de_dados.'/'.$cnpj);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// grab URL and pass it to the browser
		// curl_exec($ch);
		$result = curl_exec($ch);
		// close cURL resource, and free up system resources
		$pos = strpos('Finalizado', $result);

		curl_close($ch);

		if ($pos === false) {

           // echo "sucesso";  
           $this->session->set_flashdata('msg_alerta', 3);
		   redirect('certidao_negativa/listar_estadual');
        } else {

    		// echo "erro";  
    		$this->session->set_flashdata('msg_alerta', 4);
			redirect('certidao_negativa/listar_estadual');
        } 
	}

	public function consultar_certidao_municipal_saopaulo(){

		// VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA 

		$banco_de_dados = $server;
		$cnpj = $this->uri->segment(3);
		
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, "http://191.252.93.133/Saopaulo/certidao_sp_tempo_real/".$banco_de_dados.'/'.$cnpj);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// grab URL and pass it to the browser
		// curl_exec($ch);
		$result = curl_exec($ch);
		// close cURL resource, and free up system resources
		$pos = strpos('Finalizado', $result);

		curl_close($ch);

		if ($pos === false) {

           // echo "sucesso";  
           $this->session->set_flashdata('msg_alerta', 3);
		   redirect('certidao_negativa/listar_municipal');
        } else {

    		// echo "erro";  
    		$this->session->set_flashdata('msg_alerta', 4);
			redirect('certidao_negativa/listar_municipal');
        } 
	}

}