<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aviso extends MY_Controller {

	public function erro_autentiticar_email(){

		// CÓDIGO E0001A 
		$this->load->view('layout/head_avisos');
		$this->load->view('avisos/erro_autenticar_email');
	}

	public function erro_de_bd(){

		// CÓDIGO B0001A 
		$this->load->view('layout/head_avisos');
		$this->load->view('avisos/erro_de_banco_de_dados');
	}
}