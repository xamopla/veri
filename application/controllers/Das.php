<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Das extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('das_model');
        $this->load->model('empresa_model');
        $this->load->model('ecac_model');
    }

    public function index(){
        date_default_timezone_set('America/Sao_Paulo');
        $mes_aux = '';
        $ano_aux = '';

        if(isset($_GET['filtro'])){
            $filtro = $_GET["filtro"];
        }else{
            $filtro = 'TODAS';
        }

        $this->das_model->setFiltro_situacao($filtro);

        $dados['filtro'] = $filtro;

        $botao = $this->uri->segment(3);
        if($botao != null){

            $mes = $this->uri->segment(4);
            $mes_numero = 1;
            $ano = $this->uri->segment(5);

            switch ($mes) {
                case "Janeiro":    $mes_numero = 1;     break;
                case "Fevereiro":    $mes_numero = 2;   break;
                case "Marco":    $mes_numero = 3;       break;
                case "Abril":    $mes_numero = 4;       break;
                case "Maio":    $mes_numero = 5;        break;
                case "Junho":    $mes_numero = 6;       break;
                case "Julho":    $mes_numero = 7;       break;
                case "Agosto":    $mes_numero = 8;      break;
                case "Setembro":    $mes_numero = 9;    break;
                case "Outubro":    $mes_numero = 10;     break;
                case "Novembro":    $mes_numero = 11;    break;
                case "Dezembro":    $mes_numero = 12;    break;
            }

            if($botao == "anterior"){
                $mes_numero--;
                if($mes_numero == 0){
                    $mes_numero = 12;
                    $ano--;
                }
            }else{
                $mes_numero++;
                if($mes_numero == 13){
                    $mes_numero = 1;
                    $ano++;
                }
            }
            $this->das_model->setFiltroMes($mes_numero);
            $this->das_model->setFiltroAno($ano);

            if($mes_numero < 10){
                $mes_aux = '0'.$mes_numero;
            }else{
                $mes_aux = $mes_numero;
            }

            $ano_aux = $ano;

            $resultados = $this->das_model->listarFiltro();

            $dados['m'] = $mes_numero;
            $dados['ano'] = $ano;

        } elseif (isset($_POST['mes_selecionado'])) {

            $mes_selecionado = $this->input->post("mes_selecionado");
            $ano_selecionado = $this->input->post("ano_selecionado");

            $this->das_model->setFiltroMes($mes_selecionado);
            $this->das_model->setFiltroAno($ano_selecionado);

            $mes_aux = $mes_selecionado;
            $ano_aux = $ano_selecionado;

            $resultados = $this->das_model->listarFiltroByBotaoMes();
            $dados['m'] = $mes_selecionado;
            $dados['ano'] = $ano_selecionado;

        } else{

            $resultados = $this->das_model->listar();

            $data_atual = date('Y-m-d');

            $data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months'));

            $arr = explode("-", $data_aux);
            $mes_ = $arr[1];
            $ano_ = $arr[0];

            $dados['m'] = $mes_;
            $dados['ano'] = $ano_;

            $mes_aux = $mes_;
            $ano_aux = $ano_;
        }

        $ultimo_dia_transmitir = $this->get_days_uteis(20, $mes_aux+1, $ano_aux );
        //$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

        // $data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

        // $data_aux = date('Y-m-d', strtotime($data_format.' + 1 months'));

        $dados['ultimo_dia_transmitir'] = $ultimo_dia_transmitir;

        $debitos = $this->das_model->find_all_debitos();
        $myhashmap = array();
        foreach ($debitos as $d) {
            $myhashmap[$d->cnpj] = $d;
        }

        foreach ($resultados as $r) {
            if(isset($myhashmap[$r->cnpj])){
                $r->possui_debito = 1;
            }else{
                $r->possui_debito = 0;
            }

            // VERIFICA SE EXISTE DAS PAGO RETIFICADO
            $das_pago_retificado = $this->das_model->find_pago_retificado($r->numero_declaracao);

            if($das_pago_retificado)
                $r->possui_das_pago_retificado = true;
            else
                $r->possui_das_pago_retificado = false;

            $das_duplicidade = $this->das_model->get_das_duplicado($r->cnpj, $r->compentencia);


            if($das_duplicidade->quantidade > 1){
                $r->possui_das_duplicidade = true;
            }else{
                $r->possui_das_duplicidade = false;
            }
        }


        $dados['consulta'] = $resultados;
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];

        $dados['banco'] = $server;

        //--------------- NOTIFICAÇÕES ---------------------
        $notificacoes = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        $this->load->view('layout/head');
        $this->load->view('layout/header', $notificacoes);
        $this->load->view('layout/sidebar');
        $this->load->view('ecac/das', $dados);
        $this->load->view('layout/footer');
    }


    public static function getDiaUtil($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
        date_default_timezone_set('America/Sao_Paulo');

        $iMes = empty($iMes) ? date('m') : $iMes;
        $iAno = empty($iAno) ? date('Y') : $iAno;
        $iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));

        for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
            $iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
            //inclui apenas os dias úteis
            if ($iDiaSemana < 6) {
                $aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
            }
        }
        //ignorando os feriados
        if (sizeof($aDiasIgnorar) > 0) {
            foreach ($aDiasIgnorar as $iDia) {
                $iKey = array_search($iDia, $aDias);
                unset($aDias[$iKey]);
            }
        }

        if (isset($aDias[$iDia - 1])) {
            return $aDias[$iDia - 1];
        } else {
            //retorna o último dia útil
            return $aDias[count($aDias) - 1];
        }
    }

    public function list_from_notification(){
        $cnpj = $this->uri->segment(3);
        $razao = "";
        if($cnpj != null){
            $em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
            $razao = $em->razao_social;
        }

        date_default_timezone_set('America/Sao_Paulo');
        $mes_aux;
        $ano_aux;

        $resultados = $this->das_model->listar();

        $data_atual = date('Y-m-d');

        $data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months'));

        $arr = explode("-", $data_aux);
        $mes_ = $arr[1];
        $ano_ = $arr[0];

        $dados['m'] = $mes_;
        $dados['ano'] = $ano_;

        $mes_aux = $mes_;
        $ano_aux = $ano_;

<<<<<<< HEAD
        $ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

        $data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

        $data_aux = date('Y-m-d', strtotime($data_format.' + 1 months'));

        $dados['ultimo_dia_transmitir'] = $data_aux;
        $dados['consulta'] = $resultados;
        $dados['razao_social_filtro'] = $razao;

        //--------------- NOTIFICAÇÕES ---------------------
        $notificacoes = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        $this->load->view('layout/head');
        $this->load->view('layout/header', $notificacoes);
        $this->load->view('layout/sidebar');
        $this->load->view('ecac/das', $dados);
        $this->load->view('layout/footer');
    }

    public function find_debitos_for_modal(){
        $cnpj = $this->input->post('cnpj');

        $resultado = $this->das_model->find_debitos_for_modal($cnpj);

        echo json_encode($resultado);
    }

    public function find_das_duplicidade_modal(){
        $cnpj = $this->input->post('cnpj');
        $competencia = $this->input->post('compentencia');

        $resultado = $this->das_model->find_das_duplicidade_modal($cnpj, $competencia);

        echo json_encode($resultado);
    }
=======
		$dados['consulta'] = $resultados;
		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/das', $dados);
		$this->load->view('layout/footer');	
	}

	public function get_days_uteis($dia, $mes, $ano){
		if($mes == 13){
			$mes = '01';
			$ano++;
		}
		$tmpDate = $ano.'-'.$mes.'-'.$dia;


		$holidays = [$ano.'-01-01', $ano.'-02-17', $ano.'-04-02', $ano.'-04-21', $ano.'-05-01', $ano.'-06-03', $ano.'-09-07', $ano.'-10-12', $ano.'-11-02', $ano.'-11-15', $ano.'-12-25'];
		$i = 0;
		$nextBusinessDay = date('Y-m-d', strtotime($tmpDate . ' +' . $i . ' Weekday'));

		while (in_array($nextBusinessDay, $holidays)) {
		    $i++;
		    $nextBusinessDay = date('Y-m-d', strtotime($tmpDate . ' +' . $i . ' Weekday'));
		}
		return $nextBusinessDay;

	}
	
	public static function getDiaUtil($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
		date_default_timezone_set('America/Sao_Paulo');

		$iMes = empty($iMes) ? date('m') : $iMes;
		$iAno = empty($iAno) ? date('Y') : $iAno;
		$iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));
 
		for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
			$iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
			//inclui apenas os dias úteis
			if ($iDiaSemana < 6) {
				$aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
			}
		}
		//ignorando os feriados	
		if (sizeof($aDiasIgnorar) > 0) {
			foreach ($aDiasIgnorar as $iDia) {
				$iKey = array_search($iDia, $aDias);
				unset($aDias[$iKey]);
			}
		}
 
		if (isset($aDias[$iDia - 1])) {
			return $aDias[$iDia - 1];
		} else {
			//retorna o último dia útil
			return $aDias[count($aDias) - 1];
		}
	}

	public function list_from_notification(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;

		$resultados = $this->das_model->listar();

		$data_atual = date('Y-m-d');

		$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

		$arr = explode("-", $data_aux);
		$mes_ = $arr[1];
		$ano_ = $arr[0];

		$dados['m'] = $mes_;
		$dados['ano'] = $ano_;

		$mes_aux = $mes_;
		$ano_aux = $ano_;
		
		$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;
		$dados['consulta'] = $resultados;
		$dados['razao_social_filtro'] = $razao;

		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/das', $dados);
		$this->load->view('layout/footer');	
	}

	public function find_debitos_for_modal(){
		$cnpj = $this->input->post('cnpj');

		$resultado = $this->das_model->find_debitos_for_modal($cnpj);

		echo json_encode($resultado);
	}
>>>>>>> 2ae6c155392e7885cbb33b6f9fba074d42bc6192

    public function find_das_pago_retificado_for_modal(){
        $numero_declaracao = $this->input->post('numero_declaracao');

        $resultado = $this->das_model->find_pago_retificado($numero_declaracao);

        echo json_encode($resultado);
    }

    public function gerar_das_function(){
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];

        $banco = $server;

        $numero_declaracao = $this->input->post('numero_declaracao');
        $competencia = trim(str_replace('PA', "", $this->input->post('competencia')));
        $cnpj = $this->input->post('cnpj');

        $id_empresa = $this->das_model->find_empresa_by_cnpj($cnpj)->id;
        $cerficados = $this->das_model->get_aux($id_empresa);

        if(!empty($cerficados)){
            $retorno = $this->gerar_das_procuracao($numero_declaracao, $competencia, $cnpj, $banco, $cerficados);
            echo $retorno;
        }else{
            $retorno = $this->gerar_das($numero_declaracao, $competencia, $cnpj, $banco);
            echo $retorno;
        }
    }

    public function gerar_das($numero_declaracao, $competencia, $cnpj, $banco){

        $certificado = $this->ecac_model->find_certificado($cnpj);
        $folder_pdf = FCPATH . 'crons-api/pdf-das-ecac/'.$banco.'/' . str_replace('/', '-', $competencia);

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $caminho_aux = str_replace("/var/www/html", "",$folder_pdf);
        $caminho_download_aux = "https://veri-sp.com.br".$caminho_aux;

        $params = array('caminho_certificado' => 'crons-api/'.str_replace('//','/', $certificado->caminho_arq ) ,
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf);
        $this->load->library('Ecac_robo_library', $params, 'ecac_robo_library');

        if(!$this->ecac_robo_library->acesso_valido()){
            unset($this->ecac_robo_library);
            echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
        }
        $caminho_download = $this->ecac_robo_library->gerar_das($competencia);
        if($caminho_download != ""){
            $this->das_model->update_caminho_download_das($caminho_download_aux , $numero_declaracao);
            unset($this->ecac_robo_library);
            return $caminho_download;
        }else{
            unset($this->ecac_robo_library);
            return "ERRO";
        }
    }

    public function gerar_das_procuracao($numero_declaracao, $competencia, $cnpj, $banco, $cerficados){

        $folder_pdf = FCPATH . 'crons-api/pdf-das-ecac/'.$banco.'/' . str_replace('/', '-', $competencia);

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $caminho_aux = str_replace("/var/www/html", "",$folder_pdf);
        $caminho_download_aux = "https://veri-sp.com.br".$caminho_aux;

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        //$cerficados = $this->certificadocontador_model->get();

        foreach ($cerficados as $cerficado_contador) {

            $params = array('caminho_certificado' => 'crons-api/'.str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()) {
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj);

            if( $empresa ){

                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($cnpj);

                if(! $validado){
                    echo "ERRO2";
                    continue;
                }

                $caminho_download = $this->ecac_robo_library_eprocessos_procuracao->gerar_das($competencia, $cnpj);
                if($caminho_download != ""){
                    $this->das_model->update_caminho_download_das($caminho_download_aux , $numero_declaracao);
                    unset($this->ecac_robo_library_eprocessos_procuracao);
                    return $caminho_download;
                    break;
                }else{
                    unset($this->ecac_robo_library_eprocessos_procuracao);
                    return "ERRO";
                }


            }
        }

        unset($this->ecac_robo_library_eprocessos_procuracao);
    }

}