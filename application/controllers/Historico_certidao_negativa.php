<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Historico_certidao_negativa extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('historico_certidao_negativa_model');
    }

    public function buscar_historico_fgts(){

    	$cnpj = $this->input->post("cnpj");

		$resultado = $this->historico_certidao_negativa_model->consultar_fgts($cnpj);

		date_default_timezone_set('America/Sao_Paulo');

		foreach ($resultado as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d'));
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }

		echo json_encode($resultado);
    }

    public function buscar_historico_pgfn(){

    	$cnpj = $this->input->post("cnpj");

		$resultado = $this->historico_certidao_negativa_model->consultar_pgfn($cnpj);

		date_default_timezone_set('America/Sao_Paulo');

		foreach ($resultado as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d'));
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }

		echo json_encode($resultado);
    }

    public function buscar_historico_trabalhista(){

    	$cnpj = $this->input->post("cnpj");

		$resultado = $this->historico_certidao_negativa_model->consultar_trabalhista($cnpj);

		date_default_timezone_set('America/Sao_Paulo');

		foreach ($resultado as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d'));
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }
        
		echo json_encode($resultado);
    }
}