<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nfce_pdf extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
    }

    // VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function index(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------
		
		$this->load->library('session');		
		if(isset($this->session->userdata['nfce_pdf'])){
			$data = $this->session->userdata['nfce_pdf'];
		}else{
			$data = array(
				'autorizadas' => 0,
				'valor_autorizadas' => '0.00',
				'canceladas' => 0,
				'valor_canceladas' => '0.00',
				'tabela' => ''
			);
		}		

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/nfce_pdf', $data);
		$this->load->view('layout/footer');
	}

	public function read(){
		$this->load->library('session');
		$this->load->library('my_nfcepdf');
		if(isset($_POST['btn_cadastrar'])){
			$conversao = new My_nfcepdf;
			$data = $conversao->conversao($_FILES['pdf_file']);	
			$this->session->set_userdata("nfce_pdf",$data);
		}
		redirect('nfce_pdf/index');
	}

}