<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_api extends CI_Controller {

	public function logar()
	{   

		$this->load->model("login_model", "login");

			$verificacao = 	$this->login->logar($this->input->post("qtd_usuarios"), md5($this->input->post("qtd_empresas")));

			if ($verificacao != 'invalido' && $verificacao != 'inativo' && $verificacao != 'expirado'){
				
				$this->session->set_userdata("userprimesession", $verificacao);

				//GRAVA O LOG DE ACESSO AO SISTEMA WEB
				$this->load->model('funcionario_model');
				$this->funcionario_model->setId($this->session->userdata['userprimesession']['id']);
				date_default_timezone_set('America/Bahia');
				$this->funcionario_model->setDataAcesso(date('Y-m-d H:i:s'));
				$this->funcionario_model->update_acesso();

				$login2 = $this->session->userdata['userprimesession']['email_sefaz'];
				$senha2 = $this->session->userdata['userprimesession']['senha_sefaz'];

				if($login2 != null && $login2 != "" && $senha2 != null && $senha2 != ""){
					$data = array('param1'=>'modal');
					// store data to flashdata
					$this->session->set_flashdata('data',$data);
					
					//in other side
					$array = $this->session->flashdata('data');	
				}else{
					$data = array('param1'=>'false');
					// store data to flashdata
					$this->session->set_flashdata('data',$data);
					
					//in other side
					$array = $this->session->flashdata('data');
				}
				
				$array = array(
					'error'	=> false,
					'mensagem' => 'Login Confirmado',
					'url' => base_url() . 'painel'
				);

				echo json_encode($array); 

				
			} else if ($verificacao == 'invalido'){

				$array = array(
					'error'	=> true,
					'mensagem' => 'Usuario ou senha incorretas'
				);

				echo json_encode($array); 

			} else if ($verificacao == 'inativo') {

				$array = array(
					'error'	=> true,
					'mensagem' => 'Usuario inativado'
				);

				echo json_encode($array);  

			} else if ($verificacao == 'expirado') {

				$array = array(
					'error'	=> true,
					'mensagem' => 'O seu periodo de utilizacao expirou, por favor entre em contato'
				);

				echo json_encode($array);  
			}
		}   
	}  