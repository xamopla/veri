<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pronampe extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('pronampe_model');
       	$this->load->model('ecac_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";
		$titulo = "Todas";

		if($cnpj != "IRREGULAR" && $cnpj != "REGULAR" && $cnpj != "TODAS"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "TODAS";
		}else{
			if($cnpj == "IRREGULAR"){
				$titulo = "Irregulares";
			}else if($cnpj == "REGULAR"){
				$titulo = "Regulares";
			}

			$filtro = $cnpj;
		}

		$this->pronampe_model->setFiltro($filtro);

		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		$dados['titulo'] = $titulo;

		$resultado = $this->pronampe_model->listar();

		foreach ($resultado as $r) {
			$possui_certificado = false;
			$certificado = $this->ecac_model->find_certificado($r->cnpj);
			if( $certificado ){
				$possui_certificado = true;
			}
			$r->possui_certificado = $possui_certificado;

			if($r->lida == 1 && !empty($r->conteudo)){
				$conteudo =  $this->pronampe_model->find_info_for_modal($r->id_caixa_postal_mensagem);
				$r->hash_code = $this->extrai_hashcode($conteudo->conteudo);
			}
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
		$dados['consulta'] = $resultado;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('pronampe/pronampe', $dados);
		$this->load->view('layout/footer');
		
	}

	public function find_info_for_modal(){
		$id = $this->uri->segment(3);

		$resultado = $this->pronampe_model->find_info_for_modal($id);

		$string = $resultado->conteudo;

		$hash_code = "";
		$posicao_hash = strpos($string, "Hash Code:");
        if ($posicao_hash !== false) {

        	$string_resumida = substr($string,$posicao_hash+10);

            $inicio = explode(".", $string_resumida); 

            $hash_code = trim($inicio[0]);
        } 

        $resultado->hash_code = $hash_code;

		echo json_encode($resultado);
	}

	public function extrai_hashcode($string){
		$hash_code = "";
		$posicao_hash = strpos($string, "Hash Code:");
        if ($posicao_hash !== false) {

        	$string_resumida = substr($string,$posicao_hash+10);

            $inicio = explode(".", $string_resumida); 

            $hash_code = trim($inicio[0]);
        } 

        return $hash_code;
	}


}