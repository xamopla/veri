<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parecer extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("parecer_model");
		$this->load->model("empresa_model");
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');	
	}

	public function index(){

		//--------------- NOTIFICAÇÕES ---------------------		
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		// LISTA DE EMPRESAS PARA O MODAL
		$empresas = $this->documento_model->buscar_empresas();
		$lista_empresas = array('' => "Selecionar empresa");

		foreach ($empresas as $value) {
			
			$lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
		}

		$dados['lista_empresas'] = $lista_empresas;
		// FIM DA LISTA DE EMPRESAS PARA O MODAL

		$dados['consulta'] = $this->parecer_model->listar();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);		
		$this->load->view('parecer/parecer_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function consultaaguardando(){

		//--------------- NOTIFICAÇÕES ---------------------		
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		// LISTA DE EMPRESAS PARA O MODAL
		$empresas = $this->documento_model->buscar_empresas();
		$lista_empresas = array('' => "Selecionar empresa");

		foreach ($empresas as $value) {
			
			$lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
		}

		$dados['lista_empresas'] = $lista_empresas;
		// FIM DA LISTA DE EMPRESAS PARA O MODAL

		$dados['consulta'] = $this->parecer_model->listar_processos_aguardando();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);		
		$this->load->view('parecer/parecer_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function consultafinalizadas(){

		//--------------- NOTIFICAÇÕES ---------------------		
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		// LISTA DE EMPRESAS PARA O MODAL
		$empresas = $this->documento_model->buscar_empresas();
		$lista_empresas = array('' => "Selecionar empresa");

		foreach ($empresas as $value) {
			
			$lista_empresas[$value->id] = $value->razao_social.' - CNPJ '.$value->cnpj_completo;
		}

		$dados['lista_empresas'] = $lista_empresas;
		// FIM DA LISTA DE EMPRESAS PARA O MODAL

		$dados['consulta'] = $this->parecer_model->listar_processos_finalizados();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);		
		$this->load->view('parecer/parecer_listar', $dados);
		$this->load->view('layout/footer');
	}

	public function cadastrar(){

		if(isset($_POST['btn_cadastrar'])){

			$this->parecer_model->setIdEmpresa($this->input->post("id_empresa"));
			$this->parecer_model->setNumeroProcesso($this->input->post("numero_processo"));	
			$this->parecer_model->setStatusParecer(1);			
			date_default_timezone_set('America/Bahia');
			$this->parecer_model->setDataInclusao(date('Y-m-d H:i:s'));
			$this->parecer_model->setDataCadastro(date('Y-m-d H:i:s'));
			$this->parecer_model->setIdCadastro($this->session->userdata['userprimesession']['id']);

			$result = $this->parecer_model->cadastrar();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 1);
				redirect("parecer");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 2);			
				redirect("parecer");
				
			}

		}
	}

	public function remover(){

		$id = $this->uri->segment(3);
		$this->parecer_model->setId($id);

		$result = $this->parecer_model->remover();

		if ($result){

			$this->session->set_flashdata('msg_alerta', 3);	
			redirect('parecer');

		} else {
			
			$this->session->set_flashdata('msg_alerta', 4);		
			redirect('parecer');

		}	
	}
}