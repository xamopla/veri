<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Limite_simples extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('limite_simples_model');
        $this->load->model('empresa_model');
        $this->load->model('socios_empresas_model');
    } 


    public function listar(){

        $cnpj = $this->uri->segment(3);
        $razao = "";
        $filtro = "";

        if($cnpj != "PROXIMAS" && $cnpj != "LIMITE" && $cnpj != "TODAS" && $cnpj != "PROXIMAS_ACUMULADA" && $cnpj != "LIMITE_ACUMULADA"){
            if($cnpj != null){
                $em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
                $razao = $em->razao_social;
            }
            $filtro = "TODAS";
        }else{
            if($cnpj == "PROXIMAS"){
                $titulo = "Empresas próximas ao limite do Simples Nacional";
            }elseif($cnpj == "LIMITE"){
                $titulo = "Empresas que extrapolou o limite do Simples Nacional";
            }elseif($cnpj == "PROXIMAS_ACUMULADA"){
                $titulo = "Total Acumulado (Sócios/PJ) próximas ao limite do Simples Nacional";
            }elseif($cnpj == "LIMITE_ACUMULADA"){
                $titulo = "Total Acumulado (Sócios/PJ) que extrapolou o limite do Simples Nacional";
            }else{
                $cnpj = "TODAS";
            }
            $filtro = $cnpj;
        }

        $this->limite_simples_model->setFiltro($filtro);

        $resultado = $this->limite_simples_model->buscar();
        $myhashmap_limites = array();
        foreach ($resultado as $r) {
            $myhashmap_limites[$r->cnpj] = $r;
        }

        $array_final = array();
        foreach ($resultado as $r) {
            $this->soma_total($r, $myhashmap_limites);

            $r->total_acumulado = $r->soma_total + $r->valor_atual;
            $r->percentual_acumulado = (int) ( ( ( (float) ($r->total_acumulado) ) / 4800000 ) * 100);

            if($filtro == 'PROXIMAS_ACUMULADA'){
                if($r->percentual_acumulado >= 70 && $r->percentual_acumulado < 100){
                    array_push($array_final, $r);
                }
            }elseif($filtro == 'LIMITE_ACUMULADA'){
                if($r->percentual_acumulado >= 100){
                    array_push($array_final, $r);
                }
            }else{
                array_push($array_final, $r);
            }
        }

        $dados['razao_social_filtro'] = $razao;
        $dados['filtro'] = $filtro;

        $dados['consulta'] = $array_final;

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');        
        $this->load->view('layout/header', $dados);
        $this->load->view('simples/limite_simples', $dados);        
        $this->load->view('layout/footer');
        
    }


    public function soma_total($r, $myhashmap_limites){
        $socios = $this->socios_empresas_model->busca_socios($r->cnpj);
        $soma_total = 0;
        $r->soma_total = 0;

        $array_cpfs = array();
        $string_socios = "";
        if(!empty($socios)){

            foreach ($socios as $socio) {
                if(!empty($socio->nome_socio)){
                    if($string_socios == ""){
                        $string_socios = $socio->nome_socio." - ".$socio->cpf_cnpj;
                    }else{
                        $string_socios = $string_socios."<br>".$socio->nome_socio." - ".$socio->cpf_cnpj;
                    }
                    
                }

                array_push($array_cpfs, $socio->cpf_cnpj);
            }
            $socios_outras_empresas = $this->socios_empresas_model->busca_socios_outras_empresas($r->cnpj, $array_cpfs);
            
            foreach ($socios_outras_empresas as $s) {

                if(isset($myhashmap_limites[$s->cnpj_empresa])){
                    $a = $myhashmap_limites[$s->cnpj_empresa];
                    $soma_total = $soma_total + $a->valor_atual;
                    $r->soma_total = $soma_total;
                }
                
            }  
        }else{
          $string_socios = "QUADRO SOCIETÁRIO NÃO OBRIGATÓRIO POR SUA NATUREZA JURÍDICA";  
        }

        $r->socios_empresas_string = $string_socios;
        
    }


    public function abrirModalComposicao(){
        $cnpj = $this->input->post('cnpj');

        $this->limite_simples_model->setFiltro('TODAS');
        $resultado = $this->limite_simples_model->buscar();
        $myhashmap_limites = array();
        foreach ($resultado as $r) {
            $myhashmap_limites[$r->cnpj] = $r;
        }

        $socios = $this->socios_empresas_model->busca_socios($cnpj);
        $socios_outras_empresas = array();
        $array_cpfs = array();
        $soma_total = 0;

        if(!empty($socios)){
            foreach ($socios as $socio) {
                array_push($array_cpfs, $socio->cpf_cnpj);
            }

            $socios_outras_empresas = $this->socios_empresas_model->busca_socios_todas_empresas($array_cpfs);
            foreach ($socios_outras_empresas as $s) {
                if(isset($myhashmap_limites[$s->cnpj_empresa])){
                    $s->limite = "R$ ".number_format($myhashmap_limites[$s->cnpj_empresa]->valor_atual, 2, ',', '.');
                    $soma_total = $soma_total + $myhashmap_limites[$s->cnpj_empresa]->valor_atual;
                    $s->documento = $myhashmap_limites[$s->cnpj_empresa]->path;
                }else{
                    $s->limite = "R$ ".number_format(0, 2, ',', '.');
                    $soma_total = $soma_total + 0;
                }


            }
        }else{
            $empresa_atual = $this->socios_empresas_model->busca_socios_empresas_atual($cnpj);
            if(isset($myhashmap_limites[$empresa_atual->cnpj_empresa])){
                $empresa_atual->limite = "R$ ".number_format($myhashmap_limites[$empresa_atual->cnpj_empresa]->valor_atual, 2, ',', '.');
                $soma_total = $soma_total + $myhashmap_limites[$empresa_atual->cnpj_empresa]->valor_atual;
                $empresa_atual->documento = $myhashmap_limites[$empresa_atual->cnpj_empresa]->path;
            }else{
                $empresa_atual->limite = "R$ ".number_format(0, 2, ',', '.');
            }

            array_push($socios_outras_empresas, $empresa_atual);

            $objeto = new stdClass();
            $objeto->nome_socio = "QUADRO SOCIETÁRIO NÃO OBRIGATÓRIO POR SUA NATUREZA JURÍDICA";
            $objeto->cpf_cnpj = "";

            array_push($socios, $objeto);
        }

        $dados['socios'] = $socios;
        $dados['empresas'] = $socios_outras_empresas;
        $dados['total'] = "R$ ".number_format($soma_total, 2, ',', '.');

        echo json_encode($dados);
    }
}
