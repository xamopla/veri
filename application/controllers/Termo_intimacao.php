<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Termo_intimacao extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('termo_intimacao_model');
       	$this->load->model('ecac_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";
		$titulo = "Todas";

		$filtro = $cnpj;

		if($cnpj != "REGULARIZADO" && $cnpj != "ANALISANDO" && $cnpj != "TODAS" && $cnpj != "PENDENCIAS" && $cnpj != "CLIENTE"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "TODAS";
		}else{
			if($cnpj == "PENDENCIAS"){
				$titulo = "Pendências";
			}else if($cnpj == "REGULARIZADO"){
				$titulo = "Regulares";
			}else if($cnpj == "ANALISANDO"){
				$titulo = "Analisando";
			}

			$filtro = $cnpj;
		}

		$this->termo_intimacao_model->setFiltro($filtro);

		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		$dados['titulo'] = $titulo;

		$resultado = $this->termo_intimacao_model->listar();

		$resultado_final = array();

		foreach ($resultado as $r) {
			$possui_certificado = false;
			$certificado = $this->ecac_model->find_certificado($r->cnpj);
			if( $certificado ){
				$possui_certificado = true;
			}
			$r->possui_certificado = $possui_certificado;

			$ultimo_historico = $this->montaHistorico($r);
			if(empty($ultimo_historico)){
				$r->ultimo_historico = $ultimo_historico;
				$r->situacao = "PENDENCIAS";
			}else{
				$r->ultimo_historico = $ultimo_historico;
			}

			$lida_por = $this->termo_intimacao_model->buscar_lida_por($r->caixa_postal_id, $r->cnpj, $r->assunto, $r->data);
			if(isset($lida_por)){
				$r->lida_por = "Lida por: ".$lida_por->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($lida_por->data_alteracao));
			}
			
			if($filtro == "TODAS"){
				array_push($resultado_final, $r);
			}elseif($filtro == "REGULARIZADO"){
				if($r->situacao == "REGULARIZADO"){
					array_push($resultado_final, $r);
				}
			}elseif($filtro == "PENDENCIAS"){
				if($r->situacao == "PENDENCIAS"){
					array_push($resultado_final, $r);
				}
			}elseif($filtro == "ANALISANDO"){
				if($r->situacao == "ANALISANDO"){
					array_push($resultado_final, $r);
				}
			}elseif($filtro == "CLIENTE"){
				if($r->situacao == "CLIENTE"){
					array_push($resultado_final, $r);
				}
			}
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['filtro'] = $filtro;
	  	$dados['banco'] = $server;
	  	
		$dados['consulta'] = $resultado_final;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('intimacao/termo_intimacao', $dados);
		$this->load->view('layout/footer');
		
	}

	public function find_info_for_modal(){
		$id = $this->uri->segment(3);

		$resultado = $this->termo_intimacao_model->find_info_for_modal($id);

		$conteudo = $resultado->conteudo;
		$conteudo_format = str_replace("'", "", $conteudo);
        $conteudo_final = str_replace("&", "", $conteudo_format);

        $resultado->conteudo = $conteudo_final;

		echo json_encode($resultado);
	}


	public function buscar_mensagem(){
		$id = $this->input->post("id");

		$mensagem_ecac = $this->termo_intimacao_model->buscar_mensagem_ecac_by_id($id);
		
		echo json_encode($mensagem_ecac);
	}

	
	public function montaHistorico($r){
		$historico = $this->termo_intimacao_model->find_ultimo_historico($r);

		if(isset($historico)){
			$r->situacao = $historico->situacao;
			return "Alterado por: ".$historico->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($historico->data_alteracao));
		}else{
			return "";
		}

	}

	public function atualiza_situacao(){
		$id = $this->input->post("id_mensagem_ecac");
		$situacao = $this->input->post("situacao");
		$descricao = $this->input->post("descricao");

		$mensagem = $this->termo_intimacao_model->buscar_mensagem_ecac_by_id($id);

		$this->termo_intimacao_model->insere_historico($mensagem, $situacao, $descricao);

		redirect('termo_intimacao/listar');
	}


	public function buscar_historico(){
		$id = $this->input->post("id");
		$mensagem = $this->termo_intimacao_model->buscar_mensagem_ecac_by_id($id);

		$resultado = $this->termo_intimacao_model->buscar_historico($mensagem);

		foreach ($resultado as $r) {
			$r->data = date('d/m/Y H:i:s', strtotime($r->data_alteracao));
		}

		echo json_encode($resultado);

	}

}