<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function index()
	{
		
		if(isset($_POST['btn_login'])){
			$this->load->model("login_model", "login");
			$this->load->model("logo_model");

			$verificacao = 	$this->login->logar($this->input->post("login"), md5($this->input->post("senha")));
			if ($verificacao != 'invalido' && $verificacao != 'inativo' && $verificacao != 'expirado'){
				
				$this->session->set_userdata("userprimesession", $verificacao);

				//GRAVA O LOG DE ACESSO AO SISTEMA WEB
				$this->load->model('funcionario_model');
				$this->funcionario_model->setId($this->session->userdata['userprimesession']['id']);
				date_default_timezone_set('America/Bahia');
				$this->funcionario_model->setDataAcesso(date('Y-m-d H:i:s'));
				$this->funcionario_model->update_acesso();

				$login2 = $this->session->userdata['userprimesession']['email_sefaz'];
				$senha2 = $this->session->userdata['userprimesession']['senha_sefaz'];

				$data = array('param1'=>'modal');
				// store data to flashdata
				$this->session->set_flashdata('data',$data);
				
				if($login2 != null && $login2 != "" && $senha2 != null && $senha2 != ""){
					$data = array('param1'=>'modal');
					// store data to flashdata
					$this->session->set_flashdata('data',$data);
					
					//in other side
					$array = $this->session->flashdata('data');	
				}else{
					$data = array('param1'=>'false');
					// store data to flashdata
					$this->session->set_flashdata('data',$data);
					
					//in other side
					$array = $this->session->flashdata('data');
				}
				
				$logo = $this->logo_model->buscar_logo();
				$this->session->set_userdata("logo", base64_encode(file_get_contents($logo->caminho)));
				// base64_encode(file_get_contents('path/to/image.png'));
				
				if ($this->session->userdata['userprimesession']['nivel'] != 2){
					$this->session->set_userdata("lista_empresas_colaborador", array());
				redirect('painel/home', 'refresh');
				}
				else {
					$this->session->set_userdata("lista_empresas_colaborador", $this->empresas_colaborador());
				redirect('protocolo/listar', 'refresh');
				}

				
			} else if ($verificacao == 'invalido'){

				//TENTATIVA DE ACESSO
				$this->load->model('tentativa_acesso_model', 'l');
				$this->l->setNomeUsuario($this->input->post("login"));
				$this->l->setSenhaUsada($this->input->post("senha"));
				date_default_timezone_set('America/Bahia');
				$this->l->setDataHora(date('Y-m-d H:i:s'));
				function getUserIP()
				{
				    $client  = @$_SERVER['HTTP_CLIENT_IP'];
				    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
				    $remote  = $_SERVER['REMOTE_ADDR'];

				    if(filter_var($client, FILTER_VALIDATE_IP))
				    {
				        $ip = $client;
				    }
				    elseif(filter_var($forward, FILTER_VALIDATE_IP))
				    {
				        $ip = $forward;
				    }
				    else
				    {
				        $ip = $remote;
				    }

				    return $ip;
				}
				$ip_do_usuario = getUserIP();
				$this->l->setIp($ip_do_usuario);

				$result = $this->l->tentativa_de_logar();
				//FIM DO LOG DE TENTATIVA DE ACESSO
				
				$dados ['msg'] = 'invalido';
				$this->session->set_flashdata('invalido', 'Dados inválidos!');
				$this->load->view('index', $dados);
			} else if ($verificacao == 'inativo') {
				$dados ['msg'] = 'inativo';
				$this->session->set_flashdata('inativo', 'Seu cadastro está inativo!');
				$this->load->view('index', $dados);
			} else if ($verificacao == 'expirado') {
				$dados ['msg'] = 'expirado';
				$this->session->set_flashdata('expirado', 'O seu período de utilização expirou, por favor entre em contato.');
				$this->load->view('index', $dados);
			}
		}
		else {
			$dados ['msg'] = '';
			$this->load->view('index', $dados);
		}
		
	}
	
	public function logout()
	{
		$this->load->view('logout');
	}

	public function esqueceu_senha()
	{
		$this->load->view('password');
	}

	public function acesso_externo(){

		$this->load->model("login_model");
		$this->load->model("funcionario_model");

		$id = $this->uri->segment(3);
		$verificacao = 	$this->login_model->logar_externo($id);

		$this->session->set_userdata("userprimesession", $verificacao);

		//GRAVA O LOG DE ACESSO AO SISTEMA WEB
		$this->load->model('funcionario_model');
		$this->funcionario_model->setId($this->session->userdata['userprimesession']['id']);
		date_default_timezone_set('America/Bahia');
		$this->funcionario_model->setDataAcesso(date('Y-m-d H:i:s'));
		$this->funcionario_model->update_acesso();
		//FIM DA GRAVAÇÃO DO LOG DE ACESSO

		redirect('painel', 'refresh');

	}
}
