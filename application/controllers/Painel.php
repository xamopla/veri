<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends MY_Controller {
	
	public function index()
	{

		$dados = $this->notificacoes();
		
		$array = $this->session->flashdata('data');
		if(is_array($array)){
			$modal = $array['param1'];
			$dados['modal'] = 'true';	
		}else{
			$dados['modal'] = "false";
		}
		
		$this->load->model('empresa_model', 'e');

		$this->load->model('documento_model', 'd');

		$this->load->model('resumofiscal_model', 'rf');

		$this->load->model('email_model', 'g');

		$this->load->model('caixadeemail_model', 'f');

		$this->load->model('juceb_model', 'j');

		$this->load->model('verificador_senhas_model', 'v');
		
		$this->load->model('ipva_clientes_model'); 

		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

		$this->d->setIdContabilidade($this->session->userdata['userprimesession']['id']);

		$this->g->setIdContabilidade($this->session->userdata['userprimesession']['id']);

		$this->f->setIdContabilidade($this->session->userdata['userprimesession']['id']);

		$dados['qtd_empresas'] = $this->e->qtd_empresas();
		$dados['qtd_regulares'] = $this->e->qtd_regulares();
		
		$dados['qtd_intimadas'] = $this->e->qtd_empresas_intimadas();
		$dados['qtd_inaptas'] = $this->e->qtd_empresas_inaptas();	
		$dados['qtd_ativas'] = $this->e->qtd_ativas();
		$dados['qtd_nao_ativas'] = $this->e->qtd_nao_ativas();
		$dados['qtd_credenciadas'] = $this->e->qtd_credenciadas();
		$dados['qtd_descredenciadas'] = $this->e->qtd_descredenciadas();
		$dados['qtd_dte_ativo'] = $this->e->qtd_dte_ativo();
		$dados['qtd_contador_nao_vinculado'] = $this->e->qtd_contador_nao_vinculado();
		$dados['qtd_novos_emails'] = $this->f->qtd_novos_emails();
		$dados['qtd_documentos_vencidos'] = $this->d->qtd_documentos_vencidos();
		$dados['qtd_documentos_proxvenc'] = $this->d->qtd_documentos_proximosvencimento();
		$dados['qtd_documentos_regulares'] = $this->d->qtd_documentos_regulares();

		$dados['qtd_sem_senhas'] = $this->e->qtd_empresas_sem_senha_dte();

		$dados['qtd_senhas_erradas'] = $this->v->qtd_empresas_senhas_erradas();

		$dados['qtd_empresas_senhas_invalidas'] = $this->v->qtd_empresas_senhas_invalidas();
		
		$dados['qtd_socioirregular'] = $this->rf->dashboard1("qtd_socioirregular");
		$dados['qtd_omissodma'] = $this->rf->dashboard2("qtd_omissodma");
		$dados['qtd_omissoefd'] = $this->rf->dashboard3("qtd_omissoefd");

		$dados['qtd_parcatraso'] = $this->rf->dashboard4("qtd_parcatraso");
		$dados['qtd_div1'] = $this->rf->dashboard5("qtd_div1");
		$dados['qtd_div2'] = $this->rf->dashboard6("qtd_div2");
		$dados['qtd_div3'] = $this->rf->dashboard7("qtd_contador_nao_vinculado");
		$dados['qtd_osativa'] = $this->rf->dashboard8("qtd_novos_emails");
		$dados['qtd_osativa2'] = $this->rf->dashboard9("qtd_documentos_vencidos");
		$dados['pafsativos'] = $this->rf->dashboard10("qtd_pafs_ativos");

		$dados['qtd_juceb_prox_vencer'] = $this->j->qtd_eventos_proximos_a_vencer()->valor;
		$dados['qtd_eventos_juceb_vencidos'] = $this->j->qtd_eventos_juceb_vencidos()->valor;

		// $dados['qtd_juceb'] = $this->j->qtd_eventos_proximos_a_vencer()->valor + $this->j->qtd_eventos_juceb_vencidos()->valor;

		$dados['qtd_debito_ipva'] = $this->ipva_clientes_model->qtd_ipva();
		
		$consulta = $this->rf->dashboard11();
		$somaMsgNaoLidas = 0;
		foreach ($consulta as $value) {
			$valor = trim($this->soNumero($value->qtd));
			$somaMsgNaoLidas = $somaMsgNaoLidas + $valor;
		}
		
		$dados['msg_nao_lidas'] = $somaMsgNaoLidas; 

		
		// $this->load->view('layout/head');
		// $this->load->view('layout/sidebar');
		// $this->load->view('layout/header', $dados);
		// $this->load->view('layout/page', $dados);
		// $this->load->view('layout/footer');
		
	}

	public function soNumero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	}
	
	public function limparnotificacoes() {
		$this->load->model('empresa_model', 'e');
		
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		
		$result = $this->e->limparnotificacoes();
		
		echo json_encode($result);
	}

	public function limparnotificacoesdocumentos() {
		$this->load->model('documento_model', 'd');
		
		$this->d->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		
		$result = $this->d->limparnotificacoesdocumentos();
		
		echo json_encode($result);
	}

	public function limparnotificacoesresumofiscal() {
		$this->load->model('resumofiscal_model', 'rf');
		
		$result = $this->rf->limparnotificacoesresumofiscal();
		
		echo json_encode($result);
	}

	public function home(){
		$dados = $this->notificacoes();

		$this->load->model('certificadocontador_model'); 

		$certificado_contador = $this->certificadocontador_model->buscar_certificados_procuracao_vencidos();
		$ha_certificado_vencido = false;
		foreach ($certificado_contador as $c) {
			if(date('Ymd', $c->data_validade) < date('Ymd')){
				$ha_certificado_vencido = true;
			}
		}
		
		$dados['ha_certificado_vencido'] = $ha_certificado_vencido;

    	$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('home_principal/home', $dados);
		$this->load->view('layout/footer');
	}
}
