<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensagens extends MY_Controller {
	
	public function index(){
		$dados = $this->notificacoes();

		$this->load->model('ecac_model');
		$this->load->model('dec_model');

		$this->ecac_model->setFiltro_situacao("TODAS");
		$resultado = $this->ecac_model->listar_caixa_postal();
		
		$somaMsgNaoLidas = 0;
		$somaMsgLidas = 0;
		$qtdRegistros = 0;

		foreach ($resultado as $value) {
			$somaMsgNaoLidas = $somaMsgNaoLidas + $value->nao_lidas;
			$somaMsgLidas = $somaMsgLidas + $value->lidas;
			$qtdRegistros++;
		}

		$dados['consulta'] = $resultado;
		$dados['msg_nao_lidas'] = $somaMsgNaoLidas;
		$dados['msg_lidas'] = $somaMsgLidas;
		$dados['qtd_ecac_importante'] = $this->ecac_model->qtd_caixa_postal_importante()->qtd;

		// $this->dec_model->setFiltro("NAO_LIDAS");
		// $dados['msg_nao_lidas_dec'] = $this->dec_model->qtd()->qtd;

		// $this->dec_model->setFiltro("LIDAS");
		// $dados['msg_lidas_dec'] = $this->dec_model->qtd()->qtd;

		$dados['msg_nao_lidas_dec'] = $this->dec_model->qtd_mensagens_nao_lidas_dec()->total;
		
		$dados['id_colaborador'] = $this->session->userdata['userprimesession']['id'];
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('mensagens/dashboard', $dados);
		$this->load->view('layout/footer');
		
	}

	public function marcar_lida_por(){
		$this->load->model('mensagens_model');

		$id_caixa_postal = $this->input->post("id");
		$id_mensagem = $this->input->post("id_mensagem");

		$mensagem_ecac = $this->mensagens_model->buscar_mensagem_ecac($id_mensagem);

		$this->mensagens_model->insere_historico($mensagem_ecac);

		echo 1;
	}
}