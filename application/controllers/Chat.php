<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Chat extends MY_Controller {

	public function __construct() {
        parent::__construct();   
        $this->load->model('chat_model');  
        $this->load->model('usuario_model');      
    }

	public function index(){

		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		
    	$colaboradores = $this->usuario_model->buscar_colaboradores_chat();
    	$diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
    	$data_atual = date("Y-m-d");

    	$primeiro_colaborador = "";
    	foreach ($colaboradores as $colaborador) {
    		if(!empty($colaborador->nome)){
    			if(strlen($colaborador->nome) > 14){
    				$colaborador->nome = substr($colaborador->nome,0, 14)."...";
    			}
    		}else{
    			$colaborador->nome = $colaborador->razao_social;
    		}
    		

    		$numero_msg = $this->chat_model->numero_msg_nao_lidas($colaborador->id);
    		if(isset($numero_msg)){
    			$numero_msg = $numero_msg->qtd;
    		}else{
    			$numero_msg = 0;
    		}
    		$colaborador->numero_msg = $numero_msg;
    		$result = $this->chat_model->lista_ultima_mensagem($colaborador->id);
    		if(isset($result)){
    			$diferenca = strtotime($data_atual) - strtotime($result->data_only);
				$diff_dias = floor($diferenca / (60 * 60 * 24));
    			
    			if($diff_dias == 0){
    				$colaborador->data = $result->hora;
    			}else if($diff_dias <= 7){
    				$diasemana_numero = date('w', strtotime($result->data_only));
    				$colaborador->data = $diasemana[$diasemana_numero];
    			}else{
    				$colaborador->data = $result->data_only;
    			}

    			if(strlen($result->mensagem) > 14){
    				$colaborador->ultima_mensagem = substr($result->mensagem,0, 14).'...';
    			}else{
    				$colaborador->ultima_mensagem = $result->mensagem;
    			}
    			
    		}else{
    			$colaborador->ultima_mensagem = "";
    			$colaborador->data = "";
    			$colaborador->hora = "";	
    		}
    		
    		if($primeiro_colaborador == ""){
    			$primeiro_colaborador = $colaborador;
    		}

    		if($colaborador->foto_perfil == NULL){
				$colaborador->foto_perfil = 'assets/img/sistema/user.png';
			}
    	}

    	$dados['usuarios'] = $colaboradores;
    	$dados['primeiro_colaborador'] = $primeiro_colaborador;
    	$dados['id_usuario_logado'] = $this->session->userdata['userprimesession']['id'];

    	$foto_perfil = $this->session->userdata['userprimesession']['logo'];
    	if($foto_perfil != NULL){
    		$dados['foto_usuario_logado'] = $foto_perfil;
    	}else{
			$dados['foto_usuario_logado'] = 'assets/img/sistema/user.png';
    	}
    	

		$this->load->view('layout/head_chat');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('chat/chat', $dados);
		$this->load->view('layout/footer');
		
	}

	public function buscar_conversa(){
		$id_usuario = $this->input->post('id_usuario');
		$usuario_logado = $this->usuario_model->pesquisar_foto_chat($this->session->userdata['userprimesession']['id']);
		$usuario_conversa = $this->usuario_model->pesquisar_foto_chat($id_usuario);

		$resultado = $this->chat_model->buscar_conversa($id_usuario);
		foreach ($resultado as $r) {
			$r->hora = substr($r->hora, 0, -3);
			if($r->lida == 1){
				$r->cor = "#37c8ff";
			}else{
				$r->cor = "black";
			}
			if($r->id_envia == $this->session->userdata['userprimesession']['id']){
				$r->id_envia = 'SIM';
				$r->id_recebe = 'NAO';
			}else{
				$r->id_envia = 'NAO';
				$r->id_recebe = 'SIM';
			}
			if($usuario_logado->foto_perfil != NULL){
				$r->foto_usuario = $usuario_logado->foto_perfil;
			}else{
				$r->foto_usuario = 'assets/img/sistema/user.png';
			}
			
			if($usuario_conversa->foto_perfil != NULL){
				$r->foto_conversa = $usuario_conversa->foto_perfil;
			}else{
				$r->foto_conversa = 'assets/img/sistema/user.png';
			}
			
			if(!empty($usuario_conversa->user_name)){
				$r->nome_conversa = $usuario_conversa->user_name;
			}else{
				$r->nome_conversa = $usuario_conversa->razao_social;
			}

			$data_acesso = $usuario_conversa->data_acesso;
			$r->status = "Online";
			// $data_logout = $usuario_conversa->data_logout;

			// if($data_logout == null){
			// 	$r->status = "Online";
			// }else{
			// 	if(strtotime($data_logout) > strtotime($data_acesso)){
			// 		$r->status = "Offline";
			// 	}else{
			// 		$r->status = "Online";
			// 	}
			// }
			
			//break;
		}
		echo json_encode($resultado);
	}

	public function cadastrarMensagem(){
		$id_recebe = $this->input->post('id_usuario');
		$mensagem = $this->input->post('mensagem');
		$id_envia = $this->session->userdata['userprimesession']['id'];

		$this->chat_model->setId_recebe($id_recebe);
		$this->chat_model->setId_envia($id_envia);
		$this->chat_model->setMensagem($mensagem);

		$resultado = $this->chat_model->cadastrarMensagem();
		echo json_encode($resultado);
	}

	public function marcar_como_lida(){
		$id_envia = $this->input->post('id_usuario');
		$id_recebe = $this->session->userdata['userprimesession']['id'];

		$this->chat_model->setId_recebe($id_recebe);
		$this->chat_model->setId_envia($id_envia);

		$resultado = $this->chat_model->marcar_como_lida();
		echo json_encode($resultado);
	}

	public function monitora_conversa_atual(){
		$id_envia = $this->input->post('id_usuario');
		$id_recebe = $this->session->userdata['userprimesession']['id'];

		$this->chat_model->setId_recebe($id_recebe);
		$this->chat_model->setId_envia($id_envia);

		$resultado = $this->chat_model->monitora_conversa_atual();
		foreach ($resultado as $r) {
			$this->chat_model->marcar_como_lida_msg($r->id);
			$this->chat_model->update_notificacao($r->id);
		}

		echo json_encode($resultado);
	}

	public function monitora_outras_conversas(){
		$id_envia = $this->input->post('id_usuario');
		$id_recebe = $this->session->userdata['userprimesession']['id'];

		$this->chat_model->setId_recebe($id_recebe);
		$this->chat_model->setId_envia($id_envia);

		$resultado = $this->chat_model->monitora_outras_conversas();
		foreach ($resultado as $r) {
			$r->id_usuario_bagde = $r->id_envia;
			$mensagem = $this->chat_model->lista_ultima_mensagem_outros_chat($r->max);
			if(isset($mensagem)){
				if(strlen($mensagem->mensagem) > 14){
					$r->mensagem = substr($mensagem->mensagem,0, 14).'...';
				}else{
					$r->mensagem = $mensagem->mensagem;
				}
				
				$r->data = $mensagem->hora;

			}else{
				$r->mensagem = "";
			}

			$r->notificacao = $mensagem->notificacao;
			$this->chat_model->update_notificacao($r->max);

		}

		echo json_encode($resultado);
	}

	public function find_usuario(){
		$id_usuario = $this->input->post('id_usuario');
		$usuario_conversa = $this->usuario_model->pesquisar_foto_chat($id_usuario);

		if($usuario_conversa->foto_perfil != NULL){
			$usuario_conversa->foto_conversa = $usuario_conversa->foto_perfil;
		}else{
			$usuario_conversa->foto_conversa = 'assets/img/sistema/user.png';
		}

		$data_acesso = $usuario_conversa->data_acesso;
		$data_logout = $usuario_conversa->data_logout;

		if($data_logout == null){
			$usuario_conversa->status = "Online";
		}else{
			if(strtotime($data_logout) > strtotime($data_acesso)){
				$usuario_conversa->status = "Offline";
			}else{
				$usuario_conversa->status = "Online";
			}
		}

		echo json_encode($usuario_conversa);
	}

	public function verifica_ja_lida(){
		$id_recebe = $this->input->post('id_usuario');
		$id_envia = $this->session->userdata['userprimesession']['id'];

		$this->chat_model->setId_recebe($id_recebe);
		$this->chat_model->setId_envia($id_envia);

		$qtd = 0;
		$resultado = $this->chat_model->verifica_ja_lida();
		if(isset($resultado)){
			$qtd = $resultado->qtd;
		}
		echo json_encode($qtd);
	}


	public function monitora_outras_conversas_head(){
		$id_envia = $this->session->userdata['userprimesession']['id'];
		$id_recebe = $this->session->userdata['userprimesession']['id'];

		$this->chat_model->setId_recebe($id_recebe);
		$this->chat_model->setId_envia($id_envia);

		$resultado = $this->chat_model->monitora_outras_conversas();
		foreach ($resultado as $r) {
			$r->id_usuario_bagde = $r->id_envia;
			$mensagem = $this->chat_model->lista_ultima_mensagem_outros_chat($r->max);
			if(isset($mensagem)){
				if(strlen($mensagem->mensagem) > 14){
					$r->mensagem = substr($mensagem->mensagem,0, 14).'...';
				}else{
					$r->mensagem = $mensagem->mensagem;
				}
				
				$r->data = $mensagem->hora;

			}else{
				$r->mensagem = "";
			}

			$r->notificacao = $mensagem->notificacao;
			$this->chat_model->update_notificacao($r->max);

		}

		echo json_encode($resultado);
		
	}

}