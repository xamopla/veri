<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Protocolo extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('consulta_sipro_model');
		$this->load->model('protocolo_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$dados['filtro'] = $this->uri->segment(2);
		
		//$dados['consulta'] = $this->protocolo_model->listar();
		if ($this->session->userdata['userprimesession']['nivel'] == 1) {
			$dados['nomeusuario'] = $this->session->userdata['userprimesession']['login'];
		} else {
		$dados['nomeusuario'] = $this->session->userdata['userprimesession']['nome'];
		}
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('protocolo/protocolo_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function listarAjax(){
		$filtro = $_POST["filtros"];
		$this->protocolo_model->setFiltro($filtro);

		$protocolos = $this->protocolo_model->listar();
		echo json_encode($protocolos);
	}

	public function salvarNewPopUp(){
		$usuario = $this->input->post('usuario');
		$data = $this->input->post('data');
		$isnotificaoresumo = $this->input->post('isnotificaoresumo');
		$idnotificao = $this->input->post('idnotificacao');
		$descricao = $this->input->post('mensagem');

		$this->protocolo_model->setUsuario($usuario);
		$this->protocolo_model->setData($data);
		$this->protocolo_model->setDescricao($descricao);

		if($isnotificaoresumo == 1){
			$this->protocolo_model->setIdnotificacaoresumo($idnotificao);
		}elseif ($isnotificaoresumo == 2) {
			$this->protocolo_model->setIdnotificacaoprocessosipro($idnotificao);
		}else {
			$this->protocolo_model->setIdnotificacao($idnotificao);
		}
		
		$this->protocolo_model->salvar();

		if($isnotificaoresumo == 1){
			$this->resumofiscal_model->lerMensagem($idnotificao);
		}elseif ($isnotificaoresumo == 2) {
			$this->consulta_sipro_model->lerMensagem($idnotificao);
		}else{
			$this->empresa_model->lerMensagem($idnotificao);
		}
		
		redirect("protocolo/listar");
	}

	public function listarResumoFiscalEmpresa() {

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$id = $this->uri->segment(3); 
		
		$dados['consulta'] = $this->resumofiscal_model->listar_resumo_fiscal_por_empresa($id);
		
		$this->load->view('layout/head');		
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/resumo_fiscal', $dados);
		$this->load->view('layout/footer');
	}

	public function ler_mensagem_situacao(){
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		//$usuario = $this->session->userdata['userprimesession']['nome'];
		date_default_timezone_set('America/Bahia');
		$data = date('d/m/Y H:i:s');

		$idnotificao = $this->input->post('id_notificacao');
		$descricao = 'O usuário '.$usuario.' deu ciência da notificação';

		$this->protocolo_model->setUsuario($usuario);
		$this->protocolo_model->setData($data);
		$this->protocolo_model->setDescricao($descricao);
		$this->protocolo_model->setIdnotificacao($idnotificao);

		$this->protocolo_model->salvar();

		$this->empresa_model->lerMensagem($idnotificao);

		echo 1;
	}

	public function ler_mensagem_resumo(){
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		//$usuario = $this->session->userdata['userprimesession']['nome'];
		date_default_timezone_set('America/Bahia');
		$data = date('d/m/Y H:i:s');

		$idnotificao = $this->input->post('id_notificacao');
		$descricao = 'O usuário '.$usuario.' deu ciência da notificação';

		$this->protocolo_model->setUsuario($usuario);
		$this->protocolo_model->setData($data);
		$this->protocolo_model->setDescricao($descricao);
		$this->protocolo_model->setIdnotificacaoresumo($idnotificao);

		$this->protocolo_model->salvar();

		$this->resumofiscal_model->lerMensagem($idnotificao);

		echo 1;
	}

	public function ler_mensagem_sipro(){
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		//$usuario = $this->session->userdata['userprimesession']['nome'];
		date_default_timezone_set('America/Bahia');
		$data = date('d/m/Y H:i:s');

		$idnotificao = $this->input->post('id_notificacao');
		$descricao = 'O usuário '.$usuario.' deu ciência da notificação';

		$this->protocolo_model->setUsuario($usuario);
		$this->protocolo_model->setData($data);
		$this->protocolo_model->setDescricao($descricao);
		$this->protocolo_model->setIdnotificacaoprocessosipro($idnotificao);

		$this->protocolo_model->salvar();

		$this->consulta_sipro_model->lerMensagem($idnotificao);

		echo 1;
	}
}