<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel2 extends MY_Controller {
	
	public function index(){

		$dados = $this->notificacoes();
		
		$this->load->model('empresa_model', 'e');

		$this->load->model('documento_model', 'd');

		$this->load->model('resumofiscal_model', 'rf');
		
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);

		$this->d->setIdContabilidade($this->session->userdata['userprimesession']['id']);

		$dados['grf_1'] = $this->e->grf_situacao_cadastral();
		$dados['grf_2'] = $this->e->grf_situacao();
		$dados['grf_3'] = $this->e->grf_conta_dte();
		$dados['grf_4'] = $this->e->grf_situacao_dte();
		$dados['grf_5'] = $this->e->grf_natureza_juridica();
		$dados['grf_6'] = $this->e->grf_condicao_empresa();
		$dados['grf_7'] = $this->e->grf_forma_pagamento();
		$dados['grf_8'] = $this->rf->grf_beneficiario_decreto();

		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('layout/page2', $dados);
		$this->load->view('layout/footer');
		
	}

	public function limparnotificacoes() {
		$this->load->model('empresa_model', 'e');
		
		$this->e->setIdContador($this->session->userdata['userprimesession']['id']);
		
		$result = $this->e->limparnotificacoes();
		
		echo json_encode($result);
	}

	public function limparnotificacoesdocumentos() {
		$this->load->model('documento_model', 'd');
		
		$this->d->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		
		$result = $this->d->limparnotificacoesdocumentos();
		
		echo json_encode($result);
	}

	public function limparnotificacoesresumofiscal() {
		$this->load->model('resumofiscal_model', 'rf');
		
		$result = $this->rf->limparnotificacoesresumofiscal();
		
		echo json_encode($result);
	}
}
