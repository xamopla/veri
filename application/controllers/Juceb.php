<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Juceb extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('juceb_model');
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function cadastrar(){
		
		if(isset($_POST['btn_localizar'])){
			
			$prt = $this->input->post('protocolo');
			
			if ($prt != ''){
				
				$this->load->model('juceb_model', 'e');
				
				if ($prt){
					
					$prt = preg_replace("/[^0-9]/", "", $prt);
					
					$this->e->setProtocolo($prt);
					$this->e->setIdContabilidade($this->session->userdata['userprimesession']['id']);
					$verificacao = $this->e->verificar_cadastro_duplicado('prt');
					
					if ($verificacao){
						
						//msg que já está cadastrado					
						$this->session->set_flashdata('msg_titulo','ERRO!');
						$this->session->set_flashdata('msg_conteudo','Protocolo já cadastrado.');
						$this->session->set_flashdata('msg_tipo','danger');
						
					} else {
						redirect("juceb/finalizarcadastro/prt/$prt");
					}
					
				} else {				
				$this->session->set_flashdata('msg_titulo','ATENÇÃO!');
				$this->session->set_flashdata('msg_conteudo','Por favor informe um dos campos.');
				$this->session->set_flashdata('msg_tipo','warning');
			}
		
		}
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header');
		$this->load->view('juceb/juceb_cadastrar');
		$this->load->view('layout/footer');
	
	}
 }

 public function finalizarcadastro(){
		
		$tipo = $this->uri->segment(3);
		$valor = $this->uri->segment(4);
		
		if ($tipo != '' && $valor != ''){
			
			$this->load->model('juceb_model', 'e');

			if(isset($_POST['btn_cadastrar'])){
				
				$this->e->setProtocolo($this->validar_input($this->input->post("protocolo")));
				$this->e->setNomeDaEmpresa($this->validar_input($this->input->post("nome_da_empresa")));
				$this->e->setEstadoDoProcesso($this->validar_input($this->input->post("estado_do_processo")));
				$this->e->setStatusDoEnvio($this->validar_input($this->input->post("status_de_envio")));
				$this->e->setDataAtualizacao($this->validar_input($this->input->post("data_atualizacao")));
				$this->e->setDataIncorporacao($this->validar_input($this->input->post("data_incorporacao")));
				
				$this->e->setIdContabilidade($this->session->userdata['userprimesession']['id']);
				
				$result = $this->e->cadastrar();
					
				if ($result){
					$this->session->set_flashdata('msg_titulo','SUCESSO!');
					$this->session->set_flashdata('msg_conteudo','O cadastro foi efetuado.');
					$this->session->set_flashdata('msg_tipo','success');
					
					redirect("juceb/listar");
				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
					$this->session->set_flashdata('msg_tipo','danger');
					
					redirect("juceb/cadastrar");
				}
				
			}  else {
					redirect("juceb/cadastrar");
				}
			}
			
			$this->load->view('layout/head');
			$this->load->view('layout/sidebar');
			$this->load->view('layout/header');
			$this->load->view('juceb/juceb_finalizar_cadastro', $dados);
			$this->load->view('layout/footer');
			
		} 

		public function listar(){
		
		$this->load->model('juceb_model');
		
		$this->juceb_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->juceb_model->listar();
		
		$this->load->view('layout/head');	
		$this->load->view('layout/sidebar');	
		$this->load->view('layout/header');
		$this->load->view('juceb/juceb_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	// -------------------------------------------- JUCEB DADOS -------------------------------------------------

	public function listar_dados(){		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->listar_dados_juceb();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_dados', $dados);
		$this->load->view('layout/footer');
	}

	public function registrosAtivos(){		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->filtro_registro_ativo();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_dados', $dados);
		$this->load->view('layout/footer');
	}

	public function registrosCancelados(){		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->filtro_registro_cancelado();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_dados', $dados);
		$this->load->view('layout/footer');
	}

	public function registrosExtintos(){		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->filtro_registro_extinto();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_dados', $dados);
		$this->load->view('layout/footer');
	}

	public function registrosProxVencimento(){		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->listar_dados_juceb_prox_vencimento();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_dados', $dados);
		$this->load->view('layout/footer');
	}

	public function registrosVencidos(){		

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->listar_dados_juceb_vencidos();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_dados', $dados);
		$this->load->view('layout/footer');
	}

	// -------------------ACOMPANHAMENTO DE VIABILIDADE E LEGALIZAÇÃO--------------------------------------



	public function after ($a, $inthat)
	{
	    if (!is_bool(strpos($inthat, $a)))
	    return substr($inthat, strpos($inthat,$a)+strlen($a));
	}

	public function cadastrar_protocolo_acompanhamento(){

		if(isset($_POST['btn_cadastrar'])){

			$protocolo = $this->input->post("protocolo");

			$verificacao = $this->juceb_model->verificar_cadastro_duplicado($protocolo);

			if ($verificacao){

				$this->session->set_flashdata('msg_alerta', 12);			
				redirect("juceb/listar_acompanhamento_viabilidade");

			} else {

			$html = file('http://regin.juceb.ba.gov.br/regin.ba/CON_DadosIdentificacion.aspx?id='.$protocolo.'&pTipo=3&pTipoTela=CONSULTA');

			if (!empty($html)){   

			// COLETANDO O NUMERO DO PROTOCOLO
			$numero_do_protocolo_inicio = trim(html_entity_decode($html[177])); 

			$numero_do_protocolo_aux = strstr($numero_do_protocolo_inicio, 'value=');

			$numero_do_protocolo_fim = strstr($numero_do_protocolo_aux, 'size=', true);

			$numero_do_protocolo = $this->after('value=', $numero_do_protocolo_fim);

			// COLETANDO O NOME DA EMPRESA E REMOVENDO POSIÇÕES
			$nome_da_empresa_inicio = trim(html_entity_decode($html[184])); 

			$nome_da_empresa_aux = strstr($nome_da_empresa_inicio, 'value=');

			$nome_da_empresa_fim = strstr($nome_da_empresa_aux, 'size=', true);

			$nome_da_empresa = $this->after('value=', $nome_da_empresa_fim);

			//COLETANDO O ESTADO DO PROCESSO E REMOVENDO POSIÇÕES
			$estado_do_processo_inicio = trim(html_entity_decode($html[190])); 

			$estado_do_processo_aux = strstr($estado_do_processo_inicio, 'value=');

			$estado_do_processo_fim = strstr($estado_do_processo_aux, 'size=', true);

			$estado_do_processo = $this->after('value=', $estado_do_processo_fim);

			//COLETANDO O STATUS DE ENVIO RFB 
			$status_envio_rfb = trim(html_entity_decode(strip_tags($html[195])));

			// COLETANDO DATA DE ATUALIZAÇÃO
			$data_de_atualizacao_inicio = trim(html_entity_decode($html[201])); 

			$data_de_atualizacao_aux = strstr($data_de_atualizacao_inicio, 'value=');

			$data_de_atualizacao_fim = strstr($data_de_atualizacao_aux, 'size=', true);

			$data_de_atualizacao = $this->after('value=', $data_de_atualizacao_fim);

			// COLETANDO DATA DE INCORPORAÇÃO DO PROTOCOLO
			$data_de_incoporacao_inicio = trim(html_entity_decode($html[205])); 

			$data_de_incoporacao_aux = strstr($data_de_incoporacao_inicio, 'value=');

			$data_de_incoporacao_fim = strstr($data_de_incoporacao_aux, 'size=', true);

			$data_de_incoporacao = $this->after('value=', $data_de_incoporacao_fim);

			$numero_do_protocolo = str_replace('"','',$numero_do_protocolo); 
			$nome_da_empresa = str_replace('"','',$nome_da_empresa);
			$estado_do_processo = str_replace('"','',$estado_do_processo);
			$status_envio_rfb = str_replace('"','',$status_envio_rfb);
			$data_de_atualizacao = str_replace('"','',$data_de_atualizacao);
			$data_de_incoporacao = str_replace('"','',$data_de_incoporacao);

			if ($numero_do_protocolo == "") {
				$this->session->set_flashdata('msg_alerta', 10);			
				redirect("juceb/listar_acompanhamento_viabilidade");
			} else {

			$this->juceb_model->setProtocolo($this->input->post("protocolo")); 
			$this->juceb_model->setNomeDaEmpresa($nome_da_empresa); 
			$this->juceb_model->setEstadoDoProcesso($estado_do_processo); 
			$this->juceb_model->setStatusDoEnvio($status_envio_rfb); 
			$this->juceb_model->setDataAtualizacao($data_de_atualizacao); 
			$this->juceb_model->setDataIncorporacao($data_de_incoporacao); 
			date_default_timezone_set('America/Bahia'); 
			$this->juceb_model->setDataCadastro(date('Y-m-d H:i:s'));
			$this->juceb_model->setIdCadastro($this->session->userdata['userprimesession']['id']);

			$result = $this->juceb_model->cadastrar_protocolo();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 1);
				redirect("juceb/listar_acompanhamento_viabilidade");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 2);			
				redirect("juceb/listar_acompanhamento_viabilidade");
				
			}
			}

			} else {

		 		$this->session->set_flashdata('msg_alerta', 10);			
				redirect("juceb/listar_acompanhamento_viabilidade");
			}  
		}

		}
	}

	public function editar_protocolo_acompanhamento(){

		$id = $this->input->post("id");

        $this->juceb_model->setId($id);

		if(isset($_POST['btn_salvar'])){

			$this->juceb_model->setNomeEmpresaMostrar($this->input->post("nome_empresa_mostrar"));

			$result = $this->juceb_model->editar_protocolo();

			if ($result){
				
				$this->session->set_flashdata('msg_alerta', 3);
				redirect("juceb/listar_acompanhamento_viabilidade");
				
			} else {
				
				$this->session->set_flashdata('msg_alerta', 4);			
				redirect("juceb/listar_acompanhamento_viabilidade");
				
			}

		}
	}

	public function listar_acompanhamento_viabilidade(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->listar_acompanhamento_viabilidade();
		$dados['valor_editar'] = $this->juceb_model->pesquisar_acompanhamento_viabilidade_id();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_acompanhamento_viabilidade', $dados);
		$this->load->view('layout/footer');
	}

	public function consultaproabertos(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->listar_acompanhamento_viabilidade_abertos();
		$dados['valor_editar'] = $this->juceb_model->pesquisar_acompanhamento_viabilidade_id();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_acompanhamento_viabilidade', $dados);
		$this->load->view('layout/footer');
	}

	public function consultaprofinalizados(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->juceb_model->listar_acompanhamento_viabilidade_finalizados();
		$dados['valor_editar'] = $this->juceb_model->pesquisar_acompanhamento_viabilidade_id();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('juceb/juceb_listar_acompanhamento_viabilidade', $dados);
		$this->load->view('layout/footer');
	}

	public function remover_protocolo(){

		$id = $this->uri->segment(3);
		$this->juceb_model->setId($id);

		$result = $this->juceb_model->excluir_protocolo();

		if ($result){

			$this->session->set_flashdata('msg_alerta', 5);	
			redirect('juceb/listar_acompanhamento_viabilidade');

		} else {
			
			$this->session->set_flashdata('msg_alerta', 6);		
			redirect('juceb/listar_acompanhamento_viabilidade');

		}	
	}

}
