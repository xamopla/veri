<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parcelamento_nao_previdenciario extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('parcelamento_nao_previdenciario_model');
       	$this->load->model('empresa_model');
    }

	public function listar(){
		date_default_timezone_set('America/Sao_Paulo');

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
        $razao = "";
        $filtro = "";

		if($cnpj != "PARCELADO" && $cnpj != "TODAS"){
            if($cnpj != null){
                $em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
                $razao = $em->razao_social;
            }
            $filtro = "TODAS";
        }else{
            $filtro = $cnpj;
        }
        
        $this->parcelamento_nao_previdenciario_model->setFiltro("TODAS");

        $dados['razao_social_filtro'] = $razao;
        $dados['filtro'] = $filtro;

        $dados['processos_negociados'] = $this->parcelamento_nao_previdenciario_model->listar_processos();

		$data_atual = date('d-m-Y');
		$parcelas_nao_pagas = array();

		
		foreach ($dados['processos_negociados'] as $processo) {
			foreach ($this->parcelamento_nao_previdenciario_model->listar_tributos_negociados($processo->id) as $tributo_negociado) {
				foreach ($this->parcelamento_nao_previdenciario_model->listar_demonstrativo_das_parcelas($tributo_negociado->id) as $parcela) {
					//verifica as parcelas em atraso
					if(strtotime(str_replace('/', '-', $parcela->data_vencimento)) < strtotime($data_atual) && $parcela->situacao != 'Paga'){
						$parcela->nome_tributo = $tributo_negociado->tributo;
						$parcelas_nao_pagas[] = $parcela;
					}
					//pega data de vencimento de cada tributo do mes corrente
					if (str_replace('/', '-', substr($parcela->data_vencimento, 3 )) == date('m-Y')) {
						$parcelas_datas_vencimento[] = $parcela->data_vencimento.' ('.$tributo_negociado->tributo.')<br>';
					}
				}
			}
			$processo->parcelas_datas_vencimento = $parcelas_datas_vencimento;
			$processo->parcelas_nao_pagas = $parcelas_nao_pagas;
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;

		$dados['qtd_parcelas_em_aberto'] = $this->parcelamento_nao_previdenciario_model->get_qtd_parcelas_em_aberto()->qtd;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('parcelamento/parcelamento_nao_previdenciario', $dados);
		$this->load->view('layout/footer');
		
	}

	public function listar_detalhes(){
		$id = $this->uri->segment(3);

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		
		$dados['processo'] = $this->parcelamento_nao_previdenciario_model->listar_processo_individual($id);
		
		$tributos_negociado = $this->parcelamento_nao_previdenciario_model->listar_tributos_negociados($dados['processo']->id);
		
		foreach ($tributos_negociado as $tributo) {
			$demonstrativo_das_parcelas = $this->parcelamento_nao_previdenciario_model->listar_demonstrativo_das_parcelas($tributo->id);
			$tributo->demonstrativo_das_parcelas = $demonstrativo_das_parcelas;
		}

		$dados['tributos_negociado'] = $tributos_negociado;

		//listar_demonstrativo_das_parcelas($id_tributo);
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('parcelamento/parcelamento_nao_previdenciario_detalhes', $dados);
		$this->load->view('layout/footer');
		
	}

	public function buscar_processos_negociados_for_modal(){
		$cnpj = $this->input->post('cnpj');
		
		$resultado_final = $this->parcelamento_nao_previdenciario_model->listar_processos_negociados_for_modal($cnpj);

        echo json_encode($resultado_final);
	}

	public function buscar_demonstrativo_das_parcelas_for_modal(){
		$cnpj = $this->input->post('cnpj');
		$id_tributo = $this->input->post('id_tributo');

		$resultado_final = $this->parcelamento_nao_previdenciario_model->listar_demonstrativo_das_parcelas_for_modal($cnpj, $id_tributo);

        echo json_encode($resultado_final);
	}

	public function buscar_parcelas_pagas_for_modal(){
		$cnpj = $this->input->post('cnpj');

		$resultado_final = $this->parcelamento_nao_previdenciario_model->listar_parcelas_pagas_for_modal($cnpj);

        echo json_encode($resultado_final);
	}

	public function buscar_parcelas_nao_pagas_for_modal(){
		$id = $this->input->post('id');

		date_default_timezone_set('America/Sao_Paulo');
		$data_atual = date('d-m-Y');


		$dados['processo'] = $this->parcelamento_nao_previdenciario_model->listar_processo_individual($id);

		$parcelas_nao_pagas = array();
		foreach ($this->parcelamento_nao_previdenciario_model->listar_tributos_negociados($id) as $tributo_negociado) {
			foreach ($this->parcelamento_nao_previdenciario_model->listar_demonstrativo_das_parcelas($tributo_negociado->id) as $parcela) {
				if(strtotime(str_replace('/', '-', $parcela->data_vencimento)) < strtotime($data_atual) && $parcela->situacao != 'Paga'){
					$parcela->nome_tributo = $tributo_negociado->tributo;
					$parcela->parcelas_em_atraso = $tributo_negociado->parcelas_em_atraso;
					$parcelas_nao_pagas[] = $parcela;
				}
			}
		}
		$dados['parcelas_nao_pagas'] = $parcelas_nao_pagas;

        echo json_encode($dados);
	}
}