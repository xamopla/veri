<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consulta_nfe extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
    }

    public function consultar_emitidas_nfe(){

    	$id_empresa = $this->uri->segment(3);

		$dados['consulta'] = $this->empresa_model->consultar_login_e_senha_dte($id_empresa);
        
		$this->load->view('layout/head');
		$this->load->view('nfe/consultar_emitidas', $dados);
    }

    public function consultar_emitidas_nfe_alternativo(){

    	$id_empresa = $this->uri->segment(3);

		$dados['consulta'] = $this->empresa_model->consultar_login_e_senha_dte($id_empresa);

		$this->load->view('layout/head');
		$this->load->view('nfe/consultar_emitidas_alternativo', $dados);
    }

    public function consultar_adquiridas_nfe(){

    	$id_empresa = $this->uri->segment(3);

		$dados['consulta'] = $this->empresa_model->consultar_login_e_senha_dte($id_empresa);

		$this->load->view('layout/head');
		$this->load->view('nfe/consultar_adquiridas', $dados);
    }

    public function consultar_adquiridas_nfe_alternativo(){

    	$id_empresa = $this->uri->segment(3);

		$dados['consulta'] = $this->empresa_model->consultar_login_e_senha_dte($id_empresa);

		$this->load->view('layout/head');
		$this->load->view('nfe/consultar_adquiridas_alternativo', $dados);
    }

}