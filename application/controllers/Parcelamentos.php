<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parcelamentos extends MY_Controller {
	
	public function dashboard()
	{

		$dados = $this->notificacoes();

		$this->load->model('resumofiscal_model', 'rf');
		$this->load->model('ecac_model');
		$this->load->model('parcelamento_das_model');
		$this->load->model('parcelamento_pert_model');
		$this->load->model('parcelamento_nao_previdenciario_model');
		$this->load->model('parcelamento_lei_12996_model'); 
		$this->load->model('parcelamento_pert_rfb_model');
		$this->load->model('parcelamento_mei_model');
		
		$dados['qtd_parcatraso'] = $this->rf->dashboard4("qtd_parcatraso");
	
		//INICIO PARCELAMENTOS DAS
		$this->parcelamento_das_model->setFiltro("TODAS");
		$dados['parcelamento_das_qtd'] = $this->parcelamento_das_model->get_qtd_parcelamentos()->qtd;

		$this->parcelamento_das_model->setFiltro("EM_PARCELAMENTO");
		$dados['parcelamento_das_qtd_em_parcelamento'] = $this->parcelamento_das_model->get_qtd_parcelamentos()->qtd;
		
		$this->parcelamento_das_model->setFiltro("ENCERRADO_RECISAO");
		$dados['parcelamento_das_qtd_encerrado_recisao'] = $this->parcelamento_das_model->get_qtd_parcelamentos()->qtd;

		$this->parcelamento_das_model->setFiltro("ENCERRADO_CONTRIBUINTE");
		$dados['parcelamento_das_qtd_encerrado_contibuinte'] = $this->parcelamento_das_model->get_qtd_parcelamentos()->qtd;
		//FIM PARCELAMENTOS DAS

		//INICIO PARCELAMENTOS PERT
		$this->parcelamento_pert_model->setFiltro("TODAS");
		$dados['parcelamento_pert_qtd'] = $this->parcelamento_pert_model->get_qtd_parcelamentos()->qtd;

		$this->parcelamento_pert_model->setFiltro("EM_PARCELAMENTO");
		$dados['parcelamento_pert_qtd_em_parcelamento'] = $this->parcelamento_pert_model->get_qtd_parcelamentos()->qtd;
		
		$this->parcelamento_pert_model->setFiltro("ENCERRADO_RECISAO");
		$dados['parcelamento_pert_qtd_encerrado_recisao'] = $this->parcelamento_pert_model->get_qtd_parcelamentos()->qtd;

		$this->parcelamento_pert_model->setFiltro("ENCERRADO_CONTRIBUINTE");
		$dados['parcelamento_pert_qtd_encerrado_contibuinte'] = $this->parcelamento_pert_model->get_qtd_parcelamentos()->qtd;
		//FIM PARCELAMENTOS PERT

		//INICIO PARCELAMENTO NAO PREVIDENCIARIO
		$this->parcelamento_nao_previdenciario_model->setFiltro("TODAS");
		$dados['parcelamento_nao_previdenciario_qtd'] = $this->parcelamento_nao_previdenciario_model->get_qtd_processos_negociados()->qtd;

		$this->parcelamento_nao_previdenciario_model->setFiltro("EM_PARCELAMENTO");
		$dados['parcelamento_nao_previdenciario_qtd_em_parcelamento'] = $this->parcelamento_nao_previdenciario_model->get_qtd_processos_negociados()->qtd;
		//FIM PARCELAMENTO NAO PREVIDENCIARIO

		//INICIO PARCELAMENTO LEI 12996
		$this->parcelamento_lei_12996_model->setFiltro("TODAS");
		$dados['parcelamento_lei_12996_qtd'] = $this->parcelamento_lei_12996_model->get_qtd_processos_negociados()->qtd;

		$this->parcelamento_lei_12996_model->setFiltro("EM_PARCELAMENTO");
		$dados['parcelamento_lei_12996_qtd_em_parcelamento'] = $this->parcelamento_lei_12996_model->get_qtd_processos_negociados()->qtd;

		$this->parcelamento_lei_12996_model->setFiltro("REJEITADA");
		$dados['parcelamento_lei_12996_qtd_em_rejeitada'] = $this->parcelamento_lei_12996_model->get_qtd_processos_negociados()->qtd;
		//FIM PARCELAMENTO LEI 12996

		//INICIO PARCELAMENTOS PERT RFB
		$this->parcelamento_pert_rfb_model->setFiltro("TODAS");
		$dados['parcelamento_pert_rfb_qtd'] = $this->parcelamento_pert_rfb_model->get_qtd_parcelamentos()->qtd;

		$this->parcelamento_pert_rfb_model->setFiltro("ATIVO");
		$dados['parcelamento_pert_rfb_qtd_em_parcelamento'] = $this->parcelamento_pert_rfb_model->get_qtd_parcelamentos()->qtd;
		
		$this->parcelamento_pert_rfb_model->setFiltro("EXCLUIDO");
		$dados['parcelamento_pert_rfb_qtd_encerrado_recisao'] = $this->parcelamento_pert_rfb_model->get_qtd_parcelamentos()->qtd;

		$this->parcelamento_pert_rfb_model->setFiltro("CANCELADO");
		$dados['parcelamento_pert_rfb_qtd_encerrado_contibuinte'] = $this->parcelamento_pert_rfb_model->get_qtd_parcelamentos()->qtd;
		//FIM PARCELAMENTOS PERT RFB

		//INICIO PARCELAMENTOS MEI
		$this->parcelamento_mei_model->setFiltro("TODAS");
		$dados['parcelamento_mei_qtd'] = $this->parcelamento_mei_model->get_qtd_parcelamentos()->qtd;

		$this->parcelamento_mei_model->setFiltro("EM_PARCELAMENTO");
		$dados['parcelamento_mei_qtd_em_parcelamento'] = $this->parcelamento_mei_model->get_qtd_parcelamentos()->qtd;
		
		$this->parcelamento_mei_model->setFiltro("CONCLUIDO");
		$dados['parcelamento_mei_qtd_concluido'] = $this->parcelamento_mei_model->get_qtd_parcelamentos()->qtd;
		//FIM PARCELAMENTOS MEI

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('parcelamento/dashboard', $dados);
		$this->load->view('layout/footer');
		
	}

	
}