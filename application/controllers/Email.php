<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends MY_Controller {
	
	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function cadastrar(){
		$this->load->model('email_model', 'e');
			
		if(isset($_POST['btn_cadastrar'])){

			$this->e->setAtivo($this->validar_input($this->input->post("ativo")));
			$this->e->setNome($this->validar_input($this->input->post("nome")));
			$this->e->setEmail($this->validar_input($this->input->post("email")));
			$this->e->setIdContabilidade($this->session->userdata['userprimesession']['id']);
				
			$result = $this->e->cadastrar();
				
			if ($result){
				$this->session->set_flashdata('msg_titulo','SUCESSO!');
				$this->session->set_flashdata('msg_conteudo','O cadastro foi efetuado.');
				$this->session->set_flashdata('msg_tipo','success');
				redirect('email/listar');
			} else {
				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
				$this->session->set_flashdata('msg_tipo','danger');
			}

		}
				
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');		
		$this->load->view('layout/header');
		$this->load->view('email/email_cadastrar');
		$this->load->view('layout/footer');
	
	}
	
	public function listar(){
		
		$this->load->model('email_model');
		$this->email_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->email_model->listar();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header');
		$this->load->view('email/email_listar', $dados);
		$this->load->view('layout/footer');
		
	}
	
	public function excluir() {
	
		$id = $this->uri->segment(3);
		$id_contabilidade = $this->uri->segment(4);
	
		if ($id_contabilidade != '' || $id != ''){
				
			//VALIDAR SE O USUÁRIO É O DONO DA EMPRESA EM FOCO
			$validate = $this->validar_usuario($id_contabilidade);
				
			if ($validate){
	
				$this->load->model('email_model', 'e');
	
				$this->e->setId($id);
				$result = $this->e->excluir();
	
				if ($result){
	
					$this->session->set_flashdata('msg_titulo','SUCESSO!');
					$this->session->set_flashdata('msg_conteudo','O email foi excluído.');
					$this->session->set_flashdata('msg_tipo','success');
	
					redirect('email/listar');
	
				} else {
					$this->session->set_flashdata('msg_titulo','ERRO!');
					$this->session->set_flashdata('msg_conteudo','Não foi possível excluir.');
					$this->session->set_flashdata('msg_tipo','danger');
	
					redirect('email/listar');
	
				}
	
			} else {
				redirect('email/listar');
			}
				
				
		}
		else {
			redirect('email/listar');
		}
	
	}

	public function editar(){
	
		$id = $this->uri->segment(3);
		$id_contabilidade = $this->uri->segment(4);
	
		if ($id != '' && $id_contabilidade != ''){
			
			if ($this->validar_usuario($id_contabilidade)){
				
				$this->load->model('email_model', 'e');
				
				if(isset($_POST['btn_salvar'])){
				
					$this->e->setId($id);
					$this->e->setAtivo($this->validar_input($this->input->post("ativo")));
					$this->e->setNome($this->validar_input($this->input->post("nome")));
					$this->e->setEmail($this->validar_input($this->input->post("email")));
				
					$result = $this->e->editar();
						
					if ($result){
						$dados['msg_titulo'] = 'SUCESSO!';
						$dados['msg_conteudo'] = 'O email foi editado.';
						$dados['msg_tipo'] = 'success';
							
					} else {
						$dados['msg_titulo'] = 'ERRO!';
						$dados['msg_conteudo'] = 'Não foi possível editar.';
						$dados['msg_tipo'] = 'danger';
					}
				
				}
					
				$this->e->setId($id);
				$dados['email'] = $this->e->pesquisar_email_id();
					
				$dados['id'] = $id;
				$dados['id_contabilidade'] = $id_contabilidade;
					
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header');
				$this->load->view('email/email_editar', $dados);
				$this->load->view('layout/footer');
				
			} else {
				redirect("email/listar");
			}
				
		} else {
			redirect("email/listar");
		}
	
	}
	
}
