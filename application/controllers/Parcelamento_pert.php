<?php

use Google\Auth\Cache\Item;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parcelamento_pert extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('parcelamento_pert_model');
       	$this->load->model('empresa_model');
    }

	public function listar(){
		date_default_timezone_set('America/Sao_Paulo');

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
        $razao = "";
        $filtro = "";

        if($cnpj != "EM_PARCELAMENTO" && $cnpj != "ENCERRADO_RECISAO" && $cnpj != "TODAS" && $cnpj != "ENCERRADO_CONTRIBUINTE" && $cnpj != "PARCELAS_ATRASO" && $cnpj != "PARCELAS_SEM_ATRASO" && $cnpj != "PAGO_MES" && $cnpj != "PAGO_MES_NAO" && $cnpj != "CONCLUIDO"){
            if($cnpj != null){
                $em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
                $razao = $em->razao_social;
            }
            $filtro = "TODAS";
        }else{
            $filtro = $cnpj;
        }

        $this->parcelamento_pert_model->setFiltro("TODAS");

        $dados['razao_social_filtro'] = $razao;
        $dados['filtro'] = $filtro;

        $resultado = $this->parcelamento_pert_model->listar_pedidos();

        //busca parcelas que podem ser emitidas
        $parcelas = $this->parcelamento_pert_model->listar_parcelas();
        $parcelas_empresa = array();
        $parcelas_empresas_aux = array();

		
        foreach ($parcelas as $p) {
        	$parcelas_empresas_aux = array();

        	if(isset($parcelas_empresa[$p->cnpj])){
        		array_push($parcelas_empresa[$p->cnpj], trim($p->data_parcela));
        	}else{
        		array_push($parcelas_empresas_aux, trim($p->data_parcela));
        		$parcelas_empresa[$p->cnpj] = $parcelas_empresas_aux;
        	}
        }

        //busca parcelas já pagas
        $pagas = $this->parcelamento_pert_model->listar_parcelas_pagas();
        $parcelas_pagas = array();
        $parcelas_pagas_aux = array();

        foreach ($pagas as $p) {
        	$parcelas_pagas_aux = array();

        	if(isset($parcelas_pagas[$p->cnpj])){
        		array_push($parcelas_pagas[$p->cnpj], trim($p->mes_parcela));
        	}else{
        		array_push($parcelas_pagas_aux, trim($p->mes_parcela));
        		$parcelas_pagas[$p->cnpj] = $parcelas_pagas_aux;
        	}
        }

        $ha_parcelas_nao_pagas = false;

        $resultado_final_listagem = array();
        foreach ($resultado as $r) {
        	$parcelas_p = null;
        	$parcelas_np = null;
        	if(isset($parcelas_pagas[$r->cnpj])){
        		$parcelas_p = $parcelas_pagas[$r->cnpj];
        	}

        	if(isset($parcelas_empresa[$r->cnpj])){
        		$parcelas_np = $parcelas_empresa[$r->cnpj];
        	}
        	$this->verifica_se_ha_parcelas($r, $parcelas_p, $parcelas_np);

        	
			$this->calculaNumeroParcela($r);
			

        	$r->razao_social = str_replace("&", "", $r->razao_social);

        	//FILTRO
        	if($filtro == "EM_PARCELAMENTO"){
        		if($r->situacao == "Em parcelamento"){
        			array_push($resultado_final_listagem, $r);
        		}
        	}elseif($filtro == "ENCERRADO_RECISAO"){
        		if($r->situacao == "Encerrado por Rescisão"){
        			array_push($resultado_final_listagem, $r);
        		}
        	}elseif($filtro == "TODAS"){
        		array_push($resultado_final_listagem, $r);
        	}elseif($filtro == "ENCERRADO_CONTRIBUINTE"){
        		if($r->situacao == "Encerrado a Pedido do Contribuinte"){
        			array_push($resultado_final_listagem, $r);
        		}
        	}elseif($filtro == "PARCELAS_ATRASO"){
        		if($r->parcelas_nao_pagas == "Sim"){
        			array_push($resultado_final_listagem, $r);
        		}
        	}elseif($filtro == "PARCELAS_SEM_ATRASO"){
        		if($r->parcelas_nao_pagas == "Não"){
        			array_push($resultado_final_listagem, $r);
        		}
        	}elseif($filtro == "PAGO_MES"){
        		if($r->pagou_parcela_atual == true){
        			array_push($resultado_final_listagem, $r);
        		}
        	}elseif($filtro == "PAGO_MES_NAO"){
        		if($r->pagou_parcela_atual == false){
        			array_push($resultado_final_listagem, $r);
        		}
        	}elseif($filtro == "CONCLUIDO"){
        		if($r->situacao == "Concluído"){
        			array_push($resultado_final_listagem, $r);
        		}
        	}

        }

     
		$dados['consulta'] = $resultado_final_listagem;

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
	  	$dia_atual = date('j');
	  	$mes_atual = date('m');
	  	$ano_atual = date('Y');

	  	if($dia_atual >= 1 && $dia_atual <= 10){
	  		$dados['pode_gerar_parcela_atual'] = false;
	  	}else{
	  		$dados['pode_gerar_parcela_atual'] = true;
	  	}

	  	$dados['mes_parcela'] = $mes_atual."/".$ano_atual;


		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('parcelamento/parcelamento_pert', $dados);
		$this->load->view('layout/footer');
		
	}


	public function verifica_se_ha_parcelas($item, $parcelas_pagas, $parccelas_nao_pagas){
		$item->detalhes_parcelas_nao_pagas = "";

		if($parcelas_pagas == null && $parccelas_nao_pagas == null){
			$item->parcelas_nao_pagas = "Não";
		}elseif($parcelas_pagas == null && $parccelas_nao_pagas != null){
			$item->parcelas_nao_pagas = "Sim";
		}elseif($parcelas_pagas != null && $parccelas_nao_pagas == null){
			$item->parcelas_nao_pagas = "Não";
		}else{

			foreach ($parccelas_nao_pagas as $p) {
				if (in_array($p, $parcelas_pagas)) { 
				    $item->parcelas_nao_pagas = "Não";
				}else{
					$item->parcelas_nao_pagas = "Sim";

					$item->detalhes_parcelas_nao_pagas = $item->detalhes_parcelas_nao_pagas." | ".$p;
				}
			}

		}

	}

	public function calculaNumeroParcela($item){

		date_default_timezone_set('America/Sao_Paulo');
		$dia_atual_aux = date('j');
	  	$mes_atual = date('m');
	  	$ano_atual = date('Y');

		$dia_atual = date('d');


		$data_pedido = $item->data_pedido;

		$array = explode("/",$data_pedido);

		$data_final = '01'."/".$array[1]."/".$array[2];


		$data_parcelamento1 = str_replace('/', '-', $data_final);
		$data_parcelamento = date('Y-m-d', strtotime($data_parcelamento1));

		$data_atual = date('Y-m-d');

		$data1 = new DateTime( $data_parcelamento );
		$data2 = new DateTime( $data_atual );

		//Calcula o numero da parcela atual 1º, 2º....
		$intervalo = $data2->diff( $data1 );
		$item->parcela_atual_numero = $intervalo->m + ($intervalo->y * 12) + 1;
		
		$item->mes_parcela = $mes_atual."/".$ano_atual;

		//Verifica se a parcela atual já foi paga
		$pagou_parcela_atual = $this->parcelamento_pert_model->pagou_parcela_mes_atual($item->mes_parcela,$item->id);
		if(isset($pagou_parcela_atual)){
			$item->pagou_parcela_atual = true;
			$item->detalhes_pagamento = "Valor Pago: ".$pagou_parcela_atual->valor_pago."<br>"."Data de arrecadação: ".$pagou_parcela_atual->data_arrecadacao;
		}else{
			$item->pagou_parcela_atual = false;
			$item->parcela_atual = $this->parcelamento_pert_model->get_parcela_atual($item->mes_parcela, $item->cnpj);
			$item->detalhes_pagamento = "Não Pago";
		}

		//Verifica se todas as parcelas já foram pagas
		$qtd_parcelas_pagas = $this->parcelamento_pert_model->qtd_pagamentos($item->id);
		// if($qtd_parcelas_pagas->qtd >= $item->qtd_parcelas){
		// 	$item->situacao = "Concluído";
		// }
	}

	public function listar_detalhes(){

		$id = $this->uri->segment(3);

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$pedido = $this->parcelamento_pert_model->listar_pedidos_individual($id);

		$dados['pedido'] = $pedido;

		$dados['consulta'] = $this->parcelamento_pert_model->listar_debitos($id);

		$dados['pagamentos'] = $this->parcelamento_pert_model->listar_pagamentos($id);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('parcelamento/parcelamento_pert_detalhes', $dados);
		$this->load->view('layout/footer');
		
	}

	public function find_parcelas_for_modal(){
		$cnpj = $this->input->post('cnpj');
		$parcelas_a_pagar = array();

		//busca parcelas para gerar
		$parcelas = $this->parcelamento_pert_model->listar_parcelas_for_modal($cnpj);
        $parcelas_empresa = array();
        $parcelas_empresas_aux = array();

        foreach ($parcelas as $p) {
        	$parcelas_empresas_aux = array();

        	if(isset($parcelas_empresa[$p->cnpj])){
        		array_push($parcelas_empresa[$p->cnpj], trim($p->data_parcela));
        	}else{
        		array_push($parcelas_empresas_aux, trim($p->data_parcela));
        		$parcelas_empresa[$p->cnpj] = $parcelas_empresas_aux;
        	}
        }

        //busca parcelas já pagas
        $pagas = $this->parcelamento_pert_model->listar_parcelas_pagas_for_modal($cnpj);
        $parcelas_pagas = array();
        $parcelas_pagas_aux = array();

        foreach ($pagas as $p) {
        	$parcelas_pagas_aux = array();

        	if(isset($parcelas_pagas[$p->cnpj])){
        		array_push($parcelas_pagas[$p->cnpj], trim($p->mes_parcela));
        	}else{
        		array_push($parcelas_pagas_aux, trim($p->mes_parcela));
        		$parcelas_pagas[$p->cnpj] = $parcelas_pagas_aux;
        	}
        }


        $parcelas_p = null;
    	$parcelas_np = null;
    	if(isset($parcelas_pagas[$cnpj])){
    		$parcelas_p = $parcelas_pagas[$cnpj];
    	}

    	if(isset($parcelas_empresa[$cnpj])){
    		$parcelas_np = $parcelas_empresa[$cnpj];
    	}

        $parcelas_a_pagar = $this->obter_parcelas($parcelas_p, $parcelas_np);

        // echo sizeof($parcelas_p);
        // echo "<br>";
        // echo sizeof($parcelas_np);
        // echo "<br>";
        // echo sizeof($parcelas_a_pagar);
        // die();

        $resultado_final = array();

        foreach ($parcelas_a_pagar as $parcela) {
        	$objeto = new stdClass();
        	$p = $this->parcelamento_pert_model->listar_parcelas_for_modal_detalhe($cnpj, $parcela);
        	array_push($resultado_final, $p);
        }

        echo json_encode($resultado_final);
	}

	public function obter_parcelas($parcelas_pagas, $parccelas_nao_pagas){
		$a = array();
		if($parcelas_pagas == null && $parccelas_nao_pagas == null){
			// $item->parcelas_nao_pagas = "Não";
		}elseif($parcelas_pagas == null && $parccelas_nao_pagas != null){
			$a = $parccelas_nao_pagas;
		}elseif($parcelas_pagas != null && $parccelas_nao_pagas == null){
			// $item->parcelas_nao_pagas = "Não";
		}else{

			foreach ($parccelas_nao_pagas as $p) {
				if (in_array($p, $parcelas_pagas)) { 
					//já foi paga ou seja não deve ser exibido no link de gerar
				}else{
					array_push($a,$p);
				}
			}

		}

		return $a;
	}


}