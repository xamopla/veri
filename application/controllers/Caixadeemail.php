<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Caixadeemail extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('caixadeemail_model');
		$this->load->model('usuario_model');	
		$this->load->model('plano_model');	
		$this->load->model('dec_model');	
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	public function listar(){	

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$tour = $this->uri->segment(3);
		if($tour != ""){
			$dados['fromTour'] = "true";	
		}

		//VALIDAÇÃO DO PLANO EXCEDIDO
		$this->load->model('plano_model');
		$dados['e'] = $this->empresa_model->qtd_empresas();
		$dados['emax'] = $this->plano_model->valor_max_empresas();
		// FIM DA VALIDAÇÃO
		
		$this->caixadeemail_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		// $dados['consulta'] = $this->caixadeemail_model->listar();

		$dados['qtd_novos_emails'] = $this->caixadeemail_model->qtd_novos_emails();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('caixadeemail/caixadeemail_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function listarAjax(){
		$emails = $this->caixadeemail_model->listar();
		echo json_encode($emails);
	}

	public function visualizar() {

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);

		$this->caixadeemail_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['qtd_novos_emails'] = $this->caixadeemail_model->qtd_novos_emails();

		$this->caixadeemail_model->setId($id);

		$dados['visualizar_email'] = $this->caixadeemail_model->visualizar_email();
		$this->caixadeemail_model->marcaremailscomolido();	

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('caixadeemail/caixadeemail_visualizar', $dados);
		$this->load->view('layout/footer');
	}

	public function redirecionarparaempresa() {		

		$dados['filtro'] = $this->uri->segment(2);
	
		$id = $this->uri->segment(3);

		$this->caixadeemail_model->setIdEmpresa($id);

		$dados['consulta'] = $this->caixadeemail_model->redirecionarparaempresa();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header');
		$this->load->view('consultas/consulta_geral', $dados);
		$this->load->view('layout/footer');	
		
	}

	public function excluir() {
	
		$id = $this->uri->segment(3);
		$id_contabilidade = $this->uri->segment(4);	
	
		$this->caixadeemail_model->setId($id);
		$result = $this->caixadeemail_model->excluir();

		redirect('caixadeemail/listar');
	
	
	}

	public function excluir_todos_lidos() {
	
		$result = $this->caixadeemail_model->excluir_todos_lidos();

		redirect('caixadeemail/listar');
	
	}

	public function marcar_como_lido() {
	
		$id = $this->uri->segment(3);

		$this->caixadeemail_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		// $dados['qtd_novos_emails'] = $this->caixadeemail_model->qtd_novos_emails();

		$this->caixadeemail_model->setId($id);

		$this->caixadeemail_model->marcaremailscomolido();	
		redirect('caixadeemail/listar');
	
	}

	public function marcar_como_nao_lido() {
	
		$id = $this->uri->segment(3);

		$this->caixadeemail_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		// $dados['qtd_novos_emails'] = $this->caixadeemail_model->qtd_novos_emails();

		$this->caixadeemail_model->setId($id);

		$this->caixadeemail_model->marcaremailscomonaolido();	
		redirect('caixadeemail/listar');
	
	}

	public function sincronizar() {

		include_once ("email/db_connect.php");

		$login = 'contato.databytetecnologia@gmail.com';//email que recebe os cnpjs. 
		$senha = '1*Databyte*1';//senha do email.

		$login2 = $this->session->userdata['userprimesession']['email_sefaz'];
		$senha2 = $this->session->userdata['userprimesession']['senha_sefaz'];

		if($this->session->userdata['userprimesession']['nivel'] != 2){
			$id_contabilidade = $this->session->userdata['userprimesession']['id'];
		}else{
			$id_contabilidade = $this->session->userdata['userprimesession']['id_cadastro'];
		}
		//$id_contabilidade = $this->session->userdata['userprimesession']['id_cadastro'];

		$servidordeemail = $this->session->userdata['userprimesession']['servidor_email'];

		if ($servidordeemail == 1) {

			$str_conexao = '{imap.gmail.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 2) {

			$str_conexao = '{imap.mail.yahoo.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 3) {

			$str_conexao = '{imap-mail.outlook.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 4) {

			$str_conexao = '{imap.bol.com.br:993/imap/ssl}INBOX';

		} else if($servidordeemail == 5) {

			$str_conexao = '{imap.uol.com.br:993/imap/ssl}INBOX';

		} else if($servidordeemail == 6) {

			$str_conexao = '{imap.merca.com.br:143/imap/ssl}INBOX';
		}


		if (!extension_loaded('imap')) {

		    die('Modulo PHP/IMAP nao foi carregado');
		}

		// Abrindo conexao

		$mailbox = imap_open($str_conexao, $login2, $senha2);

		if (!$mailbox) {
		    redirect('aviso/erro_autentiticar_email');
		}


		$numero_mensagens = imap_num_msg($mailbox);
		$numero_mens_nao_lidas = imap_num_recent($mailbox);
		  

		// if ($numero_mensagens == 1) {

		//  echo "você tem $numero_mensagens mensagem";

		// }else{

		// echo "você tem $numero_mensagens mensagens";
		// } 

		// echo "<br><br>";

		//$result = imap_search($connection, 'SUBJECT "Analytics www.electrictoolbox.com" FROM "chris@example.com"');
		#$result = imap_search($mailbox,'UNSEEN SUBJECT "DTE - Recebimento de mensagem"');
		$result = imap_search($mailbox,'UNSEEN SUBJECT "Nova mensagem no DT-e"');
		if (is_array($result) || is_object($result)){
		    foreach($result as $msgno) {
		        $headers = imap_header($mailbox, $msgno);
		        
		        //$headers = imap_header($mailbox, $i);
		        $assunto            = $headers->subject;
		        $message_id         = $headers->message_id;
		        $toaddress          = $headers->toaddress;
		        $to                 = $headers->to;
		        $email_remetente    = $to[0]->mailbox;
		        $servidor_remetente = $to[0]->host;
		        $data               = $headers->date;
		        $data               = strtotime($data);
		        $data1              = date("d/m/Y H:i:s", $data);
		        # BODY
		        $body = imap_body($mailbox,$msgno); //echo "<pre>"; print_r($body); echo '</pre>'; die;

		        $overview = imap_fetch_overview($mailbox,$msgno,0);
        		$structure = imap_fetchstructure($mailbox, $msgno); //echo "<pre>"; print_r($structure); echo '</pre>';
        		# ENCODING
        		$encoding = (int) $structure->encoding;
        		if ($encoding == 0) { $body_decoded = $body; } # 7BIT
        		else if ($encoding == 1) { $body_decoded = quoted_printable_decode(imap_8bit($body)); } # 8BIT
        		else if ($encoding == 2) { $body_decoded = imap_binary($body); } # BINARY
        		else if ($encoding == 3) { $body_decoded = imap_base64($body); } # BASE64 
        		else if ($encoding == 4) { $body_decoded = quoted_printable_decode($body); } # QUOTED-PRINTABLE
        		else if ($encoding == 5) { $body_decoded = $body; }  # OTHER
        		else { $body_decoded = $body; } # UNKNOWN
		        #echo "<pre>body decodec\n"; print_r($body_decoded); echo '</pre>'; die;
				
		        # CAPTA CNPJ
		        preg_match_all('/(\d{2}.\d{3}.\d{3}\/\d{4}\-\d{2})/', $body_decoded,$matchj);

		        $result = array_unique($matchj[0]);
		        foreach($result as $ht){

		        	#echo "CNPJ: ".$ht."<br>"; die;

			        $prep_stmtAux = "SELECT id, razao_social FROM dtb_empresas WHERE cnpj_completo = ? ORDER BY id DESC LIMIT 1 ";
			        $stmt = $mysqli->prepare($prep_stmtAux);
			        $stmt->bind_param('s', $ht);
			        $stmt->execute();
			        $stmt->store_result();
			        $stmt->bind_result($id, $razao_social);

			        $razao_socialBd = '';
			        $idEmpresa = '';

			        while ($stmt->fetch()) {  
			            $razao_socialBd = $razao_social;
			            $idEmpresa = $id;

			        };

			        $stmt->close();

			        $conteudomensagem = "Prezado(a) Senhor(a), há uma nova mensagem na conta Domicílio Tributário Eletrônico para o estabelecimento ".$ht;

			        $status = "1";
			        $id_contabilidade = $this->session->userdata['userprimesession']['id'];

			        $insert_stmt = $mysqli->prepare("INSERT INTO dtb_caixadeemail (cnpj, id_empresa, razao_social, titulo, mensagem, status, id_contabilidade, remetente, dataemail) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			        $insert_stmt->bind_param('sisssiiss', $ht, $idEmpresa, $razao_social, $assunto, $conteudomensagem, $status, $id_contabilidade, $email_remetente, $data1);
			        $insert_stmt->execute();
			               
			        $insert_stmt->close();
			    }

		         //deleta o email lido
		        imap_delete($mailbox, $msgno);
		        
		        //echo "Assunto = ".$assuntos = imap_utf8($assunto)."<br>"; 
		        //echo "Remetente = $email_remetente@$servidor_remetente"."<br>"; 
		        //echo "destinatario = ".$array."<br>"; 
		        //echo "Data = $data1"."<br>"; 
		        //echo "<br><br>";
		    }
		}

		$stmt = $mysqli->prepare("SELECT cnpj FROM dtb_caixadeemail");
		$stmt->execute();
		$stmt->store_result();  
		$stmt->bind_result($cnpj_bd);

		// echo "<table id='customers'>";
		// echo "<tr>";
		// echo "<th id='titulo'>CNPJ salvo no Banco de dados</th>";
		// echo "</tr>";


		// while ($stmt->fetch()) {  



		// echo "<td>".$cnpj_bd."</td><tr>";

		// };

		// echo "</table>";

		redirect('caixadeemail/listar');

		imap_close($mailbox);
	}

	public function sincronizarForLogin() {

		include_once ("email/db_connect.php");

		$login2 = $this->session->userdata['userprimesession']['email_sefaz'];
		$senha2 = $this->session->userdata['userprimesession']['senha_sefaz'];

		if($this->session->userdata['userprimesession']['nivel'] != 2){
			$id_contabilidade = $this->session->userdata['userprimesession']['id'];
		}else{
			$id_contabilidade = $this->session->userdata['userprimesession']['id_cadastro'];
		}
		//$id_contabilidade = $this->session->userdata['userprimesession']['id_cadastro'];

		$servidordeemail = $this->session->userdata['userprimesession']['servidor_email'];

		if ($servidordeemail == 1) {

			$str_conexao = '{imap.gmail.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 2) {

			$str_conexao = '{imap.mail.yahoo.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 3) {

			$str_conexao = '{imap-mail.outlook.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 4) {

			$str_conexao = '{imap.bol.com.br:993/imap/ssl}INBOX';

		} else if($servidordeemail == 5) {

			$str_conexao = '{imap.uol.com.br:993/imap/ssl}INBOX';

		} else if($servidordeemail == 6) {

			$str_conexao = '{imap.merca.com.br:143/imap/ssl}INBOX';
		}


		if (!extension_loaded('imap')) {

		    die('Modulo PHP/IMAP nao foi carregado');
		}

		// Abrindo conexao

		$mailbox = imap_open($str_conexao, $login2, $senha2);

		if (!$mailbox) {
		    redirect('aviso/erro_autentiticar_email');
		    // die('Erro ao conectar: '.imap_last_error());
		}


		$numero_mensagens = imap_num_msg($mailbox);
		$numero_mens_nao_lidas = imap_num_recent($mailbox);
		  

		// if ($numero_mensagens == 1) {

		//  echo "você tem $numero_mensagens mensagem";

		// }else{

		// echo "você tem $numero_mensagens mensagens";
		// } 

		// echo "<br><br>";

		//$result = imap_search($connection, 'SUBJECT "Analytics www.electrictoolbox.com" FROM "chris@example.com"');
		#$result = imap_search($mailbox,'UNSEEN SUBJECT "DTE - Recebimento de mensagem"');
		$result = imap_search($mailbox,'UNSEEN SUBJECT "DTE - Recebimento de mensagem"');
		$qtdMsg = 0;

		if (is_array($result) || is_object($result)){
		    foreach($result as $msgno) {
		        $headers = imap_header($mailbox, $msgno);
		        
		        //$headers = imap_header($mailbox, $i);
		        $assunto            = $headers->subject;
		        $message_id         = $headers->message_id;
		        $toaddress          = $headers->toaddress;
		        $to                 = $headers->to;
		        $email_remetente    = $to[0]->mailbox;
		        $servidor_remetente = $to[0]->host;
		        $data               = $headers->date;
		        $data               = strtotime($data);
		        $data1              = date("d/m/Y H:i:s", $data);
		        # BODY
		        $body = imap_body($mailbox,$msgno); //echo "<pre>"; print_r($body); echo '</pre>'; die;

		        $overview = imap_fetch_overview($mailbox,$msgno,0);
        		$structure = imap_fetchstructure($mailbox, $msgno); //echo "<pre>"; print_r($structure); echo '</pre>';
        		# ENCODING
        		$encoding = (int) $structure->encoding;
        		if ($encoding == 0) { $body_decoded = $body; } # 7BIT
        		else if ($encoding == 1) { $body_decoded = quoted_printable_decode(imap_8bit($body)); } # 8BIT
        		else if ($encoding == 2) { $body_decoded = imap_binary($body); } # BINARY
        		else if ($encoding == 3) { $body_decoded = imap_base64($body); } # BASE64 
        		else if ($encoding == 4) { $body_decoded = quoted_printable_decode($body); } # QUOTED-PRINTABLE
        		else if ($encoding == 5) { $body_decoded = $body; }  # OTHER
        		else { $body_decoded = $body; } # UNKNOWN
		        #echo "<pre>body decodec\n"; print_r($body_decoded); echo '</pre>'; die;
				
		        # CAPTA CNPJ
		        preg_match_all('/(\d{2}.\d{3}.\d{3}\/\d{4}\-\d{2})/', $body_decoded,$matchj);

		        $result = array_unique($matchj[0]);
		        foreach($result as $ht){

		        	#echo "CNPJ: ".$ht."<br>"; die;

			        $prep_stmtAux = "SELECT id, razao_social FROM dtb_empresas WHERE cnpj_completo = ? ORDER BY id DESC LIMIT 1 ";
			        $stmt = $mysqli->prepare($prep_stmtAux);
			        $stmt->bind_param('s', $ht);
			        $stmt->execute();
			        $stmt->store_result();
			        $stmt->bind_result($id, $razao_social);

			        $razao_socialBd = '';
			        $idEmpresa = '';

			        while ($stmt->fetch()) {  
			            $razao_socialBd = $razao_social;
			            $idEmpresa = $id;

			        };

			        $stmt->close();

			        $conteudomensagem = "Prezado(a) Senhor(a), há uma nova mensagem na conta Domicílio Tributário Eletrônico para o estabelecimento ".$ht;

			        $status = "1";
			        $id_contabilidade = $this->session->userdata['userprimesession']['id'];

			        $insert_stmt = $mysqli->prepare("INSERT INTO dtb_caixadeemail (cnpj, id_empresa, razao_social, titulo, mensagem, status, id_contabilidade, remetente, dataemail) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			        $insert_stmt->bind_param('sisssiiss', $ht, $idEmpresa, $razao_social, $assunto, $conteudomensagem, $status, $id_contabilidade, $email_remetente, $data1);
			        $insert_stmt->execute();
			               
			        $insert_stmt->close();

			        $qtdMsg = $qtdMsg + 1;
			    }

		         //deleta o email lido
		        imap_delete($mailbox, $msgno);
		        
		        //echo "Assunto = ".$assuntos = imap_utf8($assunto)."<br>"; 
		        //echo "Remetente = $email_remetente@$servidor_remetente"."<br>"; 
		        //echo "destinatario = ".$array."<br>"; 
		        //echo "Data = $data1"."<br>"; 
		        //echo "<br><br>";
		    }
		}

		$stmt = $mysqli->prepare("SELECT cnpj FROM dtb_caixadeemail");
		$stmt->execute();
		$stmt->store_result();  
		$stmt->bind_result($cnpj_bd);

		imap_close($mailbox);

		echo $qtdMsg;
	}
	

	public function sincronizar_teste() {

		//include_once ("email/db_connect.php");
		date_default_timezone_set('America/Bahia');
		
		$login = 'contato.databytetecnologia@gmail.com';//email que recebe os cnpjs. 
		$senha = '1*Databyte*1';//senha do email.

		$resultado = $this->plano_model->pesquisar_plano_id();

		$login2 = $resultado->email_dte;//$this->session->userdata['userprimesession']['email_sefaz'];
		$senha2 = $resultado->senha_dte;//$this->session->userdata['userprimesession']['senha_sefaz'];
		$servidordeemail = $resultado->servidor_email;

		if($this->session->userdata['userprimesession']['nivel'] != 2){
			$id_contabilidade = $this->session->userdata['userprimesession']['id'];
		}else{
			$id_contabilidade = $this->session->userdata['userprimesession']['id_cadastro'];
		}
		//$id_contabilidade = $this->session->userdata['userprimesession']['id_cadastro'];

		//$servidordeemail = 1;//$this->session->userdata['userprimesession']['servidor_email'];

		if ($servidordeemail == 1) {

			$str_conexao = '{imap.gmail.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 2) {

			$str_conexao = '{imap.mail.yahoo.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 3) {

			$str_conexao = '{imap-mail.outlook.com:993/imap/ssl}INBOX';

		} else if($servidordeemail == 4) {

			$str_conexao = '{imap.bol.com.br:993/imap/ssl}INBOX';

		} else if($servidordeemail == 5) {

			$str_conexao = '{imap.uol.com.br:993/imap/ssl}INBOX';

		} else if($servidordeemail == 6) {

			$str_conexao = '{imap.merca.com.br:143/imap/ssl}INBOX';
		}


		if (!extension_loaded('imap')) {

		    die('Modulo PHP/IMAP nao foi carregado');
		}

		// Abrindo conexao

		$mailbox = imap_open($str_conexao, $login2, $senha2);

		if (!$mailbox) {
		    redirect('aviso/erro_autentiticar_email');
		}


		$numero_mensagens = imap_num_msg($mailbox);
		$numero_mens_nao_lidas = imap_num_recent($mailbox);
		  
		$result = imap_search($mailbox,'UNSEEN SUBJECT "Nova mensagem no DT-e"');
		if (is_array($result) || is_object($result)){
		    foreach($result as $msgno) {
		        $headers = imap_header($mailbox, $msgno);
		        
		        //$headers = imap_header($mailbox, $i);
		        $assunto            = $headers->subject;
		        $message_id         = $headers->message_id;
		        $toaddress          = $headers->toaddress;
		        $to                 = $headers->to;
		        $email_remetente    = $to[0]->mailbox;
		        $servidor_remetente = $to[0]->host;
		        $data               = $headers->date;
		        $data               = strtotime($data);
		        $data1              = date("d/m/Y H:i:s", $data);
		        # BODY
		        $body = imap_body($mailbox,$msgno); //echo "<pre>"; print_r($body); echo '</pre>'; die;

		        $overview = imap_fetch_overview($mailbox,$msgno,0);
        		$structure = imap_fetchstructure($mailbox, $msgno); //echo "<pre>"; print_r($structure); echo '</pre>';
        		# ENCODING
        		$encoding = (int) $structure->encoding;
        		if ($encoding == 0) { $body_decoded = $body; } # 7BIT
        		else if ($encoding == 1) { $body_decoded = quoted_printable_decode(imap_8bit($body)); } # 8BIT
        		else if ($encoding == 2) { $body_decoded = imap_binary($body); } # BINARY
        		else if ($encoding == 3) { $body_decoded = imap_base64($body); } # BASE64 
        		else if ($encoding == 4) { $body_decoded = quoted_printable_decode($body); } # QUOTED-PRINTABLE
        		else if ($encoding == 5) { $body_decoded = $body; }  # OTHER
        		else { $body_decoded = $body; } # UNKNOWN
		        #echo "<pre>body decodec\n"; print_r($body_decoded); echo '</pre>'; die;
				
		        # CAPTA CNPJ
		        preg_match_all('/(\d{2}.\d{3}.\d{3}\/\d{4}\-\d{2})/', $body_decoded,$matchj);

		        $result = array_unique($matchj[0]);
		        foreach($result as $ht){

		        	#echo "CNPJ: ".$ht."<br>"; die;
		        	$consulta = $this->caixadeemail_model->find_empresa_email($ht);

		        	$razao_socialBd = '';
			        $idEmpresa = '';

		        	if(isset($consulta)){
		        		$razao_socialBd = $consulta->razao_social;
		        		$idEmpresa = $consulta->id;
		        	}

			        $conteudomensagem = "Prezado(a) Senhor(a), há uma nova mensagem na conta Domicílio Tributário Eletrônico para o estabelecimento ".$ht;

			        $status = "1";
			        $id_contabilidade = 1;//$this->session->userdata['userprimesession']['id'];

			        $this->caixadeemail_model->inserir_msg_nova($ht, $idEmpresa, $razao_socialBd, $assunto, $conteudomensagem, $status, $id_contabilidade, $email_remetente, $data1);
			        
			       
			    }

		         //deleta o email lido
		        //imap_delete($mailbox, $msgno);
		    }
		}
	}

	public function listar_aux_nova(){	

		$id_mensagem = $this->uri->segment(3);
		$razao = "";
		$filtro = ""; 
		
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		// $tour = $this->uri->segment(3);
		// if($tour != ""){
		// 	$dados['fromTour'] = "true";	
		// }

		//VALIDAÇÃO DO PLANO EXCEDIDO
		$this->load->model('plano_model');
		$dados['e'] = $this->empresa_model->qtd_empresas();
		$dados['emax'] = $this->plano_model->valor_max_empresas();
		// FIM DA VALIDAÇÃO
		
		//$this->caixadeemail_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		// $dados['consulta'] = $this->caixadeemail_model->listar();

		//$dados['qtd_novos_emails'] = $this->caixadeemail_model->qtd_novos_emails();
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		
		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];
	  	$dados['consulta'] = $this->dec_model->listar_caixa_postal();
	  	$dados['banco'] = $server;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/mensagens_det_temp', $dados);
		$this->load->view('layout/footer');
		
	}

	public function listar_msgs_dte(){
		$filtro = $this->input->post('filtro');
    	$this->caixadeemail_model->setFiltro($filtro);
    	
		$emails = $this->caixadeemail_model->listar_nova();
		echo json_encode($emails);
	}


	public function listar_msgs_dte_novo(){
		$filtro = $this->input->post('filtro');
    	$this->caixadeemail_model->setFiltro($filtro);
    	
		$emails = $this->caixadeemail_model->listar_nova_aux();
		echo json_encode($emails);
	}

	public function marcar_como_lido_aux() {
	
		$id = $this->uri->segment(3);

		$this->caixadeemail_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		// $dados['qtd_novos_emails'] = $this->caixadeemail_model->qtd_novos_emails();

		$this->caixadeemail_model->setId($id);

		$this->caixadeemail_model->marcaremailscomolido();	
		redirect('caixadeemail/listar_aux_nova');
	
	}


	public function marcar_msg_ecac_lida(){
		$id = $this->uri->segment(3);

		$this->caixadeemail_model->setId($id);
		$this->caixadeemail_model->marcar_msg_ecac_lida();	
		redirect('ecac/listar_mensagens');

	}

	public function buscar_conteudo(){
		$id = $this->input->post('id');

		$mensagem = $this->caixadeemail_model->buscar_conteudo($id); 

		echo json_encode($mensagem);
	}

}
