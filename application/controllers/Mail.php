<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends MY_Controller {

	public function index() {		

		$this->load->model('mail_model');

		$mail = $this->mail_model->listar();
		$id_mail = "";
		if(isset($mail)){
			$id_mail = $mail->id;
		}

		$dados['mail'] = $mail;
		$dados['id_mail'] = $id_mail;
		
	    //--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('mail/mail', $dados);
		$this->load->view('layout/footer');
	}

	public function salvar(){
		$this->load->model('mail_model');

		$id = $this->uri->segment(3);

		if($id != ""){
			$this->mail_model->setEmail($this->input->post("email"));
			$this->mail_model->setAssunto($this->input->post("assunto"));
			$this->mail_model->setId($id);

			$this->mail_model->editar();
		}else{
			$this->mail_model->setEmail($this->input->post("email"));
			$this->mail_model->setAssunto($this->input->post("assunto"));

			$this->mail_model->cadastrar();
		}

		redirect("mail");
	}
}