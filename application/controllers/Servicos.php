<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicos extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
		$id_contador = $this->uri->segment(4);

		$this->empresa_model->setId($id);
		$dados['servicos'] = $this->empresa_model->listarDadosDosServicos();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/servicos', $dados);
		$this->load->view('layout/footer');
		
	}

	public function abaSenhasServicos(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->empresa_model->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->empresa_model->listarDadosDosServicos();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/servicos', $dados);
		$this->load->view('layout/footer');
	}

	public function nota_fiscal(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
		$id_contador = $this->uri->segment(4);

		$this->empresa_model->setId($id);

		$dados['empresa'] = $this->empresa_model->pesquisar_empresa_id();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/servicos_nfe', $dados);
		$this->load->view('layout/footer');
	}

	public function relacao_dae(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
		$id_contador = $this->uri->segment(4);

		$this->empresa_model->setId($id);

		$dados['empresa'] = $this->empresa_model->pesquisar_empresa_id();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('servicos/servicos_dae', $dados);
		$this->load->view('layout/footer');
	}


	public function download(){
		$ano = $this->uri->segment(3);
		$inscricao_estadual = $this->uri->segment(4);
		$login = $this->uri->segment(5);
		$senha = $this->uri->segment(6);
		
		if($ano != ''){
			$data = array('ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__LASTFOCUS'=>'',
			'__VIEWSTATE'=>'/wEPDwUKMTYwMTM5MTE2OA9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFA0RURWQCBQ8WAh8ABS5Eb21pYyYjMjM3O2xpbyBUcmlidXQmIzIyNTtyaW8gRWxldHImIzI0NDtuaWNvZAIJD2QWAmYPZBYCAgEPZBYGAgMPDxYIHjBfX1BIQ2VudHJvX21zZ0Vycm9Mb2dpbl9WaWV3U3RhdGVBU01vZGFsVGl0dWxvSUQFEVNlc3PDo28gRW5jZXJyYWRhHjJfX1BIQ2VudHJvX21zZ0Vycm9Mb2dpbl9WaWV3U3RhdGVBU01vZGFsQ29udGV1ZG9JRGUeCENzc0NsYXNzBQptb2RhbCBmYWRlHgRfIVNCAgIWDB4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljHglkcmFnZ2FibGUFBHRydWVkAgUPZBYEAgcPFgIfAGVkAgsPDxYCHgdWaXNpYmxlZ2RkAgcPFgIfAAX9Bzx0YWJsZT4NCg0KPHRyPg0KDQogICAgPHRkPg0KICAgIA0KICAgICAgICA8ZGl2IGNsYXNzPSJjb2wxIHNwYW41Ij4NCiAgICAgICAgICAgIDxpbWcgc3JjPSIvZHRlL2R0X2VfZmluYWwucG5nIiBhbHQ9IkxvZ28gRFRFIi8+DQogICAgICAgICAgICA8YnIgLz48YnIgLz48YnIgLz4NCiAgICAgICAgPC9kaXY+DQogICAgPC90ZD4NCjwvdHI+DQo8dHI+DQogICAgPHRkPg0KICAgDQogICAgICA8ZGl2Pg0KDQogICAgICAgIDxwPkZlcnJhbWVudGEgZXh0ZXJuYSBwYXJhIGdlcmVuY2lhbWVudG8gZGEgQ29udGEgbm8gRG9taWPDrWxpbyBUcmlidXTDoXJpbyBFbGV0csO0bmljbyAtIERURTwvcD4NCiAgICANCiAgICAgICAgPGgxPg0KICAgIA0KICAgICAgICA8L2gxPg0KICAgICAgICA8aDI+SW5mb3JtYcOnw7Vlcy48L2gyPg0KICAgIA0KICAgICAgICA8cD4NCiAgICAgICAgICAgIFByb2pldGFkbyBjb20gYSBpZGVpYSBkZSBkZXNlbnZvbHZlciBkZSBmb3JtYSBtYWlzIGludHVpdGl2YSwgZWZpY2llbnRlLCDDunRpbCBlIHNpbXBsZXMuIA0KICAgICAgICAgICAgPGJyIC8+DQogICAgICAgICAgICBDb20gYSBmZXJyYW1lbnRhIHBvZGVyw6EgY2FkYXN0cmFyIHVtYSBDb250YSBEb21pY8OtbGlvIFRyaWJ1dMOhcmlvLCBFbmRlcmXDp29zIEVsZXRyw7RuaWNvcywgVGVsZWZvbmVzIENlbHVsYXJlcywgTGVyIE1lbnNhZ2VucyBSZWNlYmlkYXMgZSBWaXN1YWxpemFyIFJlbGF0w7NyaW9zLg0KICAgICAgICA8L3A+DQogICAgICAgIDx1bD4NCiAgICAgICAgICAgIDxsaT4NCiAgICAgICAgICAgICAgICA8cD5BY2Vzc2UgYSBwYXJ0aXIgZGUgcXVhbHF1ZXIgZGlzcG9zaXRpdm8gbcOzdmVsLCBuw6NvIHNlIHByZW9jdXBlIGNvbSBhIHJlc29sdcOnw6NvLCBhanVzdGFtb3MgYSB0ZWxhIHBhcmEgYSBtZWxob3IgdmlzdWFsaXphw6fDo28uPC9wPg0KICAgICAgICAgICAgPC9saT4gICAgDQogICAgICAgIDwvdWw+DQoNCiAgICAgICAgPC9kaXY+DQoNCiAgICA8L3RkPg0KPC90cj4NCg0KPC90YWJsZT5kAg0PDxYIHilfX0FTTW9kYWxfRXJyb19WaWV3U3RhdGVBU01vZGFsQ29udGV1ZG9JRGUeJ19fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVDPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoB8DBQptb2RhbCBmYWRlHwQCAhYMHwUFAi0xHwYFBmRpYWxvZx8HBQR0cnVlHwgFBWZhbHNlHwkFBnN0YXRpYx8KBQR0cnVlZAIFDxYCHwAFSFNJU1RFTUEgVFJJQlVUJiMxOTM7UklPIFNFRkFaIC0gU2VjcmV0YXJpYSBkYSBGYXplbmRhIGRvIEVzdGFkbyBkYSBCYWhpYWQCBw8WAh8ABQ9BU0xJQjogMi4xLjEwLjBkZM0/dO9U5kfaYmhWFbFTCBA17kcC',
			'__VIEWSTATEGENERATOR'=>'CC7A3876',
			'__EVENTVALIDATION'=>'/wEdAApOHJQ9+CbscBlvbuZ1e/7wIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV46LBM9yZxItbePOFI7a2VLwUBa4=',
			'ctl00$PHCentro$userLogin'=>$login,
			'ctl00$PHCentro$userPass'=>$senha,
			'ctl00$PHCentro$btnLogin'=>'Entrar'); //18430685000171 -- 97376057000121

			$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login?ReturnUrl=%2fsistemas%2fDTE%2fContribuinte%2fModulos%2fConsulta%2fImpressao_DAE.aspx%3fNumeroInscricaoEstadual%3d".$inscricao_estadual."%26Ano%3d".$ano."&NumeroInscricaoEstadual=".$inscricao_estadual."&Ano=".$ano;
			
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			
			curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);
			curl_close($ch);

			//header('Cache-Control: public'); 
			//header('Content-type: application/pdf');
			//header('Content-Disposition: attachment; filename="new.pdf"');

			echo $store;

		}
		
	}
	
}
