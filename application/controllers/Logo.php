<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logo extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('logo_model');
    }

    public function atualizarfoto(){
		
        $id = $this->uri->segment(3);
        
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->logo_model->setId($id);
		$dados['logo'] = $this->logo_model->buscar_logo();

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('usuario/alterar_foto', $dados);
		$this->load->view('layout/footer');
		
	}

	public function atualizar(){

        $id = $this->uri->segment(3); 

        if ($id != ''){
                                    
                if(isset($_POST['btn_salvar'])){
                    
                    //UPLOAD
                    $this->load->library('upload');                 
                    
                    $hostCompleto = $_SERVER['HTTP_HOST'];
                    $server = explode('.', $hostCompleto);
                    $server = $server[0];

                    $image_upload_folder = FCPATH . 'assets/arquivos/perfil/'.$server.'/';

                    if (!file_exists($image_upload_folder)) {
                        mkdir($image_upload_folder, DIR_WRITE_MODE, true);
                    }

                    $this->upload_config = array(
                        'upload_path'   => $image_upload_folder,
                        'allowed_types' => 'png|jpg|jpeg',
                        'max_size'      => 2048,
                        'max_width'     => 100,
                        'max_height'    => 100,
                        'remove_space'  => TRUE,
                        'encrypt_name'  => TRUE,
                    );

                    $this->upload->initialize($this->upload_config);
                    
                    // verificamos se o upload foi processado com sucesso
                    if ( ! $this->upload->do_upload('userfile'))
                    {
                        // em caso de erro retornamos os mesmos para uma variável
                        // e enviamos para a home
                        $data= array('error' => $this->upload->display_errors());
                        //$this->load->view('home',$data); 
                        $this->session->set_flashdata('msg_conteudo', $data['error']); 
                        $this->session->set_flashdata('msg_alerta', 4);
                    }
                    else
                    {
                        //se correu tudo bem, recuperamos os dados do arquivo
                        $data['dadosArquivo'] = $this->upload->data();
                        $arquivoPath = 'assets/arquivos/perfil/'.$server.'/'.$data['dadosArquivo']['file_name'];
                                                
                        $this->logo_model->setId($id);
                        $this->logo_model->setLogo($arquivoPath);
                        
                        $this->logo_model->cadastrar();
                        $this->session->userdata['userprimesession']['logo'] = $arquivoPath;
                        // carregamos a view com as informações e link para download
                        $this->session->set_flashdata('msg_alerta', 1); 
                    }
                    
                }
                
                redirect("logo/atualizarfoto/1");
    
        } else {
            redirect("logo/atualizarfoto/1");
        }
    
    }
	
}
