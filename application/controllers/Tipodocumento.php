<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipodocumento extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
		$this->load->model('empresa_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('tipodocumento_model', 'e');		
    }

	// VALIDA SE AS VARIÁVEIS ESTÃO VAZIAS.
	public function validar_input($variavel){
		if($variavel){
			return $variavel;
		} else {
			return NULL;
		}
	}
	
	// VALIDA SE O CONTADOR É O RESPONSÁVEL PELA EMPRESA
	public function validar_usuario($id_contador){
		if($id_contador == $this->session->userdata['userprimesession']['id']){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function cadastrar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		if(isset($_POST['btn_cadastrar'])){

			$this->e->setAtivo($this->validar_input($this->input->post("ativo")));
			$this->e->setNome($this->validar_input($this->input->post("nome")));
			$this->e->setDescricao($this->validar_input($this->input->post("descricao")));
			$this->e->setUrlConsulta($this->validar_input($this->input->post("url_consulta")));
			$this->e->setOrgaoExpedidor($this->validar_input($this->input->post("orgao_expedidor")));
			$this->e->setDiasNotificacao($this->validar_input($this->input->post("diasNotificacao")));
			$this->e->setIdContabilidade($this->session->userdata['userprimesession']['id']);
				
			$result = $this->e->cadastrar();
				
			if ($result){
			
				$this->session->set_flashdata('msg_alerta', 1);
				redirect('tipodocumento/listar');
			} else {
				$this->session->set_flashdata('msg_titulo','ERRO!');
				$this->session->set_flashdata('msg_conteudo','Não foi possível cadastrar.');
				$this->session->set_flashdata('msg_tipo','danger');
			}

		}
				
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('tipodocumento/tipodocumento_cadastrar');
		$this->load->view('layout/footer');
	
	}
	
	public function listar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$this->e->setIdContabilidade($this->session->userdata['userprimesession']['id']);
		$dados['consulta'] = $this->e->listar();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('tipodocumento/tipodocumento_listar', $dados);
		$this->load->view('layout/footer');
		
	}
	
	public function excluir() {
	
		$id = $this->uri->segment(3);
		
		$this->e->setId($id);
		$result = $this->e->excluir();

		if ($result){

			$this->session->set_flashdata('msg_titulo','SUCESSO!');
			$this->session->set_flashdata('msg_conteudo','O tipo de documento foi excluído.');
			$this->session->set_flashdata('msg_tipo','success');

			redirect('tipodocumento/listar');

		} else {
			$this->session->set_flashdata('msg_titulo','ERRO!');
			$this->session->set_flashdata('msg_conteudo','Não foi possível excluir.');
			$this->session->set_flashdata('msg_tipo','danger');

			redirect('tipodocumento/listar');

		}
	
	}

	public function editar(){
		
		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$id = $this->uri->segment(3);
	
		if ($id != ''){			
				
				if(isset($_POST['btn_salvar'])){
				
					$this->e->setId($id);
					$this->e->setAtivo($this->validar_input($this->input->post("ativo")));
					$this->e->setNome($this->validar_input($this->input->post("nome")));
					$this->e->setDescricao($this->validar_input($this->input->post("descricao")));
					$this->e->setOrgaoExpedidor($this->validar_input($this->input->post("orgao_expedidor")));
					$this->e->setDiasNotificacao($this->validar_input($this->input->post("diasNotificacao")));
					$this->e->setUrlConsulta($this->validar_input($this->input->post("url_consulta")));
				
					$result = $this->e->editar();
						
					if ($result){
						
						$this->session->set_flashdata('msg_alerta', 2);
						redirect('tipodocumento/listar');
							
					} else {
						$dados['msg_titulo'] = 'ERRO!';
						$dados['msg_conteudo'] = 'Não foi possível editar.';
						$dados['msg_tipo'] = 'danger';
					}
				
				}
					
				$this->e->setId($id);
				$dados['tipodocumento'] = $this->e->pesquisar_tipodocumento_id();
					
				$dados['id'] = $id;
					
				$this->load->view('layout/head');
				$this->load->view('layout/sidebar');
				$this->load->view('layout/header', $dados);
				$this->load->view('tipodocumento/tipodocumento_editar', $dados);
				$this->load->view('layout/footer');
		
				
		} else {
			redirect("tipodocumento/listar");
		}
	
	}
	
}
