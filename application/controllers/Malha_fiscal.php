<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Malha_fiscal extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('malha_fiscal_model');
       	$this->load->model('ecac_model');
       	$this->load->model('empresa_model');
    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";
		$titulo = "Todas";

		if($cnpj != "REGULARIZADO" && $cnpj != "ANALISANDO" && $cnpj != "TODAS" && $cnpj != "PENDENCIAS" && $cnpj != "CLIENTE"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "TODAS";
		}else{
			if($cnpj == "PENDENCIAS"){
				$titulo = "Pendências";
			}else if($cnpj == "REGULARIZADO"){
				$titulo = "Regulares";
			}else if($cnpj == "ANALISANDO"){
				$titulo = "Analisando";
			}

			$filtro = $cnpj;
		}

		$this->malha_fiscal_model->setFiltro($filtro);

		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		$dados['titulo'] = $titulo;

		$resultado = $this->malha_fiscal_model->listar();

		foreach ($resultado as $r) {
			$possui_certificado = false;
			$certificado = $this->ecac_model->find_certificado($r->cnpj);
			if( $certificado ){
				$possui_certificado = true;
			}
			$r->possui_certificado = $possui_certificado;

			
			$lida_por = $this->malha_fiscal_model->buscar_lida_por($r->cnpj, $r->assunto, $r->data);
			if(isset($lida_por)){
				$r->lida_por = "Lida por: ".$lida_por->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($lida_por->data_alteracao));
			}

			$r->ultimo_historico = $this->montaHistorico($r->id);
			
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
		$dados['consulta'] = $resultado;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('malha_fiscal/malha_fiscal', $dados);
		$this->load->view('layout/footer');
		
	}

	public function find_info_for_modal(){
		$id = $this->uri->segment(3);

		$resultado = $this->malha_fiscal_model->find_info_for_modal($id);

		echo json_encode($resultado);
	}


	public function montaHistorico($id){
		$historico = $this->malha_fiscal_model->find_ultimo_historico($id);

		if(isset($historico)){
			return "Alterado por: ".$historico->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($historico->data_alteracao));
		}else{
			return "";
		}

	}

	public function atualiza_situacao(){
		$id = $this->input->post("id");
		$situacao = $this->input->post("situacao");
		$cnpj = $this->input->post("cnpj");
		$descricao = $this->input->post("descricao");

		$this->malha_fiscal_model->atualiza_situacao($id, $situacao, $cnpj, $descricao);

		redirect('malha_fiscal/listar');
	}


	public function buscar_historico(){
		$id = $this->input->post("id");

		$resultado = $this->malha_fiscal_model->buscar_historico($id);

		foreach ($resultado as $r) {
			$r->data = date('d/m/Y H:i:s', strtotime($r->data_alteracao));
		}

		echo json_encode($resultado);

	}


	public function buscar_mensagem(){
		$id = $this->input->post("id");

		$resultado = $this->malha_fiscal_model->find_info_for_modal($id);

		$mensagem_ecac = $this->malha_fiscal_model->buscar_mensagem_ecac($resultado);
		
		echo json_encode($mensagem_ecac);
	}


}