<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Ecac extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('ecac_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('empresa_model');
		$this->load->model('Calendario_model', 'e');	
		$this->load->model('mensagens_model');		
    }

	public function listar_mensagens(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		if(isset($_GET['filtro'])){
			$filtro = $_GET["filtro"];
		}else{
			$filtro = 'TODAS';
		}
		
		$this->ecac_model->setFiltro_situacao($filtro);

		$dados = $this->notificacoes();
		$resultado = $this->ecac_model->listar_caixa_postal();
		
		$somaMsgNaoLidas = 0;
		$qtdRegistros = 0;

		foreach ($resultado as $value) {
			$somaMsgNaoLidas = $somaMsgNaoLidas + $value->nao_lidas;
			$qtdRegistros++;

			if($filtro == "IMPORTANTES"){
				$value->nao_lidas = $this->ecac_model->qtd_caixa_postal_importante_by_id($value->id_caixa_postal)->qtd;
			}
		}

		$dados['consulta'] = $resultado;
		$dados['msg_nao_lidas'] = $somaMsgNaoLidas;
		$dados['qtd_registros'] = $qtdRegistros;

		if($filtro == "IMPORTANTES"){
			$dados['msg_nao_lidas'] = $this->ecac_model->qtd_caixa_postal_importante()->qtd;
		}
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		
		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/mensagens', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_mensagens_by_empresa(){
		$id_caixa_postal = $this->uri->segment(3);
		$cnpj = $this->uri->segment(4);
		$possui_certificado = false;
		$certificado = $this->ecac_model->find_certificado($cnpj);
		if( $certificado ){
			$possui_certificado = true;
		}

		$resultado = $this->ecac_model->listar_caixa_postal_by_empresa($id_caixa_postal);
		foreach ($resultado as $r) {
			$lida_por = $this->mensagens_model->buscar_lida_por($id_caixa_postal, $cnpj, $r->assunto, $r->recebida_em, $r->id_mensagem);
			if(isset($lida_por)){
				$r->lida_por = "Lida por: ".$lida_por->nome_usuario."<br>Data: ".date('d/m/Y H:i:s', strtotime($lida_por->data_alteracao));
			}
			
		}
		
		echo json_encode(array("error"=> false, "table"=>$resultado, "possui_certificado"=>$possui_certificado));
	}

	public function ler_mensagem(){
		$id_caixa_postal = $this->uri->segment(3);
		$resultado = $this->ecac_model->conteudo_mensagem($id_caixa_postal)->conteudo;
		echo json_encode(array("error"=> false, "body"=>$resultado));
	}

	public function ler_mensagem_ecac_antigo(){
		$caixa_postal_id = $this->uri->segment(3);
		$id_mensagem = $this->uri->segment(4);

		$id_mensagem_caixa = $this->ecac_model->find_mensagem($id_mensagem)->id_mensagem;
		$cnpj = $this->ecac_model->find_cnpj($caixa_postal_id)->cnpj_data;

		$certificado = $this->ecac_model->find_certificado($cnpj);

		$params = array('caminho_certificado' => 'https://veri-sp.com.br/crons-api/'.str_replace('//','/', $certificado->caminho_arq ) ,
			'cerficado_senha' => $certificado->pass,
			'caminho_da_pasta_pdfs' => '');
		$this->load->library('Ecac_robo_library', $params);

		if(!$this->ecac_robo_library->acesso_valido()){
			unset($this->ecac_robo_library);
			echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
		}

		$mensagem = $this->ecac_robo_library->buscar_conteudo_mensagem($id_mensagem_caixa);
		if($mensagem == ""){
			echo json_encode(array("error"=> true, "mensagem"=>'erro12'));
		}else{

			$this->ecac_model->update_lida($id_mensagem, $mensagem);
			echo json_encode(array("error"=> false, "mensagem"=>$mensagem));
		}

		unset($this->ecac_robo_library);

	}

	public function ler_mensagem_ecac(){
		$caixa_postal_id = $this->uri->segment(3);
		$id_mensagem = $this->uri->segment(4);

		$id_mensagem_caixa = $this->ecac_model->find_mensagem($id_mensagem)->id_mensagem;
		$cnpj = $this->ecac_model->find_cnpj($caixa_postal_id)->cnpj_data;
        $mensagem = "";
		$certificado = $this->ecac_model->find_certificado($cnpj);

        if( $certificado ){

            $params = array('caminho_certificado' => 'https://veri-sp.com.br/crons-api/'.str_replace('//','/', $certificado->caminho_arq ) ,
                'cerficado_senha' => $certificado->pass,
                'caminho_da_pasta_pdfs' => '');
            $this->load->library('Ecac_robo_library', $params);

            if(!$this->ecac_robo_library->acesso_valido()){
                unset($this->ecac_robo_library);
                echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
            }
            $mensagem = $this->ecac_robo_library->buscar_conteudo_mensagem($id_mensagem_caixa);

        }
        // else{

//           Caso nao encontre certificado tenta por procuração

            // $this->load->model('certificadocontador_model');
            // $this->load->model('contadorprocuracao_model');

            // $cerficados = $this->certificadocontador_model->get();

            // foreach ($cerficados as $cerficado_contador) {

            //     $params = array('caminho_certificado' => 'https://veri.com.br/crons-api/'.str_replace('//','/', $cerficado_contador->caminho_arq),
            //         'cerficado_senha' => $cerficado_contador->pass,
            //         'caminho_da_pasta_pdfs' => '');

            //     $this->load->library('Ecac_robo_library_procuracao', $params);

                /**
                 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
                 */
        //         if (!$this->ecac_robo_library_procuracao->acesso_valido()) {
        //             unset($this->ecac_robo_library_procuracao);
        //             continue;
        //         }

        //         $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj);

        //         if( $empresa ){
                    
        //             $validado = $this->ecac_robo_library_procuracao->trocar_perfil($cnpj);
                    
        //             if(! $validado){
        //                 echo "erro13";
        //                 continue;
        //             }

        //             $mensagem = $this->ecac_robo_library_procuracao->buscar_conteudo_mensagem($id_mensagem_caixa);

        //             break;
        //         }
        //     }
        // }

		if($mensagem == ""){

			$this->load->model('certificadocontador_model');
	        $this->load->model('contadorprocuracao_model');

	        $cerficados = $this->certificadocontador_model->get();

	        foreach ($cerficados as $cerficado_contador) {

	            $params = array('caminho_certificado' => 'https://veri-sp.com.br/crons-api/'.str_replace('//','/', $cerficado_contador->caminho_arq),
	                'cerficado_senha' => $cerficado_contador->pass,
	                'caminho_da_pasta_pdfs' => '');

	            $this->load->library('Ecac_robo_library_procuracao', $params);

	            /**
	             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
	             */
	            if (!$this->ecac_robo_library_procuracao->acesso_valido()) {
	                unset($this->ecac_robo_library_procuracao);
	                continue;
	            }

	            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj);

	            if( $empresa ){
	                
	                $validado = $this->ecac_robo_library_procuracao->trocar_perfil($cnpj);
	                
	                if(! $validado){
	                    echo "erro13";
	                    continue;
	                }

	                $mensagem = $this->ecac_robo_library_procuracao->buscar_conteudo_mensagem($id_mensagem_caixa);

	                break;
	            }
	        }

			if($mensagem == ""){
				echo json_encode(array("error"=> true, "mensagem"=>'erro12'));
			}else{

				$this->ecac_model->update_lida($id_mensagem, $mensagem);
				$this->ecac_model->update_lida_caixa_postal($caixa_postal_id);
				echo json_encode(array("error"=> false, "mensagem"=>$mensagem));
			}

			//echo json_encode(array("error"=> true, "mensagem"=>'erro12'));
		}else{

			$this->ecac_model->update_lida($id_mensagem, $mensagem);
			$this->ecac_model->update_lida_caixa_postal($caixa_postal_id);
			echo json_encode(array("error"=> false, "mensagem"=>$mensagem));
		}

		unset($this->ecac_robo_library);

	}

	public function ler_mensagem_ecac_proc(){
		$caixa_postal_id = $this->uri->segment(3);
		$id_mensagem = $this->uri->segment(4);

		$id_mensagem_caixa = $this->ecac_model->find_mensagem($id_mensagem)->id_mensagem;
		$cnpj = $this->ecac_model->find_cnpj($caixa_postal_id)->cnpj_data;

		$id_empresa = $this->empresa_model->find_empresa_by_cnpj($cnpj)->id;
        $mensagem = "";
		
		//           Caso nao encontre certificado tenta por procuração

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        $cerficados = $this->certificadocontador_model->get_aux($id_empresa);

        foreach ($cerficados as $cerficado_contador) {

            $params = array('caminho_certificado' => 'http://veri-sp.com.br/crons-api/'.str_replace('//','/', $cerficado_contador->caminho_arq),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => '');

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()) {
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj);

            if( $empresa ){
                
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($cnpj);
                
                if(! $validado){
                    echo "erro13";
                    unset($this->ecac_robo_library_eprocessos_procuracao);
                    continue;
                }

                $mensagem = $this->ecac_robo_library_eprocessos_procuracao->buscar_conteudo_mensagem($id_mensagem_caixa);

                if($mensagem != ""){
                	break;
                }
                
            }
        }

		if($mensagem == ""){
			echo json_encode(array("error"=> true, "mensagem"=>'erro12'));
		}else{

			$this->ecac_model->update_lida($id_mensagem, $mensagem);
			$this->ecac_model->update_lida_caixa_postal($caixa_postal_id);
			echo json_encode(array("error"=> false, "mensagem"=>$mensagem));
		}

		unset($this->ecac_robo_library_eprocessos_procuracao);

	}

	public function listar_parcelamento(){

		$dados = $this->notificacoes();

		$dados['consulta'] = $this->ecac_model->listar_empresas_parcelamento_simples();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_com_parcelamento_simples', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_situacao_fiscal_pendencia(){
		
		$dados = $this->notificacoes();

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_empresas_situacao_fiscal_pendecia();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_situacao_fiscal', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_situacao_fiscal_regular(){
		
		$dados = $this->notificacoes();

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_empresas_situacao_fiscal_regular();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_situacao_fiscal', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_situacao_fiscal_geral(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		$dados = $this->notificacoes();

		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_empresas_situacao_fiscal_geral();  
		$dados['razao_social_filtro'] = $razao;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_situacao_fiscal', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_empresas_sem_certificado(){

		$dados = $this->notificacoes();

		$dados['consulta'] = $this->empresa_model->listar_empresas_sem_certificado();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_sem_certificado', $dados);
		$this->load->view('layout/footer');

	}

	public function marcar_pendencia(){

		$id = $this->uri->segment(3);
		$result = $this->ecac_model->marcar_pendecia($id);

		if ($result){
		
			$this->session->set_flashdata('msg_alerta', 5);
			redirect('ecac/listar_situacao_fiscal_regular');
				
		} else {

			$this->session->set_flashdata('msg_alerta', 6);
			redirect('ecac/listar_situacao_fiscal_regular');
				
		} 
	}

	public function listar_empresas_com_certificado(){

		$dados = $this->notificacoes();

		$dados['consulta'] = $this->empresa_model->listar_empresas_com_certificado();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('consultas/consulta_empresas_com_certificado', $dados);
		$this->load->view('layout/footer');

	}

	public function listar_cadin(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		$dados = $this->notificacoes();

		$dados['nome_tela'] = 'CADIN';
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_empresas_cadin();  
		$dados['razao_social_filtro'] = $razao;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_cadin', $dados);
		$this->load->view('layout/footer');

	}

	public function listar_cadin_pendencia(){
		
		$dados = $this->notificacoes();

		$dados['nome_tela'] = 'Empresas com Pendências no CADIN';
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_empresas_cadin_pendencias();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_cadin', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_cadin_regular(){
		
		$dados = $this->notificacoes();

		$dados['nome_tela'] = 'Empresas Regulares no CADIN';
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_empresas_cadin_regulares();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_cadin', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_eprocessos_ativos(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		$dados = $this->notificacoes();

		$dados['nome_tela'] = 'e-Processos - Ativos';
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_eprocessos_ativos();  
		$dados['razao_social_filtro'] = $razao;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_processos', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_eprocessos_inativos(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		$dados = $this->notificacoes();

		$dados['nome_tela'] = 'e-Processos - Inativos';
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_eprocessos_inativos();  
		$dados['razao_social_filtro'] = $razao;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_processos', $dados);
		$this->load->view('layout/footer');
	}

	public function listar_eprocessos_todos(){
		
		$dados = $this->notificacoes();

		$dados['nome_tela'] = 'e-Processos - Todos';
		$dados['filtro'] = $this->uri->segment(2);

		$dados['consulta'] = $this->ecac_model->listar_eprocessos_todos();  

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('ecac/listar_empresas_processos', $dados);
		$this->load->view('layout/footer');
	}

	public function consulta_historico_eprocesso(){
		$id_processo = $this->input->post("id_processo");
		$ativo = $this->input->post("ativo");

		if($ativo == 1){
			$resultado = $this->ecac_model->listar_historico_processo_ativo($id_processo);
		}else{
			$resultado = $this->ecac_model->listar_historico_processo_inativo($id_processo);
		}

		echo json_encode($resultado);
		
	}

}