<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certidao_negativa extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('certidao_negativa_model');
		$this->load->model('ecac_model');
    }

    public function dashboard(){
    	//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$this->certidao_negativa_model->setFiltro('Irregulares');

		//irregulares
		$dados['qtd_estadual_irregular'] = $this->certidao_negativa_model->qtd_estadual_irregular()->qtd;
		$dados['qtd_municipal_irregular'] = $this->certidao_negativa_model->qtd_municipal_irregular()->qtd;
		$dados['qtd_pgfn_irregular'] = $this->certidao_negativa_model->qtd_federal_irregular()->qtd;
		$dados['qtd_empresas_situacao_fiscal_pendencias'] = $this->ecac_model->qtd_empresas_situacao_fiscal_pendencias();

		$dados['qtd_fgts_irregular'] = $this->certidao_negativa_model->qtd_caixa_irregular()->qtd;
		$dados['qtd_trabalhista_irregular'] = $this->certidao_negativa_model->qtd_trabalhista_irregular()->qtd;
		$dados['qtd_previdenciaria_irregular'] = $this->certidao_negativa_model->qtd_previdenciaria_irregular()->qtd;

		//regulares
		$this->certidao_negativa_model->setFiltro('Regulares');
		$dados['qtd_estadual_regular'] = $this->certidao_negativa_model->qtd_estadual_irregular()->qtd;
		$dados['qtd_municipal_regular'] = $this->certidao_negativa_model->qtd_municipal_irregular()->qtd;
		$dados['qtd_pgfn_regular'] = $this->certidao_negativa_model->qtd_federal_irregular()->qtd;

		$dados['qtd_fgts_regular'] = $this->certidao_negativa_model->qtd_caixa_irregular()->qtd;
		$dados['qtd_trabalhista_regular'] = $this->certidao_negativa_model->qtd_trabalhista_irregular()->qtd;
		$dados['qtd_previdenciaria_regular'] = $this->certidao_negativa_model->qtd_previdenciaria_irregular()->qtd;		

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/dashboard');
		$this->load->view('layout/footer');

    }

	public function listar(){

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_negativa_listar');
		$this->load->view('layout/footer');
		
	}

	public function listar_municipal(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";

		if($cnpj != "Regulares" && $cnpj != "Irregulares"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "Todas";
		}else{
			$filtro = $cnpj;
		}

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_debito_municipal');
		$this->load->view('layout/footer');
		
	}

	public function listarAjax(){
		$filtro = $this->input->post('filtro');
    	$filtro_uf = $this->input->post('filtro_uf');
		$filtro_cidade = $this->input->post('filtro_cidade');

    	$this->certidao_negativa_model->setFiltro($filtro);
    	$this->certidao_negativa_model->setFiltro_uf($filtro_uf);
    	$this->certidao_negativa_model->setFiltro_cidade($filtro_cidade);
    	
		$certidoes_municipais = $this->certidao_negativa_model->listar_municipal();

		date_default_timezone_set('America/Sao_Paulo');

		foreach ($certidoes_municipais as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d'));
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }

		echo json_encode($certidoes_municipais);
	}

	public function listar_estadual(){

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";

		if($cnpj != "Regulares" && $cnpj != "Irregulares"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "Todas";
		}else{
			$filtro = $cnpj;
		}

		// if($cnpj != null){
		// 	$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
		// 	$razao = $em->razao_social;
		// }

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_debito_estadual');
		$this->load->view('layout/footer');
		
	}

	public function listarEstadualAjax(){
		$filtro = $this->input->post('filtro');
    	$filtro_uf = $this->input->post('filtro_uf');

    	$this->certidao_negativa_model->setFiltro($filtro);
    	$this->certidao_negativa_model->setFiltro_uf($filtro_uf);

        $certidoes_estaduais = $this->certidao_negativa_model->listar_estadual();

        date_default_timezone_set('America/Sao_Paulo');
        foreach ($certidoes_estaduais as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d'));
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }

        echo json_encode($certidoes_estaduais);
    }

    public function listar_federal_pgfn(){

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";

		if($cnpj != "Regulares" && $cnpj != "Irregulares"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "Todas";
		}else{
			$filtro = $cnpj;
		}

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;

		$hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];

        if($server == "demo"){
        	$dados['server_name'] = 'demo';
        }else{
        	$dados['server_name'] = 'teste';
        }

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_debito_pgfn');
		$this->load->view('layout/footer');
		
	}

    public function listarFederalAjax(){
    	$filtro = $this->input->post('filtro');
    	$this->certidao_negativa_model->setFiltro($filtro);

        $certidoes_federais = $this->certidao_negativa_model->listar_federal();
        date_default_timezone_set('America/Sao_Paulo');

        foreach ($certidoes_federais as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d'));
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }

        echo json_encode($certidoes_federais);
    }


    public function busca_situacao_fiscal(){
    	$cnpj = $this->input->post('cnpj');

    	$pdf = $this->certidao_negativa_model->situacao_fiscal_empresa($cnpj)->pdf;
    	echo $pdf;
    }


    public function listar_caixa(){

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";

		if($cnpj != "Regulares" && $cnpj != "Irregulares"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "Todas";
		}else{
			$filtro = $cnpj;
		}

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_caixa');
		$this->load->view('layout/footer');
		
	}

    public function listarCaixaAjax(){
    	$filtro = $this->input->post('filtro');
    	$this->certidao_negativa_model->setFiltro($filtro);

    	date_default_timezone_set('America/Sao_Paulo');

        $certidoes = $this->certidao_negativa_model->listar_caixa();
        foreach ($certidoes as $c) {
           $data_original = new DateTime(date($c->data_execucao));

           $data_inicial_aux = new DateTime(date('Y-m-d')); 
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }
        
        echo json_encode($certidoes);
    }


    public function listar_trabalhista(){

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";

		if($cnpj != "Regulares" && $cnpj != "Irregulares"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "Todas";
		}else{
			$filtro = $cnpj;
		}

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_trabalhista');
		$this->load->view('layout/footer');
		
	}

    public function listarTrabalhistaAjax(){
    	$filtro = $this->input->post('filtro');
    	$this->certidao_negativa_model->setFiltro($filtro);

        $certidoes = $this->certidao_negativa_model->listar_trabalhista();

        date_default_timezone_set('America/Sao_Paulo');

        foreach ($certidoes as $c) {
           $data_original = new DateTime(date($c->data_execucao)); 

           $data_inicial_aux = new DateTime(date('Y-m-d')); 
           $data_final = new DateTime(date('Y-m-d', strtotime($c->data_execucao. ' + 30 day')));
            
           $c->data_execucao_novo = date("d/m/Y", strtotime($c->data_execucao));
           $c->proxima_execucao = date('d/m/Y', strtotime($c->data_execucao. ' + 30 day'));

           $intervalo = $data_final->diff($data_inicial_aux);
           $c->intervalo = $intervalo->format('%a');
        }
        
        echo json_encode($certidoes);
    }

    public function listar_previdenciaria(){

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";

		if($cnpj != "Regulares" && $cnpj != "Irregulares"){
			if($cnpj != null){
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "Todas";
		}else{
			$filtro = $cnpj;
		}

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('certidao/certidao_previdenciaria');
		$this->load->view('layout/footer');
		
	}

    public function listarPrevidenciariaAjax(){
    	$filtro = $this->input->post('filtro');
    	$this->certidao_negativa_model->setFiltro($filtro);

        $certidoes = $this->certidao_negativa_model->listar_previdenciaria();
        echo json_encode($certidoes);
    }

    public function adicionar_observacao_federal(){

    	$id = $this->input->post("id"); 

        $this->certidao_negativa_model->setId($id); 

        if(isset($_POST['btn_salvar'])){

			$this->certidao_negativa_model->setObservacao($this->input->post("observacao")); 

            $result = $this->certidao_negativa_model->adicionar_observacao_federal();
                
            if ($result){

                $this->session->set_flashdata('msg_alerta', 1);
                redirect('certidao_negativa/listar_federal_pgfn');

            } else {

                $this->session->set_flashdata('msg_alerta', 2);
                redirect('certidao_negativa/listar_federal_pgfn');
            }
        }  

    }

    public function adicionar_observacao_caixa(){

    	$id = $this->input->post("id"); 

        $this->certidao_negativa_model->setId($id); 

        if(isset($_POST['btn_salvar'])){

			$this->certidao_negativa_model->setObservacao($this->input->post("observacao")); 

            $result = $this->certidao_negativa_model->adicionar_observacao_caixa();
                
            if ($result){

                $this->session->set_flashdata('msg_alerta', 1);
                redirect('certidao_negativa/listar_caixa');

            } else {

                $this->session->set_flashdata('msg_alerta', 2);
                redirect('certidao_negativa/listar_caixa');
            }
        }  

    }

    public function adicionar_observacao_trabalhista(){

    	$id = $this->input->post("id"); 

        $this->certidao_negativa_model->setId($id); 

        if(isset($_POST['btn_salvar'])){

			$this->certidao_negativa_model->setObservacao($this->input->post("observacao")); 

            $result = $this->certidao_negativa_model->adicionar_observacao_trabalhista();
                
            if ($result){

                $this->session->set_flashdata('msg_alerta', 1);
                redirect('certidao_negativa/listar_trabalhista');

            } else {

                $this->session->set_flashdata('msg_alerta', 2);
                redirect('certidao_negativa/listar_trabalhista');
            }
        }  

    }

    public function adicionar_observacao_previdenciaria(){

    	$id = $this->input->post("id"); 

        $this->certidao_negativa_model->setId($id); 

        if(isset($_POST['btn_salvar'])){

			$this->certidao_negativa_model->setObservacao($this->input->post("observacao")); 

            $result = $this->certidao_negativa_model->adicionar_observacao_previdenciaria();
                
            if ($result){

                $this->session->set_flashdata('msg_alerta', 1);
                redirect('certidao_negativa/listar_previdenciaria');

            } else {

                $this->session->set_flashdata('msg_alerta', 2);
                redirect('certidao_negativa/listar_previdenciaria');
            }
        }  

    }

}