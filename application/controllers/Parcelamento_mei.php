<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Parcelamento_mei extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('parcelamento_mei_model');
		$this->load->model('empresa_model');
	}

	public function listar()
	{
		date_default_timezone_set('America/Sao_Paulo');

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 

		$cnpj = $this->uri->segment(3);
		$razao = "";
		$filtro = "";

		if ($cnpj != "EM_PARCELAMENTO" && $cnpj != "ENCERRADO_RECISAO" && $cnpj != "TODAS" && $cnpj != "ENCERRADO_CONTRIBUINTE" && $cnpj != "PARCELAS_ATRASO" && $cnpj != "PARCELAS_SEM_ATRASO" && $cnpj != "PAGO_MES" && $cnpj != "PAGO_MES_NAO" && $cnpj != "CONCLUIDO") {
			if ($cnpj != null) {
				$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
				$razao = $em->razao_social;
			}
			$filtro = "TODAS";
		} else {
			$filtro = $cnpj;
		}

		$this->parcelamento_mei_model->setFiltro("TODAS");

		$dados['razao_social_filtro'] = $razao;
		$dados['filtro'] = $filtro;

		$pedidos = $this->parcelamento_mei_model->listar_pedidos();

		$aux_pedidos = [];
		foreach ($pedidos as $pedido) {
			//FILTRO
			if ($filtro == "TODAS") {
				array_push($aux_pedidos, $pedido);
			} else if ($filtro == "EM_PARCELAMENTO" && $pedido->situacao == "Em parcelamento") {
				array_push($aux_pedidos, $pedido);
			} else if ($filtro == "CONCLUIDO" && $pedido->situacao == "Concluído") {
				array_push($aux_pedidos, $pedido);
			}

			//BUSCAR NUMERO E MÊS PARCELA
			$this->status_parcela($pedido);

			//BUSCAR PARCELAS EM ATRASO
			$pedido->em_atraso = $this->parcelamento_mei_model->listar_parcelas_em_atraso($pedido->cnpj);
		}

		$dados['pedidos'] = $aux_pedidos;


		$hostCompleto = $_SERVER['HTTP_HOST'];
		$server = explode('.', $hostCompleto);
		$server = $server[0];

		$dados['banco'] = $server;

		$dia_atual = date('j');
		$mes_atual = date('m');
		$ano_atual = date('Y');

		if ($dia_atual >= 1 && $dia_atual <= 10) {
			$dados['pode_gerar_parcela_atual'] = false;
		} else {
			$dados['pode_gerar_parcela_atual'] = true;
		}

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('parcelamento/parcelamento_mei', $dados);
		$this->load->view('layout/footer');
	}

	public function status_parcela($pedido)
	{
		date_default_timezone_set('America/Sao_Paulo');
		$mes_atual = date('m');
		$ano_atual = date('Y');

		$data_pedido = $pedido->data_pedido;

		$array = explode("/", $data_pedido);

		$data_final = '01' . "/" . $array[1] . "/" . $array[2];


		$data_parcelamento1 = str_replace('/', '-', $data_final);
		$data_parcelamento = date('Y-m-d', strtotime($data_parcelamento1));

		$data_atual = date('Y-m-d');

		$data1 = new DateTime($data_parcelamento);
		$data2 = new DateTime($data_atual);

		//Calcula o numero da parcela atual 1º, 2º....
		$intervalo = $data2->diff($data1);
		$pedido->numero_parcela = $intervalo->m + ($intervalo->y * 12) + 1;

		$pedido->mes_parcela = $mes_atual . "/" . $ano_atual;

		//Verifica se a parcela atual já foi paga
		$pagou_parcela_atual = $this->parcelamento_mei_model->pagou_parcela_mes_atual($pedido->mes_parcela, $pedido->id);
		if (isset($pagou_parcela_atual)) {
			$pedido->pagou_parcela_atual = true;
			$pedido->detalhes_pagamento = "Valor Pago: " . $pagou_parcela_atual->valor_pago . "<br>" . "Data de arrecadação: " . $pagou_parcela_atual->data_arrecadacao;
		} else {
			$pedido->pagou_parcela_atual = false;
			$pedido->parcela_atual = $this->parcelamento_mei_model->get_parcela_atual($pedido->mes_parcela, $pedido->cnpj);
			$pedido->detalhes_pagamento = "Não Pago";
		}

		//Verifica se todas as parcelas já foram pagas
		$qtd_parcelas_pagas = $this->parcelamento_mei_model->qtd_pagamentos($pedido->id);
		if ($qtd_parcelas_pagas->qtd >= $pedido->qtd_parcelas) {
			$pedido->situacao = "Concluído";
		}
	}

	public function listar_detalhes()
	{

		$id = $this->uri->segment(3);

		//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
		$dados['pedido'] = $this->parcelamento_mei_model->listar_pedidos_individual($id);

		$dados['consulta'] = $this->parcelamento_mei_model->listar_debitos($id);

		$dados['pagamentos'] = $this->parcelamento_mei_model->listar_pagamentos($id);

		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('parcelamento/parcelamento_mei_detalhes', $dados);
		$this->load->view('layout/footer');
	}

	public function find_parcelas_for_modal()
	{
		$cnpj = $this->input->post('cnpj');
		
		$parcelas = $this->parcelamento_mei_model->listar_parcelas_em_atraso($cnpj);

		echo json_encode($parcelas);
	}
}
