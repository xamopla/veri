<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);

class Api extends MY_Controller {
	
	public function __construct() {
        parent::__construct();	
        $this->load->library('form_validation');
    }

    public function listar(){
    	//--------------- NOTIFICAÇÕES ---------------------
		$dados = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ----------------

		$dados['filtro'] = $this->uri->segment(2);
		
		$this->load->view('layout/head_especial'); 
		$this->load->view('api/api_test2');
		$this->load->view('layout/footer');
    }

	public function cadastrar_empresa(){

		if($_SERVER['REQUEST_METHOD'] == 'POST') { 

			$empresa_servico = $this->input->post('empresa_servico');
			$cnpj = $this->input->post('cnpj');
			$inscricao_estadual = $this->input->post('inscricao_estadual');

			if(isset($empresa_servico)){
				if($empresa_servico){

					$data = array(
						'cnpj' => $cnpj
					);

					$array = $this->inserir_empresa_servico($data);
				}else{
					$data = array(
						'cnpj' => $cnpj,
						'inscricao_estadual' =>	$inscricao_estadual
					);
					
					$array = $this->inserir_empresa_comercio($data);
				}
			}else{
				$array = array(
					'error'	=> true,
					'mensagem' => 'O campo empresa_servico é obrigatório'
				);
			}
		}else{
			$array = array(
				'error'	=> true,
				'mensagem' => 'O método de envio deve ser POST'
			);
		}

		echo json_encode($array);
	}


	public function inserir_empresa_servico($data){
		$cnpj = $data['cnpj'];
		if($cnpj != ""){

		}else{
			$array = array(
				'error'	=> true,
				'mensagem' => 'O campo cnpj é obrigatório'
			);

			return $array;
		}
	}

	public function inserir_empresa_comercio($data){
		$cnpj = $data['cnpj'];
		$inscricao_estadual = $data['inscricao_estadual'];

		if($cnpj != "" || $inscricao_estadual != ""){

		}else{
			$array = array(
				'error'	=> true,
				'mensagem' => 'O campo cnpj é obrigatório'
			);

			return $array;
		}
	}

	public function criar_db(){

		$qtd_usuarios = $this->input->post('qtd_usuarios');
		$qtd_empresas = $this->input->post('qtd_empresas');
		$nome_do_plano = $this->input->post('nome_do_plano');
		$data_validade = $this->input->post('data_validade');
		$banco = $this->input->post('nome_do_banco');

		$servername = "localhost";
		$username = "root";
		$password = "";

		$array = array(
			'error'	=> true,
			'mensagem' => 'Erro ao criar o banco'
		);

		// Create connection
		$conn = new mysqli($servername, $username, $password);
		// Check connection
		if ($conn->connect_error) {
			$array = array(
				'error'	=> true,
				'mensagem' => 'Erro ao criar o banco'
			);
			echo json_encode($array);
		}else{
			$sql = "CREATE DATABASE ".$banco;
			if ($conn->query($sql) === TRUE) {
			  	$sql = "USE ".$banco;  
			  	if($conn->query($sql) === TRUE){

			  		$this->script_criacao($conn);
			  		$this->script_insert($conn, $qtd_usuarios, $qtd_empresas, $nome_do_plano, $data_validade);
	                $this->script_triggers($conn);

	                $array = array(
						'error'	=> false,
						'sucess' => true,
						'mensagem' => 'Cliente cadastrado',
						'url' => $banco.'.veri.com.br'
					);
			  	}else{
			  		$array = array(
						'error'	=> true,
						'mensagem' => 'Erro ao criar o banco'
					);
			  	}

			} else {
				if(strpos($conn->error, 'database exists') !== false){
					$array = array(
						'error'	=> true,
						'mensagem' => 'Cliente ja existe'
					);
				}
			}

			$conn->close();

			echo json_encode($array);
		}
		
	}


	function script_criacao($conn){

	    $sql_contents =  file_get_contents('NOVO-SCRIPT-CRIACAO.sql');
	    $sql_contents = explode(";", $sql_contents);

	    foreach($sql_contents as $query){
	    	$conn->query($query);
	    }

	}

	function script_insert($conn, $qtd_usuarios, $qtd_empresas, $nome_do_plano, $data_validade){

	    $sql_contents =  file_get_contents('NOVO-SCRIPT-INSERT.sql');
	    $sql_contents = explode("--", $sql_contents);

	    foreach($sql_contents as $query){
	    	$conn->query($query);
	    }

	    $sql_plano = "INSERT INTO `dtb_plano_contratado` (`id`, `nome_do_plano`, `qtd_empresas`, `qtd_usuarios`, `cidade`, `validade`) VALUES (1, '".$nome_do_plano."', $qtd_empresas, $qtd_usuarios, NULL, '".$data_validade."');";
	    $conn->query($sql_plano);

	}

	function script_triggers($conn){
		$sql_contents =  file_get_contents('NOVO-SCRIPT-TRIGGERS.sql');
	    $sql_contents = explode("--", $sql_contents);

	    foreach($sql_contents as $query){
	    	$conn->query($query);
	    }
	}

	function atualizar_plano(){
		$qtd_usuarios = $this->input->post('qtd_usuarios');
		$qtd_empresas = $this->input->post('qtd_empresas');
		$nome_do_plano = $this->input->post('nome_do_plano');
		$data_validade = $this->input->post('data_validade');

		$banco = $this->input->post('nome_do_banco');

		$servername = "localhost";
		$username = "root";
		$password = "";

		$array = array(
			'error'	=> true,
			'mensagem' => 'Erro ao atualizar plano'
		);

		$conn = new mysqli($servername, $username, $password, $banco);
		if ($conn->connect_error) {
			$array = array(
				'error'	=> true,
				'mensagem' => 'Não existe esse cliente'
			);

			echo json_encode($array);
		}else{

			$sql_plano = "UPDATE dtb_plano_contratado SET nome_do_plano = '$nome_do_plano', qtd_empresas=$qtd_empresas, qtd_usuarios=$qtd_usuarios, validade='$data_validade' WHERE id = 1;";

			if($conn->query($sql_plano) === TRUE){
				$array = array(
					'error'	=> false,
					'sucess' => true,
					'mensagem' => 'Plano atualizado'
				);
			}else{
				$array = array(
					'error'	=> true,
					'mensagem' => 'Erro ao atualizar plano'
				);
			}

			$conn->close();
			echo json_encode($array);
		}

	}

	public function logar()
	{   

		$this->load->model("login_model", "login");

		$verificacao = 	$this->login->logar($this->input->post("login"), md5($this->input->post("senha")));

		if ($verificacao != 'invalido' && $verificacao != 'inativo' && $verificacao != 'expirado'){

			$array = array(
				'error'	=> false,
				'mensagem' => 'Login Confirmado',
				'url' => base_url() . '/login_externo/'.$verificacao['id'];
			);

			echo json_encode($array); 

			
		} else if ($verificacao == 'invalido'){

			$array = array(
				'error'	=> true,
				'mensagem' => 'Usuario ou senha incorretas'
			);

			echo json_encode($array); 

		} else if ($verificacao == 'inativo') {

			$array = array(
				'error'	=> true,
				'mensagem' => 'Usuario inativado'
			);

			echo json_encode($array);  

		} else if ($verificacao == 'expirado') {

			$array = array(
				'error'	=> true,
				'mensagem' => 'O seu periodo de utilizacao expirou, por favor entre em contato'
			);

			echo json_encode($array);  
		}
	}   
}