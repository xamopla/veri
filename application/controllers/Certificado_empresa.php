<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Certificado_empresa extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('documento_model');
        $this->load->model('empresa_model');
        $this->load->model('resumofiscal_model');
        $this->load->model('certificado_model'); 
        $this->load->model('email_clientes_model'); 
        $this->load->model('gerenciar_clientes_model'); 
    } 

	public function cadastrar(){

        //--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        $id = $this->uri->segment(3);

        $this->empresa_model->setId($id);
        $dados['empresa'] = $this->empresa_model->pesquisar_empresa_id();
        $dados['id'] = $id;
        
        $dados["certificado"] = $this->certificado_model->get_status_certificado($dados['empresa']->cnpj);
        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);        
        $this->load->view('empresa/empresa_cadastrar_certificado_digital', $dados);
        $this->load->view('layout/footer');
	}

    public function upload(){

        // VERIFICAR O NOME DA BASE
        $hostCompleto = $_SERVER['HTTP_HOST'];
        $server = explode('.', $hostCompleto);
        $server = $server[0];
        // VERIFICAÇÃO CONCLUÍDA
        
        $targetPath = 'crons-api/assets/empresas/certificados/'.$server.'/';

        if (!file_exists($targetPath)) {
            mkdir($targetPath, DIR_WRITE_MODE, true);
        }

        $findme   = '0001';
        $valor_cnpj = $this->input->post('certificado_cnpj');
        $pos = strpos($valor_cnpj, $findme);
        
        $dados_post = array(
            'senha'=>$this->input->post('certificado_senha'),
            'cnpj'=>$this->input->post('certificado_cnpj'),
            'id_empresa'=>$this->input->post('id_empresa'),
            'dir_upload_cert'=>$targetPath /*<--- Dar permissão para essa pasta, pois, ela será o caminho do upload do arquivo */
        );

        if ($pos === false) {
            echo ($this->certificado_model->configurar_certificado_filial($dados_post, $_FILES['certificado_file']));  
        } else {
            echo ($this->certificado_model->configurar($dados_post, $_FILES['certificado_file']));  
        }       
    }

    public function todos_certificados(){

        print_r($this->certificado_model->get_todos_certificados());      
    }

    // CADASTRAR CERTIFICADO NA TELA DO CLIENTE
    public function cadastrar_certfificado_cliente(){ 

        $id = $this->uri->segment(3);
        $id_empresa = $this->gerenciar_clientes_model->pesquisar_cliente_id_for_id($id)->id_empresa;

        $this->empresa_model->setId($id_empresa);
        $dados['empresa'] = $this->empresa_model->pesquisar_empresa_id();
        $dados['id_empresa'] = $id_empresa;
        $dados['id_cliente'] = $id; 
        
        $dados["certificado"] = $this->certificado_model->get_status_certificado($dados['empresa']->cnpj);

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout_clientes/cliente/head', $dados);
        $this->load->view('layout_clientes/cliente/sidebar');
        $this->load->view('layout_clientes/cliente/header');
        $this->load->view('painel_clientes/cliente/cadastrar_certificado', $dados);
        $this->load->view('layout_clientes/cliente/right_sidebar');
        $this->load->view('layout_clientes/cliente/footer');
    }

    public function listar_certificado_cliente(){

        $id = $this->uri->segment(3);

        $this->empresa_model->setId($id);
        $dados['empresa'] = $this->empresa_model->pesquisar_empresa_id();
        $dados['consulta'] = $this->empresa_model->listar_certificado_cliente();
        $dados['id'] = $id;
        
        $dados["certificado"] = $this->certificado_model->get_status_certificado($dados['empresa']->cnpj);

        // NOTIFICAÇÕES DO EMAIL
        $dados['qtd_mensagens_nao_lidas'] = $this->email_clientes_model->qtd_emails_nao_lidas_cliente($id);
        $dados['consulta_nao_lidos'] = $this->email_clientes_model->listar_emails_nao_lidos_cliente($id);
        // NOTIFICAÇÕES DO EMAIL

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);        
        $this->load->view('empresa/empresa_cadastrar_certificado_digital', $dados);
        $this->load->view('layout/footer');
    }
}


