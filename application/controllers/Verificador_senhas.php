<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verificador_senhas extends MY_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('empresa_model');
		$this->load->model('documento_model');
		$this->load->model('resumofiscal_model');
		$this->load->model('verificador_senhas_model');
    }

    public function index()
	{  

		$dados = $this->notificacoes();

		$dados['consulta'] = $this->verificador_senhas_model->listar_empresas_invalidas();//listar();
		
		$this->load->view('layout/head');
		$this->load->view('layout/sidebar');
		$this->load->view('layout/header', $dados);
		$this->load->view('verificador_senhas/verificador_senhas_listar', $dados);
		$this->load->view('layout/footer');
		
	}

	public function alterar_senha_sefaz(){

		//--------------- NOTIFICAÇÕES ---------------------
        $dados = $this->notificacoes();
        //-------------- FIM DAS NOTIFICAÇÕES ----------------

        $id = $this->uri->segment(3);

        $this->empresa_model->setId($id);
        $dados['empresa'] = $this->empresa_model->pesquisar_empresa_id();
        $dados['id'] = $id;
     	
 	 	$dados["status_senha"] = $this->verificador_senhas_model->get_status_senha($id);

        $this->load->view('layout/head');
        $this->load->view('layout/sidebar');
        $this->load->view('layout/header', $dados);        
        $this->load->view('verificador_senhas/verificador_senhas_editar', $dados);
        $this->load->view('layout/footer');
	}

	public function testar_senha(){

        $dados_post = array(
            'senha_sefaz'=>$this->input->post('senha_sefaz'),
            'login_sefaz'=>$this->input->post('login_sefaz'),
            'id_empresa'=>$this->input->post('id_empresa'),
        );

        echo ($this->verificador_senhas_model->testar_senha($dados_post));        
    }
}