<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Darf extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('darf_model');
        $this->load->model('empresa_model');
        $this->load->model('ecac_model');
    }

	public function index(){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux = '';
		$ano_aux = '';

		if(isset($_GET['filtro'])){
			$filtro = $_GET["filtro"];
		}else{
			$filtro = 'TODAS';
		}
		
		$this->darf_model->setFiltro_situacao($filtro);

		$dados['filtro'] = $filtro;
		
		$botao = $this->uri->segment(3);
		if($botao != null){

			$mes = $this->uri->segment(4);
			$mes_numero = 1;
			$ano = $this->uri->segment(5);

			switch ($mes) {
		        case "Janeiro":    $mes_numero = 1;     break;
		        case "Fevereiro":    $mes_numero = 2;   break;
		        case "Marco":    $mes_numero = 3;       break;
		        case "Abril":    $mes_numero = 4;       break;
		        case "Maio":    $mes_numero = 5;        break;
		        case "Junho":    $mes_numero = 6;       break;
		        case "Julho":    $mes_numero = 7;       break;
		        case "Agosto":    $mes_numero = 8;      break;
		        case "Setembro":    $mes_numero = 9;    break;
		        case "Outubro":    $mes_numero = 10;     break;
		        case "Novembro":    $mes_numero = 11;    break;
		        case "Dezembro":    $mes_numero = 12;    break; 
		 	} 

			if($botao == "anterior"){
				$mes_numero--;
				if($mes_numero == 0){
					$mes_numero = 12;
					$ano--;
				}
			}else{
				$mes_numero++;
				if($mes_numero == 13){
					$mes_numero = 1;
					$ano++;
				}
			}
			$this->darf_model->setFiltroMes($mes_numero);
			$this->darf_model->setFiltroAno($ano);

			if($mes_numero < 10){
				$mes_aux = '0'.$mes_numero;
			}else{
				$mes_aux = $mes_numero;
			}
			
			$ano_aux = $ano;

			$resultados = $this->darf_model->listarFiltro();

			$dados['m'] = $mes_numero;
			$dados['ano'] = $ano;

		} elseif (isset($_POST['mes_selecionado'])) {

			$mes_selecionado = $this->input->post("mes_selecionado");
			$ano_selecionado = $this->input->post("ano_selecionado");
			
		 	$this->darf_model->setFiltroMes($mes_selecionado);
			$this->darf_model->setFiltroAno($ano_selecionado);

			$mes_aux = $mes_selecionado;
			$ano_aux = $ano_selecionado;

			$resultados = $this->darf_model->listarFiltroByBotaoMes();
			$dados['m'] = $mes_selecionado;
			$dados['ano'] = $ano_selecionado;
			
		} else{

			$resultados = $this->darf_model->listar();

			$data_atual = date('Y-m-d');

			$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

			$arr = explode("-", $data_aux);
			$mes_ = $arr[1];
			$ano_ = $arr[0];

			$dados['m'] = $mes_;
			$dados['ano'] = $ano_;

			$mes_aux = $mes_;
			$ano_aux = $ano_;
		}

		$dados['consulta'] = $resultados;

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	$dados['banco'] = $server;
	  	
		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('darf/darf', $dados);
		$this->load->view('layout/footer');	
	}

	public static function getDiaUtil($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
		date_default_timezone_set('America/Sao_Paulo');

		$iMes = empty($iMes) ? date('m') : $iMes;
		$iAno = empty($iAno) ? date('Y') : $iAno;
		$iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));
 
		for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
			$iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
			//inclui apenas os dias úteis
			if ($iDiaSemana < 6) {
				$aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
			}
		}
		//ignorando os feriados	
		if (sizeof($aDiasIgnorar) > 0) {
			foreach ($aDiasIgnorar as $iDia) {
				$iKey = array_search($iDia, $aDias);
				unset($aDias[$iKey]);
			}
		}
 
		if (isset($aDias[$iDia - 1])) {
			return $aDias[$iDia - 1];
		} else {
			//retorna o último dia útil
			return $aDias[count($aDias) - 1];
		}
	}

	public function list_from_notification(){
		$cnpj = $this->uri->segment(3);
		$razao = "";
		if($cnpj != null){
			$em = $this->empresa_model->find_empresa_by_cnpj($cnpj);
			$razao = $em->razao_social;
		}

		date_default_timezone_set('America/Sao_Paulo');
		$mes_aux;
		$ano_aux;

		$resultados = $this->darf_model->listar();

		$data_atual = date('Y-m-d');

		$data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

		$arr = explode("-", $data_aux);
		$mes_ = $arr[1];
		$ano_ = $arr[0];

		$dados['m'] = $mes_;
		$dados['ano'] = $ano_;

		$mes_aux = $mes_;
		$ano_aux = $ano_;
		
		$ultimo_dia_transmitir = 20;//$this->getDiaUtil(15, $mes_aux+1, $ano_aux);

		$data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

		$data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

		$dados['ultimo_dia_transmitir'] = $data_aux;
		$dados['consulta'] = $resultados;
		$dados['razao_social_filtro'] = $razao;

		//--------------- NOTIFICAÇÕES ---------------------
		$notificacoes = $this->notificacoes();
		//-------------- FIM DAS NOTIFICAÇÕES ---------------- 
    	
		$this->load->view('layout/head');
		$this->load->view('layout/header', $notificacoes);
		$this->load->view('layout/sidebar');
		$this->load->view('ecac/das', $dados);
		$this->load->view('layout/footer');	
	}

}