<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socios_empresas extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Declaracao_dirf_ausente/processar
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);
        $this->load->model('socios_empresas_model');

        $registros = $this->socios_empresas_model->busca_situacao_fiscal($banco);
        $pdf    =  new PdfToText() ;

        
        foreach ($registros as $e) {

            // $e->caminho_download = "https://storage.googleapis.com/cron-veri-files-br/SistemaCronsCertificado/sp/arquivos/pdf-certidao-ecac-sp/43956663000197/situa%C3%A7%C3%A3o-fiscal-2021-09-10-37059156000158.pdf";
            // $e->caminho_download = "https://storage.googleapis.com/cron-veri-files-br/SistemaCronsCertificado/sp/arquivos/pdf-certidao-ecac-sp/43956663000197/situa%C3%A7%C3%A3o-fiscal-2021-09-15-00703068000137.pdf";

            // $e->caminho_download = "https://storage.googleapis.com/cron-veri-files-br/SistemaCronsCertificado/sp/arquivos/pdf-certidao-ecac-sp/43956663000197/situa%C3%A7%C3%A3o-fiscal-2021-09-15-01471991000153.pdf";

            // $e->caminho_download = "https://storage.googleapis.com/cron-veri-files-br/SistemaCronsCertificado/sp/arquivos/pdf-certidao-ecac-sp/onixcontabilidade/situa%C3%A7%C3%A3o-fiscal-2021-09-10-39251537000160.pdf";

            if(!empty($e->caminho_download)){
                try{

                    $pdf->Load( $e->caminho_download ) ;

                    $cnpj = $e->cnpj;

                    $texto = nl2br($pdf -> Text) ;

                    //Começa a partir dessa string "Sócios e Administradores ________________________________________________________________________________________"
                    $p = explode("Sócios e Administradores ________________________________________________________________________________________", $texto); 

                    $texto_limite = $p[1];

                    //finaliza a partir dessa string "Certidão Emitida ________________________________________________________________________________________________"
                    $parte1 = explode("Certidão Emitida ________________________________________________________________________________________________", $texto_limite);

                     
                    $t = nl2br($parte1[0]);

                    if(strpos($t, " Diagnóstico") !== false){
                        $parte_ = explode("_____________________________________ Diagnóstico Fiscal na Receita Federal _____________________________________", $t);
                        $t = $parte_[0];
                    }


                    //Verifica se contém essa string de não obrigatorio, pois nela não apaerece o nome dos sócios
                    $pos_nao_obrigatorio = strpos($t, "NÃO OBRIGATÓRIO");
                    if ($pos_nao_obrigatorio !== false) {
                        continue;
                    }else{

                        $this->socios_empresas_model->limpa_tabela($banco, $e->cnpj);
                        //começa a area de busca a partir dessa palavra Votante, após ela começa a aparecer os nomes dos sócios
                        $texto_final_busca = explode("Votante", $t);
                        $area_busca = $texto_final_busca[1];
                        
                        // $socios = explode("%", $area_busca);
                        $socios = explode("%", $area_busca);

                        echo "Lendo documento ".$e->cnpj;
                        echo "<br>";
                        foreach ($socios as $s) {
                            // $s = strip_tags($s);
                            $nome_razao_aux = "";
                            $cpf_cnpj = "";

                            if(!empty($s)){
                                
                                $posicao_traco = strpos($s, "-");
                                $cpf_cnpj = substr($s, 0, $posicao_traco + 3);
                                $cpf_cnpj = $this->deixarNumero(trim($cpf_cnpj));
                                
                                //Há pelo menos 4 tipos diferentes de classificação já encontradas até o momento
                                /*
                                TITULAR PESSOA FISICA
                                RESIDENTE OU
                                DOMICILIADO NO BRASIL
                                */
                                /*
                                SOCIO
                                SOCIO ADMINISTRADOR
                                */
                                /*
                                PRESIDENTE
                                */

                                /*
                                ADMINISTRADOR
                                */

                                /*
                                PRODUTOR
                                */


                                
                                $pos_classificacao_titular = strpos($s, "TITULAR PESSOA");
                                $pos_classificacao_socio = strpos($s, "SOCIO");
                                $pos_classificacao_presidente = strpos($s, "PRESIDENTE");
                                $pos_classificacao_administrador = strpos($s, "ADMINISTRADOR");
                                $pos_classificacao_produtor = strpos($s, "PRODUTOR");

                                if ($pos_classificacao_titular !== false) {
                                    $nome_razao = substr($s, $posicao_traco + 3, $pos_classificacao_titular);
                                    $nome_razao_aux = explode("TITULAR", $nome_razao);
                                    $nome_razao_aux = $nome_razao_aux[0];

                                    echo $nome_razao_aux;
                                    echo "<br>";
                                    echo $cpf_cnpj;
                                    echo "<br>";
                                    // echo "Sócio - ".$s;
                                    // echo "<br>";
                                }elseif($pos_classificacao_socio !== false){
                                    $nome_razao = substr($s, $posicao_traco + 3, $pos_classificacao_socio);
                                    $nome_razao_aux = explode("SOCIO", $nome_razao);
                                    $nome_razao_aux = $nome_razao_aux[0];

                                    echo $nome_razao_aux;
                                    echo "<br>";
                                    echo $cpf_cnpj;
                                    echo "<br>";
                                    // echo "Sócio - ".$s;
                                    // echo "<br>";
                                }elseif($pos_classificacao_presidente !== false){
                                    $nome_razao = substr($s, $posicao_traco + 3, $pos_classificacao_presidente);
                                    $nome_razao_aux = explode("PRESIDENTE", $nome_razao);
                                    $nome_razao_aux = $nome_razao_aux[0];

                                    echo $nome_razao_aux;
                                    echo "<br>";
                                    echo $cpf_cnpj;
                                    echo "<br>";
                                    // echo "Sócio - ".$s;
                                    // echo "<br>";
                                }elseif($pos_classificacao_administrador !== false){
                                    $nome_razao = substr($s, $posicao_traco + 3, $pos_classificacao_administrador);
                                    $nome_razao_aux = explode("ADMINISTRADOR", $nome_razao);
                                    $nome_razao_aux = $nome_razao_aux[0];

                                    echo $nome_razao_aux;
                                    echo "<br>";
                                    echo $cpf_cnpj;
                                    echo "<br>";
                                    // echo "Sócio - ".$s;
                                    // echo "<br>";
                                }elseif($pos_classificacao_produtor !== false){
                                    $nome_razao = substr($s, $posicao_traco + 3, $pos_classificacao_produtor);
                                    $nome_razao_aux = explode("PRODUTOR", $nome_razao);
                                    $nome_razao_aux = $nome_razao_aux[0];

                                    echo $nome_razao_aux;
                                    echo "<br>";
                                    echo $cpf_cnpj;
                                    echo "<br>";
                                    // echo "Sócio - ".$s;
                                    // echo "<br>";
                                }else{
                                    echo "aa ".$s;
                                    echo "Novo motivo ".$e->cnpj;
                                    echo "<br>";
                                }


                                if(!empty($cpf_cnpj)){
                                    $nome_razao_aux = strip_tags($nome_razao_aux);

                                    $this->socios_empresas_model->insere_socio_empresa($banco, $e->cnpj, $cpf_cnpj, $nome_razao_aux, "", "");
                                }
                            }
                        }

                        // die(); 
                    }

                }catch(Exception $x){
                    echo "ERRO AO PROCESSAR CNPJ - ".$e->cnpj;
                    echo '<br>';
                    continue;
                }
                
            }
            
        }
        
    }


    function deixarNumero($string){
      return preg_replace("/[^0-9]/", "", $string);
    }


    

}
