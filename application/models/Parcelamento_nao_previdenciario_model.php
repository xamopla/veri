<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parcelamento_nao_previdenciario_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	public function listar_processos(){

		$this->db->select('db.id, db.cnpj, db.processo, db.data_do_deferimento, db.situacao, e.razao_social');

		$this->db->from('dtb_parcelamento_nao_previdenciario_processos_negociados db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
        $this->db->where('e.cnpj like "%0001%"');

		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function listar_processo_individual($id){

		$this->db->select('db.*, e.razao_social');

		$this->db->from('dtb_parcelamento_nao_previdenciario_processos_negociados db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');
        $this->db->where('e.cnpj like "%0001%"');

		$this->db->where("db.id", $id);
	
		return $this->db->get()->row();
	}

	public function listar_tributos_negociados($id_processo){
		$this->db->select('db.*');

		$this->db->from('dtb_parcelamento_nao_previdenciario_tributos_processo_negociados db');

		$this->db->where("db.id_processo", $id_processo);
	
		return $this->db->get()->result();
	}

	public function listar_demonstrativo_das_parcelas($id_tributo){
		$this->db->select('db.*');

		$this->db->from('dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas db');

		$this->db->where("db.id_tributo", $id_tributo);
	
		return $this->db->get()->result();
	}

	//dtb_parcelamento_nao_previdenciario_tributos_processo_negociados

	public function get_qtd_parcelas_em_aberto(){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("db.situacao = 'Em aberto'");

		return $this->db->get()->row();
	}
	
	// Metodos da Modal
	public function listar_processos_negociados_for_modal($cnpj){

		$this->db->select('*');
		$this->db->from('dtb_parcelamento_nao_previdenciario_tributos_processo_negociados db');
		$this->db->where("db.cnpj", $cnpj);
		
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function listar_demonstrativo_das_parcelas_for_modal($cnpj, $id_tributo){

		$this->db->select('db.*, e.tributo');
		
		$this->db->from('dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas db');
		$this->db->join('dtb_parcelamento_nao_previdenciario_tributos_processo_negociados e','e.id = db.id_tributo');
		
		$this->db->where("db.cnpj", $cnpj);
		$this->db->where("db.id_tributo", $id_tributo);
		
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function listar_parcelas_pagas_for_modal($cnpj){

		$this->db->select('db.*, e.tributo');
		
		$this->db->from('dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas db');
		$this->db->join('dtb_parcelamento_nao_previdenciario_tributos_processo_negociados e','e.id = db.id_tributo');
		
		$this->db->where("db.cnpj", $cnpj);
		$this->db->where("db.situacao", "Paga");
		
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	// Metodos da dashboard
	public function get_qtd_processos_negociados(){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_parcelamento_nao_previdenciario_processos_negociados db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}


		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
            if($this->getFiltro() == 'PARCELADO'){
                $this->db->where("db.situacao = 'Parcelado'");
            }
        }

		return $this->db->get()->row();
	}

}