<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dctf_model extends CI_Model {

	private $filtro_situacao;

	private $filtro_mes;
	private $filtro_ano;

	function __construct()
	{
		parent::__construct();
	}


	function setFiltroMes($filtro_mes) { 
		$this->filtro_mes = $filtro_mes; 
	}

	function getFiltroMes() { 
		return $this->filtro_mes; 
	}

	function setFiltroAno($filtro_ano) { 
		$this->filtro_ano = $filtro_ano; 
	}

	function getFiltroAno() { 
		return $this->filtro_ano; 
	}

	public function getFiltro_situacao() {
	    return $this->filtro_situacao;
	}
	 
	public function setFiltro_situacao($filtro_situacao) {
	    $this->filtro_situacao = $filtro_situacao;
	}


	public function listar(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf, d.caminho_download_declaracao   
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime != 'SIMPLES NACIONAL' ) AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf, '' as caminho_download_declaracao  
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{

			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf, d.caminho_download_declaracao   
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf, '' as caminho_download_declaracao    
			FROM dtb_empresas e 
			WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
			GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];

	  	if($server == "lojasguaibim"){
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ||  resultado.cnpj_completo like "%0072%"';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}else{
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND resultado.cnpj like "%0001%" ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND resultado.cnpj like "%0001%" ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}

		
	} 


	public function listarFiltro(){
		$sql = "";
		$filtro_ano = $this->getFiltroAno();
		$filtro_mes = $this->getFiltroMes();

		if($filtro_mes < 10){
			$filtro_mes = "0".$filtro_mes;
		}

		$data_inicial = "'".$filtro_ano.'-'.$filtro_mes.'-01'."'";
		$data_final = "'".$filtro_ano.'-'.$filtro_mes.'-31'."'";

		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf, d.caminho_download_declaracao    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.tipo_regime != 'SIMPLES NACIONAL' ) AND(d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf, '' as caminho_download_declaracao    
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;

		}else{
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf, d.caminho_download_declaracao    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") )  AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf, '' as caminho_download_declaracao    
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];
		if($server == "lojasguaibim"){
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ||  resultado.cnpj_completo like "%0072%"';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}else{
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND resultado.cnpj like "%0001%" ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND resultado.cnpj like "%0001%" ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}
	}

	public function listarFiltroByBotaoMes(){
		$sql = "";
		$filtro_ano = $this->getFiltroAno();
		$filtro_mes = $this->getFiltroMes();

		$data_inicial = "'".$filtro_ano.'-'.$filtro_mes.'-01'."'";
		$data_final = "'".$filtro_ano.'-'.$filtro_mes.'-31'."'";

		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf, d.caminho_download_declaracao    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf, '' as caminho_download_declaracao    
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf, d.caminho_download_declaracao    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf, '' as caminho_download_declaracao    
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];
		if($server == "lojasguaibim"){
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ||  resultado.cnpj_completo like "%0072%"';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}else{
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND resultado.cnpj like "%0001%" ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND resultado.cnpj like "%0001%" ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}
	}
	

	public function qtd_dctf_pendente(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf   
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf   
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{

			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf   
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf   
			FROM dtb_empresas e 
			WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
			GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];
		if($server == "lojasguaibim"){
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ||  resultado.cnpj_completo like "%0072%"';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}else{
			// 		$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}
			// }
			return $this->db->query($sql_final)->row();
	  	}else{
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND resultado.cnpj like "%0001%" ';
			// 	}else{
			// 		$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND resultado.cnpj like "%0001%" ';
			// 	}
			// }
			return $this->db->query($sql_final)->row();
	  	}
	} 


	////
	public function qtd_dctf_pendente_proximo_vencer(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf   
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (d.tipo_status NOT LIKE '%Cancelada%')      
				GROUP BY e.id";

			$sql2 = "SELECT e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf   
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{

			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  
				GROUP BY e.id";

			$sql1 = "SELECT e.cnpj_completo, e.razao_social, e.cnpj, d.periodo, d.tipo_status, d.numero_declaracao, d.numero_recibo, d.data_recepcao, 1 as sem_dctf   
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_dctf as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (d.tipo_status NOT LIKE '%Cancelada%')     
				GROUP BY e.id";

			$sql2 = "SELECT e.cnpj_completo, e.razao_social, e.cnpj, '' as periodo, '' as tipo_status, '' as numero_declaracao, '' as numero_recibo, '' as data_recepcao, 0 as sem_dctf   
			FROM dtb_empresas e 
			WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime != 'SIMPLES NACIONAL' )  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
			GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$hostCompleto = $_SERVER['HTTP_HOST'];
	  	$server = explode('.', $hostCompleto);
	  	$server = $server[0];
		if($server == "lojasguaibim"){
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ||  resultado.cnpj_completo like "%0072%"';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND (resultado.cnpj like "%0001%" ||  resultado.cnpj_completo like "%0072%") ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}else{
	  		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ';
			// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// 	if($this->getFiltro_situacao() == 'REGULAR'){
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 1 AND resultado.cnpj like "%0001%" ';
			// 	}else{
			// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_dctf = 0 AND resultado.cnpj like "%0001%" ';
			// 	}
			// }
			return $this->db->query($sql_final)->result();
	  	}
	} 


	public function update_simples($id){
		$this->db->set('tipo_regime', 'SIMPLES NACIONAL');
		$this->db->where('id', $id);
		$this->db->update('dtb_empresas');
	}

	public function update_caminho_download($caminho_download , $periodo, $cnpj){
		$this->db->set('caminho_download_declaracao', $caminho_download);
		$this->db->where('periodo', $periodo);
		$this->db->where('cnpj', $cnpj);
		$this->db->update('dtb_empresas');
	}

	public function buscar_empresas_sem_movimento($mes){
		$this->db->select('*');
		$this->db->where('mes = "'.$mes.'"');
		return $this->db->get('dtb_dctf_sem_movimento')->result();
	}


	public function marcar_sem_movimento($cnpj, $mes){
		date_default_timezone_set('America/Sao_Paulo');
		$usuario = "";
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		$dados = array(
	
				'mes' => $mes,
				'cnpj' => $cnpj,
				'data_alteracao' => date('Y-m-d H:i:s'),
				'nome_usuario' => $usuario
		);
	
		if ($this->db->insert('dtb_dctf_sem_movimento', $dados)){
			return 1;
		} else {
			return 0;
		}
	}



	public function insere_notificacao($cnpj, $mes){
        
        date_default_timezone_set('America/Sao_Paulo');

        $dados = array( 

                'cnpj' => $cnpj,
                'mes' => $mes,
                'data' => date('Y-m-d H:i:s'),
                'descricao' => 'DCTF sem movimento',
                'lida' => '0'
        );

        $this->db->insert('dtb_notificacao_dctf_sem_movimento', $dados);
        return $this->db->insert_id();
    }

    public function semMovimentoLote($empresas, $mes){
    	date_default_timezone_set('America/Sao_Paulo');
		$usuario = "";
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

        if(isset($empresas) && sizeof($empresas) != 0){
            foreach ($empresas as $empresa) {
                $dados = array(
	
						'mes' => $mes,
						'cnpj' => $empresa,
						'data_alteracao' => date('Y-m-d H:i:s'),
						'nome_usuario' => $usuario
				);

                $this->db->insert('dtb_dctf_sem_movimento', $dados);
            }
        }

        return TRUE;

    }

}