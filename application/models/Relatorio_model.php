<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio_model extends CI_Model {
	private $cnpj;
	private $cnpj_completo;
	private $id_empresa;

	public function getCnpj() {
	    return $this->cnpj;
	}
	 
	public function setCnpj($cnpj) {
	    $this->cnpj = $cnpj;
	}

	public function getCnpj_completo() {
	    return $this->cnpj_completo;
	}
	 
	public function setCnpj_completo($cnpj_completo) {
	    $this->cnpj_completo = $cnpj_completo;
	}
	public function getId_empresa() {
	    return $this->id_empresa;
	}
	 
	public function setId_empresa($id_empresa) {
	    $this->id_empresa = $id_empresa;
	}

	public function possui_certificado_digital(){
		$this->db->select('count(*) as valor');
		$this->db->where('cnpj_data', $this->getCnpj());

		return $this->db->get('dtb_certificado')->row();
	}

	public function qtd_msg_nao_lidas(){
		$this->db->select('id, nao_lidas');
		$this->db->where('cnpj_data', $this->getCnpj());

		return $this->db->get('dtb_ecac_caixa_postal')->row();
	}

	public function possui_pendencia_ecac(){
		$this->db->select('possui_pendencia, caminho_download');
		$this->db->where('cnpj_data', $this->getCnpj());

		return $this->db->get('dtb_situacao_fiscal')->row();
	}

	public function parcelamento_das(){
		$this->db->select('count(*) as qtd');
		$this->db->where('cnpj_data', $this->getCnpj());

		return $this->db->get('dtb_parcelas_emitidas')->row();
	}

	public function resumo_fiscal() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8, pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		$this->db->where('trim(dtb_resumofiscal.cnpjcpf)', $this->getCnpj_completo());

		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function qtd_registros_msg_nao_lidas_dte(){
		$this->db->select('msg_nao_lidas as valor');

		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Todas Lidas%' ");
		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Não há mensagem%' ");
		$this->db->where("trim(msg_nao_lidas) != ", "");
		$this->db->where("msg_nao_lidas is not null");
		$this->db->where("trim(cnpjcpf)", $this->getCnpj_completo());
		return $this->db->get('dtb_resumofiscal')->row();	
	}

	public function situacao_fiscal() {
		$this->db->select('vinculo_contador, dtb_empresas.id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, senha_sefaz, login_sefaz');

		$this->db->where("cnpj", $this->getCnpj());
		return $this->db->get('dtb_empresas')->row();
	}

	public function obter_parcelamento_das(){
		$this->db->select('*');
		$this->db->where('cnpj_data', $this->getCnpj());

		return $this->db->get('dtb_parcelas_emitidas')->result();
	}

	public function timeline(){
		$query_resumo = "SELECT rf.descricao as descricao, rf.data_atualizacao as data, 1 as is_resumo_fiscal, 0 as is_notificacao FROM notificacoesresumofiscal rf WHERE trim(rf.cnpjstring) = '". trim($this->getCnpj_completo())."' ";

		$query_normal = "SELECT n.situacao as descricao, n.data_atualizacao as data, 0 as is_resumo_fiscal, 1 as is_notificacao FROM notificacoes n where n.id_empresa = ".$this->getId_empresa()." ";

		$query = $query_resumo." union all ".$query_normal;

		$query_final = "select *
						from (". $query.") a
						order by data asc";

		return $this->db->query($query_final)->result();
	}

	public function omissodma() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25, e.cnpj_completo as cnpj_completo');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta2) != ", "Não");
		$this->db->where("trim(pergunta2) != ", "");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function omissoefd() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25, e.cnpj_completo as cnpj_completo');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta3) != ", "Não");
		$this->db->where("trim(pergunta3) != ", "");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}


	public function find_all_empresas(){
		$this->db->select('e.id as id, e.razao_social as razao_social, e.cnpj_completo as cnpj_completo');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
		}
		$this->db->group_by('e.id');
		return $this->db->get('dtb_empresas as e')->result();
	}

}