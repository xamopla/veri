<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Empresausuario_model extends CI_Model {

	private $id;
	private $id_empresa;
	private $id_usuario;

	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getIdEmpresa() {
		return $this->id_empresa;
	}
	public function setIdEmpresa($id_empresa) {
		$this->id_empresa = $id_empresa;
		return $this;
	}

	public function getIdUsuario() {
		return $this->id_usuario;
	}
	public function setIdUsuario($id_usuario) {
		$this->id_usuario = $id_usuario;
		return $this;
	}

	public function findAllByIdEmpresa() {
		$this->db->select('id_usuario as id');
		$this->db->where('id_empresa', $this->getIdEmpresa());

		return $this->db->get('dtb_empresa_usuario')->result();
	}

	public function buscar_empresas_vinculadas($id){
		$this->db->select('id_empresa, e.razao_social');
		$this->db->join('dtb_empresas as e', 'dtb_empresa_usuario.id_empresa = e.id', 'left');
		$this->db->where('id_usuario', $id);

		return $this->db->get('dtb_empresa_usuario')->result();
	}

	public function findUsuarios(){
		$sql = "Select GROUP_CONCAT(u.nome) as colaboradores, eu.id_empresa FROM dtb_empresa_usuario eu LEFT JOIN dtb_usuarios u ON eu.id_usuario = u.id GROUP BY eu.id_empresa";

		return $this->db->query($sql)->result();
	}

}