<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tutorial_model extends CI_Model {

	private $categoria;
	private $lida;

	public function getCategoria() {
	    return $this->categoria;
	}
	 
	public function setCategoria($categoria) {
	    $this->categoria = $categoria;
	}

	public function getLida() {
	    return $this->lida;
	}
	 
	public function setLida($lida) {
	    $this->lida = $lida;
	}

	public function listar() {
		$this->db->select('*');
		$this->db->where_in('categoria', $this->getCategoria());
		// $this->db->where_in('vista', $this->getLida());
		$this->db->order_by('id desc');
		return $this->db->get('dtb_tutorial')->result();
	}

}