<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Simples_nacional_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	// public function listar_excluidas_simples() {

	// 	$id_colaborador = $this->session->userdata['userprimesession']['id'];

	// 	if ($this->session->userdata['userprimesession']['nivel'] != 2){
	// 		//query para admins

	// 		$sql1 = "SELECT es.cnpj, es.razao_social, es.situacao_simples, es.situacao_simei, es.data_inicial_simples, es.data_final_simples, es.detalhe_simples, es.data_inicial_simei, es.data_final_simei, es.detalhe_simei, es.evento_futuro_mei, es.evento_futuro_simei, e.inscricao_estadual_completo 
	// 		FROM dtb_exclusao_simples es 
	// 		LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj ";

	// 		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	//             if($this->getFiltro() == "EXCLUIDAS"){
	//             	$sql_where = "WHERE es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null ";
	//             }else if($this->getFiltro() == "OPTANTES"){
	//             	$sql_where = "WHERE es.situacao_simples not like '%NÃO%' AND es.situacao_simples is not null  ";
	//             }
	//         }else{
	//         	$sql_where = "WHERE (es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null) OR (es.situacao_simples not like '%NÃO%') ";
	//         }
	//     }else{
	//     	$sql1 = "SELECT es.cnpj, es.razao_social, es.situacao_simples, es.situacao_simei, es.data_inicial_simples, es.data_final_simples, es.detalhe_simples, es.data_inicial_simei, es.data_final_simei, es.detalhe_simei, es.evento_futuro_mei, es.evento_futuro_simei, e.inscricao_estadual_completo 
	//     	FROM dtb_exclusao_simples es 
	// 		LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj 
	// 		LEFT JOIN dtb_empresa_usuario as eu ON e.id = eu.id_empresa 
	// 		WHERE (e.id_funcionario = ".$id_colaborador." OR eu.id_usuario = ".$id_colaborador." ) ";

	// 		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	//             if($this->getFiltro() == "EXCLUIDAS"){
	//             	$sql_where = "AND es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null ";
	//             }else if($this->getFiltro() == "OPTANTES"){
	//             	$sql_where = "AND es.situacao_simples not like '%NÃO%' AND es.situacao_simples is not null ";
	//             }
	//         }else{
	//         	$sql_where = "AND (es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null) OR (es.situacao_simples not like '%NÃO%' ) ";
	//         }

	//     }

	// 	$sql_final = $sql1." ".$sql_where." GROUP BY es.cnpj ";
	// 	return $this->db->query($sql_final)->result();

	// }

	public function listar_excluidas_simples() {

		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if ($this->session->userdata['userprimesession']['nivel'] != 2){
			//query para admins

			$sql1 = "SELECT es.cnpj, e.razao_social, es.data_inicio, es.data_fim, es.is_excluida 
			FROM dtb_empresas_excluidas_simples es 
			LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj ";

			if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	            if($this->getFiltro() == "EXCLUIDAS"){
	            	$sql_where = " WHERE es.is_excluida = 1 ";
	            }else if($this->getFiltro() == "OPTANTES"){
	            	$sql_where = " WHERE es.is_excluida = 0  ";
	            }
	        }else{
	        	$sql_where = "";
	        }
	    }else{
	    	$sql1 = "SELECT es.cnpj, e.razao_social, es.data_inicio, es.data_fim, es.is_excluida 
	    	FROM dtb_empresas_excluidas_simples es 
			LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj 
			WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) ";

			if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	            if($this->getFiltro() == "EXCLUIDAS"){
	            	$sql_where = " AND es.is_excluida = 1 ";
	            }else if($this->getFiltro() == "OPTANTES"){
	            	$sql_where = " AND es.is_excluida = 0 ";
	            }
	        }else{
	        	$sql_where = " ";
	        }

	    }

		$sql_final = $sql1." ".$sql_where." GROUP BY es.cnpj ";
		return $this->db->query($sql_final)->result();

	}

	// public function qtd_excluidas_simples() {
	// 	$id_colaborador = $this->session->userdata['userprimesession']['id'];

	// 	if ($this->session->userdata['userprimesession']['nivel'] != 2){
	// 		//query para admins

	// 		$sql1 = "SELECT es.cnpj, es.razao_social, es.situacao_simples, es.situacao_simei, es.data_inicial_simples, es.data_final_simples, es.detalhe_simples, es.data_inicial_simei, es.data_final_simei, es.detalhe_simei, es.evento_futuro_mei, es.evento_futuro_simei, e.inscricao_estadual_completo 
	// 		FROM dtb_exclusao_simples es 
	// 		LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj ";

	// 		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	//             if($this->getFiltro() == "EXCLUIDAS"){
	//             	$sql_where = "WHERE es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null ";
	//             }else if($this->getFiltro() == "OPTANTES"){
	//             	$sql_where = "WHERE es.situacao_simples not like '%NÃO%' AND es.situacao_simples is not null  ";
	//             }
	//         }else{
	//         	$sql_where = "WHERE (es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null) OR (es.situacao_simples not like '%NÃO%') ";
	//         }
	//     }else{
	//     	$sql1 = "SELECT es.cnpj, es.razao_social, es.situacao_simples, es.situacao_simei, es.data_inicial_simples, es.data_final_simples, es.detalhe_simples, es.data_inicial_simei, es.data_final_simei, es.detalhe_simei, es.evento_futuro_mei, es.evento_futuro_simei, e.inscricao_estadual_completo 
	//     	FROM dtb_exclusao_simples es 
	// 		LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj 
	// 		LEFT JOIN dtb_empresa_usuario as eu ON e.id = eu.id_empresa 
	// 		WHERE (e.id_funcionario = ".$id_colaborador." OR eu.id_usuario = ".$id_colaborador." ) ";

	// 		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	//             if($this->getFiltro() == "EXCLUIDAS"){
	//             	$sql_where = "AND es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null ";
	//             }else if($this->getFiltro() == "OPTANTES"){
	//             	$sql_where = "AND es.situacao_simples not like '%NÃO%' AND es.situacao_simples is not null ";
	//             }
	//         }else{
	//         	$sql_where = "AND (es.situacao_simples like '%NÃO%' AND es.detalhe_simples is not null) OR (es.situacao_simples not like '%NÃO%' ) ";
	//         }

	//     }

	// 	$sql_final = $sql1." ".$sql_where." GROUP BY es.cnpj ";

	// 	$sql_qtd = "SELECT COUNT(*) as qtd FROM (".$sql_final.") AS resultado ";

	// 	return $this->db->query($sql_qtd)->row();

	// }


	public function qtd_excluidas_simples() {
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if ($this->session->userdata['userprimesession']['nivel'] != 2){
			//query para admins

			$sql1 = "SELECT es.cnpj, e.razao_social, es.data_inicio, es.data_fim, es.is_excluida 
			FROM dtb_empresas_excluidas_simples es 
			LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj ";

			if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	            if($this->getFiltro() == "EXCLUIDAS"){
	            	$sql_where = " WHERE es.is_excluida = 1 ";
	            }else if($this->getFiltro() == "OPTANTES"){
	            	$sql_where = " WHERE es.is_excluida = 0  ";
	            }
	        }else{
	        	$sql_where = "";
	        }
	    }else{
	    	$sql1 = "SELECT es.cnpj, e.razao_social, es.data_inicio, es.data_fim, es.is_excluida 
	    	FROM dtb_empresas_excluidas_simples es 
			LEFT JOIN dtb_empresas e ON es.cnpj = e.cnpj 
			WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) ";

			if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
	            if($this->getFiltro() == "EXCLUIDAS"){
	            	$sql_where = " AND es.is_excluida = 1 ";
	            }else if($this->getFiltro() == "OPTANTES"){
	            	$sql_where = " AND es.is_excluida = 0 ";
	            }
	        }else{
	        	$sql_where = " ";
	        }

	    }

		$sql_final = $sql1." ".$sql_where." GROUP BY es.cnpj ";

		$sql_qtd = "SELECT COUNT(*) as qtd FROM (".$sql_final.") AS resultado ";

		return $this->db->query($sql_qtd)->row();

	}

}