<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mail_model extends CI_Model {

	private $id;
	private $email;
	private $assunto;

	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function getAssunto() {
		return $this->assunto;
	}
	public function setAssunto($assunto) {
		$this->assunto = $assunto;
		return $this;
	}


	public function listar(){
		$this->db->select('*');
		$this->db->where('id = 1');
		return $this->db->get('dtb_mail')->row();
	}

	public function cadastrar(){
	
		$dados = array(
	
				'email' => $this->getEmail(),
				'assunto' => $this->getAssunto()

		);
	
		if ($this->db->insert('dtb_mail', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function editar(){
	
		$dados = array(

				
				'email' => $this->getEmail(),
				'assunto' => $this->getAssunto(),
		);
	
		if ($this->db->update('dtb_mail', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
		
	}

}