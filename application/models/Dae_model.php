<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dae_model extends CI_Model {

	public function main($dados_post){
		$dados_request_dae = $this->logar($dados_post);
		$dados_sefaz = $this->consultar_ano($dados_post, $dados_request_dae);
		return $this->return_array($dados_sefaz);
	}

	public function logar($dados_login){
		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=>'/wEPDwUKMTYwMTM5MTE2OA9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFA0RURWQCBQ8WAh8ABS5Eb21pYyYjMjM3O2xpbyBUcmlidXQmIzIyNTtyaW8gRWxldHImIzI0NDtuaWNvZAIJD2QWAmYPZBYCAgEPZBYGAgMPDxYIHjBfX1BIQ2VudHJvX21zZ0Vycm9Mb2dpbl9WaWV3U3RhdGVBU01vZGFsVGl0dWxvSUQFEVNlc3PDo28gRW5jZXJyYWRhHjJfX1BIQ2VudHJvX21zZ0Vycm9Mb2dpbl9WaWV3U3RhdGVBU01vZGFsQ29udGV1ZG9JRGUeCENzc0NsYXNzBQptb2RhbCBmYWRlHgRfIVNCAgIWDB4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljHglkcmFnZ2FibGUFBHRydWVkAgUPZBYEAgcPFgIfAGVkAgsPDxYCHgdWaXNpYmxlZ2RkAgcPFgIfAAX9Bzx0YWJsZT4NCg0KPHRyPg0KDQogICAgPHRkPg0KICAgIA0KICAgICAgICA8ZGl2IGNsYXNzPSJjb2wxIHNwYW41Ij4NCiAgICAgICAgICAgIDxpbWcgc3JjPSIvZHRlL2R0X2VfZmluYWwucG5nIiBhbHQ9IkxvZ28gRFRFIi8+DQogICAgICAgICAgICA8YnIgLz48YnIgLz48YnIgLz4NCiAgICAgICAgPC9kaXY+DQogICAgPC90ZD4NCjwvdHI+DQo8dHI+DQogICAgPHRkPg0KICAgDQogICAgICA8ZGl2Pg0KDQogICAgICAgIDxwPkZlcnJhbWVudGEgZXh0ZXJuYSBwYXJhIGdlcmVuY2lhbWVudG8gZGEgQ29udGEgbm8gRG9taWPDrWxpbyBUcmlidXTDoXJpbyBFbGV0csO0bmljbyAtIERURTwvcD4NCiAgICANCiAgICAgICAgPGgxPg0KICAgIA0KICAgICAgICA8L2gxPg0KICAgICAgICA8aDI+SW5mb3JtYcOnw7Vlcy48L2gyPg0KICAgIA0KICAgICAgICA8cD4NCiAgICAgICAgICAgIFByb2pldGFkbyBjb20gYSBpZGVpYSBkZSBkZXNlbnZvbHZlciBkZSBmb3JtYSBtYWlzIGludHVpdGl2YSwgZWZpY2llbnRlLCDDunRpbCBlIHNpbXBsZXMuIA0KICAgICAgICAgICAgPGJyIC8+DQogICAgICAgICAgICBDb20gYSBmZXJyYW1lbnRhIHBvZGVyw6EgY2FkYXN0cmFyIHVtYSBDb250YSBEb21pY8OtbGlvIFRyaWJ1dMOhcmlvLCBFbmRlcmXDp29zIEVsZXRyw7RuaWNvcywgVGVsZWZvbmVzIENlbHVsYXJlcywgTGVyIE1lbnNhZ2VucyBSZWNlYmlkYXMgZSBWaXN1YWxpemFyIFJlbGF0w7NyaW9zLg0KICAgICAgICA8L3A+DQogICAgICAgIDx1bD4NCiAgICAgICAgICAgIDxsaT4NCiAgICAgICAgICAgICAgICA8cD5BY2Vzc2UgYSBwYXJ0aXIgZGUgcXVhbHF1ZXIgZGlzcG9zaXRpdm8gbcOzdmVsLCBuw6NvIHNlIHByZW9jdXBlIGNvbSBhIHJlc29sdcOnw6NvLCBhanVzdGFtb3MgYSB0ZWxhIHBhcmEgYSBtZWxob3IgdmlzdWFsaXphw6fDo28uPC9wPg0KICAgICAgICAgICAgPC9saT4gICAgDQogICAgICAgIDwvdWw+DQoNCiAgICAgICAgPC9kaXY+DQoNCiAgICA8L3RkPg0KPC90cj4NCg0KPC90YWJsZT5kAg0PDxYIHilfX0FTTW9kYWxfRXJyb19WaWV3U3RhdGVBU01vZGFsQ29udGV1ZG9JRGUeJ19fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVDPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoB8DBQptb2RhbCBmYWRlHwQCAhYMHwUFAi0xHwYFBmRpYWxvZx8HBQR0cnVlHwgFBWZhbHNlHwkFBnN0YXRpYx8KBQR0cnVlZAIFDxYCHwAFSFNJU1RFTUEgVFJJQlVUJiMxOTM7UklPIFNFRkFaIC0gU2VjcmV0YXJpYSBkYSBGYXplbmRhIGRvIEVzdGFkbyBkYSBCYWhpYWQCBw8WAh8ABQ9BU0xJQjogMi4xLjEwLjBkZM0/dO9U5kfaYmhWFbFTCBA17kcC',
			'__VIEWSTATEGENERATOR'=>'CC7A3876',
			'__PREVIOUSPAGE'=>'',
			'__EVENTVALIDATION'=>'/wEdAApOHJQ9+CbscBlvbuZ1e/7wIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV46LBM9yZxItbePOFI7a2VLwUBa4=',
			'__ASYNCPOST'=>'false',
			'ctl00$PHCentro$userLogin'=>$dados_login['login'],
			'ctl00$PHCentro$userPass'=>$dados_login['senha'],
			'ctl00$PHCentro$btnLogin'=>'Entrar'
		);

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login?ReturnUrl=%2fsistemas%2fDTE%2fContribuinte%2fModulos%2fConsulta%2fDAE_consulta.aspx%3fNumeroInscricaoEstadual%3d".$this->apenas_numero($dados_login['ie'])."&NumeroInscricaoEstadual=".$this->apenas_numero($dados_login['ie']);

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);
		$dados_request_dae = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_dae['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_dae['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_dae['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;

	}

	public function consultar_ano($dados_login, $dados_request_dae){

		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHConteudo$btnBuscar',
			'__EVENTTARGET'=>'ctl00$PHConteudo$btnBuscar',
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=>$dados_request_dae['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=>$dados_request_dae['VIEWSTATEGENERATOR'],
			'__PREVIOUSPAGE'=>'MEFqmrRFdAFxiPWkBvrml0QKlBAcmdcupsQFts-MX_n5xihB8aP1uAouBODwaM34MNrOVnmK1-x0cmytVVJpJQ2',
			'__EVENTVALIDATION'=>$dados_request_dae['EVENTVALIDATION'],
			'ctl00$nomeUsuario:'=>'',
			'ctl00$PHConteudo$txtAno'=> $dados_login['ano'],
			'__ASYNCPOST'=>'false'
		);

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Consulta/DAE_consulta.aspx?NumeroInscricaoEstadual=".$this->apenas_numero($dados_login['ie']);
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt'); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
		return $store;
	}

	public function return_array($dados_sefaz){
		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($dados_sefaz);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);
		$elements = $xpath->query('//table[@id="PHConteudo_ASGridDAE"]/*/*/td');
		return $elements;
	}

	public function insert_dae_bd($dados_dae){
		$this->db->insert('dae', $dados_dae);
	}

	public function get_daes_bd($ie, $ano){
		$this->db->select('*');
		$this->db->from('dae');
		$this->db->where('ie', $ie);
		$this->db->like('referencia', $ano);
		$result = $this->db->get();
		return $result->result();
	}

	public function apenas_numero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	}


}