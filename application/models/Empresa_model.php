<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model{
	
	private $id;
	private $inscricao_estadual;
	private $cnpj;
	private $inscricao_estadual_completo;
	private $cnpj_completo;
	private $razao_social;
	private $mei;
	private $nome_fantasia;
	private $natureza_juridica;
	private $unidade_atendimento;
	private $unidade_fiscalizacao;
	private $cep;
	private $logradouro;
	private $numero;
	private $complemento;
	private $bairro;
	private $cidade;
	private $uf;
	private $referencia;
	private $localizacao;
	private $telefone;
	private $email;
	private $situacao_cadastral;
	private $situacao;
	private $motivo;
	private $id_contador;
	private $sync;
	private $login_sefaz;
	private $senha_sefaz;
	private $celular_alternativo;
	private $telefone_alternativo;
	private $email_alternativo;
	private $situacao_dte;
	private $situacao_conta_dte;
	private $atividade_principal;
	private $condicao;
	private $forma_pagamento;
	private $motivo_situacao_cadastral;
	private $nome_contador;
	private $crc_contador;
	private $nome_responsavel;
	private $crc_responsavel;
	private $data_cadastro;
	private $data_atualizacao;
	private $vinculo_contador;
	private $id_funcionario;
	private $flag_empresa_sem_ie;

	private $listaUsuario;

	private $senha_mei;
	private $login_mei;

	private $cpf_alvara;
	private $nire;
	private $anotacoes;

	private $tipoRegime;

	public function __construct(){
		parent::__construct();
	}
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getInscricaoEstadual() {
		return $this->inscricao_estadual;
	}
	public function setInscricaoEstadual($inscricao_estadual) {
		$this->inscricao_estadual = $inscricao_estadual;
		return $this;
	}
	public function getCnpj() {
		return $this->cnpj;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}
	public function getInscricaoEstadualCompleto() {
		return $this->inscricao_estadual_completo;
	}
	public function setInscricaoEstadualCompleto($inscricao_estadual_completo) {
		$this->inscricao_estadual_completo = $inscricao_estadual_completo;
		return $this;
	}
	public function getCnpjCompleto() {
		return $this->cnpj_completo;
	}
	public function setCnpjCompleto($cnpj_completo) {
		$this->cnpj_completo = $cnpj_completo;
		return $this;
	}
	public function getRazaoSocial() {
		return $this->razao_social;
	}
	public function setRazaoSocial($razao_social) {
		$this->razao_social = $razao_social;
		return $this;
	}
	public function getMei() {
		return $this->mei;
	}
	public function setMei($mei) {
		$this->mei = $mei;
		return $this;
	}
	public function getNomeFantasia() {
		return $this->nome_fantasia;
	}
	public function setNomeFantasia($nome_fantasia) {
		$this->nome_fantasia = $nome_fantasia;
		return $this;
	}
	public function getNaturezaJuridica() {
		return $this->natureza_juridica;
	}
	public function setNaturezaJuridica($natureza_juridica) {
		$this->natureza_juridica = $natureza_juridica;
		return $this;
	}
	public function getUnidadeAtendimento() {
		return $this->unidade_atendimento;
	}
	public function setUnidadeAtendimento($unidade_atendimento) {
		$this->unidade_atendimento = $unidade_atendimento;
		return $this;
	}
	public function getUnidadeFiscalizacao() {
		return $this->unidade_fiscalizacao;
	}
	public function setUnidadeFiscalizacao($unidade_fiscalizacao) {
		$this->unidade_fiscalizacao = $unidade_fiscalizacao;
		return $this;
	}
	public function getCep() {
		return $this->cep;
	}
	public function setCep($cep) {
		$this->cep = $cep;
		return $this;
	}
	public function getLogradouro() {
		return $this->logradouro;
	}
	public function setLogradouro($logradouro) {
		$this->logradouro = $logradouro;
		return $this;
	}
	public function getNumero() {
		return $this->numero;
	}
	public function setNumero($numero) {
		$this->numero = $numero;
		return $this;
	}
	public function getComplemento() {
		return $this->complemento;
	}
	public function setComplemento($complemento) {
		$this->complemento = $complemento;
		return $this;
	}
	public function getBairro() {
		return $this->bairro;
	}
	public function setBairro($bairro) {
		$this->bairro = $bairro;
		return $this;
	}
	public function getCidade() {
		return $this->cidade;
	}
	public function setCidade($cidade) {
		$this->cidade = $cidade;
		return $this;
	}
	public function getUf() {
		return $this->uf;
	}
	public function setUf($uf) {
		$this->uf = $uf;
		return $this;
	}
	public function getReferencia() {
		return $this->referencia;
	}
	public function setReferencia($referencia) {
		$this->referencia = $referencia;
		return $this;
	}
	public function getLocalizacao() {
		return $this->localizacao;
	}
	public function setLocalizacao($localizacao) {
		$this->localizacao = $localizacao;
		return $this;
	}
	public function getTelefone() {
		return $this->telefone;
	}
	public function setTelefone($telefone) {
		$this->telefone = $telefone;
		return $this;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}
	public function getSituacaoCadastral() {
		return $this->situacao_cadastral;
	}
	public function setSituacaoCadastral($situacao_cadastral) {
		$this->situacao_cadastral = $situacao_cadastral;
		return $this;
	}
	public function getSituacao() {
		return $this->situacao;
	}
	public function setSituacao($situacao) {
		$this->situacao = $situacao;
		return $this;
	}
	public function getMotivo() {
		return $this->motivo;
	}
	public function setMotivo($motivo) {
		$this->motivo = $motivo;
		return $this;
	}
	public function getIdContador() {
		return $this->id_contador;
	}
	public function setIdContador($id_contador) {
		$this->id_contador = $id_contador;
		return $this;
	}
	public function getDataCadastro() {
		return $this->data_cadastro;
	}
	public function setDataCadastro($data_cadastro) {
		$this->data_cadastro = $data_cadastro;
		return $this;
	}
	public function getDataAtualizacao() {
		return $this->data_atualizacao;
	}
	public function setDataAtualizacao($data_atualizacao) {
		$this->data_atualizacao = $data_atualizacao;
		return $this;
	}
	public function getSync() {
		return $this->sync;
	}
	public function setSync($sync) {
		$this->sync = $sync;
		return $this;
	}
	public function getCelularAlternativo() {
		return $this->celular_alternativo;
	}
	public function setCelularAlternativo($celular_alternativo) {
		$this->celular_alternativo = $celular_alternativo;
		return $this;
	}
	public function getTelefoneAlternativo() {
		return $this->telefone_alternativo;
	}
	public function setTelefoneAlternativo($telefone_alternativo) {
		$this->telefone_alternativo = $telefone_alternativo;
		return $this;
	}
	public function getEmailAlternativo() {
		return $this->email_alternativo;
	}
	public function setEmailAlternativo($email_alternativo) {
		$this->email_alternativo = $email_alternativo;
		return $this;
	}

	public function getLoginSefaz() {
		return $this->login_sefaz;
	}

	public function setLoginSefaz($login_sefaz) {
		$this->login_sefaz = $login_sefaz;
		return $this;
	}

	public function getSenhaSefaz() {
		return $this->senha_sefaz;
	}
	public function setSenhaSefaz($senha_sefaz) {
		$this->senha_sefaz = $senha_sefaz;
		return $this;
	}
	public function getSituacaoDte() {
		return $this->situacao_dte;
	}
	public function setSituacaoDte($situacao_dte) {
		$this->situacao_dte = $situacao_dte;
		return $this;
	}
	public function getSituacaoContaDte() {
		return $this->situacao_conta_dte;
	}
	public function setSituacaoContaDte($situacao_conta_dte) {
		$this->situacao_conta_dte = $situacao_conta_dte;
		return $this;
	}
	public function getAtividadePrincipal() {
		return $this->atividade_principal;
	}
	public function setAtividadePrincipal($atividade_principal) {
		$this->atividade_principal = $atividade_principal;
		return $this;
	}
	public function getCondicao() {
		return $this->condicao;
	}
	public function setCondicao($condicao) {
		$this->condicao = $condicao;
		return $this;
	}
	public function getFormaPagamento() {
		return $this->forma_pagamento;
	}
	public function setFormaPagamento($forma_pagamento) {
		$this->forma_pagamento = $forma_pagamento;
		return $this;
	}
	public function getMotivoSituacaoCadastral() {
		return $this->motivo_situacao_cadastral;
	}
	public function setMotivoSituacaoCadastral($motivo_situacao_cadastral) {
		$this->motivo_situacao_cadastral = $motivo_situacao_cadastral;
		return $this;
	}
	public function getNomeContador() {
		return $this->nome_contador;
	}
	public function setNomeContador($nome_contador) {
		$this->nome_contador = $nome_contador;
		return $this;
	}
	public function getCrcContador() {
		return $this->crc_contador;
	}
	public function setCrcContador($crc_contador) {
		$this->crc_contador = $crc_contador;
		return $this;
	}
	public function getNomeResponsavel() {
		return $this->nome_responsavel;
	}
	public function setNomeResponsavel($nome_responsavel) {
		$this->nome_responsavel = $nome_responsavel;
		return $this;
	}
	public function getCrcResponsavel() {
		return $this->crc_responsavel;
	}
	public function setCrcResponsavel($crc_responsavel) {
		$this->crc_responsavel = $crc_responsavel;
		return $this;
	}
	public function getVinculoContador() {
		return $this->vinculo_contador;
	}
	public function setVinculoContador($vinculo_contador) {
		$this->vinculo_contador = $vinculo_contador;
		return $this;
	}

	public function getIdFuncionario() {
		return $this->id_funcionario;
	}
	public function setIdFuncionario($id_funcionario) {
		$this->id_funcionario = $id_funcionario;
		return $this;
	}

	public function getSenhaMei() {
		return $this->senha_mei;
	}
	public function setSenhaMei($senha_mei) {
		$this->senha_mei = $senha_mei;
		return $this;
	}

	public function getLoginMei() {
		return $this->login_mei;
	}
	public function setLoginMei($login_mei) {
		$this->login_mei = $login_mei;
		return $this;
	}

	public function getFlagEmpresaSemIe(){
		return $this->flag_empresa_sem_ie;
	}

	public function setFlagEmpresaSemIe($flag_empresa_sem_ie){
		$this->flag_empresa_sem_ie = $flag_empresa_sem_ie;
		return $this;
	}

	public function getListaUsuario(){
		return $this->listaUsuario;
	}

	public function setListaUsuario($listaUsuario){
		$this->listaUsuario = $listaUsuario;
		return $this;
	}

	public function getCpfAlvara() {
		return $this->cpf_alvara;
	}
	public function setCpfAlvara($cpf_alvara) {
		$this->cpf_alvara = $cpf_alvara;
		return $this;
	}

	public function getNire() {
		return $this->nire;
	}
	public function setNire($nire) {
		$this->nire = $nire;
		return $this;
	}

	public function getAnotacoes() {
		return $this->anotacoes;
	}
	public function setAnotacoes($anotacoes) {
		$this->anotacoes = $anotacoes;
		return $this;
	}

	public function getTipoRegime() {
		return $this->tipoRegime;
	}
	public function setTipoRegime($tipoRegime) {
		$this->tipoRegime = $tipoRegime;
		return $this;
	}

	public function cadastrar(){
	
		$dados = array(
				'inscricao_estadual' => $this->getInscricaoEstadual(),
				'cnpj' => $this->getCnpj(),
				'inscricao_estadual_completo' => $this->getInscricaoEstadualCompleto(),
				'cnpj_completo' => $this->getCnpjCompleto(),
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'mei' => $this->getMei(),
				'natureza_juridica' => $this->getNaturezaJuridica(),
				'unidade_atendimento' => $this->getUnidadeAtendimento(),
				'unidade_fiscalizacao' => $this->getUnidadeFiscalizacao(),
				'cep' => $this->getCep(),
				'logradouro' => $this->getLogradouro(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cidade' => $this->getCidade(),
				'uf' => $this->getUf(),
				'referencia' => $this->getReferencia(),
				'localizacao' => $this->getLocalizacao(),
				'telefone' => $this->getTelefone(),
				'email' => $this->getEmail(),
				'telefone_alternativo' => $this->getTelefoneAlternativo(),
				'celular_alternativo' => $this->getCelularAlternativo(),
				'email_alternativo' => $this->getEmailAlternativo(),
				'situacao_cadastral' => $this->getSituacaoCadastral(),
				'situacao' => $this->getSituacao(),
				'motivo' => $this->getMotivo(),
				'situacao_dte' => $this->getSituacaoDte(),
				'situacao_conta_dte' => $this->getSituacaoContaDte(),
				'atividade_principal' => $this->getAtividadePrincipal(),
				'condicao' => $this->getCondicao(),
				'forma_pagamento' => $this->getFormaPagamento(),
				'motivo_situacao_cadastral' => $this->getMotivoSituacaoCadastral(),
				'nome_contador' => $this->getNomeContador(),
				'crc_contador' => $this->getCrcContador(),
				'nome_responsavel' => $this->getNomeResponsavel(),
				'crc_responsavel' => $this->getCrcResponsavel(),				
				'senha_sefaz' => $this->getSenhaSefaz(),
				'id_contador' => $this->getIdContador(),
				'sync' => $this->getSync(),
				'vinculo_contador' => $this->getVinculoContador(),
				'data_cadastro' => $this->getDataCadastro(),
				'login_sefaz' => $this->getLoginSefaz(),
				'login_mei' => $this->getLoginMei(),
				'senha_mei' => $this->getSenhaMei(),
				'cpf_alvara' => $this->getCpfAlvara(),
				'nire' => $this->getNire(),
				'anotacoes' => $this->getAnotacoes(),
				'id_funcionario' => $this->getIdFuncionario(),
				'flag_empresa_sem_ie' => $this->getFlagEmpresaSemIe()
		);
	
		if ($this->db->insert('dtb_empresas', $dados)){

			$id = $this->db->insert_id();
			//$this->atualizaListaEmpresaUsuario($id, $this->getListaUsuario());
			
			return TRUE;
		} else {
			return FALSE;
		}
	
	
	}


	public function cadastrar_novatela(){
	
		$dados = array(
				'inscricao_estadual' => $this->getInscricaoEstadual(),
				'cnpj' => $this->getCnpj(),
				'inscricao_estadual_completo' => $this->getInscricaoEstadualCompleto(),
				'cnpj_completo' => $this->getCnpjCompleto(),
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'mei' => $this->getMei(),
				'natureza_juridica' => $this->getNaturezaJuridica(),
				'unidade_atendimento' => $this->getUnidadeAtendimento(),
				'unidade_fiscalizacao' => $this->getUnidadeFiscalizacao(),
				'cep' => $this->getCep(),
				'logradouro' => $this->getLogradouro(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cidade' => $this->getCidade(),
				'uf' => $this->getUf(),
				'referencia' => $this->getReferencia(),
				'localizacao' => $this->getLocalizacao(),
				'telefone' => $this->getTelefone(),
				'email' => $this->getEmail(),
				'telefone_alternativo' => $this->getTelefoneAlternativo(),
				'celular_alternativo' => $this->getCelularAlternativo(),
				'email_alternativo' => $this->getEmailAlternativo(),
				'situacao_cadastral' => $this->getSituacaoCadastral(),
				'situacao' => $this->getSituacao(),
				'motivo' => $this->getMotivo(),
				'situacao_dte' => $this->getSituacaoDte(),
				'situacao_conta_dte' => $this->getSituacaoContaDte(),
				'atividade_principal' => $this->getAtividadePrincipal(),
				'condicao' => $this->getCondicao(),
				'forma_pagamento' => $this->getFormaPagamento(),
				'motivo_situacao_cadastral' => $this->getMotivoSituacaoCadastral(),
				'nome_contador' => $this->getNomeContador(),
				'crc_contador' => $this->getCrcContador(),
				'nome_responsavel' => $this->getNomeResponsavel(),
				'crc_responsavel' => $this->getCrcResponsavel(),				
				'senha_sefaz' => $this->getSenhaSefaz(),
				'id_contador' => $this->getIdContador(),
				'sync' => $this->getSync(),
				'vinculo_contador' => $this->getVinculoContador(),
				'data_cadastro' => $this->getDataCadastro(),
				'login_sefaz' => $this->getLoginSefaz(),
				'login_mei' => $this->getLoginMei(),
				'senha_mei' => $this->getSenhaMei(),
				'cpf_alvara' => $this->getCpfAlvara(),
				'nire' => $this->getNire(),
				'anotacoes' => $this->getAnotacoes(),
				'id_funcionario' => $this->getIdFuncionario(),
				'flag_empresa_sem_ie' => $this->getFlagEmpresaSemIe(),
				'tipo_regime' => $this->getTipoRegime()
		);
	
		if ($this->db->insert('dtb_empresas', $dados)){

			$id = $this->db->insert_id();
			$this->atualizaListaEmpresaUsuario($id, $this->getListaUsuario());
			
			return $id;
		} else {
			return FALSE;
		}
	
	
	}
	
	public function pesquisar_sefaz_ie() {
		
		$dados = http_build_query(array(
				'IE' => $this->getInscricaoEstadual(),
				'B2' => 'IE  ->'
		));
		
		$contexto = stream_context_create(array(
				'http' => array(
						'method' => 'POST',
						'content' => $dados,
						'header' => "Content-type: application/x-www-form-urlencoded\r\n"
						. "Content-Length: " . strlen($dados) . "\r\n",
				)
		));
		
		$resposta = file('https://www.sefaz.ba.gov.br/scripts/cadastro/cadastroBa/result.asp', null, $contexto);	
		
		if (!empty($resposta)){
			
			for ($i=0; $i <= 90 ; $i++) {
				unset($resposta[$i]);
			}
			
			$cnpj = trim(html_entity_decode(strip_tags($resposta[108])));
			$cnpj = preg_replace('/\s\s+/', ' ', substr($cnpj, 7));
			//echo "- CNPJ: $cnpj<br>";
			$array = array();
			
			$array['cnpj'] = $cnpj;
			
			$ie = trim(html_entity_decode(strip_tags($resposta[110])));
			$ie = preg_replace('/\s\s+/', ' ', substr($ie, 23));
			//echo "- Inscrição Estadual: $ie<br>";
			$array['ie'] = $ie;
			
			$contribuinte_mei = trim(html_entity_decode(strip_tags($resposta[115])));
			$contribuinte_mei = preg_replace('/\s\s+/', ' ', substr($contribuinte_mei, 48));
			//echo "..Contribuinte Micro Empreendedor Individual: $contribuinte_mei<br>";
			$array['mei'] = $contribuinte_mei;
			
			$razao_social = trim(html_entity_decode(strip_tags($resposta[120])));
			$razao_social = preg_replace('/\s\s+/', ' ', substr($razao_social, 16));
			//echo "- Razão Social: $razao_social<br>";
			$array['razao_social'] = $razao_social;
			
			if ($contribuinte_mei == 'Sim') {
			
				$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[129])));
				$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
				//echo "- Nome Fantasia: $nome_fantasia<br>";
				$array['nome_fantasia'] = $nome_fantasia;
			
				$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[133])));
				$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
				//echo "- Natureza Jurídica: $natureza_juridica<br>";
				$array['natureza_juridica'] = $natureza_juridica;
			
				$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[137])));
				$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
				//echo "- Unidade de Atendimento: $unidade_atendimento<br>";
				$array['unidade_atendimento'] = $unidade_atendimento;
			
				$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[140])));
				$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
				//echo "- Unidade de Fiscalização: $unidade_fiscalizacao<br><br>";
				$array['unidade_fiscalizacao'] = $unidade_fiscalizacao;
			
				//echo "Endereço<br>";
			
				$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[153])));
				$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
				//echo "- Logradouro: $logradouro_empresa<br>";
				$array['logradouro'] = $logradouro_empresa;
			
				$numero_empresa = trim(html_entity_decode(strip_tags($resposta[156])));
				$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
				//echo "- Número: $numero_empresa<br>";
				$array['numero'] = $numero_empresa;
			
				$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[157])));
				$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
				//echo "- Complemento: $complemento_empresa<br>";
				$array['complemento'] = $complemento_empresa;
			
				$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[160])));
				$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
				//echo "- Bairro/Distrito: $bairro_empresa<br>";
				$array['bairro'] = $bairro_empresa;
			
				$cep_empresa = trim(html_entity_decode(strip_tags($resposta[161])));
				$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
				//echo "- CEP: $cep_empresa<br>";
				$array['cep'] = $cep_empresa;
			
				$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[164])));
				$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
				//echo "- Município: $municipio_empresa<br>";
				$array['cidade'] = $municipio_empresa;
			
				$uf_empresa = trim(html_entity_decode(strip_tags($resposta[165])));
				$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
				//echo "- UF: $uf_empresa<br>";
				$array['uf'] = $uf_empresa;
			
				$telefone = trim(html_entity_decode(strip_tags($resposta[168])));
				$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
				//echo "- Telefone: $telefone<br>";
				$array['telefone'] = $telefone;
			
				$email = trim(html_entity_decode(strip_tags($resposta[169])));
				$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
				//echo "- E-mail: $email<br>";
				$array['email'] = $email;
			
				$referencia = trim(html_entity_decode(strip_tags($resposta[173])));
				$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
				//echo "- Referência: $referencia<br>";
				$array['referencia'] = $referencia;
			
				$localizacao = trim(html_entity_decode(strip_tags($resposta[174])));
				$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
				//echo "- Localização: $localizacao<br><br>";
				$array['localizacao'] = $localizacao;
			
			} else {
			
				$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[127])));
				$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
				//echo "- Nome Fantasia: $nome_fantasia<br>";
				$array['nome_fantasia'] = $nome_fantasia;
			
				$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[131])));
				$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
				//echo "- Natureza Jurídica: $natureza_juridica<br>";
				$array['natureza_juridica'] = $natureza_juridica;
			
				$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[135])));
				$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
				//echo "- Unidade de Atendimento: $unidade_atendimento<br>";
				$array['unidade_atendimento'] = $unidade_atendimento;
			
				$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[138])));
				$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
				//echo "- Unidade de Fiscalização: $unidade_fiscalizacao<br><br>";
				$array['unidade_fiscalizacao'] = $unidade_fiscalizacao;
			
				//echo "Endereço<br>";
			
				$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[151])));
				$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
				//echo "- Logradouro: $logradouro_empresa<br>";
				$array['logradouro'] = $logradouro_empresa;
			
				$numero_empresa = trim(html_entity_decode(strip_tags($resposta[154])));
				$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
				//echo "- Número: $numero_empresa<br>";
				$array['numero'] = $numero_empresa;
			
				$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[155])));
				$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
				//echo "- Complemento: $complemento_empresa<br>";
				$array['complemento'] = $complemento_empresa;
			
				$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[158])));
				$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
				//echo "- Bairro/Distrito: $bairro_empresa<br>";
				$array['bairro'] = $bairro_empresa;
			
				$cep_empresa = trim(html_entity_decode(strip_tags($resposta[159])));
				$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
				//echo "- CEP: $cep_empresa<br>";
				$array['cep'] = $cep_empresa;
			
				$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[162])));
				$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
				//echo "- Município: $municipio_empresa<br>";
				$array['cidade'] = $municipio_empresa;
			
				$uf_empresa = trim(html_entity_decode(strip_tags($resposta[163])));
				$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
				//echo "- UF: $uf_empresa<br>";
				$array['uf'] = $uf_empresa;
			
				$telefone = trim(html_entity_decode(strip_tags($resposta[166])));
				$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
				//echo "- Telefone: $telefone<br>";
				$array['telefone'] = $telefone;
			
				$email = trim(html_entity_decode(strip_tags($resposta[167])));
				$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
				//echo "- E-mail: $email<br>";
				$array['email'] = $email;
			
				$referencia = trim(html_entity_decode(strip_tags($resposta[171])));
				$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
				//echo "- Referência: $referencia<br>";
				$array['referencia'] = $referencia;
			
				$localizacao = trim(html_entity_decode(strip_tags($resposta[172])));
				$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
				//echo "- Localização: $localizacao<br><br>";
				$array['localizacao'] = $localizacao;
			
			}
						
			//DADOS DO CONTADOR
			
			/*
			for ($i=90; $i <= 233 ; $i++) {
				unset($resposta[$i]);
			}
			*/
			$array['atividade_principal'] = '';
						
			$linhas = array();
			
			foreach ($resposta as $key => $value) {
				$text = strip_tags($value);
				$linhas[] = trim(html_entity_decode($text));
			}
			
			$texto = implode(' ', $linhas);
			
			$ini_dte = strpos($texto, 'Situação do DTE:');
			$texto1 = substr($texto, $ini_dte);
			$fim_dte = strpos($texto1, 'Data do Credenciamento:');
			
			$situacao_dte = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_dte))));
			$situacao_dte = preg_replace('/\s\s+/', ' ', substr($situacao_dte, 21));
			$array['situacao_dte'] = $situacao_dte;
			
			$ini_conta_dte = strpos($texto1, 'Situação da Conta:');
			$texto2 = substr($texto1, $ini_conta_dte);
			$fim_conta_dte = strpos($texto2, 'Data da Criação da Conta:');
			
			$situacao_conta_dte = trim(html_entity_decode(strip_tags(substr($texto2, 0, $fim_conta_dte))));
			$situacao_conta_dte = preg_replace('/\s\s+/', ' ', substr($situacao_conta_dte, 23));
			$array['situacao_conta_dte'] = $situacao_conta_dte;
			
			$ini_condicao = strripos($texto2, 'Condição:');
			$texto3 = substr($texto2, $ini_condicao);
			$fim_condicao = strpos($texto3, 'Forma de pagamento:');
			
			$condicao = trim(html_entity_decode(strip_tags(substr($texto3, 0, $fim_condicao))));
			$condicao = preg_replace('/\s\s+/', ' ', substr($condicao, 13));
			$array['condicao'] = $condicao;
			
			$texto4 = substr($texto3, $fim_condicao);
			
			$ini_forma_pagamento = strpos($texto4, 'Forma de pagamento:');
			$texto5 = substr($texto4, $ini_forma_pagamento);
			$fim_forma_pagamento = strpos($texto5, 'Situação Cadastral Vigente:');
			
			$forma_pagamento = trim(html_entity_decode(strip_tags(substr($texto5, 0, $fim_forma_pagamento))));
			$forma_pagamento = preg_replace('/\s\s+/', ' ', substr($forma_pagamento, 21));
			$array['forma_pagamento'] = $forma_pagamento;
			
			$ini_motivo_situacao_cadastral = strpos($texto5, 'Motivo desta Situação Cadastral:');
			
			if ($ini_motivo_situacao_cadastral) {
			
				$texto6 = substr($texto5, $ini_motivo_situacao_cadastral);
				$fim_motivo_situacao_cadastral = strpos($texto6, 'Endereço de Correspondência');
			
				$motivo_situacao_cadastral = trim(html_entity_decode(strip_tags(substr($texto6, 0, $fim_motivo_situacao_cadastral))));
				$motivo_situacao_cadastral = preg_replace('/\s\s+/', ' ', substr($motivo_situacao_cadastral, 35));
				$array['motivo_situacao_cadastral'] = $motivo_situacao_cadastral;
			
			} else {
				$array['motivo_situacao_cadastral'] = '';
			}
			
			$ini_atividade_economica = strripos($texto2, 'Atividade Econômica Principal:');
			$texto_atividade = substr($texto2, $ini_atividade_economica);
			$fim_atividade_economica = '';
			
			if (strpos($texto_atividade, 'Atividade Econômica Secundária')) {
				$fim_atividade_economica = strpos($texto_atividade, 'Atividade Econômica Secundária');
			} else {
				$fim_atividade_economica = strpos($texto_atividade, 'Unidade:');
			}
			
			$atividade_principal = trim(html_entity_decode(strip_tags(substr($texto_atividade, 0, $fim_atividade_economica))));
			$atividade_principal = preg_replace('/\s\s+/', ' ', substr($atividade_principal, 31));
			$array['atividade_principal'] = $atividade_principal;
			
			$cl = strpos($texto, 'Classificação CRC:');
			$texto17 = substr($texto, $cl);
			
			// CRC CONTADOR
			$crc = strpos($texto17, 'Tipo CRC:');
			$texto_crc_contador = substr($texto17, 0, $crc);
			$crc_contador = strripos($texto_crc_contador, 'CRC:');
			
			$crc_contador = substr($texto_crc_contador, $crc_contador);
			$crc_contador = trim(html_entity_decode(strip_tags($crc_contador)));
			$crc_contador = preg_replace('/\s\s+/', ' ', substr($crc_contador, 6));
			$array['crc_contador'] = preg_replace("/[^0-9]/", "", $crc_contador);
			
			$texto17 = substr($texto, $cl);
			$texto18 = substr($texto17, $crc);
			$nom = strpos($texto18, 'Nome:');
			$texto19 = substr($texto18, $nom);
			
			$resp = strpos($texto19, 'Responsável pela organização contábil');
			
			// NOME CONTADOR
			$nome_contador = trim(html_entity_decode(strip_tags(substr($texto19, 0, $resp))));
			$array['nome_contador'] = preg_replace('/\s\s+/', ' ', substr($nome_contador, 7));
			
			$texto20 = substr($texto19, $resp);
			$classi = strpos($texto20, 'Classificação CRC:');
			$texto21 = substr($texto20, $classi);
			
			// CRC RESPONSAVEL
			$crc_resp = strpos($texto21, 'Tipo CRC:');
			$texto_crc_responsavel = substr($texto21, 0, $crc_resp);
			$crc_responsavel = strripos($texto_crc_responsavel, 'CRC:');
			
			$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($texto_crc_responsavel, $crc_responsavel));
			$crc_responsavel = trim(html_entity_decode(strip_tags($crc_responsavel)));
			$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($crc_responsavel, 5));
			$array['crc_responsavel'] = preg_replace("/[^0-9]/", "", $crc_responsavel);
			
			// NOME RESPONSAVEL
			$nomeresp = strpos($texto21, 'Nome:');
			$texto22 = substr($texto21, $nomeresp);
			
			$endresp = strpos($texto22, 'Endereço');
			$nome_responsavel = trim(html_entity_decode(strip_tags(substr($texto22, 0, $endresp))));
			$array['nome_responsavel'] = preg_replace('/\s\s+/', ' ', substr($nome_responsavel, 7));
			
			//----- PARTE 2 -----
			
			$dado_dois = http_build_query(array(
					'num_inscricao' => $this->getInscricaoEstadual()
			));
			
			$contexto_dois = stream_context_create(array(
					'http' => array(
							'method' => 'POST',
							'content' => $dado_dois,
							'header' => "Content-type: application/x-www-form-urlencoded\r\n"
							. "Content-Length: " . strlen($dado_dois) . "\r\n",
					)
			));
			
			$resposta_dois = file_get_contents('https://www.sefaz.ba.gov.br/scripts/antc/result_consulta.asp', null, $contexto_dois);
			
			$lines = array();
			
			preg_match_all('#<tr[^>]*>(.*?)</tr>#is', $resposta_dois, $lines);
			
			unset($lines[0][0]);
			unset($lines[0][1]);
			unset($lines[0][2]);
			unset($lines[0][4]);
			unset($lines[0][8]);
			unset($lines[0][11]);
			unset($lines[0][12]);
			
			$results = array();
			foreach ($lines[0] as $k => $line) {
				preg_match_all('#<TR[^>]*>(.*?)</TR>#is', $line, $cell);
			
				foreach ($cell[1] as $cell) {
					$text = strip_tags($cell);
					$results[$k][] = trim(html_entity_decode($text));
				}
			}
			
			$inicio_situacao_cadastral = strripos($results[3][0], 'Situação Cadastral:');
			$situacao_cadastral = substr($results[3][0], $inicio_situacao_cadastral);
			
			$array['situacao_cadastral'] = preg_replace('/\s\s+/', ' ', substr($situacao_cadastral, 22));
			$array['credenciado'] = preg_replace('/\s\s+/', ' ', substr($results[9][0], 13));
			$array['motivo'] = preg_replace('/\s\s+/', ' ', substr($results[10][0], 10));
			
			return $array;
			
		} else {
			return 'erro';
		}
		
		
	
		
	}
	
	public function pesquisar_sefaz_cnpj() {
	
		$dados = http_build_query(array(
				'CGC' => $this->getCnpj(),
				'B1' => 'CNPJ  ->'
		));
	
		$contexto = stream_context_create(array(
				'http' => array(
						'method' => 'POST',
						'content' => $dados,
						'header' => "Content-type: application/x-www-form-urlencoded\r\n"
						. "Content-Length: " . strlen($dados) . "\r\n",
				)
		));
	
		$resposta = file('http://www.sefaz.ba.gov.br/scripts/cadastro/cadastroBa/result.asp', null, $contexto);
	
		if (!empty($resposta)){
				
			for ($i=0; $i <= 90 ; $i++) {
				unset($resposta[$i]);
			}
				
			$cnpj = trim(html_entity_decode(strip_tags($resposta[108])));
			$cnpj = preg_replace('/\s\s+/', ' ', substr($cnpj, 7));
			//echo "- CNPJ: $cnpj<br>";
			$array = array();
				
			$array['cnpj'] = $cnpj;
				
			$ie = trim(html_entity_decode(strip_tags($resposta[110])));
			$ie = preg_replace('/\s\s+/', ' ', substr($ie, 23));
			//echo "- Inscrição Estadual: $ie<br>";
			$array['ie'] = $ie;
				
			$contribuinte_mei = trim(html_entity_decode(strip_tags($resposta[115])));
			$contribuinte_mei = preg_replace('/\s\s+/', ' ', substr($contribuinte_mei, 48));
			//echo "..Contribuinte Micro Empreendedor Individual: $contribuinte_mei<br>";
			$array['mei'] = $contribuinte_mei;
				
			$razao_social = trim(html_entity_decode(strip_tags($resposta[120])));
			$razao_social = preg_replace('/\s\s+/', ' ', substr($razao_social, 16));
			//echo "- Razão Social: $razao_social<br>";
			$array['razao_social'] = $razao_social;
				
			if ($contribuinte_mei == 'Sim') {
					
				$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[129])));
				$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
				//echo "- Nome Fantasia: $nome_fantasia<br>";
				$array['nome_fantasia'] = $nome_fantasia;
					
				$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[133])));
				$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
				//echo "- Natureza Jurídica: $natureza_juridica<br>";
				$array['natureza_juridica'] = $natureza_juridica;
					
				$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[137])));
				$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
				//echo "- Unidade de Atendimento: $unidade_atendimento<br>";
				$array['unidade_atendimento'] = $unidade_atendimento;
					
				$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[140])));
				$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
				//echo "- Unidade de Fiscalização: $unidade_fiscalizacao<br><br>";
				$array['unidade_fiscalizacao'] = $unidade_fiscalizacao;
					
				//echo "Endereço<br>";
					
				$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[153])));
				$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
				//echo "- Logradouro: $logradouro_empresa<br>";
				$array['logradouro'] = $logradouro_empresa;
					
				$numero_empresa = trim(html_entity_decode(strip_tags($resposta[156])));
				$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
				//echo "- Número: $numero_empresa<br>";
				$array['numero'] = $numero_empresa;
					
				$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[157])));
				$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
				//echo "- Complemento: $complemento_empresa<br>";
				$array['complemento'] = $complemento_empresa;
					
				$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[160])));
				$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
				//echo "- Bairro/Distrito: $bairro_empresa<br>";
				$array['bairro'] = $bairro_empresa;
					
				$cep_empresa = trim(html_entity_decode(strip_tags($resposta[161])));
				$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
				//echo "- CEP: $cep_empresa<br>";
				$array['cep'] = $cep_empresa;
					
				$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[164])));
				$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
				//echo "- Município: $municipio_empresa<br>";
				$array['cidade'] = $municipio_empresa;
					
				$uf_empresa = trim(html_entity_decode(strip_tags($resposta[165])));
				$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
				//echo "- UF: $uf_empresa<br>";
				$array['uf'] = $uf_empresa;
					
				$telefone = trim(html_entity_decode(strip_tags($resposta[168])));
				$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
				//echo "- Telefone: $telefone<br>";
				$array['telefone'] = $telefone;
					
				$email = trim(html_entity_decode(strip_tags($resposta[169])));
				$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
				//echo "- E-mail: $email<br>";
				$array['email'] = $email;
					
				$referencia = trim(html_entity_decode(strip_tags($resposta[173])));
				$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
				//echo "- Referência: $referencia<br>";
				$array['referencia'] = $referencia;
					
				$localizacao = trim(html_entity_decode(strip_tags($resposta[174])));
				$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
				//echo "- Localização: $localizacao<br><br>";
				$array['localizacao'] = $localizacao;
					
			} else {
					
				$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[127])));
				$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
				//echo "- Nome Fantasia: $nome_fantasia<br>";
				$array['nome_fantasia'] = $nome_fantasia;
					
				$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[131])));
				$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
				//echo "- Natureza Jurídica: $natureza_juridica<br>";
				$array['natureza_juridica'] = $natureza_juridica;
					
				$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[135])));
				$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
				//echo "- Unidade de Atendimento: $unidade_atendimento<br>";
				$array['unidade_atendimento'] = $unidade_atendimento;
					
				$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[138])));
				$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
				//echo "- Unidade de Fiscalização: $unidade_fiscalizacao<br><br>";
				$array['unidade_fiscalizacao'] = $unidade_fiscalizacao;
					
				//echo "Endereço<br>";
					
				$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[151])));
				$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
				//echo "- Logradouro: $logradouro_empresa<br>";
				$array['logradouro'] = $logradouro_empresa;
					
				$numero_empresa = trim(html_entity_decode(strip_tags($resposta[154])));
				$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
				//echo "- Número: $numero_empresa<br>";
				$array['numero'] = $numero_empresa;
					
				$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[155])));
				$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
				//echo "- Complemento: $complemento_empresa<br>";
				$array['complemento'] = $complemento_empresa;
					
				$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[158])));
				$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
				//echo "- Bairro/Distrito: $bairro_empresa<br>";
				$array['bairro'] = $bairro_empresa;
					
				$cep_empresa = trim(html_entity_decode(strip_tags($resposta[159])));
				$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
				//echo "- CEP: $cep_empresa<br>";
				$array['cep'] = $cep_empresa;
					
				$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[162])));
				$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
				//echo "- Município: $municipio_empresa<br>";
				$array['cidade'] = $municipio_empresa;
					
				$uf_empresa = trim(html_entity_decode(strip_tags($resposta[163])));
				$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
				//echo "- UF: $uf_empresa<br>";
				$array['uf'] = $uf_empresa;
					
				$telefone = trim(html_entity_decode(strip_tags($resposta[166])));
				$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
				//echo "- Telefone: $telefone<br>";
				$array['telefone'] = $telefone;
					
				$email = trim(html_entity_decode(strip_tags($resposta[167])));
				$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
				//echo "- E-mail: $email<br>";
				$array['email'] = $email;
					
				$referencia = trim(html_entity_decode(strip_tags($resposta[171])));
				$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
				//echo "- Referência: $referencia<br>";
				$array['referencia'] = $referencia;
					
				$localizacao = trim(html_entity_decode(strip_tags($resposta[172])));
				$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
				//echo "- Localização: $localizacao<br><br>";
				$array['localizacao'] = $localizacao;
					
			}
		
			//DADOS DO CONTADOR
				
			/*
			 for ($i=90; $i <= 233 ; $i++) {
			 unset($resposta[$i]);
			 }
			 */
			$array['atividade_principal'] = '';
		
			$linhas = array();
				
			foreach ($resposta as $key => $value) {
				$text = strip_tags($value);
				$linhas[] = trim(html_entity_decode($text));
			}
				
			$texto = implode(' ', $linhas);
				
			$ini_dte = strpos($texto, 'Situação do DTE:');
			$texto1 = substr($texto, $ini_dte);
			$fim_dte = strpos($texto1, 'Data do Credenciamento:');
				
			$situacao_dte = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_dte))));
			$situacao_dte = preg_replace('/\s\s+/', ' ', substr($situacao_dte, 21));
			$array['situacao_dte'] = $situacao_dte;
				
			$ini_conta_dte = strpos($texto1, 'Situação da Conta:');
			$texto2 = substr($texto1, $ini_conta_dte);
			$fim_conta_dte = strpos($texto2, 'Data da Criação da Conta:');
				
			$situacao_conta_dte = trim(html_entity_decode(strip_tags(substr($texto2, 0, $fim_conta_dte))));
			$situacao_conta_dte = preg_replace('/\s\s+/', ' ', substr($situacao_conta_dte, 23));
			$array['situacao_conta_dte'] = $situacao_conta_dte;
				
			$ini_condicao = strripos($texto2, 'Condição:');
			$texto3 = substr($texto2, $ini_condicao);
			$fim_condicao = strpos($texto3, 'Forma de pagamento:');
				
			$condicao = trim(html_entity_decode(strip_tags(substr($texto3, 0, $fim_condicao))));
			$condicao = preg_replace('/\s\s+/', ' ', substr($condicao, 13));
			$array['condicao'] = $condicao;
				
			$texto4 = substr($texto3, $fim_condicao);
				
			$ini_forma_pagamento = strpos($texto4, 'Forma de pagamento:');
			$texto5 = substr($texto4, $ini_forma_pagamento);
			$fim_forma_pagamento = strpos($texto5, 'Situação Cadastral Vigente:');
				
			$forma_pagamento = trim(html_entity_decode(strip_tags(substr($texto5, 0, $fim_forma_pagamento))));
			$forma_pagamento = preg_replace('/\s\s+/', ' ', substr($forma_pagamento, 21));
			$array['forma_pagamento'] = $forma_pagamento;
				
			$ini_motivo_situacao_cadastral = strpos($texto5, 'Motivo desta Situação Cadastral:');
				
			if ($ini_motivo_situacao_cadastral) {
					
				$texto6 = substr($texto5, $ini_motivo_situacao_cadastral);
				$fim_motivo_situacao_cadastral = strpos($texto6, 'Endereço de Correspondência');
					
				$motivo_situacao_cadastral = trim(html_entity_decode(strip_tags(substr($texto6, 0, $fim_motivo_situacao_cadastral))));
				$motivo_situacao_cadastral = preg_replace('/\s\s+/', ' ', substr($motivo_situacao_cadastral, 35));
				$array['motivo_situacao_cadastral'] = $motivo_situacao_cadastral;
					
			} else {
				$array['motivo_situacao_cadastral'] = '';
			}
				
			$ini_atividade_economica = strripos($texto2, 'Atividade Econômica Principal:');
			$texto_atividade = substr($texto2, $ini_atividade_economica);
			$fim_atividade_economica = '';
				
			if (strpos($texto_atividade, 'Atividade Econômica Secundária')) {
				$fim_atividade_economica = strpos($texto_atividade, 'Atividade Econômica Secundária');
			} else {
				$fim_atividade_economica = strpos($texto_atividade, 'Unidade:');
			}
				
			$atividade_principal = trim(html_entity_decode(strip_tags(substr($texto_atividade, 0, $fim_atividade_economica))));
			$atividade_principal = preg_replace('/\s\s+/', ' ', substr($atividade_principal, 31));
			$array['atividade_principal'] = $atividade_principal;
				
			$cl = strpos($texto, 'Classificação CRC:');
			$texto17 = substr($texto, $cl);
				
			// CRC CONTADOR
			$crc = strpos($texto17, 'Tipo CRC:');
			$texto_crc_contador = substr($texto17, 0, $crc);
			$crc_contador = strripos($texto_crc_contador, 'CRC:');
				
			$crc_contador = substr($texto_crc_contador, $crc_contador);
			$crc_contador = trim(html_entity_decode(strip_tags($crc_contador)));
			$crc_contador = preg_replace('/\s\s+/', ' ', substr($crc_contador, 6));
			$array['crc_contador'] = preg_replace("/[^0-9]/", "", $crc_contador);
				
			$texto17 = substr($texto, $cl);
			$texto18 = substr($texto17, $crc);
			$nom = strpos($texto18, 'Nome:');
			$texto19 = substr($texto18, $nom);
				
			$resp = strpos($texto19, 'Responsável pela organização contábil');
				
			// NOME CONTADOR
			$nome_contador = trim(html_entity_decode(strip_tags(substr($texto19, 0, $resp))));
			$array['nome_contador'] = preg_replace('/\s\s+/', ' ', substr($nome_contador, 7));
				
			$texto20 = substr($texto19, $resp);
			$classi = strpos($texto20, 'Classificação CRC:');
			$texto21 = substr($texto20, $classi);
				
			// CRC RESPONSAVEL
			$crc_resp = strpos($texto21, 'Tipo CRC:');
			$texto_crc_responsavel = substr($texto21, 0, $crc_resp);
			$crc_responsavel = strripos($texto_crc_responsavel, 'CRC:');
				
			$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($texto_crc_responsavel, $crc_responsavel));
			$crc_responsavel = trim(html_entity_decode(strip_tags($crc_responsavel)));
			$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($crc_responsavel, 5));
			$array['crc_responsavel'] = preg_replace("/[^0-9]/", "", $crc_responsavel);
				
			// NOME RESPONSAVEL
			$nomeresp = strpos($texto21, 'Nome:');
			$texto22 = substr($texto21, $nomeresp);
				
			$endresp = strpos($texto22, 'Endereço');
			$nome_responsavel = trim(html_entity_decode(strip_tags(substr($texto22, 0, $endresp))));
			$array['nome_responsavel'] = preg_replace('/\s\s+/', ' ', substr($nome_responsavel, 7));
		
			//----- PARTE 2 -----
		
			$dado_dois = http_build_query(array(
					'num_inscricao' => preg_replace("/[^0-9]/", "", $ie)
			));
		
			$contexto_dois = stream_context_create(array(
					'http' => array(
							'method' => 'POST',
							'content' => $dado_dois,
							'header' => "Content-type: application/x-www-form-urlencoded\r\n"
							. "Content-Length: " . strlen($dado_dois) . "\r\n",
					)
			));
		
			$resposta_dois = file_get_contents('http://www.sefaz.ba.gov.br/scripts/antc/result_consulta.asp', null, $contexto_dois);
		
			$lines = array();
		
			preg_match_all('#<tr[^>]*>(.*?)</tr>#is', $resposta_dois, $lines);
		
			unset($lines[0][0]);
			unset($lines[0][1]);
			unset($lines[0][2]);
			unset($lines[0][4]);
			unset($lines[0][8]);
			unset($lines[0][11]);
			unset($lines[0][12]);
		
			$results = array();
			foreach ($lines[0] as $k => $line) {
				preg_match_all('#<TR[^>]*>(.*?)</TR>#is', $line, $cell);
		
				foreach ($cell[1] as $cell) {
					$text = strip_tags($cell);
					$results[$k][] = trim(html_entity_decode($text));
				}
			}
			
			$inicio_situacao_cadastral = strripos($results[3][0], 'Situação Cadastral:');
			$situacao_cadastral = substr($results[3][0], $inicio_situacao_cadastral);
			
			$array['situacao_cadastral'] = preg_replace('/\s\s+/', ' ', substr($situacao_cadastral, 22));
			$array['credenciado'] = preg_replace('/\s\s+/', ' ', substr($results[9][0], 13));
			$array['motivo'] = preg_replace('/\s\s+/', ' ', substr($results[10][0], 10));
			
			return $array;
		} else {
			return 'erro';
		}
	
	}
	
	public function pesquisar_cnpj_receita(){
		$cnpj = $this->getCnpj(); 

		$url = "https://www.receitaws.com.br/v1/cnpj/".$cnpj."/days/10";

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$headers = array(
		   "Authorization: Bearer ea45caa89aa893e3e2a7f60f514f21fe34a032895daef171d09feb57971c3593",
		);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		//for debug only!
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$resp = curl_exec($curl);
		curl_close($curl); 
		$result = json_decode($resp, true);

		$array = array();

		if($result['status'] == "OK"){

			$array['cnpj'] = $cnpj;
			
			$array['ie'] = "";
			$array['mei'] = "";
			$array['unidade_atendimento'] = "";
			$array['unidade_fiscalizacao'] = "";
			$array['referencia'] = "";
			$array['localizacao'] = "";
			$array['telefone_alternativo'] = "";
			$array['celular_alternativo'] = "";
			$array['email_alternativo'] = "";
			$array['situacao_dte'] = "";

			$array['situacao_conta_dte'] = "";
			$array['condicao'] = "";
			$array['forma_pagamento'] = "";
			$array['nome_contador'] = "";
			$array['crc_contador'] = "";
			$array['nome_responsavel'] = "";
			$array['crc_responsavel'] = "";

			$array['senha_sefaz'] = "";
			$array['login_sefaz'] = "";
			$array['login_mei'] = "";
			$array['senha_mei'] = "";
			$array['id_funcionario'] = "";

			$array['atividade_principal'] = $result['atividade_principal'][0]['text'];
			$array['data_situacao'] = $result['data_situacao'];

			$array['razao_social'] = $result['nome'];
			$array['uf'] = $result['uf'];
			$array['telefone'] = $result['telefone'];
			$array['situacao'] = $result['situacao'];
			$array['situacao_cadastral'] = $result['situacao'];
			$array['bairro'] = $result['bairro'];

			$array['logradouro'] = $result['logradouro'];
			$array['numero'] = $result['numero'];
			$array['cep'] = $result['cep'];
			$array['cidade'] = $result['municipio'];
			$array['abertura'] = $result['abertura'];
			$array['natureza_juridica'] = $result['natureza_juridica'];

			$array['nome_fantasia'] = $result['fantasia'];
			$array['cnpj_completo'] = $result['cnpj'];
			$array['ultima_atualizacao'] = $result['ultima_atualizacao'];
			$array['status'] = $result['status'];
			$array['tipo'] = $result['tipo'];
			$array['complemento'] = $result['complemento'];
			$array['email'] = $result['email'];
			$array['motivo_situacao_cadastral'] = $result['motivo_situacao'];
			$array['motivo'] = $result['motivo_situacao'];
			$array['situacao_especial'] = $result['situacao_especial'];
			$array['data_situacao_especial'] = $result['data_situacao_especial'];
			$array['capital_social'] = $result['capital_social'];
			$array['credenciado'] = "";

			$array['flag_empresa_sem_ie'] = "TRUE";
			return $array;
		}else{
			return "ERRO";
		}
		


	}

	public function listar_consulta_geral() {
		$this->db->select('vinculo_contador, dtb_empresas.id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, senha_sefaz, login_sefaz');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		return $this->db->get('dtb_empresas')->result();
	}
	
	public function verificar_cadastro_duplicado($campo) {
		$this->db->select('id');
		if ($campo == 'ie'){
			$this->db->where("inscricao_estadual", $this->getInscricaoEstadual());
		} else {
			$this->db->where("cnpj", $this->getCnpj());
		}
		// $this->db->where("id_contador", $this->getIdContador());
		return $this->db->get('dtb_empresas')->row();
	}

	public function verificar_cadastro_duplicado_by_cnpj($cnpj){
		$this->db->select('id');
		$this->db->where("cnpj", $cnpj);
		return $this->db->get('dtb_empresas')->row();
	}

	public function verificar_cadastro_duplicado_for_import($campo) {
		$this->db->select('id');
		$this->db->where("inscricao_estadual", $this->getInscricaoEstadual());
		
		// $this->db->where("id_contador", $this->getIdContador());
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function listar() {
		$this->db->select('dtb_empresas.id as id , inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz, tipo_regime');
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }
		
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function consulta_individual() {
		
		$dados = http_build_query(array(
				'num_inscricao' => preg_replace("/[^0-9]/", "", $this->getInscricaoEstadual())
		));
	
		$contexto_dois = stream_context_create(array(
				'http' => array(
						'method' => 'POST',
						'content' => $dados,
						'header' => "Content-type: application/x-www-form-urlencoded\r\n"
						. "Content-Length: " . strlen($dados) . "\r\n",
				)
		));
	
		$resposta_dois = file_get_contents('http://www.sefaz.ba.gov.br/scripts/antc/result_consulta.asp', null, $contexto_dois);
	
		$lines = array();
		
		preg_match_all('#<tr[^>]*>(.*?)</tr>#is', $resposta_dois, $lines);
			
		unset($lines[0][0]);
		unset($lines[0][1]);
		unset($lines[0][2]);
		unset($lines[0][4]);
		unset($lines[0][8]);
		unset($lines[0][11]);
		unset($lines[0][12]);
		
		$results = array();
		$cell = array();
		$array = array();
		
		foreach ($lines[0] as $k => $line) {
			preg_match_all('#<TR[^>]*>(.*?)</TR>#is', $line, $cell);
				
			foreach ($cell[1] as $cell) {
				$text = strip_tags($cell);
				$results[$k][] = trim(html_entity_decode($text));
			}
		}
		
		if (!empty($results[9][0])){			
			
			$inicio_cnpj = strpos($results[3][0], 'CNPJ:');
			$ie = substr($results[3][0], 0, $inicio_cnpj);
			$array['ie'] = substr($ie, 45);
			
			$inicio_situacao_cadastral = strpos($results[3][0], 'Situação Cadastral:');
			$situacao_cadastral = substr($results[3][0], $inicio_situacao_cadastral);
			
			$cnpj = substr($results[3][0], $inicio_cnpj, 30);
			$array['cnpj'] = substr($cnpj, 9);
				
			$array['situacao_cadastral'] = substr($situacao_cadastral, 22);
			$array['credenciado'] = substr($results[9][0], 13);
			$array['motivo'] = substr($results[10][0], 10);
			
			$data_situacao_cadastral = strripos($results[5][0], 'Data Situação Cadastral:');
			$razao_social = substr($results[5][0], 0, $data_situacao_cadastral);
			
			$array['razao_social'] = substr($razao_social, 19);
			$array['nome_fantasia'] = substr($results[6][0], 19);
			
			return $array;
		} else {
			return 'erro';
		}
	
	}
	
	public function relatorio_geral() {
		$this->db->select('inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo');
		
		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		return $this->db->get('dtb_empresas')->result();
	}
	
	public function relatorio_ativas() {
		$this->db->select('inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function relatorio_credenciadas() {
		$this->db->select('inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Credenciado');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function relatorio_descredenciadas() {
		$this->db->select('inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Descredenciado');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function excluir(){
		return $this->db->delete('dtb_empresas', "id = {$this->getId()}");
	}
	
	public function credenciadas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Credenciado');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function credenciadasativas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Credenciado');
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function credenciadasbaixadas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Credenciado');
		$this->db->where("situacao_cadastral", 'BAIXADO');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function descredenciadas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Descredenciado');
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->where("mei !=", 'Sim');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function descredenciadasativas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Descredenciado');
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function descredenciadasbaixadas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Descredenciado');
		$this->db->where("situacao_cadastral", 'BAIXADO');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function empresasregulares() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Credenciado');
		$this->db->where("situacao_cadastral", 'ATIVO');
		$this->db->where("situacao_conta_dte", 'ATIVA');
		$this->db->where("vinculo_contador", '1');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function empresasativas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral", 'ATIVO');
		$this->db->where("situacao_conta_dte", 'ATIVA');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function empresasnaoativas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral !=", 'ATIVO');
		$this->db->where("situacao_cadastral !=", 'INTIMADO P/ INAPTIDÃO');		
		$this->db->group_by('dtb_empresas.id');
		//$this->db->where("situacao_cadastral", 'BAIXADO');
		//$this->db->where("situacao_conta_dte", 'INEXISTENTE');
		return $this->db->get('dtb_empresas')->result();
	}

	public function empresasinaptas() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral", 'INAPTO');	
		$this->db->group_by('dtb_empresas.id');
		//$this->db->where("situacao_cadastral", 'BAIXADO');
		//$this->db->where("situacao_conta_dte", 'INEXISTENTE');
		return $this->db->get('dtb_empresas')->result();
	}

	public function dteativo() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_conta_dte", 'ATIVA');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function dteinexistente() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_conta_dte !=", 'ATIVA');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function empresassemie() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }
		$this->db->where("flag_empresa_sem_ie is not null");
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function update_sync(){
	
		$dados = array(
				'sync' => $this->getSync()
		);
	
		return $this->db->update('dtb_empresas', $dados, "id={$this->getId()}");
	
	}
	
	public function pesquisar_empresa_id() {
		$this->db->select('id, inscricao_estadual, cnpj, inscricao_estadual_completo, cnpj_completo, mei, unidade_atendimento, unidade_fiscalizacao, referencia, localizacao, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, telefone_alternativo, celular_alternativo, email_alternativo, situacao_cadastral, situacao, motivo, senha_sefaz, login_sefaz, atividade_principal, situacao_dte, situacao_conta_dte, nome_contador, crc_contador, nome_responsavel, crc_responsavel, motivo_situacao_cadastral, forma_pagamento, condicao, login_mei, senha_mei, cpf_alvara, nire, anotacoes, id_funcionario');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}

	public function consultar_emopresa_aux() {
		$this->db->select('dtb_empresas.id, dtb_empresas.inscricao_estadual_completo as inscricao_estadual_completo, 

			dtb_empresas.cnpj_completo as cnpj_completo, dtb_empresas.mei as mei, dtb_empresas.unidade_atendimento as 
			unidade_atendimento, dtb_empresas.unidade_fiscalizacao as unidade_fiscalizacao, dtb_empresas.referencia as 
			referencia, dtb_empresas.localizacao as localizacao, dtb_empresas.razao_social as razao_social, 

			dtb_empresas.nome_fantasia as nome_fantasia, dtb_empresas.natureza_juridica as natureza_juridica, 
			dtb_empresas.cep as cep, dtb_empresas.logradouro as logradouro, 

			dtb_empresas.numero as numero, dtb_empresas.complemento as complemento, dtb_empresas.bairro as bairro, 
			dtb_empresas.cidade as cidade, dtb_empresas.uf as uf, dtb_empresas.telefone as telefone,
			dtb_empresas.email as email, 

			dtb_empresas.id_contador as id_contador, dtb_empresas.telefone_alternativo as telefone_alternativo, 
			dtb_empresas.celular_alternativo as celular_alternativo, dtb_empresas.email_alternativo as email_alternativo, 
			dtb_empresas.situacao_cadastral as situacao_cadastral, dtb_empresas.situacao as situacao, dtb_empresas.motivo as motivo, 

			dtb_empresas.senha_sefaz as senha_sefaz, dtb_empresas.login_sefaz as login_sefaz, 
			dtb_empresas.atividade_principal as atividade_principal, dtb_empresas.situacao_dte as situacao_dte, 
			dtb_empresas.situacao_conta_dte as situacao_conta_dte, 

			dtb_empresas.nome_contador as nome_contador, dtb_empresas.crc_contador as crc_contador, 
			dtb_empresas.nome_responsavel as nome_responsavel, dtb_empresas.crc_responsavel as crc_responsavel, 
			dtb_empresas.motivo_situacao_cadastral as motivo_situacao_cadastral, dtb_empresas.forma_pagamento as 
			forma_pagamento, dtb_empresas.condicao as condicao, usuario.nome as nomefuncionario, dtb_empresas.login_mei as login_mei, dtb_empresas.senha_mei as senha_mei, dtb_empresas.cpf_alvara as cpf_alvara, dtb_empresas.nire as nire, dtb_empresas.anotacoes as anotacoes');

		$this->db->join('dtb_usuarios usuario','id_funcionario = usuario.id','left');
		$this->db->where("dtb_empresas.id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function nome_empresa() {
		$this->db->select('razao_social');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function cnpj_empresa() {
		$this->db->select('cnpj_completo');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}

	public function editar(){
	
		$dados = array(
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'telefone_alternativo' => $this->getTelefoneAlternativo(),
				'celular_alternativo' => $this->getCelularAlternativo(),
				'inscricao_estadual_completo' => $this->getInscricaoEstadualCompleto(),
				'senha_sefaz' => $this->getSenhaSefaz(),
				'email_alternativo' => $this->getEmailAlternativo(),
				'login_sefaz' => $this->getLoginSefaz(),
				'login_mei' => $this->getLoginMei(),
				'senha_mei' => $this->getSenhaMei(),
				'cpf_alvara' => $this->getCpfAlvara(),
				'nire' => $this->getNire(),
				'anotacoes' => $this->getAnotacoes(),
				'id_funcionario' => $this->getIdFuncionario()
		);
	
		if ($this->db->update('dtb_empresas', $dados, "id={$this->getId()}")){

			$this->atualizaListaEmpresaUsuario($this->getId(),$this->getListaUsuario());

			return TRUE;
		} else {
			return FALSE;
		}
	
	
	}

	public function atualizaListaEmpresaUsuario($id_empresa, $listaUsuario){
		$this->db->delete('dtb_empresa_usuario', array('id_empresa' => $id_empresa));

		if(sizeof($listaUsuario) != 0){
			foreach ($listaUsuario as $idUsuario) {	
				$data = array( 
		        	'id_empresa' =>  $id_empresa , 
		        	'id_usuario' =>  $idUsuario
			    );

				$this->db->insert('dtb_empresa_usuario', $data);		
			}
		}
		
	}

	public function editarForImport(){
		$dados = array(
				'senha_sefaz' => $this->getSenhaSefaz(),
				'login_sefaz' => $this->getLoginSefaz(),
				'login_mei' => $this->getLoginMei(),
				'senha_mei' => $this->getSenhaMei()
		);
	
		if ($this->db->update('dtb_empresas', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function empresa_id() {
		$this->db->select('inscricao_estadual_completo, cnpj_completo, razao_social');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function historico_empresa() {
		$this->db->select('situacao_cadastral, situacao, motivo, motivo_situacao_cadastral, data_atualizacao');
		$this->db->where("id_empresa", $this->getId());
		return $this->db->get('historico_empresas')->result();
	}
	
	public function qtd_empresas() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		return $this->db->get('dtb_empresas')->row();
	}
	
	public function qtd_regulares() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("situacao", 'Credenciado');
		$this->db->where("situacao_cadastral", 'ATIVO');
		$this->db->where("vinculo_contador", 1);
		$this->db->where("situacao_conta_dte", 'ATIVA');
		$this->db->where("flag_empresa_sem_ie is null");
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function grf_situacao_cadastral() {
		$this->db->select('situacao_cadastral, COUNT(distinct(dtb_empresas.id)) AS quantidade');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->group_by("situacao_cadastral");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function grf_conta_dte() {
		$this->db->select('situacao_conta_dte, COUNT(distinct(dtb_empresas.id)) AS quantidade');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->group_by("situacao_conta_dte");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function grf_situacao_dte() {
		$this->db->select('situacao_dte, COUNT(distinct(dtb_empresas.id)) AS quantidade');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->group_by("situacao_dte");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function grf_natureza_juridica() {
		$this->db->select('natureza_juridica, COUNT(distinct(dtb_empresas.id)) AS quantidade');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->group_by("natureza_juridica");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function grf_condicao_empresa() {
		$this->db->select('condicao, COUNT(distinct(dtb_empresas.id)) AS quantidade');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->group_by("condicao");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function grf_forma_pagamento() {
		$this->db->select('forma_pagamento, COUNT(distinct(dtb_empresas.id)) AS quantidade');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->group_by("forma_pagamento");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function grf_situacao() {
		$this->db->select('situacao, COUNT(distinct(dtb_empresas.id)) AS quantidade');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->group_by("situacao");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function qtd_ativas() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral", 'ATIVO');
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function qtd_nao_ativas() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral !=", 'ATIVO');
		$this->db->where("situacao_cadastral !=", 'INTIMADO P/ INAPTIDÃO');
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function qtd_credenciadas() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Credenciado');
		return $this->db->get('dtb_empresas')->row();
	}

	public function qtd_descredenciadas() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao", 'Descredenciado');
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->where("mei !=", 'Sim');
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function qtd_dte_ativo() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_conta_dte", 'ATIVA');
		return $this->db->get('dtb_empresas')->row();
	}
	
	public function validar_vinculo_contador() {
		$this->db->select('COUNT(id) AS valor');

		//$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("crc", $this->getCrcContador());
		$this->db->or_where("crc", $this->getCrcResponsavel());
		return $this->db->get('contadores')->row();
	}
	
	public function contadornaovinculado() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz, nome_contador, crc_contador');
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// $this->db->where("id_contador", $this->getIdContador());
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("vinculo_contador", 0);
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->where("mei !=", 'Sim');
		$this->db->group_by("dtb_empresas.id");
		return $this->db->get('dtb_empresas')->result();
	}
	
	public function qtd_contador_nao_vinculado() {
		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// $this->db->where("id_contador", $this->getIdContador());
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("vinculo_contador", 0);
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->where("mei !=", 'Sim');
		return $this->db->get('dtb_empresas')->row();
	}

	public function qtd_empresas_intimadas() {

		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral", 'INTIMADO P/ INAPTIDÃO');
		return $this->db->get('dtb_empresas')->row();
	}

	public function qtd_empresas_inaptas(){

		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral", 'INAPTO');
		return $this->db->get('dtb_empresas')->row();
	}

	public function notificacoes() {
		//$this->db->select('COUNT(id) AS valor');
		// $this->db->where("id_usuario", $this->getIdContador());
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas as e', 'notificacoes.id_empresa = e.id', 'left');
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("status", 1);
		$this->db->group_by('notificacoes.id');
		$this->db->order_by("notificacoes.id", "desc");
		return $this->db->get('notificacoes')->result();
	}
	
	public function qtd_notificacoes() {
		$this->db->select('COUNT(distinct(notificacoes.id)) AS valor');
		// $this->db->where("id_usuario", $this->getIdContador());
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas as e', 'notificacoes.id_empresa = e.id', 'left');
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		$this->db->where("status", 1);
		return $this->db->get('notificacoes')->row();
	}
	
	public function limparnotificacoes(){
		
		$this->db->set('status', 0);
		// $this->db->where('id_usuario', $this->getIdContador());
		$this->db->where('status', 1);

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "UPDATE notificacoes LEFT JOIN dtb_empresas e ON notificacoes.id_empresa = e.id LEFT JOIN dtb_empresa_usuario eu ON notificacoes.id_empresa = eu.id_empresa set notificacoes.status = 0 where notificacoes.status = 1 and (e.id_funcionario = ".$this->session->userdata['userprimesession']['id']." or eu.id_usuario = ".$this->session->userdata['userprimesession']['id'].")";

			if ($this->db->query($sql)){
				return true;
			} else {
				return false;
			}
		}else{
			if ($this->db->update('notificacoes')){
				return true;
			} else {
				return false;
			}
		}
		
	}

	public function lerMensagem($id){
		$this->db->set('status', 0);
		$this->db->where('id', $id);
		$this->db->update('notificacoes');
	}

	public function buscar_funcionarios(){
		$this->db->select('id, nome');
		$this->db->where("nivel", 2);
		// $this->db->where("id_cadastro", $this->getIdContador());
		return $this->db->get('dtb_usuarios')->result();
	}

	public function consultarDadosForDte(){
		$this->db->select('id, login_sefaz, senha_sefaz, cnpj, cnpj_completo');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}

	public function listar_empresas_intimadas(){

		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz, data_intimacao');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral", 'INTIMADO P/ INAPTIDÃO');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function listarDadosDosServicos(){
		$this->db->select('id, login_mei, senha_mei, cnpj, cnpj_completo, forma_pagamento, mei');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}

	public function pesquisar_sefaz_for_import() {
		
		$dados = http_build_query(array(
				'IE' => $this->getInscricaoEstadual(),
				'B2' => 'IE  ->'
		));
		
		$contexto = stream_context_create(array(
				'http' => array(
						'method' => 'POST',
						'content' => $dados,
						'header' => "Content-type: application/x-www-form-urlencoded\r\n"
						. "Content-Length: " . strlen($dados) . "\r\n",
				)
		));
		
		$resposta = file('http://www.sefaz.ba.gov.br/scripts/cadastro/cadastroBa/result.asp', null, $contexto);	
		
		if (!empty($resposta)){
			
			for ($i=0; $i <= 90 ; $i++) {
				unset($resposta[$i]);
			}

			$cnpj = trim(html_entity_decode(strip_tags($resposta[106])));
			$cnpj = preg_replace('/\s\s+/', ' ', substr($cnpj, 7));

			$this->setCnpjCompleto($cnpj);
			$this->setCnpj(preg_replace("/[^0-9]/", "", $cnpj));

			$ie = trim(html_entity_decode(strip_tags($resposta[108])));
			$ie = preg_replace('/\s\s+/', ' ', substr($ie, 23));

			$this->setInscricaoEstadualCompleto($ie);
			$this->setInscricaoEstadual(preg_replace("/[^0-9]/", "", $ie));

			$contribuinte_mei = trim(html_entity_decode(strip_tags($resposta[113])));
			$contribuinte_mei = preg_replace('/\s\s+/', ' ', substr($contribuinte_mei, 48));

			if(strpos($contribuinte_mei, 'N') !== false){
				$this->setMei("Não");	
			}else{
				$this->setMei("Sim");
			}
			
			
			$razao_social = trim(html_entity_decode(strip_tags($resposta[118])));
			$razao_social = preg_replace('/\s\s+/', ' ', substr($razao_social, 16));
			
			$this->setRazaoSocial($razao_social);
			$this->e->setSync(1);
			date_default_timezone_set('America/Bahia');
			$this->e->setDataCadastro(date('Y-m-d H:i:s'));

			if ($contribuinte_mei == 'Sim') {
			
				$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[127])));
				$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
				$this->setNomeFantasia($nome_fantasia);
			
				$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[131])));
				$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
				$this->setNaturezaJuridica($natureza_juridica);
			
				$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[135])));
				$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
				$this->setUnidadeAtendimento($unidade_atendimento);
			
				$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[138])));
				$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
				$this->setUnidadeFiscalizacao($unidade_fiscalizacao);
			
				$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[151])));
				$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
				//echo "- Logradouro: $logradouro_empresa<br>";
				$this->setLogradouro($logradouro_empresa);
			
				$numero_empresa = trim(html_entity_decode(strip_tags($resposta[154])));
				$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
				$this->setNumero($numero_empresa);
			
				$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[155])));
				$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
				$this->setComplemento($complemento_empresa);
			
				$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[158])));
				$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
				$this->setBairro($bairro_empresa);
			
				$cep_empresa = trim(html_entity_decode(strip_tags($resposta[159])));
				$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
				$this->setCep($cep_empresa);
			
				$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[162])));
				$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
				$this->setCidade($municipio_empresa);
			
				$uf_empresa = trim(html_entity_decode(strip_tags($resposta[163])));
				$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
				$this->setUf($uf_empresa);
			
				$telefone = trim(html_entity_decode(strip_tags($resposta[166])));
				$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
				$this->setTelefone($telefone);
			
				$email = trim(html_entity_decode(strip_tags($resposta[167])));
				$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
				$this->setEmail($email);
			
				$referencia = trim(html_entity_decode(strip_tags($resposta[171])));
				$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
				$this->setReferencia($referencia);
			
				$localizacao = trim(html_entity_decode(strip_tags($resposta[172])));
				$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
				$this->setLocalizacao($localizacao);
			
			} else {
			
				$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[125])));
				$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
				$this->setNomeFantasia($nome_fantasia);
			
				$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[129])));
				$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
				$this->setNaturezaJuridica($natureza_juridica);
			
				$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[133])));
				$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
				$this->setUnidadeAtendimento($unidade_atendimento);
			
				$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[136])));
				$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
				$this->setUnidadeFiscalizacao($unidade_fiscalizacao);
			
				$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[149])));
				$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
				$this->setLogradouro($logradouro_empresa);
			
				$numero_empresa = trim(html_entity_decode(strip_tags($resposta[152])));
				$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
				$this->setNumero($numero_empresa);
			
				$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[153])));
				$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
				$this->setComplemento($complemento_empresa);
			
				$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[156])));
				$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
				$this->setBairro($bairro_empresa);
			
				$cep_empresa = trim(html_entity_decode(strip_tags($resposta[157])));
				$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
				$this->setCep($cep_empresa);
			
				$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[160])));
				$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
				$this->setCidade($municipio_empresa);
			
				$uf_empresa = trim(html_entity_decode(strip_tags($resposta[161])));
				$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
				$this->setUf($uf_empresa);
			
				$telefone = trim(html_entity_decode(strip_tags($resposta[164])));
				$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
				$this->setTelefone($telefone);
			
				$email = trim(html_entity_decode(strip_tags($resposta[165])));
				$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
				$this->setEmail($email);
			
				$referencia = trim(html_entity_decode(strip_tags($resposta[169])));
				$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
				$this->setReferencia($referencia);
			
				$localizacao = trim(html_entity_decode(strip_tags($resposta[170])));
				$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
				$this->setLocalizacao($localizacao);
			
			}
						
			//DADOS DO CONTADOR
			$this->setAtividadePrincipal("");
						
			$linhas = array();
			
			foreach ($resposta as $key => $value) {
				$text = strip_tags($value);
				$linhas[] = trim(html_entity_decode($text));
			}
			
			$texto = implode(' ', $linhas);
			
			$ini_dte = strpos($texto, 'Situação do DTE:');
			$texto1 = substr($texto, $ini_dte);
			$fim_dte = strpos($texto1, 'Data do Credenciamento:');
			
			$situacao_dte = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_dte))));
			$situacao_dte = preg_replace('/\s\s+/', ' ', substr($situacao_dte, 21));
			$this->setSituacaoDte($situacao_dte);
			
			$ini_conta_dte = strpos($texto1, 'Situação da Conta:');
			$texto2 = substr($texto1, $ini_conta_dte);
			$fim_conta_dte = strpos($texto2, 'Data da Criação da Conta:');
			
			$situacao_conta_dte = trim(html_entity_decode(strip_tags(substr($texto2, 0, $fim_conta_dte))));
			$situacao_conta_dte = preg_replace('/\s\s+/', ' ', substr($situacao_conta_dte, 23));
			$this->setSituacaoContaDte($situacao_conta_dte);
			
			$ini_condicao = strripos($texto2, 'Condição:');
			$texto3 = substr($texto2, $ini_condicao);
			$fim_condicao = strpos($texto3, 'Forma de pagamento:');
			
			$condicao = trim(html_entity_decode(strip_tags(substr($texto3, 0, $fim_condicao))));
			$condicao = preg_replace('/\s\s+/', ' ', substr($condicao, 13));
			$this->setCondicao($condicao);
			
			$texto4 = substr($texto3, $fim_condicao);
			
			$ini_forma_pagamento = strpos($texto4, 'Forma de pagamento:');
			$texto5 = substr($texto4, $ini_forma_pagamento);
			$fim_forma_pagamento = strpos($texto5, 'Situação Cadastral Vigente:');
			
			$forma_pagamento = trim(html_entity_decode(strip_tags(substr($texto5, 0, $fim_forma_pagamento))));
			$forma_pagamento = preg_replace('/\s\s+/', ' ', substr($forma_pagamento, 21));
			$this->setFormaPagamento($forma_pagamento);

			$ini_motivo_situacao_cadastral = strpos($texto5, 'Motivo desta Situação Cadastral:');
			
			if ($ini_motivo_situacao_cadastral) {
			
				$texto6 = substr($texto5, $ini_motivo_situacao_cadastral);
				$fim_motivo_situacao_cadastral = strpos($texto6, 'Endereço de Correspondência');
			
				$motivo_situacao_cadastral = trim(html_entity_decode(strip_tags(substr($texto6, 0, $fim_motivo_situacao_cadastral))));
				$motivo_situacao_cadastral = preg_replace('/\s\s+/', ' ', substr($motivo_situacao_cadastral, 35));
				$this->setMotivoSituacaoCadastral($motivo_situacao_cadastral);
			
			} else {
				$this->setMotivoSituacaoCadastral("");
			}
			
			$ini_atividade_economica = strripos($texto2, 'Atividade Econômica Principal:');
			$texto_atividade = substr($texto2, $ini_atividade_economica);
			$fim_atividade_economica = '';
			
			if (strpos($texto_atividade, 'Atividade Econômica Secundária')) {
				$fim_atividade_economica = strpos($texto_atividade, 'Atividade Econômica Secundária');
			} else {
				$fim_atividade_economica = strpos($texto_atividade, 'Unidade:');
			}
			
			$atividade_principal = trim(html_entity_decode(strip_tags(substr($texto_atividade, 0, $fim_atividade_economica))));
			$atividade_principal = preg_replace('/\s\s+/', ' ', substr($atividade_principal, 31));
			$this->setAtividadePrincipal($atividade_principal);

			$cl = strpos($texto, 'Classificação CRC:');
			$texto17 = substr($texto, $cl);
			
			// CRC CONTADOR
			$crc = strpos($texto17, 'Tipo CRC:');
			$texto_crc_contador = substr($texto17, 0, $crc);
			$crc_contador = strripos($texto_crc_contador, 'CRC:');
			
			$crc_contador = substr($texto_crc_contador, $crc_contador);
			$crc_contador = trim(html_entity_decode(strip_tags($crc_contador)));
			$crc_contador = preg_replace('/\s\s+/', ' ', substr($crc_contador, 6));
			$this->setCrcContador(preg_replace("/[^0-9]/", "", $crc_contador));

			$texto17 = substr($texto, $cl);
			$texto18 = substr($texto17, $crc);
			$nom = strpos($texto18, 'Nome:');
			$texto19 = substr($texto18, $nom);
			
			$resp = strpos($texto19, 'Responsável pela organização contábil');
			
			// NOME CONTADOR
			$nome_contador = trim(html_entity_decode(strip_tags(substr($texto19, 0, $resp))));
			$this->setNomeContador(preg_replace('/\s\s+/', ' ', substr($nome_contador, 7)));
			
			$texto20 = substr($texto19, $resp);
			$classi = strpos($texto20, 'Classificação CRC:');
			$texto21 = substr($texto20, $classi);
			
			// CRC RESPONSAVEL
			$crc_resp = strpos($texto21, 'Tipo CRC:');
			$texto_crc_responsavel = substr($texto21, 0, $crc_resp);
			$crc_responsavel = strripos($texto_crc_responsavel, 'CRC:');
			
			$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($texto_crc_responsavel, $crc_responsavel));
			$crc_responsavel = trim(html_entity_decode(strip_tags($crc_responsavel)));
			$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($crc_responsavel, 5));
			$this->setCrcResponsavel(preg_replace("/[^0-9]/", "", $crc_responsavel));
			
			// NOME RESPONSAVEL
			$nomeresp = strpos($texto21, 'Nome:');
			$texto22 = substr($texto21, $nomeresp);
			
			$endresp = strpos($texto22, 'Endereço');
			$nome_responsavel = trim(html_entity_decode(strip_tags(substr($texto22, 0, $endresp))));
			$this->setNomeResponsavel(preg_replace('/\s\s+/', ' ', substr($nome_responsavel, 7)));
			
			//----- PARTE 2 -----
			
			$dado_dois = http_build_query(array(
					'num_inscricao' => $this->getInscricaoEstadual()
			));
			
			$contexto_dois = stream_context_create(array(
					'http' => array(
							'method' => 'POST',
							'content' => $dado_dois,
							'header' => "Content-type: application/x-www-form-urlencoded\r\n"
							. "Content-Length: " . strlen($dado_dois) . "\r\n",
					)
			));
			
			$resposta_dois = file_get_contents('http://www.sefaz.ba.gov.br/scripts/antc/result_consulta.asp', null, $contexto_dois);
			
			$lines = array();
			
			preg_match_all('#<tr[^>]*>(.*?)</tr>#is', $resposta_dois, $lines);
			
			unset($lines[0][0]);
			unset($lines[0][1]);
			unset($lines[0][2]);
			unset($lines[0][4]);
			unset($lines[0][8]);
			unset($lines[0][11]);
			unset($lines[0][12]);
			
			$results = array();
			foreach ($lines[0] as $k => $line) {
				preg_match_all('#<TR[^>]*>(.*?)</TR>#is', $line, $cell);
			
				foreach ($cell[1] as $cell) {
					$text = strip_tags($cell);
					$results[$k][] = trim(html_entity_decode($text));
				}
			}
			
			$inicio_situacao_cadastral = strripos($results[3][0], 'Situação Cadastral:');
			$situacao_cadastral = substr($results[3][0], $inicio_situacao_cadastral);
			
			$this->setSituacaoCadastral(preg_replace('/\s\s+/', ' ', substr($situacao_cadastral, 22)));
			$this->setSituacao(preg_replace('/\s\s+/', ' ', substr($results[9][0], 13)));
			$this->setMotivo(preg_replace('/\s\s+/', ' ', substr($results[10][0], 10)));
			
			return "sucesso";
		} else {
			return 'erro';
		}
	
	}
	
	public function relatorio_empresa_colaborador($users) {
		$this->db->select('e.id, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, u.nome');
		//$this->db->join('dtb_usuarios as ua', 'e.id_funcionario = ua.id', 'left');
		
		$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
		$this->db->join('dtb_usuarios as u', 'eu.id_usuario = u.id', 'left');

		$this->db->group_start();
			$this->db->where("e.id_funcionario", $users);
			$this->db->or_where('eu.id_usuario', $users);
        $this->db->group_end();
		//$this->db->where_in('u.id', $users);
		//$this->db->where("flag_empresa_sem_ie is null");
		//$this->db->group_by("e.id");
		$this->db->order_by('u.nome, e.razao_social');
		return $this->db->get('dtb_empresas as e')->result();
	}

	public function gerar_relatorio_empresa_colaborador($id_usuario){
		$sql1 = "SELECT e.id, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, u.nome FROM dtb_empresas e LEFT JOIN dtb_usuarios u ON e.id_funcionario = u.id where e.id_funcionario = ".$id_usuario." GROUP BY e.id";

		$sql2 = "SELECT e.id, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, u.nome FROM dtb_empresas e LEFT JOIN dtb_empresa_usuario eu ON e.id = eu.id_empresa LEFT JOIN dtb_usuarios as u ON eu.id_usuario = u.id WHERE u.id = ".$id_usuario." GROUP BY e.id";

		$sql = $sql1." UNION ".$sql2;

		return $this->db->query($sql)->result();
	}

	public function relatorio_empresa_sem_colaborador() {
		$this->db->select('e.id, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social');
	
		$this->db->where('e.id_funcionario is null');
		$this->db->where('e.id not in (select id_empresa from dtb_empresa_usuario)');
		//$this->db->where("flag_empresa_sem_ie is null");
		$this->db->order_by('e.razao_social');
		return $this->db->get('dtb_empresas as e')->result();
	}

	public function qtd_empresas_sem_senha_dte(){

		$this->db->select('COUNT(distinct(dtb_empresas.id)) AS valor');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("login_sefaz is null");
		$this->db->where("senha_sefaz is null");
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		return $this->db->get('dtb_empresas')->row();
	}

	public function listar_empresas_sem_senha_dte(){

		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz, data_intimacao');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("login_sefaz is null");
		$this->db->where("senha_sefaz is null");
		$this->db->where("situacao_cadastral !=", 'BAIXADO');	
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function empresas_mei() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("mei", 'Sim');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function empresas_simples_nacional() {
		$this->db->select('vinculo_contador, dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, login_sefaz, senha_sefaz');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		// else{
		// 	$this->db->where("id_contador", $this->getIdContador());
		// }

		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("forma_pagamento", 'SIMPLES NACIONAL');
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();
	}

	public function consultar_login_e_senha_dte($id_empresa){

		$this->db->select('login_sefaz, senha_sefaz');
		$this->db->where('dtb_empresas.id', $id_empresa);
		return $this->db->get('dtb_empresas')->row();
	}

	public function buscar_cnpj_empresa($id_empresa){

		$this->db->select('cnpj');
		$this->db->where('dtb_empresas.id', $id_empresa);
		return $this->db->get('dtb_empresas')->row();
	}

	public function editar_senha_sefaz(){

		$dados = array(
				'senha_sefaz' => $this->getSenhaSefaz(),
				'login_sefaz' => $this->getLoginSefaz()
		);
	
		if ($this->db->update('dtb_empresas', $dados, "id={$this->getId()}")){

			return TRUE;

		} else {
			
			return FALSE;
		}
	}

	public function buscar_para_vincular(){

		$this->db->select('id, razao_social, cnpj_completo');
		return $this->db->get('dtb_empresas')->result();
	}

	public function buscar_para_vincular_aux($id_contador){
		$sql = "SELECT id, razao_social, cnpj FROM dtb_empresas WHERE id not in (SELECT id_empresa from dtb_contador_procuracao )";
		return $this->db->query($sql)->result();
	}

	public function listar_para_gerenciamento() {

		$this->db->select('e.id as id , e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.id_contador, e.forma_pagamento, e.telefone_alternativo, e.celular_alternativo, e.email_alternativo, e.senha_sefaz, e.login_sefaz, ce.status as status_da_conta, ce.id as id_cliente');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id = ce.id_empresa', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		$this->db->group_by('e.id');
		return $this->db->get('dtb_empresas as e')->result();
	}

	public function pesquisar_cliente_id($id){

		$this->db->select('*');
		$this->db->where('id', $id);
		return $this->db->get('dtb_empresas')->row();
	}

	public function listar_empresas_sem_certificado(){

		$query1 = 'select dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz from dtb_empresas where NOT EXISTS (select * from dtb_certificado as ce where ce.id_empresa = dtb_empresas.id)'; 

		$query2 = 'select dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz from dtb_empresas where dtb_empresas.id NOT IN (SELECT id_empresa FROM dtb_certificado)'; 

		$query22 = 'select dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz from dtb_empresas where dtb_empresas.cnpj IN (SELECT cnpj FROM dtb_empresas_sem_procuracao)'; 

		$query_final = "SELECT * FROM (".$query1." UNION ".$query2.") as resultado GROUP BY id";
		return $this->db->query($query_final)->result();

		//return $this->db->query($query)->result();
	}

	public function listar_empresas_com_certificado(){

		$query = 'select dtb_empresas.id as id , inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz from dtb_empresas where EXISTS (select * from dtb_certificado as ce where ce.id_empresa = dtb_empresas.id)'; 

		return $this->db->query($query)->result();
	}

	public function qtd_empresas_sem_certificado_digital(){

		$query1 = 'select dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz from dtb_empresas where NOT EXISTS (select * from dtb_certificado as ce where ce.id_empresa = dtb_empresas.id)'; 

		$query2 = 'select dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz from dtb_empresas where dtb_empresas.id NOT IN (SELECT id_empresa FROM dtb_certificado)'; 

		$query22 = 'select dtb_empresas.id as id, inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, natureza_juridica, cep, logradouro, numero, complemento, bairro, cidade, uf, telefone, email, id_contador, forma_pagamento, telefone_alternativo, celular_alternativo, email_alternativo, senha_sefaz, login_sefaz from dtb_empresas where dtb_empresas.cnpj IN (SELECT cnpj FROM dtb_empresas_sem_procuracao)'; 

		$query_final = "SELECT count(*) FROM (".$query1." UNION ".$query2.") as resultado GROUP BY id";
		return $this->db->query($query_final)->result();

		//return $this->db->query($query)->result();
	}

	public function find_all_empresas(){
		$this->db->select('*');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		$this->db->group_by('e.id');
		return $this->db->get('dtb_empresas as e')->result();
	}

	public function find_empresa_by_id_empresa($id){
		$this->db->select('*');
		$this->db->where("id", $id);

		return $this->db->get('dtb_empresas')->row();
	}

	public function find_empresa_by_cnpj($cnpj){
		$this->db->select('*');
		$this->db->where("cnpj", $cnpj);

		return $this->db->get('dtb_empresas')->row();
	}

	public function update_regime($id, $regime){
		$this->db->set('tipo_regime', $regime);
		$this->db->where('id', $id);
		$this->db->update('dtb_empresas');
	}

	public function pesquisar_cnpj_receita_importador($cnpj){ 
		
		$curl = curl_init();

	    // Coloque aqui sua Chave de API
	    $api_key = "f1d7afd0-4079-4709-a1c4-b91f06cb2992-0d691d0a-3c3f-4bc7-9779-07a75446b367";

	    // Escolha o CNPJ para testar
	    $tax_id = $cnpj;

	    // Executa a chamada para API CNPJá!
	    $cnpja_url="https://api.cnpja.com.br/companies/";

	  	curl_setopt_array($curl, array(
	    CURLOPT_URL => $cnpja_url . $tax_id,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 0,
	    CURLOPT_FOLLOWLOCATION => true,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "GET",
	    CURLOPT_HTTPHEADER => array(
	      "Authorization: " . $api_key
	    ),
	  	));

	  	$response = curl_exec($curl);
	  	curl_close($curl);	  	 
	    
	  	// Decodifica o JSON de retorno
	  	$company = json_decode($response);

	  	// Acessa as propriedades que desejar 

		$array = array();

		if (isset($company->code)) {

			if ($company->code == "500") {
				return "ERRO SINTEGRA";
			} elseif ($company->code == "400") {
				return "CNPJ ERRADO";
			} elseif ($company->code == "401") {
				return "CHAVE DE API INVÁLIDA";
			} elseif ($company->code == "404") {
				return "CNPJ NÃO REGISTRADO NA RECEITA FEDERAL";
			} elseif ($company->code == "429") {
				return "LIMITE DE REQUISIÇÕES POR PLANO";
			} elseif ($company->code == "500") {
				return "ERRO NO SERVIÇO DA API";
			} elseif ($company->code == "503") {
				return "FONTE DE DADOS INDISPONIVEL";
			}

	  	} else {

	  		$this->setCnpjCompleto($cnpj);
	  		$this->setCnpj($cnpj);

	  		$inscricao_estadual = NULL;
	  		$this->setInscricaoEstadual($inscricao_estadual);

	  		$this->setInscricaoEstadualCompleto(NULL);
	  		$this->setSenhaSefaz(NULL);
	  		$this->setLoginSefaz(NULL);
	  		$this->setFormaPagamento(NULL);

	  		$mei = NULL;
			$this->setMei($mei);

	  		$unidade_atendimento = NULL;
			$this->setUnidadeAtendimento($unidade_atendimento);

	  		$unidade_fiscalizacao = NULL;
			$this->setUnidadeFiscalizacao($unidade_fiscalizacao);

	  		$referencia = NULL;
			$this->setReferencia($referencia);

	  		$localizacao = NULL;
			$this->setLocalizacao($localizacao);

	  		$telefone_alternativo = NULL;
			$this->setTelefoneAlternativo($telefone_alternativo);

	  		$celular_alternativo = NULL;
			$this->setCelularAlternativo($celular_alternativo);

	  		$email_alternativo = NULL;
			$this->setEmailAlternativo($email_alternativo);

	  		$situacao_dte = NULL;
			$this->setSituacaoDte($situacao_dte);

			$array['situacao_conta_dte'] = "";
			$array['condicao'] = "";
			$array['forma_pagamento'] = "";
			$array['nome_contador'] = "";
			$array['crc_contador'] = "";
			$array['nome_responsavel'] = "";
			$array['crc_responsavel'] = "";

			$array['senha_sefaz'] = "";
			$array['login_sefaz'] = "";
			$array['login_mei'] = "";
			$array['senha_mei'] = "";
			$array['id_funcionario'] = "";

			if (($company->simples_nacional->simples_optant == 1) AND ($company->simples_nacional->simples_excluded == null)){
				$this->setFormaPagamento("SIMPLES NACIONAL");
			}
			
			$this->setAtividadePrincipal($company->primary_activity->description);
			$this->setDataAtualizacao($company->registration->status_date);

			if ($company->name == null) {
				return "CNPJ NÃO LOCALIZADO";
			}

			$this->setRazaoSocial($company->name); 
			$this->setUf($company->address->state);
			$this->setTelefone($company->phone);
			$this->setSituacao($company->registration->status);
			$this->setSituacaoCadastral($company->registration->status);
			$this->setBairro($company->address->neighborhood);

			$this->setLogradouro($company->address->street);
			$this->setNumero($company->address->number);
			$this->setCep($company->address->zip);
			$this->setCidade($company->address->city); 
			$this->setNaturezaJuridica($company->legal_nature->description); 

			$this->setNomeFantasia($company->alias); 

			// echo $company->alias;
			// die();
			$this->setDataAtualizacao($company->last_update);
			$this->setSituacao($company->registration->status); 
			$this->setComplemento($company->address->details);
			$this->setEmail($company->email);
			$this->setMotivoSituacaoCadastral($company->registration->status_reason);
			$this->setMotivo($company->registration->status_reason);
			// $array['situacao_especial'] = $company->registration->special_status;
			// $array['data_situacao_especial'] = $company->registration->special_status_date;
			// $array['capital_social'] = $company->capital;
			// $array['credenciado'] = "";

			$this->setFlagEmpresaSemIe("TRUE");
			$this->setSync(0);
			date_default_timezone_set('America/Bahia');
			$this->setDataCadastro(date('Y-m-d H:i:s'));
			$this->setVinculoContador("0");

			return 'sucesso';
	  		
	  	} 
	}
}