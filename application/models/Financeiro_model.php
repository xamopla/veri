<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Financeiro_model extends CI_Model
{

	private $id;
	private $numero_fatura;
	private $data_vencimento;
	private $valor;
	private $data_pagamento;
	private $forma_pagamento;
	private $situacao;
	private $id_contabilidade;
	
	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getNumeroFatura() {
		return $this->numero_fatura;
	}
	public function setNumeroFatura($numero_fatura) {
		$this->numero_fatura = $numero_fatura;
		return $this;
	}

	public function getDataVencimento() {
		return $this->data_vencimento;
	}
	public function setDataVencimento($data_vencimento) {
		$this->data_vencimento = $data_vencimento;
		return $this;
	}

	public function getValor() {
		return $this->valor;
	}
	public function setValor($valor) {
		$this->valor = $valor;
		return $this;
	}

	public function getDataPagamento() {
		return $this->data_pagamento;
	}
	public function setDataPagamento($data_pagamento) {
		$this->data_pagamento = $data_pagamento;
		return $this;
	}

	public function getFormaPagamento() {
		return $this->forma_pagamento;
	}
	public function setFormaPagamento($forma_pagamento) {
		$this->forma_pagamento = $forma_pagamento;
		return $this;
	}

	public function getSituacao() {
		return $this->situacao;
	}
	public function setSituacao($situacao) {
		$this->situacao = $situacao;
		return $this;
	}

	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}

	public function listar() {
		//$this->db->where("id_contabilidade", $this->getIdContabilidade());
		return $this->db->get('dtb_financeiro')->result();
	}

}