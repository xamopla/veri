<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gerenciar_clientes_model extends CI_Model {

	private $id;
	private $id_empresa;
	private $id_onesignal;
	private $razao_social;
	private $nome_fantasia;
	private $cnpj_empresa;
	private $logradouro;
	private $numero;
	private $complemento;
	private $bairro;
	private $cidade;
	private $uf;
	private $cep;
	private $telefone;
	private $celular;
	private $logo;
	private $id_cadastro;
	private $email;
	private $login;
	private $senha;
	private $status;
	private $data_cadastro;
	private $sm;
	private $data_acesso;
	private $id_alteracao;
	private $data_alteracao;

	// VARIÁVEIS DOS COLABORADORES
	private $nivel;
	private $permissoes;
	private $id_usuario_master;
	private $nome_colaborador;

	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
	   return $this->id;
	}
	
	public function setId($id) {
	   $this->id = $id;
	}

	public function getIdEmpresa() {
	   return $this->id_empresa;
	}
	
	public function setIdEmpresa($id_empresa) {
	   $this->id_empresa = $id_empresa;
	}

	public function getIdOnesignal() {
	   return $this->id_onesignal;
	}
	
	public function setIdOnesignal($id_onesignal) {
	   $this->id_onesignal = $id_onesignal;
	}

	public function getRazaoSocial() {
	   return $this->razao_social;
	}
	
	public function setRazaoSocial($razao_social) {
	   $this->razao_social = $razao_social;
	}

	public function getNomeFantasia() {
	   return $this->nome_fantasia;
	}
	
	public function setNomeFantasia($nome_fantasia) {
	   $this->nome_fantasia = $nome_fantasia;
	}

	public function getCnpjEmpresa() {
	   return $this->cnpj_empresa;
	}
	
	public function setCnpjEmpresa($cnpj_empresa) {
	   $this->cnpj_empresa = $cnpj_empresa;
	}

	public function getLogradouro() {
	   return $this->logradouro;
	}
	
	public function setLogradouro($logradouro) {
	   $this->logradouro = $logradouro;
	}

	public function getNumero() {
	   return $this->numero;
	}
	
	public function setNumero($numero) {
	   $this->numero = $numero;
	}

	public function getComplemento() {
	   return $this->complemento;
	}
	
	public function setComplemento($complemento) {
	   $this->complemento = $complemento;
	}

	public function getBairro() {
	   return $this->bairro;
	}
	
	public function setBairro($bairro) {
	   $this->bairro = $bairro;
	}

	public function getCidade() {
	   return $this->cidade;
	}
	
	public function setCidade($cidade) {
	   $this->cidade = $cidade;
	}

	public function getUf() {
	   return $this->uf;
	}
	
	public function setUf($uf) {
	   $this->uf = $uf;
	}

	public function getCep() {
	   return $this->cep;
	}
	
	public function setCep($cep) {
	   $this->cep = $cep;
	}

	public function getTelefone() {
	   return $this->telefone;
	}
	
	public function setTelefone($telefone) {
	   $this->telefone = $telefone;
	}

	public function getCelular() {
	   return $this->celular;
	}
	
	public function setCelular($celular) {
	   $this->celular = $celular;
	}

	public function getLogo() {
	   return $this->logo;
	}
	
	public function setLogo($logo) {
	   $this->logo = $logo;
	}

	public function getIdCadastro() {
	   return $this->id_cadastro;
	}
	
	public function setIdCadastro($id_cadastro) {
	   $this->id_cadastro = $id_cadastro;
	}

	public function getEmail() {
	   return $this->email;
	}
	
	public function setEmail($email) {
	   $this->email = $email;
	}

	public function getLogin() {
	   return $this->login;
	}
	
	public function setLogin($login) {
	   $this->login = $login;
	}

	public function getSenha() {
	   return $this->senha;
	}
	
	public function setSenha($senha) {
	   $this->senha = $senha;
	}

	public function getStatus() {
	   return $this->status;
	}
	
	public function setStatus($status) {
	   $this->status = $status;
	}

	public function getDataCadastro() {
	   return $this->data_cadastro;
	}
	
	public function setDataCadastro($data_cadastro) {
	   $this->data_cadastro = $data_cadastro;
	}

	public function getSm() {
	   return $this->sm;
	}
	
	public function setSm($sm) {
	   $this->sm = $sm;
	}

	public function getDataAcesso() {
	   return $this->data_acesso;
	}
	
	public function setDataAcesso($data_acesso) {
	   $this->data_acesso = $data_acesso;
	}

	public function getIdAlteracao() {
	   return $this->id_alteracao;
	}
	
	public function setIdAlteracao($id_alteracao) {
	   $this->id_alteracao = $id_alteracao;
	}

	public function getDataAlteracao() {
	   return $this->data_alteracao;
	}
	
	public function setDataAlteracao($data_alteracao) {
	   $this->data_alteracao = $data_alteracao;
	}

	// VARIAVEIS DOS COLABORADORES
	public function getIdUsuarioMaster() {
	   return $this->id_usuario_master;
	}
	
	public function setIdUsuarioMaster($id_usuario_master) {
	   $this->id_usuario_master = $id_usuario_master;
	}

	public function getNivel() {
	   return $this->nivel;
	}
	
	public function setNivel($nivel) {
	   $this->nivel = $nivel;
	}

	public function getPermissoes() {
	   return $this->permissoes;
	}
	
	public function setPermissoes($permissoes) {
	   $this->permissoes = $permissoes;
	}

	public function getNomeColaborador() {
	   return $this->nome_colaborador;
	}
	
	public function setNomeColaborador($nome_colaborador) {
	   $this->nome_colaborador = $nome_colaborador;
	}

	public function ativar_cliente(){
	
		$dados = array(

				'id_empresa' => $this->getIdEmpresa(), 
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'cnpj_empresa' => $this->getCnpjEmpresa(),
				'logradouro' => $this->getLogradouro(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cidade' => $this->getCidade(),
				'uf' => $this->getUf(),
				'cep' => $this->getCep(),
				'telefone' => $this->getTelefone(),
				'celular' => $this->getCelular(),
				'logo' => $this->getLogo(),
				'email' => $this->getEmail(),
				'login' => $this->getLogin(),
				'senha' => $this->getSenha(),
				'status' => $this->getStatus(),
				'nivel' => $this->getNivel(),
				'data_cadastro' => $this->getDataCadastro(),
				'id_cadastro' => $this->getIdCadastro()
		);
	
		if ($this->db->insert('dtb_clientes_empresas', $dados)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}		
	}

	public function editar_cliente(){
	
		$dados = array(				
				
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'cnpj_empresa' => $this->getCnpjEmpresa(),
				'logradouro' => $this->getLogradouro(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cidade' => $this->getCidade(),
				'uf' => $this->getUf(),
				'cep' => $this->getCep(),
				'telefone' => $this->getTelefone(),
				'celular' => $this->getCelular(),
				'logo' => $this->getLogo(),
				'email' => $this->getEmail(),
				'id_alteracao' => $this->getIdAlteracao(),
				'data_alteracao' => $this->getDataAlteracao()
		);
	
		if ($this->db->update('dtb_clientes_empresas', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function criar_id_onesignal($id_cliente){

		$dados = array(				
				
				'id_onesignal' => $this->getIdOnesignal()
		);
	
		if ($this->db->update('dtb_clientes_empresas', $dados, "id={$id_cliente}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function inativar_cliente(){

		$dados = array(				
				
				'status' => 'I'
		);
	
		if ($this->db->update('dtb_clientes_empresas', $dados, "id={$this->getId()}")){
			$this->db->update('dtb_clientes_empresas', $dados, "id_usuario_master={$this->getId()}");
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function ativar_cliente_novamente(){

		$dados = array(				
				
				'status' => 'A'
		);
	
		if ($this->db->update('dtb_clientes_empresas', $dados, "id={$this->getId()}")){
			$this->db->update('dtb_clientes_empresas', $dados, "id_usuario_master={$this->getId()}");
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function alterar_login(){

		$dados = array(				
				
				'login' => $this->getLogin(),
				'senha' => $this->getSenha()
		);
	
		if ($this->db->update('dtb_clientes_empresas', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function alterar_senha_cliente(){

		$dados = array(				
				
				'senha' => $this->getSenha()
		);
	
		if ($this->db->update('dtb_clientes_empresas', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function pesquisar_cliente_id(){

		$this->db->select('*');
		$this->db->where('id', $this->getId());
		return $this->db->get('dtb_clientes_empresas')->row();
	}

	public function pesquisar_cliente_id_for_id($id){

		$this->db->select('*');
		$this->db->where('id', $id);
		return $this->db->get('dtb_clientes_empresas')->row();
	}

	public function verificarLogin($login){
		$this->db->select('COUNT(id) AS valor');
		$this->db->where("login =", $login);
		return $this->db->get('dtb_clientes_empresas')->row();
	}

	public function update_acesso(){
	
		$dados = array(
				'data_acesso' => $this->getDataAcesso()
		);
	
		return $this->db->update('dtb_clientes_empresas', $dados, "id={$this->getId()}");
	
	}

	public function cadastrar_colaborador(){

		$dados = array(

				'id_empresa' => $this->getIdEmpresa(), 
				'id_usuario_master' => $this->getIdUsuarioMaster(),
				'id_onesignal' => $this->getIdOnesignal(),				
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'cnpj_empresa' => $this->getCnpjEmpresa(),
				'logradouro' => $this->getLogradouro(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cidade' => $this->getCidade(),
				'uf' => $this->getUf(),
				'cep' => $this->getCep(),
				'telefone' => $this->getTelefone(),
				'celular' => $this->getCelular(),
				'logo' => $this->getLogo(),
				'email' => $this->getEmail(),
				'login' => $this->getLogin(),
				'senha' => $this->getSenha(),
				'status' => $this->getStatus(),
				'nivel' => $this->getNivel(),
				'permissoes' => $this->getPermissoes(),
				'nome_colaborador' => $this->getNomeColaborador(),
				'data_cadastro' => $this->getDataCadastro(),
				'id_cadastro' => $this->getIdCadastro()
		);
	
		if ($this->db->insert('dtb_clientes_empresas', $dados)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}	
	}

	public function editar_colaborador(){

		$dados = array(				
				
				'nome_colaborador' => $this->getNomeColaborador(), 
				'celular' => $this->getCelular(),
				'email' => $this->getEmail(),
				'id_alteracao' => $this->getIdAlteracao(),
				'data_alteracao' => $this->getDataAlteracao()
		);
	
		if ($this->db->update('dtb_clientes_empresas', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function listar_colaboradores_por_id_cliente($id_empresa){

		$this->db->select('id, id_empresa, id_usuario_master, permissoes, nome_colaborador, login, data_acesso, status');
		$this->db->from('dtb_clientes_empresas');
		$this->db->where('id_usuario_master', $id_empresa);  
		$this->db->group_by('id');
		return $this->db->get()->result();
	}
}