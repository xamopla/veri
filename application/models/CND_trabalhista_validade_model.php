<?php

class CND_trabalhista_validade_model extends CI_Model {

    public function busca_cnd($banco){
        $this->db->select('d.cnpj as cnpj, d.caminho_download');
        $this->db->where('d.cnpj = "01464855000136" ');
        $this->db->group_by('d.cnpj');
        return $this->db->get($banco.'.dtb_certidao_trabalhista d')->result();
    }

    public function atualiza_validade_cnd_trabalhista($banco, $cnpj, $data){
        date_default_timezone_set('America/Sao_Paulo');

        $dados = array( 

                'validade' => $cnpj
        );
    
        if ($this->db->update($banco.'.dtb_certidao_trabalhista', $dados, "cnpj=".$cnpj)){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
