<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dec_model extends CI_Model {

	private $filtro;

	public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	public function listar(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//MENSAGENS DEC
		$sql1_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 1 as tipo, 'Mensagem' as codigo_tipo, m.data_envio as data, m.data_ciencia, m.assunto, m.identificacao, m.lida, m.path_anexo         
							 FROM dtb_dec_caixapostal_mensagens as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) GROUP BY m.id";

		$sql1_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 1 as tipo, 'Mensagem' as codigo_tipo, m.data_envio as data, m.data_ciencia, m.assunto, m.identificacao, m.lida, m.path_anexo            
							 FROM dtb_dec_caixapostal_mensagens as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 GROUP BY m.id";




		//MENSAGEM DEC COMUNICADOS
		$sql2_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 2 as tipo, 'Comunicado' as codigo_tipo, m.data_envio as data, '' as data_ciencia, m.assunto, '' as identificacao, m.lida, m.path_anexo                   
							 FROM dtb_dec_caixapostal_comunicados as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) GROUP BY m.id";

		$sql2_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail,  2 as tipo, 'Comunicado' as codigo_tipo, m.data_envio as data, '' as data_ciencia, m.assunto, '' as identificacao, m.lida, m.path_anexo                    
							 FROM dtb_dec_caixapostal_comunicados as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 GROUP BY m.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
				if($this->getFiltro() == 'LIDAS'){
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.lida = 1 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'NAO_LIDAS'){
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.lida = 0 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'COMUNICADOS'){
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.tipo = 2 ORDER BY dataemail desc";
				}else{
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.tipo = 1 ORDER BY dataemail desc";
				}
			}else{
				$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado ORDER BY dataemail desc";
			}

		}else{
			if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
				if($this->getFiltro() == 'LIDAS'){
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.lida = 1 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'NAO_LIDAS'){
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.lida = 0 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'COMUNICADOS'){
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.tipo = 2 ORDER BY dataemail desc";
				}else{
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.tipo = 1 ORDER BY dataemail desc";
				}
			}else{
				$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado ORDER BY dataemail desc";
			}
			
		}

		return $this->db->query($sql)->result();

	}

	public function listar_mensagens_recebidas(){

		$this->db->select('dtb_dec_qtd_mensagem.cnpj, dtb_dec_qtd_mensagem.razao_social, dtb_dec_qtd_mensagem.qtd_nao_lida');
		$this->db->from('dtb_dec_qtd_mensagem');
		$this->db->join('dtb_empresas dtbe','dtb_dec_qtd_mensagem.cnpj = dtbe.cnpj');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		if($this->getFiltro() != 'TODAS'){
			if($this->getFiltro() == 'LIDAS'){
				$this->db->where("dtb_dec_qtd_mensagem.qtd_nao_lida = 0");
			}elseif($this->getFiltro() == 'NAO_LIDAS'){
				$this->db->where("dtb_dec_qtd_mensagem.qtd_nao_lida != 0");
			}
		}

		return $this->db->get()->result();
	}

	public function qtd(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//MENSAGENS DEC
		$sql1_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 1 as tipo, 'Mensagem' as codigo_tipo, m.data_envio as data, m.data_ciencia, m.assunto, m.identificacao, m.lida        
							 FROM dtb_dec_caixapostal_mensagens as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) GROUP BY m.id";

		$sql1_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 1 as tipo, 'Mensagem' as codigo_tipo, m.data_envio as data, m.data_ciencia, m.assunto, m.identificacao, m.lida       
							 FROM dtb_dec_caixapostal_mensagens as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 GROUP BY m.id";




		//MENSAGEM DEC COMUNICADOS
		$sql2_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 2 as tipo, 'Comunicado' as codigo_tipo, m.data_envio as data, '' as data_ciencia, m.assunto, '' as identificacao, m.lida              
							 FROM dtb_dec_caixapostal_comunicados as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) GROUP BY m.id";

		$sql2_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail,  2 as tipo, 'Comunicado' as codigo_tipo, m.data_envio as data, '' as data_ciencia, m.assunto, '' as identificacao, m.lida               
							 FROM dtb_dec_caixapostal_comunicados as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 GROUP BY m.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
				if($this->getFiltro() == 'LIDAS'){
					$sql = "SELECT COUNT(*) as qtd FROM ( SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.lida = 1 ORDER BY dataemail desc) as a";
				}else if($this->getFiltro() == 'NAO_LIDAS'){
					$sql = "SELECT COUNT(*) as qtd FROM ( SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.lida = 0 ORDER BY dataemail desc) as a";
				}else{
					$sql = "SELECT COUNT(*) as qtd FROM (SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.tipo = 2 ORDER BY dataemail desc) as a";
				}
			}else{
				$sql = "SELECT COUNT(*) FROM (SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado ORDER BY dataemail desc) as a";
			}

		}else{
			if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
				if($this->getFiltro() == 'LIDAS'){
					$sql = "SELECT COUNT(*) as qtd FROM (SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.lida = 1 ORDER BY dataemail desc) as a";
				}else if($this->getFiltro() == 'NAO_LIDAS'){
					$sql = "SELECT COUNT(*) as qtd FROM (SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.lida = 0 ORDER BY dataemail desc) as a";
				}else{
					$sql = "SELECT COUNT(*) as qtd FROM (SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.tipo = 2 ORDER BY dataemail desc) as a";
				}
			}else{
				$sql = "SELECT COUNT(*) as qtd FROM (SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado ORDER BY dataemail desc) as a";
			}
			
		}

		return $this->db->query($sql)->row();

	}


	///////////////////////////////////////////NOVO DEC/////////////////////////////////////////
	public function qtd_mensagens_nao_lidas_dec(){
		$this->db->select('SUM(dtb_dec_qtd_mensagem.qtd_nao_lida) as total');
		$this->db->from('dtb_dec_qtd_mensagem');
		$this->db->join('dtb_empresas dtbe','dtb_dec_qtd_mensagem.cnpj = dtbe.cnpj');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		return $this->db->get()->row();
	}

	public function listar_modal($cnpj){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//MENSAGENS DEC
		$sql1_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 1 as tipo, 'Mensagem' as codigo_tipo, m.data_envio as data, m.data_ciencia, m.assunto, m.identificacao, m.lida, m.path_anexo         
							 FROM dtb_dec_caixapostal_mensagens as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND m.cnpj = ".$cnpj." GROUP BY m.id";

		$sql1_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 1 as tipo, 'Mensagem' as codigo_tipo, m.data_envio as data, m.data_ciencia, m.assunto, m.identificacao, m.lida, m.path_anexo            
							 FROM dtb_dec_caixapostal_mensagens as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) WHERE m.cnpj = ".$cnpj." 
							 GROUP BY m.id";




		//MENSAGEM DEC COMUNICADOS
		$sql2_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 2 as tipo, 'Comunicado' as codigo_tipo, m.data_envio as data, '' as data_ciencia, m.assunto, '' as identificacao, m.lida, m.path_anexo                   
							 FROM dtb_dec_caixapostal_comunicados as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND m.cnpj = ".$cnpj." GROUP BY m.id";

		$sql2_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail,  2 as tipo, 'Comunicado' as codigo_tipo, m.data_envio as data, '' as data_ciencia, m.assunto, '' as identificacao, m.lida, m.path_anexo                    
							 FROM dtb_dec_caixapostal_comunicados as m 
							 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE m.cnpj = ".$cnpj." 
							 GROUP BY m.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
				if($this->getFiltro() == 'LIDAS'){
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.lida = 1 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'NAO_LIDAS'){
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.lida = 0 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'COMUNICADOS'){
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.tipo = 2 ORDER BY dataemail desc";
				}else{
					$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado WHERE resultado.tipo = 1 ORDER BY dataemail desc";
				}
			}else{
				$sql = "SELECT * FROM (".$sql1_colaborador." UNION ".$sql2_colaborador." ) as resultado ORDER BY dataemail desc";
			}

		}else{
			if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
				if($this->getFiltro() == 'LIDAS'){
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.lida = 1 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'NAO_LIDAS'){
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.lida = 0 ORDER BY dataemail desc";
				}else if($this->getFiltro() == 'COMUNICADOS'){
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.tipo = 2 ORDER BY dataemail desc";
				}else{
					$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado WHERE resultado.tipo = 1 ORDER BY dataemail desc";
				}
			}else{
				$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." ) as resultado ORDER BY dataemail desc";
			}
			
		}

		return $this->db->query($sql)->result();

	}
	
}