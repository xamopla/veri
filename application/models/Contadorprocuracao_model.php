<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contadorprocuracao_model extends CI_Model {

    private $id;
    private $id_empresa;
    private $id_contador;

    function __construct()
    {
        parent::__construct();
    }

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getIdEmpresa() {
        return $this->id_empresa;
    }
    public function setIdEmpresa($id_empresa) {
        $this->id_empresa = $id_empresa;
        return $this;
    }

    public function getIdContador() {
        return $this->id_contador;
    }
    public function setIdContador($id_contador) {
        $this->id_contador = $id_contador;
        return $this;
    }

    public function findAllByIdEmpresa() {
        $this->db->select('id_contador as id');
        $this->db->where('id_empresa', $this->getIdEmpresa());

        return $this->db->get('dtb_contador_procuracao')->result();
    }

    public function buscar_empresas_vinculadas($id){
        $this->db->select('dtb_contador_procuracao.id_empresa as id, e.razao_social, e.cnpj');
        $this->db->join('dtb_empresas as e', 'dtb_contador_procuracao.id_empresa = e.id', 'left');
        $this->db->where('dtb_contador_procuracao.id_contador', $id);

        return $this->db->get('dtb_contador_procuracao')->result();
    }

    public function buscar_empresa($id, $cnpj){
        $this->db->select('dtb_contador_procuracao.id_empresa, e.razao_social, e.cnpj');
        $this->db->join('dtb_empresas as e', 'dtb_contador_procuracao.id_empresa = e.id', 'left');
        $this->db->where('dtb_contador_procuracao.id_contador', $id);
        $this->db->where('e.cnpj', preg_replace("/[^0-9]/", "", $cnpj));

        return $this->db->get('dtb_contador_procuracao')->row();
    }

}