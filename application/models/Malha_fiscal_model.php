<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Malha_fiscal_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	public function listar() {	
		$this->db->select('m.id, m.id_mensagem, m.cnpj, m.razao_social, m.assunto, m.conteudo, m.lida, m.data, m.data_execucao, m.situacao');
		$this->db->from('dtb_malha_fiscal m');
		$this->db->join('dtb_empresas dtbe','trim(m.cnpj) = trim(dtbe.cnpj)');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
            if($this->getFiltro() == "REGULARIZADO"){
            	$this->db->where("m.situacao = 'REGULARIZADO' ");
            }else if($this->getFiltro() == "ANALISANDO"){
            	$this->db->where("m.situacao = 'ANALISANDO'");
            }else if($this->getFiltro() == "PENDENCIAS"){
            	$this->db->where("m.situacao = 'PENDENCIAS'");
            }else if($this->getFiltro() == "CLIENTE"){
            	$this->db->where("m.situacao = 'CLIENTE'");
            }
        }

		return $this->db->get()->result();
	}


	public function qtd() {	
		$this->db->select('COUNT(*) as qtd');
		$this->db->from('dtb_malha_fiscal m');
		$this->db->join('dtb_empresas dtbe','trim(m.cnpj) = trim(dtbe.cnpj)');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
            if($this->getFiltro() == "REGULARIZADO"){
            	$this->db->where("m.situacao = 'REGULARIZADO' ");
            }else if($this->getFiltro() == "ANALISANDO"){
            	$this->db->where("m.situacao = 'ANALISANDO'");
            }else if($this->getFiltro() == "PENDENCIAS"){
            	$this->db->where("m.situacao = 'PENDENCIAS'");
            }else if($this->getFiltro() == "CLIENTE"){
            	$this->db->where("m.situacao = 'CLIENTE'");
            }
        }

		return $this->db->get()->row();
	}

	public function find_info_for_modal($id){
		$this->db->select('*');
		$this->db->from('dtb_malha_fiscal cm');
		$this->db->where("cm.id", $id);
		return $this->db->get()->row();
	}


	public function find_ultimo_historico($id){
		$sql = "SELECT * FROM dtb_malha_fiscal_historico m WHERE m.id_malha_fiscal = ".$id." ORDER BY m.id DESC LIMIT 1";

		return $this->db->query($sql)->row();
	}


	public function find_todos_historicos($id){
		$sql = "SELECT * FROM dtb_malha_fiscal_historico m WHERE m.id_malha_fiscal = ".$id." ORDER BY m.id DESC";

		return $this->db->query($sql)->result();
	}

	public function atualiza_situacao($id, $situacao, $cnpj, $descricao){
		$this->db->set('situacao', $situacao);
		$this->db->where('id', $id);
		$this->db->update('dtb_malha_fiscal');

		$this->insere_historico($id, $situacao, $cnpj, $descricao);
	}

	public function insere_historico($id, $situacao, $cnpj, $descricao){
		date_default_timezone_set('America/Sao_Paulo');
		$usuario = "";
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		$dados = array(
	
				'id_malha_fiscal' => $id,
				'cnpj' => $cnpj,
				'data_alteracao' => date('Y-m-d H:i:s'),
				'nome_usuario' => $usuario,
				'situacao' => $situacao,
				'descricao' => $descricao
		);
	
		if ($this->db->insert('dtb_malha_fiscal_historico', $dados)){
			return 1;
		} else {
			return 0;
		}
	}

	public function buscar_historico($id){
		$sql = "SELECT * FROM dtb_malha_fiscal_historico m WHERE m.id_malha_fiscal = ".$id." ORDER BY m.id DESC ";

		return $this->db->query($sql)->result();
	}


	public function buscar_mensagem_ecac($malha){
		$this->db->select('m.id as id_caixa_postal_mensagem, m.caixa_postal_id, m.assunto, m.recebida_em');
		$this->db->from('dtb_ecac_caixa_postal_mensagem m');
		$this->db->join('dtb_ecac_caixa_postal c','m.caixa_postal_id = c.id');
		$this->db->where("m.recebida_em", $malha->data);
		$this->db->where("m.assunto", $malha->assunto);
		// $this->db->where("m.id_mensagem", $malha->id_mensagem);
		$this->db->where("trim(c.cnpj_data)", $malha->cnpj);
		return $this->db->get()->row();
	}



	public function buscar_lida_por($cnpj, $assunto, $recebida_em){
		$this->db->select('m.nome_usuario, m.data_alteracao');
		$this->db->from('dtb_historico_leitura_mensagem_ecac m');
		$this->db->where("m.recebida_em", $recebida_em);
		$this->db->where("m.assunto", $assunto);
		$this->db->where("trim(m.cnpj)", $cnpj);
		return $this->db->get()->row();
	}

}