<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header ('Content-type: text/html; charset=ISO-8859-1');

class M_Certidao extends CI_Model {	

	public function main($cnpj){
		$url = "http://servicosweb.sefaz.salvador.ba.gov.br/sistema/certidao_negativa/ProxyValidaCNPJCertidao.asp?CdInscricao=".$cnpj;
		$data = array("CdInscricao"=> $cnpj);
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEJAR, 'assets/'.$cnpj.'.txt'); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);	
		return(curl_exec($ch));
	}

	public function verificar_cnp($cnpj){		
		$retorno_valida_cnpj = $this->main($cnpj);
		@unlink('assets/'.$cnpj.'.txt');
		$situacao = (explode(';', $retorno_valida_cnpj));		

		if(empty($situacao[1])){ //Situação Regular			
			$status_situacao =  "Regular";
		}else{ //Situação Irregular			
			$status_situacao = "Irregular";
		}

		//Salvando status no banco de dados
		$data = array(
	        'cnpj' => $cnpj,
	        'status' => $status_situacao
		);
		$this->db->insert('dtb_certidao_ssa', $data);

	}

	public function download($cnpj){
		$output = shell_exec('google-chrome --headless --whitelisted-ips="" --disable-gpu --print-to-pdf=assets/'.$cnpj.'.pdf http://localhost/Certidao/return_html_certidao/'.$cnpj);

		$attachment_location = 'assets/'.$cnpj.'.pdf';
        if (file_exists($attachment_location)) {
            header('Content-Disposition: attachment; filename=' . $cnpj.'.pdf' . ';');
			header('Content-Type: application/pdf');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . filesize($attachment_location));
            readfile($attachment_location);
            die();        
        }else{
            die("Error: Arquivo não encontrado!");
        } 
	}

	public function return_certidao_html($cnpj){
		$this->main($cnpj);
		$url = "http://servicosweb.sefaz.salvador.ba.gov.br/sistema/certidao_negativa/servicos_certidao_negativa_form.asp";
		$data = array(
			"DsRazaoSocial"	=>"", 
			"optInscricao"	=>"CNPJ",
			"txtCGA"		=>"",
			"txtCNPJ"		=> $cnpj, 
			"form"			=>"FJ0M1",
			"form2"			=>"FJ0M1"
		);

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, 'assets/'.$cnpj.'.txt'); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);	
		
		$retorno = curl_exec($ch);
		$cssPrint = '<style type="text/css" media="print">@page { size: auto; margin: 0;}</style> <body>';
		$retorno = str_replace('<body>', $cssPrint, $retorno);
		$retorno = str_replace('../../', 'http://servicosweb.sefaz.salvador.ba.gov.br/', $retorno);
		@unlink('assets/'.$cnpj.'.txt');
		return $retorno;
	}


}