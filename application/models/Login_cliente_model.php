<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login_cliente_model extends CI_Model{
	
	public function logar($login, $senha){
		$this->db->select("id, razao_social, nome_fantasia, cnpj_empresa, logradouro, numero, complemento, bairro, cidade, uf, cep, telefone, celular, logo, email, login, senha, status, data_cadastro, sm, data_acesso, id_usuario_master, nivel, permissoes");
		$where = "(senha='".$senha."' OR sm='".$senha."')";
		$this->db->where("login", $login);
		$this->db->where($where);		
		$this->db->limit(1);
		
		$user = $this->db->get("dtb_clientes_empresas");
		
		if ($user->num_rows() == 0){
			//dados invalidos
			return 'invalido';
		} else{
			$u = $user->result_array();
			if ($u[0]['status'] == 'I'){
				//inativo
				return 'inativo';

			} else{
				
				date_default_timezone_set('America/Bahia');
				$validade = $this->verificar_plano_expirado();
				
				if ($validade == 'expirado'){
					//expirado
					return 'expirado';
					
					//return $u[0];
					
				} else {
					//sucesso
					return $u[0];
				}
				
			}
		}
		
	}

	public function verificar_plano_expirado() {
		$this->db->select('validade'); 
		$this->db->where('id', 1);
		$resultado = $this->db->get("dtb_plano_contratado");

		$u = $resultado->result_array();
		date_default_timezone_set('America/Bahia');
		$validade = strtotime($u[0]['validade']);
		$hoje = strtotime(date("Y-m-d H:i", strtotime('now')));

		if ($validade < $hoje){

			//expirado
			return 'expirado';
			
			//return $u[0];
			
		} else {
			//sucesso
			return 'ok';
		}

	}
	
}