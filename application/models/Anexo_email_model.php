<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Anexo_email_model extends CI_Model {

	private $id;
	private $id_email;
	private $anexo;
	private $path_anexo;
	private $nome_anexo;
	private $id_cadastro;
	private $data_cadastro; 

	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
	   return $this->id;
	}
	
	public function setId($id) {
	   $this->id = $id;
	}

	public function getIdEmail() {
	   return $this->id_email;
	}
	
	public function setIdEmail($id_email) {
	   $this->id_email = $id_email;
	}

	public function getAnexo() {
	   return $this->anexo;
	}
	
	public function setAnexo($anexo) {
	   $this->anexo = $anexo;
	}

	public function getAnexoPath() {
	   return $this->path_anexo;
	}
	
	public function setAnexoPath($path_anexo) {
	   $this->path_anexo = $path_anexo;
	}	

	public function getNomeAnexo() {
	   return $this->nome_anexo;
	}
	
	public function setNomeAnexo($nome_anexo) {
	   $this->nome_anexo = $nome_anexo;
	}

	public function getDataCadastro() {
	   return $this->data_cadastro;
	}
	
	public function setDataCadastro($data_cadastro) {
	   $this->data_cadastro = $data_cadastro;
	}

	public function getIdCadastro() {
	   return $this->id_cadastro;
	}
	
	public function setIdCadastro($id_cadastro) {
	   $this->id_cadastro = $id_cadastro;
	}

	public function cadastrar(){
		
		$path = $this->getAnexoPath();

		$dados = array(

				'id_email' => $this->getIdEmail(),
				'anexo' => $this->getAnexo(),
				'path_anexo' => $path,
				'nome_anexo' => $this->getNomeAnexo(),
				'id_cadastro' => $this->getIdCadastro(),
				'data_cadastro' => $this->getDataCadastro()
		);
	
		if ($this->db->insert('dtb_anexos_emails', $dados)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}

	public function qtd_anexos_email_contador($id_email){

		$this->db->select('COUNT(distinct(dtb_anexos_emails.id)) AS valor'); 

		$this->db->where('id_email', $id_email);

		return $this->db->get('dtb_anexos_emails')->row();
	}

	public function qtd_anexos_email_cliente($id_email){

		$this->db->select('COUNT(distinct(dtb_anexos_emails.id)) AS valor'); 

		$this->db->where('id_email', $id_email);

		return $this->db->get('dtb_anexos_emails')->row();
	}


	public function listar_anexos($id_email){

		$this->db->select('anexo, path_anexo, nome_anexo'); 
		
		$this->db->where('id_email', $id_email); 
		return $this->db->get('dtb_anexos_emails')->result();
	}
}	