<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parcelamento_pert_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	public function listar_pedidos(){

		$this->db->select('db.id, db.cnpj, db.numero, db.data_pedido, db.situacao, db.data_situacao, db.valor_total_consolidado_entrada, db.qtd_parcelas, db.parcela_basica, db.valor_total_consolidado_divida, db.data_consolidacao, e.razao_social');

		$this->db->from('dtb_ecac_pert_pedidos_parcelamentos db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

        $this->db->where('e.cnpj like "%0001%"');

		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
            if($this->getFiltro() == 'ATIVOS'){
                $this->db->where("db.situacao = 'Em parcelamento'");
            }else if($this->getFiltro() == 'INATIVOS'){
                $this->db->where("db.situacao != 'Em parcelamento'");
            }
        }

		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}


	public function listar_pedidos_individual($id){

		$this->db->select('db.id, db.cnpj, db.numero, db.data_pedido, db.situacao, db.data_situacao, db.valor_total_consolidado_entrada, db.qtd_parcelas, db.parcela_basica, db.valor_total_consolidado_divida, db.data_consolidacao, e.razao_social');
		$this->db->from('dtb_ecac_pert_pedidos_parcelamentos db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');
        $this->db->where('e.cnpj like "%0001%"');

		$this->db->where('db.id', $id);
		return $this->db->get()->row();
	}

	public function listar_debitos($id){

		$this->db->select('*');
		$this->db->from('dtb_ecac_pert_relacao_debitos_parcelas db');
		
		$this->db->where('db.id_parcelamento', $id);
		return $this->db->get()->result();
	}

	public function listar_pagamentos($id){

		$this->db->select('*');
		$this->db->from('dtb_ecac_pert_demonstrativo_pagamentos db');
		
		$this->db->where('db.id_parcelamento', $id);
		return $this->db->get()->result();
	}

	public function listar_parcelas(){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_atual = date('m');
	  	$ano_atual = date('Y');

	  	$mes_parcela = $mes_atual."/".$ano_atual;

		$this->db->select('*');
		$this->db->from('dtb_pert_parcelas_emitidas db');
		$this->db->join('dtb_ecac_pert_pedidos_parcelamentos e','trim(db.cnpj) = trim(e.cnpj)');
		$this->db->where("e.situacao = 'Em parcelamento'");
		$this->db->where("trim(db.data_parcela) != ", $mes_parcela);
		return $this->db->get()->result();
	}

	public function listar_parcelas_pagas(){

		$this->db->select('*');
		$this->db->from('dtb_ecac_pert_demonstrativo_pagamentos db');
		$this->db->join('dtb_ecac_pert_pedidos_parcelamentos e','trim(db.cnpj) = trim(e.cnpj)');
		$this->db->where("e.situacao = 'Em parcelamento'");
		return $this->db->get()->result();
	}

	public function qtd_pagamentos($id){
		$this->db->select('count(*) as qtd');
		$this->db->from('dtb_ecac_pert_demonstrativo_pagamentos db');
		
		$this->db->where('db.id_parcelamento', $id);
		// $this->db->group_by("db.id");
		return $this->db->get()->row();
	}




	// Metodos da Modal

	public function listar_parcelas_for_modal($cnpj){

		$this->db->select('*');
		$this->db->from('dtb_pert_parcelas_emitidas db');
		$this->db->where("db.cnpj", $cnpj);
		$this->db->group_by("db.cnpj, trim(db.data_parcela)");
		return $this->db->get()->result();
	}

	public function listar_parcelas_pagas_for_modal($cnpj){

		$this->db->select('*');
		$this->db->from('dtb_ecac_pert_demonstrativo_pagamentos db');
		$this->db->where("db.cnpj", $cnpj);
		return $this->db->get()->result();
	}


	public function listar_parcelas_for_modal_detalhe($cnpj, $data){

		$this->db->select('trim(id) as id, trim(data_parcela) as data_parcela, trim(cnpj) as cnpj, trim(valor) as valor, trim(path_download_parcela) as path_download_parcela');
		$this->db->from('dtb_pert_parcelas_emitidas db');
		$this->db->where("db.cnpj", $cnpj);
		$this->db->where("trim(db.data_parcela)", trim($data));
		return $this->db->get()->row();
	}


	public function pagou_parcela_mes_atual($mes, $id_parcelamento){
		$this->db->select('*');
		$this->db->from('dtb_ecac_pert_demonstrativo_pagamentos db');
		
		$this->db->where('db.id_parcelamento', $id_parcelamento);
		$this->db->where('db.mes_parcela', $mes);
		return $this->db->get()->row();
	}

	public function get_parcela_atual($mes, $cnpj){
		$this->db->select('*');
		$this->db->from('dtb_pert_parcelas_emitidas db');
		
		$this->db->where('db.cnpj', $cnpj);
		$this->db->where('db.data_parcela', $mes);
		return $this->db->get()->row();
	}


	public function get_qtd_parcelamentos(){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_ecac_pert_pedidos_parcelamentos db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}


		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
            if($this->getFiltro() == 'EM_PARCELAMENTO'){
                $this->db->where("db.situacao = 'Em parcelamento'");
            }else if($this->getFiltro() == 'ENCERRADO_RECISAO'){
                $this->db->where("db.situacao = 'Encerrado por Rescisão'");
            }else if($this->getFiltro() == 'ENCERRADO_CONTRIBUINTE'){
                $this->db->where("db.situacao = 'Encerrado a Pedido do Contribuinte'");
            }
        }

		return $this->db->get()->row();
	}

}