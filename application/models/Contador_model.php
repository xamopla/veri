<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contador_model extends CI_Model{
	
	private $id;
	private $nome;
	private $crc;
	private $id_contabilidade;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getNome() {
		return $this->nome;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}
	public function getCrc() {
		return $this->crc;
	}
	public function setCrc($crc) {
		$this->crc = $crc;
		return $this;
	}
	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}
	
	public function cadastrar(){
	
		$dados = array(
	
				'nome' => $this->getNome(),
				'crc' => $this->getCrc(),
				'id_contabilidade' => $this->getIdContabilidade()
		);
	
		if ($this->db->insert('contadores', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function cadastrar_novo(){
	
		$dados = array(
	
				'nome' => $this->getNome(),
				'crc' => $this->getCrc(),
				'id_contabilidade' => $this->getIdContabilidade()
		);
	
		if ($this->db->insert('contadores', $dados)){
			$id = $this->db->insert_id();
			return $id;
		} else {
			return FALSE;
		}
	}
	
	public function listar() {
		$this->db->select('*');
		return $this->db->get('contadores')->result();
	}
	
	public function excluir(){
		$this->excluirContadorProcuracao($this->getId());
		$this->excluirProcuracao($this->getId());
		return $this->db->delete('contadores', "id = {$this->getId()}");
	}

	public function excluirContadorProcuracao($id){
		return $this->db->delete('dtb_contador_procuracao', "id_contador = {$this->getId()}");
	}

	public function excluirProcuracao($id){
		return $this->db->delete('dtb_certificado_contador', "id_contador = {$this->getId()}");
	}

	public function pesquisar_contador_id(){
		$this->db->select('*');
		$this->db->where("contadores.id", $this->getId());
		return $this->db->get('contadores')->row();
	}
	
	public function editar(){
	
		$dados = array(
			
				'nome' => $this->getNome(),
				'crc' => $this->getCrc(),
		);
	
		if ($this->db->update('contadores', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

    public function atualizaListaEmpresaProcuracao($id_contador, $empresas){
        $this->db->delete('dtb_contador_procuracao', array('id_contador' => $id_contador));

        if(isset($empresas) && sizeof($empresas) != 0){
            foreach ($empresas as $empresa) {
                $data = array(
                    'id_empresa' =>  $empresa,
                    'id_contador' =>  $id_contador
                );

                $this->db->insert('dtb_contador_procuracao', $data);
            }
        }

        return TRUE;

    }
	
}