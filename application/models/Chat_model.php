<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bahia');

class Chat_model extends CI_Model{

	private $id;
	private $id_envia;
	private $id_recebe;
	private $mensagem;
	private $hora;
	private $data;
	private $data_only;
	private $lida;

	public function getId() {
	    return $this->id;
	}
	 
	public function setId($id) {
	    $this->id = $id;
	}

	public function getId_envia() {
	    return $this->id_envia;
	}
	 
	public function setId_envia($id_envia) {
	    $this->id_envia = $id_envia;
	}

	public function getId_recebe() {
	    return $this->id_recebe;
	}
	 
	public function setId_recebe($id_recebe) {
	    $this->id_recebe = $id_recebe;
	}

	public function getMensagem() {
	    return $this->mensagem;
	}
	 
	public function setMensagem($mensagem) {
	    $this->mensagem = $mensagem;
	}

	public function getHora() {
	    return $this->hora;
	}
	 
	public function setHora($hora) {
	    $this->hora = $hora;
	}

	public function getData() {
	    return $this->data;
	}
	 
	public function setData($data) {
	    $this->data = $data;
	}
	public function getData_only() {
	    return $this->data_only;
	}
	 
	public function setData_only($data_only) {
	    $this->data_only = $data_only;
	}
	public function getLida() {
	    return $this->lida;
	}
	 
	public function setLida($lida) {
	    $this->lida = $lida;
	}

	public function listar_recentes(){
		$this->db->select('*');
		$this->db->from('dtb_chat as chat');
		$this->db->group_start();
			$this->db->where("id_recebe", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('id_envia', $this->session->userdata['userprimesession']['id']);
        $this->db->group_end();
        $this->db->order_by('chat.data desc');
        $this->db->group_by('chat.id');
		return $this->db->get()->result();
	}

	public function lista_ultima_mensagem($id){
		$sql = 'SELECT * FROM dtb_chat where (id_recebe = '.$id.' AND id_envia = '.$this->session->userdata['userprimesession']['id'].') OR (id_recebe = '.$this->session->userdata['userprimesession']['id'].' AND id_envia = '.$id.') order by id desc LIMIT 1';

		return $this->db->query($sql)->row();
		
	}

	public function numero_msg_nao_lidas($id){
		$this->db->select('count(*) as qtd');
		$this->db->from('dtb_chat as chat');
		$this->db->where("id_recebe", $this->session->userdata['userprimesession']['id']);
		$this->db->where('id_envia', $id);
		$this->db->where('lida', 0);
        $this->db->group_by('chat.id');
		return $this->db->get()->row();
	}

	public function buscar_conversa($id){
		$sql = 'SELECT id_recebe, id_envia, mensagem, hora, lida FROM dtb_chat where (id_recebe = '.$id.' AND id_envia = '.$this->session->userdata['userprimesession']['id'].') OR (id_recebe = '.$this->session->userdata['userprimesession']['id'].' AND id_envia = '.$id.') order by id ASC';

		return $this->db->query($sql)->result();
		
	}

	public function cadastrarMensagem(){
		date_default_timezone_set('America/Bahia');

		$hora = date('H:i:s');
		$data = date("Y-m-d H:i:s");
		$data_only = date('Y-m-d') ;
		$lida = 0;

		$dados = array(
	
				'id_envia' => $this->getId_envia(),
				'id_recebe' => $this->getId_recebe(),
				'mensagem' => $this->getMensagem(),
				'hora' => $hora,
				'data' => $data,
				'data_only' => $data_only,
				'lida' => $lida

		);
	
		if ($this->db->insert('dtb_chat', $dados)){
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}

	public function marcar_como_lida(){
		$sql = 'UPDATE dtb_chat set lida = 1 WHERE id_envia = '.$this->getId_envia().' AND id_recebe = '.$this->getId_recebe();

		$this->db->query($sql);
		return 'TRUE';
	}

	public function monitora_conversa_atual(){
		$sql = 'SELECT id, id_recebe, id_envia, mensagem, hora, notificacao FROM dtb_chat WHERE id_envia = '.$this->getId_envia().' AND id_recebe = '.$this->getId_recebe().' AND lida = 0 order by id ASC';

		return $this->db->query($sql)->result();
		
	}

	public function marcar_como_lida_msg($id){
		$sql = 'UPDATE dtb_chat set lida = 1 WHERE id = '.$id;

		$this->db->query($sql);
	}

	public function monitora_outras_conversas(){
		$sql = 'SELECT count(*) as qtd, max(id) as max, id_envia FROM dtb_chat WHERE id_envia != '.$this->getId_envia().' AND id_recebe = '.$this->getId_recebe().' AND lida = 0 group by id_envia ';

		return $this->db->query($sql)->result();
	}

	public function monitora_outras_conversas_head(){
		$sql = 'SELECT count(*) as qtd FROM dtb_chat WHERE id_envia != '.$this->getId_envia().' AND id_recebe = '.$this->getId_recebe().' AND lida = 0 group by id_envia ';

		return $this->db->query($sql)->row();
	}

	public function lista_ultima_mensagem_outros_chat($id){
		$sql = 'SELECT mensagem, hora, notificacao FROM dtb_chat where id = '.$id.' order by data desc LIMIT 1';

		return $this->db->query($sql)->row();
		
	}

	public function verifica_ja_lida(){
		$sql = 'SELECT count(*) as qtd FROM dtb_chat WHERE id_envia = '.$this->getId_envia().' AND id_recebe = '.$this->getId_recebe().' AND lida = 0 group by id_envia ';

		return $this->db->query($sql)->row();
	}

	public function update_notificacao($id){
		$sql = 'UPDATE dtb_chat set notificacao = 1 WHERE id = '.$id;

		$this->db->query($sql);
	}

}