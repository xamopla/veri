<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Certidao_negativa_model extends CI_Model {

    private $filtro;
    private $filtro_uf;
    private $id;
    private $observacao;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getObservacao() {
        return $this->observacao;
    }
    public function setObservacao($observacao) {
        $this->observacao = $observacao;
        return $this;
    }

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

    public function getFiltro_uf() {
        return $this->filtro_uf;
    }
     
    public function setFiltro_uf($filtro_uf) {
        $this->filtro_uf = $filtro_uf;
    }

    public function getFiltro_cidade() {
        return $this->filtro_cidade;
    }
     
    public function setFiltro_cidade($filtro_cidade) {
        $this->filtro_cidade = $filtro_cidade;
    }

    public function listar_municipal(){

        $sql_sp_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_aracaju_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_aracaju cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_arapiraca_al_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'AL' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_arapiraca_al cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";        

        $sql_belo_horizonte_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_belohorizonte cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_boa_vista_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_boa_vista cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_campinas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_campinas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_contagem_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_contagem cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_cuiaba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MT' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_cuiaba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_curitiba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'PR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_curitiba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_flores_da_cunha_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_floresdacunha_rs cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_florianopolis_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_florianopolis cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_fortaleza_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'CE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_fortaleza cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_franca_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_franca cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_goiania_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'GO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_goiania cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_guarapari_es_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_guarapari_es cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_joao_pessoa_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'PB' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_joao_pessoa cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_palmas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'TO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_palmas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_porto_alegre_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_portoalegre cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_porto_velho_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_porto_velho cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rio_branco_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'AC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_rio_branco cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_varginha_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_varginha cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_vila_velha_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_vila_velha cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_vitoria_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_vitoria_es cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_teresina_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'PI' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_teresina cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_uberaba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_uberaba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_uberlandia_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_uberlandia cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_tres_pontas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_tres_pontas_mg cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_guarulhos_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_guarulhos_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_tres_coracoes_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_tres_coracoes_mg cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_votuporanga_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_votuporanga_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_mirassol_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_mirassol_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_altas_flores_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MT' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_altas_flores_mt cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador'])."))";

            $sql = $sql_sp_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_aracaju_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_arapiraca_al_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_belo_horizonte_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_boa_vista_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_campinas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_contagem_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_cuiaba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_curitiba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_flores_da_cunha_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_florianopolis_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_fortaleza_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_franca_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_goiania_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_guarapari_es_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_joao_pessoa_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_palmas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_porto_alegre_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_porto_velho_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_branco_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_varginha_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_vila_velha_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_vitoria_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_teresina_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_uberlandia_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_uberaba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_tres_pontas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_guarulhos_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_tres_coracoes_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_votuporanga_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_mirassol_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_altas_flores_admin.$sql_not_admin." GROUP BY cnpj";

        }else{
            $sql = $sql_sp_admin." GROUP BY cnpj"." UNION ".$sql_aracaju_admin." GROUP BY cnpj"." UNION ".$sql_arapiraca_al_admin." GROUP BY cnpj"." UNION ".$sql_belo_horizonte_admin." GROUP BY cnpj"." UNION ".$sql_boa_vista_admin." GROUP BY cnpj"." UNION ".$sql_campinas_admin." GROUP BY cnpj"." UNION ".$sql_contagem_admin." GROUP BY cnpj"." UNION ".$sql_cuiaba_admin." GROUP BY cnpj"." UNION ".$sql_curitiba_admin." GROUP BY cnpj"." UNION ".$sql_flores_da_cunha_admin." GROUP BY cnpj"." UNION ".$sql_florianopolis_admin." GROUP BY cnpj"." UNION ".$sql_fortaleza_admin." GROUP BY cnpj"." UNION ".$sql_franca_admin." GROUP BY cnpj"." UNION ".$sql_goiania_admin." GROUP BY cnpj"." UNION ".$sql_guarapari_es_admin." GROUP BY cnpj"." UNION ".$sql_joao_pessoa_admin." GROUP BY cnpj"." UNION ".$sql_palmas_admin." GROUP BY cnpj"." UNION ".$sql_porto_alegre_admin." GROUP BY cnpj"." UNION ".$sql_porto_velho_admin." GROUP BY cnpj"." UNION ".$sql_rio_branco_admin." GROUP BY cnpj"." UNION ".$sql_varginha_admin." GROUP BY cnpj"." UNION ".$sql_vila_velha_admin." GROUP BY cnpj"." UNION ".$sql_vitoria_admin." GROUP BY cnpj"." UNION ".$sql_teresina_admin." GROUP BY cnpj"." UNION ".$sql_uberlandia_admin." GROUP BY cnpj"." UNION ".$sql_uberaba_admin." GROUP BY cnpj"." UNION ".$sql_tres_pontas_admin." GROUP BY cnpj"." UNION ".$sql_guarulhos_admin." GROUP BY cnpj"." UNION ".$sql_tres_coracoes_admin." GROUP BY cnpj"." UNION ".$sql_votuporanga_admin." GROUP BY cnpj"." UNION ".$sql_mirassol_admin." GROUP BY cnpj"." UNION ".$sql_altas_flores_admin." GROUP BY cnpj";
        }

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 1 ";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 0 ";
            }

            if($this->getFiltro_uf() != null && $this->getFiltro_uf() != 'Todas'){
                $sql_aux = $sql_aux." AND consulta.uf = '".$this->getFiltro_uf()."'";
            }

            if($this->getFiltro_cidade() != null && $this->getFiltro_cidade() != 'Todas'){
                $sql_aux = $sql_aux." AND consulta.cidade = '".$this->getFiltro_cidade()."'";
            }
        }else{
            if($this->getFiltro_uf() != null && $this->getFiltro_uf() != 'Todas'){
                $sql_aux = $sql_aux." WHERE consulta.uf = '".$this->getFiltro_uf()."'";
            }

            if($this->getFiltro_cidade() != null && $this->getFiltro_cidade() != 'Todas'){
                $sql_aux = $sql_aux." WHERE consulta.cidade = '".$this->getFiltro_cidade()."'";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->result();

    }

    public function qtd_municipal_irregular(){

        $sql_sp_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_aracaju_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_aracaju cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_arapiraca_al_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'AL' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_arapiraca_al cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";        

        $sql_belo_horizonte_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_belohorizonte cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_boa_vista_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_boa_vista cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_campinas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_campinas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_contagem_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_contagem cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_cuiaba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MT' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_cuiaba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_curitiba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'PR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_curitiba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_flores_da_cunha_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_floresdacunha_rs cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_florianopolis_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_florianopolis cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_fortaleza_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'CE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_fortaleza cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_franca_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_franca cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_goiania_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'GO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_goiania cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_guarapari_es_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_guarapari_es cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_joao_pessoa_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'PB' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_joao_pessoa cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_palmas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'TO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_palmas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_porto_alegre_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_portoalegre cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_porto_velho_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'RO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_porto_velho cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rio_branco_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'AC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_rio_branco cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_varginha_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_varginha cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_vila_velha_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_vila_velha cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_vitoria_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_vitoria_es cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_teresina_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'PI' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_teresina cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_uberaba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_uberaba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_uberlandia_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_uberlandia cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        $sql_tres_pontas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_tres_pontas_mg cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_guarulhos_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_guarulhos_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_tres_coracoes_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_tres_coracoes_mg cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_votuporanga_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_votuporanga_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_mirassol_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'SP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_mirassol_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_altas_flores_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, '' as motivo, 'MT' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as url, cnf.data_execucao 
        FROM dtb_certidao_altas_flores_mt cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador'])."))";

            $sql = $sql_sp_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_aracaju_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_arapiraca_al_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_belo_horizonte_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_boa_vista_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_campinas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_contagem_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_cuiaba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_curitiba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_flores_da_cunha_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_florianopolis_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_fortaleza_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_franca_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_goiania_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_guarapari_es_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_joao_pessoa_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_palmas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_porto_alegre_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_porto_velho_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_branco_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_varginha_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_vila_velha_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_vitoria_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_teresina_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_uberlandia_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_uberaba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_tres_pontas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_guarulhos_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_tres_coracoes_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_votuporanga_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_mirassol_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_altas_flores_admin.$sql_not_admin." GROUP BY cnpj";

        }else{
            $sql = $sql_sp_admin." GROUP BY cnpj"." UNION ".$sql_aracaju_admin." GROUP BY cnpj"." UNION ".$sql_arapiraca_al_admin." GROUP BY cnpj"." UNION ".$sql_belo_horizonte_admin." GROUP BY cnpj"." UNION ".$sql_boa_vista_admin." GROUP BY cnpj"." UNION ".$sql_campinas_admin." GROUP BY cnpj"." UNION ".$sql_contagem_admin." GROUP BY cnpj"." UNION ".$sql_cuiaba_admin." GROUP BY cnpj"." UNION ".$sql_curitiba_admin." GROUP BY cnpj"." UNION ".$sql_flores_da_cunha_admin." GROUP BY cnpj"." UNION ".$sql_florianopolis_admin." GROUP BY cnpj"." UNION ".$sql_fortaleza_admin." GROUP BY cnpj"." UNION ".$sql_franca_admin." GROUP BY cnpj"." UNION ".$sql_goiania_admin." GROUP BY cnpj"." UNION ".$sql_guarapari_es_admin." GROUP BY cnpj"." UNION ".$sql_joao_pessoa_admin." GROUP BY cnpj"." UNION ".$sql_palmas_admin." GROUP BY cnpj"." UNION ".$sql_porto_alegre_admin." GROUP BY cnpj"." UNION ".$sql_porto_velho_admin." GROUP BY cnpj"." UNION ".$sql_rio_branco_admin." GROUP BY cnpj"." UNION ".$sql_varginha_admin." GROUP BY cnpj"." UNION ".$sql_vila_velha_admin." GROUP BY cnpj"." UNION ".$sql_vitoria_admin." GROUP BY cnpj"." UNION ".$sql_teresina_admin." GROUP BY cnpj"." UNION ".$sql_uberlandia_admin." GROUP BY cnpj"." UNION ".$sql_uberaba_admin." GROUP BY cnpj"." UNION ".$sql_tres_pontas_admin." GROUP BY cnpj"." UNION ".$sql_guarulhos_admin." GROUP BY cnpj"." UNION ".$sql_tres_coracoes_admin." GROUP BY cnpj"." UNION ".$sql_votuporanga_admin." GROUP BY cnpj"." UNION ".$sql_mirassol_admin." GROUP BY cnpj"." UNION ".$sql_altas_flores_admin." GROUP BY cnpj";
        }

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 1 ";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 0 ";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->row();

    }


    public function listar_estadual(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'SP' as uf,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_estadual_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_sergipe_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'SE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_sergipe cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rio_grande_sul_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_riograndedosul cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_ceara_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'CE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_ceara cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rio_de_janeiro_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RJ' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_riodejaneiro cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        $sql_minas_gerais_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_minas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_santa_catarina_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'SC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_santa_catarina cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_mato_grosso_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MT' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_matogrosso cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_alagoas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AL' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_alagoas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_amapa_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_amapa cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_mato_grosso_sul_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_mato_grosso_sul cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_pernambuco_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_pernambuco cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_tocantins_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'TO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_tocantins cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        $sql_amazonas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AM' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_amazonas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        $sql_distrito_federal_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'DF' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_distrito_federal cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_maranhao_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MA' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_maranhao cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";        
        
        $sql_para_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PA' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_para cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";   

        $sql_paraiba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PB' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_paraiba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";  
        
        $sql_parana_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_parana cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_rio_grande_norte_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RN' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_riograndedonorte cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_roraima_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_roraima cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rondonia_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_rondonia cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_goias_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'GO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_goias cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_espirito_santo_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'GO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_espirito_santo cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_espirito_santo_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_espirito_santo cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_acre_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_acre cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador'])."))";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj "." UNION ".$sql_sergipe_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_sul_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_ceara_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_de_janeiro_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_minas_gerais_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_santa_catarina_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_alagoas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_amapa_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_sul_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_pernambuco_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_tocantins_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_amazonas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_distrito_federal_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_maranhao_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_para_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_paraiba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_parana_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_norte_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_roraima_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rondonia_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_goias_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_espirito_santo_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_acre_admin.$sql_not_admin." GROUP BY cnpj";

        }else{
            $sql = $sql_salvador_admin." GROUP BY cnpj "." UNION ".$sql_sergipe_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_sul_admin." GROUP BY cnpj "." UNION ".$sql_ceara_admin." GROUP BY cnpj"." UNION ".$sql_rio_de_janeiro_admin." GROUP BY cnpj"." UNION ".$sql_minas_gerais_admin." GROUP BY cnpj"." UNION ".$sql_santa_catarina_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_admin." GROUP BY cnpj"." UNION ".$sql_amapa_admin." GROUP BY cnpj"." UNION ".$sql_alagoas_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_sul_admin." GROUP BY cnpj"." UNION ".$sql_pernambuco_admin." GROUP BY cnpj"." UNION ".$sql_tocantins_admin." GROUP BY cnpj"." UNION ".$sql_amazonas_admin." GROUP BY cnpj"." UNION ".$sql_distrito_federal_admin." GROUP BY cnpj"." UNION ".$sql_maranhao_admin." GROUP BY cnpj"." UNION ".$sql_para_admin." GROUP BY cnpj"." UNION ".$sql_paraiba_admin." GROUP BY cnpj"." UNION ".$sql_parana_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_norte_admin." GROUP BY cnpj"." UNION ".$sql_roraima_admin." GROUP BY cnpj"." UNION ".$sql_rondonia_admin." GROUP BY cnpj"." UNION ".$sql_goias_admin." GROUP BY cnpj"." UNION ".$sql_espirito_santo_admin." GROUP BY cnpj"." UNION ".$sql_acre_admin." GROUP BY cnpj";
        }

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 1 ";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 0 ";
            }

            if($this->getFiltro_uf() != null && $this->getFiltro_uf() != 'Todas'){
                $sql_aux = $sql_aux." AND consulta.uf = '".$this->getFiltro_uf()."'";
            }
        }else{
            if($this->getFiltro_uf() != null && $this->getFiltro_uf() != 'Todas'){
                $sql_aux = $sql_aux." WHERE consulta.uf = '".$this->getFiltro_uf()."'";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->result();

    }


    public function qtd_estadual_irregular(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'SP' as uf,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_estadual_sp cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_sergipe_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'SE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_sergipe cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rio_grande_sul_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_riograndedosul cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_ceara_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'CE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_ceara cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rio_de_janeiro_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RJ' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_riodejaneiro cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        $sql_minas_gerais_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MG' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_minas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_santa_catarina_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'SC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_santa_catarina cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_mato_grosso_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MT' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_matogrosso cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_alagoas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AL' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_alagoas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_amapa_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AP' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_amapa cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_mato_grosso_sul_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MS' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_mato_grosso_sul cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_pernambuco_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PE' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_pernambuco cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_tocantins_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'TO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_tocantins cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        $sql_amazonas_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AM' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_amazonas cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";
        
        $sql_distrito_federal_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'DF' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_distrito_federal cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_maranhao_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'MA' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_maranhao cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";        
        
        $sql_para_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PA' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_para cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";   

        $sql_paraiba_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PB' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_paraiba cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";  
        
        $sql_parana_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'PR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_parana cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_rio_grande_norte_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RN' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_riograndedonorte cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) "; 

        $sql_roraima_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RR' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_roraima cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_rondonia_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'RO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_rondonia cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_goias_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'GO' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_goias cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_espirito_santo_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'ES' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_espirito_santo cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        $sql_acre_admin = "SELECT cnf.cnpj as cnpj, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status, 'AC' as uf, e.cidade as cidade, '' as inscricao_municipal, caminho_download as caminho_download, cnf.data_execucao  
        FROM dtb_certidao_acre cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador'])."))";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj "." UNION ".$sql_sergipe_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_sul_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_ceara_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_de_janeiro_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_minas_gerais_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_santa_catarina_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_alagoas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_amapa_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_sul_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_pernambuco_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_tocantins_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_amazonas_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_distrito_federal_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_maranhao_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_para_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_paraiba_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_parana_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_norte_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_roraima_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_rondonia_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_goias_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_espirito_santo_admin.$sql_not_admin." GROUP BY cnpj"." UNION ".$sql_acre_admin.$sql_not_admin." GROUP BY cnpj";

        }else{
            $sql = $sql_salvador_admin." GROUP BY cnpj "." UNION ".$sql_sergipe_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_sul_admin." GROUP BY cnpj "." UNION ".$sql_ceara_admin." GROUP BY cnpj"." UNION ".$sql_rio_de_janeiro_admin." GROUP BY cnpj"." UNION ".$sql_minas_gerais_admin." GROUP BY cnpj"." UNION ".$sql_santa_catarina_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_admin." GROUP BY cnpj"." UNION ".$sql_amapa_admin." GROUP BY cnpj"." UNION ".$sql_alagoas_admin." GROUP BY cnpj"." UNION ".$sql_mato_grosso_sul_admin." GROUP BY cnpj"." UNION ".$sql_pernambuco_admin." GROUP BY cnpj"." UNION ".$sql_tocantins_admin." GROUP BY cnpj"." UNION ".$sql_amazonas_admin." GROUP BY cnpj"." UNION ".$sql_distrito_federal_admin." GROUP BY cnpj"." UNION ".$sql_maranhao_admin." GROUP BY cnpj"." UNION ".$sql_para_admin." GROUP BY cnpj"." UNION ".$sql_paraiba_admin." GROUP BY cnpj"." UNION ".$sql_parana_admin." GROUP BY cnpj"." UNION ".$sql_rio_grande_norte_admin." GROUP BY cnpj"." UNION ".$sql_roraima_admin." GROUP BY cnpj"." UNION ".$sql_rondonia_admin." GROUP BY cnpj"." UNION ".$sql_goias_admin." GROUP BY cnpj"." UNION ".$sql_espirito_santo_admin." GROUP BY cnpj"." UNION ".$sql_acre_admin." GROUP BY cnpj";
        }

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 1 ";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 0 ";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->row();

    }

    public function listar_federal(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.cnpj_completo as cnpj_sem_formato, cnf.observacao as observacao, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download, cnf.data_execucao  
        FROM dtb_certidao_receita cnf JOIN dtb_empresas as e on trim(cnf.cnpj_completo) = trim(e.cnpj) ";


        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }else if($this->getFiltro() == "Matriz"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status != 2";
            }else if($this->getFiltro() == "Filial"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 2";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->result();

    }

    public function situacao_fiscal_empresa($cnpj){
        $this->db->select('caminho_download as pdf'); 
        $this->db->where('cnpj_data', $cnpj);

        return $this->db->get('dtb_situacao_fiscal')->row();
    }


    public function qtd_federal_irregular(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download, cnf.data_execucao  
        FROM dtb_certidao_receita cnf JOIN dtb_empresas as e on trim(cnf.cnpj_completo) = trim(e.cnpj) ";

    
        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->row();

    }

    public function listar_caixa(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download, cnf.data_execucao  
        FROM dtb_certidao_caixa cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";


        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }
        //log_message('info', $sql);

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->result();

    }

    public function qtd_caixa_irregular(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download, cnf.data_execucao  
        FROM dtb_certidao_caixa cnf JOIN dtb_empresas as e on trim(cnf.cnpj) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }
        //log_message('info', $sql);

        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->row();

    }

    public function listar_trabalhista(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, cnf.cnpj_completo as cnpj_sem_formato, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download, cnf.data_execucao  
        FROM dtb_certidao_trabalhista cnf JOIN dtb_empresas as e on trim(cnf.cnpj_completo) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }
        
        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->result();

    }

    public function qtd_trabalhista_irregular(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download, cnf.data_execucao  
        FROM dtb_certidao_trabalhista cnf JOIN dtb_empresas as e on trim(cnf.cnpj_completo) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }
        
        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->row();

    }

    public function adicionar_observacao_federal(){

        $dados = array(
            
                'observacao' => $this->getObservacao()
        );
    
        if ($this->db->update('dtb_certidao_receita', $dados, "cnpj_completo={$this->getId()}")){
            return TRUE;
        } else {
            return FALSE;
        }   
    }

    public function adicionar_observacao_caixa(){

        $dados = array(
            
                'observacao' => $this->getObservacao()
        );
    
        if ($this->db->update('dtb_certidao_caixa', $dados, "cnpj={$this->getId()}")){
            return TRUE;
        } else {
            return FALSE;
        }   
    } 

    public function adicionar_observacao_trabalhista(){

        $dados = array(
            
                'observacao' => $this->getObservacao()
        );
    
        if ($this->db->update('dtb_certidao_trabalhista', $dados, "cnpj_completo={$this->getId()}")){
            return TRUE;
        } else {
            return FALSE;
        }   
    }

    public function adicionar_observacao_previdenciaria(){

        $dados = array(
            
                'observacao' => $this->getObservacao()
        );
    
        if ($this->db->update('dtb_certidao_previdenciaria', $dados, "cnpj_completo={$this->getId()}")){
            return TRUE;
        } else {
            return FALSE;
        }   
    }

    public function listar_previdenciaria(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, cnf.cnpj_completo as cnpj_sem_formato, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download  
        FROM dtb_certidao_previdenciaria cnf JOIN dtb_empresas as e on trim(cnf.cnpj_completo) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";

            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }
        
        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT * FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->result();

    }

    public function qtd_previdenciaria_irregular(){

        $sql_salvador_admin = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download  
        FROM dtb_certidao_previdenciaria cnf JOIN dtb_empresas as e on trim(cnf.cnpj_completo) = trim(e.cnpj) ";

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql_not_admin = " WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) AND e.situacao_cadastral != 'BAIXADO' ";
            
            $sql = $sql_salvador_admin.$sql_not_admin." GROUP BY cnpj ";

        }else{
            $sql = $sql_salvador_admin." WHERE e.situacao_cadastral != 'BAIXADO' GROUP BY cnpj ";
        }
        
        $sql_aux = "SELECT * FROM (".$sql.") as consulta ";

        if($this->getFiltro() != null && $this->getFiltro() != 'Todas'){
            if($this->getFiltro() == "Regulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 1";
            }else if($this->getFiltro() == "Irregulares"){
                $sql_aux = "SELECT COUNT(*) as qtd FROM (".$sql.") as consulta WHERE consulta.status = 0";
            }
        }
        //log_message('info', $sql);
        return $this->db->query($sql_aux)->row();

    } 

    public function buscar_ultima_caixa($cnpj){
        $sql = "SELECT cnf.cnpj as cnpj, cnf.observacao as observacao, e.cnpj_completo, e.inscricao_estadual_completo, cnf.inscricao_estadual as inscricao_estadual, cnf.razao_social as razao_social, cnf.status as status,  e.cidade as cidade, '' as inscricao_municipal, cnf.caminho_download FROM dtb_certidao_caixa cnf JOIN dtb_empresas e ON trim(cnf.cnpj) = trim(e.cnpj) WHERE cnf.cnpj = ".$cnpj." ORDER BY cnf.id DESC LIMIT 1";
        return $this->db->query($sql)->row();
    }
}