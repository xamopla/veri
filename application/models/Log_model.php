<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends CI_Model {

	private $id;

	private $id_usuario;
	private $data_hora_acao;
	private $acao_executada;
	private $ip_acesso;

	//CONSTRUTOR
	public function __construct(){
		parent::__construct();
	}

	//GETTERS E SETTERS
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getIdUsuario() {
		return $this->id_usuario;
	}
	public function setIdUsuario($id_usuario) {
		$this->id_usuario = $id_usuario;
		return $this;
	}

	public function getDataHoraAcao() {
		return $this->data_hora_acao;
	}
	public function setDataHoraAcao($data_hora_acao) {
		$this->data_hora_acao = $data_hora_acao;
		return $this;
	}

	public function getAcaoExecutada() {
		return $this->acao_executada;
	}
	public function setAcaoExecutada($acao_executada) {
		$this->acao_executada = $acao_executada;
		return $this;
	}

	public function getIpAcesso() {
		return $this->ip_acesso;
	}
	public function setIpAcesso($ip_acesso) {
		$this->ip_acesso = $ip_acesso;
		return $this;
	}

	// AÇÃO CADASTRAR
	public function acao_cadastar(){

		$dados = array(
	
				'id_usuario' => $this->getIdUsuario(),
				'data_hora_acao' => $this->getDataHoraAcao(),
				'acao_executada' => $this->getAcaoExecutada(),
				'ip_acesso' => $this->getIpAcesso()

		);
	
		if ($this->db->insert('controle', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	//AÇÃO EDITAR
	public function acao_editar(){

		$dados = array(
	
				'id_usuario' => $this->getIdUsuario(),
				'data_hora_acao' => $this->getDataHoraAcao(),
				'acao_executada' => $this->getAcaoExecutada(),
				'ip_acesso' => $this->getIpAcesso()

		);
	
		if ($this->db->insert('controle', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	//AÇÃO EXCLUIR
	public function acao_excluir(){

		$dados = array(
	
				'id_usuario' => $this->getIdUsuario(),
				'data_hora_acao' => $this->getDataHoraAcao(),
				'acao_executada' => $this->getAcaoExecutada(),
				'ip_acesso' => $this->getIpAcesso()

		);
	
		if ($this->db->insert('controle', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	//AÇÃO LISTAR
	public function acao_listar(){
		$dados = array(
	
				'id_usuario' => $this->getIdUsuario(),
				'data_hora_acao' => $this->getDataHoraAcao(),
				'acao_executada' => $this->getAcaoExecutada(),
				'ip_acesso' => $this->getIpAcesso()

		);
	
		if ($this->db->insert('controle', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
}