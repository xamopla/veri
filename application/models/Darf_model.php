<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Darf_model extends CI_Model {

	private $filtro_situacao;

	private $filtro_mes;
	private $filtro_ano;

	function __construct()
	{
		parent::__construct();
	}


	function setFiltroMes($filtro_mes) { 
		$this->filtro_mes = $filtro_mes; 
	}

	function getFiltroMes() { 
		return $this->filtro_mes; 
	}

	function setFiltroAno($filtro_ano) { 
		$this->filtro_ano = $filtro_ano; 
	}

	function getFiltroAno() { 
		return $this->filtro_ano; 
	}

	public function getFiltro_situacao() {
	    return $this->filtro_situacao;
	}
	 
	public function setFiltro_situacao($filtro_situacao) {
	    $this->filtro_situacao = $filtro_situacao;
	}


	public function listar(){
		date_default_timezone_set('America/Sao_Paulo');
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			
			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.numero_documento, d.periodo_apuracao, d.data_arrecadacao, d.data_vencimento, d.codigo_receita, d.valor_total, d.caminho_download       
				FROM dtb_ecac_darf as d  
				JOIN dtb_empresas as e ON trim(d.cnpj) = trim(e.cnpj)
				WHERE LAST_DAY(NOW()) >= DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(NOW()) <= DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') 
				";

			$sql = $sql1;
		}else{

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.numero_documento, d.periodo_apuracao, d.data_arrecadacao, d.data_vencimento, d.codigo_receita, d.valor_total, d.caminho_download       
				FROM dtb_ecac_darf as d  
				JOIN dtb_empresas as e ON trim(d.cnpj) = trim(e.cnpj) 
				WHERE LAST_DAY(NOW()) >= DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(NOW()) <= DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
				";

			$sql = $sql1;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado ';
		if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// if($this->getFiltro_situacao() == 'REGULAR'){
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.pago = "Sim" AND resultado.cnpj like "%0001%" ';
			// }else if($this->getFiltro_situacao() == 'PENDENTE'){
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.pago = "Não" AND resultado.cnpj like "%0001%" ';
			// }else{
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.cnpj in (SELECT cnpj FROM dtb_ecac_das_debitos) ';
			// }
		}
		return $this->db->query($sql_final)->result();
	} 


	public function listarFiltro(){
		$sql = "";
		$filtro_ano = $this->getFiltroAno();
		$filtro_mes = $this->getFiltroMes();

		if($filtro_mes < 10){
			$filtro_mes = "0".$filtro_mes;
		}

		$data_inicial = "'".$filtro_ano.'-'.$filtro_mes.'-01'."'";
		$data_final = "'".$filtro_ano.'-'.$filtro_mes.'-31'."'";

		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		if($this->session->userdata['userprimesession']['nivel'] !=2){
			
			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.numero_documento, d.periodo_apuracao, d.data_arrecadacao, d.data_vencimento, d.codigo_receita, d.valor_total, d.caminho_download       
				FROM dtb_ecac_darf as d  
				JOIN dtb_empresas as e ON trim(d.cnpj) = trim(e.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final."";

			$sql = $sql1;

		}else{
			
			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.numero_documento, d.periodo_apuracao, d.data_arrecadacao, d.data_vencimento, d.codigo_receita, d.valor_total, d.caminho_download       
				FROM dtb_ecac_darf as d  
				JOIN dtb_empresas as e ON trim(d.cnpj) = trim(e.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) ";

			$sql = $sql1;

		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado ';
		if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// if($this->getFiltro_situacao() == 'REGULAR'){
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.pago = "Sim" AND resultado.cnpj like "%0001%" ';
			// }else if($this->getFiltro_situacao() == 'PENDENTE'){
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.pago = "Não" AND resultado.cnpj like "%0001%" ';
			// }else{
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.cnpj in (SELECT cnpj FROM dtb_ecac_das_debitos) ';
			// }
		}
		return $this->db->query($sql_final)->result();
	}

	public function listarFiltroByBotaoMes(){
		$sql = "";
		$filtro_ano = $this->getFiltroAno();
		$filtro_mes = $this->getFiltroMes();

		$data_inicial = "'".$filtro_ano.'-'.$filtro_mes.'-01'."'";
		$data_final = "'".$filtro_ano.'-'.$filtro_mes.'-31'."'";

		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		if($this->session->userdata['userprimesession']['nivel'] !=2){
			
			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.numero_documento, d.periodo_apuracao, d.data_arrecadacao, d.data_vencimento, d.codigo_receita, d.valor_total, d.caminho_download       
				FROM dtb_ecac_darf as d  
				JOIN dtb_empresas as e ON trim(d.cnpj) = trim(e.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final."";

			$sql = $sql1;

		}else{
			
			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.numero_documento, d.periodo_apuracao, d.data_arrecadacao, d.data_vencimento, d.codigo_receita, d.valor_total, d.caminho_download       
				FROM dtb_ecac_darf as d  
				JOIN dtb_empresas as e ON trim(d.cnpj) = trim(e.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(d.periodo_apuracao,'%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) ";

			$sql = $sql1;
			
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado ';
		if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			// if($this->getFiltro_situacao() == 'REGULAR'){
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.pago = "Sim" AND resultado.cnpj like "%0001%" ';
			// }else if($this->getFiltro_situacao() == 'PENDENTE'){
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.pago = "Não" AND resultado.cnpj like "%0001%" ';
			// }else{
			// 	$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.cnpj in (SELECT cnpj FROM dtb_ecac_das_debitos) ';
			// }
		}
		return $this->db->query($sql_final)->result();
	}
	

	public function qtd_dctf_pendente(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato, d.caminho_download_das          
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql = $sql1;
		}else{

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato, d.caminho_download_das          
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql = $sql1;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ';
		if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			if($this->getFiltro_situacao() == 'REGULAR'){
				$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.pago = "Sim" AND resultado.cnpj like "%0001%" ';
			}else if($this->getFiltro_situacao() == 'IRREGULAR'){
				$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.pago = "Não" AND resultado.cnpj like "%0001%" ';
			}else{
				$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.cnpj in (SELECT cnpj from dtb_ecac_das_debitos) ';
			}
		}
		return $this->db->query($sql_final)->row();
	} 


	////
	public function qtd_dctf_pendente_proximo_vencer(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			
			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato, d.caminho_download_das          
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql = $sql1;
		}else{

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato, d.caminho_download_das          
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_ADD(NOW(), INTERVAL -1 MONTH) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND DATE_ADD(NOW(), INTERVAL -1 MONTH) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql = $sql1;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" ';
		if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			if($this->getFiltro_situacao() == 'REGULAR'){
				$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.pago = "Sim" AND resultado.cnpj like "%0001%" ';
			}else if($this->getFiltro_situacao() == 'REGULAR'){
				$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.pago = "Não" AND resultado.cnpj like "%0001%" ';
			}else{
				$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.cnpj in (SELECT cnpj from dtb_ecac_das_debitos) ';
			}
		}
		return $this->db->query($sql_final)->row();
	} 


	public function update_simples($id){
		$this->db->set('tipo_regime', 'SIMPLES NACIONAL');
		$this->db->where('id', $id);
		$this->db->update('dtb_empresas');
	}

	public function find_all_debitos(){
		$this->db->select('*');

		return $this->db->get('dtb_ecac_das_debitos')->result();
	}

	public function find_debitos_for_modal($cnpj){
		$this->db->select('*');
		$this->db->where("cnpj",$cnpj);
		return $this->db->get('dtb_ecac_das_debitos')->result();
	}

    public function update_caminho_download_das($caminho_download , $numero_declaracao){
        $this->db->set('caminho_download_das', $caminho_download);
        $this->db->where('numero_declaracao', $numero_declaracao);
        $this->db->update('dtb_ecac_das');
    }


    //Funções auxiliares na busca por certificados por procuração ou individual
    public function find_certificado($cnpj){
        $this->db->select('*');
        $this->db->where('cnpj_data', $cnpj);
        return $this->db->get('dtb_certificado')->row();
    }

    public function get_aux($id)
    {       
        $this->db->select('*');
        $this->db->join('dtb_contador_procuracao d','db.id_contador = d.id_contador', 'left');
        $this->db->where('d.id_empresa', $id);
        return $this->db->get('dtb_certificado_contador db')->result();
    }

    public function find_empresa_by_cnpj($cnpj){
        $this->db->select('id');
        $this->db->where("cnpj", $cnpj);

        return $this->db->get('dtb_empresas')->row();
    }

    //Fim das funçoes auxiliares
}