<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Caixadeemail_model extends CI_Model {

	private $id;
	private $cnpj;
	private $razao_social;
	private $id_empresa;
	private $titulo;
	private $mensagem;
	private $status;
	private $remetente;
	private $dataemail;
	private $importante;
	private $id_contabilidade;
	
	private $filtro;
	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getCnpj() {
		return $this->cnpj;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}

	public function getRazaoSocial() {
		return $this->razao_social;
	}
	public function setRazaoSocial($razao_social) {
		$this->razao_social = $razao_social;
		return $this;
	}

	public function getIdEmpresa() {
		return $this->id_empresa;
	}
	public function setIdEmpresa($id_empresa) {
		$this->id_empresa = $id_empresa;
		return $this;
	}

	public function getTitulo() {
		return $this->titulo;
	}
	public function setTitulo($titulo) {
		$this->titulo = $titulo;
		return $this;
	}

	public function getMensagem() {
		return $this->mensagem;
	}
	public function setMensagem($mensagem) {
		$this->mensagem = $mensagem;
		return $this;
	}

	public function getStatus() {
		return $this->status;
	}
	public function setStatus($status) {
		$this->status = $status;
		return $this;
	}

	public function getRemetente() {
		return $this->remetente;
	}
	public function setRemetente($remetente) {
		$this->remetente = $remetente;
		return $this;
	}

	public function getDataEmail() {
		return $this->dataemail;
	}
	public function setDataEmail($dataemail) {
		$this->dataemail = $dataemail;
		return $this;
	}

	public function getImportante() {
		return $this->importante;
	}
	public function setImportante($importante) {
		$this->importante = $importante;
		return $this;
	}

	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}

	public function getFiltro() {
	    return $this->filtro;
	}
	 
	public function setFiltro($filtro) {
	    $this->filtro = $filtro;
	}

	public function listar() {
		$this->db->select('dtb_caixadeemail.id as id, dtb_caixadeemail.cnpj as cnpj, dtb_caixadeemail.razao_social as razao_social, dtb_caixadeemail.id_empresa as id_empresa, dtb_caixadeemail.titulo as titulo, dtb_caixadeemail.mensagem as mensagem, dtb_caixadeemail.remetente as remetente, dtb_caixadeemail.dataemail as dataemail, dtb_caixadeemail.importante as importante, dtb_caixadeemail.status as status, dtb_caixadeemail.id_contabilidade as id_contabilidade, dtbe.senha_sefaz, dtbe.login_sefaz');

		$this->db->join('dtb_empresas dtbe','dtb_caixadeemail.id_empresa = dtbe.id', 'left');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->order_by("dtb_caixadeemail.id", "desc");
		$this->db->group_by('dtb_caixadeemail.id');
		return $this->db->get('dtb_caixadeemail')->result();
	}

	public function excluir(){
		return $this->db->delete('dtb_caixadeemail', "id = {$this->getId()}");
	}

	public function excluir_todos_lidos(){

		$this->db->delete('dtb_caixadeemail', "status = 0");
	}

	public function pesquisar_email_id() {
		$this->db->select('id, titulo, mensagem');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_caixadeemail')->row();
	}

	public function qtd_novos_emails() {
		$this->db->select('COUNT(distinct(dtb_caixadeemail.id)) AS valor');
		
		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->join('dtb_empresas dtbe','dtb_caixadeemail.id_empresa = dtbe.id');
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("dtb_caixadeemail.status", 1);
		return $this->db->get('dtb_caixadeemail')->row();
	}

	public function visualizar_email(){
		$this->db->select('*');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_mensagens_dte')->row();
	}

	public function marcaremailscomolido(){
		
		$this->db->set('status', 0);
		$this->db->where('id', $this->getId());
		$this->db->where('status', 1);
		$this->db->update('dtb_caixadeemail');
		
	}

	public function marcaremailscomonaolido(){
		
		$this->db->set('status', 1);
		$this->db->where('id', $this->getId());
		$this->db->where('status', 0);
		$this->db->update('dtb_caixadeemail');
		
	}

	public function redirecionarparaempresa() {
		$this->db->select('vinculo_contador, id, inscricao_estadual_completo, cnpj_completo, razao_social, situacao_cadastral, situacao, motivo, sync, id_contador, condicao, forma_pagamento, motivo_situacao_cadastral, situacao_dte, situacao_conta_dte, senha_sefaz, login_sefaz');
		$this->db->where("id", $this->getIdEmpresa());
		return $this->db->get('dtb_empresas')->result();
	}

	public function find_empresa_email($cnpj){
		$this->db->select('id, razao_social');
		$this->db->where("cnpj_completo", $cnpj);
		$this->db->group_by('id');
		return $this->db->get('dtb_empresas')->row();

	}


	public function inserir_msg_nova($cnpj, $id_empresa, $razao_social, $titulo, $mensagem, $status, $id_contabilidade, $remetente, $dataemail){

		$dados = array(

				'cnpj' => $cnpj,
				'id_empresa' => $id_empresa,
				'razao_social' => $razao_social,
				'titulo' => $titulo,
				'mensagem' => $mensagem,
				'status' => $status,
				'id_contabilidade' => $id_contabilidade,
				'remetente' => $remetente,
				'dataemail' => $dataemail
		);

		if ($this->db->insert('dtb_caixadeemail', $dados)){
			return;
		}

	}


	public function listar_nova() {
		$this->db->select('dtb_caixadeemail.id as id, dtb_caixadeemail.cnpj as cnpj, dtb_caixadeemail.razao_social as razao_social, dtb_caixadeemail.id_empresa as id_empresa, dtb_caixadeemail.titulo as titulo, dtb_caixadeemail.mensagem as mensagem, dtb_caixadeemail.remetente as remetente, dtb_caixadeemail.dataemail as dataemail, dtb_caixadeemail.importante as importante, dtb_caixadeemail.status as status, dtb_caixadeemail.id_contabilidade as id_contabilidade, dtbe.senha_sefaz, dtbe.login_sefaz, dtbe.inscricao_estadual_completo, dtbe.situacao_cadastral, dtbe.situacao, dtbe.motivo, dtbe.condicao, dtbe.forma_pagamento, dtbe.motivo_situacao_cadastral, dtbe.id as id_empresa , dtbe.situacao_dte, dtbe.situacao_conta_dte');

		$this->db->join('dtb_empresas dtbe','dtb_caixadeemail.id_empresa = dtbe.id', 'left');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
			if($this->getFiltro() == 'LIDAS'){
				$this->db->where("status", '0');
			}else{
				$this->db->where("status", '1');
			}
		}
		
		$this->db->order_by("dtb_caixadeemail.id", "desc");
		$this->db->group_by('dtb_caixadeemail.id');
		return $this->db->get('dtb_caixadeemail')->result();
	}


	public function listar_nova_aux() {
		$this->db->select("dtb_mensagens_dte.id, dtb_mensagens_dte.cnpj, dtb_mensagens_dte.razao_social, dtb_mensagens_dte.id_empresa, dtb_mensagens_dte.status, dtb_mensagens_dte.situacao_da_ciencia, dtb_mensagens_dte.codigo_tipo, dtb_mensagens_dte.destinatario, dtb_mensagens_dte.remetente, dtb_mensagens_dte.data_de_emissao, dtb_mensagens_dte.assunto, dtb_mensagens_dte.data_de_leitura, dtb_mensagens_dte.data_de_ciencia, dtb_mensagens_dte.data_de_vencimento, dtb_mensagens_dte.conteudo_mensagem, DATE_FORMAT(STR_TO_DATE(data_de_emissao, '%d/%m/%Y'), '%Y-%m-%d')  as data_teste");

		$this->db->join('dtb_empresas dtbe','dtb_mensagens_dte.id_empresa = dtbe.id', 'left');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
			if($this->getFiltro() == 'LIDAS'){
				$this->db->where("status not like '%não Lida%' ");
			}
			else if($this->getFiltro() == 'NAO_LIDAS'){
				$this->db->where("status like '%não Lida%' ");
			}
			else if($this->getFiltro() == 'INTIMACAO'){
				$this->db->where("codigo_tipo like '%54 - INTIMAÇÃO%' ");
			}
			else if($this->getFiltro() == 'AVISO'){
				$this->db->where("codigo_tipo like '%1 - AVISO%' ");
			}
			else if($this->getFiltro() == 'COMUNICADO'){
				$this->db->where("codigo_tipo like '%53 - COMUNICAÇÃO%' ");
			}
			else if($this->getFiltro() == 'CIENTIFICACAO'){
				$this->db->where("codigo_tipo like '%52 - CIENTIFICAÇÃO%' ");
			}
		}
		
		$this->db->order_by("str_to_date(dtb_mensagens_dte.data_de_emissao,'%d/%m/%Y') desc");
		$this->db->group_by('dtb_mensagens_dte.id');
		return $this->db->get('dtb_mensagens_dte')->result();
	}

	public function qtd_msgs_dte_nova_listar() {
		$this->db->select('dtb_caixadeemail.id as id, dtb_caixadeemail.cnpj as cnpj, dtb_caixadeemail.razao_social as razao_social, dtb_caixadeemail.id_empresa as id_empresa, dtb_caixadeemail.titulo as titulo, dtb_caixadeemail.mensagem as mensagem, dtb_caixadeemail.remetente as remetente, dtb_caixadeemail.dataemail as dataemail, dtb_caixadeemail.importante as importante, dtb_caixadeemail.status as status, dtb_caixadeemail.id_contabilidade as id_contabilidade, dtbe.senha_sefaz, dtbe.login_sefaz, dtbe.inscricao_estadual_completo, dtbe.situacao_cadastral, dtbe.situacao, dtbe.motivo, dtbe.condicao, dtbe.forma_pagamento, dtbe.motivo_situacao_cadastral, dtbe.id as id_empresa , dtbe.situacao_dte, dtbe.situacao_conta_dte');
		
		$this->db->join('dtb_empresas dtbe','dtb_caixadeemail.id_empresa = dtbe.id', 'left');
		
		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("dtb_caixadeemail.status", 1);
		$this->db->order_by("dtb_caixadeemail.id", "desc");
		$this->db->group_by('dtb_caixadeemail.id');
		return $this->db->get('dtb_caixadeemail')->result();
	}

	public function qtd_msgs_dte_nova() {
		$this->db->select('COUNT(distinct(dtb_caixadeemail.id)) AS valor');
		
		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->join('dtb_empresas dtbe','dtb_caixadeemail.id_empresa = dtbe.id');
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("dtb_caixadeemail.status", 1);
		return $this->db->get('dtb_caixadeemail')->row();
	}

	public function marcar_msg_ecac_lida(){
		$this->db->set('nao_lidas', 0);
		$this->db->where('id', $this->getId());
		$this->db->update('dtb_ecac_caixa_postal');
	}

	public function buscar_conteudo($id){
		$this->db->select("*");
		$this->db->where('id', $id);
		return $this->db->get('dtb_mensagens_dte')->row();
	}
	
}