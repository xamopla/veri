<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gfip_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	public function listar() {	
		$this->db->select('db.id, db.possui_pendencia, dtbe.cnpj, dtbe.razao_social');
		$this->db->from('dtb_pendencia_gfip db');
		$this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
            if($this->getFiltro() == "IRREGULAR"){
            	$this->db->where("db.possui_pendencia = 1");
            }else if($this->getFiltro() == "REGULAR"){
            	$this->db->where("db.possui_pendencia = 0");
            }
        }

		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}


	public function qtd() {	
		$this->db->select('COUNT(*) as qtd');
		$this->db->from('dtb_pendencia_gfip db');
		$this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
            if($this->getFiltro() == "IRREGULAR"){
            	$this->db->where("db.possui_pendencia = 1");
            }else if($this->getFiltro() == "REGULAR"){
            	$this->db->where("db.possui_pendencia = 0");
            }
        }

		return $this->db->get()->row();
	}

	public function find_info_for_modal($cnpj){
		$this->db->select('*');
		$this->db->from('dtb_pendencia_gfip_detalhe');
		$this->db->where("cnpj", $cnpj);
		return $this->db->get()->result();
	}

	

}