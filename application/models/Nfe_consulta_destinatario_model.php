<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nfe_consulta_destinatario_model extends CI_Model {	

	public function main($dados_post){
		$dados_request_sefaz = $this->logar($dados_post);
		$dados_request_sefaz = $this->consultar_emitente($dados_post, $dados_request_sefaz);
		return $this->return_array($this->get_dados_planilha($dados_post, $dados_request_sefaz), $dados_post, $dados_request_sefaz);
	}

	public function retorna_nfe($dados_post){
		$dados_request_sefaz = $this->logar($dados_post);
		$html_nota = $this->consulta_link_nfe_chave($dados_post["chave_nfe"], $dados_post, $dados_request_sefaz);
		$html_nota = str_replace("../../", "../../assets/nfe_view/", $html_nota);
		$html_nota = str_replace('id="btnImprimirAutorizacaoUso"', 'id="btnImprimirAutorizacaoUso" style="visibility: hidden;"', $html_nota);
		$html_nota = str_replace('id="btnNFEResumo"', 'id="btnNFEResumo" style="visibility: hidden;"', $html_nota);
		$html_nota = str_replace('<div id="containerSis">', '<!--<div id="containerSis">', $html_nota);
		$html_nota = str_replace('<form', '--><div style="width: 100%;text-align: center;background: white;"><img src="../assets/img/logos/logo.png" style="vertical-align: middle;box-sizing: inherit;display: inline-block;margin-top: 10px;margin-bottom: 10px;width: 150px;"></div><form', $html_nota);
		$html_nota = str_replace("Consulta NF-e por Chave de Acesso - Resumido", "VERI - Consulta NF-e", $html_nota);
		echo $html_nota;
		exit();
	}
    public function retorna_nfe2($dados_post){
        $chave = $this->mask($dados_post['chave_nfe'], '##-####-##.###.###/####-##-##-###-###.###.###-###.###.###-#');
        $html_nota = shell_exec("python3.9 /var/www/html/application/libraries/scripts_python/get_nfe.py \"{$dados_post['login']}\" \"{$dados_post['senha']}\" \"{$chave}\"");
        $html_nota = str_replace("../../", "../../assets/nfe_view/", $html_nota);
        $html_nota = str_replace('id="btnImprimirAutorizacaoUso"', 'id="btnImprimirAutorizacaoUso" style="visibility: hidden;"', $html_nota);
        $html_nota = str_replace('id="btnNFEResumo"', 'id="btnNFEResumo" style="visibility: hidden;"', $html_nota);
        $html_nota = str_replace('<div id="containerSis">', '<!--<div id="containerSis">', $html_nota);
        $html_nota = str_replace('<form', '--><div style="width: 100%;text-align: center;background: white;"><img src="../assets/img/logos/logo.png" style="vertical-align: middle;box-sizing: inherit;display: inline-block;margin-top: 10px;margin-bottom: 10px;width: 150px;"></div><form', $html_nota);
        $html_nota = str_replace("Consulta NF-e por Chave de Acesso - Resumido", "VERI - Consulta NF-e", $html_nota);
        echo $html_nota;
        exit();
    }
	public function logar($dados_login){
		
		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=>'/wEPDwUKLTc5NjIwMjMwNg9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFBU5GRU5DZAIFDxYCHwAFNE5vdGEgRmlzY2FsIEVsZXRyJiMyNDQ7bmljYSAtIEF1dGVudGljYSYjMjMxOyYjMjI3O29kAgkPZBYCZg9kFgICAQ9kFgYCAw8PFggeCENzc0NsYXNzBRggbW9kYWwgZmFkZSAgbW9kYWwgZmFkZSAeMF9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVHPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoEVycm8eMl9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4EXyFTQgICFgoeCHRhYmluZGV4BQItMR4Ecm9sZQUGZGlhbG9nHgthcmlhLWhpZGRlbgUEdHJ1ZR4NZGF0YS1rZXlib2FyZAUFZmFsc2UeDWRhdGEtYmFja2Ryb3AFBnN0YXRpY2QCBw9kFgoCAQ8WAh4IZGlzYWJsZWRkZAIDDxYCHwpkZAIHDxYCHwBlZAIJDw8WAh4HVmlzaWJsZWdkZAILDw8WAh8LZ2RkAgkPFgIfAAWzBDxoMT5JbnN0cnXDp8O1ZXM6PC9oMT4NCjxiciAvPg0KICAgIA0KPHVsPg0KICAgIDxsaT4NCiAgICAgICAgU2Vyw6EgcGVybWl0aWRvIGFwZW5hcyBvIGFjZXNzbyBjb20gbG9naW4gZSBzZW5oYTsNCiAgICA8L2xpPg0KICAgIDxsaT4NCiAgICAgICAgQXDDs3MgMDMgdGVudGF0aXZhcyBpbnbDoWxpZGFzIGEgcMOhZ2luYSBkZSBMb2dpbiBpcsOhIGJsb3F1ZWFyIG8gYWNlc3NvIGRvIElQIGRlIG9yaWdlbSBwb3IgMTAgbWludXRvczsNCiAgICA8L2xpPg0KICAgIDxsaT4NCiAgICAgICAgQ2FzbyBuw6NvIGxlbWJyZSBhIHNlbmhhLCBjbGlxdWUgZW0gImVzcXVlY2V1IGEgc2VuaGE/IiBlIGFww7NzIGluZm9ybWHDp8OjbyBkb3MgZGFkb3MgbmVjZXNzw6FyaW9zLCBvIGxlbWJyZXRlIHNlcsOhIGVudmlhZG8gcGFyYSBvIGVtYWlsIGNhZGFzdHJhZG8uDQogICAgPC9saT4NCjwvdWw+DQoNCjxiciAvPg0KPGg1PkVtIGNhc28gZGUgZMO6dmlkYXMgZW50cmUgZW0gY29udGF0byBhdHJhdsOpcyBkbyBmYWxlY29ub3Njb0BzZWZhei5iYS5nb3YuYnIgb3UgMDgwMCAwNzEwMDcxPC9oNT4NCg0KZAINDw8WCB4pX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHidfX0FTTW9kYWxfRXJyb19WaWV3U3RhdGVBU01vZGFsVGl0dWxvSUQFQzxpIGNsYXNzPSdpY29uLWZpeGVkLXdpZHRoIGljb24td2FybmluZy1zaWduIHRleHQtd2FybmluZyc+PC9pPsKgwqAfAQUYIG1vZGFsIGZhZGUgIG1vZGFsIGZhZGUgHwQCAhYKHwUFAi0xHwYFBmRpYWxvZx8HBQR0cnVlHwgFBWZhbHNlHwkFBnN0YXRpY2QCBQ8WAh8ABUhTSVNURU1BIFRSSUJVVCYjMTkzO1JJTyBTRUZBWiAtIFNlY3JldGFyaWEgZGEgRmF6ZW5kYSBkbyBFc3RhZG8gZGEgQmFoaWFkAgcPFgIfAAUOQVNMSUI6IDIuMi4wLjBkZNM6pS58YCjaL19B9QqCERurKOlt',
			'__VIEWSTATEGENERATOR'=>'4D2610BE',
			'__PREVIOUSPAGE'=>'nQpgC3IwW14xm_p9A86Fs1BD2uvvhdVtsNOHtQU-zmLdq1rLBV1QvM0tJTM4A9aoJ1QjtfqVP3XAQStQYGITS4fKvhSrNHXsWrGAU8UpyV6P95SWa98tF0T68ws2wKGonm-XStHtNGNupV56wR6ENjbUun01',
			'__EVENTVALIDATION'=>'/wEdAArzzd+UPA9s2BEZOVMkymAEIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV7G67MTrWGI73wI34LNPZ59KIHSM=',
			'__ASYNCPOST'=>'false',
			'ctl00$PHCentro$userLogin'=>$dados_login['login'],
			'ctl00$PHCentro$userPass'=>$dados_login['senha'],
			'ctl00$PHCentro$btnLogin'=>'Entrar'
		);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_destinatario.aspx";

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEJAR, $dados_login['cookie_file']); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);

		#VALIDANDO SE O SERVIDOR DA SEFAZ ESTÁ OFF
		if(preg_match('/Erro do Sistema/', $store)){
			#SE ESTIVER OFF, FAZ LOGIN NO SERVIDOR 2
			$data = array(
				'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
				'__EVENTTARGET'=>'',
				'__EVENTARGUMENT'=>'',
				'__VIEWSTATE'=>'/wEPDwUKLTc5NjIwMjMwNg9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFBU5GRU5DZAIFDxYCHwAFNE5vdGEgRmlzY2FsIEVsZXRyJiMyNDQ7bmljYSAtIEF1dGVudGljYSYjMjMxOyYjMjI3O29kAgkPZBYCZg9kFgICAQ9kFgYCAw8PFggeCENzc0NsYXNzBRggbW9kYWwgZmFkZSAgbW9kYWwgZmFkZSAeMF9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVHPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoEVycm8eMl9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4EXyFTQgICFgoeCHRhYmluZGV4BQItMR4Ecm9sZQUGZGlhbG9nHgthcmlhLWhpZGRlbgUEdHJ1ZR4NZGF0YS1rZXlib2FyZAUFZmFsc2UeDWRhdGEtYmFja2Ryb3AFBnN0YXRpY2QCBw9kFgoCAQ8WAh4IZGlzYWJsZWRkZAIDDxYCHwpkZAIHDxYCHwBlZAIJDw8WAh4HVmlzaWJsZWdkZAILDw8WAh8LZ2RkAgkPFgIfAAWzBDxoMT5JbnN0cnXDp8O1ZXM6PC9oMT4NCjxiciAvPg0KICAgIA0KPHVsPg0KICAgIDxsaT4NCiAgICAgICAgU2Vyw6EgcGVybWl0aWRvIGFwZW5hcyBvIGFjZXNzbyBjb20gbG9naW4gZSBzZW5oYTsNCiAgICA8L2xpPg0KICAgIDxsaT4NCiAgICAgICAgQXDDs3MgMDMgdGVudGF0aXZhcyBpbnbDoWxpZGFzIGEgcMOhZ2luYSBkZSBMb2dpbiBpcsOhIGJsb3F1ZWFyIG8gYWNlc3NvIGRvIElQIGRlIG9yaWdlbSBwb3IgMTAgbWludXRvczsNCiAgICA8L2xpPg0KICAgIDxsaT4NCiAgICAgICAgQ2FzbyBuw6NvIGxlbWJyZSBhIHNlbmhhLCBjbGlxdWUgZW0gImVzcXVlY2V1IGEgc2VuaGE/IiBlIGFww7NzIGluZm9ybWHDp8OjbyBkb3MgZGFkb3MgbmVjZXNzw6FyaW9zLCBvIGxlbWJyZXRlIHNlcsOhIGVudmlhZG8gcGFyYSBvIGVtYWlsIGNhZGFzdHJhZG8uDQogICAgPC9saT4NCjwvdWw+DQoNCjxiciAvPg0KPGg1PkVtIGNhc28gZGUgZMO6dmlkYXMgZW50cmUgZW0gY29udGF0byBhdHJhdsOpcyBkbyBmYWxlY29ub3Njb0BzZWZhei5iYS5nb3YuYnIgb3UgMDgwMCAwNzEwMDcxPC9oNT4NCg0KZAINDw8WCB4pX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHidfX0FTTW9kYWxfRXJyb19WaWV3U3RhdGVBU01vZGFsVGl0dWxvSUQFQzxpIGNsYXNzPSdpY29uLWZpeGVkLXdpZHRoIGljb24td2FybmluZy1zaWduIHRleHQtd2FybmluZyc+PC9pPsKgwqAfAQUYIG1vZGFsIGZhZGUgIG1vZGFsIGZhZGUgHwQCAhYKHwUFAi0xHwYFBmRpYWxvZx8HBQR0cnVlHwgFBWZhbHNlHwkFBnN0YXRpY2QCBQ8WAh8ABUhTSVNURU1BIFRSSUJVVCYjMTkzO1JJTyBTRUZBWiAtIFNlY3JldGFyaWEgZGEgRmF6ZW5kYSBkbyBFc3RhZG8gZGEgQmFoaWFkAgcPFgIfAAUOQVNMSUI6IDIuMi4wLjBkZNM6pS58YCjaL19B9QqCERurKOlt',
				'__VIEWSTATEGENERATOR'=>'4D2610BE',
				'__PREVIOUSPAGE'=>'nQpgC3IwW14xm_p9A86Fs1BD2uvvhdVtsNOHtQU-zmLdq1rLBV1QvM0tJTM4A9aoJ1QjtfqVP3XAQStQYGITS4fKvhSrNHXsWrGAU8UpyV6P95SWa98tF0T68ws2wKGonm-XStHtNGNupV56wR6ENjbUun01',
				'__EVENTVALIDATION'=>'/wEdAArzzd+UPA9s2BEZOVMkymAEIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV7G67MTrWGI73wI34LNPZ59KIHSM=',
				'__ASYNCPOST'=>'false',
				'ctl00$PHCentro$userLogin'=>$dados_login['login'],
				'ctl00$PHCentro$userPass'=>$dados_login['senha'],
				'ctl00$PHCentro$btnLogin'=>'Entrar'
			);

			$url = "https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_destinatario.aspx";
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, $dados_login['cookie_file']); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		
		return $dados_request_sefaz;
	}

	public function consultar_emitente($dados_login, $dados_request_sefaz){

		$data = array(
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__LASTFOCUS'=>'',
			'__VIEWSTATE'=>$dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=>$dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__VIEWSTATEENCRYPTED'=>'',
			'__EVENTVALIDATION'=>$dados_request_sefaz['EVENTVALIDATION'],
			'filtro'=>'rbt_filtro3',
			'txtPeriodoInicial'=>$dados_login['data_ini'],
			'txtPeriodoFinal'=>$dados_login['data_fim'],
			'cpf_cnpj'=>'rbt_cnpj',
			'txtCNPJEmitente'=>'',
			'AplicarFiltro'=>'Consultar',
			'CmdOrdenacao'=>'--Selecione--'
		);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_destinatario.aspx";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		
		return $dados_request_sefaz;
		
	}

	public function get_dados_planilha($dados_login, $dados_request_sefaz){
		$data = array(
		'__EVENTTARGET'=>'',
		'__EVENTARGUMENT'=>'',
		'__LASTFOCUS'=>'',
		'__VIEWSTATE'=>$dados_request_sefaz['VIEWSTATE'],
		'__VIEWSTATEGENERATOR'=>$dados_request_sefaz['VIEWSTATEGENERATOR'],
		'__VIEWSTATEENCRYPTED'=>'',
		'__EVENTVALIDATION'=>$dados_request_sefaz['EVENTVALIDATION'],
		'filtro'=>'rbt_filtro3',
		'txtPeriodoInicial'=>$dados_login['data_ini'],
		'txtPeriodoFinal'=>$dados_login['data_fim'],
		'cpf_cnpj'=>'rbt_cnpj',
		'txtCNPJDestinatario'=>'',
		'btn_GerarPlanilha'=>'Gerar Planilha',
		'CmdOrdenacao'=>'--Selecione--'
		);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_destinatario.aspx";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_HEADER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
		curl_close($ch);
		
		return $store;
	}

	//PESQUISA O LINK DA NOTA PELA CHAVE 
	public function consulta_link_nfe_chave($chave_nfe, $dados_login, $dados_request_sefaz){
		#MASK DA CHAVE PARA ACEITAR NA CONSULTA
		$chave_nfe = $this->mask($chave_nfe, '##-####-##.###.###/####-##-##-###-###.###.###-###.###.###-#');
		$data = array(
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__LASTFOCUS'=>'',
			'__VIEWSTATE'=>$dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=>$dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__VIEWSTATEENCRYPTED'=>'',
			'__EVENTVALIDATION'=>$dados_request_sefaz['EVENTVALIDATION'],
			'filtro'=>'rbt_filtro1',
			'txtChaveAcesso'=> $chave_nfe,
			'cpf_cnpj'=>'rbt_cnpj',
			'AplicarFiltro'=>'Consultar',
			'CmdOrdenacao'=>'--Selecione--'
		);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_destinatario.aspx";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_HEADER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
		curl_close($ch);

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);
		$elements = $xpath->query('//a');

		$link_nota_consulta = str_replace("../..", "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos", $elements[0]->attributes[0]->value);

		$url = $link_nota_consulta;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_HEADER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
		curl_close($ch);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/geral/NFENC_consulta_chave_acesso_detalhe.aspx";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_HEADER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
		curl_close($ch);

		# NESTE CASO SE A CONSULTA DA CHAVE FOR PARA RETORNAR O HTML DA NFE
		if(isset($dados_login["retornar_consulta"])){
			return $store;
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);
		$elements = $xpath->query('//table[@class="table_geral"]');

		$uf_nota = $elements[2]->firstChild->lastChild->lastChild->nodeValue;

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);
		$elements = $xpath->query('//table[@class="tabNFe"]/tbody/tr');

		$cidade_emitente = "";
		$cidade_destinatario = "";

		if($uf_nota == "BA"){

			$url_mdf_ba = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/geral/NFENC_consulta_chave_acesso_resumo.aspx";
			$ch_mdf_ba = curl_init(); 
			curl_setopt($ch_mdf_ba, CURLOPT_URL, $url_mdf_ba); 
			curl_setopt($ch_mdf_ba, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch_mdf_ba, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
			curl_setopt($ch_mdf_ba, CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch_mdf_ba, CURLOPT_HEADER,false);
			curl_setopt($ch_mdf_ba, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch_mdf_ba, CURLOPT_FOLLOWLOCATION, true);
			$store_mdf_ba = curl_exec($ch_mdf_ba);
			curl_close($ch_mdf_ba);

			$doc_mdf_ba = new DOMDocument();
			libxml_use_internal_errors(true);
			$doc_mdf_ba->loadHTML($store_mdf_ba);
			libxml_use_internal_errors(false);
			$xpath_mdf_ba = new DOMXpath($doc_mdf_ba);
			$elements_mdf_ba = $xpath_mdf_ba->query('//table[@class="table_geral"]');

			$mdf_ba_emitente = $elements_mdf_ba[2]->lastChild->firstChild->nodeValue;
			$cidade_emitente = substr($mdf_ba_emitente, 22);

			$mdf_ba_destinatario = $elements_mdf_ba[3]->lastChild->firstChild->nodeValue;
			$cidade_destinatario = substr($mdf_ba_destinatario, 22);

		}


		foreach ($elements as $element) {
			if (preg_match('/MDF-e Autorizado/', $element->nodeValue) && (preg_match('/: 610610/', $element->nodeValue) || preg_match('/: 610614/', $element->nodeValue))) {
				$dados_mdf = $element->nodeValue;
				$dados_mdf = explode(")", $dados_mdf);
				
				$dados_mdf_evento 	= trim($dados_mdf[0]).")";
				preg_match('/(\d{2}\/)+(\d{2}\/)+(\d{4})/', $dados_mdf[1], $dados_mdf_data);
				
				return array(
					'uf'=>$uf_nota,
					'mdf'=>$dados_mdf_evento,
					'data_mdf'=>$dados_mdf_data[0],
					'cidade_destinatario'=>$cidade_destinatario,
					'cidade_emitente'=>$cidade_emitente);
			}			
		}

		return array(
					'uf'=>$uf_nota,
					'mdf'=>'',
					'data_mdf'=>'',
					'cidade_destinatario'=>$cidade_destinatario,
					'cidade_emitente'=>$cidade_emitente);
	}

	public function return_array($string_notas, $dados_login, $dados_request_sefaz){
		$array_notas = explode("\n", $string_notas);
		
		for($i=0; $i<count($array_notas); $i++){
			$conteudo_linha = explode(";", $array_notas[$i]);
			if(strlen($this->apenas_numero($conteudo_linha[0])) == 0){
				unset($array_notas[$i]);
			}
		}
		unset($array_notas[count($array_notas)]);

		$array_nova = array();
		foreach ($array_notas as $nota) {
			$conteudo_nota = explode(";", $nota);

			#PERÍODO SEM NOTAS
			if(count($conteudo_nota) == 1 ){
				return $array_nova;
			}else{

				$chave_acesso = trim(str_replace("'", "", $conteudo_nota[5]));
				if (DateTime::createFromFormat('d/m/Y', $conteudo_nota[3]) !== FALSE) {
					$chave_acesso = trim(str_replace("'", "", $conteudo_nota[5]));
				}else{
					$chave_acesso = trim(str_replace("'", "", $conteudo_nota[6]));
				}

				if($dados_login["consultar_mdfe"] == "true"){
					$conteudo_consulta_mdf = $this->consulta_link_nfe_chave($chave_acesso, $dados_login, $dados_request_sefaz);
				}else{
					
					switch (substr($chave_acesso, 0,2)){
					    case '11':
					        $conteudo_consulta_mdf['uf'] = 'RO';
					        break;
					    case '12':
					        $conteudo_consulta_mdf['uf'] = 'AC';
					        break;
					    case '13':
					        $conteudo_consulta_mdf['uf'] = 'AM';
					        break;
				      	case '14':
					        $conteudo_consulta_mdf['uf'] = 'RR';
					        break;
				        case '15':
					        $conteudo_consulta_mdf['uf'] = 'PA';
					        break;
					    case '16':
					        $conteudo_consulta_mdf['uf'] = 'AP';
					        break;
					    case '17':
					        $conteudo_consulta_mdf['uf'] = 'TO';
					        break;
					    case '21':
					        $conteudo_consulta_mdf['uf'] = 'MA';
					        break;
				      	case '22':
					        $conteudo_consulta_mdf['uf'] = 'PI';
					        break;
				        case '23':
					        $conteudo_consulta_mdf['uf'] = 'CE';
					        break;
					    case '24':
					        $conteudo_consulta_mdf['uf'] = 'RN';
					        break;
					    case '25':
					        $conteudo_consulta_mdf['uf'] = 'PB';
					        break;
					    case '26':
					        $conteudo_consulta_mdf['uf'] = 'PE';
					        break;
				      	case '27':
					        $conteudo_consulta_mdf['uf'] = 'AL';
					        break;
				        case '28':
					        $conteudo_consulta_mdf['uf'] = 'SE';
					        break;
				      	case '29':
					        $conteudo_consulta_mdf['uf'] = 'BA';
					        break;
					    case '31':
					        $conteudo_consulta_mdf['uf'] = 'MG';
					        break;
					    case '32':
					        $conteudo_consulta_mdf['uf'] = 'ES';
					        break;
				      	case '33':
					        $conteudo_consulta_mdf['uf'] = 'RJ';
					        break;
				        case '35':
					        $conteudo_consulta_mdf['uf'] = 'SP';
					        break;
				        case '41':
					        $conteudo_consulta_mdf['uf'] = 'PR';
					        break;
					    case '42':
					        $conteudo_consulta_mdf['uf'] = 'SC';
					        break;
					    case '43':
					        $conteudo_consulta_mdf['uf'] = 'RS';
					        break;
				      	case '50':
					        $conteudo_consulta_mdf['uf'] = 'MS';
					        break;
				        case '51':
					        $conteudo_consulta_mdf['uf'] = 'MT';
					        break;
					    case '52':
					        $conteudo_consulta_mdf['uf'] = 'GO';
					        break;
				        case '53':
					        $conteudo_consulta_mdf['uf'] = 'DF';
					        break;
					}
				
					$conteudo_consulta_mdf['mdf'] 		= '';
					$conteudo_consulta_mdf['data_mdf'] 	= '';
					$conteudo_consulta_mdf['cidade_emitente'] = '';
					$conteudo_consulta_mdf['cidade_destinatario'] = '';
				}

				if (DateTime::createFromFormat('d/m/Y', $conteudo_nota[3]) !== FALSE) {
					$array_nova = array_merge($array_nova, array(
					//nfe+data+valor
					// $this->apenas_numero($conteudo_nota[0]).$this->apenas_numero($conteudo_nota[3]).$this->apenas_numero($conteudo_nota[4])
					$chave_acesso=>array(
						'doc_sefaz'=>$this-> apenas_numero($conteudo_nota[0]),
						'CNPJ/CPF'=> trim($conteudo_nota[1]),
						'razao_social_dest'=> trim($conteudo_nota[2]),
						'emissao'=>$conteudo_nota[3],
						'valor'=> str_replace(",", ".", str_replace(".", "", $conteudo_nota[4])),
						'chave_acesso'=>$chave_acesso,
						'situacao'=> trim($conteudo_nota[7]),
						'tipo_operacao'=> trim($conteudo_nota[8]),
						'uf'=>$conteudo_consulta_mdf['uf'],
						'mdf_evento'=>$conteudo_consulta_mdf['mdf'],
						'mdf_data'=>$conteudo_consulta_mdf['data_mdf'],
						'user_report'=>$dados_login["user_report"],
						'data_report'=>date('d/m/Y'),
						'cidade_emitente'=>$conteudo_consulta_mdf['cidade_emitente'],
						'cidade_destinatario'=>$conteudo_consulta_mdf['cidade_destinatario']
						)
					));
				}else{
					$array_nova = array_merge($array_nova, array(
					//nfe+data+valor
					//$this->apenas_numero($conteudo_nota[0]).$this->apenas_numero($conteudo_nota[3]).$this->apenas_numero($conteudo_nota[4])
					$chave_acesso=>array(
						'doc_sefaz'=>$this->apenas_numero($conteudo_nota[0]),
						'CNPJ/CPF'=> trim($conteudo_nota[1]),
						'razao_social_dest'=> trim($conteudo_nota[2]." ".$conteudo_nota[3]),
						'emissao'=> $conteudo_nota[4],
						'valor'=>str_replace(",", ".", str_replace(".", "", $conteudo_nota[5])), 
						'chave_acesso'=>$chave_acesso,
						'situacao'=> trim($conteudo_nota[8]),
						'tipo_operacao'=> trim($conteudo_nota[9]),
						'uf'=>$conteudo_consulta_mdf['uf'],
						'mdf_evento'=>$conteudo_consulta_mdf['mdf'],
						'mdf_data'=>$conteudo_consulta_mdf['data_mdf'],
						'user_report'=>$dados_login["user_report"],
						'data_report'=>date('d/m/Y'),
						'cidade_emitente'=>$conteudo_consulta_mdf['cidade_emitente'],
						'cidade_destinatario'=>$conteudo_consulta_mdf['cidade_destinatario']
						)
					));
				}
				
			}
		}
		
		return $array_nova;
	}

	public function apenas_numero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	}

	public function ler_csv_cliente($file){
		$linhas = file ($file['tmp_name']);

		for($i=0; $i<count($linhas); $i++){
			$conteudo_linha = explode(";", $linhas[$i]);
			if(strlen($this->apenas_numero($conteudo_linha[0])) == 0){
				unset($linhas[$i]);
			}
		}

		$array_nova = array();
		foreach ($linhas as $nota) {
			$conteudo_nota = explode(";", $nota);		

			array_push($array_nova, $this->apenas_numero($conteudo_nota[0]));	
		}
		$array_nova = array_unique($array_nova);
		return $array_nova;		
	}

	//MÁSCARA DA NOTA FISCAL
	public function mask($val, $mask)
	{
		$maskared = '';
		$k = 0;
		for($i = 0; $i<=strlen($mask)-1; $i++)
		{
			if($mask[$i] == '#')
			{
				if(isset($val[$k]))
					$maskared .= $val[$k++];
			}
			else
			{
				if(isset($mask[$i]))
					$maskared .= $mask[$i];
			}
		}
		return $maskared;
	}

	public function get_nfe_destinatario($cnpj_data){
		$this->db->select('*');
		$this->db->from('nfe_destinatario');
		$this->db->where('cnpj_data', $cnpj_data);
		return $this->db->get()->result_array();
	}

	public function delete_consulta($cnpj_data){
		$this->db->delete('nfe_destinatario', array('cnpj_data' => $cnpj_data)); 
	}

	public function insert_nfe_destinatario($dados){
		$data = array(
			"cnpj_data" => $dados["cnpj_data"],
			"doc_sefaz" => $dados["doc_sefaz"],
			"cnpj_emit" => $dados["CNPJ/CPF"],
			"razao_emit" => $dados["razao_social_dest"],
			"data_emissao" => $dados["emissao"],
			"valor" => $dados["valor"],
			"chave_nfe" => $dados["chave_acesso"],
			"situacao_nfe" => $dados["situacao"],
			"tipo_operacao" => $dados["tipo_operacao"],
			"uf" => $dados["uf"],
			"mdf_evento" => $dados["mdf_evento"],
			"mdf_data" => $dados["mdf_data"],
			"user_report" => $dados["user_report"],
			"data_report" => $dados["data_report"],
			"consultou_mdfe" => $dados["consultou_mdfe"],
			"cidade_destinatario" => $dados["cidade_destinatario"],
			"cidade_emitente" => $dados["cidade_emitente"]
		);

		$this->db->insert('nfe_destinatario', $data);
	}

}