<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionario_model extends CI_Model {
	private $id;
	private $nome;
	private $login;
	private $senha;
	private $permissoes;
	private $permissoes_menu;
	private $permissoes_nova;
	private $id_cadastro;
	private $is_funcionario;
	private $data_cadastro;
	private $validade;
	private $data_acesso;

	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getNome() {
		return $this->nome;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}

	public function getLogin() {
		return $this->login;
	}
	public function setLogin($login) {
		$this->login = $login;
		return $this;
	}
	
	public function getSenha() {
		return $this->senha;
	}
	public function setSenha($senha) {
		$this->senha = $senha;
		return $this;
	}

	public function getPermissoes() {
		return $this->permissoes_nova;
	}
	public function setPermissoes($permissoes_nova) {
		$this->permissoes_nova = $permissoes_nova;
		return $this;
	}

	public function getIdCadastro() {
		return $this->id_cadastro;
	}
	public function setIdCadastro($id_cadastro) {
		$this->id_cadastro = $id_cadastro;
		return $this;
	}

	public function getIsFuncionario() {
		return $this->is_funcionario;
	}

	public function setIsFuncionario($is_funcionario) {
		$this->is_funcionario = $is_funcionario;
		return $this;
	}
	public function getDataCadastro() {
		return $this->data_cadastro;
	}
	public function setDataCadastro($data_cadastro) {
		$this->data_cadastro = $data_cadastro;
		return $this;
	}
	public function getValidade() {
		return $this->validade;
	}
	public function setValidade($validade) {
		$this->validade = $validade;
		return $this;
	}
	public function getDataAcesso() {
		return $this->data_acesso;
	}
	public function setDataAcesso($data_acesso) {
		$this->data_acesso = $data_acesso;
		return $this;
	}

	public function cadastrar(){
	
		$dados = array(

				'nome' => $this->getNome(),
				'login' => $this->getLogin(),
				'senha' => $this->getSenha(),
				'id_cadastro' => $this->getIdCadastro(),
				'is_funcionario' => 'TRUE',
				'data_cadastro' => $this->getDataCadastro(),
				'nivel' => '2',
				'validade' => $this->getValidade()
		);
	
		if ($this->db->insert('dtb_usuarios', $dados)){
			$id = $this->db->insert_id();
			$this->inserir_empresas_colaborador($id);

			return $id;
		} else {
			return FALSE;
		}
	}

	public function inserir_empresas_colaborador($id){
		$empresas = $this->buscar_todas_empresas();

		if(sizeof($empresas) != 0){
			foreach ($empresas as $empresa) {	
				$data = array( 
		        	'id_empresa' =>  $empresa->id, 
		        	'id_usuario' =>  $id
			    );

				$this->db->insert('dtb_empresa_usuario', $data);		
			}
		}
	}

	public function buscar_todas_empresas(){
		$this->db->select('id');
		$this->db->from('dtb_empresas');
		return $this->db->get()->result();
	}
	
	public function listar() {
		
		if ($this->session->userdata['userprimesession']['nivel']!=1) {
			$this->db->where("nivel", 2);
			$this->db->where("nivel !=", 0);
		}

		$this->db->where("nivel !=", 0);
		return $this->db->get('dtb_usuarios')->result();
	}

	public function excluir(){
		return $this->db->delete('dtb_usuarios', "id = {$this->getId()}");
	}

	public function pesquisar_funcionario_id() {
		$this->db->select('*');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_usuarios')->row();
	}

	public function editar(){
	
		$dados = array(				
				'nome' => $this->getNome(),
				'login' => $this->getLogin(),
				'senha' => $this->getSenha(),
				'validade' => $this->getValidade()
		);
	
		if ($this->db->update('dtb_usuarios', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function cadastrar_permissoes(){
	
		$dados = array(				
				'permissoes_nova' => $this->getPermissoes()
		);
	
		if ($this->db->update('dtb_usuarios', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function editar_permissoes(){

		$dados = array(				
				'permissoes_nova' => $this->getPermissoes()
		);
	
		if ($this->db->update('dtb_usuarios', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_acesso(){
	
		$dados = array(
				'data_acesso' => $this->getDataAcesso()
		);
	
		return $this->db->update('dtb_usuarios', $dados, "id={$this->getId()}");
	
	}

	public function listar_painel() {
		$this->db->select('nome, nivel, data_acesso');
		$this->db->from('dtb_usuarios');
		$this->db->where("nivel !=", 0);
		$this->db->where("id_cadastro !=", 1);
		$this->db->order_by("data_acesso", "desc");
		return $this->db->get()->result();
	}

	public function tornar_master(){
	
		$dados = array(
				'is_funcionario' => NULL,
				'nivel' => '1',
				'permissoes_nova' => NULL
		);
	
		return $this->db->update('dtb_usuarios', $dados, "id={$this->getId()}");
	
	}

	public function remover_master(){
	
		$dados = array(
				'is_funcionario' => 'TRUE',
				'nivel' => '2',
				'permissoes_nova' => 'a:23:{s:10:\"mDashboard\";s:1:\"1\";s:23:\"mIndicadoresEmpresarias\";s:1:\"1\";s:16:\"mPendenciasSefaz\";s:1:\"1\";s:14:\"mDividasAtivas\";s:1:\"1\";s:9:\"mGraficos\";s:1:\"1\";s:10:\"mCadastros\";s:1:\"1\";s:9:\"mEmpresas\";s:1:\"1\";s:9:\"mUsuarios\";s:1:\"1\";s:9:\"mContador\";s:1:\"1\";s:14:\"mAlterarSenhas\";s:1:\"1\";s:17:\"mCertidaoNegativa\";s:1:\"1\";s:14:\"mComprasVendas\";s:1:\"1\";s:11:\"mDocumentos\";s:1:\"1\";s:8:\"mAlvaras\";s:1:\"1\";s:16:\"mDocumentosLista\";s:1:\"1\";s:18:\"mTiposDeDocumentos\";s:1:\"1\";s:6:\"mJuceb\";s:1:\"1\";s:25:\"mJucebConsultaViabilidade\";s:1:\"1\";s:17:\"mSituacaoRegistro\";s:1:\"1\";s:18:\"mRegistroDeAcessos\";s:1:\"1\";s:19:\"mProtocoloMensagens\";s:1:\"1\";s:11:\"mCalendario\";s:1:\"1\";s:11:\"mRelatorios\";s:1:\"1\";}'
		);
	
		return $this->db->update('dtb_usuarios', $dados, "id={$this->getId()}");
	
	}

	public function atualizaListaEmpresaUsuario($id_usuario, $empresas){
		$this->db->delete('dtb_empresa_usuario', array('id_usuario' => $id_usuario));

		if(sizeof($empresas) != 0){
			foreach ($empresas as $empresa) {	
				$data = array( 
		        	'id_empresa' =>  $empresa, 
		        	'id_usuario' =>  $id_usuario
			    );

				$this->db->insert('dtb_empresa_usuario', $data);		
			}
		}

		return TRUE;
		
	}

	public function resetar_senha(){
	
		$dados = array(
				'senha' => '81dc9bdb52d04dc20036dbd8313ed055'
		);
	
		return $this->db->update('dtb_usuarios', $dados, "id={$this->getId()}");
	
	}

}