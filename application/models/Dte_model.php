<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);

class Dte_model extends CI_Model {	

	public function main($login, $senha){

		//Realizando Login
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEJAR, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__PREVIOUSPAGE"]');
		$dados_request_sefaz['PREVIOUSPAGE'] = $elements[0]->attributes[3]->value;		

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login?ReturnUrl=%2fsistemas%2fDTE%2fContribuinte%2fDefault.aspx";
		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=> $dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=> $dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__PREVIOUSPAGE'=> $dados_request_sefaz['PREVIOUSPAGE'],
			'__EVENTVALIDATION'=> $dados_request_sefaz['EVENTVALIDATION'],
			'__ASYNCPOST'=>'false',
			'ctl00$PHCentro$userLogin'=> $login,
			'ctl00$PHCentro$userPass'=> $senha,
			'ctl00$PHCentro$btnLogin'=>'Entrar'
		);

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true, "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$elements = $xpath->query('//*[@id="PHConteudo_gdvListaContaDomicilioTributario"]/tbody/tr/td[2]');
		$cnpj = $elements[0]->nodeValue;

		#-> Verificando se na coluna Situação/Mensagem não está escrito "Não há mensagem"
		$elements = $xpath->query('//*[@id="PHConteudo_gdvListaContaDomicilioTributario"]/tbody/tr/td[7]/span');
		$msg = trim($elements[0]->nodeValue);
		if($msg == "Não há mensagem"){
			echo json_encode(array("error"=> true, "msg"=>"Não há mensagens na Sefaz referente a esta empresa!", "type"=>"warning"));
			exit();
		}

		//->Se possui mensagens, logo vou consultá-las
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Contribuinte/LeituraCaixaMensagem/LeituraCaixaMensagem.aspx?numeroCnpjCpfCompleto=".$cnpj;
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true, "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$html = explode('<table class="table table-striped table-condensed table-bordered table-hover dataTable" id="PHConteudo_gdvCaixaMensagem">', $response);
		$html = explode("</table", $html[1]);

		$html = $html[0];

		$html = str_replace('__doPostBack', 'ler_mensagem', $html);
		$html = str_replace("&#39;&#39;", "'".$login."', '".$senha."'", $html);
		echo json_encode(array("error"=> false, "table"=>$html));

	}

	public function ler_mensagem($login, $senha, $id){

		//Realizando Login
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEJAR, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__PREVIOUSPAGE"]');
		$dados_request_sefaz['PREVIOUSPAGE'] = $elements[0]->attributes[3]->value;		

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login?ReturnUrl=%2fsistemas%2fDTE%2fContribuinte%2fDefault.aspx";
		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=> $dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=> $dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__PREVIOUSPAGE'=> $dados_request_sefaz['PREVIOUSPAGE'],
			'__EVENTVALIDATION'=> $dados_request_sefaz['EVENTVALIDATION'],
			'__ASYNCPOST'=>'false',
			'ctl00$PHCentro$userLogin'=> $login,
			'ctl00$PHCentro$userPass'=> $senha,
			'ctl00$PHCentro$btnLogin'=>'Entrar'
		);

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true, "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$elements = $xpath->query('//*[@id="PHConteudo_gdvListaContaDomicilioTributario"]/tbody/tr/td[2]');
		$cnpj = $elements[0]->nodeValue;

		//->Se possui mensagens, logo vou consultá-las
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Contribuinte/LeituraCaixaMensagem/LeituraCaixaMensagem.aspx?numeroCnpjCpfCompleto=".$cnpj;
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true , "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__PREVIOUSPAGE"]');
		$dados_request_sefaz['PREVIOUSPAGE'] = $elements[0]->attributes[3]->value;	

		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|'.urldecode($id),
			'ctl00$nomeUsuario'=>'',
			'PHConteudo_gdvCaixaMensagem_length'=>'100',
			'__EVENTTARGET'=> urldecode($id),
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=> $dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=> $dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__PREVIOUSPAGE'=> $dados_request_sefaz['PREVIOUSPAGE'],
			'__EVENTVALIDATION'=> $dados_request_sefaz['EVENTVALIDATION'],
			'__VIEWSTATEENCRYPTED'=>'',			
			'__ASYNCPOST'=>'false'
		);

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Contribuinte/LeituraCaixaMensagem/LeituraCaixaMensagem.aspx?numeroCnpjCpfCompleto=".$cnpj;
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true, "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$texto = explode('<label id="Label2" class="control-label">', $response);
		$texto = explode('</label>', $texto[1]);
		$texto = $texto[0];
		

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$elements = $xpath->query('//*[@id="janelaModalMensagemDomicilioTributario"]/div/div[2]');
		$mensagem_body = $elements[0]->nodeValue;

		$elements = $xpath->query('//*[@id="Label1"]/h3');
		$mensagem_title = $elements[0]->nodeValue;

		echo(json_encode(array(
			"error"=> false,
			"title"=>$mensagem_title,
			"body"=> $texto
		)));

	}

	public function get_pdf($login, $senha, $id){

		header('Content-type: application/pdf');

		//Realizando Login
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEJAR, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__PREVIOUSPAGE"]');
		$dados_request_sefaz['PREVIOUSPAGE'] = $elements[0]->attributes[3]->value;		

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login?ReturnUrl=%2fsistemas%2fDTE%2fContribuinte%2fDefault.aspx";
		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=> $dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=> $dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__PREVIOUSPAGE'=> $dados_request_sefaz['PREVIOUSPAGE'],
			'__EVENTVALIDATION'=> $dados_request_sefaz['EVENTVALIDATION'],
			'__ASYNCPOST'=>'false',
			'ctl00$PHCentro$userLogin'=> $login,
			'ctl00$PHCentro$userPass'=> $senha,
			'ctl00$PHCentro$btnLogin'=>'Entrar'
		);

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true, "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$elements = $xpath->query('//*[@id="PHConteudo_gdvListaContaDomicilioTributario"]/tbody/tr/td[2]');
		$cnpj = $elements[0]->nodeValue;

		//->Se possui mensagens, logo vou consultá-las
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Contribuinte/LeituraCaixaMensagem/LeituraCaixaMensagem.aspx?numeroCnpjCpfCompleto=".$cnpj;
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true , "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($response);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__PREVIOUSPAGE"]');
		$dados_request_sefaz['PREVIOUSPAGE'] = $elements[0]->attributes[3]->value;	

		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|'.urldecode($id),
			'ctl00$nomeUsuario'=>'',
			'PHConteudo_gdvCaixaMensagem_length'=>'100',
			'__EVENTTARGET'=> urldecode($id),
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=> $dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=> $dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__PREVIOUSPAGE'=> $dados_request_sefaz['PREVIOUSPAGE'],
			'__EVENTVALIDATION'=> $dados_request_sefaz['EVENTVALIDATION'],
			'__VIEWSTATEENCRYPTED'=>'',	
			'__ASYNCPOST'=>'false'
		);

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Modulos/Contribuinte/LeituraCaixaMensagem/LeituraCaixaMensagem.aspx?numeroCnpjCpfCompleto=".$cnpj;
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		if(!$this->valida_request($response)){
			return(json_encode(array("error"=> true, "msg"=>"Infelizmente o processo de comunicação com a Sefaz não foi realizado.\nPor favor, tentar novamente!", "type"=>"error")));
		}

		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Relatorio/Relatorio";
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_COOKIEFILE, $login.".txt"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$response = curl_exec($ch);

		print($response);
		exit();

	}

	public function valida_request($response){
		if(strpos($response, 'Sua sessão expira em') != false){
			return true;
		}else{
			return false;
		}
	}

}