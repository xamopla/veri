<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Certidao_salvador_model extends CI_Model {

	public function listar(){
		$this->db->select('db.id_empresa, db.cnpj, db.inscricao_estadual, db.razao_social, db.status, db.motivo');
		$this->db->from('dtb_cnd_salvador db');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_cnd_salvador.cpf) = trim(e.cnpj)','left');
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->group_by('db.cnpj');
		return $this->db->get()->result();
	}

}