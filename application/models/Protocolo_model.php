<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Protocolo_model extends CI_Model {

	private $usuario;
	private $data;
	private $descricao;
	private $idnotificacao;
	private $idnotificacaoresumo;
	private $idnotificacaoprocessosipro;

	private $filtro;

	function __construct()
	{
		parent::__construct();
	}

	public function getUsuario() {
		return $this->usuario;
	}
	public function setUsuario($usuario) {
		$this->usuario = $usuario;
		return $this;
	}

	public function getData() {
		return $this->data;
	}
	public function setData($data) {
		$this->data = $data;
		return $this;
	}

	public function getDescricao() {
		return $this->descricao;
	}
	public function setDescricao($descricao) {
		$this->descricao = $descricao;
		return $this;
	}

	public function getIdnotificacao() {
		return $this->idnotificacao;
	}
	public function setIdnotificacao($idnotificacao) {
		$this->idnotificacao = $idnotificacao;
		return $this;
	}

	public function getIdnotificacaoresumo() {
		return $this->idnotificacaoresumo;
	}
	public function setIdnotificacaoresumo($idnotificacaoresumo) {
		$this->idnotificacaoresumo = $idnotificacaoresumo;
		return $this;
	}

	public function getIdnotificacaoprocessosipro() {
	   return $this->idnotificacaoprocessosipro;
	}
	
	public function setIdnotificacaoprocessosipro($idnotificacaoprocessosipro) {
	   $this->idnotificacaoprocessosipro = $idnotificacaoprocessosipro;
	}

	public function getFiltro() {
		return $this->filtro;
	}

	public function setFiltro($filtro) {
		$this->filtro = $filtro;
		return $this;
	}

	public function listar(){
		if ($this->session->userdata['userprimesession']['nivel']==2){

			$sql = "SELECT n.id as idnotificacao, n.razao_social as razao_social, n.situacao as situacao, n.data_atualizacao as data, n.status as status, FALSE as isnotificaoresumo, p.usuario, p.data as dataresolucao, p.descricao as descricao, p.id as idprotocolo, e.motivo as motivo, e.id as idEmpresa, e.cnpj_completo as cnpj_completo, e.senha_sefaz as senha_sefaz, e.login_sefaz as login_sefaz, e.inscricao_estadual as ie   
			FROM notificacoes as n
			LEFT OUTER JOIN protocolo as p on n.id = p.idnotificacao
			LEFT OUTER JOIN dtb_empresas as e on n.id_empresa = e.id 
			LEFT OUTER JOIN dtb_empresa_usuario as eu on n.id_empresa = eu.id_empresa
			WHERE (e.id_funcionario = ".$this->session->userdata['userprimesession']['id']. "
			OR eu.id_usuario = ".$this->session->userdata['userprimesession']['id'].")";

			if($this->getFiltro() != "Todas"){
				if($this->getFiltro() == "Verificadas"){
					$sql = $sql." and n.status = 0 ";
				}else{
					$sql = $sql." and n.status = 1 ";
				}
			}
			
			$sql = $sql."
			GROUP BY (n.id) 
		UNION 
			SELECT nr.id as idnotificacao, nr.razaoSocial as razao_social, nr.descricao as situacao, nr.data_atualizacao as data, nr.status as status, TRUE as isnotificaoresumo, pr.usuario, pr.data as dataresolucao, pr.descricao as descricao, pr.id as idprotocolo, null as motivo, e.id as idEmpresa, e.cnpj_completo as cnpj_completo, e.senha_sefaz as senha_sefaz, e.login_sefaz as login_sefaz, e.inscricao_estadual as ie      
			FROM notificacoesresumofiscal nr
			LEFT OUTER JOIN protocolo as pr on nr.id = pr.idnotificacaoresumo
			LEFT OUTER JOIN dtb_empresas e on trim(nr.cnpjstring) = trim(e.cnpj_completo)
			LEFT OUTER JOIN dtb_empresa_usuario as eu on e.id = eu.id_empresa 
			WHERE (e.id_funcionario = ".$this->session->userdata['userprimesession']['id']. "
			OR eu.id_usuario = ".$this->session->userdata['userprimesession']['id'].")";

			if($this->getFiltro() != "Todas"){
				if($this->getFiltro() == "Verificadas"){
					$sql = $sql." and nr.status = 0 ";
				}else{
					$sql = $sql." and nr.status = 1 ";
				}
			}

			$sql = $sql."
			GROUP BY (nr.id) 
		UNION 
			SELECT ns.id as idnotificacao, e.razao_social as razao_social, ns.objetivo as situacao, ns.data_atualizacao as data, ns.status as status, 2 as isnotificaoresumo, ps.usuario, ps.data as dataresolucao, ps.descricao as descricao, ps.id as idprotocolo, null as motivo, e.id as idEmpresa, e.cnpj_completo as cnpj_completo, e.senha_sefaz as senha_sefaz, e.login_sefaz as login_sefaz, e.inscricao_estadual as ie      
			FROM notificacoesprocessosipro ns
			LEFT OUTER JOIN protocolo as ps on ns.id = ps.idnotificacaoprocessosipro
			LEFT OUTER JOIN dtb_empresas as e on ns.id_empresa = e.id 
			LEFT OUTER JOIN dtb_empresa_usuario as eu on e.id = eu.id_empresa 
			WHERE (e.id_funcionario = ".$this->session->userdata['userprimesession']['id']. "
			OR eu.id_usuario = ".$this->session->userdata['userprimesession']['id'].")";

			if($this->getFiltro() != "Todas"){
				if($this->getFiltro() == "Verificadas"){
					$sql = $sql." and ns.status = 0 ";
				}else{
					$sql = $sql." and ns.status = 1 ";
				}
			}

			$sql = $sql."
			GROUP BY (ns.id) 
			";

		}else{
			$sql = "SELECT n.id as idnotificacao, n.razao_social as razao_social, n.situacao as situacao, n.data_atualizacao as data, n.status as status, FALSE as isnotificaoresumo, p.usuario, p.data as dataresolucao, p.descricao as descricao, p.id as idprotocolo, e.motivo as motivo, e.id as idEmpresa, e.cnpj_completo as cnpj_completo, e.senha_sefaz as senha_sefaz, e.login_sefaz as login_sefaz, e.inscricao_estadual as ie     
			FROM notificacoes as n 
			LEFT OUTER JOIN protocolo as p on n.id = p.idnotificacao 
			LEFT OUTER JOIN dtb_empresas as e on n.id_empresa = e.id ";

			if($this->getFiltro() != "Todas"){
				if($this->getFiltro() == "Verificadas"){
					$sql = $sql." where n.status = 0 ";
				}else{
					$sql = $sql." where n.status = 1 ";
				}
			}
			
			$sql = $sql."
		UNION 
			SELECT nr.id as idnotificacao, nr.razaoSocial as razao_social, nr.descricao as situacao, nr.data_atualizacao as data, nr.status as status, TRUE as isnotificaoresumo, pr.usuario, pr.data as dataresolucao, pr.descricao as descricao, pr.id as idprotocolo, null as motivo, e.id as idEmpresa, e.cnpj_completo as cnpj_completo, e.senha_sefaz as senha_sefaz, e.login_sefaz as login_sefaz, e.inscricao_estadual as ie       
			FROM notificacoesresumofiscal nr 
			LEFT OUTER JOIN protocolo as pr on nr.id = pr.idnotificacaoresumo 
			LEFT OUTER JOIN dtb_empresas e on trim(nr.cnpjstring) = trim(e.cnpj_completo) ";

			if($this->getFiltro() != "Todas"){
				if($this->getFiltro() == "Verificadas"){
					$sql = $sql." where nr.status = 0 ";
				}else{
					$sql = $sql." where nr.status = 1 ";
				}
			}

			$sql = $sql."
		UNION 
			SELECT ns.id as idnotificacao, e.razao_social as razao_social, ns.objetivo as situacao, ns.data_atualizacao as data, ns.status as status, 2 as isnotificaoresumo, ps.usuario, ps.data as dataresolucao, ps.descricao as descricao, ps.id as idprotocolo, null as motivo, e.id as idEmpresa, e.cnpj_completo as cnpj_completo, e.senha_sefaz as senha_sefaz, e.login_sefaz as login_sefaz, e.inscricao_estadual as ie   
			FROM notificacoesprocessosipro ns
			LEFT OUTER JOIN protocolo as ps on ns.id = ps.idnotificacaoprocessosipro 
			LEFT OUTER JOIN dtb_empresas as e on ns.id_empresa = e.id ";

			if($this->getFiltro() != "Todas"){
				if($this->getFiltro() == "Verificadas"){
					$sql = $sql." where ns.status = 0 ";
				}else{
					$sql = $sql." where ns.status = 1 ";
				}
			}
		}

		
		return $this->db->query($sql)->result();
	}

	public function salvar(){
		$dados = array(
	
				'usuario' => $this->getUsuario(),
				'data' => $this->getData(),
				'descricao' => $this->getDescricao(),
				'idnotificacao' => $this->getIdnotificacao(),
				'idnotificacaoresumo' => $this->getIdnotificacaoresumo(),
				'idnotificacaoprocessosipro' => $this->getIdnotificacaoprocessosipro()
		);
	
		if ($this->db->insert('protocolo', $dados)){
			return 1;
		} else {
			return 0;
		}
	}
}