<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Alvara_model extends CI_Model {

	private $cnpj;
	private $cnpjSemFormatacao;
	private $inscricaoEstadual;
	private $razaoSocial;
	private $inscricaoMunicipal;
	private $numeroTVL;
	private $situacao;
	private $validade;
	
	function __construct()
	{
		parent::__construct();
	}

	public function getCnpj() {
		return $this->cnpj;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}

	public function getCnpjSemFormatacao() {
		return $this->cnpjSemFormatacao;
	}
	public function setCnpjSemFormatacao($cnpjSemFormatacao) {
		$this->cnpjSemFormatacao = $cnpjSemFormatacao;
		return $this;
	}

	public function getInscricaoEstadual() {
		return $this->inscricaoEstadual;
	}
	public function setInscricaoEstadual($inscricaoEstadual) {
		$this->inscricaoEstadual = $inscricaoEstadual;
		return $this;
	}

	public function getRazaoSocial() {
		return $this->razaoSocial;
	}
	public function setRazaoSocial($razaoSocial) {
		$this->razaoSocial = $razaoSocial;
		return $this;
	}

	public function getInscricaoMunicipal() {
		return $this->inscricaoMunicipal;
	}
	public function setInscricaoMunicipal($inscricaoMunicipal) {
		$this->inscricaoMunicipal = $inscricaoMunicipal;
		return $this;
	}

	public function getNumeroTVL() {
		return $this->numeroTVL;
	}
	public function setNumeroTVL($numeroTVL) {
		$this->numeroTVL = $numeroTVL;
		return $this;
	}

	public function getSituacao() {
		return $this->situacao;
	}
	public function setSituacao($situacao) {
		$this->situacao = $situacao;
		return $this;
	}

	public function getValidade() {
		return $this->validade;
	}
	public function setValidade($validade) {
		$this->validade = $validade;
		return $this;
	}

	public function listar() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){

			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function vencidos() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){

			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.validade) != 'Definitivo' and trim(dtb_alvara.validade) != '' and DATE_FORMAT(STR_TO_DATE(trim(dtb_alvara.validade), '%d/%m/%Y'), '%Y-%m-%d') < now()");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function ativaprovisoria() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.situacao) like '%Ativa Provisoria%' ");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function ativaregular() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.situacao) like '%Ativa Regular%' ");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function ativatemporaria() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.situacao) like '%Ativa Temporária%' ");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function suspensaRecadastramento() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.situacao) like '%Suspensa Por Falta Recadastramento%' ");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function suspensaRenovacao() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.situacao) like '%Suspensa Por Falta Renovação Alvará%' ");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function listarForProcessamento() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.id as id_empresa');	
		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');
		$this->db->where("trim(validade) != ","");
		$this->db->where("trim(validade) != ", "Definitivo");
		return $this->db->get('dtb_alvara')->result();

	}

	public function listarForProcessamentoAlvaraPublicidade() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.id as id_empresa');	
		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');
		$this->db->where("trim(data_validade_publicidade) != ","");
		$this->db->where("trim(data_validade_publicidade) != ", "Definitivo");
		return $this->db->get('dtb_alvara')->result();

	}

	public function listarForProcessamentoAlvaraFuncionamento() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.id as id_empresa');	
		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');
		$this->db->where("trim(data_validade_funcionamento) != ","");
		$this->db->where("trim(data_validade_funcionamento) != ", "Definitivo");
		return $this->db->get('dtb_alvara')->result();

	}

	public function vencidosPublicidade() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){

			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.data_validade_publicidade) != 'Definitivo' and trim(dtb_alvara.data_validade_publicidade) != '' and DATE_FORMAT(STR_TO_DATE(trim(dtb_alvara.data_validade_publicidade), '%d/%m/%Y'), '%Y-%m-%d') < now()");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function vencidosFuncionamento() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){

			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("trim(dtb_alvara.data_validade_funcionamento) != 'Definitivo' and trim(dtb_alvara.data_validade_funcionamento) != '' and DATE_FORMAT(STR_TO_DATE(trim(dtb_alvara.data_validade_funcionamento), '%d/%m/%Y'), '%Y-%m-%d') < now()");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function semCpfRepresentante() {
		$this->db->select('dtb_alvara.cnpj, dtb_alvara.cnpj_sem_formatacao, dtb_alvara.inscricao_estadual, dtb_alvara.inscricao_municipal, dtb_alvara.razao_social, dtb_alvara.numero_tvl, dtb_alvara.situacao, dtb_alvara.validade, dtb_alvara.data_validade_publicidade, dtb_alvara.data_validade_funcionamento, e.login_mei');

		$this->db->join('dtb_empresas as e', 'dtb_alvara.cnpj_sem_formatacao = e.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}

		$this->db->where("e.login_mei is null");
		$this->db->group_by('dtb_alvara.cnpj_sem_formatacao');
		return $this->db->get('dtb_alvara')->result();

	}

	public function excluirAlvaraByCnpj(){
		return $this->db->delete('dtb_alvara', "trim(cnpj) = '{$this->getCnpj()}'");
	}
}