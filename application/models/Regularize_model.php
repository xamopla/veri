<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Regularize_model extends CI_Model {

	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	//LISTAGEM
	public function listar(){

		$this->db->select('db.id_empresa, db.tipo_de_divida, db.domicilio_devedor, e.razao_social, e.cnpj, SUM(db.valor_total_da_divida) as total');
		$this->db->join('dtb_empresas as e', 'db.id_empresa = e.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() == 'FGTS'){
			$this->db->where('db.tipo_de_divida like "%FGTS%" ');
        }
        else if($this->getFiltro() == 'PREVIDENCIARIA'){
            $this->db->where('db.tipo_de_divida like "%DÍVIDA PREVIDENCIÁRIA%" ');
        }
        else if($this->getFiltro() == 'TRABALHISTA'){
            $this->db->where('db.tipo_de_divida like "%MULTA TRABALHISTA%" ');
        }
        else if($this->getFiltro() == 'ELEITORAL'){
            $this->db->where('db.tipo_de_divida like "%ELEITORAL%" ');
        }
        else if($this->getFiltro() == 'CRIMINAL'){
            $this->db->where('db.tipo_de_divida like "%CRIMINAL%" ');
        }
        else if($this->getFiltro() == 'TRIBUTARIOS'){
            $this->db->where('db.tipo_de_divida like "%DEMAIS DÉBITOS TRIBUTÁRIOS%" ');
        }
        else if($this->getFiltro() == 'NAO_TRIBUTARIO'){
            $this->db->where('db.tipo_de_divida like "%DEMAIS DÉBITOS NÃO TRIBUTÁRIOS%" ');
        }

		$this->db->group_by('db.id_empresa');
		return $this->db->get('dtb_divida_ativa_detalhada db')->result();
	}


	//DETALHE
	public function detalhe($id_empresa){

		$this->db->select('db.id_empresa, db.tipo_de_divida, db.numero_inscricao, db.domicilio_devedor, e.razao_social, e.cnpj, db.valor_total_da_divida as total');
		$this->db->join('dtb_empresas as e', 'db.id_empresa = e.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() == 'FGTS'){
			$this->db->where('db.tipo_de_divida like "%FGTS%" ');
        }
        else if($this->getFiltro() == 'PREVIDENCIARIA'){
            $this->db->where('db.tipo_de_divida like "%DÍVIDA PREVIDENCIÁRIA%" ');
        }
        else if($this->getFiltro() == 'TRABALHISTA'){
            $this->db->where('db.tipo_de_divida like "%MULTA TRABALHISTA%" ');
        }
        else if($this->getFiltro() == 'ELEITORAL'){
            $this->db->where('db.tipo_de_divida like "%ELEITORAL%" ');
        }
        else if($this->getFiltro() == 'CRIMINAL'){
            $this->db->where('db.tipo_de_divida like "%CRIMINAL%" ');
        }
        else if($this->getFiltro() == 'TRIBUTARIOS'){
            $this->db->where('db.tipo_de_divida like "%DEMAIS DÉBITOS TRIBUTÁRIOS%" ');
        }
        else if($this->getFiltro() == 'NAO_TRIBUTARIO'){
            $this->db->where('db.tipo_de_divida like "%DEMAIS DÉBITOS NÃO TRIBUTÁRIOS%" ');
        }

		$this->db->where('db.id_empresa', $id_empresa);
		return $this->db->get('dtb_divida_ativa_detalhada db')->result();
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//CONSULTAS QUANTIDADE
	public function qtd_empresas(){

		$this->db->select('COUNT(distinct(db.id_empresa)) AS valor');
		$this->db->join('dtb_empresas as e', 'db.id_empresa = e.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() == 'FGTS'){
			$this->db->where('db.tipo_de_divida like "%FGTS%" ');
        }
        else if($this->getFiltro() == 'PREVIDENCIARIA'){
            $this->db->where('db.tipo_de_divida like "%DÍVIDA PREVIDENCIÁRIA%" ');
        }
        else if($this->getFiltro() == 'TRABALHISTA'){
            $this->db->where('db.tipo_de_divida like "%MULTA TRABALHISTA%" ');
        }
        else if($this->getFiltro() == 'ELEITORAL'){
            $this->db->where('db.tipo_de_divida like "%ELEITORAL%" ');
        }
        else if($this->getFiltro() == 'CRIMINAL'){
            $this->db->where('db.tipo_de_divida like "%CRIMINAL%" ');
        }
        else if($this->getFiltro() == 'TRIBUTARIOS'){
            $this->db->where('db.tipo_de_divida like "%DEMAIS DÉBITOS TRIBUTÁRIOS%" ');
        }
        else if($this->getFiltro() == 'NAO_TRIBUTARIO'){
            $this->db->where('db.tipo_de_divida like "%DEMAIS DÉBITOS NÃO TRIBUTÁRIOS%" ');
        }

		return $this->db->get('dtb_divida_ativa_detalhada db')->row();
	} 

}