<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Limpa_empresa_model extends CI_Model {
	private $id;
	private $cnpj;
	private $cnpj_completo;

	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getCnpj() {
		return $this->cnpj;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}

	public function getCnpjCompleto() {
		return $this->cnpj_completo;
	}
	public function setCnpjCompleto($cnpj_completo) {
		$this->cnpj_completo = $cnpj_completo;
		return $this;
	}

	public function find_empresa() {
		$this->db->select('*');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_empresas')->row();
	}

	//certidoes
	public function excluir_certidao_sefaz(){
		return $this->db->delete('dtb_certidao_sefaz', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_certidao_barreiras(){
		return $this->db->delete('dtb_certidao_barreiras', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_certidao_caixa(){
		return $this->db->delete('dtb_certidao_caixa', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_certidao_conquista(){
		return $this->db->delete('dtb_certidao_conquista', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_certidao_feira(){
		return $this->db->delete('dtb_certidao_feira', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_certidao_luizeduardo(){
		return $this->db->delete('dtb_certidao_luizeduardo', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_certidao_receita(){
		return $this->db->delete('dtb_certidao_receita', "cnpj_completo = {$this->getCnpj()}");
	}

	public function excluir_certidao_trabalhista(){
		return $this->db->delete('dtb_certidao_trabalhista', "cnpj_completo = {$this->getCnpj()}");
	}
	//fim certidao


	//certificados A1
	public function excluir_certificado(){
		return $this->db->delete('dtb_certificado', "id_empresa = {$this->getId()}");
	}

	public function excluir_contador_procuracao(){
		return $this->db->delete('dtb_contador_procuracao', "id_empresa = {$this->getId()}");
	}
	//fim certificados A1


	//DOCUMENTOS
	public function excluir_documentos(){
		return $this->db->delete('dtb_documentos', "id_empresa = {$this->getId()}");
	}
	//FIM DOCUMENTOS

	//MENSAGENS ECAC
	public function excluir_ecac_caixa_postal_mensagem(){
		$sql = "DELETE FROM dtb_ecac_caixa_postal_mensagem WHERE caixa_postal_id in (SELECT b.id FROM dtb_ecac_caixa_postal b WHERE b.cnpj_data = '".$this->getCnpj()."') ";

		return $this->db->query($sql);
	}

	public function excluir_ecac_caixa_postal(){
		return $this->db->delete('dtb_ecac_caixa_postal', "cnpj_data = {$this->getCnpj()}");
	}
	//FIM MENSAGENS ECAC


	//DAS
	public function excluir_ecac_das(){
		return $this->db->delete('dtb_ecac_das', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_ecac_das_debitos(){
		return $this->db->delete('dtb_ecac_das_debitos', "cnpj = {$this->getCnpj()}");
	}
	//FIM DAS

	//DCTF
	public function excluir_ecac_dctf(){
		return $this->db->delete('dtb_ecac_dctf', "cnpj = {$this->getCnpj()}");
	}
	//FIM DCTF

	//EXCLUSAO SIMPLES
	public function excluir_exclusao_simples(){
		return $this->db->delete('dtb_exclusao_simples', "cnpj = {$this->getCnpj()}");
	}
	//FIM EXCLUSAO SIMPLES


	public function excluir_ipva(){
		return $this->db->delete('dtb_ipva', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_juceb_dados(){
		return $this->db->delete('dtb_juceb_dados', "id_empresa = {$this->getId()}");
	}

	public function excluir_logins_validos(){
		return $this->db->delete('dtb_logins_validos', "id_empresa = {$this->getId()}");
	}

	public function excluir_mensagens_dte(){
		return $this->db->delete('dtb_mensagens_dte', "cnpj = {$this->getCnpj()}");
	}

	public function excluir_situacao_cadin(){
		return $this->db->delete('dtb_situacao_cadin', "cnpj_data = {$this->getCnpj()}");
	}

	public function excluir_situacao_fiscal(){
		return $this->db->delete('dtb_situacao_fiscal', "cnpj_data = {$this->getCnpj()}");
	}

	//cnpj com formatação
	public function excluir_resumofiscal(){
		return $this->db->delete('dtb_resumofiscal', "cnpjcpf = {$this->getCnpj()}");
	}


	public function excluir_parcelas_emitidas(){
		return $this->db->delete('dtb_parcelas_emitidas', "cnpj_data = {$this->getCnpj()}");
	}

	public function excluir_tramitacao_processo_sipro(){
		return $this->db->delete('dtb_tramitacao_processo_sipro', "id_empresa = {$this->getId()}");
	}
}