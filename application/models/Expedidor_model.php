<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Expedidor_model extends CI_Model{

	private $id;
	private $ativo;
	private $cnpj;
	private $razao_social;
	private $nome_fantasia;
	private $email_principal;
	private $email_secundario;
	private $endereco;
	private $numero;
	private $complemento;
	private $bairro;
	private $cep;
	private $cidade;
	private $estado;
	private $id_contabilidade;
	
	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getAtivo() {
		return $this->ativo;
	}
	public function setAtivo($ativo) {
		$this->ativo = $ativo;
		return $this;
	}

	public function getCnpj() {
		return $this->cnpj;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}

	public function getRazaoSocial() {
		return $this->razao_social;
	}
	public function setRazaoSocial($razao_social) {
		$this->razao_social = $razao_social;
		return $this;
	}

	public function getNomeFantasia() {
		return $this->nome_fantasia;
	}
	public function setNomeFantasia($nome_fantasia) {
		$this->nome_fantasia = $nome_fantasia;
		return $this;
	}

	public function getEmailPrincipal() {
		return $this->email_principal;
	}
	public function setEmailPrincipal($email_principal) {
		$this->email_principal = $email_principal;
		return $this;
	}

	public function getEmailSecundario() {
		return $this->email_secundario;
	}
	public function setEmailSecundario($email_secundario) {
		$this->email_secundario = $email_secundario;
		return $this;
	}

	public function getEndereco() {
		return $this->endereco;
	}
	public function setEndereco($endereco) {
		$this->endereco = $endereco;
		return $this;
	}

	public function getNumero() {
		return $this->numero;
	}
	public function setNumero($numero) {
		$this->numero = $numero;
		return $this;
	}

	public function getComplemento() {
		return $this->complemento;
	}
	public function setComplemento($complemento) {
		$this->complemento = $complemento;
		return $this;
	}

	public function getBairro() {
		return $this->bairro;
	}
	public function setBairro($bairro) {
		$this->bairro = $bairro;
		return $this;
	}

	public function getCep() {
		return $this->cep;
	}
	public function setCep($cep) {
		$this->cep = $cep;
		return $this;
	}

	public function getCidade() {
		return $this->cidade;
	}
	public function setCidade($cidade) {
		$this->cidade = $cidade;
		return $this;
	}

	public function getEstado() {
		return $this->estado;
	}
	public function setEstado($estado) {
		$this->estado = $estado;
		return $this;
	}

	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}

	public function cadastrar(){
	
		$dados = array(
	
				'ativo' => $this->getAtivo(),				
				'cnpj' => $this->getCnpj(),
				'razao_social' => $this->getRazaoSocial(),				
				'nome_fantasia' => $this->getNomeFantasia(),
				'email_principal' => $this->getEmailPrincipal(),
				'email_secundario' => $this->getEmailSecundario(),
				'endereco' => $this->getEndereco(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cep' => $this->getCep(),
				'cidade' => $this->getCidade(),
				'estado' => $this->getEstado(),
				'id_contabilidade' => $this->getIdContabilidade()
		);
	
		if ($this->db->insert('dtb_expedidor', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function listar() {
		$this->db->select('id, ativo, cnpj, razao_social, nome_fantasia, email_principal, email_secundario, endereco, numero, complemento, bairro, cep, cidade, estado');
		return $this->db->get('dtb_expedidor')->result();
	}
	
	public function excluir(){
		return $this->db->delete('dtb_expedidor', "id = {$this->getId()}");
	}

	public function pesquisar_expedidor_id() {
		$this->db->select('id, ativo, cnpj, razao_social, nome_fantasia, email_principal, email_secundario, endereco, numero, complemento, bairro, cep, cidade, estado');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_expedidor')->row();
	}

	public function editar(){
	
		$dados = array(
				'ativo' => $this->getAtivo(),				
				'cnpj' => $this->getCnpj(),
				'razao_social' => $this->getRazaoSocial(),				
				'nome_fantasia' => $this->getNomeFantasia(),
				'email_principal' => $this->getEmailPrincipal(),
				'email_secundario' => $this->getEmailSecundario(),
				'endereco' => $this->getEndereco(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cep' => $this->getCep(),
				'cidade' => $this->getCidade(),
				'estado' => $this->getEstado()
		);
	
		if ($this->db->update('dtb_expedidor', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	
	
	}
}
