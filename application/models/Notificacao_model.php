<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacao_model extends CI_Model {

	///////////////////////////// NOTIFICAÇÕES ESTADUAIS //////////////////////////////////////////

	public function estaduais(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		// SITUAÇÃO FISCAL
		$sql1_colaborador = "SELECT n.id, n.id_empresa as id_empresa, n.razao_social as razao_social, n.situacao as descricao, n.data_atualizacao as data, 0 as cnpjstring, 1 as tipo 
							FROM notificacoes as n
							JOIN dtb_empresas as e ON n.id_empresa = e.id 
					   		WHERE n.status = 1 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql1_admin = "SELECT n.id, n.id_empresa as id_empresa, n.razao_social as razao_social, n.situacao as descricao, n.data_atualizacao as data, 0 as cnpjstring, 1 as tipo FROM notificacoes as n 
					   WHERE status = 1 GROUP BY n.id";

		// RESUMO FISCAL
		$sql2_colaborador = "SELECT n.id, 0 as id_empresa, n.razaoSocial as razao_social, n.descricao as descricao, n.data_atualizacao as data, n.cnpjstring as cnpjstring, 2 as tipo 
							FROM notificacoesresumofiscal as n 
							JOIN dtb_empresas as e ON trim(n.cnpjstring) = trim(e.cnpj_completo) 
							WHERE status = 1 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql2_admin = "SELECT n.id, 0 as id_empresa, n.razaoSocial as razao_social, n.descricao as descricao, n.data_atualizacao as data, n.cnpjstring as cnpjstring, 2 as tipo FROM notificacoesresumofiscal as n WHERE n.status = 1 GROUP BY n.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT * FROM (". $sql1_colaborador." UNION ".$sql2_colaborador.") as resultado ORDER BY data desc";
		}else{
			$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin.") as resultado ORDER BY data desc";
		}

		return $this->db->query($sql)->result();
	}

	///////////////////////////// Notificações Federais //////////////////////////////////////////
	public function federais(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//SITUAÇÃO ECAC
		$sql1_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 1 as tipo  
							 FROM dtb_notificacao_ecac_situacao as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql1_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 1 as tipo  
							 FROM dtb_notificacao_ecac_situacao as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";


		//SITUAÇÃO CADIN
		$sql2_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 2 as tipo  
							 FROM dtb_notificacao_ecac_cadin as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql2_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 2 as tipo   
							 FROM dtb_notificacao_ecac_cadin as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DCTF PROX VENCER
		$sql3_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 3 as tipo  
							 FROM dtb_notificacao_dctf_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql3_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 3 as tipo   
							 FROM dtb_notificacao_dctf_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DCTF VENCIDAS
		$sql4_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 4 as tipo  
							 FROM dtb_notificacao_dctf_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql4_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 4 as tipo   
							 FROM dtb_notificacao_dctf_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DCTF SEM MOVIMENTO
		$sql5_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 5 as tipo  
							 FROM dtb_notificacao_dctf_sem_movimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql5_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 5 as tipo   
							 FROM dtb_notificacao_dctf_sem_movimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";


		//PGDAS VENCIDAS
		$sql6_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 6 as tipo  
							 FROM dtb_notificacao_pgdas_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql6_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 6 as tipo   
							 FROM dtb_notificacao_pgdas_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";


		//PGDAS proximo vencer
		$sql7_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 7 as tipo  
							 FROM dtb_notificacao_pgdas_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql7_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 7 as tipo   
							 FROM dtb_notificacao_pgdas_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DAS não pagos
		$sql8_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 8 as tipo  
							 FROM dtb_notificacao_das_nao_pagos as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql8_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 8 as tipo   
							 FROM dtb_notificacao_das_nao_pagos as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//SUBLIMITE SIMPLES
		$sql9_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 9 as tipo  
							 FROM dtb_notificacao_sublimite_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql9_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 9 as tipo   
							 FROM dtb_notificacao_sublimite_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DIVERGENCIA GFIP X GPS
		$sql10_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 10 as tipo  
							 FROM dtb_notificacao_gfip_pendencia as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql10_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 10 as tipo   
							 FROM dtb_notificacao_gfip_pendencia as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//MALHA FISCAL
		$sql11_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 11 as tipo  
							 FROM dtb_notificacao_malha_fiscal as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql11_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 11 as tipo   
							 FROM dtb_notificacao_malha_fiscal as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";


		//EXCLUIDAS SIMPLES
		$sql12_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 12 as tipo  
							 FROM dtb_notificacao_empresas_excluidas_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql12_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 12 as tipo   
							 FROM dtb_notificacao_empresas_excluidas_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//EXCLUIDAS MEI
		$sql13_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 13 as tipo  
							 FROM dtb_notificacao_empresas_excluidas_mei as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql13_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 13 as tipo   
							 FROM dtb_notificacao_empresas_excluidas_mei as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DCTF TRANSMITIDA
		// $sql6_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 5 as tipo  
		// 					 FROM dtb_notificacao_dctf_sem_movimento as n 
		// 					 LEFT JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
		// 					 LEFT JOIN dtb_empresa_usuario as eu ON e.id = eu.id_empresa 
		// 					 WHERE lida = 0 AND (e.id_funcionario = ".$id_colaborador." OR eu.id_usuario = ".$id_colaborador.") GROUP BY n.id";

		// $sql6_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 5 as tipo   
		// 					 FROM dtb_notificacao_dctf_sem_movimento as n 
		// 					 LEFT JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
		// 					 WHERE lida = 0 GROUP BY n.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT * FROM (". $sql1_colaborador." UNION ".$sql2_colaborador." UNION ".$sql3_colaborador." UNION ".$sql4_colaborador." UNION ".$sql5_colaborador." UNION ".$sql6_colaborador." UNION ".$sql7_colaborador." UNION ".$sql8_colaborador." UNION ".$sql9_colaborador." UNION ".$sql10_colaborador." UNION ".$sql11_colaborador." UNION ".$sql12_colaborador." UNION ".$sql13_colaborador.") as resultado ORDER BY data desc";
		}else{
			$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin." UNION ".$sql3_admin." UNION ".$sql4_admin." UNION ".$sql5_admin." UNION ".$sql6_admin." UNION ".$sql7_admin." UNION ".$sql8_admin." UNION ".$sql9_admin." UNION ".$sql10_admin." UNION ".$sql11_admin." UNION ".$sql12_admin." UNION ".$sql13_admin.") as resultado ORDER BY data desc";
		}

		return $this->db->query($sql)->result();

	}

	public function qtd_federais(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//SITUAÇÃO ECAC
		$sql1_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 1 as tipo  
							 FROM dtb_notificacao_ecac_situacao as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql1_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 1 as tipo  
							 FROM dtb_notificacao_ecac_situacao as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";


		//SITUAÇÃO CADIN
		$sql2_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 2 as tipo  
							 FROM dtb_notificacao_ecac_cadin as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql2_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 2 as tipo   
							 FROM dtb_notificacao_ecac_cadin as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DCTF PROX VENCER
		$sql3_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 3 as tipo  
							 FROM dtb_notificacao_dctf_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql3_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 3 as tipo   
							 FROM dtb_notificacao_dctf_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DCTF VENCIDAS
		$sql4_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 4 as tipo  
							 FROM dtb_notificacao_dctf_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql4_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 4 as tipo   
							 FROM dtb_notificacao_dctf_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DCTF SEM MOVIMENTO
		$sql5_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 5 as tipo  
							 FROM dtb_notificacao_dctf_sem_movimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql5_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 5 as tipo   
							 FROM dtb_notificacao_dctf_sem_movimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//PGDAS VENCIDAS
		$sql6_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 6 as tipo  
							 FROM dtb_notificacao_pgdas_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql6_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 6 as tipo   
							 FROM dtb_notificacao_pgdas_nao_entregue as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";


		//PGDAS proximo vencer
		$sql7_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 7 as tipo  
							 FROM dtb_notificacao_pgdas_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql7_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 7 as tipo   
							 FROM dtb_notificacao_pgdas_proxima_vencimento as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//DAS não pagos
		$sql8_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 8 as tipo  
							 FROM dtb_notificacao_das_nao_pagos as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql8_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.mes as descricao, 8 as tipo   
							 FROM dtb_notificacao_das_nao_pagos as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//SUBLIMITE SIMPLES
		$sql9_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 9 as tipo  
							 FROM dtb_notificacao_sublimite_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql9_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 9 as tipo   
							 FROM dtb_notificacao_sublimite_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";
        
        //DIVERGENCIA GFIP X GPS
		$sql10_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 10 as tipo  
							 FROM dtb_notificacao_gfip_pendencia as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql10_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 10 as tipo   
							 FROM dtb_notificacao_gfip_pendencia as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";
        
        //MALHA FISCAL
		$sql11_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 11 as tipo  
							 FROM dtb_notificacao_malha_fiscal as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql11_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 11 as tipo   
							 FROM dtb_notificacao_malha_fiscal as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//EXCLUIDAS SIMPLES
		$sql12_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 12 as tipo  
							 FROM dtb_notificacao_empresas_excluidas_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql12_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 12 as tipo   
							 FROM dtb_notificacao_empresas_excluidas_simples as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		//EXCLUIDAS MEI
		$sql13_colaborador = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 13 as tipo  
							 FROM dtb_notificacao_empresas_excluidas_mei as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY n.id";

		$sql13_admin = "SELECT n.cnpj, n.id, e.razao_social, n.data, n.descricao, 13 as tipo   
							 FROM dtb_notificacao_empresas_excluidas_mei as n 
							 JOIN dtb_empresas as e ON trim(n.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY n.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT count(*) as valor FROM (". $sql1_colaborador." UNION ".$sql2_colaborador." UNION ".$sql3_colaborador." UNION ".$sql4_colaborador." UNION ".$sql5_colaborador." UNION ".$sql6_colaborador." UNION ".$sql7_colaborador." UNION ".$sql8_colaborador." UNION ".$sql9_colaborador." UNION ".$sql10_colaborador." UNION ".$sql11_colaborador." UNION ".$sql12_colaborador." UNION ".$sql13_colaborador.") as resultado ORDER BY data desc";
		}else{
			$sql = "SELECT count(*) as valor FROM (".$sql1_admin." UNION ".$sql2_admin." UNION ".$sql3_admin." UNION ".$sql4_admin." UNION ".$sql5_admin." UNION ".$sql6_admin." UNION ".$sql7_admin." UNION ".$sql8_admin." UNION ".$sql9_admin." UNION ".$sql10_admin." UNION ".$sql11_admin." UNION ".$sql12_admin." UNION ".$sql13_admin.") as resultado ORDER BY data desc";
		}

		return $this->db->query($sql)->row();
		
	}

	public function marcar_situacao_ecac_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_ecac_situacao');
	}

	public function marcar_situacao_cadin_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_ecac_cadin');
	}

	public function marcar_dctf_vencida_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_dctf_nao_entregue');
	}


	public function marcar_dctf_proxima_vencer_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_dctf_proxima_vencimento');
	}


	public function marcar_dctf_sem_movimento_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_dctf_sem_movimento');
	}

	public function marcar_pgdas_vencida_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_pgdas_nao_entregue');
	}

	public function marcar_dpgdas_proxima_vencer_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_pgdas_proxima_vencimento');
	}

	public function marcar_das_nao_pago_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_das_nao_pagos');
	}

	public function marcar_sublimite_simples_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_sublimite_simples');
	}

	public function marcar_gfip_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_gfip_pendencia');
	}

	public function marcar_malha_fiscal_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_malha_fiscal');
	}

	public function marcar_excluida_simples_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_empresas_excluidas_simples');
	}

	public function marcar_excluida_mei_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_empresas_excluidas_mei');
	}

	///////////////////////////// MENSAGENS //////////////////////////////////////////

	public function mensagens(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";


		//MENSAGENS DEC
		$sql1_colaborador = "SELECT '' as id, m.cnpj, e.razao_social, e.id as id_empresa, now() as dataemail, m.qtd_nao_lida as nao_lidas, 1 as tipo, '' as codigo_tipo    
							 FROM dtb_dec_qtd_mensagem as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE m.qtd_nao_lida != 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) GROUP BY m.cnpj";

		$sql1_admin = "SELECT '' as id, m.cnpj, e.razao_social, e.id as id_empresa, now() as dataemail,  m.qtd_nao_lida as nao_lidas, 1 as tipo, ''  as codigo_tipo     
							 FROM dtb_dec_qtd_mensagem as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
							 WHERE m.qtd_nao_lida != 0 GROUP BY m.cnpj";
							 
		//MENSAGENS DEC
		// $sql1_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 0 as nao_lidas, 1 as tipo, m.categoria as codigo_tipo    
		// 					 FROM dtb_dec_caixapostal_mensagens as m 
		// 					 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
		// 					 WHERE m.lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		// $sql1_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail,  0 as nao_lidas, 1 as tipo, m.categoria  as codigo_tipo     
		// 					 FROM dtb_dec_caixapostal_mensagens as m 
		// 					 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
		// 					 WHERE m.lida = 0 GROUP BY m.id";




		//MENSAGEM DEC COMUNICADOS
		// $sql3_colaborador = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail, 0 as nao_lidas, 3 as tipo, 'Comunicado' as codigo_tipo    
		// 					 FROM dtb_dec_caixapostal_comunicados as m 
		// 					 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
		// 					 WHERE m.lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		// $sql3_admin = "SELECT m.id, m.cnpj, e.razao_social, e.id as id_empresa, DATE_FORMAT(STR_TO_DATE(m.data_envio, '%d/%m/%Y'), '%Y-%m-%d') as dataemail,  0 as nao_lidas, 3 as tipo, 'Comunicado' as codigo_tipo     
		// 					 FROM dtb_dec_caixapostal_comunicados as m 
		// 					 LEFT JOIN dtb_empresas as e ON trim(m.cnpj) = trim(e.cnpj) 
		// 					 WHERE m.lida = 0 GROUP BY m.id";


		//MENSAGEM ECAC
		$sql2_colaborador = "SELECT m.id, m.cnpj_data as cnpj, e.razao_social, e.id as id_empresa, m.data_execucao as dataemail, m.nao_lidas, 2 as tipo, '' as codigo_tipo   
							 FROM dtb_ecac_caixa_postal as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE nao_lidas > 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		$sql2_admin = "SELECT m.id, m.cnpj_data as cnpj, e.razao_social, e.id as id_empresa, m.data_execucao as dataemail, m.nao_lidas, 2 as tipo, '' as codigo_tipo     
							 FROM dtb_ecac_caixa_postal as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE nao_lidas > 0 GROUP BY m.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT * FROM (". $sql1_colaborador." UNION ".$sql2_colaborador.") as resultado ORDER BY dataemail desc";
		}else{
			$sql = "SELECT * FROM (".$sql1_admin." UNION ".$sql2_admin.") as resultado ORDER BY dataemail desc";
		}

		return $this->db->query($sql)->result();
		
	}


	public function qtd_mensagens(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//MENSAGEM DEC 
		$sql1_colaborador = "SELECT count(*) as total 
							 FROM dtb_dec_caixapostal_mensagens as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE m.lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		$sql1_admin = "SELECT count(*) as total 
							 FROM dtb_dec_caixapostal_mensagens as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE m.lida = 0 GROUP BY m.id";

		//MENSAGEM DEC COMUNICADOS 
		$sql3_colaborador = "SELECT count(*) as total 
							 FROM dtb_dec_caixapostal_comunicados as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE m.lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		$sql3_admin = "SELECT count(*) as total 
							 FROM dtb_dec_caixapostal_comunicados as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE m.lida = 0 GROUP BY m.id";


		//MENSAGEM ECAC
		$sql2_colaborador = "SELECT SUM(m.nao_lidas) as total   
							 FROM dtb_ecac_caixa_postal as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE nao_lidas > 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		$sql2_admin = "SELECT SUM(m.nao_lidas) as total  
							 FROM dtb_ecac_caixa_postal as m 
							 JOIN dtb_empresas as e ON trim(m.cnpj_data) = trim(e.cnpj) 
							 WHERE nao_lidas > 0 GROUP BY m.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT SUM(total) as total FROM (". $sql1_colaborador." UNION ".$sql2_colaborador." UNION ".$sql3_colaborador.") as resultado ";
		}else{
			$sql = "SELECT SUM(total) as total FROM (".$sql1_admin." UNION ".$sql2_admin." UNION ".$sql3_admin.") as resultado ";
		}

		return $this->db->query($sql)->row();
		
	}


	public function marcar_msg_dte_lida($id_notificacao){
		$this->db->set('lida', 1);
		$this->db->where('id', $id_notificacao);
		$this->db->update('dtb_dec_caixapostal_mensagens');
	}

	public function marcar_msg_dte_lida2($id_notificacao){
		$this->db->set('lida', 1);
		$this->db->where('id', $id_notificacao);
		$this->db->update('dtb_dec_caixapostal_comunicados');
	}

	public function marcar_msg_ecac_lida($id_notificacao){
		$this->db->set('nao_lidas', 0);
		$this->db->where('id', $id_notificacao);
		$this->db->update('dtb_ecac_caixa_postal');
	}

	///////////////////////////// CERTIDÕES //////////////////////////////////////////
	//CADA NOVA CERTIDÃO IMPLICA EM MUDANÇA NESTE METODO E DA CRIAÇÃO DO METODO DE MARCAR COMO LIDA
	public function certidoes(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//CERTIDÃO FEDERAL
		$sql1_colaborador = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 1 as tipo 
							 FROM dtb_notificacao_certidao_receita as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj_completo) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql1_admin = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 1 as tipo 
							 FROM dtb_notificacao_certidao_receita as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj_completo) 
							 WHERE lida = 0 GROUP BY c.id";


		//CERTIDAO SEFAZ SP
		$sql2_colaborador = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 2 as tipo 
							 FROM dtb_notificacao_certidao_sefaz_sp as c 
							 LEFT JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql2_admin = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 2 as tipo 
							 FROM dtb_notificacao_certidao_sefaz_sp as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)
							 WHERE lida = 0 GROUP BY c.id";

		//CERTIDAO SAO PAULO
		$sql3_colaborador = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 3 as tipo 
							 FROM dtb_notificacao_certidao_municipio_sp as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql3_admin = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 3 as tipo 
							 FROM dtb_notificacao_certidao_municipio_sp as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)
							 WHERE lida = 0 GROUP BY c.id";


		 //CERTIDÃO CAIXA
		$sql4_colaborador = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 7 as tipo 
							 FROM dtb_notificacao_certidao_caixa as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql4_admin = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 7 as tipo 
							 FROM dtb_notificacao_certidao_caixa as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY c.id";


		//CERTIDÃO TRABALHISTA
		$sql5_colaborador = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 8 as tipo 
							 FROM dtb_notificacao_certidao_trabalhista as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql5_admin = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 8 as tipo 
							 FROM dtb_notificacao_certidao_trabalhista as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj) 
							 WHERE lida = 0 GROUP BY c.id";



		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT * FROM (". $sql1_colaborador." UNION ".$sql2_colaborador." UNION ".$sql3_colaborador." UNION ".$sql4_colaborador." UNION ".$sql5_colaborador.") as resultado ORDER BY data desc";
		}else{
			$sql = "SELECT * FROM (". $sql1_admin." UNION ".$sql2_admin." UNION ".$sql3_admin." UNION ".$sql4_admin." UNION ".$sql5_admin.") as resultado ORDER BY data desc";
		}

		return $this->db->query($sql)->result();
	}

	public function marcar_notificacao_certidao_federal($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_certidao_receita');
	}

	public function marcar_notificacao_certidao_estadual($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_certidao_sefaz_sp');
	}

	public function marcar_notificacao_certidao_sao_paulo($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_certidao_municipio_sp');
	}

	public function marcar_notificacao_certidao_caixa($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_certidao_caixa');
	}

	public function marcar_notificacao_certidao_trabalhista($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_certidao_trabalhista');
	}
	

	
	///////////////////////////// PROCESSOS //////////////////////////////////////////
	public function processos(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//SIPRO
		$sql1_colaborador = "SELECT c.id, c.id_empresa, e.cnpj,  e.razao_social, c.data_atualizacao as data, '' as descricao, 1 as tipo 
							 FROM notificacoesprocessosipro as c 
							 JOIN dtb_empresas as e ON c.id_empresa = e.id 
							 WHERE c.status = 1 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql1_admin = "SELECT c.id, c.id_empresa, e.cnpj, e.razao_social, c.data_atualizacao as data, '' as descricao, 1 as tipo 
							 FROM notificacoesprocessosipro as c 
							 JOIN dtb_empresas as e ON c.id_empresa = e.id 
							 WHERE c.status = 1 GROUP BY c.id";


	    $hostCompleto = $_SERVER['HTTP_HOST'];
	    $server = explode('.', $hostCompleto);
	    $server = $server[0];

	    $banco = $server;

	    if($banco == 'demo'){ 
	    

    	// E-processos ativos
		$sql2_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  2 as tipo 
							 FROM dtb_notificacao_ecac_eprocessos_ativos as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql2_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  2 as tipo 
							 FROM dtb_notificacao_ecac_eprocessos_ativos as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

		//E-processos inativos
		$sql3_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  3 as tipo 
							 FROM dtb_notificacao_ecac_eprocessos_inativos as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql3_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  3 as tipo 
							 FROM dtb_notificacao_ecac_eprocessos_inativos as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";
    	}

		//E-processos ativos
		// $sql2_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  2 as tipo 
		// 					 FROM dtb_notificacao_ecac_eprocessos_ativos as c 
		// 					 LEFT JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj) 
		// 					 LEFT JOIN dtb_empresa_usuario as eu ON e.id = eu.id_empresa  
		// 					 WHERE lida = 0 AND (e.id_funcionario = ".$id_colaborador." OR eu.id_usuario = ".$id_colaborador.") GROUP BY c.id";

		// $sql2_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  2 as tipo 
		// 					 FROM dtb_notificacao_ecac_eprocessos_ativos as c 
		// 					 LEFT JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
		// 					 WHERE lida = 0 GROUP BY c.id";

		// //E-processos inativos
		// $sql3_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  3 as tipo 
		// 					 FROM dtb_notificacao_ecac_eprocessos_inativos as c 
		// 					 LEFT JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj) 
		// 					 LEFT JOIN dtb_empresa_usuario as eu ON e.id = eu.id_empresa  
		// 					 WHERE lida = 0 AND (e.id_funcionario = ".$id_colaborador." OR eu.id_usuario = ".$id_colaborador.") GROUP BY c.id";

		// $sql3_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  3 as tipo 
		// 					 FROM dtb_notificacao_ecac_eprocessos_inativos as c 
		// 					 LEFT JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
		// 					 WHERE lida = 0 GROUP BY c.id";

		//JUCEB
		// $sql4_colaborador = "SELECT c.id, e.id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  3 as tipo
		// 					 FROM dtb_notificacao_juceb_viabilidade as c 
		// 					 LEFT JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj) 
		// 					 LEFT JOIN dtb_empresa_usuario as eu ON e.id = eu.id_empresa  
		// 					 WHERE lida = 0 AND (e.id_funcionario = ".$id_colaborador." OR eu.id_usuario = ".$id_colaborador.") GROUP BY c.id";

		// $sql4_admin = "SELECT c.id, c.cnpj, e.razao_social, c.descricao, c.data, 4 as tipo 
		// 					 FROM dtb_notificacao_juceb_viabilidade as c 
		// 					 LEFT JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)
		// 					 WHERE lida = 0 GROUP BY c.id";


		if ($this->session->userdata['userprimesession']['nivel']==2){
			// $sql = "SELECT * FROM (". $sql1_colaborador." UNION ".$sql2_colaborador." UNION ".$sql3_colaborador.") as resultado ORDER BY data desc";
			$sql = "SELECT * FROM (". $sql1_colaborador.") as resultado ORDER BY data desc";
		}else{
			// $sql = "SELECT * FROM (". $sql1_admin." UNION ".$sql2_admin." UNION ".$sql3_admin.") as resultado ORDER BY data desc";
			$sql = "SELECT * FROM (". $sql1_admin.") as resultado ORDER BY data desc";
		}


		if ($banco == 'demo'){
			$sql = "SELECT * FROM (". $sql1_admin." UNION ".$sql2_admin." UNION ".$sql3_admin.") as resultado ORDER BY data desc";
		}

		return $this->db->query($sql)->result();
	}

	public function marcar_eprocesso_ativo_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_ecac_eprocessos_ativos');
	}

	public function marcar_eprocesso_inativo_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_ecac_eprocessos_inativos');
	}


	///////////////////Calendario/////////
	public function calendario(){
		$this->db->select('eventos_criados.id, eventos_criados.nome as title, eventos_criados.datainicio as start, eventos_criados.datafim as end, eventos_criados.cor as backgroundColor, eventos_criados.url as url, eventos_criados.description, eventos_criados.id_evento, eventos_criados.observacao, eventos_criados.id_evento as id_evento, DATEDIFF(eventos_criados.datafim, CURRENT_DATE) AS dias_vencimento');
		$this->db->from('eventos_criados');
		$this->db->where('DATEDIFF(eventos_criados.datafim, CURRENT_DATE) <= 5');
		$this->db->where('DATEDIFF(eventos_criados.datafim, CURRENT_DATE) >= 0');
        if ( isset($this->session->userdata['userprimesession']) ){
            if($this->session->userdata['userprimesession']['nivel'] == 2){
                $this->db->join('dtb_usuarios u', 'eventos_criados.id_usuario = u.id', 'left');
                $this->db->group_start();
                $this->db->where('eventos_criados.id_usuario', $this->session->userdata['userprimesession']['id']);
                $this->db->or_where('u.nivel != 2');
                $this->db->group_end();

            }
        }


		return $this->db->get()->result();
	}


	/////////////////////////////////////ATUALIZAÇÕES/////////////////////////////////
	public function atualizacoes(){
		$sql = "SELECT c.id, c.descricao, c.link_download, c.data 
							 FROM dtb_notificacao_atualizacoes as c 
							 WHERE c.lida = 0 GROUP BY c.id 
							 ORDER BY data desc ";

		return $this->db->query($sql)->result();
	}

	public function marcar_atualizacoes_lida($id){
		$this->db->set('lida', 1);
		$this->db->where('id', $id);
		$this->db->update('dtb_notificacao_atualizacoes');
	}

    ///////////////////////////// PARCELAMENTOS //////////////////////////////////////////
    public function parcelamentos(){
        $id_colaborador = $this->session->userdata['userprimesession']['id'];


        //Parcelamentos Das
        $sql1_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  1 as tipo 
							 FROM dtb_notificacao_parcelamento_das_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql1_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  1 as tipo 
							 FROM dtb_notificacao_parcelamento_das_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql2_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  2 as tipo 
							 FROM dtb_notificacao_parcelamento_das_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql2_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  2 as tipo 
							 FROM dtb_notificacao_parcelamento_das_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        //Parcelamentos PERT-SN
        $sql3_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  3 as tipo 
							 FROM dtb_notificacao_parcelamento_pertsn_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql3_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  3 as tipo 
							 FROM dtb_notificacao_parcelamento_pertsn_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql4_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  4 as tipo 
							 FROM dtb_notificacao_parcelamento_pertsn_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql4_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  4 as tipo 
							 FROM dtb_notificacao_parcelamento_pertsn_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        //Parcelamentos Nao Previdenciario
        $sql5_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  5 as tipo 
							 FROM dtb_notificacao_parcelamento_nao_previdenciario_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql5_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  5 as tipo 
							 FROM dtb_notificacao_parcelamento_nao_previdenciario_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql6_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  6 as tipo 
							 FROM dtb_notificacao_parcelamento_nao_previdenciario_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql6_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  6 as tipo 
							 FROM dtb_notificacao_parcelamento_nao_previdenciario_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

		$sql7_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  7 as tipo 
							FROM dtb_notificacao_parcelamento_nao_previdenciario_em_atraso as c 
							JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

		$sql7_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  7 as tipo 
							FROM dtb_notificacao_parcelamento_nao_previdenciario_em_atraso as c 
							JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							WHERE lida = 0 GROUP BY c.id";

        //Parcelamentos lei 12996
        $sql8_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  8 as tipo 
							 FROM dtb_notificacao_parcelamento_lei_12996_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql8_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  8 as tipo 
							 FROM dtb_notificacao_parcelamento_lei_12996_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql9_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  9 as tipo 
							 FROM dtb_notificacao_parcelamento_lei_12996_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql9_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  9 as tipo 
							 FROM dtb_notificacao_parcelamento_lei_12996_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql10_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  10 as tipo 
							 FROM dtb_notificacao_parcelamento_lei_12996_em_atraso as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql10_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  10 as tipo 
							 FROM dtb_notificacao_parcelamento_lei_12996_em_atraso as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

		//Parcelamentos pert rfb
        $sql11_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  11 as tipo 
							 FROM dtb_notificacao_parcelamento_pert_rfb_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql11_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  11 as tipo 
							 FROM dtb_notificacao_parcelamento_pert_rfb_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql12_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  12 as tipo 
							 FROM dtb_notificacao_parcelamento_pert_rfb_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql12_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  12 as tipo 
							 FROM dtb_notificacao_parcelamento_pert_rfb_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql13_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  13 as tipo 
							 FROM dtb_notificacao_parcelamento_pert_rfb_em_atraso as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql13_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  13 as tipo 
							 FROM dtb_notificacao_parcelamento_pert_rfb_em_atraso as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";
		
		//Parcelamentos mei
        $sql14_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  14 as tipo 
							 FROM dtb_notificacao_parcelamento_mei_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql14_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  14 as tipo 
							 FROM dtb_notificacao_parcelamento_mei_status as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql15_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  15 as tipo 
							 FROM dtb_notificacao_parcelamento_mei_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql15_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  15 as tipo 
							 FROM dtb_notificacao_parcelamento_mei_pago as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";

        $sql16_colaborador = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  16 as tipo 
							 FROM dtb_notificacao_parcelamento_mei_em_atraso as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)   
							 WHERE lida = 0 AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY c.id";

        $sql16_admin = "SELECT c.id, e.id as id_empresa, c.cnpj, e.razao_social, c.data, c.descricao,  16 as tipo 
							 FROM dtb_notificacao_parcelamento_mei_em_atraso as c 
							 JOIN dtb_empresas as e ON trim(c.cnpj) = trim(e.cnpj)  
							 WHERE lida = 0 GROUP BY c.id";


        if ($this->session->userdata['userprimesession']['nivel']==2){
            $sql = "SELECT * FROM ({$sql1_colaborador} UNION {$sql2_colaborador} UNION {$sql3_colaborador} UNION {$sql4_colaborador} 
            UNION {$sql5_colaborador} UNION {$sql6_colaborador} UNION {$sql7_colaborador} UNION {$sql8_colaborador} UNION {$sql9_colaborador}
			UNION {$sql10_colaborador} UNION {$sql11_colaborador} UNION {$sql12_colaborador} UNION {$sql13_colaborador} UNION {$sql14_colaborador}
			UNION {$sql15_colaborador} UNION {$sql16_colaborador}) as resultado ORDER BY data desc";
        }else{
            $sql = "SELECT * FROM ({$sql1_admin} UNION {$sql2_admin} UNION {$sql3_admin} UNION {$sql4_admin} UNION
            {$sql5_admin} UNION {$sql6_admin} UNION {$sql7_admin} UNION {$sql8_admin} UNION {$sql9_admin}  UNION {$sql10_admin}
			UNION {$sql11_admin}   UNION {$sql12_admin}  UNION {$sql13_admin}  UNION {$sql14_admin}  UNION {$sql15_admin}  UNION {$sql16_admin}) as resultado ORDER BY data desc";

        }

        return $this->db->query($sql)->result();
    }

    public function marcar_parcelamento_das_pago_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_das_pago');
    }

    public function marcar_parcelamento_das_status_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_das_status');
    }

    public function marcar_parcelamento_pertsn_pago_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_pertsn_pago');
    }

    public function marcar_parcelamento_pertsn_status_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_pertsn_status');
    }

    public function marcar_parcelamento_nao_previdenciario_pago_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_nao_previdenciario_pago');
    }

	public function marcar_parcelamento_nao_previdenciario_em_atraso_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_nao_previdenciario_em_atraso');
    }

    public function marcar_parcelamento_nao_previdenciario_status_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_nao_previdenciario_status');
    }

    public function marcar_parcelamento_lei_12996_pago_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_lei_12996_pago');
    }

	public function marcar_parcelamento_lei_12996_em_atraso_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_lei_12996_em_atraso');
    }

    public function marcar_parcelamento_lei_12996_status_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_pert_rfb_status');
    }

	public function marcar_parcelamento_pert_rfb_pago_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_pert_rfb_pago');
    }

	public function marcar_parcelamento_pert_rfb_em_atraso_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_pert_rfb_em_atraso');
    }

    public function marcar_parcelamento_pert_rfb_status_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_pert_rfb_status');
    }

	public function marcar_parcelamento_mei_pago_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_mei_pago');
    }

	public function marcar_parcelamento_mei_em_atraso_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_mei_em_atraso');
    }

    public function marcar_parcelamento_mei_status_lida($id){
        $this->db->set('lida', 1);
        $this->db->where('id', $id);
        $this->db->update('dtb_notificacao_parcelamento_mei_status');
    }
}