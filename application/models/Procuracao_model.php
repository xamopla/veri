<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Procuracao_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}

	// public function listar() {
	// 	$this->db->select('dtb_ecac_procuracao.cnpj as cnpj, dtb_ecac_procuracao.cnpj_outorgante, dtb_ecac_procuracao.nome_outorgante, dtb_ecac_procuracao.data_inicio, dtb_ecac_procuracao.data_fim, dtb_ecac_procuracao.situacao, e.cnpj_completo, e.razao_social');

	// 	$this->db->join('dtb_empresas as e', 'dtb_ecac_procuracao.cnpj_outorgante = e.cnpj', 'left');

	// 	if ($this->session->userdata['userprimesession']['nivel'] == 2){

	// 		$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			
	// 		$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
	// 		$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			
	// 	}

	// 	$this->db->group_by('dtb_ecac_procuracao.cnpj_outorgante');
	// 	return $this->db->get('dtb_ecac_procuracao')->result();

	// }

	public function listar(){
		$sql = "SELECT * FROM dtb_ecac_procuracao WHERE id in (SELECT max(id) FROM dtb_ecac_procuracao group by cnpj_outorgante)";
		return $this->db->query($sql)->result();
	}

	public function buscar_ultima_proc($cnpj){
		$sql = "SELECT *, DATE_FORMAT(STR_TO_DATE(data_fim, '%d/%m/%Y'), '%Y-%m-%d') as data_or FROM dtb_ecac_procuracao WHERE cnpj_outorgante = '".$cnpj."' order by DATE_FORMAT(STR_TO_DATE(data_fim, '%d/%m/%Y'), '%Y-%m-%d') desc LIMIT 1";
		return $this->db->query($sql)->row();
	}


	public function empresas_cadastradas(){
		$this->db->select('e.cnpj as cnpj');
		if ($this->session->userdata['userprimesession']['nivel'] == 2){

			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
			
		}
		return $this->db->get('dtb_empresas as e')->result();
	}

}