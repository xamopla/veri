<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Juceb_model extends CI_Model {

	private $id;
	private $protocolo;
	private $nome_empresa_mostrar;
	private $nome_da_empresa;
	private $estado_do_processo;
	private $status_de_envio;
	private $data_atualizacao;
	private $data_incorporacao;
	private $id_contabilidade;
	private $data_cadastro;
	private $id_cadastro;
	
	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getProtocolo() {
		return $this->protocolo;
	}
	public function setProtocolo($protocolo) {
		$this->protocolo = $protocolo;
		return $this;
	}

	public function getNomeEmpresaMostrar() {
		return $this->nome_empresa_mostrar;
	}
	public function setNomeEmpresaMostrar($nome_empresa_mostrar) {
		$this->nome_empresa_mostrar = $nome_empresa_mostrar;
		return $this;
	}

	public function getNomeDaEmpresa() {
		return $this->nome_da_empresa;
	}
	public function setNomeDaEmpresa($nome_da_empresa) {
		$this->nome_da_empresa = $nome_da_empresa;
		return $this;
	}

	public function getEstadoDoProcesso() {
		return $this->estado_do_processo;
	}
	public function setEstadoDoProcesso($estado_do_processo) {
		$this->estado_do_processo = $estado_do_processo;
		return $this;
	}

	public function getStatusDoEnvio() {
		return $this->status_de_envio;
	}
	public function setStatusDoEnvio($status_de_envio) {
		$this->status_de_envio = $status_de_envio;
		return $this;
	}

	public function getDataAtualizacao() {
		return $this->data_atualizacao;
	}
	public function setDataAtualizacao($data_atualizacao) {
		$this->data_atualizacao = $data_atualizacao;
		return $this;
	}

	public function getDataIncorporacao() {
		return $this->data_incorporacao;
	}
	public function setDataIncorporacao($data_incorporacao) {
		$this->data_incorporacao = $data_incorporacao;
		return $this;
	}

	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}

	public function getIdCadastro() {
		return $this->id_cadastro;
	}
	public function setIdCadastro($id_cadastro) {
		$this->id_cadastro = $id_cadastro;
		return $this;
	}

	public function getDataCadastro() {
		return $this->data_cadastro;
	}
	public function setDataCadastro($data_cadastro) {
		$this->data_cadastro = $data_cadastro;
		return $this;
	}


	public function cadastrar(){
	
		$dados = array(
				'protocolo' => $this->getProtocolo(),
				'nome_da_empresa' => $this->getNomeDaEmpresa(),
				'estado_do_processo' => $this->getEstadoDoProcesso(),
				'status_de_envio' => $this->getStatusDoEnvio(),
				'data_atualizacao' => $this->getDataAtualizacao(),
				'data_incorporacao' => $this->getDataIncorporacao(),
				'id_contabilidade' => $this->getIdContabilidade()
		);
	
		if ($this->db->insert('dtb_juceb', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	
	
	} 

	public function listar() {
		$this->db->select('id, protocolo, nome_da_empresa, estado_do_processo, status_de_envio, data_atualizacao, data_incorporacao');		
		
		return $this->db->get('dtb_juceb')->result();
	}

	public function excluir(){
		return $this->db->delete('dtb_juceb', "id = {$this->getId()}");
	}

	// -------------------------------------------------- JUCEB DADOS -----------------------------------------------------

	public function listar_dados_juceb(){

		$this->db->select('j.id, j.id_empresa, j.juceb_nome_empresarial, j.juceb_nire, j.juceb_cnpj, j.juceb_situacao, j.juceb_status, j.juceb_natureza_juridica, j.juceb_capital_social, j.juceb_capital_integralizado, j.juceb_data_do_ato_constitutivo, j.juceb_data_inicio_das_atividades, j.juceb_logradouro, j.juceb_complemento, j.juceb_numero, j.juceb_bairro, j.juceb_cep, j.juceb_municipio, j.juceb_objeto_social, j.juceb_historico_data_ultimo_arquivamento, j.juceb_historico_nome_do_evento, j.juceb_historico_numero_arquivamento, j.juceb_historico_descricao_ato, j.juceb_data_evento_expirar, dtbe.cnpj_completo, dtbe.razao_social');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}

	public function filtro_registro_ativo(){

		$this->db->select('j.id, j.id_empresa, j.juceb_nome_empresarial, j.juceb_nire, j.juceb_cnpj, j.juceb_situacao, j.juceb_status, j.juceb_natureza_juridica, j.juceb_capital_social, j.juceb_capital_integralizado, j.juceb_data_do_ato_constitutivo, j.juceb_data_inicio_das_atividades, j.juceb_logradouro, j.juceb_complemento, j.juceb_numero, j.juceb_bairro, j.juceb_cep, j.juceb_municipio, j.juceb_objeto_social, j.juceb_historico_data_ultimo_arquivamento, j.juceb_historico_nome_do_evento, j.juceb_historico_numero_arquivamento, j.juceb_historico_descricao_ato, j.juceb_data_evento_expirar, dtbe.cnpj_completo, dtbe.razao_social');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("j.juceb_situacao", 'REGISTRO ATIVO /');
		$this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}

	public function filtro_registro_cancelado(){

		$this->db->select('j.id, j.id_empresa, j.juceb_nome_empresarial, j.juceb_nire, j.juceb_cnpj, j.juceb_situacao, j.juceb_status, j.juceb_natureza_juridica, j.juceb_capital_social, j.juceb_capital_integralizado, j.juceb_data_do_ato_constitutivo, j.juceb_data_inicio_das_atividades, j.juceb_logradouro, j.juceb_complemento, j.juceb_numero, j.juceb_bairro, j.juceb_cep, j.juceb_municipio, j.juceb_objeto_social, j.juceb_historico_data_ultimo_arquivamento, j.juceb_historico_nome_do_evento, j.juceb_historico_numero_arquivamento, j.juceb_historico_descricao_ato, j.juceb_data_evento_expirar, dtbe.cnpj_completo, dtbe.razao_social');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("j.juceb_situacao", 'CANCELADA - ART.60 LEI 8934/94 /');
		$this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}

	public function filtro_registro_extinto(){

		$this->db->select('j.id, j.id_empresa, j.juceb_nome_empresarial, j.juceb_nire, j.juceb_cnpj, j.juceb_situacao, j.juceb_status, j.juceb_natureza_juridica, j.juceb_capital_social, j.juceb_capital_integralizado, j.juceb_data_do_ato_constitutivo, j.juceb_data_inicio_das_atividades, j.juceb_logradouro, j.juceb_complemento, j.juceb_numero, j.juceb_bairro, j.juceb_cep, j.juceb_municipio, j.juceb_objeto_social, j.juceb_historico_data_ultimo_arquivamento, j.juceb_historico_nome_do_evento, j.juceb_historico_numero_arquivamento, j.juceb_historico_descricao_ato, j.juceb_data_evento_expirar, dtbe.cnpj_completo, dtbe.razao_social');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("j.juceb_situacao", 'EXTINTA /');
		$this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}

	public function listar_dados_juceb_prox_vencimento(){

		$this->db->select('j.id, j.id_empresa, j.juceb_nome_empresarial, j.juceb_nire, j.juceb_cnpj, j.juceb_situacao, j.juceb_status, j.juceb_natureza_juridica, j.juceb_capital_social, j.juceb_capital_integralizado, j.juceb_data_do_ato_constitutivo, j.juceb_data_inicio_das_atividades, j.juceb_logradouro, j.juceb_complemento, j.juceb_numero, j.juceb_bairro, j.juceb_cep, j.juceb_municipio, j.juceb_objeto_social, j.juceb_historico_data_ultimo_arquivamento, j.juceb_historico_nome_do_evento, j.juceb_historico_numero_arquivamento, j.juceb_historico_descricao_ato, j.juceb_data_evento_expirar, dtbe.cnpj_completo, dtbe.razao_social');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("j.juceb_historico_data_ultimo_arquivamento !=", '');
		$this->db->where("DATEDIFF(j.juceb_data_evento_expirar, CURDATE()) <= 90");
		$this->db->where("DATEDIFF(j.juceb_data_evento_expirar, CURDATE()) >= 0");
		$this->db->where("j.juceb_cnpj LIKE '%0001%' ");
		$this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}

	public function listar_dados_juceb_vencidos(){

		$this->db->select('j.id, j.id_empresa, j.juceb_nome_empresarial, j.juceb_nire, j.juceb_cnpj, j.juceb_situacao, j.juceb_status, j.juceb_natureza_juridica, j.juceb_capital_social, j.juceb_capital_integralizado, j.juceb_data_do_ato_constitutivo, j.juceb_data_inicio_das_atividades, j.juceb_logradouro, j.juceb_complemento, j.juceb_numero, j.juceb_bairro, j.juceb_cep, j.juceb_municipio, j.juceb_objeto_social, j.juceb_historico_data_ultimo_arquivamento, j.juceb_historico_nome_do_evento, j.juceb_historico_numero_arquivamento, j.juceb_historico_descricao_ato, j.juceb_data_evento_expirar, dtbe.cnpj_completo, dtbe.razao_social');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("j.juceb_historico_data_ultimo_arquivamento !=", '');	
		$this->db->where("j.juceb_data_evento_expirar < CURDATE()");
		$this->db->where("j.juceb_cnpj LIKE '%0001%' ");			
		$this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}

	public function qtd_eventos_proximos_a_vencer(){

		$this->db->select('COUNT(distinct(j.id)) AS valor');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("j.juceb_historico_data_ultimo_arquivamento !=", '');
		$this->db->where("DATEDIFF(j.juceb_data_evento_expirar, CURDATE()) <= 90");
		$this->db->where("DATEDIFF(j.juceb_data_evento_expirar, CURDATE()) >= 0");
		$this->db->where("j.juceb_cnpj LIKE '%0001%' ");	 
		return $this->db->get()->row();
	}

	public function qtd_eventos_juceb_vencidos(){

		$this->db->select('COUNT(distinct(j.id)) AS valor');		
		
		$this->db->from('dtb_juceb_dados j');
		$this->db->join('dtb_empresas dtbe','j.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("j.juceb_historico_data_ultimo_arquivamento !=", '');	
		$this->db->where("j.juceb_data_evento_expirar < CURDATE()"); 
		$this->db->where("j.juceb_cnpj LIKE '%0001%' ");	
		return $this->db->get()->row();
	}

	// -------------------ACOMPANHAMENTO DE VIABILIDADE E LEGALIZAÇÃO--------------------------------------

	public function cadastrar_protocolo(){

		$dados = array(

				'protocolo' => $this->getProtocolo(),
				'nome_da_empresa' => $this->getNomeDaEmpresa(),
				'estado_do_processo' => $this->getEstadoDoProcesso(),
				'status_de_envio' => $this->getStatusDoEnvio(),
				'data_atualizacao' => $this->getDataAtualizacao(),
				'data_incorporacao' => $this->getDataIncorporacao(),
				'data_cadastro' => $this->getDataCadastro(),
				'id_cadastro' => $this->getIdCadastro()

		);
	
		if ($this->db->insert('dtb_viabilidade_juceb', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function listar_acompanhamento_viabilidade() {
		$this->db->select('id, protocolo, nome_empresa_mostrar, nome_da_empresa, estado_do_processo, status_de_envio, data_atualizacao, data_incorporacao, data_cadastro, id_cadastro');		
		
		return $this->db->get('dtb_viabilidade_juceb')->result();
	}

	public function qtd_listar_acompanhamento_abertos(){
		$this->db->select('COUNT(dtb_viabilidade_juceb.id) as qtd');		
		
		$this->db->where("estado_do_processo !=", 'PROCESSO FINALIZADO');
		// $this->db->group_by('dtb_viabilidade_juceb.id');
		return $this->db->get('dtb_viabilidade_juceb')->row();
	}

	public function listar_acompanhamento_viabilidade_abertos() {
		$this->db->select('id, protocolo, nome_empresa_mostrar, nome_da_empresa, estado_do_processo, status_de_envio, data_atualizacao, data_incorporacao, data_cadastro, id_cadastro');		
		
		$this->db->where("estado_do_processo !=", 'PROCESSO FINALIZADO');
		$this->db->group_by('dtb_viabilidade_juceb.id');
		return $this->db->get('dtb_viabilidade_juceb')->result();
	}

	public function qtd_listar_acompanhamento_finalizados(){
		$this->db->select('COUNT(dtb_viabilidade_juceb.id) as qtd');		
		
		$this->db->where("estado_do_processo =", 'PROCESSO FINALIZADO');
		// $this->db->group_by('dtb_viabilidade_juceb.id');
		return $this->db->get('dtb_viabilidade_juceb')->row();
	}

	public function listar_acompanhamento_viabilidade_finalizados() {
		$this->db->select('id, protocolo, nome_empresa_mostrar, nome_da_empresa, estado_do_processo, status_de_envio, data_atualizacao, data_incorporacao, data_cadastro, id_cadastro');		
		
		$this->db->where("estado_do_processo", 'PROCESSO FINALIZADO');
		$this->db->group_by('dtb_viabilidade_juceb.id');
		return $this->db->get('dtb_viabilidade_juceb')->result();
	}

	public function pesquisar_acompanhamento_viabilidade_id() {
		$this->db->select('*');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_viabilidade_juceb')->row();
	}

	public function excluir_protocolo(){
		return $this->db->delete('dtb_viabilidade_juceb', "id = {$this->getId()}");
	}

	public function editar_protocolo(){
	
		$dados = array(				
				
				'nome_empresa_mostrar' => $this->getNomeEmpresaMostrar()
		);
	
		if ($this->db->update('dtb_viabilidade_juceb', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function verificar_cadastro_duplicado($campo) {

		$this->db->select('id');
		 
		$this->db->where("protocolo", $campo);

		return $this->db->get('dtb_viabilidade_juceb')->row();
	}

}