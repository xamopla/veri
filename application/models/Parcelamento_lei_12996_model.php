<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parcelamento_lei_12996_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}
 
	public function listar_dividas_consolidadas(){

		$this->db->select('db.*, e.razao_social');

		$this->db->from('dtb_parcelamento_lei_12996_divida_consolidada db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
        $this->db->where('e.cnpj like "%0001%"');

		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function get_qtd_parcelas_a_vencer(){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_parcelamento_lei_12996_demonstrativo_prestacoes db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->where("db.indicador_situacao_parcela = 'A'");

		return $this->db->get()->row();
	}

	public function get_parcelas_vencidas(){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_parcelamento_lei_12996_demonstrativo_prestacoes db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->where("db.indicador_situacao_parcela = 'D'");

		return $this->db->get()->row();
	}

	public function listar_divida_consolidada_individual($id){

		$this->db->select('db.*, e.razao_social');

		$this->db->from('dtb_parcelamento_lei_12996_divida_consolidada db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		$this->db->where("db.id", $id);
        $this->db->where('e.cnpj like "%0001%"');

        return $this->db->get()->row();
	}

	public function listar_demonstrativo_prestacoes($id_divida_consolidada){
		$this->db->select('db.*');

		$this->db->from('dtb_parcelamento_lei_12996_demonstrativo_prestacoes db');

		$this->db->where("db.id_divida_consolidada", $id_divida_consolidada);
	
		return $this->db->get()->result();
	}

	// // Metodos da Modal
	public function listar_parcelas_nao_pagas_for_modal($id_divida_consolidada){

		$this->db->select('db.*');
		
		$this->db->from('dtb_parcelamento_lei_12996_demonstrativo_prestacoes db');
		
		$this->db->where("db.id_divida_consolidada", $id_divida_consolidada);
		$this->db->where("db.indicador_situacao_parcela", "D");
		
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	// Metodos da dashboard
	public function get_qtd_processos_negociados(){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_parcelamento_lei_12996_divida_consolidada db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}


		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){

            if($this->getFiltro() == 'EM_PARCELAMENTO'){
                $this->db->where("db.nome_situacao = 'Em Parcelamento'");

            } else if($this->getFiltro() == 'REJEITADA'){
                $this->db->where("db.nome_situacao = 'Rejeitada na Consolidação'");
            }
        }

		return $this->db->get()->row();
	}

}