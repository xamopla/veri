<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nfe_destinatario_mdfe extends CI_Model {

    CONST NAO_PROCESSADO = 0;
    CONST MDFE_OK = 1;
    CONST MDFE_INEXISTENTE = 2;
    CONST MDFE_NAO_PRECISA = 3;

    public function existe_registro($chave_acesso){
        $this->db->select('*');
        $this->db->where('chave_acesso', $chave_acesso);
        return $this->db->get('dtb_sefaz_destinatario_mdfe')->row();
    }

    public function inserir_se_nao_existe( $chave ){
        $registro = $this->existe_registro( $chave );
        if ( ! $registro ){
            $data = array(
                "chave_acesso" => $chave,
            );
            $this->db->insert('dtb_sefaz_destinatario_mdfe', $data);
            return false;
        }else{
            return $registro;
        }
    }

    public function atualizar_mdfe($dados, $chave){

        $status = $this->verifica_mdfe($dados);

        $dados = array(

            'status' => $status,
            'mdf_data' => $dados['mdf_data'],
        );

        if ($this->db->update('dtb_sefaz_destinatario_mdfe', $dados, "chave_acesso='".$chave."'")){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function verifica_mdfe($nota){
        $status = self::NAO_PROCESSADO;

        if(strlen(trim($nota['mdf_data'])) != 0){
            $status = self::MDFE_OK;
            return $status;
        }

        if($nota['uf'] != "BA" && strlen(trim($nota['mdf_data'])) == 0){
            $status = self::MDFE_INEXISTENTE;
        }

        if($nota['uf'] != "BA" && strlen(trim($nota['mdf_data'])) != 0){
            $status = self::MDFE_OK;
        }

        if($nota['uf'] == "BA"){
            $modalidade_frete = intval($this->apenas_numero($nota['modalidade_frete']));
            $modalidade_frete_obrigatorio = ($modalidade_frete == 0 || $modalidade_frete == 2 || $modalidade_frete == 4);
            $cidade_diferente = intval($this->apenas_numero($nota['cidade_emitente'])) != intval($this->apenas_numero($nota['cidade_destinatario']));

            if( $modalidade_frete_obrigatorio && $cidade_diferente && (strlen(trim($nota['mdf_data'])) == 0)){
                $status = self::MDFE_INEXISTENTE;
            }else{
                $status = self::MDFE_NAO_PRECISA;
            }
        }

        return $status;
    }

    public function precisa_processar($chave_acesso){
        $this->db->select('status');
        $this->db->where('chave_acesso', $chave_acesso);
        $data = $this->db->get('dtb_sefaz_destinatario_mdfe')->row();
        if($data->status == self::NAO_PROCESSADO || $data->status == self::MDFE_INEXISTENTE)
            return true;
        return false;
    }

    public function apenas_numero($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }

}