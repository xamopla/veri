<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Termo_intimacao_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	public function listar() {	
		$this->db->select('dtbe.cnpj, dtbe.razao_social, c.id as caixa_postal_id, cm.assunto, cm.conteudo, cm.id as id_caixa_postal_mensagem, cm.id_mensagem as id_mensagem, cm.lida, cm.recebida_em as data');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->join('dtb_ecac_caixa_postal c','cm.caixa_postal_id = c.id');
		$this->db->join('dtb_empresas dtbe','trim(c.cnpj_data) = trim(dtbe.cnpj)');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("cm.assunto like '%Termo de Intimação%' ");
		$this->db->where("cm.recebida_em >= '2021-01-01'");

		// $this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}


	public function qtd() {	
		$this->db->select('COUNT(*) as qtd');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->join('dtb_ecac_caixa_postal c','cm.caixa_postal_id = c.id');
		$this->db->join('dtb_empresas dtbe','trim(c.cnpj_data) = trim(dtbe.cnpj)');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("cm.assunto like '%Termo de Intimação%' ");
		$this->db->where("cm.recebida_em >= '2021-01-01'");
		return $this->db->get()->row();
	}

	public function find_info_for_modal($id){
		$this->db->select('cm.conteudo');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->where("cm.id", $id);
		return $this->db->get()->row();
	}



	public function find_ultimo_historico($r){
		$sql = "SELECT * FROM dtb_termo_intimacao_historico m WHERE m.cnpj = ".$r->cnpj." AND m.recebida_em = '".$r->data."' AND trim(m.assunto) = '".trim($r->assunto)."' AND m.caixa_postal_id = ".$r->caixa_postal_id." ORDER BY m.id DESC LIMIT 1";

		return $this->db->query($sql)->row();
	}


	public function insere_historico($r, $situacao, $descricao){
		date_default_timezone_set('America/Sao_Paulo');
		$usuario = "";
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		$dados = array(
	
				'assunto' => $r->assunto,
				'cnpj' => $r->cnpj,
				'recebida_em' => $r->recebida_em,
				'caixa_postal_id' => $r->caixa_postal_id,
				'data_alteracao' => date('Y-m-d H:i:s'),
				'nome_usuario' => $usuario,
				'situacao' => $situacao, 
				'descricao' => $descricao
		);
	
		if ($this->db->insert('dtb_termo_intimacao_historico', $dados)){
			return 1;
		} else {
			return 0;
		}
	}

	public function buscar_historico($r){
		$sql = "SELECT * FROM dtb_termo_intimacao_historico m WHERE m.cnpj = ".$r->cnpj." AND m.recebida_em = '".$r->recebida_em."' AND m.assunto = '".$r->assunto."' AND m.caixa_postal_id = ".$r->caixa_postal_id." ORDER BY m.id DESC ";

		return $this->db->query($sql)->result();
	}


	public function buscar_mensagem_ecac_by_id($id){
		$this->db->select('m.id as id_caixa_postal_mensagem, m.caixa_postal_id, m.assunto, m.recebida_em, m.caixa_postal_id, c.cnpj_data as cnpj');
		$this->db->from('dtb_ecac_caixa_postal_mensagem m');
		$this->db->join('dtb_ecac_caixa_postal c','m.caixa_postal_id = c.id');
		$this->db->where("m.id", $id);
		return $this->db->get()->row();
	}


	public function buscar_lida_por($id_caixa_postal,$cnpj, $assunto, $recebida_em){
		$this->db->select('m.nome_usuario, m.data_alteracao');
		$this->db->from('dtb_historico_leitura_mensagem_ecac m');
		$this->db->where("m.caixa_postal_id", $id_caixa_postal);
		$this->db->where("m.recebida_em", $recebida_em);
		$this->db->where("m.assunto", $assunto);
		$this->db->where("trim(m.cnpj)", $cnpj);
		return $this->db->get()->row();
	}

}