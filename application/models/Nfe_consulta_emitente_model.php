<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nfe_consulta_emitente_model extends CI_Model {	

	public function main($dados_post){
		$dados_request_sefaz = $this->logar($dados_post);
		$dados_request_sefaz = $this->consultar_emitente($dados_post, $dados_request_sefaz);
		return $this->return_array($this->get_dados_planilha($dados_post, $dados_request_sefaz), $dados_post, $dados_request_sefaz);
	}

	public function logar($dados_login){
		$data = array(
			'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__VIEWSTATE'=>'/wEPDwUKLTc5NjIwMjMwNg9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFBU5GRU5DZAIFDxYCHwAFNE5vdGEgRmlzY2FsIEVsZXRyJiMyNDQ7bmljYSAtIEF1dGVudGljYSYjMjMxOyYjMjI3O29kAgkPZBYCZg9kFgICAQ9kFgYCAw8PFggeCENzc0NsYXNzBQwgbW9kYWwgZmFkZSAeMF9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVHPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoEVycm8eMl9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4EXyFTQgICFgoeCHRhYmluZGV4BQItMR4Ecm9sZQUGZGlhbG9nHgthcmlhLWhpZGRlbgUEdHJ1ZR4NZGF0YS1rZXlib2FyZAUFZmFsc2UeDWRhdGEtYmFja2Ryb3AFBnN0YXRpY2QCBw9kFgYCBw8WAh8AZWQCCQ8PFgIeB1Zpc2libGVnZGQCCw8PFgIfCmdkZAIJDxYCHwAFswQ8aDE+SW5zdHJ1w6fDtWVzOjwvaDE+DQo8YnIgLz4NCiAgICANCjx1bD4NCiAgICA8bGk+DQogICAgICAgIFNlcsOhIHBlcm1pdGlkbyBhcGVuYXMgbyBhY2Vzc28gY29tIGxvZ2luIGUgc2VuaGE7DQogICAgPC9saT4NCiAgICA8bGk+DQogICAgICAgIEFww7NzIDAzIHRlbnRhdGl2YXMgaW52w6FsaWRhcyBhIHDDoWdpbmEgZGUgTG9naW4gaXLDoSBibG9xdWVhciBvIGFjZXNzbyBkbyBJUCBkZSBvcmlnZW0gcG9yIDEwIG1pbnV0b3M7DQogICAgPC9saT4NCiAgICA8bGk+DQogICAgICAgIENhc28gbsOjbyBsZW1icmUgYSBzZW5oYSwgY2xpcXVlIGVtICJlc3F1ZWNldSBhIHNlbmhhPyIgZSBhcMOzcyBpbmZvcm1hw6fDo28gZG9zIGRhZG9zIG5lY2Vzc8OhcmlvcywgbyBsZW1icmV0ZSBzZXLDoSBlbnZpYWRvIHBhcmEgbyBlbWFpbCBjYWRhc3RyYWRvLg0KICAgIDwvbGk+DQo8L3VsPg0KDQo8YnIgLz4NCjxoNT5FbSBjYXNvIGRlIGTDunZpZGFzIGVudHJlIGVtIGNvbnRhdG8gYXRyYXbDqXMgZG8gZmFsZWNvbm9zY29Ac2VmYXouYmEuZ292LmJyIG91IDA4MDAgMDcxMDA3MTwvaDU+DQoNCmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFDCBtb2RhbCBmYWRlIB8EAgIWCh8FBQItMR8GBQZkaWFsb2cfBwUEdHJ1ZR8IBQVmYWxzZR8JBQZzdGF0aWNkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFDkFTTElCOiAyLjIuMC4wZGT6MKVqeVxa95aw7mgbgAhzuIg4AA==',
			'__VIEWSTATEGENERATOR'=>'4D2610BE',
			'__PREVIOUSPAGE'=>'nQpgC3IwW14xm_p9A86Fs1BD2uvvhdVtsNOHtQU-zmLdq1rLBV1QvM0tJTM4A9aoJ1QjtfqVP3XAQStQYGITS4fKvhSrNHXsWrGAU8UpyV6P95SWa98tF0T68ws2wKGonm-XStHtNGNupV56wR6ENjbUun01',
			'__EVENTVALIDATION'=>'/wEWCgKL7IYoAq6Hp60KAqGxl50BAqaPurkJAvK++aIIAreE9IQDArrMto0NAp3o68oBAuSM8cYDAs3yq5IEbyN/x7eTzjhhqWDV0UevqqOHoLA=',
			'__ASYNCPOST'=>'false',
			'ctl00$PHCentro$userLogin'=>$dados_login['login'],
			'ctl00$PHCentro$userPass'=>$dados_login['senha'],
			'ctl00$PHCentro$btnLogin'=>'Entrar'
		);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_emitente.aspx";

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEJAR, $dados_login['cookie_file']); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);


		#VALIDANDO SE O SERVIDOR DA SEFAZ ESTÁ OFF
		if(preg_match('/Erro do Sistema/', $store)){
			#SE ESTIVER OFF, FAZ LOGIN NO SERVIDOR 2
			$data = array(
				'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
				'__EVENTTARGET'=>'',
				'__EVENTARGUMENT'=>'',
				'__VIEWSTATE'=>'/wEPDwUKLTc5NjIwMjMwNg9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFBU5GRU5DZAIFDxYCHwAFNE5vdGEgRmlzY2FsIEVsZXRyJiMyNDQ7bmljYSAtIEF1dGVudGljYSYjMjMxOyYjMjI3O29kAgkPZBYCZg9kFgICAQ9kFgYCAw8PFggeCENzc0NsYXNzBRggbW9kYWwgZmFkZSAgbW9kYWwgZmFkZSAeMF9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVHPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoEVycm8eMl9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4EXyFTQgICFgoeCHRhYmluZGV4BQItMR4Ecm9sZQUGZGlhbG9nHgthcmlhLWhpZGRlbgUEdHJ1ZR4NZGF0YS1rZXlib2FyZAUFZmFsc2UeDWRhdGEtYmFja2Ryb3AFBnN0YXRpY2QCBw9kFgoCAQ8WAh4IZGlzYWJsZWRkZAIDDxYCHwpkZAIHDxYCHwAFFUhvbW9sb2dhJiMyMzE7JiMyMjc7b2QCCQ8PFgIeB1Zpc2libGVnZGQCCw8PFgIfC2dkZAIJDxYCHwAFswQ8aDE+SW5zdHJ1w6fDtWVzOjwvaDE+DQo8YnIgLz4NCiAgICANCjx1bD4NCiAgICA8bGk+DQogICAgICAgIFNlcsOhIHBlcm1pdGlkbyBhcGVuYXMgbyBhY2Vzc28gY29tIGxvZ2luIGUgc2VuaGE7DQogICAgPC9saT4NCiAgICA8bGk+DQogICAgICAgIEFww7NzIDAzIHRlbnRhdGl2YXMgaW52w6FsaWRhcyBhIHDDoWdpbmEgZGUgTG9naW4gaXLDoSBibG9xdWVhciBvIGFjZXNzbyBkbyBJUCBkZSBvcmlnZW0gcG9yIDEwIG1pbnV0b3M7DQogICAgPC9saT4NCiAgICA8bGk+DQogICAgICAgIENhc28gbsOjbyBsZW1icmUgYSBzZW5oYSwgY2xpcXVlIGVtICJlc3F1ZWNldSBhIHNlbmhhPyIgZSBhcMOzcyBpbmZvcm1hw6fDo28gZG9zIGRhZG9zIG5lY2Vzc8OhcmlvcywgbyBsZW1icmV0ZSBzZXLDoSBlbnZpYWRvIHBhcmEgbyBlbWFpbCBjYWRhc3RyYWRvLg0KICAgIDwvbGk+DQo8L3VsPg0KDQo8YnIgLz4NCjxoNT5FbSBjYXNvIGRlIGTDunZpZGFzIGVudHJlIGVtIGNvbnRhdG8gYXRyYXbDqXMgZG8gZmFsZWNvbm9zY29Ac2VmYXouYmEuZ292LmJyIG91IDA4MDAgMDcxMDA3MTwvaDU+DQoNCmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFGCBtb2RhbCBmYWRlICBtb2RhbCBmYWRlIB8EAgIWCh8FBQItMR8GBQZkaWFsb2cfBwUEdHJ1ZR8IBQVmYWxzZR8JBQZzdGF0aWNkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFDkFTTElCOiAyLjIuMC4wZGRmsiRtIZQVT70O0TmdsxsIhGNvLg==',
				'__VIEWSTATEGENERATOR'=>'4D2610BE',
				'__PREVIOUSPAGE'=>'nQpgC3IwW14xm_p9A86Fs1BD2uvvhdVtsNOHtQU-zmLdq1rLBV1QvM0tJTM4A9aoJ1QjtfqVP3XAQStQYGITS4fKvhSrNHXsWrGAU8UpyV6P95SWa98tF0T68ws2wKGonm-XStHtNGNupV56wR6ENjbUun01',
				'__EVENTVALIDATION'=>'/wEdAAopxbC7G69VA4whnvgh6FOaIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniVKGvNR0oAYscFLd2RcOwR9Ljsfho=',
				'__ASYNCPOST'=>'false',
				'ctl00$PHCentro$userLogin'=>$dados_login['login'],
				'ctl00$PHCentro$userPass'=>$dados_login['senha'],
				'ctl00$PHCentro$btnLogin'=>'Entrar'
			);

			$url = "https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_emitente.aspx";
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, $dados_login['cookie_file']); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);
		}

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;


		
		return $dados_request_sefaz;
	}

	public function consultar_emitente($dados_login, $dados_request_sefaz){

		$data = array(
			'__EVENTTARGET'=>'',
			'__EVENTARGUMENT'=>'',
			'__LASTFOCUS'=>'',
			'__VIEWSTATE'=>$dados_request_sefaz['VIEWSTATE'],
			'__VIEWSTATEGENERATOR'=>$dados_request_sefaz['VIEWSTATEGENERATOR'],
			'__VIEWSTATEENCRYPTED'=>'',
			'__EVENTVALIDATION'=>$dados_request_sefaz['EVENTVALIDATION'],
			'filtro'=>'rbt_filtro3',
			'txtPeriodoInicial'=>$dados_login['data_ini'],
			'txtPeriodoFinal'=>$dados_login['data_fim'],
			'cpf_cnpj'=>'rbt_cnpj',
			'txtCNPJDestinatario'=>'',
			'AplicarFiltro'=>'Consultar',
			'CmdOrdenacao'=>'--Selecione--'
		);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_emitente.aspx";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);

		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($store);
		libxml_use_internal_errors(false);
		$xpath = new DOMXpath($doc);

		$dados_request_sefaz = array();		
		$elements = $xpath->query('//input[@id="__VIEWSTATE"]');
		$dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;		
		$elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
		$dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
		$elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
		$dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
		


		return $dados_request_sefaz;
		
	}

	public function get_dados_planilha($dados_login, $dados_request_sefaz){
		$data = array(
		'__EVENTTARGET'=>'',
		'__EVENTARGUMENT'=>'',
		'__LASTFOCUS'=>'',
		'__VIEWSTATE'=>$dados_request_sefaz['VIEWSTATE'],
		'__VIEWSTATEGENERATOR'=>$dados_request_sefaz['VIEWSTATEGENERATOR'],
		'__VIEWSTATEENCRYPTED'=>'',
		'__EVENTVALIDATION'=>$dados_request_sefaz['EVENTVALIDATION'],
		'filtro'=>'rbt_filtro3',
		'txtPeriodoInicial'=>$dados_login['data_ini'],
		'txtPeriodoFinal'=>$dados_login['data_fim'],
		'cpf_cnpj'=>'rbt_cnpj',
		'txtCNPJDestinatario'=>'',
		'btn_GerarPlanilha'=>'Gerar Planilha',
		'CmdOrdenacao'=>'--Selecione--'
		);

		$url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_emitente.aspx";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $dados_login['cookie_file']); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_HEADER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
		curl_close($ch);
		
		return $store;
	}

	public function return_array($string_notas, $dados_login, $dados_request_sefaz){

		$array_notas = explode("\n", $string_notas);
		
		for($i=0; $i<count($array_notas); $i++){
			$conteudo_linha = explode(";", $array_notas[$i]);
			if(strlen($this->apenas_numero($conteudo_linha[0])) == 0){
				unset($array_notas[$i]);
			}
		}
		unset($array_notas[count($array_notas)]);

		$array_nova = array();
		foreach ($array_notas as $nota) {
			$conteudo_nota = explode(";", $nota);

			#PERÍODO SEM NOTAS
			if(count($conteudo_nota) == 1 ){
				return $array_nova;
			}else{

				$chave_acesso = trim(str_replace("'", "", $conteudo_nota[5]));

				$array_nova = array_merge($array_nova, array(
					// $this->apenas_numero($conteudo_nota[0]).$this->apenas_numero($conteudo_nota[3]).$this->apenas_numero($conteudo_nota[4])
					$chave_acesso=>array(
						'doc_sefaz'=>$this->apenas_numero($conteudo_nota[0]),
						'CNPJ/CPF'=>trim($conteudo_nota[1]),
						'razao_social_dest'=>trim($conteudo_nota[2]),
						'emissao'=>trim($conteudo_nota[3]),
						'valor'=>str_replace(",", ".", str_replace(".", "", $conteudo_nota[4])),
						'chave_acesso'=>$chave_acesso,
						'situacao'=>trim($conteudo_nota[6]),
						'tipo_operacao'=>trim($conteudo_nota[7]),
						'user_report'=>$dados_login["user_report"],
						'data_report'=>date('d/m/Y')
					)
				));
			}
		}
		
		return $array_nova;
	}

	public function apenas_numero($str) {
    	return preg_replace("/[^0-9]/", "", $str);
	}

	public function ler_csv_cliente($file){
		$linhas = file ($file['tmp_name']);

		for($i=0; $i<count($linhas); $i++){
			$conteudo_linha = explode(";", $linhas[$i]);
			if(strlen($this->apenas_numero($conteudo_linha[0])) == 0){
				unset($linhas[$i]);
			}
		}

		$array_nova = array();
		foreach ($linhas as $nota) {
			$conteudo_nota = explode(";", $nota);		

			array_push($array_nova, $this->apenas_numero($conteudo_nota[0]));	
		}
		$array_nova = array_unique($array_nova);
		return $array_nova;		
	}

	//MÁSCARA DA NOTA FISCAL
	public function mask($val, $mask)
	{
		$maskared = '';
		$k = 0;
		for($i = 0; $i<=strlen($mask)-1; $i++)
		{
			if($mask[$i] == '#')
			{
				if(isset($val[$k]))
					$maskared .= $val[$k++];
			}
			else
			{
				if(isset($mask[$i]))
					$maskared .= $mask[$i];
			}
		}
		return $maskared;
	}

	public function get_nfe_emitente($cnpj_data){
		$this->db->select('*');
		$this->db->from('nfe_emitente');
		$this->db->where('cnpj_data', $cnpj_data);
		return $this->db->get()->result_array();
	}

	public function insert_nfe_emitente($dados){
		$data = array(
			"cnpj_data" => $dados["cnpj_data"],
			"doc_sefaz" => $dados["doc_sefaz"],
			"cnpj_dest" => $dados["CNPJ/CPF"],
			"razao_dest" => $dados["razao_social_dest"],
			"data_emissao" => $dados["emissao"],
			"valor" => $dados["valor"],
			"chave_nfe" => $dados["chave_acesso"],
			"situacao_nfe" => $dados["situacao"],
			"tipo_operacao" => $dados["tipo_operacao"],
			"user_report" => $dados["user_report"],
			"data_report" => $dados["data_report"],
		);

		$this->db->insert('nfe_emitente', $data);
	}

}