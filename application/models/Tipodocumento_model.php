<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tipodocumento_model extends CI_Model{

	private $id;
	private $ativo;
	private $nome;
	private $descricao;
	private $tipo_de_validade;
	private $url_consulta;
	private $id_contabilidade;
	private $orgao_expedidor;
	private $diasNotificacao;
	
	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getAtivo() {
		return $this->ativo;
	}
	public function setAtivo($ativo) {
		$this->ativo = $ativo;
		return $this;
	}

	public function getNome() {
		return $this->nome;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}

	public function getDescricao() {
		return $this->descricao;
	}
	public function setDescricao($descricao) {
		$this->descricao = $descricao;
		return $this;
	}

	public function getTipoDeValidade() {
		return $this->tipo_de_validade;
	}
	public function setTipoDeValidade($tipo_de_validade) {
		$this->tipo_de_validade = $tipo_de_validade;
		return $this;
	}

	public function getUrlConsulta() {
		return $this->url_consulta;
	}
	public function setUrlConsulta($url_consulta) {
		$this->url_consulta = $url_consulta;
		return $this;
	}

	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}

	public function getOrgaoExpedidor() {
		return $this->orgao_expedidor;
	}
	public function setOrgaoExpedidor($orgao_expedidor) {
		$this->orgao_expedidor = $orgao_expedidor;
		return $this;
	}

	public function getDiasNotificacao() {
		return $this->diasNotificacao;
	}
	public function setDiasNotificacao($diasNotificacao) {
		$this->diasNotificacao = $diasNotificacao;
		return $this;
	}



	public function cadastrar(){
	
		$dados = array(
	
				'ativo' => $this->getAtivo(),
				'nome' => $this->getNome(),
				'descricao' => $this->getDescricao(),
				'url_consulta' => $this->getUrlConsulta(),
				'id_contabilidade' => $this->getIdContabilidade(),
				'orgao_expedidor' => $this->getOrgaoExpedidor(),
				'diasNotificacao' => $this->getDiasNotificacao()
		);
	
		if ($this->db->insert('dtb_tipodedocumento', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function listar() {
		$this->db->select('id, ativo, nome, descricao, orgao_expedidor, url_consulta');
		return $this->db->get('dtb_tipodedocumento')->result();
	}

	public function excluir(){
		return $this->db->delete('dtb_tipodedocumento', "id = {$this->getId()}");
	}

	public function pesquisar_tipodocumento_id() {
		$this->db->select('id, ativo, nome, descricao, orgao_expedidor, url_consulta, tipo_de_validade, diasNotificacao');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_tipodedocumento')->row();
	}

	public function editar(){
	
		$dados = array(
				'ativo' => $this->getAtivo(),				
				'nome' => $this->getNome(),
				'descricao' => $this->getDescricao(),
				'url_consulta' => $this->getUrlConsulta(),
				'orgao_expedidor' => $this->getOrgaoExpedidor(),
				'diasNotificacao' => $this->getDiasNotificacao()
		);
	
		if ($this->db->update('dtb_tipodedocumento', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	
	
	}
}