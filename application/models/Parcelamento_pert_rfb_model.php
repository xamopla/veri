<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parcelamento_pert_rfb_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	public function buscar_parcelamentos(){
		$this->db->select('db.*, e.razao_social');

		$this->db->from('dtb_parcelamento_pert_rfb db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}


		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
            if($this->getFiltro() == 'ATIVO'){
                $this->db->where("db.situacao = 'Ativo'");
            }else if($this->getFiltro() == 'EXCLUIDO'){
                $this->db->where("db.situacao = 'Excluído'");
            }else if($this->getFiltro() == 'CANCELADO'){
                $this->db->where("db.situacao = 'Requerimento cancelado'");
            }
        }

		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function buscar_parcelamentos_individual($id){
		$this->db->select('db.*, e.razao_social');

		$this->db->from('dtb_parcelamento_pert_rfb db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		$this->db->where("db.id", $id);
	
		return $this->db->get()->row();
	}

	public function buscar_parcelas($cnpj, $id_parcelamento){
		$this->db->select('db.*');

		$this->db->from('dtb_parcelamento_pert_rfb_demonstrativo_de_parcelas db');

        $this->db->where("db.id_parcelamento", $id_parcelamento);
        $this->db->where("db.cnpj", $cnpj);

		return $this->db->get()->result();
	}

	public function buscar_parcelas_a_vencer($cnpj, $id_parcelamento){
		$this->db->select('db.*, e.razao_social');

		$this->db->from('dtb_parcelamento_pert_rfb_demonstrativo_de_parcelas db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

        $this->db->where("db.id_parcelamento = $id_parcelamento");
        $this->db->where("db.cnpj = $cnpj");
        $this->db->where("db.situacao = 'A vencer'");
		return $this->db->get()->result();
	}

	public function buscar_parcelas_em_atraso($cnpj, $id_parcelamento){
		$this->db->select('db.*, e.razao_social');

		$this->db->from('dtb_parcelamento_pert_rfb_demonstrativo_de_parcelas db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');
		
		$this->db->where("db.id_parcelamento = $id_parcelamento");
        $this->db->where("db.cnpj = $cnpj");
        $this->db->where("db.situacao = 'Devedora'");
		return $this->db->get()->result();
	}

	public function buscar_pagamentos($cnpj, $id_parcelamento){
		$this->db->select('db.*');

		$this->db->from('dtb_parcelamento_pert_rfb_demonstrativo_de_pagamentos db');

        $this->db->where("db.id_parcelamento = $id_parcelamento");
        $this->db->where("db.cnpj = $cnpj");
		return $this->db->get()->result();
	}

	// Metodos da dashboard
	public function get_qtd_parcelamentos(){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_parcelamento_pert_rfb db');
		$this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
            if($this->getFiltro() == 'ATIVO'){
                $this->db->where("db.situacao = 'Ativo'");
            } else if($this->getFiltro() == 'EXCLUIDO'){
                $this->db->where("db.situacao = 'Excluído'");
            } else if($this->getFiltro() == 'CANCELADO'){
                $this->db->where("db.situacao = 'Requerimento cancelado'");
            } 
        }

		return $this->db->get()->row();
	}
}