<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Calendario_model extends CI_Model{
	
	private $id;
	private $nome;
	private $datainicial;
	private $datafinal;
	private $url;
	private $id_usuario;
	private $cor;
	private $remover;
	private $id_evento;
	private $observacao;
	private $frequencia;

	public function __construct(){
		parent::__construct();
	}
	
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getNome() {
		return $this->nome;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}
	public function getDataInicial() {
		return $this->datainicial;
	}
	public function setDataInicial($datainicial) {
		$this->datainicial = $datainicial;
		return $this;
	}
	public function getDataFinal() {
		return $this->datafinal;
	}
	public function setDataFinal($datafinal) {
		$this->datafinal = $datafinal;
		return $this;
	}
	
	public function getUrl() {
		return $this->url;
	}
	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}

	public function getIdUsuario() {
		return $this->id_usuario;
	}
	public function setIdUsuario($id_usuario) {
		$this->id_usuario = $id_usuario;
		return $this;
	}

	public function getCor() {
		return $this->cor;
	}
	public function setCor($cor) {
		$this->cor = $cor;
		return $this;
	}

	public function getId_evento() {
	    return $this->id_evento;
	}
	 
	public function setId_evento($id_evento) {
	    $this->id_evento = $id_evento;
	}

	public function getObservacao() {
	    return $this->observacao;
	}
	 
	public function setObservacao($observacao) {
	    $this->observacao = $observacao;
	}


	public function getFrequencia() {
	    return $this->frequencia;
	}
	 
	public function setFrequencia($frequencia) {
	    $this->frequencia = $frequencia;
	}

	public function cadastrar(){
		$nome = $this->session->userdata['userprimesession']['nome'];
		if($nome == null || $nome == ""){
			$nome = "Administrador";
		}
		$dados = array(
	
				'nome' => $this->getNome(),
				'datainicio' => $this->getDataInicial(),
				'datafim' => $this->getDataFinal(),
				'url' => $this->getUrl(),
				'id_usuario' => $this->getIdUsuario(),
				'cor' => $this->getCor(),
				'usuario_name' => $nome
		);
	
		if ($this->db->insert('eventos', $dados)){
			$evento = $this->retornaUltimoRegistro();
			$id_evento;
			foreach ($evento as $value) {
				$id_evento =  $value->id;
			}
			return $id_evento;
		} else {
			return 0;
		}
	}

	public function cadastrar_eventos_criados(){
		$usuariologado = $this->session->userdata['userprimesession']['nome'];
		if($usuariologado == null || $usuariologado == ""){
			$usuariologado = "Administrador";
		}

		$nome = $this->getNome().";".$usuariologado;
		$dados = array(
	
				'nome' => $this->getNome(),
				'datainicio' => $this->getDataInicial(),
				'datafim' => $this->getDataFinal(),
				'id_usuario' => $this->getIdUsuario(),
				'cor' => $this->getCor(),
				'description' => $nome,
				'id_evento' => $this->getId_evento(),
				'observacao' => $this->getObservacao(),
				'frequencia' => $this->getFrequencia()
		);
	
		if ($this->db->insert('eventos_criados', $dados)){
		$evento = $this->retornaUltimoEventoCriado();
		$id_evento;
		foreach ($evento as $value) {
		$id_evento =  $value->id;
		}

		if($this->getFrequencia() != "NENHUMA"){
			$this->cadastrar_recorrencia($dados);
			return $id_evento;
		}else{
			return $id_evento;
		}
		

		//return 1;
		} else {
		return 0;
		}
	}

	public function cadastrar_recorrencia($dados){
		$frequencia = $dados['frequencia'];

		if($frequencia == "DIAS"){
			$this->cadastrar_recorrencia_dias($dados);
		}elseif($frequencia == "SEMANAS"){
			$this->cadastrar_recorrencia_semanas($dados);
		}elseif($frequencia == "MESES"){
			$this->cadastrar_recorrencia_meses($dados);
		}elseif($frequencia == "ANOS"){
			$this->cadastrar_recorrencia_anos($dados);
		}
	}


	public function cadastrar_recorrencia_dias($dados){
		$dias = 30;
		for($i=1;$i<=$dias;$i++){
			$dados['datainicio'] = date('Y-m-d H:i:s', strtotime($dados['datainicio'] . ' +1 day'));
			$dados['datafim'] = date('Y-m-d H:i:s', strtotime($dados['datafim'] . ' +1 day'));

			$this->db->insert('eventos_criados', $dados);
		}
	}

	public function cadastrar_recorrencia_semanas($dados){
		$dias = 8;
		for($i=1;$i<=$dias;$i++){
			$dados['datainicio'] = date('Y-m-d H:i:s', strtotime($dados['datainicio'] . ' +1 week'));
			$dados['datafim'] = date('Y-m-d H:i:s', strtotime($dados['datafim'] . ' +1 week'));

			$this->db->insert('eventos_criados', $dados);
		}
	}

	public function cadastrar_recorrencia_meses($dados){
		$dias = 12;
		for($i=1;$i<=$dias;$i++){
			$dados['datainicio'] = date('Y-m-d H:i:s', strtotime($dados['datainicio'] . ' +1 month'));
			$dados['datafim'] = date('Y-m-d H:i:s', strtotime($dados['datafim'] . ' +1 month'));

			$this->db->insert('eventos_criados', $dados);
		}
	}

	public function cadastrar_recorrencia_anos($dados){
		$dias = 1;
		for($i=1;$i<=$dias;$i++){
			$dados['datainicio'] = date('Y-m-d H:i:s', strtotime($dados['datainicio'] . ' +1 year'));
			$dados['datafim'] = date('Y-m-d H:i:s', strtotime($dados['datafim'] . ' +1 year'));

			$this->db->insert('eventos_criados', $dados);
		}
	}

	public function retornaUltimoEventoCriado(){
		$this->db->select('MAX(id) as id');
		$this->db->from('eventos_criados');
		$this->db->where("id_usuario", $this->getIdUsuario());
		return $this->db->get()->result();
	}
	
	public function retornaUltimoRegistro(){
		$this->db->select('MAX(id) as id');
		$this->db->from('eventos');
		$this->db->where("id_usuario", $this->getIdUsuario());
		return $this->db->get()->result();
	}


	public function listarForEventos(){
		$this->db->select('id, nome as title, datainicio as start, datafim as end, url, cor');
		$this->db->from('eventos');
		$this->db->where('remover', "FALSE");
		$this->db->where('id_usuario', $this->getIdUsuario());

		return $this->db->get()->result();
	}

	public function listar(){

		$this->db->select('eventos_criados.id, eventos_criados.nome as title, eventos_criados.datainicio as start, eventos_criados.datafim as end, eventos_criados.cor as backgroundColor, eventos_criados.url as url, eventos_criados.description');
		$this->db->from('eventos_criados');
		//$this->db->where('datainicio is not null');
		if($this->session->userdata['userprimesession']['nivel'] == 2){
		$this->db->join('dtb_usuarios as u', 'eventos_criados.id_usuario = u.id', 'left');
		$this->db->group_start();
		$this->db->where('eventos_criados.id_usuario', $this->getIdUsuario());
		$this->db->or_where('u.nivel != 2');
		           $this->db->group_end();


		}


		return $this->db->get()->result();
	}

	public function deleteEvento($id){
		$this->db->where('id', $id);
		return $this->db->delete('eventos_criados');
	}

	public function updEvento2($param){
		$campos = array(
			'nombre' => $param['nome'],
			'url' => $param['web']
			);

		$this->db->where('id',$param['id']);
		$this->db->update('eventos',$campos);

		if ($this->db->affected_rows() == 1) {
			return 1;
		}else{
			return 0;
		}
	}

	public function atualizaDataInicial($param){
		$campos = array(
			'datainicio' => $param['fecini']
		);

		$this->db->where('id',$param['id']);
		$this->db->update('eventos',$campos);
		
		if ($this->db->affected_rows() == 1) {
			return 1;
		}else{
			return 0;
		}		
	}
	
	public function updEvento($param){
		$campos = array(
			'datainicio' => $param['fecini'],
			'datafim' => $param['fecfin']
			);

		$this->db->where('id',$param['id']);
		$this->db->update('eventos_criados',$campos);

		if ($this->db->affected_rows() == 1) {
			return 1;
		}else{
			return 0;
		}
	}

	public function removeEventoDaLista($param){
		$campos = array(
			'remover' => "1"
			);

		$this->db->where('id',$param['id']);
		$this->db->update('eventos',$campos);

		if ($this->db->affected_rows() == 1) {
			return 1;
		}else{
			return 0;
		}		
	}

	public function listar_eventos(){
		$this->db->select('id, nome, remover, cor');
		$this->db->from('eventos');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where('eventos.id_usuario', $this->session->userdata['userprimesession']['id']);
		}

		$this->db->order_by('id desc');
		return $this->db->get()->result();
	}

	public function listar_eventos_calendario($filtro){

		$this->db->select('eventos_criados.id, eventos_criados.nome as title, eventos_criados.datainicio as start, eventos_criados.datafim as end, eventos_criados.cor as backgroundColor, eventos_criados.url as url, eventos_criados.description, eventos_criados.id_evento, eventos_criados.observacao, eventos_criados.id_evento as id_evento, eventos_criados.frequencia');
		$this->db->from('eventos_criados');

		if($filtro != 0){
			$this->db->where('eventos_criados.id_evento', $filtro);
		}
		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->join('dtb_usuarios u', 'eventos_criados.id_usuario = u.id', 'left');
			$this->db->group_start();
				$this->db->where('eventos_criados.id_usuario', $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('u.nivel != 2');
            $this->db->group_end();
			
		}

		return $this->db->get()->result();
	}

	public function remover_evento_lista($id){
		$campos = array(
			'remover' => "1"
		);

		$this->db->where('id',$id);
		$this->db->update('eventos',$campos);

		if ($this->db->affected_rows() == 1) {
			return 1;
		}else{
			return 0;
		}		
	}

	public function get_color_evento($id_evento){
		$this->db->select('cor');
		$this->db->from('eventos');
		$this->db->where('id', $id_evento);
		return $this->db->get()->row();
	}

	public function editar_eventos_modal(){
		$dados = array(
	
				'nome' => $this->getNome(),
				'datainicio' => $this->getDataInicial(),
				'datafim' => $this->getDataFinal(),
				'cor' => $this->getCor(),
				'id_evento' => $this->getId_evento(),
				'observacao' => $this->getObservacao()
		);
	
		if ($this->db->update('eventos_criados', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}


	public function excluir_tarefa($id){
		$this->db->where('id', $id);
		return $this->db->delete('eventos_criados');
	}
	
}