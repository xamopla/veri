<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Limite_simples_model extends CI_Model {

    private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

    public function buscar(){

        $this->db->select('db.cnpj, db.valor_atual, db.percentual, e.razao_social, db.path');
        $this->db->from('dtb_limite_simples db');

        $this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)','left');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
            if($this->getFiltro() == 'PROXIMAS'){
                $this->db->where("db.percentual >= 70");
                $this->db->where("db.percentual < 100");
            }else if($this->getFiltro() == 'LIMITE'){
                $this->db->where("db.percentual >= 100");
            }
        }

        $this->db->where("db.cnpj like '%0001%' ");
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }


    public function qtd(){

        $this->db->select('COUNT(*) as qtd');
        $this->db->from('dtb_limite_simples db');

        $this->db->join('dtb_empresas e','trim(db.cnpj) = trim(e.cnpj)','left');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            $this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        if($this->getFiltro() != null && $this->getFiltro() != "TODAS"){
            if($this->getFiltro() == 'PROXIMAS'){
                $this->db->where("db.percentual >= 70");
                $this->db->where("db.percentual < 100");
            }else if($this->getFiltro() == 'LIMITE'){
                $this->db->where("db.percentual >= 100");
            }
        }

        // $this->db->group_by('db.cnpj');
        return $this->db->get()->row();
    }

    
}
