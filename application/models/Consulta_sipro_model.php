<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Consulta_sipro_model extends CI_Model {

	private $id;
	private $id_empresa;
	private $numero_processo;
	private $numero_processo_formatado;
	private $data_cadastramento;
	private $nome_interessado;
	private $tipo_processo;
	private $local_processo;
	private $situacao_processo;
	private $envio_data;
	private $envio_local;
	private $recepcao_data;
	private $recepcao_local;
	private $objetivo;
	private $id_cadastro;
	private $data_cadastro;
	private $data_atualizacao;

	public function getId() {
	   return $this->id;
	}
	
	public function setId($id) {
	   $this->id = $id;
	}

	public function getId_empresa() {
	   return $this->id_empresa;
	}
	
	public function setId_empresa($id_empresa) {
	   $this->id_empresa = $id_empresa;
	}

	public function getNumero_processo() {
	   return $this->numero_processo;
	}
	
	public function setNumero_processo($numero_processo) {
	   $this->numero_processo = $numero_processo;
	}

	public function getNumero_processo_formatado() {
	   return $this->numero_processo_formatado;
	}
	
	public function setNumero_processo_formatado($numero_processo_formatado) {
	   $this->numero_processo_formatado = $numero_processo_formatado;
	}

	public function getData_cadastramento() {
	   return $this->data_cadastramento;
	}
	
	public function setData_cadastramento($data_cadastramento) {
	   $this->data_cadastramento = $data_cadastramento;
	}

	public function getNome_interessado() {
	   return $this->nome_interessado;
	}
	
	public function setNome_interessado($nome_interessado) {
	   $this->nome_interessado = $nome_interessado;
	}

	public function getTipo_processo() {
	   return $this->tipo_processo;
	}
	
	public function setTipo_processo($tipo_processo) {
	   $this->tipo_processo = $tipo_processo;
	}

	public function getLocal_processo() {
	   return $this->local_processo;
	}
	
	public function setLocal_processo($local_processo) {
	   $this->local_processo = $local_processo;
	}

	public function getSituacao_processo() {
	   return $this->situacao_processo;
	}
	
	public function setSituacao_processo($situacao_processo) {
	   $this->situacao_processo = $situacao_processo;
	}

	public function getEnvio_data() {
	   return $this->envio_data;
	}
	
	public function setEnvio_data($envio_data) {
	   $this->envio_data = $envio_data;
	}

	public function getEnvio_local() {
	   return $this->envio_local;
	}
	
	public function setEnvio_local($envio_local) {
	   $this->envio_local = $envio_local;
	}

	public function getRecepcao_data() {
	   return $this->recepcao_data;
	}
	
	public function setRecepcao_data($recepcao_data) {
	   $this->recepcao_data = $recepcao_data;
	}

	public function getRecepcao_local() {
	   return $this->recepcao_local;
	}
	
	public function setRecepcao_local($recepcao_local) {
	   $this->recepcao_local = $recepcao_local;
	}

	public function getObjetivo() {
	   return $this->objetivo;
	}
	
	public function setObjetivo($objetivo) {
	   $this->objetivo = $objetivo;
	}

	public function getId_cadastro() {
	   return $this->id_cadastro;
	}
	
	public function setId_cadastro($id_cadastro) {
	   $this->id_cadastro = $id_cadastro;
	}

	public function getData_cadastro() {
	   return $this->data_cadastro;
	}
	
	public function setData_cadastro($data_cadastro) {
	   $this->data_cadastro = $data_cadastro;
	}

	public function getData_atualizacao() {
	   return $this->data_atualizacao;
	}
	
	public function setData_atualizacao($data_atualizacao) {
	   $this->data_atualizacao = $data_atualizacao;
	}

	public function cadastrar(){

		$dados = array(

				'id_empresa' => $this->getId_empresa(),
				'numero_processo' => $this->getNumero_processo(),
				'numero_processo_formatado' => $this->getNumero_processo_formatado(),
				'data_cadastramento' => $this->getData_cadastramento(),
				'nome_interessado' => $this->getNome_interessado(),
				'tipo_processo' => $this->getTipo_processo(),
				'local_processo' => $this->getLocal_processo(),
				'situacao_processo' => $this->getSituacao_processo(),
				'envio_data' => $this->getEnvio_data(),
				'envio_local' => $this->getEnvio_local(),
				'recepcao_data' => $this->getRecepcao_data(),
				'recepcao_local' => $this->getRecepcao_local(),
				'objetivo' => $this->getObjetivo(),
				'id_cadastro' => $this->getId_cadastro(),
				'data_cadastro' => $this->getData_cadastro()
		);
	
		if ($this->db->insert('dtb_tramitacao_processo_sipro', $dados)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}

	public function editar(){
	
		$dados = array(				

				'id_empresa' => $this->getId_empresa()
		);
	
		if ($this->db->update('dtb_tramitacao_processo_sipro', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function listar() {
		$this->db->select('p.id, p.numero_processo, p.numero_processo_formatado, p.data_cadastramento, p.nome_interessado, p.tipo_processo, p.local_processo, p.situacao_processo, p.envio_data, p.envio_local, p.recepcao_data, p.recepcao_local, p.objetivo, p.id_cadastro, p.data_cadastro, p.data_atualizacao, e.razao_social, e.cnpj_completo');		
		$this->db->join('dtb_empresas as e', 'p.id_empresa = e.id', 'left');		
		return $this->db->get('dtb_tramitacao_processo_sipro as p')->result();
	}

	public function qtd_sipro() {
		$this->db->select('count(*) as qtd');		
		$this->db->join('dtb_empresas as e', 'p.id_empresa = e.id', 'left');		
		return $this->db->get('dtb_tramitacao_processo_sipro as p')->row();
	}

	public function excluir(){
		return $this->db->delete('dtb_tramitacao_processo_sipro', "id = {$this->getId()}");
	}

	public function pesquisar_processo_id() {
		$this->db->select('*');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_tramitacao_processo_sipro')->row();
	}

	public function buscar_empresas(){
		$this->db->select('dtb_empresas.id, dtb_empresas.razao_social, dtb_empresas.cnpj_completo');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtb_empresas.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();

	}

	public function verificar_cadastro_duplicado($campo) {

		$this->db->select('id');
		 
		$this->db->where("numero_processo", $campo);

		return $this->db->get('dtb_tramitacao_processo_sipro')->row();
	}

	public function qtd_notificacoes_consulta_sipro() {

		$this->db->select('COUNT(distinct(notificacoesprocessosipro.id)) AS valor');
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','notificacoesprocessosipro.id_empresa = e.id','left');
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		$this->db->where("status", 1);
		return $this->db->get('notificacoesprocessosipro')->row();
	}

	public function notificacoes_consulta_sipro() {
		$this->db->select('n.id, n.id_empresa, n.id_processo_sipro, n.numero_processo, n.numero_processo_formatado, n.objetivo, n.status, n.envio_data, n.envio_local, n.recepcao_data, n.recepcao_local, n.data_atualizacao, e.razao_social');

		$this->db->join('dtb_empresas e','n.id_empresa = e.id','left');
			
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		$this->db->where("status", 1);
		$this->db->order_by("n.id", "desc");
		$this->db->group_by("n.id");
		return $this->db->get('notificacoesprocessosipro as n')->result();
	}

	public function lerMensagem($id){
		$this->db->set('status', 0);
		$this->db->where('id', $id);
		$this->db->update('notificacoesprocessosipro');
	}
}