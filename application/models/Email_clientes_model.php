<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email_clientes_model extends CI_Model { 

	private $id;
	private $id_empresa;
	private $email_destinatario;
	private $assunto;
	private $mensagem;
	private $status;
	private $importante;
	private $excluido;
	private $com_estrela;
	private $enviado_recebido;
	private $contador_cliente;
	private $possui_anexo;
	private $data_cadastro;
	private $id_cadastro;

	function __construct()
	{
		parent::__construct();
	}
	
	public function getId() {
	   return $this->id;
	}
	
	public function setId($id) {
	   $this->id = $id;
	}

	public function getIdEmpresa() {
	   return $this->id_empresa;
	}
	
	public function setIdEmpresa($id_empresa) {
	   $this->id_empresa = $id_empresa;
	}

	public function getEmailDestinatario() {
	   return $this->email_destinatario;
	}
	
	public function setEmailDestinatario($email_destinatario) {
	   $this->email_destinatario = $email_destinatario;
	}

	public function getAssunto() {
	   return $this->assunto;
	}
	
	public function setAssunto($assunto) {
	   $this->assunto = $assunto;
	}

	public function getMensagem() {
	   return $this->mensagem;
	}
	
	public function setMensagem($mensagem) {
	   $this->mensagem = $mensagem;
	}

	public function getStatus() {
	   return $this->status;
	}
	
	public function setStatus($status) {
	   $this->status = $status;
	}

	public function getImportante() {
	   return $this->importante;
	}
	
	public function setImportante($importante) {
	   $this->importante = $importante;
	}

	public function getExcluido() {
	   return $this->excluido;
	}
	
	public function setExcluido($excluido) {
	   $this->excluido = $excluido;
	}

	public function getComEstrela() {
	   return $this->com_estrela;
	}
	
	public function setComEstrela($com_estrela) {
	   $this->com_estrela = $com_estrela;
	}

	public function getEnviadoRecebido() {
	   return $this->enviado_recebido;
	}
	
	public function setEnviadoRecebido($enviado_recebido) {
	   $this->enviado_recebido = $enviado_recebido;
	}

	public function getContadorCliente() {
	   return $this->contador_cliente;
	}
	
	public function setContadorCliente($contador_cliente) {
	   $this->contador_cliente = $contador_cliente;
	}

	public function getPossuiAnexo() {
	   return $this->possui_anexo;
	}
	
	public function setPossuiAnexo($possui_anexo) {
	   $this->possui_anexo = $possui_anexo;
	}

	public function getDataCadastro() {
	   return $this->data_cadastro;
	}
	
	public function setDataCadastro($data_cadastro) {
	   $this->data_cadastro = $data_cadastro;
	}

	public function getIdCadastro() {
	   return $this->id_cadastro;
	}
	
	public function setIdCadastro($id_cadastro) {
	   $this->id_cadastro = $id_cadastro;
	}

	// ----------------------------------------- PAINEL DO CONTADOR ---------------------------------------------

	public function enviar_email_contador(){
	
		$dados = array(

				'id_empresa' => $this->getIdEmpresa(),
				'email_destinatario' => $this->getEmailDestinatario(),
				'assunto' => $this->getAssunto(),
				'mensagem' => $this->getMensagem(),
				'enviado_recebido' => 'Enviado',
				'contador_cliente' => 'Contador',
				'status' => 'Nao_Lida',
				'possui_anexo' => $this->getPossuiAnexo(),
				'data_cadastro' => $this->getDataCadastro(),
				'id_cadastro' => $this->getIdCadastro()
		);
	
		if ($this->db->insert('dtb_clientes_emails', $dados)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}		
	}

	public function ler_email_contador(){

		$dados = array(				
				
				'status' => 'Lida'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function definir_email_importante_contador(){

		$dados = array(				
				
				'importante' => 'S'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function definir_email_estrela_contador(){

		$dados = array(				
				
				'com_estrela' => 'S'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function remover_estrela_contador(){

		$dados = array(				
				
				'com_estrela' => 'N'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function excluir_email_contador(){

		$dados = array(				
				
				'excluido' => 'S'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function listar_emails_contador($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Cliente');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_emails_nao_lidos_contador($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Cliente');
		$this->db->where('e.status', 'Nao_Lida'); 
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_emails_com_estrela_contador($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.com_estrela', 'S');
		$this->db->where('e.contador_cliente', 'Cliente');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_email_enviados_contador($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Contador');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_email_excluidos_contador($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Cliente');
		$this->db->where('e.excluido', 'S');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function qtd_emails_nao_lidas_contador($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('status', 'Nao_Lida');
		$this->db->where('contador_cliente', 'Cliente');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	} 

	public function qtd_emails_com_estrelas_contador($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('com_estrela', 'S');
		$this->db->where('contador_cliente', 'Cliente');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	}

	public function qtd_emails_enviados_contador($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('contador_cliente', 'Contador');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	}

	public function qtd_emails_excluidos_contador($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('contador_cliente', 'Cliente');
		$this->db->where('excluido', 'S');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	}

	public function pesquisar_email_id(){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, ce.email, e.email_destinatario, e.assunto, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');
		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left');
		$this->db->where('e.id', $this->getId());
		return $this->db->get('dtb_clientes_emails as e')->row();
	}

	// ---------------------------------------- PAINEL DO CLIENTE -----------------------------------------

	public function enviar_email_cliente(){
	
		$dados = array(

				'id_empresa' => $this->getIdEmpresa(),
				'email_destinatario' => $this->getEmailDestinatario(),
				'assunto' => $this->getAssunto(),
				'mensagem' => $this->getMensagem(),
				'enviado_recebido' => 'Enviado',
				'contador_cliente' => 'Cliente',
				'status' => 'Nao_Lida',
				'possui_anexo' => $this->getPossuiAnexo(),
				'data_cadastro' => $this->getDataCadastro(),
				'id_cadastro' => $this->getIdCadastro()
		);
	
		if ($this->db->insert('dtb_clientes_emails', $dados)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}		
	}

	public function ler_email_cliente(){

		$dados = array(				
				
				'status' => 'Lida'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function definir_email_importante_cliente(){

		$dados = array(				
				
				'importante' => 'S'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function definir_email_estrela_cliente(){

		$dados = array(				
				
				'com_estrela' => 'S'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function remover_estrela_cliente(){

		$dados = array(				
				
				'com_estrela' => 'N'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function excluir_email_cliente(){

		$dados = array(				
				
				'excluido' => 'S'
		);
	
		if ($this->db->update('dtb_clientes_emails', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function listar_emails_cliente($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Contador');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_emails_nao_lidos_cliente($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Contador');
		$this->db->where('e.status', 'Nao_Lida');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_emails_com_estrela_cliente($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.com_estrela', 'S');
		$this->db->where('e.contador_cliente', 'Contador');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_email_enviados_cliente($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Cliente');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function listar_email_excluidos_cliente($id_empresa){

		$this->db->select('e.id, e.id_empresa, ce.razao_social, e.email_destinatario, e.assunto, e.possui_anexo, e.mensagem, e.status, e.importante, e.excluido, e.com_estrela, e.enviado_recebido, e.data_cadastro');

		$this->db->join('dtb_clientes_empresas as ce', 'e.id_empresa = ce.id', 'left'); 
		
		$this->db->where('e.id_empresa', $id_empresa);
		$this->db->where('e.contador_cliente', 'Contador');
		$this->db->where('e.excluido', 'S');
		$this->db->order_by('e.data_cadastro desc');
		return $this->db->get('dtb_clientes_emails as e')->result();
	}

	public function qtd_emails_nao_lidas_cliente($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('status', 'Nao_Lida');
		$this->db->where('contador_cliente', 'Contador');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	} 

	public function qtd_emails_com_estrelas_cliente($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('com_estrela', 'S');
		$this->db->where('contador_cliente', 'Contador');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	}

	public function qtd_emails_enviados_cliente($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('contador_cliente', 'Cliente');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	}

	public function qtd_emails_excluidos_cliente($id){

		$this->db->select('COUNT(distinct(dtb_clientes_emails.id)) AS valor');

		$this->db->where('contador_cliente', 'Contador');
		$this->db->where('excluido', 'S');
		$this->db->where('id_empresa', $id);

		return $this->db->get('dtb_clientes_emails')->row();
	}
}