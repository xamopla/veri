<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends CI_Model {

	private $id;
	private $ativo;
	private $nome;
	private $email;
	private $id_contabilidade;
	
	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getAtivo() {
		return $this->ativo;
	}
	public function setAtivo($ativo) {
		$this->ativo = $ativo;
		return $this;
	}

	public function getNome() {
		return $this->nome;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}

	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}

	public function cadastrar(){
	
		$dados = array(
	
				'ativo' => $this->getAtivo(),
				'nome' => $this->getNome(),
				'email' => $this->getEmail(),
				'id_contabilidade' => $this->getIdContabilidade()
		);
	
		if ($this->db->insert('dtb_email', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function listar() {
		$this->db->select('id, ativo, nome, email');
		return $this->db->get('dtb_email')->result();
	}

	public function excluir(){
		return $this->db->delete('dtb_email', "id = {$this->getId()}");
	}

	public function pesquisar_email_id() {
		$this->db->select('id, ativo, nome, email');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_email')->row();
	}

	public function editar(){
	
		$dados = array(
				'ativo' => $this->getAtivo(),				
				'nome' => $this->getNome(),
				'email' => $this->getEmail()
		);
	
		if ($this->db->update('dtb_email', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function qtd_emails_cadastrados() {
		$this->db->select('COUNT(id) AS valor');
		return $this->db->get('dtb_email')->row();
	}

}