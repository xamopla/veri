<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ecac_model extends CI_Model {
	private $filtro_situacao;

	public function getFiltro_situacao() {
	    return $this->filtro_situacao;
	}
	 
	public function setFiltro_situacao($filtro_situacao) {
	    $this->filtro_situacao = $filtro_situacao;
	}

	public function listar_caixa_postal(){
		$this->db->select('dtb_ecac_caixa_postal.lidas, dtb_ecac_caixa_postal.nao_lidas, dtbe.razao_social, dtbe.cnpj_completo, dtbe.id, dtb_ecac_caixa_postal.id as id_caixa_postal, dtbe.cnpj');

		$this->db->join('dtb_empresas dtbe','dtb_ecac_caixa_postal.cnpj_data = dtbe.cnpj', 'left');

		if($this->getFiltro_situacao() == 'IMPORTANTES'){
			$this->db->join('dtb_ecac_caixa_postal_mensagem ca','dtb_ecac_caixa_postal.id = ca.caixa_postal_id', 'left');
		}

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		if($this->getFiltro_situacao() != 'TODAS'){
			if($this->getFiltro_situacao() == 'LIDAS'){
				$this->db->where("dtb_ecac_caixa_postal.nao_lidas = 0");
			}elseif($this->getFiltro_situacao() == 'NAO_LIDAS'){
				$this->db->where("dtb_ecac_caixa_postal.nao_lidas != 0");
			}elseif($this->getFiltro_situacao() == 'IMPORTANTES'){
				$this->db->where("ca.lida = 0");
				$this->db->where("ca.importante = 1");
			}
		}

		$this->db->order_by("dtb_ecac_caixa_postal.id", "desc");
		$this->db->group_by('dtb_ecac_caixa_postal.id');
		return $this->db->get('dtb_ecac_caixa_postal')->result();
	}

	public function qtd_caixa_postal_importante(){
		$this->db->select('COUNT(ca.id) as qtd');

		$this->db->join('dtb_empresas dtbe','dtb_ecac_caixa_postal.cnpj_data = dtbe.cnpj', 'left');
		$this->db->join('dtb_ecac_caixa_postal_mensagem ca','dtb_ecac_caixa_postal.id = ca.caixa_postal_id', 'left');

		

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->where("ca.lida = 0");
		$this->db->where("ca.importante = 1");
		return $this->db->get('dtb_ecac_caixa_postal')->row();
	}

	public function qtd_caixa_postal_importante_by_id($id){
		$this->db->select('COUNT(*) as qtd');
		$this->db->where("ca.caixa_postal_id", $id);
		$this->db->where("ca.lida = 0");
		$this->db->where("ca.importante = 1");
		return $this->db->get('dtb_ecac_caixa_postal_mensagem ca')->row();
	}

	public function listar_caixa_postal_by_empresa($id){
		$this->db->select('*');
		$this->db->where('dtb_ecac_caixa_postal_mensagem.caixa_postal_id', $id);
		$this->db->order_by("dtb_ecac_caixa_postal_mensagem.id", "asc");
		$this->db->group_by('dtb_ecac_caixa_postal_mensagem.id');
		return $this->db->get('dtb_ecac_caixa_postal_mensagem')->result();
	}

	public function conteudo_mensagem($id){
		$this->db->select('conteudo');
		$this->db->where('dtb_ecac_caixa_postal_mensagem.id', $id);
		return $this->db->get('dtb_ecac_caixa_postal_mensagem')->row();
	}

	public function find_cnpj($id_caixa_postal){
		$this->db->select('cnpj_data');
		$this->db->where('dtb_ecac_caixa_postal.id', $id_caixa_postal);
		return $this->db->get('dtb_ecac_caixa_postal')->row();
	}

	public function find_certificado($cnpj){
		$this->db->select('*');
		$this->db->where('cnpj_data', $cnpj);
		return $this->db->get('dtb_certificado')->row();
	}

	public function find_mensagem($id){
		$this->db->select('id_mensagem');
		$this->db->where('dtb_ecac_caixa_postal_mensagem.id', $id);
		return $this->db->get('dtb_ecac_caixa_postal_mensagem')->row();
	}

	public function update_lida($id, $conteudo){
		$campos = array(
			'lida' => 1,
			'conteudo' => $conteudo
		);

		$this->db->where('id',$id);
		$this->db->update('dtb_ecac_caixa_postal_mensagem',$campos);

		if ($this->db->affected_rows() == 1) {
			return 1;
		}else{
			return 0;
		}
	}

	public function update_lida_caixa_postal($id){
		$sql = 'UPDATE dtb_ecac_caixa_postal set nao_lidas = (nao_lidas - 1) where id = '.$id;
		$this->db->query($sql);
	}

	public function listar_empresas_situacao_fiscal_pendecia(){

		$this->db->select('df.id, df.cnpj_data, df.possui_pendencia, df.data_execucao, df.caminho_download, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.inscricao_estadual_completo, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.forma_pagamento'); 
		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 1); 

		// $this->db->order_by("dtb_ecac_caixa_postal_mensagem.id", "asc");
		// $this->db->group_by('dtb_ecac_caixa_postal_mensagem.id');
		return $this->db->get('dtb_situacao_fiscal as df')->result();
	}

	public function listar_empresas_situacao_fiscal_regular(){

		$this->db->select('df.id, df.cnpj_data, df.possui_pendencia, df.data_execucao, df.caminho_download, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.inscricao_estadual_completo, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.forma_pagamento'); 
		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left');

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 0);

		// $this->db->order_by("dtb_ecac_caixa_postal_mensagem.id", "asc");
		// $this->db->group_by('dtb_ecac_caixa_postal_mensagem.id');
		return $this->db->get('dtb_situacao_fiscal as df')->result();
	}

	public function listar_empresas_situacao_fiscal_geral(){

		$this->db->select('df.id, df.cnpj_data, df.possui_pendencia, df.data_execucao, df.caminho_download, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.inscricao_estadual_completo, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.forma_pagamento'); 
		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		// $this->db->order_by("dtb_ecac_caixa_postal_mensagem.id", "asc");
		// $this->db->group_by('dtb_ecac_caixa_postal_mensagem.id');
		return $this->db->get('dtb_situacao_fiscal as df')->result();
	}

	public function qtd_empresas_situacao_fiscal_pendencias(){

		$this->db->select('COUNT(distinct(df.id)) AS valor'); 

		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 1);

		return $this->db->get('dtb_situacao_fiscal as df')->row();
	}

	public function qtd_empresas_situacao_fiscal_regulares(){

		$this->db->select('COUNT(distinct(df.id)) AS valor'); 

		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 0);

		return $this->db->get('dtb_situacao_fiscal as df')->row();
	}

	public function qtd_empresas_com_parcelamento_simples_nacional(){

		$this->db->select('COUNT(distinct(df.id)) AS valor'); 

		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 
		$this->db->join('dtb_parcelas_emitidas as p', 'df.cnpj_data = p.cnpj_data', 'INNER'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pedido', 1);
		return $this->db->get('dtb_consulta_pedidos as df')->row();
	}

	public function listar_empresas_parcelamento_simples(){

		$this->db->select('df.id, df.cnpj_data, df.possui_pedido, df.data_execucao, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.inscricao_estadual_completo, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.forma_pagamento'); 
		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left');
		$this->db->join('dtb_parcelas_emitidas as p', 'df.cnpj_data = p.cnpj_data', 'INNER'); 
		
		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pedido', 1); 
		$this->db->group_by('df.cnpj_data');
		return $this->db->get('dtb_consulta_pedidos as df')->result();
	}

	public function marcar_pendecia($id){

		$dados = array(				
				
				'possui_pendencia' => '1'
		);
	
		if ($this->db->update('dtb_situacao_fiscal', $dados, "id={$id}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function listar_empresas_cadin(){

		$this->db->select('df.id, df.cnpj_data, df.possui_pendencia, df.data_execucao, df.caminho_download, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.inscricao_estadual_completo, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.forma_pagamento'); 
		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->group_by('df.cnpj_data');
		return $this->db->get('dtb_situacao_cadin as df')->result();
	}

	public function listar_empresas_cadin_pendencias(){

		$this->db->select('df.id, df.cnpj_data, df.possui_pendencia, df.data_execucao, df.caminho_download, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.inscricao_estadual_completo, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.forma_pagamento'); 
		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 1);

		$this->db->group_by('df.cnpj_data');
		return $this->db->get('dtb_situacao_cadin as df')->result();
	}

	public function listar_empresas_cadin_regulares(){

		$this->db->select('df.id, df.cnpj_data, df.possui_pendencia, df.data_execucao, df.caminho_download, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.inscricao_estadual_completo, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.forma_pagamento'); 
		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 0);

		$this->db->group_by('df.cnpj_data');
		return $this->db->get('dtb_situacao_cadin as df')->result();
	}

	public function qtd_empresas_cadin(){
		$this->db->select('COUNT(distinct(df.cnpj_data)) AS valor'); 

		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 1);

		return $this->db->get('dtb_situacao_cadin as df')->row();
	}


	public function qtd_empresas_cadin_regulares(){
		$this->db->select('COUNT(distinct(df.cnpj_data)) AS valor'); 

		$this->db->join('dtb_empresas as e', 'df.cnpj_data = e.cnpj', 'left'); 

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where('df.possui_pendencia', 0);

		return $this->db->get('dtb_situacao_cadin as df')->row();
	}

	public function qtd_eprocessos(){

		$this->db->select('COUNT(distinct(df.id)) AS valor');  

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->join('dtb_empresas as e', 'df.cnpj = e.cnpj', 'left'); 
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		return $this->db->get('dtb_ecac_eprocessos_ativos as df')->row();
	}

	public function qtd_eprocessos_inativos(){

		$this->db->select('COUNT(distinct(df.id)) AS valor');  
		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->join('dtb_empresas as e', 'df.cnpj = e.cnpj', 'left'); 
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		return $this->db->get('dtb_ecac_eprocessos_inativos as df')->row();
	}

	public function listar_eprocessos_todos(){
		$id_func = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$sql1 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 1 as ativo FROM dtb_ecac_eprocessos_ativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj) WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY ep.idProcesso ";

			$sql2 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 0 as ativo FROM dtb_ecac_eprocessos_inativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj)  WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY ep.idProcesso ";


			$sql = $sql1." UNION ".$sql2;
		}else{
			$sql1 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 1 as ativo FROM dtb_ecac_eprocessos_ativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj) GROUP BY ep.idProcesso";

			$sql2 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 0 as ativo FROM dtb_ecac_eprocessos_inativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj) GROUP BY ep.idProcesso";


			$sql = $sql1." UNION ".$sql2;
		}

		return $this->db->query($sql)->result();

	}

	public function listar_eprocessos_ativos(){
		$id_func = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$sql1 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 1 as ativo FROM dtb_ecac_eprocessos_ativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj) WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY ep.idProcesso ";
		}else{
			$sql1 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 1 as ativo FROM dtb_ecac_eprocessos_ativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj) GROUP BY ep.idProcesso ";
		}
		

		return $this->db->query($sql1)->result();
	}

	public function listar_eprocessos_inativos(){

		$id_func = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$sql2 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 0 as ativo FROM dtb_ecac_eprocessos_inativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj) WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY ep.idProcesso ";
		}else{
			$sql2 = "SELECT ep.id, ep.idProcesso, ep.dataProtocolo, ep.dataProtocoloFormatada, ep.grupo, ep.indicadorVersaoProcesso, ep.localizacao, ep.natureza, ep.niInteressadoFormatado, ep.nomeInteressado, ep.numero, ep.numeroFormatado, ep.numeroProcessoProcedimentoFormatado, ep.situacao, ep.subtipo, ep.tipo, e.razao_social, e.cnpj_completo, e.inscricao_estadual_completo, 0 as ativo FROM dtb_ecac_eprocessos_inativos as ep LEFT JOIN dtb_empresas e ON trim(ep.cnpj) = trim(e.cnpj) GROUP BY ep.idProcesso ";
		}


		

		return $this->db->query($sql2)->result();
	}

	public function listar_historico_processo_ativo($id){
		$this->db->select('*');  
		$this->db->where('idProcesso', $id); 
		$this->db->order_by('id asc');
		return $this->db->get('dtb_ecac_eprocessos_ativos_historico')->result();
	}

	public function listar_historico_processo_inativo($id){
		$this->db->select('*');  
		$this->db->where('idProcesso', $id); 
		$this->db->order_by('id asc');
		return $this->db->get('dtb_ecac_eprocessos_inativos_historico')->result();
	}
}