<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tentativa_acesso_model extends CI_Model {

	private $id;

	private $nome_usuario;
	private $senha_usada;
	private $data_hora;
	private $ip;

	//CONSTRUTOR
	public function __construct(){
		parent::__construct();
	}

	//GETTERS E SETTERS
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getNomeUsuario() {
		return $this->nome_usuario;
	}
	public function setNomeUsuario($nome_usuario) {
		$this->nome_usuario = $nome_usuario;
		return $this;
	}

	public function getSenhaUsada() {
		return $this->senha_usada;
	}
	public function setSenhaUsada($senha_usada) {
		$this->senha_usada = $senha_usada;
		return $this;
	}

	public function getDataHora() {
		return $this->data_hora;
	}
	public function setDataHora($data_hora) {
		$this->data_hora = $data_hora;
		return $this;
	}

	public function getIp() {
		return $this->ip;
	}
	public function setIp($ip) {
		$this->ip = $ip;
		return $this;
	}

	public function tentativa_de_logar(){

		$dados = array(
	
				'nome_usuario' => $this->getNomeUsuario(),
				'senha_usada' => $this->getSenhaUsada(),
				'data_hora' => $this->getDataHora(),
				'ip' => $this->getIp()
		);
	
		if ($this->db->insert('tentativas_acesso', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

}