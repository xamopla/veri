<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Julgamento_model extends CI_Model {

 	private $id;
 	private $data_do_julgamento;
 	private $sessao;
 	private $local_do_julgamento;
 	private $data_disponibilizacao_informacao;
 	private $relator;
 	private $auto_de_infracao;
 	private $tipo_de_recurso;
 	private $recorrente;
 	private $recorridos;
 	private $autuantes;
 	private $data_cadastro;
 	private $data_atualizacao;

 	private $id_empresa;
 	private $cnpj;
 	private $status;

 	public function __construct(){
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getDataDoJulgamento() {
		return $this->data_do_julgamento;
	}
	public function setDataDoJulgamento($data_do_julgamento) {
		$this->data_do_julgamento = $data_do_julgamento;
		return $this;
	}

	public function getSessao() {
		return $this->sessao;
	}
	public function setSessao($sessao) {
		$this->sessao = $sessao;
		return $this;
	}

	public function getLocalDoJulgamento() {
		return $this->local_do_julgamento;
	}
	public function setLocalDoJulgamento($local_do_julgamento) {
		$this->local_do_julgamento = $local_do_julgamento;
		return $this;
	}

	public function getDataDisponibilizacaoInformacao() {
		return $this->data_disponibilizacao_informacao;
	}
	public function setDataDisponibilizacaoInformacao($data_disponibilizacao_informacao) {
		$this->data_disponibilizacao_informacao = $data_disponibilizacao_informacao;
		return $this;
	}

	public function getRelator() {
		return $this->relator;
	}
	public function setRelator($relator) {
		$this->relator = $relator;
		return $this;
	}

	public function getAutoDeInfracao() {
		return $this->auto_de_infracao;
	}
	public function setAutoDeInfracao($auto_de_infracao) {
		$this->auto_de_infracao = $auto_de_infracao;
		return $this;
	}

	public function getTipoDeRecurso() {
		return $this->tipo_de_recurso;
	}
	public function setTipoDeRecurso($tipo_de_recurso) {
		$this->tipo_de_recurso = $tipo_de_recurso;
		return $this;
	}

	public function getRecorrente() {
		return $this->recorrente;
	}
	public function setRecorrente($recorrente) {
		$this->recorrente = $recorrente;
		return $this;
	}

	public function getRecorridos() {
		return $this->recorridos;
	}
	public function setRecorridos($recorridos) {
		$this->recorridos = $recorridos;
		return $this;
	}

	public function getAutuantes() {
		return $this->autuantes;
	}
	public function setAutuantes($autuantes) {
		$this->autuantes = $autuantes;
		return $this;
	}

	public function getDataCadastro() {
		return $this->data_cadastro;
	}
	public function setDataCadastro($data_cadastro) {
		$this->data_cadastro = $data_cadastro;
		return $this;
	}

	public function getDataAtualizacao() {
		return $this->data_atualizacao;
	}
	public function setDataAtualizacao($data_atualizacao) {
		$this->data_atualizacao = $data_atualizacao;
		return $this;
	}

	public function getIdEmpresa() {
		return $this->id_empresa;
	}
	public function setIdEmpresa($id_empresa) {
		$this->id_empresa = $id_empresa;
		return $this;
	}

	public function getCnpj() {
		return $this->cnpj;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}

	public function getStatus() {
		return $this->status;
	}
	public function setStatus($status) {
		$this->status = $status;
		return $this;
	}

	public function listar(){

		$this->db->select('db.id, db.id_empresa, db.auto_de_infracao as auto_de_infracao, db.data_do_julgamento as data_do_julgamento, db.local_do_julgamento as local_do_julgamento, db.sessao as sessao, db.relator as relator, db.recorrente as recorrente, db.recorridos as recorridos, db.autuantes as autuantes, db.tipo_de_recurso as tipo_de_recurso, dtbe.razao_social as razao_social, dtbe.cnpj_completo as cnpj_completo, dtbe.nome_fantasia');
		$this->db->from('dtb_julgamentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		} 

		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function qtd_empresas_em_julgamento(){		

		$this->db->select('COUNT(distinct(dtb_julgamentos.id)) AS valor');

		$this->db->join('dtb_empresas dtbe','dtb_julgamentos.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		} 
		return $this->db->get('dtb_julgamentos')->row();
	}

}