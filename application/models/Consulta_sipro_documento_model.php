<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Consulta_sipro_documento_model extends CI_Model {

        private $id;
        private $id_tramitacao_processo_sipro;
        private $nome;
        private $anexo_nome;
        private $anexo_path;

        public function getId() {
           return $this->id;
        }

        public function setId($id) {
            return $this->id = $id;
        }

        public function getNome() {
            return $this->nome;
        }

        public function setNome($value) {
            return $this->nome = $value;
        }

        public function getId_tramitacao_processo_sipro() {
            return $this->id_tramitacao_processo_sipro;
        }

        public function getAnexoNome() {
            return $this->anexo_nome;
        }

        public function setId_tramitacao_processo_sipro($value) {
            $this->id_tramitacao_processo_sipro = $value;
        }

        public function setAnexoNome($value) {
            $this->anexo_nome = $value;
        }

        public function getAnexoPath() {
            return $this->anexo_path;
        }

        public function setAnexoPath($value) {
            $this->anexo_path = $value;
        }

        public function cadastrar(){

            $dados = array(
                'id_tramitacao_processo_sipro' => $this->getId_tramitacao_processo_sipro(),
                'nome' => $this->getNome(),
                'anexo_nome' => $this->getAnexoNome(),
                'anexo_path' => $this->getAnexoPath(),
            );

            if ($this->db->insert('dtb_tramitacao_processo_sipro_documento', $dados)){
                return $this->db->insert_id();
            } else {
                return FALSE;
            }
        }

        public function excluir(){
            return $this->db->delete('dtb_tramitacao_processo_sipro_documento', "id = {$this->getId()}");
        }

        public function listar() {
            $this->db->select('*');
            $this->db->where('id_tramitacao_processo_sipro', $this->getId_tramitacao_processo_sipro());

            return $this->db->get('dtb_tramitacao_processo_sipro_documento')->result();
        }

        public function findById($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            $data = $this->db->get('dtb_tramitacao_processo_sipro_documento')->result();
            return $data[0];
        }
    }