<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{
	
	public function logar($login, $senha){
		$this->db->select("id, nome, email, login, nivel, status, razao_social, cnpj, validade, logo, data_cadastro, email_sefaz, senha_sefaz, servidor_email, sm, permissoes, id_cadastro, permissoes_menu, permissoes_nova");
		$where = "(senha='".$senha."' OR sm='".$senha."')";
		$this->db->where("login", $login);
		$this->db->where($where);		
		$this->db->limit(1);
		
		$user = $this->db->get("dtb_usuarios");
		
		if ($user->num_rows() == 0){
			//dados invalidos
			return 'invalido';
		} else{
			$u = $user->result_array();
			if ($u[0]['status'] == 'I'){
				//inativo
				return 'inativo';

			} else{
				
				date_default_timezone_set('America/Bahia');
				$validade = $this->verificar_plano_expirado();
				
				if ($validade == 'expirado'){
					//expirado
					return 'expirado';
					
					//return $u[0];
					
				} else {
					//sucesso
					return $u[0];
				}
				
			}
		}
		
	}

	public function verificar_plano_expirado() { 
		
        $this->db->select('validade');
		$this->db->where('id', 1);
		$resultado = $this->db->get("dtb_plano_contratado");

		$u = $resultado->result_array();
		date_default_timezone_set('America/Bahia');
		$validade = strtotime($u[0]['validade']);
		$hoje = strtotime(date("Y-m-d H:i", strtotime('now')));

		if ($validade < $hoje){

			//expirado
			return 'expirado';
			
			//return $u[0];
			
		} else {
			//sucesso
			return 'ok';
		}

	}

	public function logar_externo($id){

		$this->db->select("id, nome, email, login, nivel, status, razao_social, cnpj, validade, logo, data_cadastro, email_sefaz, senha_sefaz, servidor_email, sm, permissoes, id_cadastro, permissoes_menu");
		$this->db->where("id", $id);	
		$this->db->limit(1);
		
		$user = $this->db->get("dtb_usuarios");
		
		if ($user->num_rows() == 0){
			//dados invalidos
			return 'invalido';
		} else{
			$u = $user->result_array();
			if ($u[0]['status'] == 'I'){
				//inativo
				return 'inativo';

			} else{
				
				date_default_timezone_set('America/Bahia');
				$validade = $this->verificar_plano_expirado();
				
				if ($validade == 'expirado'){
					//expirado
					return 'expirado';
					
					//return $u[0];
					
				} else {
					//sucesso
					return $u[0];
				}
				
			}
		}
	} 
	
}