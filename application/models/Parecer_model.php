<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parecer_model extends CI_Model {

	private $id;
	private $id_empresa;
	private $numero_processo;
	private $data_inclusao;
	private $status_parecer;

	private $data_cadastro;
	private $id_cadastro;

	function __construct()
	{
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getIdEmpresa() {
		return $this->id_empresa;
	}
	public function setIdEmpresa($id_empresa) {
		$this->id_empresa = $id_empresa;
		return $this;
	}

	public function getNumeroProcesso() {
		return $this->numero_processo;
	}
	public function setNumeroProcesso($numero_processo) {
		$this->numero_processo = $numero_processo;
		return $this;
	}

	public function getDataInclusao() {
		return $this->data_inclusao;
	}
	public function setDataInclusao($data_inclusao) {
		$this->data_inclusao = $data_inclusao;
		return $this;
	}

	public function getStatusParecer() {
		return $this->status_parecer;
	}
	public function setStatusParecer($status_parecer) {
		$this->status_parecer = $status_parecer;
		return $this;
	}

	public function getDataCadastro() {
		return $this->data_cadastro;
	}
	public function setDataCadastro($data_cadastro) {
		$this->data_cadastro = $data_cadastro;
		return $this;
	}

	public function getIdCadastro() {
		return $this->id_cadastro;
	}
	public function setIdCadastro($id_cadastro) {
		$this->id_cadastro = $id_cadastro;
		return $this;
	}

	public function cadastrar(){
	
		$dados = array(

				'id_empresa' => $this->getIdEmpresa(),
				'numero_processo' => $this->getNumeroProcesso(),
				'data_inclusao' => $this->getDataInclusao(),
				'status_parecer' => $this->getStatusParecer(),
				'data_cadastro' => $this->getDataCadastro(),
				'id_cadastro' => $this->getIdCadastro()
		);
	
		if ($this->db->insert('dtb_parecer', $dados)){
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}

	public function editar(){
	
		$dados = array(				
				
				'id_empresa' => $this->getIdEmpresa(),
				'numero_processo' => $this->getNumeroProcesso(),
				'data_inclusao' => $this->getDataInclusao(),
				'status_parecer' => $this->getStatusParecer(),
				'data_cadastro' => $this->getDataCadastro(),
				'id_cadastro' => $this->getIdCadastro()
		);
	
		if ($this->db->update('dtb_parecer', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}	
	}

	public function listar(){

		 $this->db->select('db.id,db.id_empresa, db.numero_processo, db.status_parecer, db.data_inclusao, db.id_cadastro, dtbe.razao_social, dtbe.cnpj_completo, dtbe.nome_fantasia, dtbe.senha_sefaz, dtbe.login_sefaz');
		$this->db->from('dtb_parecer db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function listar_processos_aguardando(){

		 $this->db->select('db.id,db.id_empresa, db.numero_processo, db.status_parecer, db.data_inclusao, db.id_cadastro, dtbe.razao_social, dtbe.cnpj_completo, dtbe.nome_fantasia, dtbe.senha_sefaz, dtbe.login_sefaz');
		$this->db->from('dtb_parecer db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where('db.status_parecer', 1);
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function listar_processos_finalizados(){

		 $this->db->select('db.id,db.id_empresa, db.numero_processo, db.status_parecer, db.data_inclusao, db.id_cadastro, dtbe.razao_social, dtbe.cnpj_completo, dtbe.nome_fantasia, dtbe.senha_sefaz, dtbe.login_sefaz');
		$this->db->from('dtb_parecer db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where('db.status_parecer', 0);
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	// REMOVER
	public function remover(){
		return $this->db->delete('dtb_parecer', "id = {$this->getId()}");
	}

	//PESQUISAR PARECER
	public function pesquisar_parecer_id() {

		$this->db->select('*');
		$this->db->where('id', $this->getId());
		return $this->db->get('dtb_parecer')->row();
	}

} 