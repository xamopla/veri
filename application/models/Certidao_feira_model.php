<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Certidao_feira_model extends CI_Model {

	public function listar(){
		$this->db->select('db.cnpj, db.cnpj_completo, db.inscricao_estadual, db.inscricao_estadual_completo, db.razao_social, db.inscricao_municipal, db.status');
		$this->db->from('dtb_certidao_feira db');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
			$this->db->where_in("e.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->group_by('db.cnpj_completo');
		return $this->db->get()->result();
	}

}