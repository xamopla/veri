<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ipva_clientes_model extends CI_Model {

	public function listar_ipva_clientes($id_empresa){

		$this->db->select('i.cnpj, i.id_empresa, i.inscricao_estadual, i.placas, ce.email, ce.celular'); 
		$this->db->join('dtb_clientes_empresas as ce', 'i.id_empresa = ce.id_empresa', 'left');

		$this->db->where('ce.id_empresa', $id_empresa); 
		$this->db->group_by('i.cnpj');
		return $this->db->get('dtb_ipva as i')->result();
	}

	public function find_id($id){

		$this->db->select('id_empresa as id'); 
		$this->db->where('id', $id); 
		return $this->db->get('dtb_clientes_empresas')->row();
	} 

	public function listar() {	
		$this->db->select('db.id_empresa, db.cnpj, db.inscricao_estadual, db.placas, dtbe.razao_social, dtbe.cnpj_completo');
		$this->db->from('dtb_ipva db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}  

		return $this->db->get()->result();
	}

	public function qtd_ipva() {	
		$this->db->select('count(*) as qtd');
		$this->db->from('dtb_ipva db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtbe.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("dtbe.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}  

		return $this->db->get()->row();
	}

}