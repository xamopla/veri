<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pgdas_model extends CI_Model {

	private $filtro_situacao;

	private $filtro_mes;
	private $filtro_ano;

	function __construct()
	{
		parent::__construct();
	}


	function setFiltroMes($filtro_mes) { 
		$this->filtro_mes = $filtro_mes; 
	}

	function getFiltroMes() { 
		return $this->filtro_mes; 
	}

	function setFiltroAno($filtro_ano) { 
		$this->filtro_ano = $filtro_ano; 
	}

	function getFiltroAno() { 
		return $this->filtro_ano; 
	}

	public function getFiltro_situacao() {
	    return $this->filtro_situacao;
	}
	 
	public function setFiltro_situacao($filtro_situacao) {
	    $this->filtro_situacao = $filtro_situacao;
	}


	public function listar(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL') AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sq = "SELECT max(numero_declaracao) FROM dtb_ecac_das WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') group by cnpj ";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO')  AND d.numero_declaracao IN (".$sq.") 
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato 
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{

			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sq = "SELECT max(numero_declaracao) FROM dtb_ecac_das WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') group by cnpj ";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.situacao_cadastral != 'BAIXADO')  AND d.numero_declaracao IN (".$sq.") 
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato     
			FROM dtb_empresas e 
			WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
			GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
		// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
		// 	if($this->getFiltro_situacao() == 'REGULAR'){
		// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 1 AND resultado.cnpj like "%0001%" ';
		// 	}else{
		// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 0 AND resultado.cnpj like "%0001%" ';
		// 	}
		// }
		return $this->db->query($sql_final)->result();
	} 


	public function listarFiltro(){
		$sql = "";
		$filtro_ano = $this->getFiltroAno();
		$filtro_mes = $this->getFiltroMes();

		if($filtro_mes < 10){
			$filtro_mes = "0".$filtro_mes;
		}

		$data_inicial = "'".$filtro_ano.'-'.$filtro_mes.'-01'."'";
		$data_final = "'".$filtro_ano.'-'.$filtro_mes.'-31'."'";

		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sq = "SELECT max(numero_declaracao) FROM dtb_ecac_das WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." group by cnpj ";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO') AND d.numero_declaracao IN (".$sq.")   
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato        
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;

		}else{
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO')   
				GROUP BY e.id";

			$sq = "SELECT max(numero_declaracao) FROM dtb_ecac_das WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." group by cnpj ";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  AND d.numero_declaracao IN (".$sq.")   
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato       
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
		// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
		// 	if($this->getFiltro_situacao() == 'REGULAR'){
		// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 1 AND resultado.cnpj like "%0001%" ';
		// 	}else{
		// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 0 AND resultado.cnpj like "%0001%" ';
		// 	}
		// }
		return $this->db->query($sql_final)->result();
	}

	public function listarFiltroByBotaoMes(){
		$sql = "";
		$filtro_ano = $this->getFiltroAno();
		$filtro_mes = $this->getFiltroMes();

		$data_inicial = "'".$filtro_ano.'-'.$filtro_mes.'-01'."'";
		$data_final = "'".$filtro_ano.'-'.$filtro_mes.'-31'."'";

		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO')   
				GROUP BY e.id";

			$sq = "SELECT max(numero_declaracao) FROM dtb_ecac_das WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." group by cnpj ";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  AND d.numero_declaracao IN (".$sq.")   
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato       
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{
			$sql_aux = "SELECT e.cnpj  
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sq = "SELECT max(numero_declaracao) FROM dtb_ecac_das WHERE DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') >= ".$data_inicial." AND DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') <= ".$data_final." group by cnpj ";


			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')   AND d.numero_declaracao IN (".$sq.")   
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato      
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO')  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
		// if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
		// 	if($this->getFiltro_situacao() == 'REGULAR'){
		// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 1 AND resultado.cnpj like "%0001%" ';
		// 	}else{
		// 		$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 0 AND resultado.cnpj like "%0001%" ';
		// 	}
		// }
		return $this->db->query($sql_final)->result();
	}
	

	public function qtd_dctf_pendente(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato      
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO')   
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{

			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO')   
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)  
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato      
			FROM dtb_empresas e 
			WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
			GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
		if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			if($this->getFiltro_situacao() == 'REGULAR'){
				$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 1 AND resultado.cnpj like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
			}else{
				$sql_final = 'SELECT COUNT(*) as qtd FROM ('.$sql.') as resultado where resultado.sem_pgdas = 0 AND resultado.cnpj like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
			}
		}
		return $this->db->query($sql_final)->row();
	} 


	////
	public function qtd_dctf_pendente_proximo_vencer(){
		$sql = "";
		$id_colaborador = $this->session->userdata['userprimesession']['id'];

		if($this->session->userdata['userprimesession']['nivel'] !=2){
			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj)
				WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato      
				FROM dtb_empresas e 
				WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' ) AND (e.situacao_cadastral != 'BAIXADO')   
				GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}else{

			$sql_aux = "SELECT e.cnpj 
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_ADD(NOW(), INTERVAL -1 MONTH) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND DATE_ADD(NOW(), INTERVAL -1 MONTH) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql1 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, d.compentencia, d.numero_declaracao, d.data_hora_transmissao, d.numero_das, d.data_hora_emissao, 1 as sem_pgdas, d.pago, d.caminho_download_recibo, d.caminho_download_declaracao, d.caminho_download_extrato    
				FROM dtb_empresas e 
				LEFT JOIN dtb_ecac_das as d ON trim(e.cnpj) = trim(d.cnpj) 
				WHERE DATE_ADD(NOW(), INTERVAL -1 MONTH) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND DATE_ADD(NOW(), INTERVAL -1 MONTH) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  
				GROUP BY e.id";

			$sql2 = "SELECT e.id, e.cnpj_completo, e.razao_social, e.cnpj, '' as compentencia, '' as numero_declaracao, '' as data_hora_transmissao, '' as numero_das, '' as data_hora_emissao, 0 as sem_pgdas, '' as pago, '' as  caminho_download_recibo, '' as caminho_download_declaracao, '' as caminho_download_extrato      
			FROM dtb_empresas e 
			WHERE cnpj not in (".$sql_aux.") AND (e.tipo_regime = 'SIMPLES NACIONAL' )  AND (e.situacao_cadastral != 'BAIXADO')  AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).") ) 
			GROUP BY e.id";

			$sql = $sql1." UNION ".$sql2;
		}

		$sql_final = 'SELECT * FROM ('.$sql.') as resultado WHERE resultado.cnpj_completo like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
		if($this->getFiltro_situacao() != null && $this->getFiltro_situacao() != "TODAS"){
			if($this->getFiltro_situacao() == 'REGULAR'){
				$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 1 AND resultado.cnpj like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
			}else{
				$sql_final = 'SELECT * FROM ('.$sql.') as resultado where resultado.sem_pgdas = 0 AND resultado.cnpj like "%0001%" AND resultado.cnpj not in (SELECT cnpj FROM dtb_empresas_excluidas_mei WHERE is_excluida = 0) ';
			}
		}
		return $this->db->query($sql_final)->row();
	} 


	public function update_simples($id){
		$this->db->set('tipo_regime', 'LUCRO PRESUMIDO');
		$this->db->where('id', $id);
		$this->db->update('dtb_empresas');
	}

	public function busca_dados_modal($numero){
		$this->db->select('*');
		$this->db->where("numero_declaracao = ".$numero);
		return $this->db->get('dtb_ecac_das')->row();
	}


	public function buscar_empresas_sem_movimento($mes){
		$this->db->select('*');
		$this->db->where('mes = "'.$mes.'"');
		return $this->db->get('dtb_pgdas_sem_movimento')->result();
	}


	public function marcar_sem_movimento($cnpj, $mes){
		date_default_timezone_set('America/Sao_Paulo');
		$usuario = "";
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		$dados = array(
	
				'mes' => $mes,
				'cnpj' => $cnpj,
				'data_alteracao' => date('Y-m-d H:i:s'),
				'nome_usuario' => $usuario
		);
	
		if ($this->db->insert('dtb_pgdas_sem_movimento', $dados)){
			return 1;
		} else {
			return 0;
		}
	}



	public function insere_notificacao($cnpj, $mes){
        
        date_default_timezone_set('America/Sao_Paulo');

        $dados = array( 

                'cnpj' => $cnpj,
                'mes' => $mes,
                'data' => date('Y-m-d H:i:s'),
                'descricao' => 'PGDAS sem movimento',
                'lida' => '0'
        );

        $this->db->insert('dtb_notificacao_pgdas_sem_movimento', $dados);
        return $this->db->insert_id();
    }
}