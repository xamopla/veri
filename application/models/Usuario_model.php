<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model{
	
	private $id;
	private $ie;
	private $cnpj;
	private $razao_social;
	private $nome_fantasia;
	private $cep;
	private $logradouro;
	private $numero;
	private $complemento;
	private $bairro;
	private $cidade;
	private $uf;
	private $telefone;
	private $email;
	private $celular;
	private $logo;
	private $status;
	private $data_cadastro;
	private $validade;
	private $id_cadastro;
	private $nivel;
	private $senha;
	private $nome;
	private $login;
	private $email_sefaz;
	private $senha_sefaz;
	private $servidor_email;
	
	public function __construct(){
		parent::__construct();
	}
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getIe() {
		return $this->ie;
	}
	public function setIe($ie) {
		$this->ie = $ie;
		return $this;
	}
	public function getCnpj() {
		return $this->cnpj;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}
	public function getRazaoSocial() {
		return $this->razao_social;
	}
	public function setRazaoSocial($razao_social) {
		$this->razao_social = $razao_social;
		return $this;
	}
	public function getNomeFantasia() {
		return $this->nome_fantasia;
	}
	public function setNomeFantasia($nome_fantasia) {
		$this->nome_fantasia = $nome_fantasia;
		return $this;
	}
	public function getCep() {
		return $this->cep;
	}
	public function setCep($cep) {
		$this->cep = $cep;
		return $this;
	}
	public function getLogradouro() {
		return $this->logradouro;
	}
	public function setLogradouro($logradouro) {
		$this->logradouro = $logradouro;
		return $this;
	}
	public function getNumero() {
		return $this->numero;
	}
	public function setNumero($numero) {
		$this->numero = $numero;
		return $this;
	}
	public function getComplemento() {
		return $this->complemento;
	}
	public function setComplemento($complemento) {
		$this->complemento = $complemento;
		return $this;
	}
	public function getBairro() {
		return $this->bairro;
	}
	public function setBairro($bairro) {
		$this->bairro = $bairro;
		return $this;
	}
	public function getCidade() {
		return $this->cidade;
	}
	public function setCidade($cidade) {
		$this->cidade = $cidade;
		return $this;
	}
	public function getUf() {
		return $this->uf;
	}
	public function setUf($uf) {
		$this->uf = $uf;
		return $this;
	}
	public function getTelefone() {
		return $this->telefone;
	}
	public function setTelefone($telefone) {
		$this->telefone = $telefone;
		return $this;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}
	public function getCelular() {
		return $this->celular;
	}
	public function setCelular($celular) {
		$this->celular = $celular;
		return $this;
	}
	public function getLogo() {
		return $this->logo;
	}
	public function setLogo($logo) {
		$this->logo = $logo;
		return $this;
	}
	public function getStatus() {
		return $this->status;
	}
	public function setStatus($status) {
		$this->status = $status;
		return $this;
	}
	public function getDataCadastro() {
		return $this->data_cadastro;
	}
	public function setDataCadastro($data_cadastro) {
		$this->data_cadastro = $data_cadastro;
		return $this;
	}
	public function getValidade() {
		return $this->validade;
	}
	public function setValidade($validade) {
		$this->validade = $validade;
		return $this;
	}
	public function getNivel() {
		return $this->nivel;
	}
	public function setNivel($nivel) {
		$this->nivel = $nivel;
		return $this;
	}
	public function getSenha() {
		return $this->senha;
	}
	public function setSenha($senha) {
		$this->senha = $senha;
		return $this;
	}
	public function getIdCadastro() {
		return $this->id_cadastro;
	}
	public function setIdCadastro($id_cadastro) {
		$this->id_cadastro = $id_cadastro;
		return $this;
	}
	public function getNome() {
		return $this->nome;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}
	public function getLogin() {
		return $this->login;
	}
	public function setLogin($login) {
		$this->login = $login;
		return $this;
	}

	public function getEmailSefaz() {
		return $this->email_sefaz;
	}
	public function setEmailSefaz($email_sefaz) {
		$this->email_sefaz = $email_sefaz;
		return $this;
	}
	public function getSenhaSefaz() {
		return $this->senha_sefaz;
	}
	public function setSenhaSefaz($senha_sefaz) {
		$this->senha_sefaz = $senha_sefaz;
		return $this;
	}
	public function getServidorEmail() {
		return $this->servidor_email;
	}
	public function setServidorEmail($servidor_email) {
		$this->servidor_email = $servidor_email;
		return $this;
	}
	
	public function cadastrar(){
	
		$dados = array(
	
				'nome' => $this->getNome(),
				'cnpj' => $this->getCnpj(),
				'ie' => $this->getIe(),
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'logradouro' => $this->getLogradouro(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cidade' => $this->getCidade(),
				'uf' => $this->getUf(),
				'cep' => $this->getCep(),
				'telefone' => $this->getTelefone(),
				'celular' => $this->getCelular(),
				'logo' => $this->getLogo(),
				'validade' => $this->getValidade(),
				'email' => $this->getEmail(),
				'login' => $this->getLogin(),
				'senha' => $this->getSenha(),
				'nivel' => $this->getNivel(),
				'status' => $this->getStatus(),
				'id_cadastro' => $this->getIdCadastro(),
				'data_cadastro' => $this->getDataCadastro(),
				'email_sefaz' => $this->getEmailSefaz(),
				'senha_sefaz' => $this->getSenhaSefaz(),
				'servidor_email' => $this->getServidorEmail()

		);
	
		if ($this->db->insert('dtb_usuarios', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function listar() {
		$this->db->select('(SELECT COUNT(e.id) FROM dtb_empresas as e WHERE e.id_contador = u.id) as qtd_empresas, u.id, u.nome, u.email, u.cnpj, u.ie, u.razao_social, u.nome_fantasia, u.logradouro, u.numero, u.complemento, u.bairro, u.cidade, u.uf, u.cep, u.telefone, u.celular, u.validade, u.status, u.nivel, u.validade');
		$this->db->from('dtb_usuarios as u');
		return $this->db->get()->result();
		//return $this->db->get('dtb_usuarios')->result();
	}

	public function listar_usuarios_suporte(){
		$this->db->select('u.id, u.nome, u.email, u.cnpj, u.ie, u.razao_social, u.nome_fantasia, u.logradouro, u.numero, u.complemento, u.bairro, u.cidade, u.uf, u.cep, u.telefone, u.celular, u.validade, u.status, u.nivel, u.validade, u.login');
		$this->db->where('u.nivel !=', 0);		
		$this->db->from('dtb_usuarios as u');
		return $this->db->get()->result();
	}
	
	public function editar(){
	
		$dados = array(

				'cnpj' => $this->getCnpj(),
				'ie' => $this->getIe(),
				'razao_social' => $this->getRazaoSocial(),
				'nome_fantasia' => $this->getNomeFantasia(),
				'logradouro' => $this->getLogradouro(),
				'numero' => $this->getNumero(),
				'complemento' => $this->getComplemento(),
				'bairro' => $this->getBairro(),
				'cidade' => $this->getCidade(),
				'uf' => $this->getUf(),
				'cep' => $this->getCep(),
				'telefone' => $this->getTelefone(),
				'celular' => $this->getCelular(),
				'email' => $this->getEmail(),
				'email_sefaz' => $this->getEmailSefaz(),
				'senha_sefaz' => $this->getSenhaSefaz(),
				'servidor_email' => $this->getServidorEmail(),
		);
	
		if ($this->db->update('dtb_usuarios', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
	public function pesquisar_usuario_id() {
		$this->db->where('id', $this->getId());
		return $this->db->get('dtb_usuarios')->row();
	}
	
	public function alterarsenha(){
	
		$dados = array(
				'senha' => $this->getSenha()
		);
	
		if ($this->db->update('dtb_usuarios', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	
	}
	
	public function pesquisar_logo() {
		$this->db->select('logo');
		$this->db->where('id', $this->getId());
		return $this->db->get('dtb_usuarios')->row();
	}
	
	public function uploadimg(){
	
		$dados = array(
				'logo' => $this->getLogo()
		);
	
		if ($this->db->update('dtb_usuarios', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	
	}
	
	public function findAll() {
		$this->db->select('id, nome');
		$this->db->from('dtb_usuarios');
		$this->db->where('nivel',"2");
		return $this->db->get()->result();
	}

	public function qtd_usuarios(){

		$this->db->select('COUNT(id) AS valor');
		$this->db->where("nivel !=", 0);
		return $this->db->get('dtb_usuarios')->row();
	}

	public function consultar_dados_usuario_master(){
		$this->db->select('nome_fantasia as nome_fantasia_contabilidade');
		$this->db->from('dtb_usuarios');
		$this->db->where('id',"2");
		return $this->db->get()->row();
	}

	public function buscar_colaboradores_chat(){
		$this->db->select('id, nome, razao_social, logo as foto_perfil');
		$this->db->where("id != ".$this->session->userdata['userprimesession']['id']);
		$this->db->where("nivel != 0");
		return $this->db->get('dtb_usuarios')->result();
	} 

	public function pesquisar_foto_chat($id) {
		$this->db->select('nome as user_name,razao_social, logo as foto_perfil, data_acesso, data_logout');
		$this->db->where('id', $id);
		return $this->db->get('dtb_usuarios')->row();
	}

	// FAZER UPLOAD DA FOTO DO PERFIL
	public function upload_img_perfil(){
	
		$dados = array(
				'logo' => $this->getLogo()
		);
	
		if ($this->db->update('dtb_usuarios', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}
	
	}


	public function find_empresas_user(){
		$this->db->select('e.cnpj');
		$this->db->join('dtb_empresas as e', 'd.id_empresa = e.id');
		$this->db->where('d.id_usuario', $this->session->userdata['userprimesession']['id']);
		return $this->db->get('dtb_empresa_usuario d')->result();
	}
}