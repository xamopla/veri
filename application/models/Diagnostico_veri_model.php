<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnostico_veri_model extends CI_Model {

	
	function __construct()
	{
		parent::__construct();
	}

	public function listar_empresas(){
		$this->db->select('cnpj, razao_social'); 

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		return $this->db->get('dtb_empresas')->result();
	}


	public function detalhar_empresas($empresas, $todas_empresas){
		$this->db->select('e.cnpj, e.razao_social, tipo_regime'); 

		if($todas_empresas == false){
			$this->db->where_in("e.cnpj", $empresas);
		}

		$this->db->group_by('e.id');
		return $this->db->get('dtb_empresas e')->result();
	}


	// FUNÇÕES DO RELATÓRIO

	//MENSAGENS DO ECAC ****************************************************
	public function mensagem_ecac($empresa){
		$this->db->select('m.nao_lidas');
		$this->db->where("m.cnpj_data", $empresa->cnpj);
		$this->db->group_by('m.id');
		return $this->db->get('dtb_ecac_caixa_postal m')->row();
	}

	//MENSAGENS DO DEC ****************************************************
	public function mensagem_dec($empresa){
		$this->db->select('m.qtd_nao_lida');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_dec_qtd_mensagem m')->row();
	}

	//DIAGNOSTICO FISCAL ****************************************************
	public function diagnostico_fiscal($empresa){
		$this->db->select('m.possui_pendencia, m.caminho_download');
		$this->db->where("m.cnpj_data", $empresa->cnpj);
		$this->db->group_by('m.cnpj_data');
		return $this->db->get('dtb_situacao_fiscal m')->row();
	}

	//DIVERGENCIA GFIP X GPS ****************************************************
	public function divergencia_gfip_gps($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_pendencia_gfip m')->row();
	}

	//DCTF ****************************************************
	public function dctf($empresa){
		$sql = "SELECT * FROM dtb_ecac_dctf as d WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) >= DATE_FORMAT(STR_TO_DATE(d.periodo_inicial, '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -2 MONTH)) <= DATE_FORMAT(STR_TO_DATE(d.periodo_final, '%d/%m/%Y'), '%Y-%m-%d') AND (d.tipo_status NOT LIKE '%Cancelada%') AND (d.cnpj = ".$empresa->cnpj.") LIMIT 1 ";

		return $this->db->query($sql)->row();
	}


	//PGDAS ****************************************************
	public function pgdas($empresa){
		$sql = "SELECT * FROM dtb_ecac_das as d WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (d.cnpj = ".$empresa->cnpj.") LIMIT 1 ";

		return $this->db->query($sql)->row();
	}


	//DAS NÃO PAGOS ****************************************************
	public function das_nao_pagos($empresa){
		$sql = "SELECT * FROM dtb_ecac_das as d WHERE LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) >= DATE_FORMAT(STR_TO_DATE(CONCAT('01/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) <= DATE_FORMAT(STR_TO_DATE(CONCAT('31/',REPLACE(d.compentencia, 'PA ', '')), '%d/%m/%Y'), '%Y-%m-%d') AND (d.cnpj = ".$empresa->cnpj.") LIMIT 1 ";

		return $this->db->query($sql)->row();
	}


	//PRONAMPE ****************************************************
	public function pronampe($empresa){
		$this->db->select('c.id as caixa_postal_id, cm.assunto, cm.conteudo, cm.id as id_caixa_postal_mensagem, cm.id_mensagem as id_mensagem, cm.lida');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->join('dtb_ecac_caixa_postal c','cm.caixa_postal_id = c.id');
		$this->db->where("cm.assunto like '%Programa Nacional de Apoio%' ");
		$this->db->where("cm.recebida_em >= '2021-01-01'");
		$this->db->where("c.cnpj_data", $empresa->cnpj);
		return $this->db->get()->result();
	}


	//AUSÊNCIA ECF ****************************************************
	public function ausencia_ecf($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_ecf m')->row();
	}


	//AUSÊNCIA GFIP ****************************************************
	public function ausencia_gfip($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_gfip m')->row();
	}

	//AUSÊNCIA DCTF ****************************************************
	public function ausencia_dctf($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_dctf m')->row();
	}

	//AUSÊNCIA PGDAS ****************************************************
	public function ausencia_pgdas($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_pgdas m')->row();
	}

	//CADIN ****************************************************
	public function cadin($empresa){
		$this->db->select('m.possui_pendencia, m.caminho_download');
		$this->db->where("m.cnpj_data", $empresa->cnpj);
		$this->db->group_by('m.cnpj_data');
		return $this->db->get('dtb_situacao_cadin m')->row();
	}


	//MALHA FISCAL
	public function malha_fiscal($empresa) {	
		$this->db->select('m.id, m.id_mensagem, m.cnpj, m.razao_social, m.assunto, m.conteudo, m.lida, m.data, m.data_execucao, m.situacao');
		$this->db->from('dtb_malha_fiscal m');
		$this->db->where("m.cnpj", $empresa->cnpj);
		return $this->db->get()->result();
	}


	//AUSÊNCIA DEFIS ****************************************************
	public function ausencia_defis($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_defis m')->row();
	}

	//AUSÊNCIA EFD ****************************************************
	public function ausencia_efd($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_efd m')->row();
	}

	//AUSÊNCIA DIRF ****************************************************
	public function ausencia_dirf($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_dirf m')->row();
	}

	//AUSÊNCIA DASN ****************************************************
	public function ausencia_dasn($empresa){
		$this->db->select('m.possui_pendencia');
		$this->db->where("m.cnpj", $empresa->cnpj);
		$this->db->group_by('m.cnpj');
		return $this->db->get('dtb_ausencia_dasn m')->row();
	}


	//PROCURAÇÕES ****************************************************
	public function procuracoes($empresa){
		$sql = "SELECT *, DATE_FORMAT(STR_TO_DATE(data_fim, '%d/%m/%Y'), '%Y-%m-%d') as data_or FROM dtb_ecac_procuracao WHERE cnpj_outorgante = '".$empresa->cnpj."' order by DATE_FORMAT(STR_TO_DATE(data_fim, '%d/%m/%Y'), '%Y-%m-%d') desc LIMIT 1";
		return $this->db->query($sql)->row();
	}


	//TERMO DE EXCLUSÃO ****************************************************
	public function termo_exclusao($empresa){
		$this->db->select('c.cnpj_data, c.id as caixa_postal_id, cm.assunto, cm.conteudo, cm.id as id_caixa_postal_mensagem, cm.id_mensagem as id_mensagem, cm.lida, cm.recebida_em as data');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->join('dtb_ecac_caixa_postal c','cm.caixa_postal_id = c.id');

		$this->db->where("cm.assunto like '%TERMO DE EXCLUSÃO DO SIMPLES NACIONAL%' ");
		$this->db->where("cm.recebida_em >= '2021-01-01'");
		$this->db->where("c.cnpj_data", $empresa->cnpj);

		return $this->db->get()->result();

	}


	//SUBLIMITE SIMPLES NACIONAL ****************************************************
	public function sublimite_simples($empresa){
		$this->db->select('db.cnpj, db.valor_atual, db.percentual');
        $this->db->from('dtb_limite_simples db');
        $this->db->where("db.cnpj", $empresa->cnpj);
        return $this->db->get()->row();

	}


	//EXCLUIDAS SIMPLES ****************************************************
	public function exclusao_simples($empresa){
		$this->db->select('*');
        $this->db->from('dtb_empresas_excluidas_simples db');
        $this->db->where("db.cnpj", $empresa->cnpj);
        return $this->db->get()->row();

	}

	//EXCLUIDAS SIMPLES ****************************************************
	public function exclusao_mei($empresa){
		$this->db->select('*');
        $this->db->from('dtb_empresas_excluidas_mei db');
        $this->db->where("db.cnpj", $empresa->cnpj);
        return $this->db->get()->row();

	}


	//TERMO DE INTIMAÇÃO
	public function termo_intimacao($empresa){
		$this->db->select('c.id as caixa_postal_id, cm.assunto, cm.conteudo, cm.id as id_caixa_postal_mensagem, cm.id_mensagem as id_mensagem, cm.lida, cm.recebida_em as data');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->join('dtb_ecac_caixa_postal c','cm.caixa_postal_id = c.id');
		$this->db->where("c.cnpj_data", $empresa->cnpj);
		$this->db->where("cm.assunto like '%Termo de Intimação%' ");
		$this->db->where("cm.recebida_em >= '2021-01-01'");

		return $this->db->get()->result();
	}


	//e-Processos
	public function eprocessos($empresa){
		$this->db->select('*');
		$this->db->from('dtb_ecac_eprocessos_ativos d');
		$this->db->where("d.cnpj", $empresa->cnpj);
		return $this->db->get()->result();
	}

	//PARCELAMENTO DAS ---------------------------------------------------------------------------------------------
	public function parcelamento_das($empresa){

		$this->db->select('db.id,db.cnpj, db.numero, db.data_pedido, db.situacao, db.data_situacao, db.observacao, db.path_recibo_adesao, db.valor_total_consolidado, db.qtd_parcelas, db.primeira_parcela, db.parcela_basica, db.data_consolidacao');
		$this->db->from('dtb_ecac_simplesnacional_pedidos_parcelamentos db');
		$this->db->where("db.cnpj", $empresa->cnpj);
		return $this->db->get()->row();
	}

	public function listar_parcelas($empresa){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_atual = date('m');
	  	$ano_atual = date('Y');

	  	$mes_parcela = $mes_atual."/".$ano_atual;

		$this->db->select('*');
		$this->db->from('dtb_simplesnacional_parcelas_emitidas db');
		$this->db->join('dtb_ecac_simplesnacional_pedidos_parcelamentos e','trim(db.cnpj) = trim(e.cnpj)');
		$this->db->where("e.situacao = 'Em parcelamento'");
		$this->db->where("db.cnpj", $empresa->cnpj);
		$this->db->where("trim(db.data_parcela) != ", $mes_parcela);
		return $this->db->get()->result();
	}

	public function listar_parcelas_pagas($empresa){

		$this->db->select('*');
		$this->db->from('dtb_ecac_simplesnacional_demonstrativo_pagamentos db');
		$this->db->join('dtb_ecac_simplesnacional_pedidos_parcelamentos e','trim(db.cnpj) = trim(e.cnpj)');
		$this->db->where("db.cnpj", $empresa->cnpj);
		$this->db->where("e.situacao = 'Em parcelamento'");
		return $this->db->get()->result();
	}
	

	//PARCELAMENTO PERT SN -------------------------------------------------------------------------------------------
	public function parcelamento_pert($empresa){

		$this->db->select('db.id, db.cnpj, db.numero, db.data_pedido, db.situacao, db.data_situacao, db.valor_total_consolidado_entrada, db.qtd_parcelas, db.parcela_basica, db.valor_total_consolidado_divida, db.data_consolidacao');

		$this->db->from('dtb_ecac_pert_pedidos_parcelamentos db');
		$this->db->where("db.cnpj", $empresa->cnpj);
		return $this->db->get()->row();
	}

	public function listar_parcelas_pert($empresa){
		date_default_timezone_set('America/Sao_Paulo');
		$mes_atual = date('m');
	  	$ano_atual = date('Y');

	  	$mes_parcela = $mes_atual."/".$ano_atual;

		$this->db->select('*');
		$this->db->from('dtb_pert_parcelas_emitidas db');
		$this->db->join('dtb_ecac_pert_pedidos_parcelamentos e','trim(db.cnpj) = trim(e.cnpj)');
		$this->db->where("e.situacao = 'Em parcelamento'");
		$this->db->where("db.cnpj", $empresa->cnpj);
		$this->db->where("trim(db.data_parcela) != ", $mes_parcela);
		return $this->db->get()->result();
	}

	public function listar_parcelas_pagas_pert($empresa){

		$this->db->select('*');
		$this->db->from('dtb_ecac_pert_demonstrativo_pagamentos db');
		$this->db->join('dtb_ecac_pert_pedidos_parcelamentos e','trim(db.cnpj) = trim(e.cnpj)');
		$this->db->where("e.situacao = 'Em parcelamento'");
		$this->db->where("db.cnpj", $empresa->cnpj);
		return $this->db->get()->result();
	}

	//PARCELAMENTO NÃO PREVIDENCIÁRIO -------------------------------------------------------------------------------------------
	public function listar_parcelmaneto_nao_previdenciario($empresa){

		$this->db->select('db.id, db.cnpj, db.processo, db.data_do_deferimento, db.situacao');

		$this->db->from('dtb_parcelamento_nao_previdenciario_processos_negociados db');
		$this->db->where("db.cnpj", $empresa->cnpj);

		return $this->db->get()->result();
	}

	//PARCELAMENTO LEI 12.996
	public function listar_parcelmaneto_lei($empresa){
		$this->db->select('db.*');

		$this->db->from('dtb_parcelamento_lei_12996_divida_consolidada db');
		$this->db->where("db.cnpj", $empresa->cnpj);

		return $this->db->get()->result();
	}

	public function get_parcelas_vencidas($empresa){
		$this->db->select('count(*) as qtd');

		$this->db->from('dtb_parcelamento_lei_12996_demonstrativo_prestacoes db');
		$this->db->where("db.cnpj", $empresa->cnpj);
		$this->db->where("db.indicador_situacao_parcela = 'D'");

		return $this->db->get()->row();
	}

}	