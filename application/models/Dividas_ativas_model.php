<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dividas_ativas_model extends CI_Model {

	// --------------------------- FGTS ----------------------------
	public function qtd_empresas_divida_fgts(){

		$this->db->select('COUNT(distinct(dtb_divida_ativa_fgts_n.id)) AS valor');

		return $this->db->get('dtb_divida_ativa_fgts_n')->row();
	} 

	public function qtd_empresas_divida_previdencia(){

		$this->db->select('COUNT(distinct(dtb_divida_ativa_previdencia_n.id)) AS valor');

		return $this->db->get('dtb_divida_ativa_previdencia_n')->row();
	} 

	public function qtd_empresas_divida_multatrabalhista(){

		$this->db->select('COUNT(distinct(dtb_divida_ativa_multatrabalhista.id)) AS valor');

		return $this->db->get('dtb_divida_ativa_multatrabalhista')->row();
	} 

	public function qtd_empresas_divida_multaeleitoral(){

		$this->db->select('COUNT(distinct(dtb_divida_ativa_multaeleitoral.id)) AS valor');

		return $this->db->get('dtb_divida_ativa_multaeleitoral')->row();
	}

	public function qtd_empresas_divida_multacriminal(){

		$this->db->select('COUNT(distinct(dtb_divida_ativa_multacriminal.id)) AS valor');

		return $this->db->get('dtb_divida_ativa_multacriminal')->row();
	}

	public function listar_empresas_divida_fgts(){

		$this->db->select('da.id, da.id_empresa, da.cpf_cnpj, da.nome_razaosocial, da.nome_fantasia, da.valor_total, da.valor_divida_selecionada, da.data_cadastro, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.id = da.id_empresa', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_divida_ativa_fgts_n as da')->result();
	} 

	public function listar_empresas_divida_previdencia(){

		$this->db->select('da.id, da.id_empresa, da.cpf_cnpj, da.nome_razaosocial, da.nome_fantasia, da.valor_total, da.valor_divida_selecionada, da.data_cadastro, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.id = da.id_empresa', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_divida_ativa_previdencia_n as da')->result();
	} 

	public function listar_empresas_divida_multatrabalhista(){

		$this->db->select('da.id, da.id_empresa, da.cpf_cnpj, da.nome_razaosocial, da.nome_fantasia, da.valor_total, da.valor_divida_selecionada, da.data_cadastro, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.id = da.id_empresa', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_divida_ativa_multatrabalhista as da')->result();
	}

	public function listar_empresas_divida_multaeleitoral(){

		$this->db->select('da.id, da.id_empresa, da.cpf_cnpj, da.nome_razaosocial, da.nome_fantasia, da.valor_total, da.valor_divida_selecionada, da.data_cadastro, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.id = da.id_empresa', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_divida_ativa_multaeleitoral as da')->result();
	}

	public function listar_empresas_divida_multacriminal(){

		$this->db->select('da.id, da.id_empresa, da.cpf_cnpj, da.nome_razaosocial, da.nome_fantasia, da.valor_total, da.valor_divida_selecionada, da.data_cadastro, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.id = da.id_empresa', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_divida_ativa_multacriminal as da')->result();
	}

	// DIVIDA ATIVA ECAC
	public function listar_empresas_divida_fgts_ecac(){

		$this->db->select('da.cnpj, da.numero_inscricao, da.cnpj_devedor_principal, da.devedor_principal, da.situacao as situacao_divida, da.valor_total_debito, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.cnpj = da.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_ecac_divida_ativa_fgts as da')->result();
	}



	public function listar_empresas_divida_previdenciaria_ecac(){

		$this->db->select('da.cnpj, da.numero_inscricao, da.cnpj_devedor_principal, da.devedor_principal, da.fase_atual, da.valor_total_debito, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.cnpj = da.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_ecac_divida_ativa_previdenciaria as da')->result();
	}



	public function listar_empresas_divida_naoprevidenciaria_ecac(){

		$this->db->select('da.cnpj, da.numero_inscricao, da.cnpj_devedor_principal, da.numero_processo, da.situacao as situacao_divida, da.valor_consolidado, da.data_consolidacao, da.extinta, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.cnpj = da.cnpj', 'left');

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_ecac_divida_ativa_nao_previdenciaria as da')->result();
	}

	public function listar_empresas_divida_naoprevidenciaria_ecac_extinta(){

		$this->db->select('da.cnpj, da.numero_inscricao, da.cnpj_devedor_principal, da.numero_processo, da.situacao as situacao_divida, da.valor_consolidado, da.data_consolidacao, da.extinta, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.cnpj = da.cnpj', 'left');
		$this->db->where("da.extinta","SIM");

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_ecac_divida_ativa_nao_previdenciaria as da')->result();
	}

	public function listar_empresas_divida_naoprevidenciaria_ecac_naoextinta(){

		$this->db->select('da.cnpj, da.numero_inscricao, da.cnpj_devedor_principal, da.numero_processo, da.situacao as situacao_divida, da.valor_consolidado, da.data_consolidacao, da.extinta, e.vinculo_contador, e.id as id_tabela_empresa, e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.situacao_cadastral, e.situacao, e.motivo, e.sync, e.id_contador, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas as e', 'e.cnpj = da.cnpj', 'left');
		$this->db->where("da.extinta","NAO");

		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_empresas.id');
		}

		return $this->db->get('dtb_ecac_divida_ativa_nao_previdenciaria as da')->result();
	}

	public function qtd_empresas_divida_fgts_ecac(){

		$this->db->select('COUNT(distinct(dtb_ecac_divida_ativa_fgts.cnpj)) AS valor');

		return $this->db->get('dtb_ecac_divida_ativa_fgts')->row();
	} 

	public function qtd_empresas_divida_previdencia_ecac(){

		$this->db->select('COUNT(distinct(dtb_ecac_divida_ativa_previdenciaria.cnpj)) AS valor');

		return $this->db->get('dtb_ecac_divida_ativa_previdenciaria')->row();
	} 

	public function qtd_empresas_divida_nao_previdencia_ecac(){

		$this->db->select('COUNT(distinct(dtb_ecac_divida_ativa_nao_previdenciaria.cnpj)) AS valor');

		return $this->db->get('dtb_ecac_divida_ativa_nao_previdenciaria')->row();
	} 


}