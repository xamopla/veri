<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Documentos_clientes_model extends CI_Model { 
	
	public function listar_documentos_contador($id_empresa){

		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.cnpj_completo, dtbe.nome_fantasia,
			dtbt.nome, db.dataEmissao, db.dataValidade, db.diasNotificacao, db.numero_documento, db.id_tipoDocumento');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');
		$this->db->join('dtb_clientes_empresas as ce', 'db.id_empresa = ce.id_empresa', 'left');
		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id', 'left');
		$this->db->where('ce.id_empresa', $id_empresa);  
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function listar_documentos_cliente($id_empresa){

		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.cnpj_completo, dtbe.nome_fantasia,
			dtbt.nome, db.dataEmissao, db.dataValidade, db.diasNotificacao, db.numero_documento, db.id_tipoDocumento');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');
		$this->db->join('dtb_clientes_empresas as ce', 'db.id_empresa = ce.id_empresa', 'left');
		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id', 'left');
		$this->db->where('ce.id_empresa', $id_empresa);  
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function find_id($id){

		$this->db->select('id_empresa as id'); 
		$this->db->where('id', $id); 
		return $this->db->get('dtb_clientes_empresas')->row();
	}
}