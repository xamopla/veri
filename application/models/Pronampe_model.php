<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pronampe_model extends CI_Model {
	
	private $filtro;

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

	function __construct()
	{
		parent::__construct();
	}

	public function listar() {	
		$this->db->select('dtbe.cnpj, dtbe.razao_social, c.id as caixa_postal_id, cm.assunto, cm.conteudo, cm.id as id_caixa_postal_mensagem, cm.id_mensagem as id_mensagem, cm.lida');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->join('dtb_ecac_caixa_postal c','cm.caixa_postal_id = c.id');
		$this->db->join('dtb_empresas dtbe','trim(c.cnpj_data) = trim(dtbe.cnpj)');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("cm.assunto like '%Programa Nacional de Apoio%' ");
		$this->db->where("cm.recebida_em >= '2021-01-01'");

		// if($this->getFiltro() != null && $this->getFiltro() != 'TODAS'){
  //           if($this->getFiltro() == "IRREGULAR"){
  //           	$this->db->where("db.possui_pendencia = 1");
  //           }else if($this->getFiltro() == "REGULAR"){
  //           	$this->db->where("db.possui_pendencia = 0");
  //           }
  //       }

		$this->db->group_by('dtbe.cnpj');
		return $this->db->get()->result();
	}


	public function qtd() {	
		$this->db->select('COUNT(distinct(c.cnpj_data)) as qtd');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->join('dtb_ecac_caixa_postal c','cm.caixa_postal_id = c.id');
		$this->db->join('dtb_empresas dtbe','trim(c.cnpj_data) = trim(dtbe.cnpj)');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->where("cm.assunto like '%Programa Nacional de Apoio%' ");
		$this->db->where("cm.recebida_em >= '2021-01-01'");
		// $this->db->group_by('dtbe.cnpj');
		return $this->db->get()->row();
	}

	public function find_info_for_modal($id){
		$this->db->select('cm.conteudo');
		$this->db->from('dtb_ecac_caixa_postal_mensagem cm');
		$this->db->where("cm.id", $id);
		return $this->db->get()->row();
	}

	

}