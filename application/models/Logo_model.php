<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logo_model extends CI_Model{
	
	private $id;
	private $logo;

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getLogo() {
		return $this->logo;
	}
	public function setLogo($logo) {
		$this->logo = $logo;
		return $this;
	}

	public function cadastrar(){
		$dados = array(
				'caminho' => $this->getLogo()
		);
	
		if ($this->db->update('dtb_logo', $dados, "id={$this->getId()}")){
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function buscar_logo(){
		$this->db->select("*");
		$this->db->where("id = 1");
		return $this->db->get("dtb_logo")->row();
	}
	
}