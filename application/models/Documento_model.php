<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Documento_model extends CI_Model {

	private $id;
	private $ativo;
	private $id_empresa;
	private $id_expedidor;
	private $id_tipoDocumento;
	private $numero_documento;
	private $numero_protocolo;
	private $dataEmissao;
	private $dataValidade;
	private $diasNotificacao;
	private $observacoes;
	private $anexo1;
	private $anexo2;
	private $anexo3;
	private $anexo4;
	// private $id_email;
	private $id_contabilidade;

	private $anexoNome;
	private $anexoPath;

	public function __construct(){
		parent::__construct();
	}

	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getAtivo() {
		return $this->ativo;
	}
	public function setAtivo($ativo) {
		$this->ativo = $ativo;
		return $this;
	}

	public function getIdEmpresa() {
		return $this->id_empresa;
	}
	public function setIdEmpresa($id_empresa) {
		$this->id_empresa = $id_empresa;
		return $this;
	}

	public function getIdExpedidor() {
		return $this->id_expedidor;
	}
	public function setIdExpedidor($id_expedidor) {
		$this->id_expedidor = $id_expedidor;
		return $this;
	}

	public function getIdTipoDocumento() {
		return $this->id_tipoDocumento;
	}
	public function setIdTipoDocumento($id_tipoDocumento) {
		$this->id_tipoDocumento = $id_tipoDocumento;
		return $this;
	}

	public function getNumeroDocumento() {
		return $this->numero_documento;
	}
	public function setNumeroDocumento($numero_documento) {
		$this->numero_documento = $numero_documento;
		return $this;
	}

	public function getNumeroProtocolo() {
		return $this->numero_protocolo;
	}
	public function setNumeroProtocolo($numero_protocolo) {
		$this->numero_protocolo = $numero_protocolo;
		return $this;
	}

	public function getDataEmissao() {
		return $this->dataEmissao;
	}
	public function setDataEmissao($dataEmissao) {
		$this->dataEmissao = $dataEmissao;
		return $this;
	}

	public function getDataValidade() {
		return $this->dataValidade;
	}
	public function setDataValidade($dataValidade) {
		$this->dataValidade = $dataValidade;
		return $this;
	}

	public function getDiasNotificacao() {
		return $this->diasNotificacao;
	}
	public function setDiasNotificacao($diasNotificacao) {
		$this->diasNotificacao = $diasNotificacao;
		return $this;
	}

	public function getObservacoes() {
		return $this->observacoes;
	}
	public function setObservacoes($observacoes) {
		$this->observacoes = $observacoes;
		return $this;
	}

	public function getAnexo1() {
		return $this->anexo1;
	}
	public function setAnexo1($anexo1) {
		$this->anexo1 = $anexo1;
		return $this;
	}

	public function getAnexo2() {
		return $this->anexo2;
	}
	public function setAnexo2($anexo2) {
		$this->anexo2 = $anexo2;
		return $this;
	}

	public function getAnexo3() {
		return $this->anexo3;
	}
	public function setAnexo3($anexo3) {
		$this->anexo3 = $anexo3;
		return $this;
	}

	public function getAnexo4() {
		return $this->anexo4;
	}
	public function setAnexo4($anexo4) {
		$this->anexo4 = $anexo4;
		return $this;
	}

	// public function getIdEmail() {
	// 	return $this->id_email;
	// }
	// public function setIdEmail($id_email) {
	// 	$this->id_email = $id_email;
	// 	return $this;
	// }

	public function getIdContabilidade() {
		return $this->id_contabilidade;
	}
	public function setIdContabilidade($id_contabilidade) {
		$this->id_contabilidade = $id_contabilidade;
		return $this;
	}

	public function getAnexoNome() {
		return $this->anexoNome;
	}
	public function setAnexoNome($anexoNome) {
		$this->anexoNome = $anexoNome;
		return $this;
	}


	public function getAnexoPath() {
		return $this->anexoPath;
	}
	public function setAnexoPath($anexoPath) {
		$this->anexoPath = $anexoPath;
		return $this;
	}

	public function cadastrar(){

		$dados = array(

				'ativo' => $this->getAtivo(),
				'id_empresa' => $this->getIdEmpresa(),
				//'id_expedidor' => $this->getIdExpedidor(),
				'id_tipoDocumento' => $this->getIdTipoDocumento(),
				'numero_documento' => $this->getNumeroDocumento(),
				'numero_protocolo' => $this->getNumeroProtocolo(),
				'dataEmissao' => $this->getDataEmissao(),
				'dataValidade' => $this->getDataValidade(),
				'diasNotificacao' => $this->getDiasNotificacao(),
				'observacoes' => $this->getObservacoes(),
				//'anexo1' => $this->getAnexo1(),
				//'anexo2' => $this->getAnexo2(),
				//'anexo3' => $this->getAnexo3(),
				//'anexo4' => $this->getAnexo4(),
				//'id_email' => $this->getIdEmail(),
				'id_contabilidade' => $this->getIdContabilidade()
		);

		if ($this->db->insert('dtb_documentos', $dados)){

			$documento = $this->buscaUltimoDocumento($this->getIdContabilidade());
			$id_documento;
		
			foreach ($documento as $value) {
				$id_documento =  $value->id;
				//$nome = $value->nome;
			}
			$nome;
			$empresa;
			$nomeColaborador;

			$tipodocumento = $this->buscar_detalhes_documento($id_documento);

			foreach ($tipodocumento as $value) {
				$nome = $value->nome;
				$empresa = $value->razao_social;
				$nomeColaborador = $value->nome_colaborador;
			}

			if($nomeColaborador != null && $nomeColaborador != ""){
				$empresa = $empresa.";".$nomeColaborador;
			}else{
				$empresa = $empresa.";Administrador";
			}

			$this->insereNovoEvento($nome, $id_documento, $empresa);

			if($this->getAnexoNome() != ""){
				$this->salvaDocumento($this->getAnexo1(), $this->getIdContabilidade());

				$anexo = $this->buscaUltimoAnexo($this->getIdContabilidade());
				$id_anexo;
				foreach ($anexo as $value) {
					$id_anexo =  $value->id;
				}

				$dataEmissaoDoc = $this->getDataEmissao();
				$dataValidadeDoc = $this->getDataValidade();

				$this->salvaDocumentoAnexo($id_documento,$id_anexo,$dataEmissaoDoc,$dataValidadeDoc);

			}

			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function insereNovoEvento($nome, $id_documento, $razao_social){
		$url = base_url()."calendario/renovardocumento/".$id_documento."/".$this->getIdContabilidade();

		$dados = array(
				'nome' => $nome,
				'datainicio' => $this->getDataValidade(),
				'datafim' => $this->getDataValidade(),
				'id_usuario' => $this->getIdContabilidade(),
				'cor' => "rgb(153,153,153)",
				'id_documento' => $id_documento,
				"url" => $url,
				"description" => $razao_social
		);

		$this->db->insert('eventos_criados', $dados);
	}

	public function salvaDocumento($anexo, $id_contabilidade){

		$filename = $this->getAnexoNome();//$_FILES['anexo']['name'];
	    $path = $this->getAnexoPath();//$_FILES['file']['tmp_name'];
		
		$dados = array(
				'nome' => $filename,
				'path' => $path,
				'id_contabilidade' => $id_contabilidade
		);
		$this->db->insert('dtb_anexo', $dados);
	}

	public function salvaDocumentoAnexo($id_documento,$id_anexo,$data_emissao,$data_vencimento){
		$dados = array(
				'data_emissao' => $data_emissao,
				'data_vencimento' => $data_vencimento,
				'id_documento' => $id_documento,
				'id_anexo' => $id_anexo
		);
		$this->db->insert('dtb_documento_anexo', $dados);
	}

	public function buscar_detalhes_documento($id){
		$this->db->select('db.id, dtbt.nome, dtbe.razao_social, u.nome as nome_colaborador');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id');
		$this->db->join('dtb_usuarios u','db.id_contabilidade = u.id');
		$this->db->where("db.id", $id);
		return $this->db->get()->result();
	}

	public function buscaUltimoDocumento($id_contabilidade){
		$this->db->select('MAX(db.id) as id');
		$this->db->from('dtb_documentos db');
		$this->db->where("db.id_contabilidade", $id_contabilidade);
		return $this->db->get()->result();
	}

	public function buscaUltimoAnexo($id_contabilidade){
		$this->db->select('MAX(anexo.id) as id');
		$this->db->from('dtb_anexo anexo');
		$this->db->where("anexo.id_contabilidade", $id_contabilidade);
		return $this->db->get()->result();
	}

	public function listar() {	
		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.cnpj_completo, dtbe.nome_fantasia,
			dtbt.nome, db.dataEmissao, db.dataValidade, db.diasNotificacao, db.numero_documento, db.id_tipoDocumento');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id', 'left');
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function listaranexos(){
		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbt.nome, db.dataEmissao, db.dataValidade, db.numero_documento, anexo.nome as nomedocumento, anexo.id as id_anexo, anexo.path as path, dtba.data_emissao as data_va, dtba.data_vencimento as data_emi');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id');
		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id');
		$this->db->join('dtb_documento_anexo dtba','db.id = dtba.id_documento');
		$this->db->join('dtb_anexo anexo','anexo.id = dtba.id_anexo');

		$this->db->where("db.id", $this->getId());
		// $this->db->where("anexo.id_contabilidade", $this->getIdContabilidade());
		
		return $this->db->get()->result();

	}
	
	public function buscar_documentos_byEmpresa(){
		$this->db->select('id');
		$this->db->where("id_empresa", $this->getIdEmpresa());
		return $this->db->get('dtb_documentos')->result();
	}

	public function excluirDocumentoByEmpresa(){
		$documentos = $this->buscar_documentos_byEmpresa($this->getIdEmpresa());
		foreach ($documentos as $value) {
			$this->setId($value->id);
			$this->excluir();
		}
	}

	public function excluir(){
		$this->excluirAnexo($this->getId());
		$this->excluirDocumentoAnexo($this->getId());

		return $this->db->delete('dtb_documentos', "id = {$this->getId()}");
	}

	public function excluirAnexo($id_documento){
		$sql = "DELETE db FROM dtb_anexo db
	  		JOIN dtb_documento_anexo t2 ON db.id = t2.id_anexo
	  		WHERE t2.id_documento = ?";
		$this->db->query($sql, array($id_documento));
	}

	public function excluirDocumentoAnexo($id_documento){
		$sql = "DELETE db FROM dtb_documento_anexo db
	  		WHERE db.id_documento = ?";
		$this->db->query($sql, array($id_documento));
	}

	public function pesquisar_documento_id() {
		$this->db->select('id, ativo, id_empresa, id_tipoDocumento, numero_documento, numero_protocolo, dataEmissao, dataValidade, observacoes, diasNotificacao, id_contabilidade');
		$this->db->where("id", $this->getId());
		return $this->db->get('dtb_documentos')->row();
	}

	public function pesquisar_documento_id_renovar() {
		$this->db->select('dtb_documentos.id as id, dtb_documentos.ativo as ativo, dtb_documentos.diasNotificacao as dias_notificacao, dtb_documentos.id_empresa as id_empresa, dtb_documentos.id_tipoDocumento as id_tipoDocumento, dtbt.url_consulta as url_consulta, dtbe.cnpj as cnpj, dtb_documentos.numero_documento as numero_documento, dtb_documentos.numero_protocolo as numero_protocolo, dtb_documentos.dataEmissao as dataEmissao, dtb_documentos.dataValidade as dataValidade, dtb_documentos.observacoes as observacoes, dtb_documentos.id_contabilidade as id_contabilidade, dtbe.razao_social as razao_social, dtbt.nome as nometipodocumento');
		$this->db->join('dtb_empresas dtbe','dtb_documentos.id_empresa = dtbe.id');
		$this->db->join('dtb_tipodedocumento dtbt','dtb_documentos.id_tipoDocumento = dtbt.id');

		$this->db->where("dtb_documentos.id", $this->getId());
		return $this->db->get('dtb_documentos')->row();
	}

	public function updateNovoEvento($nome, $id_documento, $razao_social){
		$dados = array(
				'nome' => $nome,
				'datainicio' => $this->getDataValidade(),
				'datafim' => $this->getDataValidade(),
				'id_usuario' => $this->getIdContabilidade(),
				'cor' => "rgb(153,153,153)",
				'id_documento' => $id_documento,
				'description' => $razao_social
		);

		$this->db->update('eventos_criados', $dados, "id_documento={$this->getId()}");
	}

	public function updateNovoEventoRenovar($id_documento){
		$dados = array(
				
				'datainicio' => $this->getDataValidade(),
				'datafim' => $this->getDataValidade()
		);

		$this->db->update('eventos_criados', $dados, "id_documento={$this->getId()}");
	}

	public function editar(){
	
		$dados = array(
				'ativo' => $this->getAtivo(),				
				'id_empresa' => $this->getIdEmpresa(),
				//'id_expedidor' => $this->getIdExpedidor(),
				'id_tipoDocumento' => $this->getIdTipoDocumento(),
				'numero_documento' => $this->getNumeroDocumento(),
				'numero_protocolo' => $this->getNumeroProtocolo(),
				'dataEmissao' => $this->getDataEmissao(),
				'dataValidade' => $this->getDataValidade(),
				'diasNotificacao' => $this->getDiasNotificacao(),
				'observacoes' => $this->getObservacoes(),
				//'anexo1' => $this->getAnexo1(),
				//'anexo2' => $this->getAnexo2(),
				//'anexo3' => $this->getAnexo3(),
				//'anexo4' => $this->getAnexo4(),
				//'id_email' => $this->getIdEmail()
		);
	
		if ($this->db->update('dtb_documentos', $dados, "id={$this->getId()}")){

			$id_documento = $this->getId();

			$nome;
			$razao_social;
			$tipodocumento = $this->buscar_detalhes_documento($id_documento);
			$nomeColaborador;

			foreach ($tipodocumento as $value) {
				$nome = $value->nome;
				$razao_social = $value->razao_social;
				$nomeColaborador = $value->nome_colaborador;
			}

			if($nomeColaborador != null && $nomeColaborador != ""){
				$razao_social = $razao_social.";".$nomeColaborador;
			}else{
				$razao_social = $razao_social.";Administrador";
			}

			$this->updateNovoEvento($nome, $id_documento, $razao_social);

			if($this->getAnexoNome() != ""){
				$this->salvaDocumento($this->getAnexo1(), $this->getIdContabilidade());

				$anexo = $this->buscaUltimoAnexo($this->getIdContabilidade());
				$id_anexo;
				foreach ($anexo as $value) {
					$id_anexo =  $value->id;
				}

				$dataEmissaoDoc = $this->getDataEmissao();
				$dataValidadeDoc = $this->getDataValidade();

				$this->salvaDocumentoAnexo($id_documento,$id_anexo,$dataEmissaoDoc,$dataValidadeDoc);
			}
			

			return TRUE;
		} else {
			return FALSE;
		}
	
	}

	public function renovardocumento() {

		$dados = array(				
				'dataEmissao' => $this->getDataEmissao(),
				'dataValidade' => $this->getDataValidade()				
		);

		if ($this->db->update('dtb_documentos', $dados, "id={$this->getId()}")){

			$id_documento = $this->getId();

			$this->updateNovoEventoRenovar($id_documento);

			// if($this->getAnexoNome() != ""){
			// 	$this->salvaDocumento($this->getAnexo1(), $this->getIdContabilidade());

			// 	$documento = $this->buscaUltimoDocumento($this->getIdContabilidade());
			// 	$id_documento;
			// 	foreach ($documento as $value) {
			// 		$id_documento =  $value->id;
			// 	}

			// 	$anexo = $this->buscaUltimoAnexo($this->getIdContabilidade());
			// 	$id_anexo;
			// 	foreach ($anexo as $value) {
			// 		$id_anexo =  $value->id;
			// 	}

			// 	$dataEmissaoDoc = $this->getDataEmissao();
			// 	$dataValidadeDoc = $this->getDataValidade();

			// 	$this->salvaDocumentoAnexo($id_documento,$id_anexo,$dataEmissaoDoc,$dataValidadeDoc);
			// }

			if($this->getAnexoNome() != ""){
				$this->salvaDocumento($this->getAnexo1(), $this->getIdContabilidade());

				$anexo = $this->buscaUltimoAnexo($this->getIdContabilidade());
				$id_anexo;
				foreach ($anexo as $value) {
					$id_anexo =  $value->id;
				}

				$dataEmissaoDoc = $this->getDataEmissao();
				$dataValidadeDoc = $this->getDataValidade();

				$this->salvaDocumentoAnexo($id_documento,$id_anexo,$dataEmissaoDoc,$dataValidadeDoc);
			}
			

			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function buscar_empresas(){
		$this->db->select('dtb_empresas.id, dtb_empresas.razao_social, dtb_empresas.cnpj_completo');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtb_empresas.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->group_by('dtb_empresas.id');
		return $this->db->get('dtb_empresas')->result();

	}

	public function buscar_tipodocumento(){
		$this->db->select('id, nome');
		//$this->db->where("id_contabilidade", $this->getIdContabilidade());
		return $this->db->get('dtb_tipodedocumento')->result();
	}

	public function buscar_expedidor(){
		$this->db->select('id, razao_social, cnpj');
		$this->db->where("id_contabilidade", $this->getIdContabilidade());
		return $this->db->get('dtb_expedidor')->result();
	}

	// public function buscar_email(){
	// 	$this->db->select('id, email');
	// 	$this->db->where("id_contabilidade", $this->getIdContabilidade());
	// 	$this->db->where("ativo = 'Sim'");
	// 	return $this->db->get('dtb_email')->result();
	// }


	//Consulta Notificações dos Documentos
	public function notificacoes_documentos() {
		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.nome_fantasia,
			dtbt.nome, db.dataEmissao, db.dataValidade, db.diasNotificacao, db.numero_documento');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id', 'left');		
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) <= db.diasNotificacao');
		// $this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) >= 0');
		//$this->db->where('dataValidade + 15 = CURDATE()');
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}
	
	public function qtd_notificacoes_documentos() {
		$this->db->select('COUNT(distinct(dtb_documentos.id)) AS valor');

		$this->db->join('dtb_empresas dtbe','dtb_documentos.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) <= diasNotificacao');
		// $this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) >= 0');
		return $this->db->get('dtb_documentos')->row();
	}

	//DELETAR AS NOTIFICAÇÕES APÓS SER LIDA
	public function limparnotificacoesdocumentos(){		
		$this->db->set('status', 0);
		if ($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->where('id_usuario', $this->getIdContabilidade());
		}
		$this->db->where('status', 1);
		if ($this->db->update('notificacoesdocumento')){
			return true;
		} else {
			return false;
		}
		
	}

	//LISTAR DOCUMENTOS PROXIMOS DO VENCIMENTO (DASHBOARD)
	public function listardocumentoproxvenci() {
		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.nome_fantasia,
			dtbt.nome, dtbe.cnpj_completo, db.dataEmissao, db.dataValidade, db.diasNotificacao, db.numero_documento');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id', 'left');		
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) <= db.diasNotificacao');
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) >= 0');
		//$this->db->where('dataValidade + 15 = CURDATE()');
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}	

	public function qtd_documentos_proximosvencimento() {
		$this->db->select('COUNT(distinct(dtb_documentos.id)) AS valor');

		$this->db->join('dtb_empresas dtbe','dtb_documentos.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) <= diasNotificacao');
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) >= 0');
		return $this->db->get('dtb_documentos')->row();
	}

	//LISTAR DOCUMENTOS VENCIDOS
	public function listardocumentosvencidos() {
		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.nome_fantasia,
			dtbt.nome, dtbe.cnpj_completo, db.dataEmissao, db.dataValidade, db.diasNotificacao, db.numero_documento');
		$this->db->from('dtb_documentos db');
		
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id', 'left');
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) < 0');
		$this->db->group_by('db.id');
		return $this->db->get()->result();
	}

	public function qtd_documentos_vencidos() {
		$this->db->select('COUNT(distinct(dtb_documentos.id)) AS valor');

		$this->db->join('dtb_empresas dtbe','dtb_documentos.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->where('dataValidade < CURDATE()');
		return $this->db->get('dtb_documentos')->row();
	}

	//LISTAR DOCUMENTOS REGULARES()
	public function listardocumentosregulares() {
		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.nome_fantasia,
			dtbt.nome, dtbe.cnpj_completo, db.dataEmissao, db.dataValidade, db.numero_documento');
		$this->db->from('dtb_documentos db');
		
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}

		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id', 'left');

		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) > db.diasNotificacao');
		$this->db->group_by('db.id');		
		return $this->db->get()->result();
	}

	public function qtd_documentos_regulares() {
		
		$this->db->select('COUNT(distinct(dtb_documentos.id)) AS valor');

		$this->db->join('dtb_empresas dtbe','dtb_documentos.id_empresa = dtbe.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
		}
		
		$this->db->where('DATEDIFF(dataValidade, CURRENT_DATE) > diasNotificacao');
		return $this->db->get('dtb_documentos')->row();
	}

	public function listar_documentos_por_empresa() {
		$this->db->select('db.id, db.id_empresa, db.id_contabilidade, dtbe.razao_social, dtbe.nome_fantasia,
			dtbt.nome, dtbe.cnpj_completo, db.dataEmissao, db.dataValidade, db.diasNotificacao, db.numero_documento');
		$this->db->from('dtb_documentos db');
		$this->db->join('dtb_empresas dtbe','db.id_empresa = dtbe.id');
		$this->db->join('dtb_tipodedocumento dtbt','db.id_tipoDocumento = dtbt.id');
		$this->db->where("db.id_empresa", $this->getIdEmpresa());
		
		return $this->db->get()->result();
	}

	public function pesquisar_documento_processamento_alvara($idempresa, $idTipoDocumento) {
		$this->db->select('d.id, d.ativo, d.id_empresa, d.id_tipoDocumento, d.numero_documento, d.numero_protocolo, d.dataEmissao, d.dataValidade, d.observacoes, d.diasNotificacao, d.id_contabilidade');
		//$this->db->join('dtb_empresas dtbe','d.id_empresa = dtbe.id');
		$this->db->where("d.id_empresa", $idempresa);
		$this->db->where("d.id_tipoDocumento", $idTipoDocumento);
		return $this->db->get('dtb_documentos d')->row();
	}

	public function excluir_anexos_documentos(){
		return $this->db->delete('dtb_documento_anexo', "id = {$this->getId()}");
	}

	public function update_tipo_documento($id, $tipo_documento){
		$this->db->set('id_tipoDocumento', $tipo_documento);
		$this->db->where('id', $id);
		$this->db->update('dtb_documentos');
	}
}