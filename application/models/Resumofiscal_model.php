<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resumofiscal_model extends CI_Model {

	private $unidadeDeAtendimento;
	private $unidadeDeFiscalizacao;
	private $inscricaoEstadual;	
	private $cnpjcpf;
	private $simplesNacional;	
	private $razaoSocial;
	private $situacao;
	private $motivo;
	private $condicao;	
	private $cnaeFiscal;
	private $telefone;	
	private $contador;	
	private $crc;				
	private $telefone2; 				
	private $porte;				
	private $dataInclusao;		
	private $opcaoSimplesNacional;
	private $situacaoProjetada;	
	private $condicaoProjetada;
	private $MEI;

	private $pergunta1;
	private $pergunta2;
	private $pergunta3;
	private $pergunta4;
	private $pergunta5;
	private $pergunta6;
	private $pergunta7;
	private $pergunta8;
	private $pergunta9;
	private $pergunta10;
	private $pergunta11;
	private $pergunta12;
	private $pergunta13;
	private $pergunta14;
	private $pergunta15;
	private $pergunta16;
	private $pergunta17;
	private $pergunta18;
	private $pergunta19;
	private $pergunta20;
	private $pergunta21;
	private $pergunta22;
	private $pergunta23;
	private $pergunta24;
	private $pergunta25;
	private $pergunta26;
	
	function __construct()
	{
		parent::__construct();
	}

	public function getUnidadeDeAtendimento() {
		return $this->unidadeDeAtendimento;
	}
	public function setUnidadeDeAtendimento($unidadeDeAtendimento) {
		$this->unidadeDeAtendimento = $unidadeDeAtendimento;
		return $this;
	}

	public function getUnidadeDeFiscalizacao() {
		return $this->unidadeDeFiscalizacao;
	}

	public function setUnidadeDeFiscalizacao($unidadeDeFiscalizacao) {
		$this->unidadeDeFiscalizacao = $unidadeDeFiscalizacao;
		return $this;
	}

	public function getInscricaoEstadual() {
		return $this->inscricaoEstadual;
	}

	public function setInscricaoEstadual($inscricaoEstadual) {
		$this->inscricaoEstadual = $inscricaoEstadual;
		return $this;
	}

	public function getCnpjCpf() {
		return $this->cnpjcpf;
	}

	public function setCnpjCpf($cnpjcpf) {
		$this->cnpjcpf = $cnpjcpf;
		return $this;
	}

	public function getSimplesNacional() {
		return $this->simplesNacional;
	}

	public function setSimplesNacional($simplesNacional) {
		$this->simplesNacional = $simplesNacional;
		return $this;
	}

	public function getRazaoSocial() {
		return $this->razaoSocial;
	}

	public function setRazaoSocial($razaoSocial) {
		$this->razaoSocial = $razaoSocial;
		return $this;
	}

	public function getSituacao() {
		return $this->situacao;
	}

	public function setSituacao($situacao) {
		$this->situacao = $situacao;
		return $this;
	}

	public function getMotivacao() {
		return $this->motivacao;
	}

	public function setMotivacao($motivacao) {
		$this->motivacao = $motivacao;
		return $this;
	}

	public function getCondicao() {
		return $this->condicao;
	}

	public function setCondicao($condicao) {
		$this->condicao = $condicao;
		return $this;
	}

	public function getCnaeFiscal() {
		return $this->cnaeFiscal;
	}

	public function setCnaeFiscal($cnaeFiscal) {
		$this->cnaeFiscal = $cnaeFiscal;
		return $this;
	}

	public function getTelefone() {
		return $this->telefone;
	}

	public function setTelefone($telefone) {
		$this->telefone = $telefone;
		return $this;
	}

	public function getContador() {
		return $this->contador;
	}

	public function setContador($contador) {
		$this->contador = $contador;
		return $this;
	}

	public function getCrc() {
		return $this->crc;
	}

	public function setCrc($crc) {
		$this->crc = $crc;
		return $this;
	}

	public function getTelefone2() {
		return $this->telefone2;
	}

	public function setTelefone2($telefone2) {
		$this->telefone2 = $telefone2;
		return $this;
	}

	public function getPorte() {
		return $this->porte;
	}

	public function setPorte($porte) {
		$this->porte = $porte;
		return $this;
	}

	public function getDataInclusao() {
		return $this->dataInclusao;
	}

	public function setDataInclusao($dataInclusao) {
		$this->dataInclusao = $dataInclusao;
		return $this;
	}

	public function getOpcaoSimplesNacional() {
		return $this->opcaoSimplesNacional;
	}

	public function setOpcaoSimplesNacional($opcaoSimplesNacional) {
		$this->opcaoSimplesNacional = $opcaoSimplesNacional;
		return $this;
	}

	public function getSituacaoProjetada() {
		return $this->situacaoProjetada;
	}

	public function setSituacaoProjetada($situacaoProjetada) {
		$this->situacaoProjetada = $situacaoProjetada;
		return $this;
	}

	public function getCondicaoProjetada() {
		return $this->condicaoProjetada;
	}

	public function setCondicaoProjetada($condicaoProjetada) {
		$this->condicaoProjetada = $condicaoProjetada;
		return $this;
	}

	public function getMEI() {
		return $this->MEI;
	}

	public function setMEI($MEI) {
		$this->MEI = $MEI;
		return $this;
	}

	public function getPergunta1() {
		return $this->pergunta1;
	}

	public function setPergunta1($pergunta1) {
		$this->pergunta1 = $pergunta1;
		return $this;
	}

	public function getPergunta2() {
		return $this->pergunta2;
	}

	public function setPergunta2($pergunta2) {
		$this->pergunta2 = $pergunta2;
		return $this;
	}

	public function getPergunta3() {
		return $this->pergunta3;
	}

	public function setPergunta3($pergunta3) {
		$this->pergunta3 = $pergunta3;
		return $this;
	}

	public function getPergunta4() {
		return $this->pergunta4;
	}

	public function setPergunta4($pergunta4) {
		$this->pergunta4 = $pergunta4;
		return $this;
	}

	public function getPergunta5() {
		return $this->pergunta5;
	}

	public function setPergunta5($pergunta5) {
		$this->pergunta5 = $pergunta5;
		return $this;
	}

	public function getPergunta6() {
		return $this->pergunta1;
	}

	public function setPergunta6($pergunta6) {
		$this->pergunta6 = $pergunta6;
		return $this;
	}

	public function getPergunta7() {
		return $this->pergunta7;
	}

	public function setPergunta7($pergunta7) {
		$this->pergunta7 = $pergunta7;
		return $this;
	}

	public function getPergunta8() {
		return $this->pergunta8;
	}

	public function setPergunta8($pergunta8) {
		$this->pergunta8 = $pergunta8;
		return $this;
	}

	public function getPergunta9() {
		return $this->pergunta9;
	}

	public function setPergunta10($pergunta10) {
		$this->pergunta10 = $pergunta10;
		return $this;
	}

	public function getPergunta11() {
		return $this->pergunta11;
	}

	public function setPergunta11($pergunta11) {
		$this->pergunta11 = $pergunta11;
		return $this;
	}

	public function getPergunta12() {
		return $this->pergunta12;
	}

	public function setPergunta12($pergunta12) {
		$this->pergunta12 = $pergunta12;
		return $this;
	}

	public function getPergunta13() {
		return $this->pergunta13;
	}

	public function setPergunta13($pergunta13) {
		$this->pergunta13 = $pergunta13;
		return $this;
	}
	
	public function getPergunta14() {
		return $this->pergunta14;
	}

	public function setPergunta14($pergunta14) {
		$this->pergunta14 = $pergunta14;
		return $this;
	}

	public function getPergunta15() {
		return $this->pergunta15;
	}

	public function setPergunta15($pergunta15) {
		$this->pergunta15 = $pergunta15;
		return $this;
	}

	public function getPergunta16() {
		return $this->pergunta16;
	}

	public function setPergunta16($pergunta16) {
		$this->pergunta16 = $pergunta16;
		return $this;
	}

	public function getPergunta17() {
		return $this->pergunta17;
	}

	public function setPergunta17($pergunta17) {
		$this->pergunta17 = $pergunta17;
		return $this;
	}

	public function getPergunta18() {
		return $this->pergunta18;
	}

	public function setPergunta18($pergunta18) {
		$this->pergunta18 = $pergunta18;
		return $this;
	}

	public function getPergunta19() {
		return $this->pergunta19;
	}

	public function setPergunta19($pergunta19) {
		$this->pergunta19 = $pergunta19;
		return $this;
	}

	public function getPergunta20() {
		return $this->pergunta20;
	}


	public function setPergunta20($pergunta20) {
		$this->pergunta20 = $pergunta20;
		return $this;
	}

	public function getPergunta21() {
		return $this->pergunta21;
	}

	public function setPergunta21($pergunta21) {
		$this->pergunta21 = $pergunta21;
		return $this;
	}

	public function getPergunta22() {
		return $this->pergunta22;
	}

	public function setPergunta22($pergunta22) {
		$this->pergunta22 = $pergunta22;
		return $this;
	}

	public function getPergunta23() {
		return $this->pergunta23;
	}

	public function setPergunta23($pergunta23) {
		$this->pergunta23 = $pergunta23;
		return $this;
	}

	public function getPergunta24() {
		return $this->pergunta24;
	}

	public function setPergunta24($pergunta24) {
		$this->pergunta24 = $pergunta24;
		return $this;
	}

	public function getPergunta25() {
		return $this->pergunta25;
	}

	public function setPergunta25($pergunta25) {
		$this->pergunta25 = $pergunta25;
		return $this;
	}

	public function getPergunta26() {
		return $this->pergunta26;
	}

	public function setPergunta26($pergunta26) {
		$this->pergunta26 = $pergunta26;
		return $this;
	}

	public function listar() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8, pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if($this->session->userdata['userprimesession']['nivel'] == 2){
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');

			$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
		}

		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function socioirregular() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta1) != ", "Não");
		$this->db->where("trim(pergunta1) != ", "");

		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function omissodma() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta2) != ", "Não");
		$this->db->where("trim(pergunta2) != ", "");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function omissoefd() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta3) != ", "Não");
		$this->db->where("trim(pergunta3) != ", "");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function parcelamentoatraso() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta4) != ", "Não");
		$this->db->where("trim(pergunta4) != ", "");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function divergencia1() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta7) != ", "Não");
		$this->db->where("trim(pergunta7) != ", "");
		$this->db->where("trim(pergunta7) != ", "|||");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function divergencia2() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta8) != ", "Não");
		$this->db->where("trim(pergunta8) != ", "");
		$this->db->where("trim(pergunta8) != ", "|||");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function divergencia3() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta9) != ", "Não");
		$this->db->where("trim(pergunta9) != ", "");
		$this->db->where("trim(pergunta9) != ", "|||");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function os1() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta18) != ", "Não");
		$this->db->where("trim(pergunta18) != ", "");		
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function os2() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta19) != ", "Não");
		$this->db->where("trim(pergunta19) != ", "");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function pafativo() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta6) != ", "Não");
		$this->db->where("trim(pergunta6) != ", "");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function grf_beneficiario_decreto(){
		$this->db->select('pergunta25, e.atividade_principal');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		//$this->db->group_by("pergunta25");
		$this->db->group_by("e.id");
		return $this->db->get('dtb_resumofiscal')->result();	
	}

	public function consultaBeneficiarias(){
		$this->db->select('pergunta25, razaoSocial, inscricaoEstadual, cnpjcpf, e.atividade_principal as cnaeFiscal');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		//$this->db->group_by("pergunta25");
		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();	
	}

	//Consulta Notificações do Resumo Fiscal
	public function notificacoes_resumofiscal() {
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(notificacoesresumofiscal.cnpjstring) = trim(e.cnpj_completo)','left');
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		$this->db->where("status", 1);
		$this->db->order_by("notificacoesresumofiscal.id", "desc");
		$this->db->group_by("notificacoesresumofiscal.id");
		return $this->db->get('notificacoesresumofiscal')->result();
	}
	
	public function qtd_notificacoes_resumofiscal() {
		$this->db->select('COUNT(distinct(notificacoesresumofiscal.id)) AS valor');
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(notificacoesresumofiscal.cnpjstring) = trim(e.cnpj_completo)','left');
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		$this->db->where("status", 1);
		return $this->db->get('notificacoesresumofiscal')->row();
	}

	//DELETAR AS NOTIFICAÇÕES DO RESUMO FISCAL APÓS SER LIDA
	public function limparnotificacoesresumofiscal(){		
		$this->db->set('status', 0);
		$this->db->where('status', 1);

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "UPDATE notificacoesresumofiscal LEFT JOIN dtb_empresas e ON trim(notificacoesresumofiscal.cnpjstring) = trim(e.cnpj_completo) LEFT JOIN dtb_empresa_usuario eu ON e.id = eu.id_empresa set notificacoesresumofiscal.status = 0 where notificacoesresumofiscal.status = 1 and (e.id_funcionario = ".$this->session->userdata['userprimesession']['id']." or eu.id_usuario = ".$this->session->userdata['userprimesession']['id'].")";

			if ($this->db->query($sql)){
				return true;
			} else {
				return false;
			}
		}else{
			if ($this->db->update('notificacoesresumofiscal')){
				return true;
			} else {
				return false;
			}
		}
		
	}

	public function lerMensagem($id){
		$this->db->set('status', 0);
		$this->db->where('id', $id);
		$this->db->update('notificacoesresumofiscal');
	}
	//FIM da Consulta Notificações do Resumo Fiscal

	public function dashboard1($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta1) != ", "Não");
		$this->db->where("trim(pergunta1) != ", "");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard2($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta2) != ", "Não");
		$this->db->where("trim(pergunta2) != ", "");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard3($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta3) != ", "Não");
		$this->db->where("trim(pergunta3) != ", "");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard4($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta4) != ", "Não");
		$this->db->where("trim(pergunta4) != ", "");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard5($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta7) != ", "Não");
		$this->db->where("trim(pergunta7) != ", "");
		$this->db->where("trim(pergunta7) != ", "|||");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}
	public function dashboard6($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta8) != ", "Não");
		$this->db->where("trim(pergunta8) != ", "");
		$this->db->where("trim(pergunta8) != ", "|||");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}
	public function dashboard7($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta9) != ", "Não");
		$this->db->where("trim(pergunta9) != ", "");
		$this->db->where("trim(pergunta9) != ", "|||");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard8($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("trim(pergunta18) != ", "Não");
		$this->db->where("trim(pergunta18) != ", "");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard9($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		
		$this->db->where("trim(pergunta19) != ", "Não");
		$this->db->where("trim(pergunta19) != ", "");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard10($param) {
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		
		$this->db->where("trim(pergunta6) != ", "Não");
		$this->db->where("trim(pergunta6) != ", "");
		$this->db->where("trim(cnpjcpf) != ", "");
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function dashboard11(){
		$this->db->select('dtb_resumofiscal.msg_nao_lidas as qtd');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');	
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		
		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Todas Lidas%' ");
		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Não há mensagem%' ");
		$this->db->where("trim(msg_nao_lidas) != ", "");
		$this->db->where("msg_nao_lidas is not null");
		$this->db->where("trim(cnpjcpf) != ", "");
		$this->db->group_by("trim(cnpjcpf)");
		return $this->db->get('dtb_resumofiscal')->result();
		
	}

	public function excluirResumoByCnpj(){
		return $this->db->delete('dtb_resumofiscal', "trim(cnpjcpf) = '{$this->getCnpjCpf()}'");
	}

	public function findPergunta($inscricaoEstadual, $pergunta){
		if($pergunta == 7){
			$this->db->select('pergunta7 as pergunta');
		}elseif ($pergunta == 8) {
			$this->db->select('pergunta8 as pergunta');
		}else{
			$this->db->select('pergunta9 as pergunta');
		}

		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function findPaf($inscricaoEstadual){
		$this->db->select('pergunta6 as pergunta');
		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function findSocioIrregular($inscricaoEstadual){
		$this->db->select('pergunta1 as pergunta');
		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function findEFD($inscricaoEstadual){
		$this->db->select('pergunta3 as pergunta');
		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function findDMA($inscricaoEstadual){
		$this->db->select('pergunta2 as pergunta');
		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function findOsMonitoramento($inscricaoEstadual){
		$this->db->select('pergunta18 as pergunta');
		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function findOsAuditoria($inscricaoEstadual){
		$this->db->select('pergunta19 as pergunta');
		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function findParcelamentoEmAtraso($inscricaoEstadual){
		$this->db->select('pergunta4 as pergunta');
		$this->db->where("trim(inscricaoEstadual) = ", $inscricaoEstadual);
		return $this->db->get('dtb_resumofiscal')->row();
	}

	public function consultaMsgNaoLidas(){
		$this->db->select('dtb_resumofiscal.razaoSocial as razao_social, dtb_resumofiscal.inscricaoEstadual as inscricao_estadual_completo, dtb_resumofiscal.cnpjcpf as cnpj_completo, dtb_resumofiscal.msg_nao_lidas as msg_nao_lidas, e.vinculo_contador,  e.situacao_cadastral, e.situacao, e.motivo, e.condicao, e.forma_pagamento, e.motivo_situacao_cadastral, e.id, e.situacao_dte, e.situacao_conta_dte, e.senha_sefaz, e.login_sefaz');

		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		//$this->db->group_by("pergunta25");
		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Todas Lidas%' ");
		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Não há mensagem%' ");
		$this->db->where("trim(msg_nao_lidas) != ", "");
		$this->db->where("msg_nao_lidas is not null");
		$this->db->group_by("trim(cnpjcpf)");
		return $this->db->get('dtb_resumofiscal')->result();	
	}

	public function listar_resumo_fiscal_por_empresa($id) {
		$this->db->select('*');

		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		$this->db->where("e.id", $id);
		$this->db->group_by("trim(cnpjcpf)");
		return $this->db->get('dtb_resumofiscal')->result();
	}

	public function qtd_registros_msg_nao_lidas(){
		$this->db->select('count(distinct(trim(cnpjcpf))) as valor');

		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		//$this->db->group_by("pergunta25");
		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Todas Lidas%' ");
		$this->db->where("trim(msg_nao_lidas) NOT LIKE '%Não há mensagem%' ");
		$this->db->where("trim(msg_nao_lidas) != ", "");
		$this->db->where("msg_nao_lidas is not null");
		// $this->db->group_by("trim(cnpjcpf)");
		return $this->db->get('dtb_resumofiscal')->row();	
	}


	public function listagem_notificacoes_msg_dte(){
		$this->db->select('dtb_resumofiscal.razaoSocial as razao_social, ndte.data_atualizacao as data_atualizacao');

		$this->db->join('dtb_resumofiscal','trim(ndte.cnpjstring) = trim(dtb_resumofiscal.cnpjcpf)','left');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}
		//$this->db->group_by("pergunta25");
		$this->db->where("trim(dtb_resumofiscal.msg_nao_lidas) NOT LIKE '%Todas Lidas%' ");
		$this->db->where("trim(dtb_resumofiscal.msg_nao_lidas) NOT LIKE '%Não há mensagem%' ");
		$this->db->where("trim(dtb_resumofiscal.msg_nao_lidas) != ", "");
		$this->db->where("msg_nao_lidas is not null");
		$this->db->group_by("trim(ndte.cnpjcpf)");
		$this->db->order_by("ndte.id desc");
		return $this->db->get('notificacoemsgdte ndte')->result();	
	}

	public function simples_nacional() {
		$this->db->select('dtb_resumofiscal.razaoSocial as razaoSocial, dtb_resumofiscal.cnpjcpf as cnpjcpf, dtb_resumofiscal.inscricaoEstadual as inscricaoEstadual, pergunta1, pergunta2, pergunta3, pergunta4, pergunta6, pergunta7, pergunta8,pergunta9, pergunta18, pergunta19, e.id, e.senha_sefaz, e.login_sefaz, pergunta25');
		$this->db->join('dtb_empresas e','trim(dtb_resumofiscal.cnpjcpf) = trim(e.cnpj_completo)','left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("e.forma_pagamento = ", "SIMPLES NACIONAL");

		$this->db->group_by("dtb_resumofiscal.cnpjcpf");
		return $this->db->get('dtb_resumofiscal')->result();
	}
}