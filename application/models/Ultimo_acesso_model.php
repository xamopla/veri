<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ultimo_acesso_model extends CI_Model {

	private $id;

	private $id_usuario;
	private $data_hora;
	private $ip;
	private $acao;

	//CONSTRUTOR
	public function __construct(){
		parent::__construct();
	}

	//GETTERS E SETTERS
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getIdUsuario() {
		return $this->id_usuario;
	}
	public function setIdUsuario($id_usuario) {
		$this->id_usuario = $id_usuario;
		return $this;
	}

	public function getDataHora() {
		return $this->data_hora;
	}
	public function setDataHora($data_hora) {
		$this->data_hora = $data_hora;
		return $this;
	}

	public function getIp() {
		return $this->ip;
	}
	public function setIp($ip) {
		$this->ip = $ip;
		return $this;
	}

	public function getAcao() {
		return $this->acao;
	}
	public function setAcao($acao) {
		$this->acao = $acao;
		return $this;
	}

	public function ultimo_acesso(){

		$dados = array(
	
				'id_usuario' => $this->getIdUsuario(),
				'data_hora' => $this->getDataHora(),
				'ip' => $this->getIp(),
				'acao' => $this->getAcao()
		);
	
		if ($this->db->insert('ultimo_acesso', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function ultimo_acesso_logout(){

		$dados = array(
	
				'id_usuario' => $this->getIdUsuario(),
				'data_hora' => $this->getDataHora(),
				'ip' => $this->getIp(),
				'acao' => $this->getAcao()
		);
	
		if ($this->db->insert('ultimo_acesso', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

}