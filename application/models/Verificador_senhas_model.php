<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Verificador_senhas_model extends CI_Model {

	public function listar() {

		$this->db->select('dtb_logins_validos.id_empresa, e.id as id , e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.id_contador, e.forma_pagamento, e.telefone_alternativo, e.celular_alternativo, e.email_alternativo, e.senha_sefaz, e.login_sefaz, e.situacao_cadastral, e.situacao, e.motivo, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte,');

		$this->db->join('dtb_empresas as e', 'e.id = dtb_logins_validos.id_empresa', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtb_logins_validos.id_empresa = eu.id_empresa', 'left');
			$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('dtb_logins_validos.id_empresa');
		} 
		
		$this->db->where("status_da_senha", 'ERRO');
		return $this->db->get('dtb_logins_validos')->result();

	}

	public function qtd_empresas_invalidas(){
		$this->db->select('count(e.id) as qtd');

		$this->db->join('dtb_logins_validos', 'dtb_logins_validos.id_empresa = e.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
			$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			$this->db->group_by('e.id');
		} 
		
		$this->db->group_start();
			$this->db->where("flag_empresa_sem_ie is null");
			$this->db->where("login_sefaz is null");
			$this->db->where("senha_sefaz is null");
			$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->group_end();
		$this->db->or_where("dtb_logins_validos.status_da_senha", 'ERRO');
		$this->db->group_by('e.id');
		return $this->db->get('dtb_empresas e')->row();
	}

	public function qtd_empresas_senhas_invalidas(){
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT COUNT(distinct(e.id)) AS valor FROM `dtb_empresas` e 
			LEFT JOIN dtb_logins_validos d on d.id_empresa = e.id 
			LEFT JOIN dtb_empresa_usuario as eu on e.id = eu.id_empresa 
			WHERE (e.id_funcionario = ".$this->session->userdata['userprimesession']['id']. "
			OR eu.id_usuario = ".$this->session->userdata['userprimesession']['id'].")
			AND e.flag_empresa_sem_ie is null 
			AND inscricao_estadual_completo not like '%PR%' 
			AND e.situacao_cadastral != 'BAIXADO' 
			AND ( (e.login_sefaz is null AND e.senha_sefaz is null) OR (d.status_da_senha = 'ERRO') )";
		}else{
			$sql = "SELECT COUNT(distinct(e.id)) AS valor FROM `dtb_empresas` e 
			LEFT JOIN dtb_logins_validos d on d.id_empresa = e.id 
			WHERE e.flag_empresa_sem_ie is null 
			AND inscricao_estadual_completo not like '%PR%' 
			AND e.situacao_cadastral != 'BAIXADO' 
			AND ( (e.login_sefaz is null AND e.senha_sefaz is null) OR (d.status_da_senha = 'ERRO') )";
		}
		
		return $this->db->query($sql)->row();
	}

	public function listar_empresa_senhas_invalidas(){
		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT * FROM `dtb_empresas` e 
			LEFT JOIN dtb_logins_validos d on d.id_empresa = e.id 
			LEFT JOIN dtb_empresa_usuario as eu on e.id = eu.id_empresa 
			WHERE (e.id_funcionario = ".$this->session->userdata['userprimesession']['id']. "
			OR eu.id_usuario = ".$this->session->userdata['userprimesession']['id'].")
			AND ( (e.flag_empresa_sem_ie is null AND e.login_sefaz is null AND e.senha_sefaz is null) OR (d.status_da_senha = 'ERRO') ) AND e.situacao_cadastral != 'BAIXADO' GROUP BY e.id";
		}else{
			$sql = "SELECT * FROM `dtb_empresas` e 
			LEFT JOIN dtb_logins_validos d on d.id_empresa = e.id 
			WHERE ( (e.flag_empresa_sem_ie is null AND e.login_sefaz is null AND e.senha_sefaz is null) OR (d.status_da_senha = 'ERRO') ) AND e.situacao_cadastral != 'BAIXADO' GROUP BY e.id";
		}
		
		return $this->db->query($sql)->result();
	}

	public function listar_empresas_invalidas(){
		$this->db->select('dtb_logins_validos.id_empresa, e.id as id , e.inscricao_estadual_completo, e.cnpj_completo, e.razao_social, e.nome_fantasia, e.natureza_juridica, e.cep, e.logradouro, e.numero, e.complemento, e.bairro, e.cidade, e.uf, e.telefone, e.email, e.id_contador, e.forma_pagamento, e.telefone_alternativo, e.celular_alternativo, e.email_alternativo, e.senha_sefaz, e.login_sefaz, e.situacao_cadastral, e.situacao, e.motivo, e.motivo_situacao_cadastral, e.situacao_dte, e.situacao_conta_dte');

		$this->db->join('dtb_logins_validos', 'dtb_logins_validos.id_empresa = e.id', 'left');

		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'e.id = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
            
			// $this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
			// $this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
			// $this->db->group_by('e.id');
		} 
		
		$this->db->where("flag_empresa_sem_ie is null");
		$this->db->where("situacao_cadastral !=", 'BAIXADO');
		$this->db->where("situacao_cadastral !=", '');
		$this->db->where("inscricao_estadual_completo not like '%PR%' ");
		
		$this->db->group_start();
			$this->db->where("login_sefaz is null AND senha_sefaz is null");
			$this->db->or_where("dtb_logins_validos.status_da_senha", 'ERRO');
		$this->db->group_end();
		
		$this->db->group_by('e.id');
		return $this->db->get('dtb_empresas e')->result();
	}

	public function qtd_empresas_senhas_erradas(){

		$this->db->select('COUNT(distinct(dtb_logins_validos.id_empresa)) AS valor');
		
		$this->db->join('dtb_empresas as e', 'e.id = dtb_logins_validos.id_empresa', 'left');
		
		if ($this->session->userdata['userprimesession']['nivel']==2){
			
			$this->db->join('dtb_empresa_usuario as eu', 'dtb_logins_validos.id_empresa = eu.id_empresa', 'left');
			$this->db->group_start();
				$this->db->where("e.id_funcionario", $this->session->userdata['userprimesession']['id']);
				$this->db->or_where('eu.id_usuario', $this->session->userdata['userprimesession']['id']);
            $this->db->group_end();
		}

		$this->db->where("status_da_senha", 'ERRO');
		$this->db->where("e.situacao_cadastral !=", 'BAIXADO');
		return $this->db->get('dtb_logins_validos')->row();
	}

	public function testar_senha($dados_post){

		$senha_sefaz = $dados_post["senha_sefaz"];
        $login_sefaz = $dados_post["login_sefaz"];
        $id_empresa = $dados_post["id_empresa"];

        //CRIANDO OS DADOS DO REQUEST COM O LOGIN FORNECIDO
		$data = array('ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin', 
	'__VIEWSTATEGENERATOR'=>'CC7A3876',
	'__PREVIOUSPAGE'=>'MEFqmrRFdAFxiPWkBvrml05rnIRe8jJVKDh1wj89FKs1G2Rek4IryjTaHlC-rNd-2HtkAqhiSUIKkE54qPVkuVoJhSkLx4ZYDPUwBFKncms1',
	'__EVENTVALIDATION'=>'/wEdAApjJ4ohBpDDzCIiP8wjuq6hIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniVoFUXmUm1skx5kv6KTgEBVucq+Nk=',
	'__VIEWSTATE'=>'/wEPDwUKMTYwMTM5MTE2OA9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFA0RURWQCBQ8WAh8ABS5Eb21pYyYjMjM3O2xpbyBUcmlidXQmIzIyNTtyaW8gRWxldHImIzI0NDtuaWNvZAIJD2QWAmYPZBYCAgEPZBYGAgMPDxYIHghDc3NDbGFzcwUKbW9kYWwgZmFkZR4wX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUc8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgRXJybx4yX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHgRfIVNCAgIWDB4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljHglkcmFnZ2FibGUFBHRydWVkAgUPZBYGAgcPFgIfAGVkAgsPDxYCHgdWaXNpYmxlZ2RkAg0PDxYCHwtnZGQCBw8WAh8ABf0HPHRhYmxlPg0KDQo8dHI+DQoNCiAgICA8dGQ+DQogICAgDQogICAgICAgIDxkaXYgY2xhc3M9ImNvbDEgc3BhbjUiPg0KICAgICAgICAgICAgPGltZyBzcmM9Ii9kdGUvZHRfZV9maW5hbC5wbmciIGFsdD0iTG9nbyBEVEUiLz4NCiAgICAgICAgICAgIDxiciAvPjxiciAvPjxiciAvPg0KICAgICAgICA8L2Rpdj4NCiAgICA8L3RkPg0KPC90cj4NCjx0cj4NCiAgICA8dGQ+DQogICANCiAgICAgIDxkaXY+DQoNCiAgICAgICAgPHA+RmVycmFtZW50YSBleHRlcm5hIHBhcmEgZ2VyZW5jaWFtZW50byBkYSBDb250YSBubyBEb21pY8OtbGlvIFRyaWJ1dMOhcmlvIEVsZXRyw7RuaWNvIC0gRFRFPC9wPg0KICAgIA0KICAgICAgICA8aDE+DQogICAgDQogICAgICAgIDwvaDE+DQogICAgICAgIDxoMj5JbmZvcm1hw6fDtWVzLjwvaDI+DQogICAgDQogICAgICAgIDxwPg0KICAgICAgICAgICAgUHJvamV0YWRvIGNvbSBhIGlkZWlhIGRlIGRlc2Vudm9sdmVyIGRlIGZvcm1hIG1haXMgaW50dWl0aXZhLCBlZmljaWVudGUsIMO6dGlsIGUgc2ltcGxlcy4gDQogICAgICAgICAgICA8YnIgLz4NCiAgICAgICAgICAgIENvbSBhIGZlcnJhbWVudGEgcG9kZXLDoSBjYWRhc3RyYXIgdW1hIENvbnRhIERvbWljw61saW8gVHJpYnV0w6FyaW8sIEVuZGVyZcOnb3MgRWxldHLDtG5pY29zLCBUZWxlZm9uZXMgQ2VsdWxhcmVzLCBMZXIgTWVuc2FnZW5zIFJlY2ViaWRhcyBlIFZpc3VhbGl6YXIgUmVsYXTDs3Jpb3MuDQogICAgICAgIDwvcD4NCiAgICAgICAgPHVsPg0KICAgICAgICAgICAgPGxpPg0KICAgICAgICAgICAgICAgIDxwPkFjZXNzZSBhIHBhcnRpciBkZSBxdWFscXVlciBkaXNwb3NpdGl2byBtw7N2ZWwsIG7Do28gc2UgcHJlb2N1cGUgY29tIGEgcmVzb2x1w6fDo28sIGFqdXN0YW1vcyBhIHRlbGEgcGFyYSBhIG1lbGhvciB2aXN1YWxpemHDp8Ojby48L3A+DQogICAgICAgICAgICA8L2xpPiAgICANCiAgICAgICAgPC91bD4NCg0KICAgICAgICA8L2Rpdj4NCg0KICAgIDwvdGQ+DQo8L3RyPg0KDQo8L3RhYmxlPmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFCm1vZGFsIGZhZGUfBAICFgwfBQUCLTEfBgUGZGlhbG9nHwcFBHRydWUfCAUFZmFsc2UfCQUGc3RhdGljHwoFBHRydWVkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFD0FTTElCOiAyLjEuMTAuMGRkEzytXcLa4YyfkajFBRKCVE2wQGc=',
	'ctl00$PHCentro$btnLogin'=>'Entrar',

	'ctl00$PHCentro$userLogin'=>$login_sefaz,
	'ctl00$PHCentro$userPass'=>$senha_sefaz
	);

	//FAZENDO A REQUISIÇÃO USANDO CURL
	$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
	curl_setopt($ch, CURLOPT_POST, 1); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
	curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	$store = curl_exec($ch);
	

	$pos = strpos($store, "Resumo Fiscal");
	if($pos != false){
		//LOGIN FOI REALIZADO COM SUCESSO
		$this->atualizar_empresa($id_empresa,$login_sefaz,$senha_sefaz);		
		$this->alterar_status($id_empresa);
		return json_encode(array("error"=>false, "msg"=>"Senha Verificada e Correta!"));
	}else{
		//COMO O LOGIN FOI NEGADO, IREMOS FAZER UM REQUEST COM O LOGIN CORRETO PARA O CONTADOR NO SITE DA SEFAZ SE RESETAR
		$this->resetar_consulta();
		return(json_encode(array("error"=>true, "msg"=>"Senha Incorreta! Por favor tente outra")));		
	} 

	}

	public function resetar_consulta(){

		//SETANDO O LOGIN MASTER
		$loginMaster = "10964257600";
		$senhaMaster = "exa2107";

		//CRIANDO OS DADOS DO REQUEST COM O LOGIN FORNECIDO
		$data = array('ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin', 
	'__VIEWSTATEGENERATOR'=>'CC7A3876',
	'__PREVIOUSPAGE'=>'MEFqmrRFdAFxiPWkBvrml05rnIRe8jJVKDh1wj89FKs1G2Rek4IryjTaHlC-rNd-2HtkAqhiSUIKkE54qPVkuVoJhSkLx4ZYDPUwBFKncms1',
	'__EVENTVALIDATION'=>'/wEdAApjJ4ohBpDDzCIiP8wjuq6hIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniVoFUXmUm1skx5kv6KTgEBVucq+Nk=',
	'__VIEWSTATE'=>'/wEPDwUKMTYwMTM5MTE2OA9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFA0RURWQCBQ8WAh8ABS5Eb21pYyYjMjM3O2xpbyBUcmlidXQmIzIyNTtyaW8gRWxldHImIzI0NDtuaWNvZAIJD2QWAmYPZBYCAgEPZBYGAgMPDxYIHghDc3NDbGFzcwUKbW9kYWwgZmFkZR4wX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUc8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgRXJybx4yX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHgRfIVNCAgIWDB4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljHglkcmFnZ2FibGUFBHRydWVkAgUPZBYGAgcPFgIfAGVkAgsPDxYCHgdWaXNpYmxlZ2RkAg0PDxYCHwtnZGQCBw8WAh8ABf0HPHRhYmxlPg0KDQo8dHI+DQoNCiAgICA8dGQ+DQogICAgDQogICAgICAgIDxkaXYgY2xhc3M9ImNvbDEgc3BhbjUiPg0KICAgICAgICAgICAgPGltZyBzcmM9Ii9kdGUvZHRfZV9maW5hbC5wbmciIGFsdD0iTG9nbyBEVEUiLz4NCiAgICAgICAgICAgIDxiciAvPjxiciAvPjxiciAvPg0KICAgICAgICA8L2Rpdj4NCiAgICA8L3RkPg0KPC90cj4NCjx0cj4NCiAgICA8dGQ+DQogICANCiAgICAgIDxkaXY+DQoNCiAgICAgICAgPHA+RmVycmFtZW50YSBleHRlcm5hIHBhcmEgZ2VyZW5jaWFtZW50byBkYSBDb250YSBubyBEb21pY8OtbGlvIFRyaWJ1dMOhcmlvIEVsZXRyw7RuaWNvIC0gRFRFPC9wPg0KICAgIA0KICAgICAgICA8aDE+DQogICAgDQogICAgICAgIDwvaDE+DQogICAgICAgIDxoMj5JbmZvcm1hw6fDtWVzLjwvaDI+DQogICAgDQogICAgICAgIDxwPg0KICAgICAgICAgICAgUHJvamV0YWRvIGNvbSBhIGlkZWlhIGRlIGRlc2Vudm9sdmVyIGRlIGZvcm1hIG1haXMgaW50dWl0aXZhLCBlZmljaWVudGUsIMO6dGlsIGUgc2ltcGxlcy4gDQogICAgICAgICAgICA8YnIgLz4NCiAgICAgICAgICAgIENvbSBhIGZlcnJhbWVudGEgcG9kZXLDoSBjYWRhc3RyYXIgdW1hIENvbnRhIERvbWljw61saW8gVHJpYnV0w6FyaW8sIEVuZGVyZcOnb3MgRWxldHLDtG5pY29zLCBUZWxlZm9uZXMgQ2VsdWxhcmVzLCBMZXIgTWVuc2FnZW5zIFJlY2ViaWRhcyBlIFZpc3VhbGl6YXIgUmVsYXTDs3Jpb3MuDQogICAgICAgIDwvcD4NCiAgICAgICAgPHVsPg0KICAgICAgICAgICAgPGxpPg0KICAgICAgICAgICAgICAgIDxwPkFjZXNzZSBhIHBhcnRpciBkZSBxdWFscXVlciBkaXNwb3NpdGl2byBtw7N2ZWwsIG7Do28gc2UgcHJlb2N1cGUgY29tIGEgcmVzb2x1w6fDo28sIGFqdXN0YW1vcyBhIHRlbGEgcGFyYSBhIG1lbGhvciB2aXN1YWxpemHDp8Ojby48L3A+DQogICAgICAgICAgICA8L2xpPiAgICANCiAgICAgICAgPC91bD4NCg0KICAgICAgICA8L2Rpdj4NCg0KICAgIDwvdGQ+DQo8L3RyPg0KDQo8L3RhYmxlPmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFCm1vZGFsIGZhZGUfBAICFgwfBQUCLTEfBgUGZGlhbG9nHwcFBHRydWUfCAUFZmFsc2UfCQUGc3RhdGljHwoFBHRydWVkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFD0FTTElCOiAyLjEuMTAuMGRkEzytXcLa4YyfkajFBRKCVE2wQGc=',
	'ctl00$PHCentro$btnLogin'=>'Entrar',

	'ctl00$PHCentro$userLogin'=>$loginMaster,
	'ctl00$PHCentro$userPass'=>$senhaMaster
	);

		//FAZENDO A REQUISIÇÃO USANDO CURL
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
	}

	public function get_status_senha($id){

		$this->db->select('COUNT(distinct(dtb_logins_validos.id_empresa)) AS valor'); 

		$this->db->where("dtb_logins_validos.status_da_senha", 'ERRO');
		$this->db->where("dtb_logins_validos.id_empresa", $id);

		return $this->db->get('dtb_logins_validos')->row();
	}

	public function atualizar_empresa($id_empresa,$login_sefaz,$senha_sefaz){

		$dados = array(
			
				'login_sefaz' => $login_sefaz,
				'senha_sefaz' => $senha_sefaz,
		);
	
		if ($this->db->update('dtb_empresas', $dados, "id={$id_empresa}")){
			return TRUE;
		} else {
			return FALSE;
		} 
	}

	public function alterar_status($id_empresa){

		$dados = array(
			
				'status_da_senha' => "OK",
		);
	
		if ($this->db->update('dtb_logins_validos', $dados, "id_empresa={$id_empresa}")){
			return TRUE;
		} else {
			return FALSE;
		} 
	}
}