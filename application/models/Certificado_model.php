<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Certificado_model extends CI_Model {	

	public function get_status_certificado($cnpj){
		$this->db->select('*');
		$this->db->from('dtb_certificado');
		$this->db->where('cnpj_data', $cnpj);
		$retorno_bd_certificado = $this->db->get()->result_array();
		if(count($retorno_bd_certificado) == 0){
			//Empresa não possui certificado salvo no BD
			return(array("situacao"=>"Não Configurado", "cor"=>"#f00", "validade"=>""));
		}else{
			//Empresa possui certificado salvo no BD
			if(date('Ymd', $retorno_bd_certificado[0]["data_validade"]) < date('Ymd')){
				//Certificado Vencido
				return(array("situacao"=>"Vencido", "cor"=>"#ff6a00", "validade"=>date('d/m/Y', $retorno_bd_certificado[0]["data_validade"])));
			}else{
				//Certificado Configurado e Válido
				return(array("situacao"=>"Configurado", "cor"=>"#1fb51f", "validade"=>date('d/m/Y', $retorno_bd_certificado[0]["data_validade"])));
			}
		}
	}


	public function get_todos_certificados(){
		$this->db->select('*');
		$this->db->from('dtb_certificado');
		$retorno_bd_certificados = $this->db->get()->result_array();
		$array_retorno = array();
		foreach ($retorno_bd_certificados as $retorno_bd_certificado) {
			if(date('Ymd', $retorno_bd_certificado["data_validade"]) < date('Ymd')){
				array_push($array_retorno, array("cnpj"=>$retorno_bd_certificado["cnpj"], "valido"=>false));
			}else{
				array_push($array_retorno, array("cnpj"=>$retorno_bd_certificado["cnpj"], "valido"=>true));
			}
		}

		return $array_retorno;
	}

	public function configurar($dados_post, $file_certificado){

		$pfxCertPrivado = $file_certificado["tmp_name"];
        $cert_password  = $dados_post["senha"];

        if (!file_exists($pfxCertPrivado)) {
            return(json_encode(array("error"=>true, "msg"=>"Não foi possível executar a rotina.\nEntre em contato com o Administrador!")));
        }

        $pfxContent = file_get_contents($pfxCertPrivado);

        if (!openssl_pkcs12_read($pfxContent, $x509certdata, $cert_password)) {
            return json_encode(array("error"=>true, "msg"=>"Senha ou Certificado Inválido!"));
        }else{
            $CertPriv   = array();
            $CertPriv   = openssl_x509_parse(openssl_x509_read($x509certdata['cert']));

            $teste = explode(":", $CertPriv['subject']['CN']);
            $teste_cnpj = $teste[1];

            $cnpj_certificado = trim($teste_cnpj); 

            $caminho_completo = $dados_post["dir_upload_cert"].''.$cnpj_certificado.'.pfx';
            $caminho_completo = str_replace('crons-api/', '', $caminho_completo);
 	
 			// if($cnpj_certificado != $dados_post["cnpj"]){
    //             return(json_encode(array("error"=>true, "msg"=>"Certificado não se refere a esta empresa!")));
    //         }else{

	            if(move_uploaded_file($pfxCertPrivado, $dados_post["dir_upload_cert"].$dados_post["cnpj"].".pfx")){
	                $this->db->delete('dtb_certificado', array('cnpj_data' => $dados_post["cnpj"])); 
					$this->db->insert('dtb_certificado', array('cnpj_data'=>$dados_post["cnpj"], 'id_empresa'=>$dados_post["id_empresa"], 'caminho_arq'=>$caminho_completo, 'pass'=>$dados_post["senha"], 'data_validade'=>$CertPriv['validTo_time_t']));

					// CRIAÇÃO DO DOCUMENTO CORRESPONDENTE
					$verificador = $this->verificar_se_ja_existe_registro($dados_post["cnpj"]);

					if ($verificador) {
						
						$var_id_empresa = $dados_post["id_empresa"];
						$var_data_emissao = implode("-", array_reverse(explode("/", date('d/m/Y'))));
						$var_data_validade = implode("-", array_reverse(explode("/", date('d/m/Y', $CertPriv['validTo_time_t']))));
						$var_chave = $dados_post["cnpj"]; 

						$this->atualizar_tabela_documentos($var_id_empresa, $var_data_emissao, $var_data_validade, $var_chave, $verificador->id);

					} else {
						$this->db->insert('dtb_documentos', array('ativo'=>'Sim', 'id_empresa'=>$dados_post["id_empresa"], 'id_tipoDocumento'=>'8', 'numero_documento'=>'', 'numero_protocolo'=>'', 'dataEmissao'=>implode("-", array_reverse(explode("/", date('d/m/Y')))), 'dataValidade'=>implode("-", array_reverse(explode("/", date('d/m/Y', $CertPriv['validTo_time_t'])))), 'diasNotificacao'=>'30', 'id_contabilidade'=>'1', 'id_certificado_cnpj'=>$dados_post["cnpj"]));
					}					

	                return json_encode(array("error"=>false, "msg"=>"Certificado Cadastrado Com Sucesso!"));
	            }else{
	                return(json_encode(array("error"=>true, "msg"=>"Não foi possível salvar o certificado.\nEntre em contato com o Administrador!")));
	            } 

            // }
        }
		
	}

	public function configurar_certificado_filial($dados_post, $file_certificado){

		$pfxCertPrivado = $file_certificado["tmp_name"];
        $cert_password  = $dados_post["senha"];

        if (!file_exists($pfxCertPrivado)) {
            return(json_encode(array("error"=>true, "msg"=>"Não foi possível executar a rotina.\nEntre em contato com o Administrador!")));
        }

        $pfxContent = file_get_contents($pfxCertPrivado);

        if (!openssl_pkcs12_read($pfxContent, $x509certdata, $cert_password)) {
            return json_encode(array("error"=>true, "msg"=>"Senha ou Certificado Inválido!"));
        }else{
            $CertPriv   = array();
            $CertPriv   = openssl_x509_parse(openssl_x509_read($x509certdata['cert']));

            $teste = explode(":", $CertPriv['subject']['CN']);
            $teste_cnpj = $teste[1];

            $cnpj_certificado = trim($teste_cnpj); 

            $caminho_completo = $dados_post["dir_upload_cert"].''.$cnpj_certificado.'.pfx';
            $caminho_completo = str_replace('crons-api/', '', $caminho_completo);

 
            if(move_uploaded_file($pfxCertPrivado, $dados_post["dir_upload_cert"].$dados_post["cnpj"].".pfx")){
                $this->db->delete('dtb_certificado', array('cnpj_data' => $dados_post["cnpj"])); 
				$this->db->insert('dtb_certificado', array('cnpj_data'=>$dados_post["cnpj"], 'id_empresa'=>$dados_post["id_empresa"], 'caminho_arq'=>$caminho_completo, 'pass'=>$dados_post["senha"], 'data_validade'=>$CertPriv['validTo_time_t']));

				// CRIAÇÃO DO DOCUMENTO CORRESPONDENTE
				$verificador = $this->verificar_se_ja_existe_registro($dados_post["cnpj"]);

				if ($verificador) {
					
					$var_id_empresa = $dados_post["id_empresa"];
					$var_data_emissao = implode("-", array_reverse(explode("/", date('d/m/Y'))));
					$var_data_validade = implode("-", array_reverse(explode("/", date('d/m/Y', $CertPriv['validTo_time_t']))));
					$var_chave = $dados_post["cnpj"]; 

					$this->atualizar_tabela_documentos($var_id_empresa, $var_data_emissao, $var_data_validade, $var_chave, $verificador->id);

				} else {
					$this->db->insert('dtb_documentos', array('ativo'=>'Sim', 'id_empresa'=>$dados_post["id_empresa"], 'id_tipoDocumento'=>'8', 'numero_documento'=>'', 'numero_protocolo'=>'', 'dataEmissao'=>implode("-", array_reverse(explode("/", date('d/m/Y')))), 'dataValidade'=>implode("-", array_reverse(explode("/", date('d/m/Y', $CertPriv['validTo_time_t'])))), 'diasNotificacao'=>'30', 'id_contabilidade'=>'1', 'id_certificado_cnpj'=>$dados_post["cnpj"]));
				}					

                return json_encode(array("error"=>false, "msg"=>"Certificado Cadastrado Com Sucesso!"));
            }else{
                return(json_encode(array("error"=>true, "msg"=>"Não foi possível salvar o certificado.\nEntre em contato com o Administrador!")));
            }
        }
		
	}

	public function verificar_se_ja_existe_registro($cnpj){
		$this->db->select('id'); 
		$this->db->where('id_certificado_cnpj', $cnpj);
		return $this->db->get('dtb_documentos')->row();
	}

	public function atualizar_tabela_documentos($var_id_empresa, $var_data_emissao, $var_data_validade, $var_chave, $verificador){

		$dados = array(
			
				'dataEmissao' => $var_data_emissao,
				'dataValidade' => $var_data_validade,
		);
	
		if ($this->db->update('dtb_documentos', $dados, "id={$verificador}")){
			return TRUE;
		} else {
			return FALSE;
		} 
	}

	public function listar_certificados_todos(){
		$sql1 = "SELECT c.id_empresa as id, c.data_validade as data_validade, e.cnpj_completo as cnpj_completo, e.razao_social as razao_social, e.inscricao_estadual_completo as inscricao_estadual_completo, false as is_procuracao FROM dtb_certificado as c LEFT JOIN dtb_empresas as e ON c.id_empresa = e.id GROUP BY e.id";

		$sql2 = "SELECT c.id_contador as id, c.data_validade as data_validade, '' as cnpj_completo, '' as razao_social, '' as inscricao_estadual_completo, true as is_procuracao FROM dtb_certificado_contador as c GROUP BY c.id";

		$sql = $sql1." UNION ".$sql2;

		return $this->db->query($sql)->result();
	}


	public function verifica_se_certificado_empresa($id_empresa){
        $this->db->select('COUNT(*) AS qtd');
        $this->db->where('id_empresa', $id_empresa);
        return $this->db->get('dtb_certificado')->row();
    }

}