<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Certidao_negativa_estados_model extends CI_Model {

    public function getFiltro() {
        return $this->filtro;
    }
     
    public function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

    public function getFiltro_uf() {
        return $this->filtro_uf;
    }
     
    public function setFiltro_uf($filtro_uf) {
        $this->filtro_uf = $filtro_uf;
    }

    // LISTAR ESTADOS
    public function listar_estadual_ac($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_acre db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_al($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_alagoas db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_ap($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_amapa db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_am($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_amazonas db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_ba($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_sefaz db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_ce($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_ceara db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_df($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_distrito_federal db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_es($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_espirito_santo db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_go($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_goias db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_ma($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_maranhao db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_ms($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_mato_grosso_sul db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_mt($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_matogrosso db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_mg($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_minas db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_pa($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_para db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_pb($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_paraiba db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_pr($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_parana db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_pe($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_pernambuco db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    // public function listar_estadual_pi($estado){
    //     $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
    //     $this->db->from('dtb_certidao_estadual_sp db');
    //     $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

    //     if ($this->session->userdata['userprimesession']['nivel']==2){
            
    //         $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
    //     }

    //     $this->db->where("dtbe.uf", $estado);        
    //     $this->db->group_by('db.cnpj');
    //     return $this->db->get()->result();
    // }

    public function listar_estadual_rj($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_riodejaneiro db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_rn($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('    dtb_certidao_riograndedonorte db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_rs($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_riograndedosul db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_ro($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_rondonia db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_rr($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_roraima db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_sc($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_santa_catarina db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_sp($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_estadual_sp db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_se($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_sergipe db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }

    public function listar_estadual_to($estado){
        $this->db->select('db.cnpj, db.razao_social, db.status, db.caminho_download, db.data_execucao, dtbe.cidade, dtbe.uf');
        $this->db->from('dtb_certidao_tocantins db');
        $this->db->join('dtb_empresas dtbe','db.cnpj = dtbe.cnpj');

        if ($this->session->userdata['userprimesession']['nivel']==2){
            
            $this->db->where_in("dtbe.cnpj", $this->session->userdata['lista_empresas_colaborador']);
        }

        $this->db->where("dtbe.uf", $estado);        
        $this->db->group_by('db.cnpj');
        return $this->db->get()->result();
    }
}