<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mensagens_model extends CI_Model {


	public function qtd_mensagens(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//MENSAGEM DTE
		$sql1_colaborador = "SELECT count(*) as total 
							 FROM dtb_mensagens_dte as m 
							 LEFT JOIN dtb_empresas as e ON m.id_empresa = e.id
							 WHERE status like '%não Lida%' AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		$sql1_admin = "SELECT count(*) as total 
							 FROM dtb_mensagens_dte as m 
							 LEFT JOIN dtb_empresas as e ON m.id_empresa = e.id
							 WHERE status like '%não Lida%' GROUP BY m.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT count(total) as total FROM (". $sql1_colaborador.") as resultado ";
		}else{
			$sql = "SELECT count(total) as total FROM (".$sql1_admin.") as resultado ";
		}

		return $this->db->query($sql)->row();
		
	}


	public function qtd_mensagens_lidas(){
		$id_colaborador = $this->session->userdata['userprimesession']['id'];
		$sql = "";

		//MENSAGEM DTE
		$sql1_colaborador = "SELECT count(*) as total 
							 FROM dtb_mensagens_dte as m 
							 LEFT JOIN dtb_empresas as e ON m.id_empresa = e.id
							 WHERE status not like '%não Lida%' AND (e.cnpj in (".implode(",", $this->session->userdata['lista_empresas_colaborador']).")) GROUP BY m.id";

		$sql1_admin = "SELECT count(*) as total 
							 FROM dtb_mensagens_dte as m 
							 LEFT JOIN dtb_empresas as e ON m.id_empresa = e.id
							 WHERE status not like '%não Lida%' GROUP BY m.id";

		if ($this->session->userdata['userprimesession']['nivel']==2){
			$sql = "SELECT count(total) as total FROM (". $sql1_colaborador.") as resultado ";
		}else{
			$sql = "SELECT count(total) as total FROM (".$sql1_admin.") as resultado ";
		}

		return $this->db->query($sql)->row();
		
	}


	public function buscar_mensagem_ecac($id_mensagem){
		$this->db->select('m.id as id_caixa_postal_mensagem, m.caixa_postal_id, m.assunto, m.recebida_em, c.cnpj_data as cnpj, m.id_mensagem as id_mensagem');
		$this->db->from('dtb_ecac_caixa_postal_mensagem m');
		$this->db->join('dtb_ecac_caixa_postal c','m.caixa_postal_id = c.id');
		$this->db->where("m.id", $id_mensagem);
		return $this->db->get()->row();
	}


	public function insere_historico($objeto){
		date_default_timezone_set('America/Sao_Paulo');
		$usuario = "";
		if ($this->session->userdata['userprimesession']['nivel'] == 1){
			if (empty($this->session->userdata['userprimesession']['razao_social'])) {
				$usuario = $this->session->userdata['userprimesession']['nome']; 
			}else{
      			$usuario = $this->session->userdata['userprimesession']['razao_social'];
    		}
        } else {
          $usuario = $this->session->userdata['userprimesession']['nome']; 
        }

		$dados = array(
	
				'id_mensagem' => $objeto->id_mensagem,
				'assunto' => $objeto->assunto,
				'caixa_postal_id' => $objeto->caixa_postal_id,
				'recebida_em' => $objeto->recebida_em,
				'cnpj' => $objeto->cnpj,
				'data_alteracao' => date('Y-m-d H:i:s'),
				'nome_usuario' => $usuario
		);
	
		if ($this->db->insert('dtb_historico_leitura_mensagem_ecac', $dados)){
			return 1;
		} else {
			return 0;
		}
	}


	public function buscar_lida_por($id_caixa_postal, $cnpj, $assunto, $recebida_em, $id_mensagem){
		$this->db->select('m.nome_usuario, m.data_alteracao');
		$this->db->from('dtb_historico_leitura_mensagem_ecac m');
		$this->db->where("m.caixa_postal_id", $id_caixa_postal);
		$this->db->where("m.recebida_em", $recebida_em);
		$this->db->where("m.assunto", $assunto);
		// $this->db->where("m.id_mensagem", $id_mensagem);
		$this->db->where("trim(m.cnpj)", $cnpj);
		return $this->db->get()->row();
	}


}