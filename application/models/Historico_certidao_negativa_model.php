<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Historico_certidao_negativa_model extends CI_Model {

	public function consultar_fgts($cnpj){

		$sql = "SELECT * FROM dtb_historico_cnd_fgts m WHERE m.cnpj_completo = ".$cnpj." ORDER BY m.id DESC ";

		return $this->db->query($sql)->result();
	}

	public function consultar_pgfn($cnpj){

		$sql = "SELECT * FROM dtb_historico_cnd_pgfn m WHERE m.cnpj_completo = ".$cnpj." ORDER BY m.id DESC ";

		return $this->db->query($sql)->result();
	}

	public function consultar_trabalhista($cnpj){

		$sql = "SELECT * FROM dtb_historico_cnd_trabalhista m WHERE m.cnpj_completo = ".$cnpj." ORDER BY m.id DESC ";

		return $this->db->query($sql)->result();
	}

}