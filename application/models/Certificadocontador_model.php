<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Certificadocontador_model extends CI_Model {

	public function get_status_certificado($id_contador){
		$this->db->select('*');
		$this->db->from('dtb_certificado_contador');
		$this->db->where('id_contador', $id_contador);
		$retorno_bd_certificado = $this->db->get()->result_array();
		if(count($retorno_bd_certificado) == 0){
			//Empresa não possui certificado salvo no BD
			return(array("situacao"=>"Não Configurado", "cor"=>"#f00", "validade"=>""));
		}else{
			//Empresa possui certificado salvo no BD
			if(date('Ymd', $retorno_bd_certificado[0]["data_validade"]) < date('Ymd')){
				//Certificado Vencido
				return(array("situacao"=>"Vencido", "cor"=>"#ff6a00", "validade"=>date('d/m/Y', $retorno_bd_certificado[0]["data_validade"])));
			}else{
				//Certificado Configurado e Válido
				return(array("situacao"=>"Configurado", "cor"=>"#1fb51f", "validade"=>date('d/m/Y', $retorno_bd_certificado[0]["data_validade"])));
			}
		}
	}

	public function get_todos_certificados(){
		$this->db->select('*');
		$this->db->from('dtb_certificado_contador');
		$retorno_bd_certificados = $this->db->get()->result_array();
		$array_retorno = array();
		foreach ($retorno_bd_certificados as $retorno_bd_certificado) {
			if(date('Ymd', $retorno_bd_certificado["data_validade"]) < date('Ymd')){
				array_push($array_retorno, array("cnpj"=>$retorno_bd_certificado["cnpj"], "valido"=>false));
			}else{
				array_push($array_retorno, array("cnpj"=>$retorno_bd_certificado["cnpj"], "valido"=>true));
			}
		}

		return $array_retorno;
	}

	public function configurar($dados_post, $file_certificado){

		$pfxCertPrivado = $file_certificado["tmp_name"];
        $cert_password  = $dados_post["senha"];

        if (!file_exists($pfxCertPrivado)) {
            return(json_encode(array("error"=>true, "msg"=>"Não foi possível executar a rotina.\nEntre em contato com o Administrador!")));
        }

        $pfxContent = file_get_contents($pfxCertPrivado);

        if (!openssl_pkcs12_read($pfxContent, $x509certdata, $cert_password)) {
            return json_encode(array("error"=>true, "msg"=>"Senha ou Certificado Inválido!"));
        }else{
            $CertPriv   = array();
            $CertPriv   = openssl_x509_parse(openssl_x509_read($x509certdata['cert']));

            $teste = explode(":", $CertPriv['subject']['CN']);
            $teste_cnpj = $teste[1];

            $cnpj_certificado = trim($teste_cnpj); 

            $caminho_completo = $dados_post["dir_upload_cert"].''.$cnpj_certificado.'.pfx';
            $caminho_completo = str_replace('crons-api/', '', $caminho_completo);

 
                if(move_uploaded_file($pfxCertPrivado, $dados_post["dir_upload_cert"].$cnpj_certificado.".pfx")){
                    $this->db->delete('dtb_certificado_contador', array('id_contador' => $dados_post["id_contador"]));
					$this->db->insert('dtb_certificado_contador', array('id_contador'=>$dados_post["id_contador"], 'caminho_arq'=>$caminho_completo, 'pass'=>$dados_post["senha"], 'data_validade'=>$CertPriv['validTo_time_t']));

					// CRIAÇÃO DO DOCUMENTO CORRESPONDENTE
					$verificador = $this->verificar_se_ja_existe_registro($dados_post["cnpj"]);

					if ($verificador) {

						$var_id_empresa = null;
						$var_data_emissao = implode("-", array_reverse(explode("/", date('d/m/Y'))));
						$var_data_validade = implode("-", array_reverse(explode("/", date('d/m/Y', $CertPriv['validTo_time_t']))));
						$var_chave = $dados_post["cnpj"];

						$this->atualizar_tabela_documentos($var_id_empresa, $var_data_emissao, $var_data_validade, $var_chave, $verificador->id);

					} else {
						$this->db->insert('dtb_documentos', array('ativo'=>'Sim', 'id_empresa'=>'0', 'id_tipoDocumento'=>'8', 'numero_documento'=>'', 'numero_protocolo'=>'', 'dataEmissao'=>implode("-", array_reverse(explode("/", date('d/m/Y')))), 'dataValidade'=>implode("-", array_reverse(explode("/", date('d/m/Y', $CertPriv['validTo_time_t'])))), 'diasNotificacao'=>'30', 'id_contabilidade'=>'1', 'id_certificado_cnpj'=>$dados_post["cnpj"]));
					}

                    return json_encode(array("error"=>false, "msg"=>"Certificado Cadastrado Com Sucesso!"));
                }else{
                    return(json_encode(array("error"=>true, "msg"=>"Não foi possível salvar o certificado.\nEntre em contato com o Administrador!")));
                } 
        }
		
	}

	public function verificar_se_ja_existe_registro($cnpj){
		$this->db->select('id');
		$this->db->where('id_certificado_cnpj', $cnpj);
		return $this->db->get('dtb_documentos')->row();
	}

	public function atualizar_tabela_documentos($var_id_empresa, $var_data_emissao, $var_data_validade, $var_chave, $verificador){

		$dados = array(
			
				'dataEmissao' => $var_data_emissao,
				'dataValidade' => $var_data_validade,
		);
	
		if ($this->db->update('dtb_documentos', $dados, "id={$verificador}")){
			return TRUE;
		} else {
			return FALSE;
		} 
	}

	public function get()
	{		
		$query = $this->db->get('dtb_certificado_contador');
		return $query->result();
	}

	public function get_aux($id)
	{		
		$this->db->select('*');
		$this->db->join('dtb_contador_procuracao d','db.id_contador = d.id_contador', 'left');
		$this->db->where('d.id_empresa', $id);
		return $this->db->get('dtb_certificado_contador db')->result();
	}


	public function busca_todos_certificados(){
		$this->db->select('d.id, d.nome');
		$this->db->join('contadores d','db.id_contador = d.id');
		return $this->db->get('dtb_certificado_contador db')->result();
	}


	public function vicular_empresa_certificado($id_empresa, $id_contador){
	
		$dados = array(
	
				'id_empresa' => $id_empresa,
				'id_contador' => $id_contador
		);
	
		if ($this->db->insert('dtb_contador_procuracao', $dados)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function busca_certificado_empresa_vinculada($id){
		$this->db->select('db.id, d.nome');
		$this->db->join('contadores d','db.id_contador = d.id');
		$this->db->where('db.id_empresa', $id);
		return $this->db->get('dtb_contador_procuracao db')->row();
	}


	public function existe_certificado_contador($id){
		$this->db->select('COUNT(distinct(dtb_certificado_contador.id)) AS qtd');
        $this->db->where('id_contador', $id);
        return $this->db->get('dtb_certificado_contador')->row();
	}


	public function buscar_certificados_procuracao_vencidos(){
		$sql = "SELECT * from dtb_certificado_contador where id_contador in (SELECT id_contador FROM `dtb_contador_procuracao`)";

		return $this->db->query($sql)->result();
		
	}

}