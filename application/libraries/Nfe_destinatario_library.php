<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/Simple_html_dom.php');

class Nfe_destinatario_library {

	/**
	 * LOGIN URL
	 *
	 * URL que faz o login 
	 *
	 * @var	string
	 */
	protected $login_url		= 'https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_destinatario.aspx';

    /**
     * USUARIO
     *
     * USUARIO LOGIN
     *
     * @var	string
     */
    protected $usuario		= '';

    /**
     * SENHA
     *
     * SENHA LOGIN
     *
     * @var	string
     */
    protected $senha		= '';

    /**
     * COOKIE_PATH
     *
     * Caminho do cookie de sessão
     *
     * @var	string
     */
    protected $cookie_path		= '';

    /**
     * DADOS REQUEST SEFAZ
     *
     * Dados request sefaz
     *
     * @var	string
     */
    protected $dados_request_sefaz		= array();

    /**
     * DATA INICIAL
     *
     * Data inicial do filtro
     *
     * @var	string
     */
    protected $data_inicial		= '';

    /**
     * DATA FINAL
     *
     * Data final do filtro
     *
     * @var	string
     */
    protected $data_final		= '';

    /**
     * USER REPORT
     *
     * usuario relatorio
     *
     * @var	string
     */
    protected $user_report		= '';
    /**
     * DATA REPORT
     *
     * data relatorio
     *
     * @var	string
     */
    protected $data_report		= '';

    /**
     * html
     *
     * html carregado
     *
     * @var	string
     */

    private $html = '';

    /**
	 * CI Singleton
	 *
	 * @var	object
	 */
	protected $CI;

	private $curl;

	public function __construct($params = array())
	{
		$this->CI =& get_instance();
		$this->curl = curl_init();
		$this->initialize($params);
		$this->login();
        $this->html = new Simple_html_dom();

    }

	public function initialize(array $params = array())
	{
		foreach ($params as $key => $val)
		{
			if (property_exists($this, $key))
			{
				$this->$key = $val;
			}
		}

		return $this;
	}

    public function login(){
        $data = array(
            'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
            '__EVENTTARGET'=>'',
            '__EVENTARGUMENT'=>'',
            '__VIEWSTATE'=>'/wEPDwUKLTc5NjIwMjMwNg9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFBU5GRU5DZAIFDxYCHwAFNE5vdGEgRmlzY2FsIEVsZXRyJiMyNDQ7bmljYSAtIEF1dGVudGljYSYjMjMxOyYjMjI3O29kAgkPZBYCZg9kFgICAQ9kFgYCAw8PFggeCENzc0NsYXNzBRggbW9kYWwgZmFkZSAgbW9kYWwgZmFkZSAeMF9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxUaXR1bG9JRAVHPGkgY2xhc3M9J2ljb24tZml4ZWQtd2lkdGggaWNvbi13YXJuaW5nLXNpZ24gdGV4dC13YXJuaW5nJz48L2k+wqDCoEVycm8eMl9fUEhDZW50cm9fbXNnRXJyb0xvZ2luX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4EXyFTQgICFgoeCHRhYmluZGV4BQItMR4Ecm9sZQUGZGlhbG9nHgthcmlhLWhpZGRlbgUEdHJ1ZR4NZGF0YS1rZXlib2FyZAUFZmFsc2UeDWRhdGEtYmFja2Ryb3AFBnN0YXRpY2QCBw9kFgoCAQ8WAh4IZGlzYWJsZWRkZAIDDxYCHwpkZAIHDxYCHwBlZAIJDw8WAh4HVmlzaWJsZWdkZAILDw8WAh8LZ2RkAgkPFgIfAAWzBDxoMT5JbnN0cnXDp8O1ZXM6PC9oMT4NCjxiciAvPg0KICAgIA0KPHVsPg0KICAgIDxsaT4NCiAgICAgICAgU2Vyw6EgcGVybWl0aWRvIGFwZW5hcyBvIGFjZXNzbyBjb20gbG9naW4gZSBzZW5oYTsNCiAgICA8L2xpPg0KICAgIDxsaT4NCiAgICAgICAgQXDDs3MgMDMgdGVudGF0aXZhcyBpbnbDoWxpZGFzIGEgcMOhZ2luYSBkZSBMb2dpbiBpcsOhIGJsb3F1ZWFyIG8gYWNlc3NvIGRvIElQIGRlIG9yaWdlbSBwb3IgMTAgbWludXRvczsNCiAgICA8L2xpPg0KICAgIDxsaT4NCiAgICAgICAgQ2FzbyBuw6NvIGxlbWJyZSBhIHNlbmhhLCBjbGlxdWUgZW0gImVzcXVlY2V1IGEgc2VuaGE/IiBlIGFww7NzIGluZm9ybWHDp8OjbyBkb3MgZGFkb3MgbmVjZXNzw6FyaW9zLCBvIGxlbWJyZXRlIHNlcsOhIGVudmlhZG8gcGFyYSBvIGVtYWlsIGNhZGFzdHJhZG8uDQogICAgPC9saT4NCjwvdWw+DQoNCjxiciAvPg0KPGg1PkVtIGNhc28gZGUgZMO6dmlkYXMgZW50cmUgZW0gY29udGF0byBhdHJhdsOpcyBkbyBmYWxlY29ub3Njb0BzZWZhei5iYS5nb3YuYnIgb3UgMDgwMCAwNzEwMDcxPC9oNT4NCg0KZAINDw8WCB4pX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHidfX0FTTW9kYWxfRXJyb19WaWV3U3RhdGVBU01vZGFsVGl0dWxvSUQFQzxpIGNsYXNzPSdpY29uLWZpeGVkLXdpZHRoIGljb24td2FybmluZy1zaWduIHRleHQtd2FybmluZyc+PC9pPsKgwqAfAQUYIG1vZGFsIGZhZGUgIG1vZGFsIGZhZGUgHwQCAhYKHwUFAi0xHwYFBmRpYWxvZx8HBQR0cnVlHwgFBWZhbHNlHwkFBnN0YXRpY2QCBQ8WAh8ABUhTSVNURU1BIFRSSUJVVCYjMTkzO1JJTyBTRUZBWiAtIFNlY3JldGFyaWEgZGEgRmF6ZW5kYSBkbyBFc3RhZG8gZGEgQmFoaWFkAgcPFgIfAAUOQVNMSUI6IDIuMi4wLjBkZNM6pS58YCjaL19B9QqCERurKOlt',
            '__VIEWSTATEGENERATOR'=>'4D2610BE',
            '__PREVIOUSPAGE'=>'nQpgC3IwW14xm_p9A86Fs1BD2uvvhdVtsNOHtQU-zmLdq1rLBV1QvM0tJTM4A9aoJ1QjtfqVP3XAQStQYGITS4fKvhSrNHXsWrGAU8UpyV6P95SWa98tF0T68ws2wKGonm-XStHtNGNupV56wR6ENjbUun01',
            '__EVENTVALIDATION'=>'/wEdAArzzd+UPA9s2BEZOVMkymAEIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniV7G67MTrWGI73wI34LNPZ59KIHSM=',
            '__ASYNCPOST'=>'false',
            'ctl00$PHCentro$userLogin'=> $this->usuario,
            'ctl00$PHCentro$userPass'=> $this->senha,
            'ctl00$PHCentro$btnLogin'=>'Entrar'
        );

        $url = "https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_destinatario.aspx";

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt( $this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl); 

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($store);
        libxml_use_internal_errors(false);
        $xpath = new DOMXpath($doc);

        $elements = $xpath->query('//input[@id="__VIEWSTATE"]');
        $this->dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
        $this->dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
        $this->dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
    }
    
    public function consultar_emitente(){

        $data = array(
            '__EVENTTARGET'=>'',
            '__EVENTARGUMENT'=>'',
            '__LASTFOCUS'=>'',
            '__VIEWSTATE'=>$this->dados_request_sefaz['VIEWSTATE'],
            '__VIEWSTATEGENERATOR'=>$this->dados_request_sefaz['VIEWSTATEGENERATOR'],
            '__VIEWSTATEENCRYPTED'=>'',
            '__EVENTVALIDATION'=>$this->dados_request_sefaz['EVENTVALIDATION'],
            'filtro'=>'rbt_filtro3',
            'txtPeriodoInicial'=>$this->data_inicial,
            'txtPeriodoFinal'=>$this->data_final,
            'cpf_cnpj'=>'rbt_cnpj',
            'txtCNPJEmitente'=>'',
            'AplicarFiltro'=>'Consultar',
            'CmdOrdenacao'=>'--Selecione--'
        );

        $url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_destinatario.aspx";
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->curl, CURLOPT_COOKIEFILE, $this->cookie_path);
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl);

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($store);
        libxml_use_internal_errors(false);
        $xpath = new DOMXpath($doc);

        $dados_request_sefaz = array();
        $elements = $xpath->query('//input[@id="__VIEWSTATE"]');
        $this->dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
        $this->dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
        $this->dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;

        return $dados_request_sefaz;

    }
    
    public function obter_notas_planilha(){
	    $this->consultar_emitente();
        $data = array(
            '__EVENTTARGET'=>'',
            '__EVENTARGUMENT'=>'',
            '__LASTFOCUS'=>'',
            '__VIEWSTATE'=> $this->dados_request_sefaz['VIEWSTATE'],
            '__VIEWSTATEGENERATOR'=> $this->dados_request_sefaz['VIEWSTATEGENERATOR'],
            '__VIEWSTATEENCRYPTED'=>'',
            '__EVENTVALIDATION'=> $this->dados_request_sefaz['EVENTVALIDATION'],
            'filtro'=>'rbt_filtro3',
            'txtPeriodoInicial'=> $this->data_inicial,
            'txtPeriodoFinal'=> $this->data_final,
            'cpf_cnpj'=>'rbt_cnpj',
            'txtCNPJDestinatario'=>'',
            'btn_GerarPlanilha'=>'Gerar Planilha',
            'CmdOrdenacao'=>'--Selecione--'
        );

        $url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_destinatario.aspx";
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_HEADER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl);
        $array_notas = explode("\n", $store);
        array_shift($array_notas);

        $notas_fiscais = array();
        
        foreach ($array_notas as $array){
            $nota = explode(';', $array);

            if (!isset($nota[5]))
                continue;
            $chave = $this->apenas_numero(trim($nota[5]));
            if ( strlen($chave) < 44)
                continue;
            $emissao = date('Y-m-d', strtotime(trim($nota[3])));
            $situacao = trim($nota[7]);
            $cod_situacao_nota_fisc_eletr = '';
            if($situacao == 'Autorizada')
                $cod_situacao_nota_fisc_eletr = 1;
            elseif($situacao == 'Cancelada')
                $cod_situacao_nota_fisc_eletr = 2;

            $notas_fiscais[$nota[5]] = array(
                'link_nota_detalhada' => 'https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Geral/NFENC_consulta_chave_acesso.aspx?cod_chave_acesso='.$chave.'&dtc_emissao='.$emissao.'&cod_situacao_nota_fisc_eletr='.$cod_situacao_nota_fisc_eletr,
                'doc_sefaz'=> trim($nota[0]),
                'CNPJ/CPF'=> trim($nota[1]),
                'razao_social_dest'=> $nota[2],
                'emissao'=> trim($nota[3]),
                'valor'=> str_replace(",", ".", str_replace(".", "", $nota[4])),
                'chave_acesso'=> $chave,
                'situacao'=> $situacao,
                'tipo_operacao'=> trim($nota[8]),
                'uf'=> trim($nota[6]),
                'mdf_evento'=> '',
                'mdf_data'=> '',
                'user_report'=> $this->user_report,
                'data_report'=> $this->data_report,
                'cidade_emitente'=> '',
                'cidade_destinatario'=> '',
                'modalidade_frete' => '',
                'status' => 'not_found_csv',
            );
        }
        
        return $notas_fiscais;
    }

    public function apenas_numero($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }

    public function consulta_mdfe($link){
	    $nota_detalhada = $this->obter_nota_detalhada($link);
	    $informacao_mdfe = $this->get_informacao_mdfe($nota_detalhada);

	    return $informacao_mdfe;
	}

    public function obter_nota_detalhada($link){

        $link_nota_consulta = str_replace("../..", "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos", $link);

        $url = $link_nota_consulta;
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_HEADER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($this->curl);

        $url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/geral/NFENC_consulta_chave_acesso_detalhe.aspx";
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_HEADER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl);

       return $store;
    }

    public function get_informacao_mdfe($nota_detalhada){
        $html = new Simple_html_dom();
        $html = $html->load($nota_detalhada);

//        DETALHE EVENTOS

        $tableEventos = $html->find('table[class=tabNFe]', 0);

        $dados_nota = array();
        $dados_nota['mdf_data'] = '';
        $dados_nota['mdf_evento'] = '';
        if($tableEventos){
            $linhas = $tableEventos->find('tr');
            array_shift($linhas);
            foreach ($linhas as $linha){
                $value = $linha->find('td', 0)->plaintext;
                if( strpos( $value, 'MDF-e Autorizado' ) !== false ){
                    $mdf_data = $linha->find('td', 2)->plaintext;
                    $dados_nota['mdf_data'] = $mdf_data;
                    $dados_nota['mdf_evento'] = $value;
                    break;
                }
            }
        }
//      CHAVE DE ACESSO
        $chave = $html->find('span[id=lblChaveAcesso]', 0)->plaintext;
        $dados_nota['chave_acesso'] = $chave;

//        DETALHE TRANSPORTE

        $divTranporte = $html->find('div[id=Transporte]', 0)->find('table', 0);
        $dados_nota['modalidade_frete'] = trim($divTranporte->find('p[class=linha]', 0)->plaintext);

//        DETALHE DESTINATARIO

        $divDestinatario = $html->find('div[id=Destinatario_Remetente]', 0);
        $table = $divDestinatario->find('table', 0);
        $dados_nota['cidade_destinatario'] = trim($table->find('tr', 3)->find('td', 0)->find('p', 0)->plaintext);

//        DETALHE EMITENTE
        $divEmitente = $html->find('div[id=tb_emitente]', 0);
        $table = $divEmitente->find('table', 0);
        $dados_nota['cidade_emitente'] = trim($table->find('tr', 3)->find('td', 0)->find('p', 0)->plaintext);
        $dados_nota['uf'] = trim($table->find('tr', 4)->find('td', 0)->find('p', 0)->plaintext);

        return $dados_nota;
    }

	public function converterCaracterEspecial($text){
		return html_entity_decode($text, ENT_QUOTES, "utf-8");
	}

	public function encerrar_conection(){
		curl_close( $this->curl );
	}

	function __destruct()
	{
		$this->encerrar_conection();
	}
}
