<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/Simple_html_dom.php');

class Nfe_emitente_library {

	/**
	 * LOGIN URL
	 *
	 * URL que faz o login 
	 *
	 * @var	string
	 */
	protected $login_url		= 'https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_emitente.aspx';

    /**
     * USUARIO
     *
     * USUARIO LOGIN
     *
     * @var	string
     */
    protected $usuario		= '';

    /**
     * SENHA
     *
     * SENHA LOGIN
     *
     * @var	string
     */
    protected $senha		= '';

    /**
     * COOKIE_PATH
     *
     * Caminho do cookie de sessão
     *
     * @var	string
     */
    protected $cookie_path		= '';

    /**
     * DADOS REQUEST SEFAZ
     *
     * Dados request sefaz
     *
     * @var	string
     */
    protected $dados_request_sefaz		= array();

    /**
     * DATA INICIAL
     *
     * Data inicial do filtro
     *
     * @var	string
     */
    protected $data_inicial		= '';

    /**
     * DATA FINAL
     *
     * Data final do filtro
     *
     * @var	string
     */
    protected $data_final		= '';

    /**
     * USER REPORT
     *
     * usuario relatorio
     *
     * @var	string
     */
    protected $user_report		= '';
    /**
     * DATA REPORT
     *
     * data relatorio
     *
     * @var	string
     */
    protected $data_report		= '';

    /**
     * html
     *
     * html carregado
     *
     * @var	string
     */

    private $html = '';

    /**
	 * CI Singleton
	 *
	 * @var	object
	 */
	protected $CI;

	private $curl;

	public function __construct($params = array())
	{
		$this->CI =& get_instance();
		$this->curl = curl_init();
		$this->initialize($params);
		$this->login();
        $this->html = new Simple_html_dom();

    }

	public function initialize(array $params = array())
	{
		foreach ($params as $key => $val)
		{
			if (property_exists($this, $key))
			{
				$this->$key = $val;
			}
		}

		return $this;
	}

    public function login(){
        curl_setopt($this->curl, CURLOPT_URL, 'https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_emitente.aspx');
        curl_setopt( $this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_COOKIEFILE, $this->cookie_path);
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, 1);
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl);

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($store);
        libxml_use_internal_errors(false);
        $xpath = new DOMXpath($doc);
        $elements = $xpath->query('//input[@id="__VIEWSTATE"]');
        $__VIEWSTATE = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
        $__VIEWSTATEGENERATOR = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
        $__EVENTVALIDATION = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__PREVIOUSPAGE"]');
        $__PREVIOUSPAGE = $elements[0]->attributes[3]->value;
        $data = array(
            'ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin',
            '__EVENTTARGET'=>'',
            '__EVENTARGUMENT'=>'',
            '__VIEWSTATE'=> $__VIEWSTATE,
            '__VIEWSTATEGENERATOR'=> $__VIEWSTATEGENERATOR,
            '__PREVIOUSPAGE'=> $__PREVIOUSPAGE,
            '__EVENTVALIDATION'=> $__EVENTVALIDATION,
            '__ASYNCPOST'=>'false',
            'ctl00$PHCentro$userLogin'=> $this->usuario,
            'ctl00$PHCentro$userPass'=> $this->senha,
            'ctl00$PHCentro$btnLogin'=>'Entrar'
        );

        $url = "https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_emitente.aspx";

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt( $this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl); 

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($store);
        libxml_use_internal_errors(false);
        $xpath = new DOMXpath($doc);

        $elements = $xpath->query('//input[@id="__VIEWSTATE"]');
        $this->dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
        $this->dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
        $this->dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;
    }
    
    public function consultar_emitente(){

        $data = array(
            '__EVENTTARGET'=>'',
            '__EVENTARGUMENT'=>'',
            '__LASTFOCUS'=>'',
            '__VIEWSTATE'=>$this->dados_request_sefaz['VIEWSTATE'],
            '__VIEWSTATEGENERATOR'=>$this->dados_request_sefaz['VIEWSTATEGENERATOR'],
            '__VIEWSTATEENCRYPTED'=>'',
            '__EVENTVALIDATION'=>$this->dados_request_sefaz['EVENTVALIDATION'],
            'filtro'=>'rbt_filtro3',
            'txtPeriodoInicial'=>$this->data_inicial,
            'txtPeriodoFinal'=>$this->data_final,
            'cpf_cnpj'=>'rbt_cnpj',
            'txtCNPJDestinatario'=>'',
            'AplicarFiltro'=>'Consultar',
            'CmdOrdenacao'=>'--Selecione--'
        );

        $url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_emitente.aspx";
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->curl, CURLOPT_COOKIEFILE, $this->cookie_path);
        curl_setopt($this->curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl);

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($store);
        libxml_use_internal_errors(false);
        $xpath = new DOMXpath($doc);

        $dados_request_sefaz = array();
        $elements = $xpath->query('//input[@id="__VIEWSTATE"]');
        $this->dados_request_sefaz['VIEWSTATE'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__VIEWSTATEGENERATOR"]');
        $this->dados_request_sefaz['VIEWSTATEGENERATOR'] = $elements[0]->attributes[3]->value;
        $elements = $xpath->query('//input[@id="__EVENTVALIDATION"]');
        $this->dados_request_sefaz['EVENTVALIDATION'] = $elements[0]->attributes[3]->value;

        return $dados_request_sefaz;

    }
    
    public function obter_notas_planilha(){
	    $this->consultar_emitente();
        $data = array(
            '__EVENTTARGET'=>'',
            '__EVENTARGUMENT'=>'',
            '__LASTFOCUS'=>'',
            '__VIEWSTATE'=> $this->dados_request_sefaz['VIEWSTATE'],
            '__VIEWSTATEGENERATOR'=> $this->dados_request_sefaz['VIEWSTATEGENERATOR'],
            '__VIEWSTATEENCRYPTED'=>'',
            '__EVENTVALIDATION'=> $this->dados_request_sefaz['EVENTVALIDATION'],
            'filtro'=>'rbt_filtro3',
            'txtPeriodoInicial'=> $this->data_inicial,
            'txtPeriodoFinal'=> $this->data_final,
            'cpf_cnpj'=>'rbt_cnpj',
            'txtCNPJDestinatario'=>'',
            'btn_GerarPlanilha'=>'Gerar Planilha',
            'CmdOrdenacao'=>'--Selecione--'
        );

        $url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_emitente.aspx";
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_HEADER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl);
        $array_notas = explode("\n", $store);
        array_shift($array_notas);

        $notas_fiscais = array();
        
        foreach ($array_notas as $array){
            $nota = explode(';', $array);

            if (!isset($nota[5]))
                continue;
            $chave = $this->apenas_numero(trim($nota[5]));
            if ( strlen($chave) < 44)
                continue;
            $emissao = date('Y-m-d', strtotime(trim($nota[3])));
            $situacao = trim($nota[7]);
            $cod_situacao_nota_fisc_eletr = '';
            if($situacao == 'Autorizada')
                $cod_situacao_nota_fisc_eletr = 1;
            elseif($situacao == 'Cancelada')
                $cod_situacao_nota_fisc_eletr = 2;

            $notas_fiscais[$nota[5]] = array(
                'link_nota_detalhada' => 'https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Geral/NFENC_consulta_chave_acesso.aspx?cod_chave_acesso='.$chave.'&dtc_emissao='.$emissao.'&cod_situacao_nota_fisc_eletr='.$cod_situacao_nota_fisc_eletr,
                'doc_sefaz'=> trim($nota[0]),
                'CNPJ/CPF'=> trim($nota[1]),
                'razao_social_dest'=> $nota[2],
                'emissao'=> trim($nota[3]),
                'valor'=> str_replace(",", ".", str_replace(".", "", $nota[4])),
                'chave_acesso'=> $chave,
                'situacao'=> $situacao,
                'tipo_operacao'=> trim($nota[8]),
                'uf'=> trim($nota[6]),
                'user_report'=> $this->user_report,
                'data_report'=> $this->data_report,
                'cidade_emitente'=> '',
                'cidade_destinatario'=> '',
                'status' => 'not_found_csv',
            );
        }
        
        return $notas_fiscais;
    }

    public function apenas_numero($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }


    public function obter_nota_detalhada($link){

        $link_nota_consulta = str_replace("../..", "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos", $link);

        $url = $link_nota_consulta;
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_HEADER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($this->curl);

        $url = "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/geral/NFENC_consulta_chave_acesso_detalhe.aspx";
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_ENCODING, "UTF-8" );
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_path);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($this->curl, CURLOPT_HEADER,false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $store = curl_exec($this->curl);

       return $store;
    }

	public function converterCaracterEspecial($text){
		return html_entity_decode($text, ENT_QUOTES, "utf-8");
	}

	public function encerrar_conection(){
		curl_close( $this->curl );
	}

	function __destruct()
	{
		$this->encerrar_conection();
	}
}
