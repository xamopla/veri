<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/Certificate/Pkcs12.php');

class Dec_library {
    /**
     * PRIVATE_KEY
     *
     * Path para a chave privada do certificado
     *
     * @var string
     */
    protected $private_key      = '';

    /**
     * PUBLIC_KEY
     *
     * Path para chave publica do certificado
     *
     * @var string
     */
    protected $public_key       = '';

    /**
     * CERT_KEY
     *
     * Path para o arquivo cert key
     *
     * @var string
     */
    protected $cert_key     = '';

    /**
     * CERTIFICADO_SENHA
     *
     * Senha do certificado digital
     *
     * @var string
     */
    protected $cerficado_senha      = '';

    /**
     * caminho_certificado
     *
     * caminho do certificado digital
     *
     * @var string
     */
    protected $caminho_certificado      = '';

    /**
     * CAMINHO_DA_PASTA_PDFS
     *
     * Indica o caminho para a pasta que salva os pdfs
     *
     * @var string
     */
    protected $caminho_da_pasta_pdfs        = '';
    /**
     * NUMERO DOCUMENTO
     * @var string
     */
    protected $numero_documento        = '';


    /**
     * CI Singleton
     *
     * @var object
     */
    protected $CI;

    private $curl;

    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        $this->curl = curl_init();
        $this->initialize($params);
        try {
            $this->gerar_chaves();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        log_message('info', 'Ecac Robo Class Initialized');
    }

    public function initialize(array $params = array())
    {
        foreach ($params as $key => $val)
        {
            if (property_exists($this, $key))
            {
                $this->$key = $val;
            }
        }

        return $this;
    }

    function get_numero_documento(){
        return $this->numero_documento;
    }
    function set_numero_documento( $numero_documento ){
        $this->numero_documento = $numero_documento;
    }

    /**
     * gerar_chaves
     *
     * Gera as chaves de acesso do certificado informado
     *
     */

    function gerar_chaves(){
        $pkcs = new Pkcs12(APPPATH . 'libraries/Certificate/certificados_clientes/');

        $pkcs->loadPfxFile($this->caminho_certificado, $this->cerficado_senha);

//      seta as chaves na classe
        $this->public_key = $pkcs->pubKeyFile;
        $this->private_key = $pkcs->priKeyFile;
        $this->cert_key = $pkcs->certKeyFile;
        $this->numero_documento = $pkcs->cnpj;
        return true;
    }

    function get_qtd_mensagem(){
        $ch = curl_init();
        $post = array(
            'cert_key' => $this->cert_key,
        );
        curl_setopt($ch, CURLOPT_URL, "http://0.0.0.0:5000/dec-qtd-mensagem");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    public function get_caixa_postal($cnpj=''){
        $ch = curl_init();
        $post = array(
            'cert_key' => $this->cert_key,
            'cnpj' => $cnpj != '' ? $cnpj : $this->numero_documento
        );
        curl_setopt($ch, CURLOPT_URL, "http://localhost:5000/dec-caixapostal");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    function ler_mensagem($cnpj, $identificacao_buscar){
        $ch = curl_init();
        $post = array(
            'cert_key' => $this->cert_key,
            'cnpj' => $cnpj,
            'folder_pdf' => $this->caminho_da_pasta_pdfs,
            'identificacao_buscar' => $identificacao_buscar
        );

        curl_setopt($ch, CURLOPT_URL, "http://0.0.0.0:5000/dec-ler-mensagem");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    function ler_comunicado($cnpj, $assunto_buscar, $complemento_buscar, $data_envio_buscar){
        $ch = curl_init();
        $post = array(
            'cert_key' => $this->cert_key,
            'cnpj' => $cnpj,
            'folder_pdf' => $this->caminho_da_pasta_pdfs,
            'assunto_buscar' => $assunto_buscar,
            'complemento_buscar' => $complemento_buscar,
            'data_envio_buscar' => $data_envio_buscar,
        );

        curl_setopt($ch, CURLOPT_URL, "http://0.0.0.0:5000/dec-ler-comunicado");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    function encerrar_conection(){
        curl_close($this->curl);
    }
    function __destruct()
    {
        $this->encerrar_conection();
    }
}
