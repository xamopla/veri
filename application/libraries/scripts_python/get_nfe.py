import sys
import requests
import warnings
from lxml import html
warnings.filterwarnings('ignore')

login = sys.argv[1]
senha = sys.argv[2]
chave_nfe = sys.argv[3]
with requests.Session() as session:
	request = session.get('https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login', verify = False)        
	byte_data = request.content 
	source_code = html.fromstring(byte_data)
	data_login = {'ctl00$ScriptManager2':'ctl00$updPanel|ctl00$PHCentro$btnLogin',
				'__EVENTTARGET':'',
				'__EVENTARGUMENT':'',
				'__VIEWSTATE':source_code.xpath('//*[@id="__VIEWSTATE"]/@value')[0],
				'__VIEWSTATEGENERATOR':source_code.xpath('//*[@id="__VIEWSTATEGENERATOR"]/@value')[0],
				'__PREVIOUSPAGE':source_code.xpath('//*[@id="__PREVIOUSPAGE"]/@value')[0],
				'__EVENTVALIDATION':source_code.xpath('//*[@id="__EVENTVALIDATION"]/@value')[0],
				'__ASYNCPOST':'false',
				'ctl00$PHCentro$userLogin':login,
				'ctl00$PHCentro$userPass': senha,
				'ctl00$PHCentro$btnLogin':'Entrar'}
	request = session.post('https://nfe.sefaz.ba.gov.br/servicos/NFENC/SSL/ASLibrary/Login?ReturnUrl=%2fservicos%2fnfenc%2fModulos%2fAutenticado%2fRestrito%2fNFENC_consulta_destinatario.aspx', data = data_login, verify = False)        
	byte_data = request.content
	source_code = html.fromstring(byte_data)


	data_consulta = {
			'__EVENTTARGET':'',
			'__EVENTARGUMENT':'',
			'__LASTFOCUS':'',
			'__VIEWSTATE':source_code.xpath('//*[@id="__VIEWSTATE"]/@value')[0],
			'__VIEWSTATEGENERATOR':source_code.xpath('//*[@id="__VIEWSTATEGENERATOR"]/@value')[0],
			'__VIEWSTATEENCRYPTED':'',
			'__EVENTVALIDATION':source_code.xpath('//*[@id="__EVENTVALIDATION"]/@value')[0],
			'filtro':'rbt_filtro1',
			'txtChaveAcesso': chave_nfe,
			'cpf_cnpj':'rbt_cnpj',
			'AplicarFiltro':'Consultar',
			'CmdOrdenacao':'--Selecione--'
	}

	request = session.post('https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/Autenticado/Restrito/NFENC_consulta_destinatario.aspx', data = data_consulta, verify = False)        
	byte_data = request.text
	source_code = html.fromstring(byte_data)
	
	url_nota = source_code.xpath('//a')[0].get('href').replace("../..", "https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos")
	request = session.get(url_nota, verify = False) 
	request = session.get('https://nfe.sefaz.ba.gov.br/servicos/nfenc/Modulos/geral/NFENC_consulta_chave_acesso_detalhe.aspx', verify = False)       
	print(request.text)


