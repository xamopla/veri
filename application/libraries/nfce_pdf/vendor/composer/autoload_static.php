<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfd8677394d5aea695ff87632e56a5004
{
    public static $files = array (
        '5255c38a0faeba867671b61dfda6d864' => __DIR__ . '/..' . '/paragonie/random_compat/lib/random.php',
        '72579e7bd17821bb1321b87411366eae' => __DIR__ . '/..' . '/illuminate/support/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'I' => 
        array (
            'Illuminate\\Support\\' => 19,
            'Illuminate\\Contracts\\' => 21,
            'Illuminate\\Config\\' => 18,
        ),
        'G' => 
        array (
            'Gufy\\PdfToHtml\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Illuminate\\Support\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/support',
        ),
        'Illuminate\\Contracts\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/contracts',
        ),
        'Illuminate\\Config\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/config',
        ),
        'Gufy\\PdfToHtml\\' => 
        array (
            0 => __DIR__ . '/..' . '/gufy/pdftohtml-php/src',
        ),
    );

    public static $prefixesPsr0 = array (
        's' => 
        array (
            'stringEncode' => 
            array (
                0 => __DIR__ . '/..' . '/paquettg/string-encode/src',
            ),
        ),
        'P' => 
        array (
            'PHPHtmlParser' => 
            array (
                0 => __DIR__ . '/..' . '/paquettg/php-html-parser/src',
            ),
        ),
        'G' => 
        array (
            'Gufy' => 
            array (
                0 => __DIR__ . '/..' . '/gufy/pdftohtml-php/src',
            ),
        ),
        'D' => 
        array (
            'Doctrine\\Common\\Inflector\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/inflector/lib',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitfd8677394d5aea695ff87632e56a5004::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitfd8677394d5aea695ff87632e56a5004::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitfd8677394d5aea695ff87632e56a5004::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
