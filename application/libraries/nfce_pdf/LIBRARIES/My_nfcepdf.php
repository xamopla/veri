﻿<?php
	header("Content-Type: application/json; charset=utf-8");
	ini_set('memory_limit', '-1');
	error_reporting(E_ERROR | E_PARSE);

	include 'nfce_pdf/vendor/autoload.php';
	use Gufy\PdfToHtml\Config;
	#Defenir os caminhos aqui
	\Gufy\PdfToHtml\Config::set('pdftohtml.bin', '/usr/local/bin/pdftohtml');
	\Gufy\PdfToHtml\Config::set('pdfinfo.bin', '/usr/local/bin/pdfinfo');

class My_nfcepdf{

	public function teste($file){

		$ext = strtolower(substr($file['name'],-4)); //Pegando extensão do arquivo
  		$new_name = date("Y.m.d-H.i.s") . $ext;
		$dir = '/var/www/html/application/libraries/nfce_pdf/pdf_temp';
		#$dir = '/var/www/html/uploads/anexos';
		move_uploaded_file($file['tmp_name'], $dir.$new_name);

		$pdf = new Gufy\PdfToHtml\Pdf($dir.$new_name);
		$total_pages = $pdf->getPages();		
		
		$dados = array();

		#Laço para percorrer todos as páginas do PDF
		for($page=1; $page<=$total_pages; $page++){

			#PDF -> HTML
			$html = $pdf->html($page);
			$html = str_replace('<body', '<!-- ', $html);
			$html = str_replace('image">', '--> ', $html);

			#STRING para HTML->DOM
			$doc = new DOMDocument();
			libxml_use_internal_errors(true);
			$doc->loadHTML(utf8_decode($html));
			libxml_use_internal_errors(false);
			$xpath = new DOMXPath($doc);

			#Get apenas os campos com dados
			$dados_tabela = $xpath->query( "//p[@class='ft01']");
			$linhas	= ($dados_tabela->length)/6;

			$indice = 0;
			for($linha=0; $linha<$linhas; $linha++){
				array_push($dados,array(
					"chave"=> $dados_tabela[$indice]->nodeValue,
					"emissao" => $dados_tabela[$indice+1]->nodeValue,
					"nfce"=> $dados_tabela[$indice+2]->nodeValue,
					"serie" => $dados_tabela[$indice+3]->nodeValue,
					"situacao" => $dados_tabela[$indice+4]->nodeValue,
					"valor" => number_format($this->get_valor($dados_tabela[$indice+5]->nodeValue),2)
				));
				$indice +=6;
			}

			if($page == 1){
				$dados_tabela2 = $xpath->query( "//p[@class='ft03']");
				$indice_2 = 0;
				$razao_social = $dados_tabela2[$indice_2+1]->nodeValue;
			}
		}

		#Aqui já tenho o array  todo populado com as informações do PDF
		$autorizadas = 0;
		$canceladas = 0;
		$valor_autorizadas = 0;
		$valor_canceladas = 0;
		foreach ($dados as $nfce) {
			if($nfce['situacao'] == 'Autorizada'){
				$autorizadas++;
				$valor_autorizadas += $nfce['valor'];
			}
			if($nfce['situacao'] == 'Cancelada'){
				$canceladas++;
				$valor_canceladas += $nfce['valor'];
			}
		}

		$tabela = '<div hidden id="dv"><table id="nfce-table"><thead><tr><th>Chave de Acesso</th><th>Dt Emissão</th><th>Nº NFC-e</th><th>Série</th><th>Situação</th><th>Valor (R$)</th></tr></thead>';
		foreach ($dados as $nfce) {
			$tabela .= '<tr>';
			$tabela .= "<td>'".$nfce['chave']."</td>";
			$tabela .= '<td>'.$nfce['emissao'].'</td>';
			$tabela .= '<td>'.$nfce['nfce'].'</td>';
			$tabela .= '<td>'.$nfce['serie'].'</td>';
			$tabela .= '<td>'.$nfce['situacao'].'</td>';
			$tabela .= '<td>'.$nfce['valor'].'</td>';
			$tabela .= '</tr>';
		}
		$tabela .= '</table></div>';

		$dados_gerais = array(
			'autorizadas' => $autorizadas,
			'valor_autorizadas' => round($valor_autorizadas,2),
			'canceladas' => $canceladas,
			'valor_canceladas' => round($valor_canceladas,2),
			'tabela' => $tabela,
			'razao_social' => $razao_social
		);
		unlink($dir.$new_name);
		return ($dados_gerais);
	}
	
	public function get_valor($valor){		
		return floatval(str_replace(',', '.',$valor));
	}

}