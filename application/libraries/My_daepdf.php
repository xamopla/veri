<?php
	header("Content-Type: application/json; charset=utf-8");
	date_default_timezone_set('America/Sao_Paulo');
	ini_set('memory_limit', '-1');
	//error_reporting(E_ERROR | E_PARSE);
	
	include 'nfce_pdf/vendor/autoload.php';
	use Gufy\PdfToHtml\Config;
	\Gufy\PdfToHtml\Config::set('pdftohtml.bin', 'C:/xampp/htdocs/veri_4.0/application/libraries/nfce_pdf/poppler-0.67.0/bin/pdftohtml.exe');
	\Gufy\PdfToHtml\Config::set('pdfinfo.bin', 'C:/xampp/htdocs/veri_4.0/application/libraries/nfce_pdf/poppler-0.67.0/bin/pdfinfo.exe');

class My_daepdf{

	public function read($file){

		$pdfToHtml = 'C:/xampp/htdocs/veri_4.0/application/libraries/nfce_pdf/poppler-0.67.0/bin/pdftohtml.exe';

		$ext = strtolower(substr($file['name'],-4)); //Pegando extensão do arquivo
  		$new_name = date("YmdHis");
		$dir = "C:/xampp/htdocs/veri_4.0/assets/dae_pdf/pdfs/";
		move_uploaded_file($file['tmp_name'], $dir.$new_name.$ext);	

		$pdf = new Gufy\PdfToHtml\Pdf($dir.$new_name.$ext);
		$html = $pdf->html();
		$html = str_replace('<body', '<!-- ', $html);
		$html = str_replace('image">', '--> ', $html);
	
		#STRING para HTML->DOM
		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML(utf8_decode($html));
		libxml_use_internal_errors(false);
		$xpath = new DOMXPath($doc);

		#GET APENAS OS CAMPOS COM DADOS
		$dados_tabela = $xpath->query( "//p");
		$linhas	= ($dados_tabela->length);

		#VERIFICANDO AS LINHAS DE NOTA
		$notas = "";
		$linha_nota = str_replace("//", '', $dados_tabela[76]->nodeValue);
		$linha_nota_numerica = str_replace("%C2%A0%C2%A0", '', urlencode($linha_nota));
		if(is_numeric(trim($linha_nota_numerica))){
			$notas .= str_replace("%C2%A0%C2%A0", ';', urlencode($linha_nota)).";";
		}
		$linha_nota = str_replace("//", '', $dados_tabela[78]->nodeValue);
		$linha_nota_numerica = str_replace("%C2%A0%C2%A0", '', urlencode($linha_nota));
		if(is_numeric(trim($linha_nota_numerica))){
			$notas .= str_replace("%C2%A0%C2%A0", ';', urlencode($linha_nota)).";";
		}
		$linha_nota = str_replace("//", '', $dados_tabela[80]->nodeValue);
		$linha_nota_numerica = str_replace("%C2%A0%C2%A0", '', urlencode($linha_nota));
		if(is_numeric(trim($linha_nota_numerica))){
			$notas .= str_replace("%C2%A0%C2%A0", ';', urlencode($linha_nota)).";";
		}

		$dados_dae = array();
		$dados_dae = array(
			'codigo_receita'=>$dados_tabela[21]->nodeValue,
			'vencimento'=>$dados_tabela[24]->nodeValue,
			'data_inclusao'=>date('d/m/Y'),
			'ie'=>$dados_tabela[27]->nodeValue,
			'referencia'=>$dados_tabela[36]->nodeValue,
			'num_dae'=>$dados_tabela[30]->nodeValue,
			'cnpj'=>$dados_tabela[39]->nodeValue,
			'razao_social'=>$dados_tabela[42]->nodeValue,
			'valor'=>$dados_tabela[62]->nodeValue,
			'notas'=>$notas
		);
		return $dados_dae;
	}

}