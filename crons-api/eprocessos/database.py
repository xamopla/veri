import mysql.connector

class DataBase:

    def __init__(self, database):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="12*bDV3r1sA0pAuL0*21",
            database=database
        )

    def get_certs(self):
        mycursor = self.mydb.cursor()
        mycursor.execute("""
            select 
                dtb_certificado_contador.id_contador,
                dtb_certificado_contador.caminho_arq,
                dtb_certificado_contador.pass
            from dtb_certificado_contador
        """)
        myresult = mycursor.fetchall()
        return myresult
    

    def get_empresas_by_contador_id(self, id_contador):
        mycursor = self.mydb.cursor()
        mycursor.execute(f"""
            select
                dtb_empresas.id as id_empresa,
                dtb_empresas.cnpj as cnpj
                from dtb_contador_procuracao
                    inner join dtb_empresas
                        on dtb_empresas.id = dtb_contador_procuracao.id_empresa
                where dtb_empresas.uf = 'SP'
                    and dtb_contador_procuracao.id_contador = '{id_contador}'
        """)
        myresult = mycursor.fetchall()
        return myresult

    
    def insert_processo_ativo(self, processo):

        mycursor = self.mydb.cursor()
        sql = f"SELECT * FROM dtb_ecac_eprocessos_ativos WHERE idProcesso = '{processo['idProcesso']}'"
        mycursor.execute(sql)
        if len(mycursor.fetchall()) > 0:
            return 'Processo já existe!'

        fields = ', '.join(processo.keys())
        values = []
        for key in processo.keys():
            values.append(processo[key])
        values = tuple(values)

        sql = f"INSERT INTO dtb_ecac_eprocessos_ativos ({fields}) VALUES {str(values)}"
        mycursor.execute(sql)
        self.mydb.commit()


    def insert_historico_ativo(self, historico):

        if self.historico_exist(historico) > 0:
            return 'Histórico já existe!'

        fields = ', '.join(historico.keys())

        values = []
        for key in historico.keys():
            values.append(historico[key])
        values = tuple(values)

        mycursor = self.mydb.cursor()
        sql = f"INSERT INTO dtb_ecac_eprocessos_ativos_historico ({fields}) VALUES {str(values)}"
        mycursor.execute(sql)
        self.mydb.commit()


    def insert_processo_inativo(self, processo):

        mycursor = self.mydb.cursor()
        sql = f"SELECT * FROM dtb_ecac_eprocessos_inativos WHERE idProcesso = '{processo['idProcesso']}'"
        mycursor.execute(sql)
        if len(mycursor.fetchall()) > 0:
            return 'Processo já existe!'

        fields = ', '.join(processo.keys())

        values = []
        for key in processo.keys():
            values.append(processo[key])
        values = tuple(values)

        sql = f"INSERT INTO dtb_ecac_eprocessos_inativos ({fields}) VALUES {str(values)}"
        mycursor.execute(sql)
        self.mydb.commit()


    def insert_historico_inativo(self, historico):
        
        if self.historico_exist(historico) > 0:
            return 'Histórico já existe!'
        
        fields = ', '.join(historico.keys())

        values = []
        for key in historico.keys():
            values.append(historico[key])
        values = tuple(values)

        mycursor = self.mydb.cursor()
        sql = f"INSERT INTO dtb_ecac_eprocessos_inativos_historico ({fields}) VALUES {str(values)}"
        mycursor.execute(sql)
        self.mydb.commit()

    
    def historico_exist(self, historico):
        
        filters = []
        for key in historico.keys():
            filters.append(f"{key} = '{historico[key]}'")

        mycursor = self.mydb.cursor()
        sql = 'SELECT * FROM dtb_ecac_eprocessos_inativos_historico WHERE '+(' AND '.join(filters))
        mycursor.execute(sql)

        return len(mycursor.fetchall())
