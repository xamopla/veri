from ecac import Ecac
from database import DataBase
import sys

database = DataBase(sys.argv[1])

certificados = database.get_certs()

for certificado in certificados:

    myEcac = Ecac(
        binary_path='/var/www/html/crons-api/eprocessos/geckodriver',
        certificate_path='/var/www/html/crons-api/'+certificado[1],
        certificate_psw=certificado[2],
        database=sys.argv[1]
    )
    myEcac.login()

    for empresa in database.get_empresas_by_contador_id(certificado[0]):
        if myEcac.alterar_perfil(empresa[1]):
            myEcac.acessar_processos_digitais()
            myEcac.get_processos(1)
            myEcac.get_processos(2)

    myEcac.quit()