import tempfile
import shutil
import os
import subprocess

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from database import DataBase
import datetime

import time

class Ecac:

    def __init__(self, binary_path, certificate_path, certificate_psw, database):        
        
        self.binary_path = binary_path
        self.certificate_path = certificate_path
        self.certificate_psw = certificate_psw        
        self.mydb = DataBase(database)
        self.cnpj = None       

        # Iniciando os arquivos de Profile do Firefox
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--lang=pt-BR")
        profile = webdriver.FirefoxProfile()
        print(profile)
        profile.set_preference("security.default_personal_cert", "Select Automatically")
        capabilities = webdriver.DesiredCapabilities().FIREFOX
        capabilities["marionette"] = True
        self.driver = webdriver.Firefox(options=options, executable_path=self.binary_path, firefox_profile=profile, capabilities=capabilities,
            service_args=["--marionette-port", "2828"])
        
        self.profile_path = self.driver.firefox_profile.path
        self.print_log('Profile-path: '+self.profile_path)        
        
        self.driver.close()
        
        # Criando os arquivos de banco de certificado
        self.generate_certificate()

    
    def login(self):        
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--lang=pt-BR")
        profile = webdriver.FirefoxProfile(self.profile_path)
        profile.set_preference("security.default_personal_cert", "Select Automatically")
        capabilities = webdriver.DesiredCapabilities().FIREFOX
        capabilities["marionette"] = True
        self.print_log('Abrindo sessão de login')
        self.driver = webdriver.Firefox(options=options, executable_path=self.binary_path, firefox_profile=profile, capabilities=capabilities,
            service_args=["--marionette-port", "2828"])
        self.driver.get("https://cav.receita.fazenda.gov.br/autenticacao/login")
        
        # Login com GovBr
        self.print_log('Realizando login com o GovBr')
        btn_login_govbr = self.driver.find_element_by_xpath('//*[@id="login-dados-certificado"]/p[2]/a')
        self.driver.get(btn_login_govbr.get_attribute('href'))

        # Login com Certificado
        self.print_log('Realizando login com o certificado')
        self.driver.find_element_by_xpath('/html/body/div[1]/main/form/div[1]/div[4]/a').click()

    def send_another_page(self):
        self.driver.get('https://google.com.br')

    def verifyMailbox(self):
        try:
            btn = self.driver.find_elements_by_xpath("//*[contains(text(), 'Ir para a Caixa Postal')]")
            if btn is not None and len(btn) > 0:
                btn[0].click()
            self.print_log('Usuário possui Caixa Postal pendente')
        finally:
            self.print_log('Verificação de Caixa Postal finalizada')            

    def alterar_perfil(self, cnpj):
        self.print_log(f'Alterando perfil para o cnpj {cnpj}')
        url = 'https://cav.receita.fazenda.gov.br/ecac/'
        if(url not in self.driver.current_url):
            self.driver.get(url)

        self.driver.find_element_by_xpath('//*[@id="btnPerfil"]').click()        
        input_cnpj = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="txtNIPapel2"]'))
        )
        input_cnpj.clear()
        input_cnpj.send_keys(cnpj)
        self.driver.find_element_by_xpath('//*[@id="formPJ"]/input[4]').click()

        time.sleep(3)
        if(self.driver.find_element_by_xpath('//*[@id="perfilAcesso"]').is_displayed()):
            # Houve erro ao realizar a mudança de alteração de perfil
            self.print_log(f'CNPJ não possui dados para realizar alteração de perfil.')
            try:
            	# Pode haver uma ocorrência de mesmo evento
            	self.driver.find_element_by_xpath('/html/body/div[6]/div[1]/a').click()
            except:
            	pass
            return False
        else:
            self.print_log('Perfil alterado')
            self.cnpj = cnpj
            return True

    def acessar_processos_digitais(self):
        self.print_log('Acessando Processos Digitais...')
        WebDriverWait(self.driver, 20).until(
            EC.invisibility_of_element_located((By.XPATH, "//div[@class='ui-widget-overlay']"))
        )
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="btn295"]/a'))
        ).click()
        link_processos_digitais = WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="containerServicos295"]/div[1]/ul/li/a'))
        )
        self.driver.get(link_processos_digitais.get_attribute('href'))

        xpath_meus_processos = '/html/body/app-root/div/app-tela-inicial/div/div/div/div/div[1]/a'
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, xpath_meus_processos))
        )
        self.driver.execute_script(f"(document.evaluate('{xpath_meus_processos}', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();")

        # Esperando carregar
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_element_located((By.XPATH, '/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[1]/div/p-datatable/div/p-paginator/div/span/a'))
        )

    
    def get_processos(self, index_tabpanel):
        
        self.driver.save_screenshot('screenie.png')
        self.change_tabpanel(index_tabpanel)

        # Verificando se há processos no painel selecionado
        xpath_td_without_data = f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr/td'
        if self.driver.find_element_by_xpath(xpath_td_without_data).text == 'Não foi encontrado processo que este contribuinte seja o interessado.':
            self.print_log('Não foi encontrado processo que este contribuinte seja o interessado.')          
            return None

        pages = len(self.driver.find_elements_by_xpath(f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/p-paginator/div/span/a'))
        self.print_log('Gravando no BD processos encontrados...')
        for index_page in range(1, pages+1):
            # Clicando na page
            WebDriverWait(self.driver, 20).until(
                EC.element_to_be_clickable((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/p-paginator/div/span/a[{index_page}]'))
            ).click()            
            
            time.sleep(1)
            table_lines = WebDriverWait(self.driver, 20).until(
                EC.presence_of_all_elements_located((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr'))
            )

            for tr in range(1, len(table_lines)+1):
                WebDriverWait(self.driver, 20).until(
                    EC.visibility_of_element_located((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[6]'))
                )
                processo = {}
                processo['cnpj'] = self.cnpj
                processo['numero'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[2]').text
                processo['dataProtocolo'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[3]').text
                processo['tipo'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[4]').text
                processo['subtipo'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[5]').text
                processo['localizacao'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[6]').text
                if index_tabpanel == 2:
                    processo['situacao'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[7]').text
                processo['idProcesso'] = ''.join(i for i in processo['numero'] if i.isdigit()) 

                if index_tabpanel == 1:
                    self.mydb.insert_processo_ativo(processo)
                else:
                    self.mydb.insert_processo_inativo(processo)
                
                self.get_historico(index_page, index_tabpanel, tr, processo['idProcesso'])

    def get_historico(self, index_page, index_tabpanel, tr, id_processo):
        
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr}]/td[1]/a'))
        ).click() # Ações +
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr[{tr+1}]/td/div/div/div[1]/div[3]/a'))
        ).click() # Consultar Histórico do Processo

        WebDriverWait(self.driver, 20).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr'))
        )
        
        # Esperando o spin sair da tela
        WebDriverWait(self.driver, 20).until(
            EC.invisibility_of_element_located((By.XPATH, "//div[@id='spin_modal_overlay']"))
        )

        # Selecionando o tamanho da tabela em 100
        select = Select(self.driver.find_element_by_xpath('/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/p-paginator/div/select'))
        select.select_by_index(3)
        time.sleep(1)


        table_lines_historico = len(self.driver.find_elements_by_xpath('/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr'))
        for tr_historico in range(1, table_lines_historico+1):
            WebDriverWait(self.driver, 20).until(
                EC.visibility_of_element_located((By.XPATH, f'/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr[{tr_historico}]/td[6]'))
            )
            historico_processo = {}
            historico_processo['idProcesso'] = id_processo
            historico_processo['siglaUnidade'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr[{tr_historico}]/td[1]').text
            historico_processo['equipeOuOperacao'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr[{tr_historico}]/td[2]').text
            historico_processo['nomeAtividade'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr[{tr_historico}]/td[3]').text
            historico_processo['dataEntrada'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr[{tr_historico}]/td[4]').text
            historico_processo['tempoAtividade'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr[{tr_historico}]/td[5]').text
            historico_processo['tempoMedioAtividade'] = self.driver.find_element_by_xpath(f'/html/body/app-root/div/app-historico-processo-consultar/div/div/p-datatable/div/div[2]/table/tbody/tr[{tr_historico}]/td[6]').text
            
            # Inserindo no BD o historico do processo
            if index_tabpanel == 1:
                self.mydb.insert_historico_ativo(historico_processo)
            else:
                self.mydb.insert_historico_inativo(historico_processo)

            
        self.driver.back()   
        
        # Ao retornar é necessário se posicionar na página novamente
        WebDriverWait(self.driver, 20).until(
            EC.invisibility_of_element_located((By.XPATH, "//div[@id='spin_modal_overlay']"))
        )
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/p-paginator/div/span/a[{index_page}]'))
        ).click()  
        time.sleep(1)
        WebDriverWait(self.driver, 20).until(
            EC.presence_of_all_elements_located((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/div[2]/table/tbody/tr'))
        )

        
    
    def change_tabpanel(self, index_tabpanel):
        # Esperando possível spin de load
        WebDriverWait(self.driver, 20).until(
            EC.invisibility_of_element_located((By.XPATH, "//div[@id='spin_modal_overlay']"))
        )

        # Clicando na tabpanel referente ao processo desejado -> 1-Ativo | 2-Inativo
        WebDriverWait(self.driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/ul/li[{index_tabpanel}]'))
        ).click()

        # Esperando o tabpanel renderizar
        WebDriverWait(self.driver, 20).until(
            EC.visibility_of_element_located((By.XPATH, f'/html/body/app-root/div/app-processos-consultar/div/div/p-tabview/div/div/p-tabpanel[{index_tabpanel}]/div/p-datatable/div/p-paginator/div/span/a'))
        )
        

    def generate_certificate(self):
        os.chdir(self.profile_path)
        os.system(f"pk12util -i '{self.certificate_path}' -d ./ -W '{self.certificate_psw}' -K ''")
        return True

    def print_log(self, msg):
        print(f'[{datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")}] {msg}')

    def quit(self):
        self.print_log('Processo finalizado!')
        self.driver.quit()