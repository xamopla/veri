<footer class="page-footer">
<div class="font-13">2020 © <b>Veri</b> - Todos os direitos reservados. <a href="https://www.sistemaveri.com.br/contrato/TermosDeUso.pdf" style="color: blue;" target="_blank" rel="noopener">Termo de Uso</a> do sistema.</div>
<div class="to-top"><i class="fa fa-angle-double-up"></i></div>
</footer>

</body>

</html>