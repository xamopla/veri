

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>VERI - Você no controle</title>

    <!-- FAVICON-->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/img/favicon/favicon.png'); ?>">

    <!-- CSS DO TEMPLATE-->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/line-awesome.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/plugins/themify-icons/css/themify-icons.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/toastr.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/bootstrap-select.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/sweetalert.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/select2.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/jquery.minicolors.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/daterangepicker.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/bootstrap-clockpicker.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/multi-select.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/plugins/tour/bootstrap-tour.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/main.min.css'); ?>" rel="stylesheet" />
    <!-- FIM DO CSS DO TEMPLATE-->

    <!-- SCRIPTS-->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/metisMenu.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/idle-timer.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/toastr.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-select.min.js'); ?>"></script>    
    <script src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.bootstrap4.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.colorbox-min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/sweetalert.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/scripts/sweetalert-demo.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.minicolors.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/mascaraValidacao.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/daterangepicker.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-clockpicker.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/mailbox.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/Chart.min.js') ?>"></script>   
    <script src="<?php echo base_url('assets/js/jquery.multi-select.js') ?>"></script>    
    <script src="<?php echo base_url('assets/plugins/tour/bootstrap-tour.js'); ?>"></script>   
    <!-- SCRIPTS-->
</head>

<body class="drawer-sidebar">
    <div class="page-wrapper">