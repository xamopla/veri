<link href="<?php echo base_url('assets/css/api.css'); ?>" rel="stylesheet" />
<style type="text/css">
    table, th, td {
      border: 1px solid black;
    }

    .form-control2 {
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border-color: rgba(0,0,0,.1);
        padding: .65rem 1.25rem;
        font-family: sans-serif,Arial;
    }

</style>
<div class="content-wrapper">
<!-- START PAGE CONTENT-->
<div class="page-heading">
    <h1 class="page-title">Documentação API</h1>
    <ol class="breadcrumb">
        
    </ol>
</div>
<div class="page-content fade-in-up">

    <div class="ibox">

            <div class="ibox-body">
                <div class="docs_table"> 
                    <table> 
                        <thead> 
                            <tr> 
                                <th>URL</th> 
                                <th>HTTP Verb</th> 
                                <th>Função</th> 
                            </tr> 
                        </thead> 
                        <tbody> 
                            <tr> 
                                <td><code>/API/V1/Api/criar_db</code></td> 
                                <td class="http_verb_post">POST</td> 
                                <td>Criar conta de novo cliente</td> 
                            </tr> 
                            <tr> 
                                <td><code>/API/V1/Api/atualizar_plano</code></td> 
                                <td class="http_verb_post">POST</td> 
                                <td>Atualizar plano contratado</td> 
                            </tr> 
                        </tbody> 
                    </table> 
                </div>
            </div>

            <div class="ibox-head">
                <div class="ibox-title">API Criar conta e Atualizar conta</div>
            </div>
            <div class="ibox" style="margin-left: 20px">
            <br>
            <div > 
                <table> 
                    <thead> 
                        <tr> 
                            <th style="width:20%;">Parâmetro</th> 
                            <th style="width:10%;">Obrigatório?</th> 
                            <th style="width:10%;">Tipo</th> 
                            <th>Descrição</th> 
                        </tr> 
                    </thead> 
                    <tbody> 
                        <tr> 
                            <td><code>nome_do_banco</code></td> 
                            <td><span class="obrigatorio">Obrigatório</span></td> 
                            <td>string</td> 
                            <td>Identificação do banco e subdominio que será criado para o usuario<br><code class="explain">Ex: bomfim -> criará o banco bomfim e será criado o subdominio bomfim.veri.com.br</code></td> 
                        </tr> 
                        <tr> 
                            <td><code>nome_do_plano</code></td> 
                            <td><span class="obrigatorio">Obrigatório</span></td> 
                            <td>string</td> 
                            <td>Descrição do Plano Contratado</td> 
                        </tr> 
                        <tr> 
                            <td><code>qtd_empresas</code></td> 
                            <td><span class="obrigatorio">Obrigatório</span></td> 
                            <td>integer</td> 
                            <td>Qtd máxima de empresas que podem ser cadastradas<br><code class="explain">Ex: Plano 20-50 empresas -> o campo deve ser preenchido com 50<br></code></td> 
                        </tr> 
                        <tr> 
                            <td><code>qtd_usuarios</code></td> 
                            <td><span class="obrigatorio">Obrigatório</span></td> 
                            <td>integer</td> 
                            <td>Qtd máxima de usuários que podem ser cadastrados<br><code class="explain">Ex: Plano 0-5 usuários -> o campo deve ser preenchido com 5<br></code></td> 
                        </tr> 
                        <tr> 
                            <td><code>data_validade</code></td> 
                            <td><span class="obrigatorio">Obrigatório</span></td> 
                            <td>date</td> 
                            <td>Data que expira o plano costuma ser contratos de 1 ano<br><code class="explain">Formato deve ser yyyy-mm-dd<br></code></td>
                        </tr> 
                    </tbody> 
                </table> 
            </div>
            </div>
            <div class="ibox" style="margin-left: 20px">
                <p>Todas as respostas são no formato objeto JSON.</p>
                <p>Uma requisição com erro retornará erro=true e uma mensagem com o motivo:</p>
                <div class="code-toolbar">
                    <pre class="language-json">
                        <code class="language-json">
                            <span class="token punctuation">{</span>
                            <span class="token property"> "error":"true",</span>
                            <span class="token operator"> "mensagem":"Erro ao criar o banco" || "Cliente ja existe"</span> 
                            <span class="token punctuation">}</span>
                        </code>
                    </pre>
                </div>
            </div>

            <div class="ibox" style="margin-left: 20px">
                <p>Uma requisição com sucesso retornará erro=false, sucess=true, uma mensagem com o motivo e a url da conta criada:</p>
                <div class="code-toolbar">
                    <pre class="language-json">
                        <code class="language-json">
                            <span class="token punctuation">{</span>
                            <span class="token property"> "error":"false",</span>
                            <span class="token property"> "sucess":"true",</span>
                            <span class="token operator"> "mensagem":"Cliente cadastrado",</span> 
                            <span class="token operator"> "url":"banco.veri.com.br"</span> 
                            <span class="token punctuation">}</span>
                        </code>
                    </pre>
                </div>
            </div>

            <form id="user_form" method="POST">
            <div class="ibox-body">
                
                <div class="row">
                     <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Nome do Banco:</label>
                            <input type="text" name="nome_do_banco" id="nome_do_banco" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Nome do Plano:</label>
                            <input type="text" name="nome_do_plano" id="nome_do_plano" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Qtd Empresas:</label>
                             <input type="text" name="qtd_empresas" id="qtd_empresas" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Qtd Usuários:</label>
                             <input type="text" name="qtd_usuarios" id="qtd_usuarios" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Data Validade:</label>
                             <?php echo form_input(array('name'=>'data_validade', 'id'=>'data_validade', 'class'=>'form-control2 date', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), '', 'required'); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="ibox-footer text-right">
                <button id="salvar" name="salvar" onclick="salvar()">Salvar</button>
                <button id="atualizar" onclick="atualizar()">Atualizar</button>
                <button id="logar" onclick="logar()">Logar</button>
            </div>
            </form>

    </div>
</div>
</div>
<script type="text/javascript">
    $('#data_validade').datepicker({
        startView: 0,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

   
</script>
<script type="text/javascript">
   
    function salvar(){
        $.ajax({
            url:"<?php echo base_url() . 'API/V1/Api/criar_db' ?>",
            method:"POST",
            data:$('#user_form').serialize(),
            dataType:"json",
            success:function(data)
            {
                if(data.error){
                    alert(data.mensagem);
                }else{
                    alert(data.mensagem);
                }
        
            }
        });
    }

    function atualizar(){
        $.ajax({
            url:"<?php echo base_url() . 'API/V1/Api/atualizar_plano' ?>",
            method:"POST",
            data:$('#user_form').serialize(),
            dataType:"json",
            success:function(data)
            {
                if(data.error){
                    alert(data.mensagem);
                }else{
                    alert(data.mensagem);
                }
        
            }
        });
    }

    function logar(){
        var nome_banco = $("#nome_banco").val();
       $.ajax({
            url:"https://demo.veri.com.br/API/V1/Api/logar",
            method:"POST",
            data:{'qtd_usuarios': $("#qtd_usuarios").val(), 'qtd_empresas': $("#qtd_empresas").val()},
            dataType:"json",
            success:function(data)
            {
                if(data.error){
                    alert(data.mensagem);
                }else{
                    alert(data.url);
                    //window.location = data.url;
                    // alert(data.mensagem);
                }
        
            }
        });
    }
</script>