<div class="content-wrapper">
<!-- START PAGE CONTENT-->
<div class="page-heading">
    <h1 class="page-title">Editar Plano</h1>
    <ol class="breadcrumb">
        
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Preencha os campos abaixo:</div>
            </div>
            <div class="ibox-body">
                <form id="user_form">
                <div class="row">
                     <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Nome do Banco:</label>
                            <input type="text" name="nome_do_banco" id="nome_do_banco">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Nome do Plano:</label>
                            <input type="text" name="nome_do_plano" id="nome_do_plano">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Qtd Empresas:</label>
                             <input type="text" name="qtd_empresas" id="qtd_empresas">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Qtd Usuários:</label>
                             <input type="text" name="qtd_usuarios" id="qtd_usuarios">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Data Validade:</label>
                             <?php echo form_input(array('name'=>'data_validade', 'id'=>'data_validade', 'class'=>'form-control date', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), ''); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="ibox-footer text-right">
                <button type="submit" name="salvar">Salvar</button>
                
            </div>
            </form>    
    </div>
</div>

<h2>Editar</h2>
<div class="page-content fade-in-up">
    <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Preencha os campos abaixo:</div>
            </div>
            <div class="ibox-body">
                <form id="user_form2">
                <div class="row">
                     <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Nome do Banco:</label>
                            <input type="text" name="nome_do_banco" id="nome_do_banco">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Nome do Plano:</label>
                            <input type="text" name="nome_do_plano" id="nome_do_plano">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Qtd Empresas:</label>
                             <input type="text" name="qtd_empresas" id="qtd_empresas">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Qtd Usuários:</label>
                             <input type="text" name="qtd_usuarios" id="qtd_usuarios">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Data Validade:</label>
                             <?php echo form_input(array('name'=>'data_validade', 'id'=>'data_validade2', 'class'=>'form-control date', 'placeholder'=>'', 'style'=>'border-color: #5c6bc0;'), ''); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="ibox-footer text-right">
                <button type="submit" name="salvar">Atualizar</button>
                
            </div>
            </form>

            
    </div>
</div>


<h2>Logar</h2>
<div class="page-content fade-in-up">
    <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Preencha os campos abaixo:</div>
            </div>
            <div class="ibox-body">
                <form id="user_form3">
                <div class="row">
                     <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Login</label>
                            <input type="text" name="login" id="login">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Senha</label>
                            <input type="text" name="senha" id="senha">
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox-footer text-right">
                <button type="submit" name="salvar">Logar</button>
                
            </div>
            </form>

            
    </div>
</div>

</div>
<script type="text/javascript">
    $('#data_validade').datepicker({
        startView: 0,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('#data_validade2').datepicker({
        startView: 0,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

   
</script>
<script type="text/javascript">
    $(document).on('submit', '#user_form', function(event){
        event.preventDefault();
        $.ajax({
            url:"<?php echo base_url() . 'API/V1/Api/criar_db' ?>",
            method:"POST",
            data:$(this).serialize(),
            dataType:"json",
            success:function(data)
            {
                if(data.error){
                    alert(data.mensagem);
                }else{
                    alert(data.mensagem);
                }
        
            }
        });
    });

    $(document).on('submit', '#user_form2', function(event){
        event.preventDefault();
        $.ajax({
            url:"<?php echo base_url() . 'API/V1/Api/atualizar_plano' ?>",
            method:"POST",
            data:$(this).serialize(),
            dataType:"json",
            success:function(data)
            {
                if(data.error){
                    alert(data.mensagem);
                }else{
                    alert(data.mensagem);
                }
        
            }
        });
    });

    $(document).on('submit', '#user_form3', function(event){
        event.preventDefault();
        $.ajax({
           url:"https://demo.veri.com.br/API/V1/Api/logar",
           method:"POST",
           data:$(this).serialize(),
           dataType:"json",
           success:function(data)
           {
               if(data.error){
                   alert(data.mensagem);
               }else{
                   alert(data.url);
                   //window.location = data.url;
                   // alert(data.mensagem);
               }
       
           }
       });
    });

 
</script>