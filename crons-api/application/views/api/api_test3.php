<!-- <link href="<?php echo base_url('assets/css/api.css'); ?>" rel="stylesheet" /> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<style type="text/css">
    table, th, td {
      border: 1px solid black;
    }

    .form-control2 {
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border-color: rgba(0,0,0,.1);
        padding: .65rem 1.25rem;
        font-family: sans-serif,Arial;
    }

</style>
<div class="content-wrapper">
<!-- START PAGE CONTENT-->
<div class="page-heading">
    <h1 class="page-title">Documentação API</h1>
    <ol class="breadcrumb">
        
    </ol>
</div>
<div class="page-content fade-in-up">

    <div class="ibox">

            <div class="ibox-body">
                <div class="docs_table"> 
                    <table> 
                        <thead> 
                            <tr> 
                                <th>URL</th> 
                                <th>HTTP Verb</th> 
                                <th>Função</th> 
                            </tr> 
                        </thead> 
                        <tbody> 

                            <tr> 
                                <td><code>http://veri-sp.com.br/crons-api/api/v/api_v2/verifica_nome_banco</code></td> 
                                <td class="http_verb_post">POST</td> 
                                <td>Verificar se o nome do banco já existe</td> 
                            </tr>

                            <tr> 
                                <td><code>http://veri-sp.com.br/crons-api/api/v/api_v2/criar_db</code></td> 
                                <td class="http_verb_post">POST</td> 
                                <td>Criar conta de novo cliente, chamar depois do aceite do contrato</td> 
                            </tr> 
                             
                        </tbody> 
                    </table> 
                </div>
            </div>

            <br>
            <div class="ibox-head">
                <div class="ibox-title">API Criar conta</div>
            </div>
            <div class="ibox" style="margin-left: 20px">
            <br>
            <div > 
                <table> 
                    <thead> 
                        <tr> 
                            <th style="width:20%;">Parâmetro</th> 
                            <th style="width:10%;">Obrigatório?</th> 
                            <th style="width:10%;">Tipo</th> 
                            <th>Descrição</th> 
                        </tr> 
                    </thead> 
                    <tbody> 
                        <tr> 
                            <td><code>nome_do_banco</code></td> 
                            <td><span class="obrigatorio">SIM</span></td> 
                            <td>string</td> 
                            <td>Identificação do banco e subdominio que será criado para o usuario<br><code class="explain">Ex: bomfim -> criará o banco bomfim e será criado o subdominio bomfim.veri.com.br</code></td> 
                        </tr> 
                        
                        <tr> 
                            <td><code>qtd_empresas</code></td> 
                            <td><span class="obrigatorio">SIM</span></td> 
                            <td>integer</td> 
                            <td>Qtd máxima de empresas que podem ser cadastradas<br><code class="explain">Ex: Plano 20-50 empresas -> o campo deve ser preenchido com 50<br></code></td> 
                        </tr> 

                        <!-- $cnpj, $razao_social, $nome_fantasia, $logradouro, $numero, $complemento, $bairro, $cidade, $uf, $cep, $email, $login -->
                        <tr> 
                            <td><code>cnpj</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Cnpj da empresa<br><code class="explain">Ex: 11.111.111/0001-11 <br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>razao_social</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Razão Social da empresa<br><code class="explain"><br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>nome_fantasia</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Nome Fantasia da empresa<br><code class="explain"><br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>logradouro</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Logradouro da empresa<br><code class="explain"><br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>numero</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Número da empresa<br><code class="explain"><br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>complemento</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Complemento da empresa<br><code class="explain"><br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>bairro</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Bairro da empresa<br><code class="explain"><br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>cidade</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>Cidade da empresa<br><code class="explain"><br></code></td> 
                        </tr> 

                        <tr> 
                            <td><code>uf</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>UF da empresa<br><code class="explain"><br></code></td> 
                        </tr>

                        <tr> 
                            <td><code>cep</code></td> 
                            <td><span class="obrigatorio">Não</span></td> 
                            <td>string</td> 
                            <td>CEP da empresa<br><code class="explain"><br></code></td> 
                        </tr>

                        <tr> 
                            <td><code>email</code></td> 
                            <td><span class="obrigatorio">SIM</span></td> 
                            <td>string</td> 
                            <td>Email da empresa<br><code class="explain"><br></code></td> 
                        </tr>

                        <tr> 
                            <td><code>login</code></td> 
                            <td><span class="obrigatorio">SIM</span></td> 
                            <td>string</td> 
                            <td>Login do usuário master<br><code class="explain">Ex: pode ser o email do campo anterior<br></code></td> 
                        </tr>
                         
                        
                    </tbody> 
                </table> 
            </div>
            </div>
            <div class="ibox" style="margin-left: 20px">
                <p>Todas as respostas são no formato objeto JSON.</p>
                <p>Uma requisição com erro retornará erro=true e uma mensagem com o motivo:</p>
                <div class="code-toolbar">
                    <pre class="language-json">
                        <code class="language-json">
                            <span class="token punctuation">{</span>
                            <span class="token property"> "error":"true",</span>
                            <span class="token property"> "sucess":"false",</span>
                            <span class="token operator"> "mensagem":"Erro ao criar o banco" || "Cliente ja existe"</span> 
                            <span class="token operator"> "url":""</span>
                            <span class="token punctuation">}</span>
                        </code>
                    </pre>
                </div>
            </div>

            <div class="ibox" style="margin-left: 20px">
                <p>Uma requisição com sucesso retornará erro=false, sucess=true, uma mensagem com o motivo e a url da conta criada:</p>
                <div class="code-toolbar">
                    <pre class="language-json">
                        <code class="language-json">
                            <span class="token punctuation">{</span>
                            <span class="token property"> "error":"false",</span>
                            <span class="token property"> "sucess":"true",</span>
                            <span class="token operator"> "mensagem":"Cliente cadastrado",</span> 
                            <span class="token operator"> "url":"banco.veri.com.br"</span> 
                            <span class="token punctuation">}</span>
                        </code>
                    </pre>
                </div>
            </div>

            <form id="user_form" method="POST">
            <div class="ibox-body">
                
                <div class="row">
                     <div class="col-md-4">
                        <div class="form-group mb-4">
                            <label for="nome">Nome do Banco:</label>
                            <input type="text" name="nome_do_banco" id="nome_do_banco" class="form-control2">
                        </div>
                    </div>

                    

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Qtd Empresas:</label>
                             <input type="text" name="qtd_empresas" id="qtd_empresas" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>CNPJ:</label>
                             <input type="text" name="cnpj" id="cnpj" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Razão Social:</label>
                             <input type="text" name="razao_social" id="razao_social" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Logradouro:</label>
                             <input type="text" name="logradouro" id="logradouro" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Numero:</label>
                             <input type="text" name="numero" id="numero" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Complemento:</label>
                             <input type="text" name="complemento" id="complemento" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Bairro:</label>
                             <input type="text" name="bairro" id="bairro" class="form-control2">
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Cidade:</label>
                             <input type="text" name="cidade" id="cidade" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Uf:</label>
                             <input type="text" name="uf" id="uf" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Cep:</label>
                             <input type="text" name="cep" id="cep" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>E-mail:</label>
                             <input type="text" name="email" id="email" class="form-control2">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group mb-4">
                             <label>Login:</label>
                             <input type="text" name="login" id="login" class="form-control2">
                        </div>
                    </div>

                </div>
            </div>
            <div class="ibox-footer text-right">
                <a href="javascript:void(0);" onclick="salvar()">Cadastrar</a>
                <br>
                <a href="javascript:void(0);" onclick="atualizar()">Verificar Banco</a>
            </div>
            </form>

    </div>
</div>
</div>

<script type="text/javascript">
   
    function salvar(){
        $.ajax({

            url:"https://veri-sp.com.br/crons-api/api/v/api_v2/criar_db",
            method:"POST",
            data:$('#user_form').serialize(),
            dataType:"json",
            success:function(data)
            {
                if(data.error){
                    alert(data.mensagem);
                }else{
                    alert(data.mensagem);
                }
        
            }
        });
    }

    function atualizar(){
        $.ajax({
            url:"https://veri-sp.com.br/crons-api/api/v/api_v2/verifica_nome_banco",
            method:"POST",
            data:$('#user_form').serialize(),
            dataType:"application/json",
            success:function(data)
            {
                if(data.error){
                    alert(data.mensagem);
                }else{
                    alert(data.mensagem);
                }
        
            }
        });
    }
    
</script>