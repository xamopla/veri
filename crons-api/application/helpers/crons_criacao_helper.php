<?php
	
	function criar_crons_br($nomeBanco)
	{
		$_TOKEN = 'EJ1OGAPPWO5JVM7DNYPYKZBTJ1ISE4PV';

		$servidores = ["34.136.135.16","104.154.147.3","35.238.110.3","34.133.3.93","35.222.105.241","34.121.10.118","35.185.87.48","34.71.57.46","35.225.104.129","35.237.10.231","34.75.88.231","104.196.133.186","34.139.147.201","34.138.5.76"];

		//------------------------------------------ ROBÔ ------------------------------------------------------

		// CRON - DIAGNOSTICO FISCAL BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://".$nomeServer."/SistemaCronsCertificado/sp/Situacao_fiscal_ecac_procuracao/cron_situacao_fiscal_com_procuracao/".$nomeBanco;
		$expressao = "R R,0-6,18-23 * * 1-5";
		$grupo = 168541;
		$time_zone = "America/Sao_Paulo";
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CADIN BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://".$nomeServer."/SistemaCronsCertificado/sp/Situacao_cadin_ecac_procuracao/cron_pendencia_cadin_com_procuracao/".$nomeBanco;
		$expressao = "R R,8-18 * * 1-5";
		$grupo = 168549;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - MENSAGENS ECAC BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://".$nomeServer."/SistemaCronsCertificado/sp/Mensagens_ecac_procuracao/buscar_ecac_com_procuracao/".$nomeBanco;
		$expressao = "R R,0-6,18-23 * * 1-5";
		$grupo = 168557;
		$time_zone = "America/Sao_Paulo";  
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - DAS BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://".$nomeServer."/SistemaCronsCertificado/sp/Das_ecac_procuracao/cron_das/".$nomeBanco;
		$expressao = "R R,0-6,18-23 * * 1-5";
		$grupo = 168565;
		$time_zone = "America/Sao_Paulo";  
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - DCTF BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://".$nomeServer."/SistemaCronsCertificado/sp/Dctf_ecac_procuracao/cron_dctf/".$nomeBanco;
		$expressao = "R R,0-6,18-23 * * 1-5";
		$grupo = 168573;
		$time_zone = "America/Sao_Paulo";  
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - DAS DEBITOS BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://".$nomeServer."/SistemaCronsCertificado/sp/Das_ecac_procuracao/cron_das_debitos/".$nomeBanco;
		$expressao = "R R,0-6,18-23 * * 1-5";
		$grupo = 168581;
		$time_zone = "America/Sao_Paulo";  
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName); 

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - VENCIMENTO PROCURACAO BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://".$nomeServer."/SistemaCronsCertificado/sp/vencimento_procuracao/cron_procuracao/".$nomeBanco;
		$expressao = "R R,0-6,18-23 * * 1-5";
		$grupo = 168589;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - EXECUTAR GERAL - BR
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "https://veri-sp.com.br/crons-api/executar_geral/processar/".$nomeBanco;
		$expressao = "R R,8-18 * * 1-5";
		$grupo = 168181;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);
	}

// --------------------------------------- CRIAR CRON ESTADUAL -----------------------------------------------

	function criar_crons_estadual($nomeBanco)
	{
		$_TOKEN = 'REJFE2CHL1LBYWPW1JPTMS4LCHWLJBZP';

		$servidores = ["34.136.135.16","104.154.147.3","35.238.110.3","34.133.3.93","35.222.105.241","34.121.10.118","35.185.87.48","34.71.57.46","35.225.104.129","35.237.10.231","34.75.88.231","104.196.133.186","34.139.147.201","34.138.5.76"];

		// CRON - CERTIDÃO ESTADUAL SP
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Saopaulo/buscar_estadual/".$nomeBanco;
		$expressao = "R R 15 * *";
		$grupo = 159209;
		$time_zone = "America/Sao_Paulo";
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO MUNICIPAL SP 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Saopaulo/buscar_municipal/".$nomeBanco;
		$expressao = "R R 10 * *";
		$grupo = 159213;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		//CRON - CERTIDÃO NEGATIVA RIO DE JANEIRO 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Certidao_rio/buscar_rio/".$nomeBanco;
		$expressao = "R R 27 * *";
		$grupo = 164793;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO NEGATIVA FORTALEZA
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Certidao_ceara/buscar_fortaleza/".$nomeBanco;
		$expressao = "R R 27 * *";
		$grupo = 164789;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO NEGATIVA CEARA 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Certidao_ceara/buscar_estadual/".$nomeBanco;
		$expressao = "R R 27 * *";
		$grupo = 164785;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO NEGATIVA PORTO ALEGRE 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Certidao_rs/buscar_porto_alegre/".$nomeBanco;
		$expressao = "R R 19 * *";
		$grupo = 164781;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO NEGATIVA RIO GRANDE DO SUL
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Certidao_rs/buscar_estadual/".$nomeBanco;
		$expressao = "R R 17 * *";
		$grupo = 164777;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO NEGATIVA ARACAJU
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Certidao_sergipe/buscar_aracaju/".$nomeBanco;
		$expressao = "R R 3 * *";
		$grupo = 164773;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO PGFN SP 1
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Federal_sp/buscar/".$nomeBanco;
		$expressao = "R R L * *";
		$grupo = 159217;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO PGFN SP EXTRA 1
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Federal_sp/buscar_extra/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 164437;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO PGFN SP EXTRA 2
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Federal_sp/buscar_extra_2/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 164441;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO PGFN SP EXTRA 3 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Federal_sp/buscar_extra_3/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 164445;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO PGFN SP EXTRA 4 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Federal_sp/buscar_extra_4/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 164449;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO FGTS SP 1 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Caixa_sp/buscar/".$nomeBanco;
		$expressao = "R R L * *";
		$grupo = 159225;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO FGTS SP EXTRA 1 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Caixa_sp/buscar_extra/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 163797;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO FGTS SP EXTRA 2
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Caixa_sp/buscar_extra_2/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 163801;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO FGTS SP EXTRA 3 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Caixa_sp/buscar_extra_3/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 163805;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO FGTS SP EXTRA 4 
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Caixa_sp/buscar_extra_4/".$nomeBanco;
		$expressao = "R R 2 * *";
		$grupo = 163809;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO TRABALHISTA SP 1
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Trabalhista_sp/buscar/".$nomeBanco;
		$expressao = "R R 5 * *";
		$grupo = 159221;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO TRABALHISTA SP EXTRA 1
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Trabalhista_sp/buscar_extra/".$nomeBanco;
		$expressao = "R R 7 * *";
		$grupo = 164421;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO TRABALHISTA SP EXTRA 2
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Trabalhista_sp/buscar_extra_2/".$nomeBanco;
		$expressao = "R R 7 * *";
		$grupo = 164425;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO TRABALHISTA SP EXTRA 3
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Trabalhista_sp/buscar_extra_3/".$nomeBanco;
		$expressao = "R R 7 * *";
		$grupo = 164429;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);

		//------------------------------------------ ROBÔ ------------------------------------------------------
		// CRON - CERTIDÃO TRABALHISTA SP EXTRA 4
		$nomeServer = $servidores[rand(0, count($servidores)-1)];

		$url = "http://191.252.93.133/Trabalhista_sp/buscar_extra_4/".$nomeBanco;
		$expressao = "R R 7 * *";
		$grupo = 164433;
		$time_zone = "America/Sao_Paulo"; 
		$nome_banco = $nomeBanco;

		$arrayName = array('token' => $_TOKEN, 'url' => $url, 'expression' => $expressao, 'timezone'=> $time_zone, 'group' => $grupo, 'name' => $nome_banco);

		$URL_CRON_JOB = "https://www.setcronjob.com/api/cron.add?" . http_build_query($arrayName);

		$curl = curl_init($URL_CRON_JOB);
		curl_setopt($curl, RETURNTRANSFER, 1);
		curl_exec($curl);
	}
