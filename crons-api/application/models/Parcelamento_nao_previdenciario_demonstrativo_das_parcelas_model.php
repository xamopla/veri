<?php

class Parcelamento_nao_previdenciario_demonstrativo_das_parcelas_model extends CI_Model {

    public $id;
    public $id_tributo;
    public $cnpj;
    public $numero_parcela;
    public $data_vencimento;
    public $valor_ate_vencimento;
    public $saldo_devedor_atual;
    public $situacao;
    public $path_download_parcela;

    public function clear($cnpj, $banco){
        return $this->db->delete($banco.'.dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas', "cnpj = '{$cnpj}'");
    }

    public function insert($dados, $banco){
        date_default_timezone_set('America/Sao_Paulo');
        $this->id_tributo = $dados['id_tributo'];
        $this->cnpj = $dados['cnpj'];
        $this->numero_parcela = $dados['numero_parcela'];
        $this->data_vencimento = $dados['data_vencimento'];
        $this->valor_ate_vencimento = $dados['valor_ate_vencimento'];
        $this->saldo_devedor_atual = $dados['saldo_devedor_atual'];
        $this->situacao = $dados['situacao'];

        $this->db->insert($banco.'.dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas', $this);
        return $this->db->insert_id();
    }

    public function update($dados, $banco){
        date_default_timezone_set('America/Sao_Paulo');
        $this->db->set('saldo_devedor_atual', $dados['saldo_devedor_atual']);
        $this->db->set('situacao', $dados['situacao']);

        $this->db->where('cnpj', $dados['cnpj']);
        $this->db->where('id_tributo', $dados['id_tributo']);

        $this->db->update($banco.'.dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas');
    }

    // public function get_demonstrativo($cnpj, $banco, $id_tributo, $numero_parcela){
    //     $this->db->select('db.*');
        
    //     $this->db->where('cnpj', $cnpj);
    //     $this->db->where('id_tributo', $id_tributo);
    //     $this->db->where('numero_parcela', $numero_parcela);

    //     return $this->db->get($banco.'.dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas db')->row();
    // }

    public function update_path($banco, $numero_parcela, $cnpj, $id_tributo, $caminho_download){
        date_default_timezone_set('America/Sao_Paulo');
        $this->db->set('path_download_parcela', $caminho_download);

        $this->db->where('cnpj', $cnpj);
        $this->db->where('id_tributo', $id_tributo);
        $this->db->where('numero_parcela', $numero_parcela);

        $this->db->update($banco.'.dtb_parcelamento_nao_previdenciario_demonstrativo_das_parcelas');
    }

    public function get_aux($id, $banco)
    {       
        $this->db->select('*');
        $this->db->join($banco.'.dtb_contador_procuracao d','db.id_contador = d.id_contador');
        $this->db->where('d.id_empresa', $id);
        return $this->db->get($banco.'.dtb_certificado_contador db')->result();
    }

    public function find_empresa_by_cnpj($banco, $cnpj){
        $this->db->select('id');
        $this->db->where("cnpj", $cnpj);

        return $this->db->get($banco.'.dtb_empresas')->row();
    }
}
