<?php

class Dec_qtd_mensagem_model extends CI_Model {

    public  $cnpj;
    public  $razao_social ;
    public  $qtd_nao_lida ;

    public function clear($cnpj, $banco){
        return $this->db->delete($banco.'.dtb_dec_qtd_mensagem', "cnpj = {$cnpj}");
    }

    public function insert($dados, $banco){
        date_default_timezone_set('America/Sao_Paulo');

        $this->cnpj = $dados['cnpj'];
        $this->razao_social = $dados['razao_social'];
        $this->qtd_nao_lida = $dados['qtd_nao_lida'];

        $this->db->insert($banco.'.dtb_dec_qtd_mensagem', $this);
        return $this->db->insert_id();
    }

    public function get_cnpjs($banco){
		$this->db->select('cnpj');
		$resultado = $this->db->get($banco.'.dtb_dec_qtd_mensagem')->result();
		$cnpjs = array();
		foreach ($resultado as $r){
			$cnpjs[] = $r->cnpj;
		}
		return $cnpjs;
	}
}
