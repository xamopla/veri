<?php

class Simplesnacional_emissao_parcela_model extends CI_Model {

	public $valor;
	public $data_parcela;
	public $cnpj;
    public $path_download_parcela;

	public function insert($cnpj, $banco, $dados)
	{
		$this->valor = $dados['valor'];
		$this->data_parcela	 = $dados['data_parcela'];
		$this->cnpj = $cnpj;
		$this->db->insert($banco.'.dtb_simplesnacional_parcelas_emitidas', $this);
	}

    public function update($cnpj, $banco, $dados)
    {
        $this->db->set('valor', $dados['valor']);
        $this->db->where('data_parcela', $dados['data_parcela']);
        $this->db->where('cnpj', $cnpj);
        $this->db->update($banco.'.dtb_simplesnacional_parcelas_emitidas');
    }

    public function verifica_se_existe($banco, $cnpj, $data_parcela ){
        $this->db->select('COUNT(distinct(dtb_simplesnacional_parcelas_emitidas.data_parcela)) AS qtd');
        $this->db->where('cnpj', $cnpj);
        $this->db->where('data_parcela', $data_parcela);
        return $this->db->get($banco.'.dtb_simplesnacional_parcelas_emitidas')->row();
    }

    public function update_path($banco, $data_parcela, $cnpj, $path_download_parcela)
    {
        $this->db->set('path_download_parcela', $path_download_parcela);
        $this->db->where('data_parcela', $data_parcela);
        $this->db->where('cnpj', $cnpj);
        $this->db->update($banco.'.dtb_simplesnacional_parcelas_emitidas');
    }
}
