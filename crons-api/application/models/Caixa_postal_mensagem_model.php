<?php

class Caixa_postal_mensagem_model extends CI_Model {

	public $assunto;
	public $remetente;
	public $recebida_em;
	public $caixa_postal_id;
	public $lida;
	public $importante;
	public $id_mensagem;

	public function insert($data, $banco)
	{
		$var = $data['recebida_em'];
		$date = str_replace('/', '-', $var);

		$this->assunto    = $data['assunto'];
		$this->conteudo = $data['conteudo'];
		$this->remetente  = $data['remetente'];
		$this->recebida_em     = date('Y-m-d', strtotime($date));
		$this->caixa_postal_id     = $data['caixa_postal_id'];
		$this->lida = $data['lida'];
		$this->importante = $data['importante'];
		$this->id_mensagem = $data['id_mensagem'];
		
		$this->db->insert($banco.'.dtb_ecac_caixa_postal_mensagem', $this);
		return $this->db->insert_id();
	}

	public function limpaTabelaMensagens($caixa_postal_id, $banco){
		return $this->db->delete($banco.'.dtb_ecac_caixa_postal_mensagem', "caixa_postal_id = {$caixa_postal_id}");
	}
}
