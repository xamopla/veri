<?php

class Parcelamento_lei_12996_demonstrativo_prestacoes_model extends CI_Model {

    public $id;
    public $id_divida_consolidada;
    public $cnpj;
    public $parcela_id;
    public $data_parcela;
    public $valor_parc_minima;
    public $valor_parcela_divida;
    public $valor_parc_calculada;
    public $saldo_parc_devedora;
    public $juros_parc_deverdora;
    public $indicador_parcela_devida;
    public $indicador_situacao_parcela;
    public $indicador_reducao;
    public $valor_total_arrecadacao;
    public $valor_reducao_mes;
    public $valor_antecipacao_mes;
    public $quantidade_parc_red;
    public $path_download_parcela;

    public function buscar_dados_gerar_parcela($id){

		$this->db->select('db.*, db2.*');

		$this->db->from('dtb_parcelamento_lei_12996_demonstrativo_prestacoes db');
		$this->db->join('dtb_parcelamento_lei_12996_divida_consolidada db2','trim(db.id_divida_consolidada) = trim(db2.id)');

		$this->db->where("db.id", $id);
	
		return $this->db->get()->row();
	}

    public function update_path($dados, $caminho_download){
        date_default_timezone_set('America/Sao_Paulo');
        $this->db->set('path_download_parcela', $caminho_download);

        $this->db->where('cnpj', $dados['cnpj']);
        $this->db->where('data_parcela', $dados['strPeriodoApuracao']);

        $this->db->update($dados['banco'].'.dtb_parcelamento_lei_12996_demonstrativo_prestacoes');

        //print_r($this->db->last_query());
    }

    public function get_aux($id, $banco)
    {       
        $this->db->select('*');
        $this->db->join($banco.'.dtb_contador_procuracao d','db.id_contador = d.id_contador');
        $this->db->where('d.id_empresa', $id);
        return $this->db->get($banco.'.dtb_certificado_contador db')->result();
    }

    public function find_empresa_by_cnpj($banco, $cnpj){
        $this->db->select('id');
        $this->db->where("cnpj", $cnpj);

        return $this->db->get($banco.'.dtb_empresas')->row();
    }
}
