<?php

class Mei_emissao_parcela_model extends CI_Model {

	public $valor;
	public $data_parcela;
	public $cnpj;
    public $path_download_parcela;

	public function insert($cnpj, $banco, $dados)
	{
		$this->valor = $dados['valor'];
		$this->data_parcela	 = $dados['data_parcela'];
		$this->cnpj = $cnpj;
		$this->db->insert($banco.'.dtb_mei_parcelas_emitidas', $this);
	}

    public function update($cnpj, $banco, $dados)
    {
        $this->db->set('valor', $dados['valor']);
        $this->db->where('data_parcela', $dados['data_parcela']);
        $this->db->where('cnpj', $cnpj);
        $this->db->update($banco.'.dtb_mei_parcelas_emitidas');
    }

    public function verifica_se_existe($banco, $cnpj, $data_parcela ){
        $this->db->select('COUNT(distinct(dtb_mei_parcelas_emitidas.data_parcela)) AS qtd');
        $this->db->where('cnpj', $cnpj);
        $this->db->where('data_parcela', $data_parcela);
        return $this->db->get($banco.'.dtb_mei_parcelas_emitidas')->row();
    }

    public function update_path($banco, $data_parcela, $cnpj, $path_download_parcela)
    {
        $this->db->set('path_download_parcela', $path_download_parcela);
        $this->db->where('trim(data_parcela)', trim($data_parcela));
        $this->db->where('cnpj', $cnpj);
        $this->db->update($banco.'.dtb_mei_parcelas_emitidas');
    }

    public function get_aux($id, $banco)
    {       
        $this->db->select('*');
        $this->db->join($banco.'.dtb_contador_procuracao d','db.id_contador = d.id_contador');
        $this->db->where('d.id_empresa', $id);
        return $this->db->get($banco.'.dtb_certificado_contador db')->result();
    }

    public function find_empresa_by_cnpj($banco, $cnpj){
        $this->db->select('id');
        $this->db->where("cnpj", $cnpj);

        return $this->db->get($banco.'.dtb_empresas')->row();
    }
}
