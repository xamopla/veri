<?php

class Parcelamento_pert_rfb_model extends CI_Model {

    public function update_path($banco, $cnpj, $id_parcela, $caminho_download){
        date_default_timezone_set('America/Sao_Paulo');
        $this->db->set('path_download_parcela', $caminho_download);

        $this->db->where('cnpj', $cnpj);
        $this->db->where('id_parcela', $id_parcela);

        $this->db->update($banco.'.dtb_parcelamento_pert_rfb_demonstrativo_de_parcelas');
    }

    public function get_aux($id, $banco)
    {       
        $this->db->select('*');
        $this->db->join($banco.'.dtb_contador_procuracao d','db.id_contador = d.id_contador');
        $this->db->where('d.id_empresa', $id);
        return $this->db->get($banco.'.dtb_certificado_contador db')->result();
    }

    public function find_empresa_by_cnpj($banco, $cnpj){
        $this->db->select('id');
        $this->db->where("cnpj", $cnpj);

        return $this->db->get($banco.'.dtb_empresas')->row();
    }
}
