<?php

class Pert_model extends CI_Model {

	public $id;
	public $cnpj;
    public $compentencia;
    public $numero_declaracao;
    public $data_hora_transmissao;
    public $numero_das;
    public $data_hora_emissao;
    public $pago;
    public $caminho_download_recibo;
    public $caminho_download_declaracao;
    public $caminho_download_extrato;

	public function verifica_se_existe($numero_declaracao, $banco, $cnpj){
		$this->db->select('COUNT(distinct(dtb_ecac_pert.id)) AS qtd');
        $this->db->where('numero_declaracao', $numero_declaracao);
		$this->db->where('cnpj', $cnpj);
		return $this->db->get($banco.'.dtb_ecac_pert')->row();
	}

	public function insert($dados, $banco){
		date_default_timezone_set('America/Sao_Paulo');
	    $this->cnpj = $dados['cnpj'];
        $this->compentencia = $dados['compentencia'];
        $this->numero_declaracao = $dados['numero_declaracao'];
        $this->data_hora_transmissao = $dados['data_hora_transmissao'];
        $this->numero_das = $dados['numero_das'];
        $this->data_hora_emissao = $dados['data_hora_emissao'];
        $this->pago = $dados['pago'];
        $this->caminho_download_recibo = $dados['caminho_download_recibo'];
        $this->caminho_download_declaracao = $dados['caminho_download_declaracao'];
        $this->caminho_download_extrato = $dados['caminho_download_extrato'];

		$this->db->insert($banco.'.dtb_ecac_pert', $this);
		return $this->db->insert_id();
	}

	public function update($dados, $banco){
		date_default_timezone_set('America/Sao_Paulo');

		if ($this->db->update($banco.'.dtb_ecac_pert', $dados, "numero_declaracao='".$dados['numero_declaracao']."'")){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_caminho_download_das($caminho_download , $numero_declaracao, $banco){
        $this->db->set('caminho_download_das', $caminho_download);
        $this->db->where('numero_declaracao', $numero_declaracao);
        $this->db->update($banco.'.dtb_ecac_pert');
    }

	//Funções auxiliares na busca por certificados por procuração ou individual
    public function find_certificado($cnpj, $banco){
        $this->db->select('*');
        $this->db->where('cnpj_data', $cnpj);
        return $this->db->get($banco.'.dtb_certificado')->row();
    }

    public function get_aux($id, $banco)
    {       
        $this->db->select('*');
        $this->db->join($banco.'.dtb_contador_procuracao d','db.id_contador = d.id_contador');
        $this->db->where('d.id_empresa', $id);
        return $this->db->get($banco.'.dtb_certificado_contador db')->result();
    }

    public function find_empresa_by_cnpj($banco, $cnpj){
        $this->db->select('id');
        $this->db->where("cnpj", $cnpj);

        return $this->db->get($banco.'.dtb_empresas')->row();
    }

    //Fim das funçoes auxiliares

    public function find_all_cert($banco){
        $this->db->select('*');
        return $this->db->get($banco.'.dtb_certificado_contador db')->result();
    }
}
