<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contadorprocuracao_model extends CI_Model {

    private $id;
    private $id_empresa;
    private $id_contador;

    function __construct()
    {
        parent::__construct();
    }

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getIdEmpresa() {
        return $this->id_empresa;
    }
    public function setIdEmpresa($id_empresa) {
        $this->id_empresa = $id_empresa;
        return $this;
    }

    public function getIdContador() {
        return $this->id_contador;
    }
    public function setIdContador($id_contador) {
        $this->id_contador = $id_contador;
        return $this;
    }

    public function findAllByIdEmpresa() {
        $this->db->select('id_contador as id');
        $this->db->where('id_empresa', $this->getIdEmpresa());

        return $this->db->get('dtb_contador_procuracao')->result();
    }

    public function buscar_empresas_vinculadas($banco, $id){
        $this->db->select('dbtc.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dbtc.id_empresa = e.id', 'left');
        $this->db->where('dbtc.id_contador', $id);
        $this->db->where('dbtc.id_empresa <=', 250);
        $this->db->order_by('dbtc.id_empresa asc');

        return $this->db->get($banco.'.dtb_contador_procuracao as dbtc')->result();
    }

    public function buscar_empresas_vinculadas_extra($banco, $id){
        $this->db->select('dbtc.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dbtc.id_empresa = e.id', 'left');
        $this->db->where('dbtc.id_contador', $id);
        $this->db->where('dbtc.id_empresa >', 250);
        $this->db->where('dbtc.id_empresa <=', 500);
        $this->db->order_by('dbtc.id_empresa asc');

        return $this->db->get($banco.'.dtb_contador_procuracao as dbtc')->result();
    }

    public function buscar_empresas_vinculadas_extra_2($banco, $id){
        $this->db->select('dbtc.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dbtc.id_empresa = e.id', 'left');
        $this->db->where('dbtc.id_contador', $id);
        $this->db->where('dbtc.id_empresa >', 500);
        $this->db->where('dbtc.id_empresa <=', 700);
        $this->db->order_by('dbtc.id_empresa asc');

        return $this->db->get($banco.'.dtb_contador_procuracao as dbtc')->result();
    }

    public function buscar_empresas_vinculadas_extra_3($banco, $id){
        $this->db->select('dbtc.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dbtc.id_empresa = e.id', 'left');
        $this->db->where('dbtc.id_contador', $id);
        $this->db->where('dbtc.id_empresa >', 700);
        $this->db->where('dbtc.id_empresa <=', 900);
        $this->db->order_by('dbtc.id_empresa asc');

        return $this->db->get($banco.'.dtb_contador_procuracao as dbtc')->result();
    }

    public function buscar_empresas_vinculadas_extra_4($banco, $id){
        $this->db->select('dbtc.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dbtc.id_empresa = e.id', 'left');
        $this->db->where('dbtc.id_contador', $id);
        $this->db->where('dbtc.id_empresa >', 900);
        $this->db->order_by('dbtc.id_empresa asc');

        return $this->db->get($banco.'.dtb_contador_procuracao as dbtc')->result();
    }

    public function buscar_empresas_vinculadas_extra_validar($banco, $id){
        $this->db->select('dbtc.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dbtc.id_empresa = e.id', 'left');
        $this->db->where('dbtc.id_contador', $id);
        $this->db->where('dbtc.id_empresa', 88);

        return $this->db->get($banco.'.dtb_contador_procuracao as dbtc')->result();
    }

     public function buscar_empresas_vinculadas_teste($banco, $id){
        $this->db->select('dbtc.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dbtc.id_empresa = e.id', 'left');
        $this->db->where('dbtc.id_contador', $id);
        $this->db->where('e.cnpj like "%0001%" ');

        return $this->db->get($banco.'.dtb_contador_procuracao as dbtc')->result();
    }

    public function buscar_empresas_vinculadas_dec($banco, $id){
        
        $this->db->select($banco.'.dtb_contador_procuracao_dec_sp.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dtb_contador_procuracao_dec_sp.id_empresa = e.id', 'left');
        $this->db->where($banco.'.dtb_contador_procuracao_dec_sp.id_contador', $id);

        return $this->db->get($banco.'.dtb_contador_procuracao')->result();
    }

    public function insere_empresas_sem_procuracao($banco, $cnpj){
        $dados = array( 
                'cnpj' => $cnpj
        );
        
        if ($this->db->insert($banco.'.dtb_empresas_sem_procuracao', $dados)){
            return $this->db->insert_id();
        } else {
            return FALSE;
        }

    }

    public function clear_table($banco){
        $this->db->truncate($banco.'.dtb_empresas_sem_procuracao');
    }

    public function buscar_empresa($id, $cnpj,$banco){
        
        $this->db->select('dtb_contador_procuracao.id_empresa, e.razao_social, e.cnpj');
        $this->db->join($banco.'.dtb_empresas as e', 'dtb_contador_procuracao.id_empresa = e.id');
        $this->db->where('dtb_contador_procuracao.id_contador', $id);
        $this->db->where('e.cnpj', preg_replace("/[^0-9]/", "", $cnpj));

        return $this->db->get($banco.'.dtb_contador_procuracao')->row();
    }

}