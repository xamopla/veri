<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/Certificate/Pkcs12.php');

class Dec_library_teste {
      /**
     * BANCO
     *
     * Indica o caminho para a pasta que salva os pdfs
     *
     * @var string
     */
    protected $banco        = '';

    /**
     * CAMINHO_DA_PASTA_PDFS
     *
     * Indica o caminho para a pasta que salva os pdfs
     *
     * @var string
     */
    protected $caminho_da_pasta_pdfs        = '';
    
    /**
     * NUMERO DOCUMENTO
     * @var string
     */
    protected $cnpj        = '';


    /**
     * CI Singleton
     *
     * @var object
     */
    protected $CI;

    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        $this->initialize($params);
        log_message('info', 'Ecac Robo Class Initialized');
    }

    public function initialize(array $params = array())
    {
        foreach ($params as $key => $val)
        {
            if (property_exists($this, $key))
            {
                $this->$key = $val;
            }
        }

        return $this;
    }

    public function get_qtd_mensagem(){
        $ch = curl_init();
        $post = array(
            'banco' =>  $this->banco,
            'cnpj' => $this->cnpj,
        );
        curl_setopt($ch, CURLOPT_URL, "http://34.139.11.176/index.php/Dec/dec_caixapostal_qtd");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    public function get_caixa_postal($cnpj=''){
        $ch = curl_init();
        $post = array(
            'banco' => $this->banco,
            'cnpj_buscar' => $cnpj != '' ? $cnpj : $this->cnpj,
            'cnpj' => $this->cnpj
        );
        curl_setopt($ch, CURLOPT_URL, "http://34.139.11.176/index.php/Dec/dec_caixa_postal");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    public function ler_mensagem($cnpj, $identificacao_buscar){
        $ch = curl_init();
        $post = array(
            'banco' => $this->banco,
            'cnpj' => $this->cnpj,
            'cnpj_buscar' => $cnpj,
            'identificacao' => $identificacao_buscar
        );

        curl_setopt($ch, CURLOPT_URL, "http://34.139.11.176/index.php/Dec/dec_ler_mensagem");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        return $response;
    }

    public function ler_comunicado($cnpj, $assunto_buscar, $complemento_buscar, $data_envio_buscar){
        $ch = curl_init();
        $post = array(
            'cert_key' => $this->cert_key,
            'cnpj' => $cnpj,
            'folder_pdf' => $this->caminho_da_pasta_pdfs,
            'assunto_buscar' => $assunto_buscar,
            'complemento_buscar' => $complemento_buscar,
            'data_envio_buscar' => $data_envio_buscar,
        );

        curl_setopt($ch, CURLOPT_URL, "http://0.0.0.0:5000/dec-ler-comunicado");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        return json_decode($response, true);
    }

}
