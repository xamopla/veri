<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/Simple_html_dom.php');
require_once(APPPATH.'libraries/Certificate/Pkcs12.php');

class SefazBA_antigo_library {


    /**
     * LOGIN URL
     *
     * URL que faz o login com certificado digital A1
     *
     * @var	string
     */
    protected $login_url_certificado = 'https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/ASLibrary.X509/CertLogin';

    /**
     * CODIGO ACESSO
     *
     * Codigo de acesso pessoal do ecac, usado somente
     * se quiser acessar o portal sem o certificado com o codigo de acesso
     *
     * @var	string
     */
    protected $codigo_acesso		= '';

    /**
     * Numero do documento
     *
     * Número do documento CPF ou CNPJ
     *
     * @var	string
     */
    protected $numero_documento		= '';

    /**
     * SENHA
     *
     * Senha para o acesso através de codigo de acesso
     *
     * @var	string
     */
    protected $senha_codigo_acesso		= '';

    /**
     * PRIVATE_KEY
     *
     * Path para a chave privada do certificado
     *
     * @var	string
     */
    protected $private_key		= '';

    /**
     * PUBLIC_KEY
     *
     * Path para chave publica do certificado
     *
     * @var	string
     */
    protected $public_key		= '';

    /**
     * CERT_KEY
     *
     * Path para o arquivo cert key
     *
     * @var	string
     */
    protected $cert_key		= '';

    /**
     * CERTIFICADO_SENHA
     *
     * Senha do certificado digital
     *
     * @var	string
     */
    protected $cerficado_senha		= '';

    /**
     * caminho_certificado
     *
     * caminho do certificado digital
     *
     * @var	string
     */
    protected $caminho_certificado		= '';

    /**
     * URL
     *
     * A url que deseja acessar no momento após o login
     *
     * @var	string
     */
    protected $url		= '';

    /**
     * ACESSO_VALIDO
     *
     * Valida a conexão feita com o site, caso tenha dado algum erro emite uma mensagem e vai para o próximo
     *
     * @var	string
     */
    protected $acesso_valido		= true;

    /**
     * CI Singleton
     *
     * @var	object
     */
    protected $CI;

    private $curl;

    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        $this->curl = curl_init();
        $this->initialize($params);
        $this->gerar_chaves();
        if(!$this->conectar_via_certificado())
            $this->acesso_valido = false;
    }

    public function initialize(array $params = array())
    {
        foreach ($params as $key => $val)
        {
            if (property_exists($this, $key))
            {
                $this->$key = $val;
            }
        }

        return $this;
    }

    /**
     * gerar_chaves
     *
     * Gera as chaves de acesso do certificado informado
     *
     */

    function gerar_chaves(){

        $pkcs = new Pkcs12(APPPATH . 'libraries/Certificate/certificados_clientes/');

        $pkcs->loadPfxFile($this->caminho_certificado, $this->cerficado_senha);

//		seta as chaves na classe
        $this->public_key = $pkcs->pubKeyFile;
        $this->private_key = $pkcs->priKeyFile;
        $this->cert_key = $pkcs->certKeyFile;
        $this->numero_documento = $pkcs->cnpj;
        return true;
    }
    /**
     * conectar_via_certificado
     */
    function conectar_via_certificado(){

        $url_login = $this->login_url_certificado ;

// 		Faz login e pega o cookie de sessao
        curl_setopt($this->curl, CURLOPT_URL , $url_login );
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, 1);
        curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($this->curl, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($this->curl, CURLOPT_USERAGENT, "UZ_".uniqid());
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array(  "Connection: keep-alive",
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Sec-Fetch-Site: none",
            "Sec-Fetch-Mode: navigate",
            "Sec-Fetch-User: ?1",
            "Sec-Fetch-Dest: document",
            "Accept-Language: pt-BR,pt;q=0.9"));
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($this->curl, CURLOPT_SSLCERT, $this->public_key);
        curl_setopt($this->curl, CURLOPT_SSLKEY, $this->private_key);
        curl_setopt($this->curl, CURLOPT_CAINFO, $this->cert_key);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_PORT , 443);
        curl_setopt($this->curl, CURLOPT_ENCODING, '');

        if( curl_errno($this->curl) )
            echo curl_error( $this->curl );

        $response = curl_exec( $this->curl );

        return true;
    }

    public function get_home(){
        $url = 'https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/Contribuinte/ConsultaContaDomicilioTributario/CadastroContaDomicilioTributario';
        $header = array(  "Connection: keep-alive",
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Sec-Fetch-Site: none",
            "Sec-Fetch-Mode: navigate",
            "Sec-Fetch-Dest: document",
            "Accept-Language: pt-BR,pt;q=0.9",);

// 		Faz login e pega o cookie de sessao
        curl_setopt($this->curl, CURLOPT_URL , $url );
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, 1);
        curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($this->curl, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($this->curl, CURLOPT_USERAGENT, "UZ_".uniqid());
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_PORT , 443);
        curl_setopt($this->curl, CURLOPT_ENCODING, '');

        if( curl_errno($this->curl) )
            echo curl_error( $this->curl );

        $response = curl_exec( $this->curl );

        echo $this->converterCaracterEspecial( $response );

        return true;
    }

    public function converterCaracterEspecial($text){
        return html_entity_decode($text, ENT_QUOTES, "utf-8");
    }

    public function encerrar_conection(){
        curl_close( $this->curl );
    }

    public function obter_numero_documento(){
        return $this->numero_documento;
    }

    function __destruct()
    {
        $this->encerrar_conection();
    }
}
