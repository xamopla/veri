<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/Simple_html_dom.php');

class Cnd_barreiras_library {

    /**
     * CNPJ
     *
     * cnpj da empresa
     *
     * @var	string
     */
    protected $cnpj		= '';

    /**
     * CAMINHO_DA_PASTA_PDFS
     *
     * caminho da pasta onde ficarao salvos os pdfs
     *
     * @var	string
     */
    protected $caminho_da_pasta_pdfs		= '';

    private $curl;

    private $impedimento_na_emissao = false;

    protected $CI;

    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        $this->curl = curl_init();
        $this->initialize($params);
        $this->fazer_login();
//        Necessário chamar aqui para setar se tem ou nao impedimento na emissao
        $this->get_pagina_principal();
    }

    public function fazer_login(){
        $url_login = "https://barreiras.saatri.com.br/Certidao/Emitir?Pint_TipoLista=2&hdn_CertidaoEmpresa=True&hdn_CertidaoImovel=True&hdn_CertidaoContribuinte=True&Pint_TipoCertidao=3&Pstr_CpfCnpj={$this->cnpj}&hdn_CpfCnpjCertidaoAnterior=&";
        return $this->get($url_login);
    }

    public function get_pagina_principal(){
        $url_pagina = "https://barreiras.saatri.com.br/Certidao/Emitir?Pint_TipoLista=2&hdn_CertidaoEmpresa=True&hdn_CertidaoImovel=True&hdn_CertidaoContribuinte=True&Pint_TipoCertidao=3&Pstr_CpfCnpj={$this->cnpj}&hdn_CpfCnpjCertidaoAnterior=&Pbol_ComMenuEsquerdo=True";
        $page = $this->get($url_pagina);

        $html = new Simple_html_dom();
        $html->load($page);
        $input_certidao = $html->find('input[id=hdn_Mensagem]', 0);
        $texto_base = "Impedimento na emissão";
        $pos = strpos($input_certidao->value, $texto_base);

        if ($pos !== false)
            $this->impedimento_na_emissao = true;

        return $page;
    }

    public function baixar_pdf() {
        $page = $this->get_pagina_principal();
        $html = new Simple_html_dom();
        $html->load($page);

        $table_principal = $html->find('table[id=tbl_GridCertidao]', 0);
        $linhas = $table_principal->find('tr');
        array_shift($linhas);
        $url_download_pdf = '';
        foreach ($linhas as $linha){
            $url_download_pdf = "https://barreiras.saatri.com.br".$this->converterCaracterEspecial($linha->find('a[class=ImprimirCertidao]', 0)->href);
            break;
        }
        return $this->get_pdf($url_download_pdf);
    }

    public function initialize(array $params = array())
    {
        foreach ($params as $key => $val)
        {
            if (property_exists($this, $key))
            {
                $this->$key = $val;
            }
        }

        return $this;
    }

    public function get($url = '')
    {
        $headers = array("Connection: keep-alive",
            "Cache-Control: no-cache",
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-Mode: navigate',
            'Sec-Fetch-User: ?1',
            'Sec-Fetch-Dest: document',
            'Referer: https://barreiras.saatri.com.br/',
            'Accept-Language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7');
        curl_setopt($this->curl, CURLOPT_URL , $url );
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($this->curl, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, 1);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'cookie_barreiras.txt');


        $response = curl_exec($this->curl);
        if(curl_errno($this->curl)){
            echo curl_error($this->curl);
        }

        return $response;
    }

    public function get_pdf($url = '')
    {
        $headers = array("Connection: keep-alive",
            "Cache-Control: no-cache",
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-Mode: navigate',
            'Sec-Fetch-User: ?1',
            'Sec-Fetch-Dest: document',
            'Referer: https://barreiras.saatri.com.br/',
            'Accept-Language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7');
        curl_setopt($this->curl, CURLOPT_URL , $url );
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($this->curl, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, 1);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'cookie_barreiras.txt');
        $fp = fopen ($this->caminho_da_pasta_pdfs."/certidao-negativa-{$this->cnpj}.pdf", 'w+');
        curl_setopt($this->curl, CURLOPT_FILE, $fp);

        curl_exec($this->curl);
        if(curl_errno($this->curl)){
            echo curl_error($this->curl);
        }

        return $this->caminho_da_pasta_pdfs."/certidao-negativa-{$this->cnpj}.pdf";
    }

    public function impedimento_na_emissao(){
        return $this->impedimento_na_emissao;
    }

    public function converterCaracterEspecial($text){
        return html_entity_decode($text, ENT_QUOTES, "utf-8");
    }

    public function encerrar_conection(){
        curl_close( $this->curl );
    }

    function __destruct()
    {
        $this->encerrar_conection();
    }
}
