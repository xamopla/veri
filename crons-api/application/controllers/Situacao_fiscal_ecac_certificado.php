<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Situacao_fiscal_ecac_certificado extends CI_Controller {

	function cron_situacao_fiscal(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

		$banco = $this->uri->segment(3);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificado_cron_model', 'certificado');
		date_default_timezone_set('America/Bahia');

		$cerficados = $this->certificado->get($banco);
		
		$folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '1200000');
		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library
			 */
			$params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
			'caminho_da_pasta_pdfs' => $folder_pdf);
			$this->load->library('Ecac_robo_eprocessos_library', $params);

			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */
			if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
				unset($this->ecac_robo_eprocessos_library);
				continue;
			}

			/**
			 * Grava a situação fiscal e o pdf
			 */

            $path_pdf = $this->ecac_robo_eprocessos_library->baixar_pdf_situacao_fiscal();

            $carregado = false;
            $pdf    =  new PdfToText() ;

            try{
                $pdf->Load( $path_pdf ) ;
                $carregado = true;
            }catch (Exception $e){
                continue;
            } 
            
            $texto_base = "Pendência -";
            $pos = strpos($pdf->Text, $texto_base);

            $possui_pendencia =  false;

            if ($pos !== false)
                $possui_pendencia = true;

            $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

            $caminho_download = "https://veri-sp.com.br".$caminho_aux;

            $existe_situacao = $this->verifica_se_existe_situacao($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            if($existe_situacao > 0){
                $this->update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }else{
                $this->inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }

            echo "==============SUCESSO NA OPERAÇÃO==========\n";
            echo "Documento: {$this->ecac_robo_eprocessos_library->obter_numero_documento()}\n";
            $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
            echo "Situação Fiscal: {$mensagem_pendencia}\n";
            echo "PDF situação: {$path_pdf}\n";
            echo "===========================================\n";


			unset($this->ecac_robo_eprocessos_library);

		}

	}

	function cron_situacao_fiscal_extra(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

		$banco = $this->uri->segment(3);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificado_cron_model', 'certificado');
		date_default_timezone_set('America/Bahia');

		$cerficados = $this->certificado->get_extra($banco);
		
		$folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '1200000');
		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library
			 */
			$params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
			'caminho_da_pasta_pdfs' => $folder_pdf);
			$this->load->library('Ecac_robo_eprocessos_library', $params);

			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */
			if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
				unset($this->ecac_robo_eprocessos_library);
				continue;
			}

			/**
			 * Grava a situação fiscal e o pdf
			 */

            $path_pdf = $this->ecac_robo_eprocessos_library->baixar_pdf_situacao_fiscal();

            $carregado = false;
            $pdf    =  new PdfToText() ;

            try{
                $pdf->Load( $path_pdf ) ;
                $carregado = true;
            }catch (Exception $e){
                continue;
            } 
            
            $texto_base = "Pendência -";
            $pos = strpos($pdf->Text, $texto_base);

            $possui_pendencia =  false;

            if ($pos !== false)
                $possui_pendencia = true;

            $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

            $caminho_download = "https://veri-sp.com.br".$caminho_aux;

            $existe_situacao = $this->verifica_se_existe_situacao($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            if($existe_situacao > 0){
                $this->update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }else{
                $this->inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }

            echo "==============SUCESSO NA OPERAÇÃO==========\n";
            echo "Documento: {$this->ecac_robo_eprocessos_library->obter_numero_documento()}\n";
            $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
            echo "Situação Fiscal: {$mensagem_pendencia}\n";
            echo "PDF situação: {$path_pdf}\n";
            echo "===========================================\n";


			unset($this->ecac_robo_eprocessos_library);

		}

	}

	function cron_situacao_fiscal_extra_2(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

		$banco = $this->uri->segment(3);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificado_cron_model', 'certificado');
		date_default_timezone_set('America/Bahia');

		$cerficados = $this->certificado->get_extra_2($banco);
		
		$folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '1200000');
		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library
			 */
			$params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
			'caminho_da_pasta_pdfs' => $folder_pdf);
			$this->load->library('Ecac_robo_eprocessos_library', $params);

			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */
			if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
				unset($this->ecac_robo_eprocessos_library);
				continue;
			}

			/**
			 * Grava a situação fiscal e o pdf
			 */

            $path_pdf = $this->ecac_robo_eprocessos_library->baixar_pdf_situacao_fiscal();

            $carregado = false;
            $pdf    =  new PdfToText() ;

            try{
                $pdf->Load( $path_pdf ) ;
                $carregado = true;
            }catch (Exception $e){
                continue;
            } 
            
            $texto_base = "Pendência -";
            $pos = strpos($pdf->Text, $texto_base);

            $possui_pendencia =  false;

            if ($pos !== false)
                $possui_pendencia = true;

            $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

            $caminho_download = "https://veri-sp.com.br".$caminho_aux;

            $existe_situacao = $this->verifica_se_existe_situacao($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            if($existe_situacao > 0){
                $this->update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }else{
                $this->inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }

            echo "==============SUCESSO NA OPERAÇÃO==========\n";
            echo "Documento: {$this->ecac_robo_eprocessos_library->obter_numero_documento()}\n";
            $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
            echo "Situação Fiscal: {$mensagem_pendencia}\n";
            echo "PDF situação: {$path_pdf}\n";
            echo "===========================================\n";


			unset($this->ecac_robo_eprocessos_library);

		}

	}

	function cron_situacao_fiscal_extra_3(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

		$banco = $this->uri->segment(3);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificado_cron_model', 'certificado');
		date_default_timezone_set('America/Bahia');

		$cerficados = $this->certificado->get_extra_3($banco);
		
		$folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '1200000');
		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library
			 */
			$params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
			'caminho_da_pasta_pdfs' => $folder_pdf);
			$this->load->library('Ecac_robo_eprocessos_library', $params);

			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */
			if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
				unset($this->ecac_robo_eprocessos_library);
				continue;
			}

			/**
			 * Grava a situação fiscal e o pdf
			 */

            $path_pdf = $this->ecac_robo_eprocessos_library->baixar_pdf_situacao_fiscal();

            $carregado = false;
            $pdf    =  new PdfToText() ;

            try{
                $pdf->Load( $path_pdf ) ;
                $carregado = true;
            }catch (Exception $e){
                continue;
            } 
            
            $texto_base = "Pendência -";
            $pos = strpos($pdf->Text, $texto_base);

            $possui_pendencia =  false;

            if ($pos !== false)
                $possui_pendencia = true;

            $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

            $caminho_download = "https://veri-sp.com.br".$caminho_aux;

            $existe_situacao = $this->verifica_se_existe_situacao($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            if($existe_situacao > 0){
                $this->update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }else{
                $this->inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }

            echo "==============SUCESSO NA OPERAÇÃO==========\n";
            echo "Documento: {$this->ecac_robo_eprocessos_library->obter_numero_documento()}\n";
            $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
            echo "Situação Fiscal: {$mensagem_pendencia}\n";
            echo "PDF situação: {$path_pdf}\n";
            echo "===========================================\n";


			unset($this->ecac_robo_eprocessos_library);

		}

	}

	function cron_situacao_fiscal_extra_4(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

		$banco = $this->uri->segment(3);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificado_cron_model', 'certificado');
		date_default_timezone_set('America/Bahia');

		$cerficados = $this->certificado->get_extra_4($banco);
		
		$folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '1200000');
		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library
			 */
			$params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
			'caminho_da_pasta_pdfs' => $folder_pdf);
			$this->load->library('Ecac_robo_eprocessos_library', $params);

			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */
			if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
				unset($this->ecac_robo_eprocessos_library);
				continue;
			}

			/**
			 * Grava a situação fiscal e o pdf
			 */

            $path_pdf = $this->ecac_robo_eprocessos_library->baixar_pdf_situacao_fiscal();

            $carregado = false;
            $pdf    =  new PdfToText() ;

            try{
                $pdf->Load( $path_pdf ) ;
                $carregado = true;
            }catch (Exception $e){
                continue;
            } 
            
            $texto_base = "Pendência -";
            $pos = strpos($pdf->Text, $texto_base);

            $possui_pendencia =  false;

            if ($pos !== false)
                $possui_pendencia = true;

            $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

            $caminho_download = "https://veri-sp.com.br".$caminho_aux;

            $existe_situacao = $this->verifica_se_existe_situacao($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            if($existe_situacao > 0){
                $this->update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }else{
                $this->inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            }

            echo "==============SUCESSO NA OPERAÇÃO==========\n";
            echo "Documento: {$this->ecac_robo_eprocessos_library->obter_numero_documento()}\n";
            $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
            echo "Situação Fiscal: {$mensagem_pendencia}\n";
            echo "PDF situação: {$path_pdf}\n";
            echo "===========================================\n";


			unset($this->ecac_robo_eprocessos_library);

		}

	}

	function verifica_se_existe_situacao($cnpj_data, $banco){
		$this->load->model('situacao_fiscal_model');
		$resultado = $this->situacao_fiscal_model->verifica_se_existe($cnpj_data, $banco);
		return $resultado->qtd;
	}

	function inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $cnpj_data, $banco){
		$this->load->model('situacao_fiscal_model');
		$this->situacao_fiscal_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $cnpj_data, $banco);
	}

	function update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $cnpj_data, $banco){
		$this->load->model('situacao_fiscal_model');
		$this->situacao_fiscal_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $cnpj_data, $banco);
	}
}
