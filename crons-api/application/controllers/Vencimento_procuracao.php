<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vencimento_procuracao extends CI_Controller {

	function cron_procuracao(){
        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('procuracao_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => '');
            $this->load->library('Ecac_robo_library_eprocessos_procuracao_vencimento', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library_eprocessos_procuracao_vencimento->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao_vencimento);
                continue;
            }

            /**
             * PROCURAÇÃO consulta
             */
            $procuracoes = $this->ecac_robo_library_eprocessos_procuracao_vencimento->get_procuracoes();

            if ($procuracoes){
                $this->procuracao_model->clear($this->ecac_robo_library_eprocessos_procuracao_vencimento->obter_numero_documento(), $banco);

                foreach ($procuracoes as $item) {
                    $this->procuracao_model->insert($this->ecac_robo_library_eprocessos_procuracao_vencimento->obter_numero_documento(), $item, $banco);
                }
            }
            unset($this->ecac_robo_library_eprocessos_procuracao_vencimento);
        }
    }

}