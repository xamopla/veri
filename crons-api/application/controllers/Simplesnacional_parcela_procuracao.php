<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simplesnacional_parcela_procuracao extends CI_Controller {

    function buscar_parcelas()
    {
        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('Simplesnacional_emissao_parcela_model');

        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado) {
            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//', '/', $cerficado->caminho_arq),
                'cerficado_senha' => $cerficado->pass,
            );

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library_procuracao');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library_procuracao->acesso_valido()) {
                unset($this->ecac_robo_library_procuracao);
                continue;
            }
            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);
            foreach ($empresas_com_procuracao as $item) {
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_procuracao->trocar_perfil($item->cnpj);
                if (!$validado) {
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    continue;
                }
                $parcelas = $this->ecac_robo_library_procuracao->obter_simples_nacional_emissao_parcela();
                if ($parcelas) {
                    foreach ($parcelas as $parcela) {

                        echo var_export($parcela);
                        echo "<br>";
                        $result = $this->Simplesnacional_emissao_parcela_model->verifica_se_existe($banco, $this->ecac_robo_library_procuracao->obter_numero_documento(), $parcela['data_parcela']);
                        if ($result->qtd > 0) {
                            $this->Simplesnacional_emissao_parcela_model->update($this->ecac_robo_library_procuracao->obter_numero_documento(), $banco, $parcela);
                        } else {
                            $this->Simplesnacional_emissao_parcela_model->insert($this->ecac_robo_library_procuracao->obter_numero_documento(), $banco, $parcela);
                        }
                    }
                }
            }

//			Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
            unset($this->ecac_robo_library_procuracao);
        }
    }
}
