<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

date_default_timezone_set('America/Sao_Paulo');

class Executar_geral extends CI_Controller {

	public function processar(){

	$banco = $this->uri->segment(3);

    // CRON - VERIFICA PGDAS - BR
    $urlVerificarPgdas = "https://veri-sp.com.br/crons-api/Verifica_pgdas_matriz/processar/".$banco;
    $this->get($urlVerificarPgdas);
    echo "CRON - VERIFICA PGDAS - BR";
    echo "<br>";

	// CRON - CORRIGE PGDAS - BR
	$urlCorrigePgdas = "https://veri-sp.com.br/crons-api/Verifica_pgdas_vinculado/processar/".$banco;
    $this->get($urlCorrigePgdas);
    echo "CRON - CORRIGE PGDAS - BR";
    echo "<br>";
	
	// CRON - MALHA FISCAL - BR
	$urlMalhaFisca = "https://veri-sp.com.br/crons-api/Cron_malha_fiscal/processar/".$banco;
    $this->get($urlMalhaFisca);
    echo "CRON - MALHA FISCAL - BR";
    echo "<br>";

    // CRON - AUSÊNCIA DECLARAÇÃO DCTF - BR	
	$urlAusenciaDctf = "https://veri-sp.com.br/crons-api/Declaracao_dctf_ausente/processar/".$banco;
    $this->get($urlAusenciaDctf);  
    echo "CRON - AUSÊNCIA DECLARAÇÃO DCTF - BR";
    echo "<br>";
	
	// CRON - AUSÊNCIA DECLARAÇÃO PGDAS - BR
 	$urlAusenciaPgdas = "https://veri-sp.com.br/crons-api/Declaracao_pgdas_ausente/processar/".$banco;
    $this->get($urlAusenciaPgdas);
    echo "CRON - AUSÊNCIA DECLARAÇÃO PGDAS - BR";
    echo "<br>";
	
	// CRON - AUSÊNCIA DECLARAÇÃO EFD - BR
	$urlAusenciaEfd = "https://veri-sp.com.br/crons-api/Declaracao_efd_ausente/processar/".$banco;
    $this->get($urlAusenciaEfd);
    echo "CRON - AUSÊNCIA DECLARAÇÃO EFD - BR";
    echo "<br>";

    // CRON - AUSÊNCIA DECLARAÇÃO ECF - BR
	$urlAusenciaEcf = "https://veri-sp.com.br/crons-api/Declaracao_ecf_ausente/processar/".$banco;
    $this->get($urlAusenciaEcf);
    echo "CRON - AUSÊNCIA DECLARAÇÃO ECF - BR";
    echo "<br>";
	
	// CRON - AUSÊNCIA DECLARAÇÃO DASN - BR
	$urlAusenciaDasn = "https://veri-sp.com.br/crons-api/Declaracao_dasn_ausente/processar/".$banco;
    $this->get($urlAusenciaDasn);
    echo "CRON - AUSÊNCIA DECLARAÇÃO DASN - BR";
    echo "<br>";
	
	// CRON - AUSÊNCIA DECLARAÇÃO DIRF - BR
	$urlAusenciaDirf = "https://veri-sp.com.br/crons-api/Declaracao_dirf_ausente/processar/".$banco;
    $this->get($urlAusenciaDirf);
    echo "CRON - AUSÊNCIA DECLARAÇÃO DIRF - BR";
    echo "<br>";

    // CRON - AUSÊNCIA DECLARAÇÃO DEFIS - BR
    $urlAusenciaDefis = "https://veri-sp.com.br/crons-api/Declaracao_defis_ausente/processar/".$banco;
    $this->get($urlAusenciaDefis);
    echo "CRON - AUSÊNCIA DECLARAÇÃO DEFIS - BR";
    echo "<br>";

    // CRON - AUSENCIA DECLARAÇÃO GFIP - BR
    $urlAusenciaGfip = "http://veri-sp.com.br/crons-api/Declaracao_gfip_ausente/processar/".$banco;
    $this->get($urlAusenciaGfip);
    echo "CRON - AUSENCIA DECLARAÇÃO GFIP - BR";
    echo "<br>";

    // CRON - PROCESSAR GFIP - BR
    $urlProcessarGfip = "https://veri-sp.com.br/crons-api/gfip_empresa/processar/".$banco;
    $this->get($urlProcessarGfip);
    echo "CRON - PROCESSAR GFIP - BR";
    echo "<br>";

    // CRON - GERAR CHAVES - BR
    $urlGerarChaves = "https://veri-sp.com.br/crons-api/Certificado_chaves/atualizar_todos/".$banco;
    $this->get($urlGerarChaves);
    echo "CRON - GERAR CHAVES - BR";
    echo "<br>";

    // CRON - LIMITE SIMPLES
    $urlLimiteSimples = "https://veri-sp.com.br/crons-api/Limite_simples/processar/".$banco;
    $this->get($urlLimiteSimples);
    echo "CRON - LIMITE SIMPLES";
    echo "<br>";

    // CRON - REGIME EMPRESAS / EXCLUSÃO SIMPLES 
    $urlRegimeEmpresas = "https://veri-sp.com.br/crons-api/regime_empresa/processar/".$banco;
    $this->get($urlRegimeEmpresas);
    echo "CRON - REGIME EMPRESAS / EXCLUSÃO SIMPLES";
    echo "<br>";

    // CRON - NOTIFICAÇÃO DCTF 
    // $urlNotificacaoDctf = "https://veri-sp.com.br/crons-api/dctf_notificacao/processar/".$banco;
    // $this->get($urlNotificacaoDctf); 
    // echo "CRON - NOTIFICAÇÃO DCTF";
    // echo "<br>";

	} 

	function get($url){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        curl_close($ch);
    }

}
