<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dec_qtd_mensagem_procuracao extends CI_Controller {
	
	public function buscar()
	{	

		$banco = $this->uri->segment(3);

        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('Dec_qtd_mensagem_model');


		date_default_timezone_set('America/Bahia');
		$cerficados = $this->certificado->get($banco);
		
		foreach ($cerficados as $cerficado){
			$params = array(
				'banco' => $banco ,
				'cnpj' => $cerficado->cnpj_data,
			);
			$this->load->library('Dec_library', $params);

            $registros = $this->dec_library->get_qtd_mensagem();

            foreach ($registros as $registro){
                $this->Dec_qtd_mensagem_model->clear($registro['cnpj'], $banco);
                $this->Dec_qtd_mensagem_model->insert($registro, $banco);
            }

            unset($this->dec_library);
		}

	}
}
