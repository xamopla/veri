<?php
defined('BASEPATH') or exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Parcelamento_pert_rfb_emitir_darf extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Parcelamento_pert_rfb_model');
        $this->load->model('certificado_model');
        $this->load->model('certificadocontador_model');
    }

    public function emitir_function()
    {
        $banco = trim(strval($this->input->post('banco')));
        $cnpj = trim(strval($this->input->post('cnpj')));
        $id_formatado = trim(strval($this->input->post('id_formatado')));
        $id_parcela = trim(strval($this->input->post('id_parcela')));

        $id_empresa = $this->Parcelamento_pert_rfb_model->find_empresa_by_cnpj($banco, $cnpj)->id;
        $cerficados = $this->Parcelamento_pert_rfb_model->get_aux($id_empresa, $banco);

        if (!empty($cerficados)) {
            $retorno = $this->emitir_parcela_procuracao($banco, $cnpj, $id_formatado, $id_parcela, $cerficados);
            echo $retorno;
        } else {
            $retorno = $this->emitir_parcela($banco, $cnpj, $id_formatado, $id_parcela);

            echo $retorno;
        }
    }

    public function emitir_parcela_procuracao($banco, $cnpj, $id_formatado, $id_parcela, $cerficados)
    {
        $folder_pdf = FCPATH . 'pdf-parcelamento-pert-rfb/' . $banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        foreach ($cerficados as $cerficado_contador) {
            $params = array(
                'caminho_certificado' => str_replace('//', '/', $cerficado_contador->caminho_arq),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf
            );

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library->acesso_valido()) {
                unset($this->ecac_robo_library);
                continue;
            }


            $validado = $this->ecac_robo_library->trocar_perfil($cnpj);

            if (!$validado) {
                echo "ERRO2";
                continue;
            }

            $caminho_download = $this->ecac_robo_library->gerar_parcela_pert_rfb($id_formatado, $id_parcela);

            if ($caminho_download == 'ERROECAC')
                return 'ERROECAC';

            if ($caminho_download != "") {
                $this->Parcelamento_pert_rfb_model->update_path($banco, $cnpj, $id_parcela, $caminho_download);
                unset($this->ecac_robo_library);
                return $caminho_download;
            } else {
                unset($this->ecac_robo_library);
                return "ERRO";
            }
        }
        unset($this->ecac_robo_library);
    }

    public function emitir_parcela($banco, $cnpj, $id_formatado, $id_parcela)
    {
        $certificado = $this->certificado_model->find_certificado($cnpj, $banco);
        $folder_pdf = FCPATH . 'pdf-parcelamento-pert-rfb/' . $banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $params = array(
            'caminho_certificado' => str_replace('//', '/', $certificado->caminho_arq),
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf
        );
        $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

        if (!$this->ecac_robo_library->acesso_valido()) {
            unset($this->ecac_robo_library);
            echo json_encode(array("error" => true, "mensagem" => "erro1"));
        }
        $caminho_download = $this->ecac_robo_library->gerar_parcela_pert_rfb($id_formatado, $id_parcela);

        if ($caminho_download == 'ERROECAC')
            return 'ERROECAC';

        if ($caminho_download != "") {
            $caminho_download = str_replace("/var/www/html", "https://veri-sp.com.br", $caminho_download);
            $this->Parcelamento_pert_rfb_model->update_path($banco, $cnpj, $id_parcela, $caminho_download);

            unset($this->ecac_robo_library);
            return $caminho_download;
        } else {
            unset($this->ecac_robo_library);
            return "ERRO";
        }
    }
}
