<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dec_qtd_mensagem extends CI_Controller {
	
	public function buscar()
	{	

		$banco = $this->uri->segment(3);

		$this->load->model('certificado_model', 'certificado');
        $this->load->model('Dec_qtd_mensagem_model');


		date_default_timezone_set('America/Bahia');
		$cerficados = $this->certificado->get($banco);
		
		foreach ($cerficados as $cerficado){


			$params = array(
			    'caminho_certificado' => 'https://veri.com.br/'.str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
            );
			$this->load->library('Dec_library', $params);

            $registros = $this->dec_library->get_qtd_mensagem();

            foreach ($registros as $registro){
                $this->Dec_qtd_mensagem_model->clear($registro['cnpj'], $banco);
                $this->Dec_qtd_mensagem_model->insert($registro, $banco);
            }

            unset($this->dec_library);
		}

	}
}
