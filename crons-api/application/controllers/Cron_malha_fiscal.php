<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_malha_fiscal extends CI_Controller {

    public function processar(){

        //http://localhost/veri-sp-1.0/Api-Crons/Cron_malha_fiscal/processar/

        $banco = $this->uri->segment(3);
        $this->load->model('malha_fiscal_model');

        $mensagens = $this->malha_fiscal_model->find_emails_nao_enviados($banco);

        foreach ($mensagens as $m) {
            $existe_registro = $this->malha_fiscal_model->verifica_se_existe($banco, $m->cnpj, $m->id_mensagem_ecac, $m->assunto, $m->recebida_em);

            if($existe_registro->qtd <= 0){
                $this->malha_fiscal_model->insere_mensagem_malha($banco, $m->id_mensagem_ecac, $m->recebida_em, $m->assunto, $m->conteudo, $m->lida, $m->cnpj, $m->razao_social);
            }else{

                $malha = $this->malha_fiscal_model->find_id($banco, $m->cnpj, $m->id_mensagem_ecac, $m->assunto, $m->recebida_em);
                $this->malha_fiscal_model->update($banco, $m->id_mensagem_ecac, $m->recebida_em, $m->assunto, $m->conteudo, $m->lida, $m->cnpj, $m->razao_social, $malha->id);
            }
            
        }
        
    }

}
