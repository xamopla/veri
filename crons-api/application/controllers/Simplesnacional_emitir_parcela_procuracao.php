<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Simplesnacional_emitir_parcela_procuracao extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Simplesnacional_emissao_parcela_model');
        $this->load->model('certificado_model');
        $this->load->model('empresa_model');
        $this->load->model('certificadocontador_model');

    }

    public function emitir(){
        $banco = $this->input->post('banco');
        $cnpj = $this->input->post('cnpj');
        $data_parcela = $this->input->post('data_parcela');

        $id_empresa = $this->empresa_model->find_empresa_by_cnpj($banco, $cnpj)->id;
        $cerficados = $this->certificadocontador_model->find_by_empresa_id($id_empresa, $banco);

        $retorno = $this->emitir_parcela_procuracao( $data_parcela, $cnpj, $banco, $cerficados);
        echo $retorno;
    }

    public function emitir_parcela_procuracao($data_parcela, $cnpj, $banco, $cerficados){

        $folder_pdf = FCPATH . 'pdf-parcela-simplesnacional-ecac/'.$banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        foreach ($cerficados as $cerficado_contador) {

            $params = array('caminho_certificado' => str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library_dctf->acesso_valido()) {
                unset($this->ecac_robo_library_dctf);
                continue;
            }

            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj, $banco);

            if( $empresa ){

                $validado = $this->ecac_robo_library_dctf->trocar_perfil($cnpj);

                if(! $validado){
                    echo "ERRO2";
                    continue;
                }
                $caminho_download = $this->ecac_robo_library_dctf->gerar_parcela_simplesnacional($data_parcela, $cnpj);
                if($caminho_download != ""){
                    $caminho_download = str_replace("/var/www/html", "https://veri.com.br",$caminho_download);
                    $this->Simplesnacional_emissao_parcela_model->update_path($banco, $data_parcela, $cnpj, $caminho_download);
                    unset($this->ecac_robo_library_dctf);
                    return $caminho_download;
                }else{
                    unset($this->ecac_robo_library_dctf);
                    return "ERRO";
                }
            }
        }
        unset($this->ecac_robo_library_dctf);
    }
}
