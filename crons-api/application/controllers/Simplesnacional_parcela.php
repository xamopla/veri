<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simplesnacional_parcela extends CI_Controller {

    function buscar_parcelas(){
        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificado_model', 'certificado');
        $this->load->model('Simplesnacional_emissao_parcela_model');


        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado) {

            /**
             * Carrega a library principal Ecac_robo_library
             */
            $params = array('caminho_certificado' => 'https://veri.com.br/' . str_replace('//', '/', $cerficado->caminho_arq),
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));

            $this->load->library('Ecac_robo_library_procuracao', $params);

            $parcelas = $this->ecac_robo_library_procuracao->obter_simples_nacional_emissao_parcela();
            if ($parcelas){
                foreach ($parcelas as $parcela){
                    $result = $this->Simplesnacional_emissao_parcela_model->verifica_se_existe($banco,  $this->ecac_robo_library_procuracao->obter_numero_documento(), $parcela['data_parcela']);
                    if($result->qtd > 0){
                        $this->Simplesnacional_emissao_parcela_model->update($this->ecac_robo_library_procuracao->obter_numero_documento(), $banco, $parcela);
                    }else{
                        $this->Simplesnacional_emissao_parcela_model->insert($this->ecac_robo_library_procuracao->obter_numero_documento(), $banco, $parcela);
                    }
                }
            }
        }
    }
}
