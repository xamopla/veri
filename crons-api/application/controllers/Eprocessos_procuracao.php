<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eprocessos_procuracao extends CI_Controller {

	function buscar(){
		$banco = $this->uri->segment(3);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificadocontador_model', 'certificado');
		$this->load->model('contadorprocuracao_model', 'contadorprocuracao');
		$this->load->model('eprocessos_ativos_historico_model');
		$this->load->model('eprocessos_ativos_model');
		$this->load->model('eprocessos_inativos_historico_model');
		$this->load->model('eprocessos_inativos_model');


		date_default_timezone_set('America/Bahia');

		$cerficados = $this->certificado->get($banco);


		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library_procuracao
			 */
			$params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
				'cerficado_senha' => $cerficado->pass,
			);

			$this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);
			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */

			if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
				unset($this->ecac_robo_library_eprocessos_procuracao);
				continue;
			}

			$empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);

			//limpar tabela antes do laço
			$this->contadorprocuracao->clear_table($banco);

			foreach ($empresas_com_procuracao as $item){
				/**
				 * Função que altera o perfil no portal
				 */
				$validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
				if(! $validado){
					echo "CNPJ: {$item->cnpj} - sem procuração";

					$this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
					continue;
				}

				$cnpj = $item->cnpj;
				$ativos = $this->ecac_robo_library_eprocessos_procuracao->get_eprocessos_ativos();
				if ($ativos){
					$this->eprocessos_ativos_model->clear($cnpj, $banco);
					foreach ($ativos as $ativo){
						if( ! isset($ativo['id']) )
							continue;
						$this->eprocessos_ativos_model->insert($cnpj, $ativo, $banco);
						$this->eprocessos_ativos_historico_model->clear($ativo['id'], $banco);

						$historico_lista = $this->ecac_robo_library_eprocessos_procuracao->get_eprocesso_historico($ativo['numero']);
						foreach ($historico_lista as $historico){
							$this->eprocessos_ativos_historico_model->insert($historico, $ativo['id'], $banco);
						}
					}
				}

				$inativos = $this->ecac_robo_library_eprocessos_procuracao->get_eprocessos_inativos();
				if ($inativos){
					$this->eprocessos_inativos_model->clear($cnpj, $banco);
					foreach ($inativos as $inativo){
						if( ! isset($inativo['id']) )
							continue;
						$this->eprocessos_inativos_model->insert($cnpj, $inativo, $banco);
						$this->eprocessos_inativos_historico_model->clear($inativo['id'], $banco);

						$historico_lista = $this->ecac_robo_library_eprocessos_procuracao->get_eprocesso_historico($inativo['numero']);
						foreach ($historico_lista as $historico){
							$this->eprocessos_inativos_historico_model->insert($historico, $inativo['id'], $banco);
						}
					}
				}

			}
			unset($this->ecac_robo_library_eprocessos_procuracao);
		}
	}
}
