<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class PgDas_individual extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('das_model');
    }

	public function buscar_das_function(){
        $banco = $this->input->post('banco');
        $cnpj = $this->input->post('cnpj');
        $id_empresa = $this->das_model->find_empresa_by_cnpj($banco, $cnpj)->id;
        $cerficados = $this->das_model->get_aux($id_empresa, $banco);

        if(!empty($cerficados)){
            $this->buscar_das_procuracao($cnpj, $banco, $cerficados);
        }else{
            $this->buscar_das($cnpj, $banco);
        }
    }

    public function buscar_das($cnpj, $banco){

        $cerficado = $this->das_model->find_certificado($cnpj, $banco);
        $folder_pdf = FCPATH . 'pdf-das-ecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        date_default_timezone_set('America/Bahia');

        $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
            'cerficado_senha' => $cerficado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf,
        );
        $this->load->library('Ecac_robo_eprocessos_library', $params, 'ecac_robo_library');

        /**
         * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
         */
        if($this->ecac_robo_library->acesso_valido()){
            echo "Buscando dados da empresa: ".$this->ecac_robo_library->obter_numero_documento()."\n";
            $lista_das = $this->ecac_robo_library->get_das($this->ecac_robo_library->obter_numero_documento());
            if ($lista_das && count($lista_das) > 0)
                foreach ($lista_das as $dados){
                    $dados['cnpj'] = $this->ecac_robo_library->obter_numero_documento();
                    $existe_situacao = $this->das_model->verifica_se_existe($dados['numero_declaracao'], $banco, $dados['cnpj']);

                    $dire_declaracao = $dados['caminho_download_declaracao'];
                    if(!empty($dire_declaracao)){
                        $aux_dir_dec = str_replace("/var/www/html", "",$dire_declaracao);
                        $cami_aux_dec = "https://veri-sp.com.br".$aux_dir_dec;

                        $dados['caminho_download_declaracao'] = $cami_aux_dec;
                    }


                    $dire_extrato = $dados['caminho_download_extrato'];
                    if(!empty($dire_extrato)){
                        $aux_dir_ext = str_replace("/var/www/html", "",$dire_extrato);
                        $cami_aux_ext = "https://veri-sp.com.br".$aux_dir_ext;

                        $dados['caminho_download_extrato'] = $cami_aux_ext;
                    }

                    $dire_recibo = $dados['caminho_download_recibo'];
                    if(!empty($dire_recibo)){
                        $aux_dir_rec = str_replace("/var/www/html", "",$dire_recibo);
                        $cami_aux_rec = "https://veri-sp.com.br".$aux_dir_rec;

                        $dados['caminho_download_recibo'] = $cami_aux_rec;
                    }

                    if($existe_situacao->qtd > 0){
                        $this->das_model->update($dados, $banco);
                    }else{
                        $this->das_model->insert($dados, $banco);
                    }
                }
        }

    }

    public function buscar_das_procuracao($cnpj, $banco, $cerficados){
        $folder_pdf = FCPATH . 'pdf-das-ecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        date_default_timezone_set('America/Bahia');

        foreach ($cerficados as $cerficado){
            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);
            $array_cnpjs = array();
            foreach ($empresas_com_procuracao as $empresa){
                $array_cnpjs[]=$empresa->cnpj;
            }

            if ( ! in_array($cnpj, $array_cnpjs) )
                continue;

            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf,
            );

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library_procuracao');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if($this->ecac_robo_library_procuracao->acesso_valido()){
                echo "Buscando dados da empresa: ".$cnpj."\n";
                $lista_das = $this->ecac_robo_library_procuracao->get_das($cnpj);
                if ($lista_das && count($lista_das) > 0)
                    foreach ($lista_das as $dados){
                        $dados['cnpj'] = $cnpj;
                        $existe_situacao = $this->das_model->verifica_se_existe($dados['numero_declaracao'], $banco, $cnpj);

                        $dire_declaracao = $dados['caminho_download_declaracao'];
                        if(!empty($dire_declaracao)){
                            $aux_dir_dec = str_replace("/var/www/html", "",$dire_declaracao);
                            $cami_aux_dec = "https://veri-sp.com.br".$aux_dir_dec;

                            $dados['caminho_download_declaracao'] = $cami_aux_dec;
                        }


                        $dire_extrato = $dados['caminho_download_extrato'];
                        if(!empty($dire_extrato)){
                            $aux_dir_ext = str_replace("/var/www/html", "",$dire_extrato);
                            $cami_aux_ext = "https://veri-sp.com.br".$aux_dir_ext;

                            $dados['caminho_download_extrato'] = $cami_aux_ext;
                        }

                        $dire_recibo = $dados['caminho_download_recibo'];
                        if(!empty($dire_recibo)){
                            $aux_dir_rec = str_replace("/var/www/html", "",$dire_recibo);
                            $cami_aux_rec = "https://veri-sp.com.br".$aux_dir_rec;

                            $dados['caminho_download_recibo'] = $cami_aux_rec;
                        }

                        if($existe_situacao->qtd > 0){
                            $this->das_model->update($dados, $banco);
                        }else{
                            $this->das_model->insert($dados, $banco);
                        }
                    }

                // Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
                unset($this->ecac_robo_library_procuracao);
            }
        }
    }

}
