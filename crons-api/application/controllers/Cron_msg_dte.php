<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_msg_dte extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('caixadeemail_model');
    }

	public function sincronizar_teste() {

		$banco = $this->uri->segment(3);

		// $host = "207.246.115.103";
		// $user = "usuarioteste";
		// $pass = "12*databyte*21";
		// $db = $banco;

		$host ="35.247.210.57";
		$user = "user1*1outras*ma";
		$pass = "12*databyte*21";
		$db = $banco;

	// 	"hostname": "http://207.246.115.103/",
 // "usuario": "usuarioteste",
 // "senha": "12*databyte*21",
 // "banco": "testegeral"

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

		$query = ("SELECT * FROM dtb_plano_contratado where id = 1 LIMIT 1");
		$dados = mysqli_query($con, $query);

		while($dados_Login = mysqli_fetch_assoc($dados)){
			$login2 = $dados_Login['email_dte'];
			$senha2 = $dados_Login['senha_dte'];

			$servidordeemail = $dados_Login['servidor_email'];
			date_default_timezone_set('America/Bahia');

			if ($servidordeemail == 1) {

			$str_conexao = '{imap.gmail.com:993/imap/ssl}INBOX';

			} else if($servidordeemail == 2) {

				$str_conexao = '{imap.mail.yahoo.com:993/imap/ssl}INBOX';

			} else if($servidordeemail == 3) {

				$str_conexao = '{imap-mail.outlook.com:993/imap/ssl}INBOX';

			} else if($servidordeemail == 4) {

				$str_conexao = '{imap.bol.com.br:993/imap/ssl}INBOX';

			} else if($servidordeemail == 5) {

				$str_conexao = '{imap.uol.com.br:993/imap/ssl}INBOX';

			} else if($servidordeemail == 6) {

				$str_conexao = '{imap.merca.com.br:143/imap/ssl}INBOX';
			}


			if (!extension_loaded('imap')) {

			    die('Modulo PHP/IMAP nao foi carregado');
			}

			// Abrindo conexao

			$mailbox = imap_open($str_conexao, $login2, $senha2);

			if (!$mailbox) {
			    redirect('aviso/erro_autentiticar_email');
			}


			$numero_mensagens = imap_num_msg($mailbox);
			$numero_mens_nao_lidas = imap_num_recent($mailbox);
			  
			$result = imap_search($mailbox,'UNSEEN SUBJECT "Nova mensagem no DT-e"');
			if (is_array($result) || is_object($result)){
			    foreach($result as $msgno) {
			        $headers = imap_header($mailbox, $msgno);
			        
			        //$headers = imap_header($mailbox, $i);
			        $assunto            = $headers->subject;
			        $message_id         = $headers->message_id;
			        $toaddress          = $headers->toaddress;
			        $to                 = $headers->to;
			        $email_remetente    = $to[0]->mailbox;
			        $servidor_remetente = $to[0]->host;
			        $data               = $headers->date;
			        $data               = strtotime($data);
			        $data1              = date("d/m/Y H:i:s", $data);
			        # BODY
			        $body = imap_body($mailbox,$msgno); //echo "<pre>"; print_r($body); echo '</pre>'; die;

			        $overview = imap_fetch_overview($mailbox,$msgno,0);
	        		$structure = imap_fetchstructure($mailbox, $msgno); //echo "<pre>"; print_r($structure); echo '</pre>';
	        		# ENCODING
	        		$encoding = (int) $structure->encoding;
	        		if ($encoding == 0) { $body_decoded = $body; } # 7BIT
	        		else if ($encoding == 1) { $body_decoded = quoted_printable_decode(imap_8bit($body)); } # 8BIT
	        		else if ($encoding == 2) { $body_decoded = imap_binary($body); } # BINARY
	        		else if ($encoding == 3) { $body_decoded = imap_base64($body); } # BASE64 
	        		else if ($encoding == 4) { $body_decoded = quoted_printable_decode($body); } # QUOTED-PRINTABLE
	        		else if ($encoding == 5) { $body_decoded = $body; }  # OTHER
	        		else { $body_decoded = $body; } # UNKNOWN
			        #echo "<pre>body decodec\n"; print_r($body_decoded); echo '</pre>'; die;
					
			        # CAPTA CNPJ
			        //preg_match_all('/(\d{2}.\d{3}.\d{3}\/\d{4}\-\d{2})/', $body_decoded,$matchj);
	        		preg_match_all('/(\d{14})/', $body_decoded,$matchj);

			        $result = array_unique($matchj[0]);
			        foreach($result as $ht){

			   //      	$this->db->select('id, razao_social');
						// $this->db->where("cnpj_completo", $cnpj);
						// $this->db->group_by('id');
						// return $this->db->get($banco'.dtb_empresas')->row();

			        	#echo "CNPJ: ".$ht."<br>"; die;
			        	//$consulta = $this->caixadeemail_model->find_empresa_email($ht, $banco);

			        	$query_aux = ("SELECT id, razao_social, cnpj_completo FROM dtb_empresas where cnpj = '".$ht."' group by id LIMIT 1");
						$dados_aux = mysqli_query($con, $query_aux);

						$razao_socialBd = '';
				        $idEmpresa = '';

						while($dados_empresa = mysqli_fetch_assoc($dados_aux)){
							$razao_socialBd = $dados_empresa['razao_social'];
			        		$idEmpresa = $dados_empresa['id'];
			        		$ht = $dados_empresa['cnpj_completo'];
						}

				        $conteudomensagem = "Prezado(a) Senhor(a), há uma nova mensagem na conta Domicílio Tributário Eletrônico para o estabelecimento ".$ht;

				        $status = "1";
				        $id_contabilidade = 1;//$this->session->userdata['userprimesession']['id'];

				        $query_insert = ("INSERT INTO `dtb_caixadeemail`(`cnpj`, `id_empresa`, `razao_social`, `titulo`,`mensagem`, `status`, `id_contabilidade`, `remetente`, `dataemail`) VALUES ('$ht', '$idEmpresa', '$razao_socialBd', '$assunto', '$conteudomensagem', '$status', '$id_contabilidade', '$email_remetente', '$data1' );");
						$query_insert = str_replace("\n", " ",$query_insert);		
						mysqli_query($con, $query_insert);
				        
				       
				    }

			         //deleta o email lido
			        //imap_delete($mailbox, $msgno);
			    }
			}

		}
	}

}