<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Situacao_cadin_ecac_procuracao extends CI_Controller {

	function cron_pendencia_cadin_com_procuracao(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('situacao_cadin_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);
            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */

            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);

            //limpar tabela antes do laço
            $this->contadorprocuracao->clear_table($banco);

            foreach ($empresas_com_procuracao as $item){
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";

                    $this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
                    continue;
                }

                $path_pdf = $this->ecac_robo_library_eprocessos_procuracao->baixar_pdf_cadin();

                $pdf    =  new PdfToText() ;
                $carregado = false;
                try {
                    $pdf->Load( $path_pdf ) ;
                    $carregado = true;
                }catch (Exception $e){
                    $carregado = false;
                }

                if ( $carregado ){
                    $texto_base = "NÃO INCLUÍDO PELA RFB";
                    $pos = strpos($pdf->Text, $texto_base);

                    $texto_base2 = "EXCLUÍDO PELA RFB";
                    $pos2 = strpos($pdf->Text, $texto_base2);

                    $possui_pendencia =  true;

                    if ($pos !== false)
                        $possui_pendencia = false;

                    if ($pos2 !== false)
                        $possui_pendencia = false;

                    $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                    $caminho_download = "https://veri-sp.com.br".$caminho_aux;

                    $existe_situacao = $this->situacao_cadin_model->verifica_se_existe($this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                    if($existe_situacao->qtd > 0){
                        $this->situacao_cadin_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }else{
                        $this->situacao_cadin_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }
                }

            }

            unset($this->ecac_robo_library_eprocessos_procuracao);

        }

    }

    function cron_pendencia_cadin_com_procuracao_extra(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('situacao_cadin_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);
            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */

            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra($banco, $cerficado->id_contador);

            //limpar tabela antes do laço
            $this->contadorprocuracao->clear_table($banco);

            foreach ($empresas_com_procuracao as $item){
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";

                    $this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
                    continue;
                }

                $path_pdf = $this->ecac_robo_library_eprocessos_procuracao->baixar_pdf_cadin();

                $pdf    =  new PdfToText() ;
                $carregado = false;
                try {
                    $pdf->Load( $path_pdf ) ;
                    $carregado = true;
                }catch (Exception $e){
                    $carregado = false;
                }

                if ( $carregado ){
                    $texto_base = "NÃO INCLUÍDO PELA RFB";
                    $pos = strpos($pdf->Text, $texto_base);

                    $texto_base2 = "EXCLUÍDO PELA RFB";
                    $pos2 = strpos($pdf->Text, $texto_base2);

                    $possui_pendencia =  true;

                    if ($pos !== false)
                        $possui_pendencia = false;

                    if ($pos2 !== false)
                        $possui_pendencia = false;

                    $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                    $caminho_download = "https://veri-sp.com.br".$caminho_aux;

                    $existe_situacao = $this->situacao_cadin_model->verifica_se_existe($this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                    if($existe_situacao->qtd > 0){
                        $this->situacao_cadin_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }else{
                        $this->situacao_cadin_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }
                }

            }

            unset($this->ecac_robo_library_eprocessos_procuracao);

        }

    }

    function cron_pendencia_cadin_com_procuracao_extra_2(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('situacao_cadin_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);
            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */

            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_2($banco, $cerficado->id_contador);

            //limpar tabela antes do laço
            $this->contadorprocuracao->clear_table($banco);

            foreach ($empresas_com_procuracao as $item){
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";

                    $this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
                    continue;
                }

                $path_pdf = $this->ecac_robo_library_eprocessos_procuracao->baixar_pdf_cadin();

                $pdf    =  new PdfToText() ;
                $carregado = false;
                try {
                    $pdf->Load( $path_pdf ) ;
                    $carregado = true;
                }catch (Exception $e){
                    $carregado = false;
                }

                if ( $carregado ){
                    $texto_base = "NÃO INCLUÍDO PELA RFB";
                    $pos = strpos($pdf->Text, $texto_base);

                    $texto_base2 = "EXCLUÍDO PELA RFB";
                    $pos2 = strpos($pdf->Text, $texto_base2);

                    $possui_pendencia =  true;

                    if ($pos !== false)
                        $possui_pendencia = false;

                    if ($pos2 !== false)
                        $possui_pendencia = false;

                    $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                    $caminho_download = "https://veri-sp.com.br".$caminho_aux;

                    $existe_situacao = $this->situacao_cadin_model->verifica_se_existe($this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                    if($existe_situacao->qtd > 0){
                        $this->situacao_cadin_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }else{
                        $this->situacao_cadin_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }
                }

            }

            unset($this->ecac_robo_library_eprocessos_procuracao);

        }

    }

    function cron_pendencia_cadin_com_procuracao_extra_3(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('situacao_cadin_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);
            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */

            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_3($banco, $cerficado->id_contador);

            //limpar tabela antes do laço
            $this->contadorprocuracao->clear_table($banco);

            foreach ($empresas_com_procuracao as $item){
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";

                    $this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
                    continue;
                }

                $path_pdf = $this->ecac_robo_library_eprocessos_procuracao->baixar_pdf_cadin();

                $pdf    =  new PdfToText() ;
                $carregado = false;
                try {
                    $pdf->Load( $path_pdf ) ;
                    $carregado = true;
                }catch (Exception $e){
                    $carregado = false;
                }

                if ( $carregado ){
                    $texto_base = "NÃO INCLUÍDO PELA RFB";
                    $pos = strpos($pdf->Text, $texto_base);

                    $texto_base2 = "EXCLUÍDO PELA RFB";
                    $pos2 = strpos($pdf->Text, $texto_base2);

                    $possui_pendencia =  true;

                    if ($pos !== false)
                        $possui_pendencia = false;

                    if ($pos2 !== false)
                        $possui_pendencia = false;

                    $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                    $caminho_download = "https://veri-sp.com.br".$caminho_aux;

                    $existe_situacao = $this->situacao_cadin_model->verifica_se_existe($this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                    if($existe_situacao->qtd > 0){
                        $this->situacao_cadin_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }else{
                        $this->situacao_cadin_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }
                }

            }

            unset($this->ecac_robo_library_eprocessos_procuracao);

        }

    }

    function cron_pendencia_cadin_com_procuracao_extra_4(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('situacao_cadin_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);
            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */

            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_4($banco, $cerficado->id_contador);

            //limpar tabela antes do laço
            $this->contadorprocuracao->clear_table($banco);

            foreach ($empresas_com_procuracao as $item){
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";

                    $this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
                    continue;
                }

                $path_pdf = $this->ecac_robo_library_eprocessos_procuracao->baixar_pdf_cadin();

                $pdf    =  new PdfToText() ;
                $carregado = false;
                try {
                    $pdf->Load( $path_pdf ) ;
                    $carregado = true;
                }catch (Exception $e){
                    $carregado = false;
                }

                if ( $carregado ){
                    $texto_base = "NÃO INCLUÍDO PELA RFB";
                    $pos = strpos($pdf->Text, $texto_base);

                    $texto_base2 = "EXCLUÍDO PELA RFB";
                    $pos2 = strpos($pdf->Text, $texto_base2);

                    $possui_pendencia =  true;

                    if ($pos !== false)
                        $possui_pendencia = false;

                    if ($pos2 !== false)
                        $possui_pendencia = false;

                    $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                    $caminho_download = "https://veri-sp.com.br".$caminho_aux;

                    $existe_situacao = $this->situacao_cadin_model->verifica_se_existe($this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                    if($existe_situacao->qtd > 0){
                        $this->situacao_cadin_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }else{
                        $this->situacao_cadin_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                    }
                }

            }

            unset($this->ecac_robo_library_eprocessos_procuracao);

        }

    }



}