<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pgdas_notificacao extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Pgdas_notificacao/processar/base_ita2
        $banco = $this->uri->segment(3);
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('pgdas_notificacao_model');


        $pgdas_nao_entregues = $this->pgdas_notificacao_model->pgdas_nao_entregues_mes($banco);

        $data_atual = date('Y-m-d');

        $data_aux = date('Y-m-d', strtotime($data_atual.' - 1 months')); 

        $arr = explode("-", $data_aux);
        $mes_ = $arr[1];
        $ano_ = $arr[0];

        $mes_aux = $mes_;
        $ano_aux = $ano_;

        $ultimo_dia_transmitir = $this->getDiaUtil(15, $mes_aux+1, $ano_aux);

        $data_format = $ano_aux."-".$mes_aux."-".$ultimo_dia_transmitir;

        $data_aux = date('Y-m-d', strtotime($data_format.' + 1 months')); 

        $data_final = strtotime($data_aux);

        $df = new DateTime($data_aux);
        $da = new DateTime(date(''));
        $intervalo = $df->diff($da);

        $mes_filtro_aux = ltrim($mes_, "0");
        $ano_filtro_aux = $ano_;

        $data_filtro_aux = $mes_filtro_aux."/".$ano_filtro_aux;

        // $sem_movimento = $this->pgdas_notificacao_model->buscar_empresas_sem_movimento($banco, $data_filtro_aux);
        // $myhashmap = array();
        // foreach ($sem_movimento as $d) {
        //     $myhashmap[$d->cnpj] = $d;
        // }

        $data_mes = $mes_."/".$ano_;

        if ($data_final < strtotime(date('Y-m-d'))) {

            $this->pgdas_notificacao_model->clear_notificacoes_mes($banco, $data_mes);
            // $this->pgdas_notificacao_model->clear_notificacoes_proximo_vencer_mes($banco, $data_mes);

            foreach ($pgdas_nao_entregues as $e) {

                $this->pgdas_notificacao_model->insere_notificacao($e->cnpj, $data_mes, $banco);
               
            }

        }else{
            if($intervalo->format('%a') <= 3){
                $this->pgdas_notificacao_model->clear_notificacoes_proximo_vencer_mes($banco, $data_mes);

                foreach ($pgdas_nao_entregues as $e) {
                    $this->pgdas_notificacao_model->insere_notificacao_proximo_vencer($e->cnpj, $data_mes, $banco);
                }
            }
        }


        // $das_nao_gerados = $this->pgdas_notificacao_model->das_nao_gerado_mes($banco);
        // $this->pgdas_notificacao_model->clear_notificacoes_das_nao_gerado_mes($banco, $data_mes);
        // foreach ($das_nao_gerados as $e) {

        //     if(isset($myhashmap[$e->cnpj])){
        //         continue;
        //     }else{
        //         $this->pgdas_notificacao_model->insere_notificacao_das_nao_gerado($e->cnpj, $data_mes, $banco);
        //     }
            
        // }

        $das_nao_pagos = $this->pgdas_notificacao_model->das_nao_pagos_mes($banco);
        $this->pgdas_notificacao_model->clear_notificacoes_das_nao_pagos_mes($banco, $data_mes);

        foreach ($das_nao_pagos as $e) {

            $this->pgdas_notificacao_model->insere_notificacao_das_nao_pago($e->cnpj, $data_mes, $banco);
            
        }

    }
	

    public static function getDiaUtil($iDia, $iMes = null, $iAno = null, $aDiasIgnorar = array()) {
        date_default_timezone_set('America/Sao_Paulo');

        $iMes = empty($iMes) ? date('m') : $iMes;
        $iAno = empty($iAno) ? date('Y') : $iAno;
        $iUltimoDiaMes = date("t", mktime(0, 0, 0, $iMes, '01', $iAno));
 
        for ($i = 1; $i <= $iUltimoDiaMes; $i++) {
            $iDiaSemana = date('N', mktime(0, 0, 0, $iMes, $i, $iAno));
            //inclui apenas os dias úteis
            if ($iDiaSemana < 6) {
                $aDias[] = date('j', mktime(0, 0, 0, $iMes, $i, $iAno));
            }
        }
        //ignorando os feriados 
        if (sizeof($aDiasIgnorar) > 0) {
            foreach ($aDiasIgnorar as $iDia) {
                $iKey = array_search($iDia, $aDias);
                unset($aDias[$iKey]);
            }
        }
 
        if (isset($aDias[$iDia - 1])) {
            return $aDias[$iDia - 1];
        } else {
            //retorna o último dia útil
            return $aDias[count($aDias) - 1];
        }
    }

}
