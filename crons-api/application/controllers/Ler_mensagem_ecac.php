<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Ler_mensagem_ecac extends CI_Controller {

	public function __construct() {
        parent::__construct();
       	$this->load->model('mensagens_ecac_model');	 
    }

    public function ler_mensagem(){

		$caixa_postal_id = $this->uri->segment(3);
		$id_mensagem = $this->uri->segment(4);
		$banco = $this->uri->segment(5);

		$id_mensagem_caixa = $this->mensagens_ecac_model->find_mensagem($id_mensagem, $banco)->id_mensagem;
		$cnpj = $this->mensagens_ecac_model->find_cnpj($caixa_postal_id, $banco)->cnpj_data; 

        $mensagem = "";
		$certificado = $this->mensagens_ecac_model->find_certificado($cnpj, $banco);

        if( $certificado ){

            $params = array('caminho_certificado' => 'https://veri-sp.com.br/crons-api/'.str_replace('//','/', $certificado->caminho_arq ) ,
                'cerficado_senha' => $certificado->pass,
                'caminho_da_pasta_pdfs' => '');
            $this->load->library('Ecac_robo_library_dctf', $params);

            if(!$this->ecac_robo_library_dctf->acesso_valido()){
                unset($this->ecac_robo_library_dctf);
                echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
            }
            $mensagem = $this->ecac_robo_library_dctf->buscar_conteudo_mensagem($id_mensagem_caixa);

        }

		if($mensagem == ""){

			$this->load->model('certificadocontador_model');
	        $this->load->model('contadorprocuracao_model');

	        $cerficados = $this->certificadocontador_model->get($banco);

	        foreach ($cerficados as $cerficado_contador) {

	            $params = array('caminho_certificado' => 'https://veri-sp.com.br/crons-api/'.str_replace('//','/', $cerficado_contador->caminho_arq),
	                'cerficado_senha' => $cerficado_contador->pass,
	                'caminho_da_pasta_pdfs' => '');

	            $this->load->library('Ecac_robo_library_dctf', $params);

	            /**
	             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
	             */
	            if (!$this->ecac_robo_library_dctf->acesso_valido()) {
	                unset($this->ecac_robo_library_dctf);
	                continue;
	            }

	            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj, $banco);

	            if( $empresa ){
	                
	                $validado = $this->ecac_robo_library_dctf->trocar_perfil($cnpj);
	                
	                if(! $validado){
	                    echo "erro13";
	                    continue;
	                }

	                $mensagem = $this->ecac_robo_library_dctf->buscar_conteudo_mensagem($id_mensagem_caixa);

	                break;
	            }
	        }

			if($mensagem == ""){
				echo json_encode(array("error"=> true, "mensagem"=>'erro12'));
			}else{

				$this->mensagens_ecac_model->update_lida($id_mensagem, $mensagem, $banco);
				$this->mensagens_ecac_model->update_lida_caixa_postal($caixa_postal_id, $banco);
				echo json_encode(array("error"=> false, "mensagem"=>$mensagem));
			}

			//echo json_encode(array("error"=> true, "mensagem"=>'erro12'));
		}else{

			$this->mensagens_ecac_model->update_lida($id_mensagem, $mensagem, $banco);
			$this->mensagens_ecac_model->update_lida_caixa_postal($caixa_postal_id, $banco);
			echo json_encode(array("error"=> false, "mensagem"=>$mensagem));
		}

		unset($this->ecac_robo_library_dctf);

	}

	public function ler_mensagem_proc(){
		$caixa_postal_id = $this->uri->segment(3);
		$id_mensagem = $this->uri->segment(4);
		$banco = $this->uri->segment(5);

		$id_mensagem_caixa = $this->mensagens_ecac_model->find_mensagem($id_mensagem, $banco)->id_mensagem;
		$cnpj = $this->mensagens_ecac_model->find_cnpj($caixa_postal_id, $banco)->cnpj_data;

		$id_empresa = $this->mensagens_ecac_model->find_empresa_by_cnpj($cnpj, $banco)->id;
        $mensagem = "";
		
		//           Caso nao encontre certificado tenta por procuração

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        $cerficados = $this->certificadocontador_model->get_all($banco);
        	echo "asdasd";

        foreach ($cerficados as $cerficado_contador) {
        	echo "asdasd";
            $params = array('caminho_certificado' => 'https://veri-sp.com.br/crons-api/'.str_replace('//','/', $cerficado_contador->caminho_arq),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => '');

            $this->load->library('Ecac_robo_library_dctf', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library_dctf->acesso_valido()) {
                unset($this->ecac_robo_library_dctf);
                continue;
            }

            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj, $banco);

            // if( $empresa ){
                
                $validado = $this->ecac_robo_library_dctf->trocar_perfil($cnpj);
                
                if(! $validado){
                    echo "erro13";
                    unset($this->ecac_robo_library_dctf);
                    continue;
                }

                $mensagem = $this->ecac_robo_library_dctf->buscar_conteudo_mensagem($id_mensagem_caixa);

                if($mensagem != ""){
                	break;
                }
                
            // }
        }

		if($mensagem == ""){
			echo json_encode(array("error"=> true, "mensagem"=>'erro12'));
		}else{

			$this->mensagens_ecac_model->update_lida($id_mensagem, $mensagem, $banco);
			$this->mensagens_ecac_model->update_lida_caixa_postal($caixa_postal_id, $banco);
			echo json_encode(array("error"=> false, "mensagem"=>$mensagem));
		}

		unset($this->ecac_robo_library_dctf);

	}
}