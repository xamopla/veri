<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dec_consultar_mensagens extends CI_Controller {

    function chamar_api( $url ){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_URL, $url);
      $result=curl_exec($ch);
      curl_close($ch);
      return json_decode($result, true);
    }

    function cron_caixapostal_procuracao_disable(){
          $banco = $this->uri->segment(3);
          $this->load->model('certificadocontador_model', 'certificado');
          $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
          foreach ($cerficados as $cerficado){

              $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);
              foreach ($empresas_com_procuracao as $empresa){
                  if (is_null($empresa->cnpj))
                    continue;
                  $cnpj_procurador = $cerficado->cnpj_data;
                  $cnpj_buscar = $empresa->cnpj;
                  $url = "http://191.252.193.86/api/v1/dec-sp/procuracao-caixa-postal?cnpj_procurador={$cnpj_procurador}&cnpj_buscar={$cnpj_buscar}&banco={$banco}";
                  $resultado = $this->chamar_api($url);
                  if ( $resultado ){
                      $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                      $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                      if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                          $this->Dec_caixapostal_mensagens_model->clear($empresa->cnpj, $banco);
                          if ($mensagens_nao_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 0;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }

                          if ($mensagens_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 1;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }
                      }


                      $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                      $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                      if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                          $this->Dec_caixapostal_comunicados_model->clear($empresa->cnpj, $banco);

                          if ($comunicados_nao_lidas > 0){
                              $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 0;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }

                          }

                          if ($comunicados_lidos > 0){
                              $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 1;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }
                          }
                      }
                  }
              }
          }
    }

    function cron_caixapostal_procuracao_extra_disable(){
          $banco = $this->uri->segment(3);
          $this->load->model('certificadocontador_model', 'certificado');
          $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
          foreach ($cerficados as $cerficado){

              $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra($banco, $cerficado->id_contador);
              foreach ($empresas_com_procuracao as $empresa){
                  if (is_null($empresa->cnpj))
                    continue;
                  $cnpj_procurador = $cerficado->cnpj_data;
                  $cnpj_buscar = $empresa->cnpj;
                  $url = "http://191.252.193.86/api/v1/dec-sp/procuracao-caixa-postal?cnpj_procurador={$cnpj_procurador}&cnpj_buscar={$cnpj_buscar}&banco={$banco}";
                  $resultado = $this->chamar_api($url);
                  if ( $resultado ){
                      $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                      $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                      if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                          $this->Dec_caixapostal_mensagens_model->clear($empresa->cnpj, $banco);
                          if ($mensagens_nao_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 0;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }

                          if ($mensagens_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 1;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }
                      }


                      $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                      $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                      if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                          $this->Dec_caixapostal_comunicados_model->clear($empresa->cnpj, $banco);

                          if ($comunicados_nao_lidas > 0){
                              $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 0;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }

                          }

                          if ($comunicados_lidos > 0){
                              $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 1;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }
                          }
                      }
                  }
              }
          }
    }

    function cron_caixapostal_procuracao_extra_2_disable(){
          $banco = $this->uri->segment(3);
          $this->load->model('certificadocontador_model', 'certificado');
          $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
          foreach ($cerficados as $cerficado){

              $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_2($banco, $cerficado->id_contador);
              foreach ($empresas_com_procuracao as $empresa){
                  if (is_null($empresa->cnpj))
                    continue;
                  $cnpj_procurador = $cerficado->cnpj_data;
                  $cnpj_buscar = $empresa->cnpj;
                  $url = "http://191.252.193.86/api/v1/dec-sp/procuracao-caixa-postal?cnpj_procurador={$cnpj_procurador}&cnpj_buscar={$cnpj_buscar}&banco={$banco}";
                  $resultado = $this->chamar_api($url);
                  if ( $resultado ){
                      $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                      $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                      if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                          $this->Dec_caixapostal_mensagens_model->clear($empresa->cnpj, $banco);
                          if ($mensagens_nao_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 0;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }

                          if ($mensagens_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 1;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }
                      }


                      $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                      $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                      if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                          $this->Dec_caixapostal_comunicados_model->clear($empresa->cnpj, $banco);

                          if ($comunicados_nao_lidas > 0){
                              $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 0;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }

                          }

                          if ($comunicados_lidos > 0){
                              $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 1;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }
                          }
                      }
                  }
              }
          }
    }

    function cron_caixapostal_procuracao_extra_3_disable(){
          $banco = $this->uri->segment(3);
          $this->load->model('certificadocontador_model', 'certificado');
          $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
          foreach ($cerficados as $cerficado){

              $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_3($banco, $cerficado->id_contador);
              foreach ($empresas_com_procuracao as $empresa){
                  if (is_null($empresa->cnpj))
                    continue;
                  $cnpj_procurador = $cerficado->cnpj_data;
                  $cnpj_buscar = $empresa->cnpj;
                  $url = "http://191.252.193.86/api/v1/dec-sp/procuracao-caixa-postal?cnpj_procurador={$cnpj_procurador}&cnpj_buscar={$cnpj_buscar}&banco={$banco}";
                  $resultado = $this->chamar_api($url);
                  if ( $resultado ){
                      $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                      $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                      if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                          $this->Dec_caixapostal_mensagens_model->clear($empresa->cnpj, $banco);
                          if ($mensagens_nao_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 0;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }

                          if ($mensagens_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 1;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }
                      }


                      $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                      $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                      if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                          $this->Dec_caixapostal_comunicados_model->clear($empresa->cnpj, $banco);

                          if ($comunicados_nao_lidas > 0){
                              $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 0;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }

                          }

                          if ($comunicados_lidos > 0){
                              $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 1;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }
                          }
                      }
                  }
              }
          }
    }

    function cron_caixapostal_procuracao_extra_4_disable(){
          $banco = $this->uri->segment(3);
          $this->load->model('certificadocontador_model', 'certificado');
          $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
          foreach ($cerficados as $cerficado){

              $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_4($banco, $cerficado->id_contador);
              foreach ($empresas_com_procuracao as $empresa){
                  if (is_null($empresa->cnpj))
                    continue;
                  $cnpj_procurador = $cerficado->cnpj_data;
                  $cnpj_buscar = $empresa->cnpj;
                  $url = "http://191.252.193.86/api/v1/dec-sp/procuracao-caixa-postal?cnpj_procurador={$cnpj_procurador}&cnpj_buscar={$cnpj_buscar}&banco={$banco}";
                  $resultado = $this->chamar_api($url);
                  if ( $resultado ){
                      $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                      $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                      if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                          $this->Dec_caixapostal_mensagens_model->clear($empresa->cnpj, $banco);
                          if ($mensagens_nao_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 0;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }

                          if ($mensagens_lidas > 0){
                              $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                              foreach ($mensagens as $mensagem){
                                  $mensagem['cnpj'] = $empresa->cnpj;
                                  $mensagem['lida'] = 1;
                                  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                              }
                          }
                      }


                      $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                      $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                      if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                          $this->Dec_caixapostal_comunicados_model->clear($empresa->cnpj, $banco);

                          if ($comunicados_nao_lidas > 0){
                              $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 0;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }

                          }

                          if ($comunicados_lidos > 0){
                              $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                              foreach ($comunicados as $comunicado){
                                  $comunicado['cnpj'] = $empresa->cnpj;
                                  $comunicado['lida'] = 1;
                                  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                              }
                          }
                      }
                  }
              }
          }
    }

    function cron_caixapostal_disable(){
          $banco = $this->uri->segment(3);

          $this->load->model('certificado_model', 'certificado');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
   
          foreach ($cerficados as $cerficado){
             $url = "http://191.252.193.86/api/v1/dec-sp/caixa-postal?cnpj={$cerficado->cnpj_data}&banco={$banco}";
              $resultado = $this->chamar_api($url);
              if ( $resultado ){
                  $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                  $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                  if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                      $this->Dec_caixapostal_mensagens_model->clear($cerficado->cnpj_data, $banco);
                      if ($mensagens_nao_lidas > 0){
                          $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                          foreach ($mensagens as $mensagem){
                              $mensagem['cnpj'] = $cerficado->cnpj_data;
                              $mensagem['lida'] = 0;
                              $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                          }
                      }

                      if ($mensagens_lidas > 0){
                          $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                          foreach ($mensagens as $mensagem){
                              $mensagem['cnpj'] = $cerficado->cnpj_data;
                              $mensagem['lida'] = 1;
                              $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                          }
                      }
                  }


                  $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                  $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                  if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                      $this->Dec_caixapostal_comunicados_model->clear($cerficado->cnpj_data, $banco);

                      if ($comunicados_nao_lidas > 0){
                          $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                          foreach ($comunicados as $comunicado){
                              $comunicado['cnpj'] = $cerficado->cnpj_data;
                              $comunicado['lida'] = 0;
                              $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                          }

                      }

                      if ($comunicados_lidos > 0){
                          $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                          foreach ($comunicados as $comunicado){
                              $comunicado['cnpj'] = $cerficado->cnpj_data;
                              $comunicado['lida'] = 1;
                              $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                          }
                      }
                  }
              }
          }

      }


      // NOVA VERSÃO

      function cron_caixapostal_procuracao_individual(){

          $banco = $this->uri->segment(3);
          $cnpj_passado = $this->uri->segment(4);

          $this->load->model('certificadocontador_model', 'certificado');
          $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
          foreach ($cerficados as $cerficado){

            $cnpj_procurador = $cerficado->cnpj_data; 

            $url = "http://191.252.193.86/api/v1/dec-sp/procuracao-caixa-postal?cnpj_procurador={$cnpj_procurador}&cnpj_buscar={$cnpj_passado}&banco={$banco}";
            $resultado = $this->chamar_api($url);
            if ( $resultado ){
                $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                    $this->Dec_caixapostal_mensagens_model->clear($empresa->cnpj, $banco);
                    if ($mensagens_nao_lidas > 0){
                        $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                        foreach ($mensagens as $mensagem){
                            $mensagem['cnpj'] = $empresa->cnpj;
                            $mensagem['lida'] = 0;
                            $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                        }
                    }

                    if ($mensagens_lidas > 0){
                        $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                        foreach ($mensagens as $mensagem){
                            $mensagem['cnpj'] = $empresa->cnpj;
                            $mensagem['lida'] = 1;
                            $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                        }
                    }
                }


                $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                    $this->Dec_caixapostal_comunicados_model->clear($empresa->cnpj, $banco);

                    if ($comunicados_nao_lidas > 0){
                        $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                        foreach ($comunicados as $comunicado){
                            $comunicado['cnpj'] = $empresa->cnpj;
                            $comunicado['lida'] = 0;
                            $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                        }

                    }

                    if ($comunicados_lidos > 0){
                        $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                        foreach ($comunicados as $comunicado){
                            $comunicado['cnpj'] = $empresa->cnpj;
                            $comunicado['lida'] = 1;
                            $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                        }
                    }
                }
            } 
          }
    }

    public function ler_function(){
      $this->load->model('das_model');

      $banco = $this->input->post('banco');
      $cnpj = $this->input->post('cnpj');

      $id_empresa = $this->das_model->find_empresa_by_cnpj($banco, $cnpj)->id;
      $cerficados = $this->das_model->get_aux($id_empresa, $banco);
      $certificado_individual = $this->das_model->find_certificado($cnpj, $banco);

      if(!empty($certificado_individual)){        
        if($tipo == 1){
          echo $this->ler_mensagem($banco, $id, $cnpj);
        }else{
          echo $this->ler_comunicado($banco, $id, $cnpj);
        }
      }elseif(!empty($cerficados)){
        if($tipo == 1){
          echo $this->ler_mensagem_procuracao($banco, $id, $cerficados, $cnpj);
        }else{
          echo $this->ler_comunicado_procuracao($banco, $id, $cerficados, $cnpj);
        }
      }
      else{
        echo "erro";
      }
    }

    function cron_caixapostal_certificado_individual(){

          $banco = $this->uri->segment(3);
          $cnpj_passado = $this->uri->segment(4);

          $this->load->model('certificado_model', 'certificado');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->getCertificadoIndividual($banco,$cnpj_passado);
   
          foreach ($cerficados as $cerficado){
             $url = "http://191.252.193.86/api/v1/dec-sp/caixa-postal?cnpj={$cerficado->cnpj_data}&banco={$banco}";
              $resultado = $this->chamar_api($url);
              if ( $resultado ){
                  $mensagens_nao_lidas = $resultado['caixa_postal']['nao_lidas']['mensagens_nao_lidas'];
                  $mensagens_lidas = $resultado['caixa_postal']['lidas']['mensagens_lidas'];

                  if ($mensagens_nao_lidas > 0 || $mensagens_lidas > 0){
                      $this->Dec_caixapostal_mensagens_model->clear($cerficado->cnpj_data, $banco);
                      if ($mensagens_nao_lidas > 0){
                          $mensagens =  $resultado['caixa_postal']['nao_lidas']['mensagens'];
                          foreach ($mensagens as $mensagem){
                              $mensagem['cnpj'] = $cerficado->cnpj_data;
                              $mensagem['lida'] = 0;
                              $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                          }
                      }

                      if ($mensagens_lidas > 0){
                          $mensagens =  $resultado['caixa_postal']['lidas']['mensagens'];
                          foreach ($mensagens as $mensagem){
                              $mensagem['cnpj'] = $cerficado->cnpj_data;
                              $mensagem['lida'] = 1;
                              $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
                          }
                      }
                  }


                  $comunicados_nao_lidas =  $resultado['caixa_postal']['nao_lidas']['comunicados_nao_lidos'];
                  $comunicados_lidos =  $resultado['caixa_postal']['lidas']['comunicados_lidos'];

                  if ( $comunicados_nao_lidas > 0 || $comunicados_lidos > 0){
                      $this->Dec_caixapostal_comunicados_model->clear($cerficado->cnpj_data, $banco);

                      if ($comunicados_nao_lidas > 0){
                          $comunicados =  $resultado['caixa_postal']['nao_lidas']['comunicados'];
                          foreach ($comunicados as $comunicado){
                              $comunicado['cnpj'] = $cerficado->cnpj_data;
                              $comunicado['lida'] = 0;
                              $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                          }

                      }

                      if ($comunicados_lidos > 0){
                          $comunicados =  $resultado['caixa_postal']['lidas']['comunicados'];
                          foreach ($comunicados as $comunicado){
                              $comunicado['cnpj'] = $cerficado->cnpj_data;
                              $comunicado['lida'] = 1;
                              $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
                          }
                      }
                  }
              }
          }

      }
}
