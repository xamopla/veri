<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gfip_empresa extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Gfip_empresa/processar
        // $banco = $this->uri->segment(3);
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);
        $this->load->model('gfip_empresa_model');

        $registros = $this->gfip_empresa_model->busca_situacao_fiscal($banco);
        $pdf    =  new PdfToText() ;

        foreach ($registros as $e) {

            if(!empty($e->caminho_download)){
                try{

                    // $caminho_teste = 'https://veri-sp.com.br/crons-api/pdf-certidao-ecac/certidaoecac/osacontabilidade//situa%C3%A7%C3%A3o-fiscal-2021-05-25-19762724000109.pdf';
                    // $caminho_teste = 'https://veri-sp.com.br/crons-api/pdf-certidao-ecac/certidaoecac/onixcontabilidade//situa%C3%A7%C3%A3o-fiscal-2021-05-20-15777548000110.pdf';

                    $pdf->Load( $e->caminho_download ) ;

                    $cnpj = $e->cnpj;

                    $texto = $pdf -> Text ;

                    $p = explode("Pendência - ", $texto); 

                    $gfip_regular = true;
                    foreach ($p as $index) {

                        //verifica se existe pendencia de gfip
                        $pos_gfip = strpos($index, "Divergência GFIP x GPS");
                        if ($pos_gfip !== false) {
                            $this->extrair_gfip($index, $cnpj, $banco);
                            $gfip_regular = false;
                            break;
                        }
                    }

                    if($gfip_regular == true){

                        $existe_registro = $this->gfip_empresa_model->verifica_se_existe($banco, $cnpj);

                        if($existe_registro->qtd > 0){
                            $this->gfip_empresa_model->limpa_detalhe_gfip($banco, $cnpj);
                            $this->gfip_empresa_model->atualiza_gfip_regular($banco, $cnpj);
                        }else{
                            $this->gfip_empresa_model->insere_gfip_regular($banco, $cnpj); 
                        }

                    }else{
                        
                        $existe_registro = $this->gfip_empresa_model->verifica_se_existe($banco, $cnpj);

                        if($existe_registro->qtd > 0){
                            $this->gfip_empresa_model->atualiza_gfip_irregular($banco, $cnpj);
                        }else{
                            $this->gfip_empresa_model->insere_gfip_irregular($banco, $cnpj); 
                        }

                    }

                }catch(Exception $x){
                    echo "ERRO AO PROCESSAR CNPJ - ".$e->cnpj;
                    echo '<br>';
                    continue;
                }
                
            }
            
        }
        
    }


    public function extrair_gfip($string, $cnpj, $banco){

        $area_reduzida = explode("__________________________ Diagnóstico", $string); 

        //limpa tabela de detalhes gfip
        $this->gfip_empresa_model->limpa_detalhe_gfip($banco, $cnpj);

        //inicio das colunas de gfip
        $pos_inicio = strpos($area_reduzida[0], "RubricaValor");
        $pos_final = strpos($area_reduzida[0], " ");

        $inicio_gfip = substr($area_reduzida[0], $pos_inicio + 12);

        $outra_pagina = strpos($inicio_gfip, "Página");
        if ($outra_pagina !== false) {

            $paginas = explode("Página", $inicio_gfip); 

            foreach ($paginas as $pagina) {
                $this->extrai_pagina($pagina, $cnpj, $banco);
            }
        }else{
            $this->extrai_pagina($inicio_gfip, $cnpj, $banco);
        }
    }


    public function extrai_pagina($pagina, $cnpj, $banco){
        $primeira_pagina = $pagina;

        $primeira_pagina2 = str_replace("Outras Entidades", "OutrasEntidades", $primeira_pagina);
        
        $da = preg_split('/\s+/', $primeira_pagina2);

        foreach ($da as $d ) {

            $competencia = "";
            $fpas = "";
            $situacao = "";
            $rubrica_texto = "";
            $rubrica_texto_out = "";
            $valor = "";

            //existem diversos tipos e cada um muda a posição exata do texto
            if(strpos($d, "Previdência") !== false){
                $competencia = substr($d, 0, 7);
                $fpas = substr($d, 7, 3);
                $situacao = substr($d,10,3);
                $rubrica_texto = substr($d,13);
                $rubrica_texto_out = substr($rubrica_texto,0, 12);
                $valor = substr($d,25);
            }elseif(strpos($d, "OutrasEntidades") !== false){
                $competencia = substr($d, 0, 7);
                if(strpos($competencia, "/") !== false){
                    $competencia = substr($d, 0, 7);
                    $fpas = substr($d, 7, 3);
                    $situacao = substr($d,10,3);
                    // $rubrica_texto = substr($d,13);
                    // $rubrica_texto_out = substr($rubrica_texto,0, 15);
                    $rubrica_texto_out = "Outras Entidades";
                    $valor = substr($d,28);
                }else{
                    $competencia = "";
                    $fpas = substr($d, 0, 3);
                    $situacao = substr($d,3,3);
                    // $rubrica_texto = substr($d,6);
                    // $rubrica_texto_out = substr($rubrica_texto,0, 15);
                    $rubrica_texto_out = "Outras Entidades";
                    $valor = substr($d,21);
                }

            }
            

            if(!empty($fpas)){

                $this->gfip_empresa_model->insere_detalhe_gfip($banco, $cnpj, $competencia, $fpas, $situacao, $rubrica_texto_out, $valor);
               
            }

            
        }
    }

}
