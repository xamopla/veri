<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Pert_emitir_parcela_geral extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('pert_emissao_parcela_model');
        $this->load->model('certificado_model');
        $this->load->model('certificadocontador_model');
        $this->load->model('pert_model');

    }

	public function emitir_function(){
        $banco = $this->input->post('banco');
        $cnpj = $this->input->post('cnpj');
        $data_parcela = $this->input->post('data_parcela');

              
        $id_empresa = $this->pert_model->find_empresa_by_cnpj($banco, $cnpj)->id;
        $cerficados = $this->pert_model->get_aux($id_empresa, $banco);

        if(!empty($cerficados)){
            $retorno = $this->emitir_parcela_procuracao($data_parcela, $cnpj, $banco, $cerficados);
            echo $retorno;
        }else{
            $retorno = $this->emitir_parcela($data_parcela, $cnpj, $banco);
            echo $retorno;
        }
    }

   public function emitir_parcela_procuracao($data_parcela, $cnpj, $banco, $cerficados){

        $folder_pdf = FCPATH . 'pdf-parcela-pert-ecac/'.$banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        foreach ($cerficados as $cerficado_contador) {

            $params = array('caminho_certificado' => str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library->acesso_valido()) {
                unset($this->ecac_robo_library);
                continue;
            }

            $validado = $this->ecac_robo_library->trocar_perfil($cnpj);

            if(! $validado){
                echo "ERRO2";
                continue;
            }

            $caminho_download = $this->ecac_robo_library->gerar_parcela_pert($data_parcela);
            
            if ($caminho_download == 'ERROECAC')
                return 'ERROECAC';
        
            if($caminho_download != ""){
                $caminho_download = str_replace("/var/www/html", "https://veri-sp.com.br",$caminho_download);
                $this->pert_emissao_parcela_model->update_path($banco, $data_parcela, $cnpj, $caminho_download);
                unset($this->ecac_robo_library);
                return $caminho_download;
            }else{
                unset($this->ecac_robo_library);
                return "ERRO";
            }
        }
        unset($this->ecac_robo_library);
    }


    public function emitir_parcela($data_parcela, $cnpj, $banco){

        $certificado = $this->certificado_model->find_certificado($cnpj, $banco);
        $folder_pdf = FCPATH . 'pdf-parcela-pert-ecac/'.$banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $params = array('caminho_certificado' => str_replace('//','/', $certificado->caminho_arq ) ,
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf);
        $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

        if(!$this->ecac_robo_library->acesso_valido()){
            unset($this->ecac_robo_library);
            echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
        }
        $caminho_download = $this->ecac_robo_library->gerar_parcela_pert($data_parcela);

        if ($caminho_download == 'ERROECAC')
            return 'ERROECAC';

        if($caminho_download != ""){
            $caminho_download = str_replace("/var/www/html", "https://veri-sp.com.br",$caminho_download);
            $this->Pert_emissao_parcela_model->update_path($banco,$data_parcela, $cnpj, $caminho_download);

            unset($this->ecac_robo_library);
            return $caminho_download;
        }else{
            unset($this->ecac_robo_library);
            return "ERRO";
        }
    }

}
