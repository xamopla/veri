<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Situacao_cadin_ecac_certificado extends CI_Controller {

	function cron_pendencia_cadin(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificado_model', 'certificado');
        $this->load->model('situacao_cadin_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-cadin-ecac/cadinecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);
            $this->load->library('Ecac_robo_eprocessos_library', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
                unset($this->ecac_robo_eprocessos_library);
                continue;
            }

            $path_pdf = $this->ecac_robo_eprocessos_library->baixar_pdf_cadin();

            $carregado = false;
            $pdf	=  new PdfToText() ;

            try{
                $pdf->Load( $path_pdf ) ;
                $carregado = true;
            }catch (Exception $e){
                $carregado = false;
            }

            if ( $carregado ){
                $texto_base = "NÃO INCLUÍDO PELA RFB";
                $pos = strpos($pdf->Text, $texto_base);

                $texto_base2 = "EXCLUÍDO PELA RFB";
                $pos2 = strpos($pdf->Text, $texto_base2);

                $possui_pendencia =  true;

                if ($pos !== false)
                    $possui_pendencia = false;

                if ($pos2 !== false)
                    $possui_pendencia = false;
                    
                $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                $caminho_download = "https://veri-sp.com.br".$caminho_aux;

                $existe_situacao = $this->situacao_cadin_model->verifica_se_existe($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);

                if($existe_situacao->qtd > 0){
                    $this->situacao_cadin_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
                }else{
                    $this->situacao_cadin_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
                }
            }

            unset($this->ecac_robo_eprocessos_library);
        }

    }
}