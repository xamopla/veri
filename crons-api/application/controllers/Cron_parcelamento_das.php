<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_parcelamento_das extends CI_Controller {
	
    
    function cron_simplesnacional_pedidos_parcelamento(){
        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificado_model', 'certificado');
        $this->load->model('Simplesnacional_debitos_parcelas_model');
        $this->load->model('Simplesnacional_demonstrativo_pagamentos_model');
        $this->load->model('Simplesnacional_pedidos_parcelamentos_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);
        $folder_pdf = FCPATH . 'recibos-parcelamento-simplesnacional/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }
        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf,
            );
            $this->load->library('Ecac_robo_eprocessos_library', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library->acesso_valido()){
                unset($this->ecac_robo_library);
                continue;
            }

            $registros = $this->ecac_robo_library->get_simplesnacional_pedidos_parcelamentos();
            if ( $registros ){
                foreach ($registros as $registro){
                    
                    $registro['cnpj'] = $item->cnpj;

                    $existe_pedido = $this->Simplesnacional_pedidos_parcelamentos_model->verifica_se_existe($registro['cnpj'], $banco, $registro['numero']);

                    if($existe_pedido->qtd > 0){
                        $id_parcelamento = $existe_pedido->id;
                        $this->Simplesnacional_pedidos_parcelamentos_model->update( $registro['cnpj'], $banco, $registro['numero'],  $registro['situacao']);
                    }else{
                        if ($registro['situacao'] != 'Em Parcelamento')
                            continue;
                        $id_parcelamento = $this->Simplesnacional_pedidos_parcelamentos_model->insert($registro, $banco);
                    }


                    $existe_debitos_parcelas = $this->Simplesnacional_debitos_parcelas_model->verifica_se_existe($registro['cnpj'], $banco, $id_parcelamento);

                    if($existe_debitos_parcelas->qtd <= 0){
                        foreach ($registro['relacao_debitos_parcelas'] as $rdp){
                            // $this->Simplesnacional_debitos_parcelas_model->clear($registro['cnpj'], $banco);
                            $rdp['cnpj'] = $item->cnpj;
                            $rdp['id_parcelamento'] = $id_parcelamento;
                            $this->Simplesnacional_debitos_parcelas_model->insert($rdp, $banco);
                        }
                    }


                    foreach ($registro['demonstrativo_pagamentos'] as $dp){
                        $dp['cnpj'] = $item->cnpj;
                        $dp['id_parcelamento'] = $id_parcelamento;

                        $existe_pagamento = $this->Simplesnacional_demonstrativo_pagamentos_model->verifica_se_existe($dp['cnpj'], $banco, $id_parcelamento, $dp['mes_parcela']);
                        if($existe_pagamento->qtd <= 0){
                            $this->Simplesnacional_demonstrativo_pagamentos_model->insert($dp, $banco);
                        }
                        // $this->Simplesnacional_demonstrativo_pagamentos_model->clear($registro['cnpj'], $banco);
                    }
                        
                }
            }
            unset($this->ecac_robo_library);
        }
    }


}
