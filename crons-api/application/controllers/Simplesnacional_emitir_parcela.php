<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Simplesnacional_emitir_parcela extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Simplesnacional_emissao_parcela_model');
        $this->load->model('certificado_model');
        $this->load->model('empresa_model');
        $this->load->model('certificadocontador_model');

    }

    public function emitir(){
        $banco = $this->input->post('banco');
        $cnpj = $this->input->post('cnpj');
        $data_parcela = $this->input->post('data_parcela');

        $retorno = $this->emitir_parcela( $data_parcela,  $cnpj, $banco);
        echo $retorno;
    }

    public function emitir_parcela($data_parcela, $cnpj, $banco){

        $certificado = $this->certificado_model->find_certificado($cnpj, $banco);
        $folder_pdf = FCPATH . 'pdf-parcela-simplesnacional-ecac/'.$banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $params = array('caminho_certificado' => str_replace('//','/', $certificado->caminho_arq ) ,
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf);
        $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

        if(!$this->ecac_robo_library->acesso_valido()){
            unset($this->ecac_robo_library);
            echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
        }
        $caminho_download = $this->ecac_robo_library->gerar_parcela_simplesnacional($data_parcela);

        if($caminho_download != ""){

            $caminho_download = str_replace("/var/www/html", "https://veri.com.br",$caminho_download);

            $this->Simplesnacional_emissao_parcela_model->update_path($banco,$data_parcela, $cnpj, $caminho_download);

            unset($this->ecac_robo_library);
            return $caminho_download;
        }else{
            unset($this->ecac_robo_library);
            return "ERRO";
        }
    }

}
