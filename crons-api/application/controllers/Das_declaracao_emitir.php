<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Das_declaracao_emitir extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('das_model');
        $this->load->model('certificado_model');
        $this->load->model('certificadocontador_model');
        $this->load->model('das_model');
        $this->load->helper('googlestorage_helper');
    }

    public function emitir_function(){
            $banco = $this->input->post('banco');
            $numero_declaracao = $this->input->post('numero_declaracao');
            $das = $this->das_model->find_by_numero_declaracao($numero_declaracao, $banco);
            $competencia = $das->compentencia;
            $cnpj = $das->cnpj;

            $ano = trim(explode('/', $competencia)[1]);
            $id_empresa = $this->das_model->find_empresa_by_cnpj($banco, $cnpj)->id;
            $cerficados = $this->das_model->get_aux($id_empresa, $banco);

            if(!empty($cerficados)){
                $retorno = $this->emitir_declaracao_procuracao($numero_declaracao,  $ano, $cnpj, $banco, $cerficados);
                echo $retorno;
            }else{
                $retorno = $this->emitir_declaracao($numero_declaracao,  $ano, $cnpj, $banco);
                echo $retorno;
            }
        }

    public function emitir_declaracao_procuracao($numero_declaracao,  $ano, $cnpj, $banco, $cerficados){

        $folder_pdf = FCPATH . 'arquivos/pdf-das-ecac-ba/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        foreach ($cerficados as $cerficado_contador) {
				
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library->acesso_valido()) {
                unset($this->ecac_robo_library);
                continue;
            }

            $validado = $this->ecac_robo_library->trocar_perfil($cnpj);

            if(! $validado){
                echo "ERRO2";
                unset($this->ecac_robo_library);
                continue;
            }
            $caminho_download = $this->ecac_robo_library->baixar_declaracao_das($numero_declaracao, $ano);
            if($caminho_download != ""){
                $this->das_model->update_caminho_download_declaracao($caminho_download, $numero_declaracao, $banco);
                unset($this->ecac_robo_library);
                return $caminho_download;
            }else{
                unset($this->ecac_robo_library);
                return "ERRO";
            }
	    unset($this->ecac_robo_library);
        }
        
    }

    public function emitir_declaracao($numero_declaracao,  $ano, $cnpj, $banco){

        $certificado = $this->certificado_model->find_certificado($cnpj, $banco);
        $folder_pdf = FCPATH . 'arquivos/pdf-das-ecac-ba/'.$banco.'/';


        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $params = array('caminho_certificado' => str_replace('//','/', $certificado->caminho_arq ) ,
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf);
        $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

        if(!$this->ecac_robo_library->acesso_valido()){
            unset($this->ecac_robo_library);
            echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
        }
        $caminho_download = $this->ecac_robo_library->baixar_declaracao_das($numero_declaracao, $ano);

        if($caminho_download != ""){

            $caminho_download = str_replace("/var/www/html", "https://veri.com.br",$caminho_download);

            $this->das_model->update_caminho_download_declaracao($banco,$numero_declaracao, $cnpj, $caminho_download);

            unset($this->ecac_robo_library);
            return $caminho_download;
        }else{
            unset($this->ecac_robo_library);
            return "ERRO";
        }
    }
}
