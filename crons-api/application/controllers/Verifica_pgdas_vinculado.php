<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifica_pgdas_vinculado extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Verifica_pgdas_vinculado/processar

        $banco = $this->uri->segment(3);
        $this->load->model('verifica_pgdas_vinculado_model');

        $registros = $this->verifica_pgdas_vinculado_model->busca_empresas_pgdas($banco);

        
        foreach ($registros as $r) {
            try{

                $cnpj_base = substr($r->cnpj,0,8);
                $numero_declaracao = substr($r->numero_declaracao,0,8);

                if($numero_declaracao != $cnpj_base){
                    echo $r->cnpj;
                    echo "<br>";

                    $empresa = $this->verifica_pgdas_vinculado_model->busca_empresa_certa($banco, $numero_declaracao);

                    echo "cnpj-certo = ".$empresa->cnpj;
                    echo "<br>";

                    $this->verifica_pgdas_vinculado_model->atualiza_empresa_certa($banco, $r, $empresa->cnpj);
                }
            
            }catch(Exception $x){
                echo "ERRO AO PROCESSAR CNPJ - ".$r->cnpj;
                echo '<br>';
                continue;
            }
        }
    }

}
