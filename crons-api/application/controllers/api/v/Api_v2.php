<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Api_v2 extends CI_Controller {

    public function listar(){  
		
		// $this->load->view('layout/head_especial'); s
		$this->load->view('api/api_test3');
		// $this->load->view('layout/footer');
    }

    public function verifica_nome_banco(){
    	$nome_banco = $this->input->post('nome_do_banco');
    	$banco_minusculo = strtolower($nome_banco);

    	// $dbh = new PDO('mysql:host=localhost;', 'root', '12*bDV3r1sA0pAuL0*21', array(
     //        PDO::ATTR_PERSISTENT => true
     //    ));

    	try{
    		$dbh = new PDO('mysql:host=localhost;', 'root', '12*bDV3r1sA0pAuL0*21', array(
            PDO::ATTR_PERSISTENT => true
	        ));


	        $sql = $dbh->query("SELECT DISTINCT SCHEMA_NAME AS `database`
	                            FROM information_schema.SCHEMATA
	                            WHERE  SCHEMA_NAME NOT IN ('information_schema', 'performance_schema', 'mysql', 'phpmyadmin')
	                            ORDER BY SCHEMA_NAME");
	        $getAllDbs = $sql->fetchALL(PDO::FETCH_ASSOC);

	        $erro = false;
	        foreach ($getAllDbs as $DB) {  

	        	if($banco_minusculo == $DB['database']){
	        		$erro = true;
	        		break;
	        	}

	        };

	        if($erro == false){
	        	$array = array(
					'error'	=> false,
					'sucess' => true,
					'mensagem' => 'Base disponivel',
					'url' => $banco_minusculo.'.veri-sp.com.br'
				);
	        }else{
	        	$array = array(
					'error'	=> true,
					'sucess' => false,
					'mensagem' => 'Base indisponivel',
					'url' => ''
				);
	        }

	        echo json_encode($array);
    	}catch(Exception $e){
    		$array = array(
					'error'	=> true,
					'sucess' => false,
					'mensagem' => 'Erro de comunicação',
					'url' => ''
				);

    		echo json_encode($array);
    	}
        

    }


	public function criar_db(){

		$qtd_empresas = $this->input->post('qtd_empresas');

		$nome_do_plano = "";
		if($qtd_empresas <= 50){
			$nome_do_plano = "Bronze";
		}elseif($qtd_empresas > 50 && $qtd_empresas <= 200) {
			$nome_do_plano = "Prata";
		}elseif($qtd_empresas > 200) {
			$nome_do_plano = "Ouro";
		}

		$data_validade = date('Y-m-d H:i:s', strtotime('+2 year'));
		$banco = $this->input->post('nome_do_banco');

		//dados plano usuario
		$cnpj = $this->input->post('cnpj');
		$razao_social = $this->input->post('razao_social');
		$nome_fantasia = $this->input->post('nome_fantasia');
		$logradouro = $this->input->post('logradouro');
		$numero = $this->input->post('numero');
		$complemento = $this->input->post('complemento');
		$bairro = $this->input->post('bairro');
		$cidade = $this->input->post('cidade');
		$uf = $this->input->post('uf');
		$cep = $this->input->post('cep');
		$email = $this->input->post('email');
		$login = $this->input->post('login');

		if(!empty($banco) && !empty($email) && !empty($login)){
			
	        $dbh = new PDO('mysql:host=localhost;', 'root', '12*bDV3r1sA0pAuL0*21', array(
	            PDO::ATTR_PERSISTENT => true
	        ));

	        $sql = $dbh->query("SELECT DISTINCT SCHEMA_NAME AS `database`
	                            FROM information_schema.SCHEMATA
	                            WHERE  SCHEMA_NAME NOT IN ('information_schema', 'performance_schema', 'mysql', 'phpmyadmin')
	                            ORDER BY SCHEMA_NAME");
	        $getAllDbs = $sql->fetchALL(PDO::FETCH_ASSOC);

	        $erro = false;
	        foreach ($getAllDbs as $DB) {  

	        	if($banco == $DB['database']){
	        		$erro = true;
	        		break;
	        	}

	        };

	        if($erro == false){
	        	$banco_aux = "`".$banco."`";
	        	$dbh->query("CREATE DATABASE ".$banco_aux."; ");


				$erro2 = $this->script_criacao($dbh, $banco);

				// if($erro2 == false){
				// 	$array = array(
				// 		'error'	=> true,
				// 		'sucess' => false,
				// 		'mensagem' => 'Banco já existe',
				// 		'url' => ''
				// 	);

				// 	echo json_encode($array);
				// }

				$retorno_update_plano = $this->script_update_plano($dbh, $banco, $qtd_empresas, $nome_do_plano, $data_validade);
				$retorno_update_usuario = $this->script_update_usuario($dbh, $banco, $cnpj, $razao_social, $nome_fantasia, $logradouro, $numero, $complemento, $bairro, $cidade, $uf, $cep, $email, $login);

				$array = array(
					'error'	=> false,
					'sucess' => true,
					'mensagem' => 'Cliente cadastrado',
					'url' => $banco.'.veri-sp.com.br'
				);

				echo json_encode($array);
	        }else{
	        	$array = array(
					'error'	=> true,
					'sucess' => false,
					'mensagem' => 'Banco já existe',
					'url' => ''
				);

				echo json_encode($array);
	        }
		}

		else{
			$array = array(
					'error'	=> true,
					'sucess' => false,
					'mensagem' => 'Nome do banco , qtd de empresas, login e email são obrigatórios',
					'url' => ''
				);

			echo json_encode($array);
		}
		
        
	}


	function script_criacao($db, $banco){

		$db->query("USE `".$banco."`; ");

		$query = file_get_contents("script_atual_sp.sql");

		$stmt = $db->prepare($query);

		if ($stmt->execute()){
		     return false;
		}else{ 
		     return true;
		}

	}

	function script_update_plano($db, $banco, $qtd_empresas, $nome_do_plano, $data_validade){

		$sql = "USE `".$banco."`; ";

		$sql_plano = "UPDATE dtb_plano_contratado SET nome_do_plano = '$nome_do_plano', qtd_empresas=$qtd_empresas, validade='$data_validade' WHERE id = 1;";

		$sql_final = $sql.$sql_plano;
		$db->query($sql_final);

	}

	function script_update_usuario($db, $banco, $cnpj, $razao_social, $nome_fantasia, $logradouro, $numero, $complemento, $bairro, $cidade, $uf, $cep, $email, $login){

		$sql = "USE `".$banco."`; ";

		$sql_plano = "UPDATE dtb_usuarios SET cnpj = '$cnpj', razao_social= '$razao_social', nome_fantasia='$nome_fantasia', logradouro='$logradouro', numero='$numero', complemento='$complemento', bairro='$bairro', cidade='$cidade', uf='$uf', cep='$cep', email='$email', login='$login' WHERE id = 2;";

		$sql_final = $sql.$sql_plano;
		$db->query($sql_final);

	}
  
}