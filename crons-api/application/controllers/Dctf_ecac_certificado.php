<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dctf_ecac_certificado extends CI_Controller {

    function cron_dctf(){
            $banco = $this->uri->segment(3);

            $this->config->load('ecac_robo_config');
            $this->load->model('certificado_model', 'certificado');
            $this->load->model('dctf_model');

            date_default_timezone_set('America/Bahia');

            $cerficados = $this->certificado->get($banco);

            $dctf_declarados = $this->dctf_model->find_all_dctf($banco);
            $myhashmap = array();
            foreach ($dctf_declarados as $d) {
                $myhashmap[$d->cnpj."/".$d->periodo] = $d;
            }

            foreach ($cerficados as $cerficado){

                /**
                 * Carrega a library principal Ecac_robo_library
                 */
                $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                    'cerficado_senha' => $cerficado->pass,
                    'caminho_da_pasta_pdfs' => '');
                $this->load->library('Ecac_robo_eprocessos_library', $params);

                /**
                 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
                 */
                if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
                    unset($this->ecac_robo_eprocessos_library);
                    continue;
                }

                /**
                 * DCTF consulta
                 */
                // $this->dctf_model->clear($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
                $dctf = $this->ecac_robo_eprocessos_library->get_dctf($myhashmap);
                foreach ($dctf as $item) {
                    $this->dctf_model->insert($item, $banco);
                }

                unset($this->ecac_robo_eprocessos_library);
            }
        }

    function cron_dctf_declaracao_teste(){
        
        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificado_model', 'certificado');
        $this->load->model('dctf_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-dctf-ecac/declaracaodctf/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);
            $this->load->library('Ecac_robo_eprocessos_library', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_eprocessos_library->acesso_valido()){
                unset($this->ecac_robo_eprocessos_library);
                continue;
            }

            /**
             * DCTF consulta
             */
            $this->dctf_model->clear($this->ecac_robo_eprocessos_library->obter_numero_documento(), $banco);
            $dctf = $this->ecac_robo_eprocessos_library->get_dctf_declaracao();
            foreach ($dctf as $item) {
                $this->dctf_model->insert($item, $banco);
            }

            unset($this->ecac_robo_eprocessos_library);
        }
    }

}
