<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Limite_simples extends CI_Controller {

    public function processar(){
        //http://localhost/veri-6.0/Api-Crons/Limite_simples/processar
        // $banco = $this->uri->segment(3);
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);
        $this->load->model('limite_simples_model');

        $empresas = $this->limite_simples_model->busca_empresas_das($banco);
        $pdf    =  new PdfToText() ;
        foreach ($empresas as $e) {
            $extrato = $this->limite_simples_model->busca_das_ano_corrente($banco, $e->cnpj);
            if(!empty($extrato->caminho_download_recibo)){
                try{
                    $pdf->Load( $extrato->caminho_download_recibo ) ;

                    $texto = $pdf -> Text ;

                    $posicao_inicio = strpos($texto,"(RBA)");
                    $valor = substr($texto,$posicao_inicio+5);

                    $valor_aux = explode("Receita", $valor);
                    $valor_final = $valor_aux[0];

                    $valores = explode(",", $valor_final);
                    $total = $valores[2];


                    $valor_atual = substr($total,2).",".$valores[3];

                    $valorAtualBanco = str_replace(".","",$valor_atual);
                    $valorAtualBanco = str_replace(",",".",$valorAtualBanco);

                    $percentual = (int) ( ( ( (float) ($valorAtualBanco) ) / 3600000 ) * 100);

                    $existe_situacao = $this->limite_simples_model->verifica_se_existe($e->cnpj, $banco)->qtd;
                    if($existe_situacao > 0){
                        $this->limite_simples_model->update($e->cnpj, $valor_atual, $percentual, $banco, $extrato->caminho_download_recibo);
                    }else{
                        $this->limite_simples_model->insert($e->cnpj, $valor_atual, $percentual, $banco, $extrato->caminho_download_recibo);
                    }


                }catch(Exception $x){
                    echo $x->getMessage();
                    echo "ERRO AO PROCESSAR CNPJ - ".$e->cnpj;
                    echo '<br>';
                    continue;
                }
                
            }
            
        }
        
    }
}
