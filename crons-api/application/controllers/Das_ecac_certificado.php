<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Das_ecac_certificado extends CI_Controller {

	function cron_das(){
		
        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificado_model', 'certificado');
        $this->load->model('das_model');
        $folder_pdf = FCPATH . 'pdf-das-ecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf,
            );
            $this->load->library('Ecac_robo_eprocessos_library', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library->acesso_valido()){
                unset($this->ecac_robo_library);
                continue;
            }

            echo "Buscando dados da empresa: ".$this->ecac_robo_library->obter_numero_documento()."\n";
            $lista_das = $this->ecac_robo_library->get_das($this->ecac_robo_library->obter_numero_documento());
            if ($lista_das && count($lista_das) > 0)
                foreach ($lista_das as $dados){
                    $dados['cnpj'] = $this->ecac_robo_library->obter_numero_documento();
                    $existe_situacao = $this->das_model->verifica_se_existe($dados['numero_declaracao'], $banco, $dados['cnpj']);

                    $dire_declaracao = $dados['caminho_download_declaracao'];
                    if(!empty($dire_declaracao)){
                        $aux_dir_dec = str_replace("/var/www/html", "",$dire_declaracao);
                        $cami_aux_dec = "https://veri-sp.com.br".$aux_dir_dec;

                        $dados['caminho_download_declaracao'] = $cami_aux_dec;
                    }


                    $dire_extrato = $dados['caminho_download_extrato'];
                    if(!empty($dire_extrato)){
                        $aux_dir_ext = str_replace("/var/www/html", "",$dire_extrato);
                        $cami_aux_ext = "https://veri-sp.com.br".$aux_dir_ext;

                        $dados['caminho_download_extrato'] = $cami_aux_ext;
                    }

                    $dire_recibo = $dados['caminho_download_recibo'];
                    if(!empty($dire_recibo)){
                        $aux_dir_rec = str_replace("/var/www/html", "",$dire_recibo);
                        $cami_aux_rec = "https://veri-sp.com.br".$aux_dir_rec;

                        $dados['caminho_download_recibo'] = $cami_aux_rec;
                    }
                        
                    if($existe_situacao->qtd > 0){
                        $this->das_model->update($dados, $banco);
                    }else{
                        $this->das_model->insert($dados, $banco);
                    }
                }

            unset($this->ecac_robo_library);

        }

    }

    function cron_das_debitos(){

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificado_model', 'certificado');
        $this->load->model('das_debitos_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
            );
            $this->load->library('Ecac_robo_eprocessos_library', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library->acesso_valido()){
                unset($this->ecac_robo_library);
                continue;
            }

            $registros = $this->ecac_robo_library->get_das_debitos();
            if ( $registros ){
                $this->das_debitos_model->clear($this->ecac_robo_library->get_numero_documento(), $banco);
                foreach ($registros as $registro){
                    $registro['cnpj'] = $this->ecac_robo_library->get_numero_documento();
                    $this->das_debitos_model->insert($registro, $banco);
                }
            }

            unset($this->ecac_robo_library);

        }

    }
}