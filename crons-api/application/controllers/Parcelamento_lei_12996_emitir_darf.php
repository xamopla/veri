<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Parcelamento_lei_12996_emitir_darf extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Parcelamento_lei_12996_demonstrativo_prestacoes_model');
        $this->load->model('certificado_model');
        $this->load->model('certificadocontador_model');
    }

	public function emitir_function(){
        $banco = trim(strval($this->input->post('banco')));
        $id_parcela = trim(strval($this->input->post('id_parcela')));

        $buscar_dados = $this->Parcelamento_lei_12996_demonstrativo_prestacoes_model->buscar_dados_gerar_parcela($banco, $id_parcela);
       
        //**************************/
        $dados = array(
            'banco' => $banco,
            'cnpj' => $buscar_dados->cnpj,
            'strCPFCNPJ' => $buscar_dados->cnpj_formatado,
            'strCodReceita' => $buscar_dados->cod_receita,
            'strDataLimitePagamento' => $buscar_dados->proximo_dia_util,
            'strDataVencimentoImposto' => $buscar_dados->data_parcela,
            'strMunicipio' => $buscar_dados->municipio,
            'strNome' => $buscar_dados->nome_empresarial,
            'strPeriodoApuracao' => $buscar_dados->data_parcela,
            'strValorJuros' => ($buscar_dados->indicador_situacao_parcela == 'A') ? '' : $buscar_dados->juros_parc_deverdora,
            'strValorPrincipal' => ($buscar_dados->indicador_situacao_parcela == 'A') ? $buscar_dados->saldo_parc_devedora + $buscar_dados->juros_parc_deverdora : $buscar_dados->saldo_parc_devedora,
            'strValorTotal' => $buscar_dados->saldo_parc_devedora + $buscar_dados->juros_parc_deverdora,
        );
        
        $id_empresa = $this->Parcelamento_lei_12996_demonstrativo_prestacoes_model->find_empresa_by_cnpj($dados['banco'], $dados['cnpj'])->id;
        $cerficados = $this->Parcelamento_lei_12996_demonstrativo_prestacoes_model->get_aux($id_empresa, $dados['banco']);

        if(!empty($cerficados)){
            $retorno = $this->emitir_parcela_procuracao($dados, $cerficados);
            echo $retorno;
        }
        else{
           $retorno = $this->emitir_parcela($dados);

            echo $retorno;
        }
    }

    public function emitir_parcela_procuracao($dados, $cerficados)
    {
        $folder_pdf = FCPATH . 'pdf-parcelamento-lei-12996/'.$dados['banco'];

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        foreach ($cerficados as $cerficado_contador) {
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library->acesso_valido()) {
                unset($this->ecac_robo_library);
                continue;
            }

            $validado = $this->ecac_robo_library->trocar_perfil($dados['cnpj']);

            if(! $validado){
                echo "ERRO2";
                continue;
            }

            $caminho_download = $this->ecac_robo_library->gerar_parcela_lei_12996($dados);

            if ($caminho_download == 'ERROECAC')
                return 'ERROECAC';

            if($caminho_download != ""){
                $caminho_download = str_replace("/var/www/html", "https://veri-sp.com.br",$caminho_download);
                $this->Parcelamento_lei_12996_demonstrativo_prestacoes_model->update_path($dados, $caminho_download);
                unset($this->ecac_robo_library);
                return $caminho_download;
            }else{
                unset($this->ecac_robo_library);
                return "ERRO";
            }
        }
        unset($this->ecac_robo_library);
    }

    public function emitir_parcela($dados)
    {
        $certificado = $this->certificado_model->find_certificado($dados['cnpj'], $dados['banco']);
        $folder_pdf = FCPATH . 'pdf-parcelamento-lei-12996/'.$dados['banco'];

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $params = array('caminho_certificado' => str_replace('//','/', $certificado->caminho_arq ) ,
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf);
        $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

        if(!$this->ecac_robo_library->acesso_valido()){
            unset($this->ecac_robo_library);
            echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
        }
        $caminho_download = $this->ecac_robo_library->gerar_parcela_lei_12996($dados);

        if ($caminho_download == 'ERROECAC')
            return 'ERROECAC';

        if($caminho_download != ""){
            $caminho_download = str_replace("/var/www/html", "https://veri-sp.com.br",$caminho_download);
            $this->Parcelamento_lei_12996_demonstrativo_prestacoes_model->update_path($dados, $caminho_download);

            unset($this->ecac_robo_library);
            return $caminho_download;
        }else{
            unset($this->ecac_robo_library);
            return "ERRO";
        }
    }

}
