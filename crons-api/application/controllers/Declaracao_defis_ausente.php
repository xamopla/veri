<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Declaracao_defis_ausente extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Declaracao_defis_ausente/processar
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);
        $this->load->model('declaracao_ausente_model');
        $this->load->model('declaracao_ausente_defis_model');

        $registros = $this->declaracao_ausente_model->busca_situacao_fiscal($banco);
        $pdf    =  new PdfToText() ;

        foreach ($registros as $e) {

            if(!empty($e->caminho_download)){
                try{

                    $pdf->Load( $e->caminho_download ) ;

                    $cnpj = $e->cnpj;

                    $texto = $pdf -> Text ;

                    $p = explode("Pendência - ", $texto); 

                    $declaracao_regular = true;
                    foreach ($p as $index) {

                        //verifica se existe ausencia de declaração
                        $pos_declaracao = strpos($index, "Ausência de Declaração");
                        if ($pos_declaracao !== false) {

                            $this->extrair_declaracao($index, $cnpj, $banco);
                            $declaracao_regular = false;
                            break;
                        }
                    }

                    // break;

                    if($declaracao_regular){
                        $existe_registro = $this->declaracao_ausente_defis_model->verifica_se_existe($banco, $cnpj);

                        if($existe_registro->qtd > 0){
                            $this->declaracao_ausente_defis_model->atualiza_ausencia_defis_regular($banco, $cnpj);
                        }else{
                            $this->declaracao_ausente_defis_model->insere_ausencia_defis_regular($banco, $cnpj); 
                        }
                    }

                }catch(Exception $x){
                    echo "ERRO AO PROCESSAR CNPJ - ".$e->cnpj;
                    echo '<br>';
                    continue;
                }
                
            }
            
        }
        
    }


    public function extrair_declaracao($string, $cnpj, $banco){

        $area_total = $string;
        //Verifica se existe parcelamento no pdf
        $area_principal = explode("Parcelamento", $string);
        if(count($area_principal) > 1){
            $area_total = $area_principal[0];
        }

        $posicao_diagnostico = strpos($area_total, "__________________________ Diagnóstico");
        if ($posicao_diagnostico !== false) {

            $paginas = explode("__________________________ Diagnóstico", $area_total); 

            $this->extrai_defis($paginas[0], $cnpj, $banco);
        }else{
            $this->extrai_defis($area_total, $cnpj, $banco);
        }   

    }

    public function extrai_defis($string, $cnpj, $banco){

        $area_reduzida_defis = explode("DEFIS(Ano-Calendário)", $string); 

        $count_defis = count($area_reduzida_defis);
        if ($count_defis > 1) {

            echo "<br>";
            echo "Possui".$cnpj;
            //SUBSTITUI TODAS AS STRINGS DE DECLARAÇAO PARA PODER DAR UM EXPLODE
            $texto_a_utilizar = $area_reduzida_defis[1];

            $texto_a_utilizar = str_replace("EFD-CONTRIB", "break;", $texto_a_utilizar);
            $texto_a_utilizar = str_replace("GFIP", "break;", $texto_a_utilizar);
            $texto_a_utilizar = str_replace("ECF", "break;", $texto_a_utilizar);
            $texto_a_utilizar = str_replace("DASN", "break;", $texto_a_utilizar);
            $texto_a_utilizar = str_replace("DCTF", "break;", $texto_a_utilizar);
            $texto_a_utilizar = str_replace("DIRF", "break;", $texto_a_utilizar);
            // $texto_a_utilizar = str_replace("defis", "break;", $area_reduzida_defis[1]);
            $texto_a_utilizar = str_replace("PGDAS", "break;", $texto_a_utilizar);

            $texto_a_utilizar_final = explode("break;", $texto_a_utilizar);

            $existe_registro = $this->declaracao_ausente_defis_model->verifica_se_existe($banco, $cnpj);

            if($existe_registro->qtd > 0){
                $this->declaracao_ausente_defis_model->atualiza_ausencia_defis_irregular($banco, $cnpj);
            }else{
                $this->declaracao_ausente_defis_model->insere_ausencia_defis_irregular($banco, $cnpj); 
            }

            echo $texto_a_utilizar_final[0];


            // echo $gfip_sem_cnpj;
            // echo "<br>";
            // echo "<br>";
            
            $t = nl2br($texto_a_utilizar_final[0]);

            $paginas = explode("Página", $t);
            $count_paginas = count($paginas);
            if($count_paginas > 1){
                $t = $paginas[0];
            }

            $debito = explode("Débito", $t);
            $count_debito = count($debito);
            if($count_debito > 1){
                $t = $debito[0];
            }

            $defis = explode("<br />", $t);

            $periodo = $defis[0];
           
            $this->declaracao_ausente_defis_model->limpa_ausencia_defis_detalhe($banco, $cnpj);
            $this->declaracao_ausente_defis_model->insere_ausencia_defis_detalhe($banco, $cnpj, "DEFIS", "(Ano-Calendário)", $periodo);

        }else{

            $existe_registro = $this->declaracao_ausente_defis_model->verifica_se_existe($banco, $cnpj);

            if($existe_registro->qtd > 0){
                $this->declaracao_ausente_defis_model->atualiza_ausencia_defis_regular($banco, $cnpj);
            }else{
                $this->declaracao_ausente_defis_model->insere_ausencia_defis_regular($banco, $cnpj); 
            }
            echo "<br>";
            echo $cnpj." não possui";
        }
        
    }


    

}
