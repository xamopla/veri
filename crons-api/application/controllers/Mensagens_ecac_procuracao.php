<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensagens_ecac_procuracao extends CI_Controller {

	public function buscar_ecac_com_procuracao()
    {

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');

        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);

            foreach ($empresas_com_procuracao as $item){

                /**
                 * Função que altera o perfil no portal
                 */
                echo "==============CNPJ ATUAL - {$item->cnpj}==========\n";
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    continue;
                }

                /**
                 * Grava as mensagens da caixa postal
                 */
                $caixa_postal = $this->ecac_robo_library_eprocessos_procuracao->obter_mensagem_caixa_postal();
                if($caixa_postal)
                    $this->inserir_caixa_postal($caixa_postal, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                /**
                 * Grava as parcelas do simples nacional
                 */

                // $parcelas = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_emissao_parcela();
                // if($parcelas)
                //     $this->inserir_parcelas_emitidas($parcelas, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Grava se possui pedidos de parcelas ou não
                 */

                // $possui_pedidos = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_pedidos_parcela();
                // if($possui_pedidos)
                //     $this->inserir_consulta_pedidos($possui_pedidos, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Emite mensagem de sucesso e resumo da operação
                 */
                echo "==============SUCESSO NA OPERAÇÃO==========\n";
//                echo "Documento: {$this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento()}\n";
                // $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
                // echo "Situação Fiscal: {$mensagem_pendencia}\n";
                // echo "PDF situação: {$path_pdf}\n";
                echo "===========================================\n";
            }

//			Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
            unset($this->ecac_robo_library_eprocessos_procuracao);
        }

    }

	function inserir_caixa_postal($data, $cnpj_data, $banco){

		$this->load->model('caixa_postal_mensagem_model', 'caixa_postal_mensagem');
		$this->load->model('caixa_postal_model', 'caixa_postal');

		$result = $this->caixa_postal->existe_caixa_postal($cnpj_data, $banco);
		if($result->qtd > 0){
			$caixa_postal_id = $result->id;
			$this->caixa_postal->update($data, $cnpj_data, $caixa_postal_id, $banco);
		}else{
			$caixa_postal_id = $this->caixa_postal->insert($data, $cnpj_data, $banco);
		}

		$this->caixa_postal_mensagem->limpaTabelaMensagens($caixa_postal_id, $banco);

		foreach ($data['mensagens'] as $mensagem)
		{
			$mensagem['caixa_postal_id'] = $caixa_postal_id;
			$this->caixa_postal_mensagem->insert($mensagem, $banco);
		}
	}

	function inserir_parcelas_emitidas($parcelas, $cnpj_data, $banco){
		$this->load->model('emissao_parcela_model');
		$this->emissao_parcela_model->delete_parcelas($cnpj_data, $banco);
		foreach($parcelas as $parcela){
			$this->emissao_parcela_model->insert($parcela, $cnpj_data, $banco);
		}
	}

	function inserir_consulta_pedidos($possui_pedidos,  $cnpj_data, $banco){
		$this->load->model('pedidos_parcela_model');

		$this->pedidos_parcela_model->delete_consulta_pedido($cnpj_data, $banco);
		$this->pedidos_parcela_model->insert($possui_pedidos,  $cnpj_data, $banco);
	}

    // CRONS EXTRAS
    public function buscar_ecac_com_procuracao_extra(){

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');

        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra($banco, $cerficado->id_contador);

            foreach ($empresas_com_procuracao as $item){

                /**
                 * Função que altera o perfil no portal
                 */
                echo "==============CNPJ ATUAL - {$item->cnpj}==========\n";
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    continue;
                }

                /**
                 * Grava as mensagens da caixa postal
                 */
                $caixa_postal = $this->ecac_robo_library_eprocessos_procuracao->obter_mensagem_caixa_postal();
                if($caixa_postal)
                    $this->inserir_caixa_postal($caixa_postal, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                /**
                 * Grava as parcelas do simples nacional
                 */

                // $parcelas = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_emissao_parcela();
                // if($parcelas)
                //     $this->inserir_parcelas_emitidas($parcelas, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Grava se possui pedidos de parcelas ou não
                 */

                // $possui_pedidos = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_pedidos_parcela();
                // if($possui_pedidos)
                //     $this->inserir_consulta_pedidos($possui_pedidos, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Emite mensagem de sucesso e resumo da operação
                 */
                echo "==============SUCESSO NA OPERAÇÃO==========\n";
            // echo "Documento: {$this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento()}\n";
                // $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
                // echo "Situação Fiscal: {$mensagem_pendencia}\n";
                // echo "PDF situação: {$path_pdf}\n";
                echo "===========================================\n";
            }

            // Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
            unset($this->ecac_robo_library_eprocessos_procuracao);
        }

    }

    public function buscar_ecac_com_procuracao_extra_2(){

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');

        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_2($banco, $cerficado->id_contador);

            foreach ($empresas_com_procuracao as $item){

                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    continue;
                }

                /**
                 * Grava as mensagens da caixa postal
                 */
                $caixa_postal = $this->ecac_robo_library_eprocessos_procuracao->obter_mensagem_caixa_postal();
                if($caixa_postal)
                    $this->inserir_caixa_postal($caixa_postal, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                /**
                 * Grava as parcelas do simples nacional
                 */

                // $parcelas = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_emissao_parcela();
                // if($parcelas)
                //     $this->inserir_parcelas_emitidas($parcelas, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Grava se possui pedidos de parcelas ou não
                 */

                // $possui_pedidos = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_pedidos_parcela();
                // if($possui_pedidos)
                //     $this->inserir_consulta_pedidos($possui_pedidos, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Emite mensagem de sucesso e resumo da operação
                 */
                echo "==============SUCESSO NA OPERAÇÃO==========\n";
            // echo "Documento: {$this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento()}\n";
                // $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
                // echo "Situação Fiscal: {$mensagem_pendencia}\n";
                // echo "PDF situação: {$path_pdf}\n";
                echo "===========================================\n";
            }

            // Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
            unset($this->ecac_robo_library_eprocessos_procuracao);
        }

    }

    public function buscar_ecac_com_procuracao_extra_3(){

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');

        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_3($banco, $cerficado->id_contador);

            foreach ($empresas_com_procuracao as $item){

                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    continue;
                }

                /**
                 * Grava as mensagens da caixa postal
                 */
                $caixa_postal = $this->ecac_robo_library_eprocessos_procuracao->obter_mensagem_caixa_postal();
                if($caixa_postal)
                    $this->inserir_caixa_postal($caixa_postal, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                /**
                 * Grava as parcelas do simples nacional
                 */

                // $parcelas = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_emissao_parcela();
                // if($parcelas)
                //     $this->inserir_parcelas_emitidas($parcelas, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Grava se possui pedidos de parcelas ou não
                 */

                // $possui_pedidos = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_pedidos_parcela();
                // if($possui_pedidos)
                //     $this->inserir_consulta_pedidos($possui_pedidos, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Emite mensagem de sucesso e resumo da operação
                 */
                echo "==============SUCESSO NA OPERAÇÃO==========\n";
            // echo "Documento: {$this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento()}\n";
                // $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
                // echo "Situação Fiscal: {$mensagem_pendencia}\n";
                // echo "PDF situação: {$path_pdf}\n";
                echo "===========================================\n";
            }

            // Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
            unset($this->ecac_robo_library_eprocessos_procuracao);
        }

    }

    public function buscar_ecac_com_procuracao_extra_4(){

        $banco = $this->uri->segment(3);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');

        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library_eprocessos_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_eprocessos_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas_extra_4($banco, $cerficado->id_contador);

            foreach ($empresas_com_procuracao as $item){

                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_eprocessos_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    continue;
                }

                /**
                 * Grava as mensagens da caixa postal
                 */
                $caixa_postal = $this->ecac_robo_library_eprocessos_procuracao->obter_mensagem_caixa_postal();
                if($caixa_postal)
                    $this->inserir_caixa_postal($caixa_postal, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);

                /**
                 * Grava as parcelas do simples nacional
                 */

                // $parcelas = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_emissao_parcela();
                // if($parcelas)
                //     $this->inserir_parcelas_emitidas($parcelas, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Grava se possui pedidos de parcelas ou não
                 */

                // $possui_pedidos = $this->ecac_robo_library_eprocessos_procuracao->obter_simples_nacional_pedidos_parcela();
                // if($possui_pedidos)
                //     $this->inserir_consulta_pedidos($possui_pedidos, $this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento(), $banco);
                /**
                 * Emite mensagem de sucesso e resumo da operação
                 */
                echo "==============SUCESSO NA OPERAÇÃO==========\n";
            // echo "Documento: {$this->ecac_robo_library_eprocessos_procuracao->obter_numero_documento()}\n";
                // $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
                // echo "Situação Fiscal: {$mensagem_pendencia}\n";
                // echo "PDF situação: {$path_pdf}\n";
                echo "===========================================\n";
            }

            // Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
            unset($this->ecac_robo_library_eprocessos_procuracao);
        }

    }
}