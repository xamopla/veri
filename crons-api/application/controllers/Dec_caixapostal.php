<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dec_caixapostal extends CI_Controller {

    function cron_caixapostal(){
          $banco = $this->uri->segment(3);
          $this->load->model('certificadocontador_model', 'certificado');
          $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
          $this->load->model('Dec_caixapostal_mensagens_model');
          $this->load->model('Dec_caixapostal_comunicados_model');

          date_default_timezone_set('America/Bahia');

          $cerficados = $this->certificado->get($banco);
          foreach ($cerficados as $cerficado){
			  $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
				  'cerficado_senha' => $cerficado->pass,
			  );
			  $this->load->library('Dec_library', $params);
              $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);
              foreach ($empresas_com_procuracao as $empresa){
                  if (is_null($empresa->cnpj))
                    continue;
				  $cnpj = $empresa->cnpj;
				  $resultado = $this->dec_library->get_caixa_postal($cnpj);
				  if ( $resultado ){
					  $this->Dec_caixapostal_mensagens_model->clear($cnpj, $banco);
					  $mensagens =  $resultado['mensagens'];
					  foreach ($mensagens as $mensagem){
						  $mensagem['cnpj'] = $cnpj;
						  $this->Dec_caixapostal_mensagens_model->insert($mensagem, $banco);
					  }

					  $this->Dec_caixapostal_comunicados_model->clear($cnpj, $banco);
					  $comunicados =  $resultado['comunicados'];
					  foreach ($comunicados as $comunicado){
						  $comunicado['cnpj'] = $cnpj;
						  $this->Dec_caixapostal_comunicados_model->insert($comunicado, $banco);
					  }
				  }
              }
          }
    }

}
