<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regime_empresa extends CI_Controller {

    public function processar(){
        //http://localhost/veri-6.0/Api-Crons/Limite_simples/processar
        // $banco = $this->uri->segment(3);
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);
        $this->load->model('regime_empresa_model');

        $registros = $this->regime_empresa_model->busca_situacao_fiscal($banco);
        $pdf    =  new PdfToText() ;
        foreach ($registros as $e) {

            if(!empty($e->caminho_download)){
                try{
                    $pdf->Load( $e->caminho_download ) ;

                    $cnpj = $e->cnpj;

                    $texto = $pdf -> Text ;

                    $pos = strpos($texto, "Opção pelo Simples Nacional");
                    
                    $foi_excluido_simples = 0;
                    $foi_excluido_simei = 0;
                    $tipo_regime;   
                    if ($pos === false) {
                        $tipo_regime = "NORMAL";

                        $this->regime_empresa_model->update_normal($banco, $e->cnpj);

                    } else {
                        $tipo_regime = "SIMPLES NACIONAL";

                        $pos_exclusao_simples = strpos($texto, "InclusãoExclusão");

                        $pos_final = strpos($texto, "Sócios");

                        $texto_resumo = substr($texto, 0, $pos_final);

                        $texto_simples = substr($texto_resumo, $pos_exclusao_simples + 18, $pos_final);

                        $pos_simei = strpos($texto_simples, "InclusãoExclusão");
                        if ($pos_simei === false) {
                            
                            $t = nl2br($texto_simples);

                            $datas_simples = explode("/>", $t);

                            $qtd = count($datas_simples);

                            $ultimo_elemento = $datas_simples[$qtd - 2];
                            
                            if(strlen($ultimo_elemento) > 16){
                                $foi_excluido_simples = 1;

                                // echo "Empresa ".$e->cnpj." foi excluida do simples"."<br>";

                                $data_inicio = substr($ultimo_elemento, 0, 11);
                                $data_fim = substr($ultimo_elemento, 11, 20);

                                //atualiza optantes por simples
                                $existe_registro = $this->regime_empresa_model->existe_excluida_simples($banco, $e->cnpj);
                                if($existe_registro->qtd > 0){
                                    $this->regime_empresa_model->atualiza_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples);
                                }else{
                                   $this->regime_empresa_model->insere_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples); 
                                }

                                //atualiza tabela de empresas
                                $this->regime_empresa_model->update_normal($banco, $e->cnpj);

                            }else{
                                $foi_excluido_simples = 0;

                                $data_inicio = substr($ultimo_elemento, 0, 11);
                                $data_fim = null;

                                //atualiza optantes por simples
                                $existe_registro = $this->regime_empresa_model->existe_excluida_simples($banco, $e->cnpj);
                                if($existe_registro->qtd > 0){
                                    $this->regime_empresa_model->atualiza_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples);
                                }else{
                                   $this->regime_empresa_model->insere_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples); 
                                }

                                //atualiza tabela de empresas
                                $this->regime_empresa_model->update_simples($banco, $e->cnpj);
                                
                                
                            }


                        }else{

                            $posicao_inicio_simei = strpos($texto_simples, "Op");
                            $texto_simples2 = substr($texto_simples, 0, $posicao_inicio_simei);

                            //SIMPLES NACIONAL
                            $t = nl2br($texto_simples2);

                            $datas_simples = explode("/>", $t);

                            $qtd = count($datas_simples);

                            $ultimo_elemento = $datas_simples[$qtd - 2];

                            if(strlen($ultimo_elemento) > 16){
                                
                                $foi_excluido_simples = 1;

                                // echo "Empresa ".$e->cnpj." foi excluida do simples"."<br>";

                                $data_inicio = substr($ultimo_elemento, 0, 11);
                                $data_fim = substr($ultimo_elemento, 11, 20);

                                //atualiza optantes por simples
                                $existe_registro = $this->regime_empresa_model->existe_excluida_simples($banco, $e->cnpj);
                                if($existe_registro->qtd > 0){
                                    $this->regime_empresa_model->atualiza_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples);
                                }else{
                                   $this->regime_empresa_model->insere_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples); 
                                }

                                //atualiza tabela de empresas
                                $this->regime_empresa_model->update_normal($banco, $e->cnpj);

                            }else{
                                
                                $foi_excluido_simples = 0;

                                $data_inicio = substr($ultimo_elemento, 0, 11);
                                $data_fim = null;

                                //atualiza optantes por simples
                                $existe_registro = $this->regime_empresa_model->existe_excluida_simples($banco, $e->cnpj);
                                if($existe_registro->qtd > 0){
                                    $this->regime_empresa_model->atualiza_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples);
                                }else{
                                   $this->regime_empresa_model->insere_exclusao_simples($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simples); 
                                }

                                //atualiza tabela de empresas
                                $this->regime_empresa_model->update_simples($banco, $e->cnpj);
                            }

                            //FIM SIMPLES NACIONAL

                            //SIMEI
                            $texo_simei = substr($texto_simples, $pos_simei + 18);

                            $t2 = nl2br($texo_simei);
                            $datas_simei = explode("/>", $t2);

                            $qtd_simei = count($datas_simei);

                            $ultimo_elemento_simei = $datas_simei[$qtd_simei - 2];

                            if(strlen($ultimo_elemento_simei) > 16){
                                $foi_excluido_simei = 1;

                                $data_inicio = substr($ultimo_elemento_simei, 0, 11);
                                $data_fim = substr($ultimo_elemento_simei, 11, 20);

                                //atualiza optantes por mei
                                $existe_registro = $this->regime_empresa_model->existe_excluida_mei($banco, $e->cnpj);
                                if($existe_registro->qtd > 0){
                                    $this->regime_empresa_model->atualiza_exclusao_mei($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simei);
                                }else{
                                   $this->regime_empresa_model->insere_exclusao_mei($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simei); 
                                }

                            }else{
                                $foi_excluido_simei = 0;

                                $data_inicio = substr($ultimo_elemento_simei, 0, 11);
                                $data_fim = null;

                                $existe_registro = $this->regime_empresa_model->existe_excluida_mei($banco, $e->cnpj);
                                if($existe_registro->qtd > 0){
                                    $this->regime_empresa_model->atualiza_exclusao_mei($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simei);
                                }else{
                                   $this->regime_empresa_model->insere_exclusao_mei($data_inicio, $data_fim, $banco, $cnpj, $foi_excluido_simei); 
                                }

                            }
                            //FIM SIMEI
                            

                        }
                    }

                    // echo $e->cnpj."--".$tipo_regime;
                    // echo "<BR>";

                }catch(Exception $x){
                    echo "ERRO AO PROCESSAR CNPJ - ".$e->cnpj;
                    echo '<br>';
                    continue;
                }
                
            }
            
        }
        
    }
}
