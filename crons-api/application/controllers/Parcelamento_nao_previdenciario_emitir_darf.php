<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Parcelamento_nao_previdenciario_emitir_darf extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('parcelamento_nao_previdenciario_demonstrativo_das_parcelas_model');
        $this->load->model('certificado_model');
        $this->load->model('certificadocontador_model');
    }

	public function emitir_function(){
        $banco = trim(strval($this->input->post('banco')));
        $cnpj = trim(strval($this->input->post('cnpj')));
        $nome_tributo = trim(strval($this->input->post('nome_tributo')));
        $id_tributo = trim(strval($this->input->post('id_tributo')));
        $numero_parcela = trim(strval($this->input->post('numero_parcela')));
        $numero_processo = trim(strval($this->input->post('numero_processo')));
        $situacao = trim(strval($this->input->post('situacao')));
        $qtd_parcela = trim(strval($this->input->post('qtd_parcela')));
        $data_vencimento = trim(strval($this->input->post('data_vencimento')));
        $valor_parcela = trim(strval($this->input->post('valor_parcela')));

        $id_empresa = $this->parcelamento_nao_previdenciario_demonstrativo_das_parcelas_model->find_empresa_by_cnpj($banco, $cnpj)->id;
        $cerficados = $this->parcelamento_nao_previdenciario_demonstrativo_das_parcelas_model->get_aux($id_empresa, $banco);
        if(!empty($cerficados)){
            $retorno = $this->emitir_parcela_procuracao($numero_processo, $situacao, $nome_tributo, $qtd_parcela, $numero_parcela, $data_vencimento, $valor_parcela, $cnpj, $banco, $cerficados, $id_tributo);
            echo $retorno;
        }else{
           $retorno = $this->emitir_parcela($numero_processo, $situacao, $nome_tributo, $qtd_parcela, $numero_parcela, $data_vencimento, $valor_parcela, $cnpj, $banco, $id_tributo);

            echo $retorno;
        }
    }

    public function emitir_parcela_procuracao($numero_processo, $situacao, $nome_tributo, $qtd_parcela, $numero_parcela, $data_vencimento, $valor_parcela, $cnpj, $banco, $cerficados, $id_tributo){

        $folder_pdf = FCPATH . 'pdf-parcelamento-nao-previdenciario/'.$banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        foreach ($cerficados as $cerficado_contador) {
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library->acesso_valido()) {
                unset($this->ecac_robo_library);
                continue;
            }

            $validado = $this->ecac_robo_library->trocar_perfil($cnpj);

            if(! $validado){
                echo "ERRO2";
                continue;
            }

            $caminho_download = $this->ecac_robo_library->gerar_parcela_nao_previdenciario($numero_processo, $situacao, $nome_tributo, $qtd_parcela, $numero_parcela, $data_vencimento, $valor_parcela);

            if ($caminho_download == 'ERROECAC')
                return 'ERROECAC';

            if($caminho_download != ""){
                $caminho_download = str_replace("/var/www/html", "https://veri-sp.com.br",$caminho_download);
                $this->parcelamento_nao_previdenciario_demonstrativo_das_parcelas_model->update_path($banco, $numero_parcela, $cnpj, $id_tributo, $caminho_download);
                unset($this->ecac_robo_library);
                return $caminho_download;
            }else{
                unset($this->ecac_robo_library);
                return "ERRO";
            }
        }
        unset($this->ecac_robo_library);
    }


    public function emitir_parcela($numero_processo, $situacao, $nome_tributo, $qtd_parcela, $numero_parcela, $data_vencimento, $valor_parcela, $cnpj, $banco, $id_tributo){

        $certificado = $this->certificado_model->find_certificado($cnpj, $banco);
        $folder_pdf = FCPATH . 'pdf-parcelamento-nao-previdenciario/'.$banco;

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $params = array('caminho_certificado' => str_replace('//','/', $certificado->caminho_arq ) ,
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf);
        $this->load->library('Ecac_robo_library_eprocessos_procuracao', $params, 'ecac_robo_library');

        if(!$this->ecac_robo_library->acesso_valido()){
            unset($this->ecac_robo_library);
            echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
        }
        $caminho_download = $this->ecac_robo_library->gerar_parcela_nao_previdenciario($numero_processo, $situacao, $nome_tributo, $qtd_parcela, $numero_parcela, $data_vencimento, $valor_parcela);

        if ($caminho_download == 'ERROECAC')
            return 'ERROECAC';

        if($caminho_download != ""){
            $caminho_download = str_replace("/var/www/html", "https://veri-sp.com.br",$caminho_download);
            $this->parcelamento_nao_previdenciario_demonstrativo_das_parcelas_model->update_path($banco, $numero_parcela, $cnpj, $id_tributo, $caminho_download);

            unset($this->ecac_robo_library);
            return $caminho_download;
        }else{
            unset($this->ecac_robo_library);
            return "ERRO";
        }
    }

}
