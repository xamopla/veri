<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_cnd_luis_eduardo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cnd_luizeduardo_model');

    }

	public function buscar(){

		$banco = $this->uri->segment(3);
        
        $host ="localhost";
        $user = "root";
        $pass = "12*Gr4p0D4t4ByT3SySV3r1*21";

		$db = $banco;

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

		$query = ("SELECT id, cnpj, cnpj_completo, inscricao_estadual,inscricao_estadual_completo, razao_social FROM dtb_empresas where cidade = 'LUIS EDUARDO MAGALHAES' ");
		$consulta = mysqli_query($con, $query);

		while($dados = mysqli_fetch_assoc($consulta)){
            $cnpj = $dados['cnpj'];
            $cnpj_completo = $dados['cnpj_completo'];
            $inscricao_estadual = $dados['inscricao_estadual'];
            $inscricao_estadual_completo = $dados['inscricao_estadual_completo'];
            $razao_social = $dados['razao_social'];

            $folder_pdf = FCPATH . 'pdf-certidao/pdf-certidao-luizeduardo/'.$banco.'/';

            if (!file_exists($folder_pdf)) {
                mkdir($folder_pdf, DIR_WRITE_MODE, true);
            }

            $params = array('cnpj' => $cnpj,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Cnd_luiz_eduardo_library', $params);
            $possui_impedimento_na_emissao = $this->cnd_luiz_eduardo_library->impedimento_na_emissao();

            $caminho_download = '';
            $status = $possui_impedimento_na_emissao ? 'Irregular' : 'Regular';

            if(!$possui_impedimento_na_emissao){
                $path_pdf = $this->cnd_luiz_eduardo_library->baixar_pdf();
                $caminho_aux = str_replace("/var/www/html", "",$path_pdf);
                $caminho_download = "http://veri.com.br".$caminho_aux;
            }

            $existe_certidao = $this->cnd_luizeduardo_model->verifica_se_existe($cnpj, $banco);

            if($existe_certidao->qtd > 0){
                $this->cnd_luizeduardo_model->update($status, $caminho_download, $cnpj, $cnpj_completo, $inscricao_estadual, $inscricao_estadual_completo, $razao_social, $banco);
            }else{
                $this->cnd_luizeduardo_model->insert($status, $caminho_download, $cnpj, $cnpj_completo, $inscricao_estadual, $inscricao_estadual_completo, $razao_social, $banco);
            }

            unset($this->cnd_luiz_eduardo_library);
		}
	}
}