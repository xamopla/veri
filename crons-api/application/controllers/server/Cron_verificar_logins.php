<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_verificar_logins extends CI_Controller {

	public function buscar(){

		// error_reporting(0);

		$banco = $this->uri->segment(3);

		$host ="207.246.115.230";
		$user = "user1*1outras*ma";
		$pass = "12*databyte*21";
		$db = $banco;

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

		date_default_timezone_set('America/Bahia'); 

		header('Content-Type: application/json; charset=utf-8');

		$apaga_tabela_query = ("TRUNCATE TABLE dtb_logins_validos");

		$apaga_tabela_resposta = mysqli_query($con, $apaga_tabela_query);

		$query = ("SELECT id,login_sefaz,senha_sefaz FROM dtb_empresas WHERE flag_empresa_sem_ie is null");
		$dados = mysqli_query($con, $query);

		$data_verificacao = date('Y-m-d H:i:s');

		while($dados_Login = mysqli_fetch_assoc($dados)){

			//RECEBENDO OS DADOS VIA GET
			$login = $dados_Login['login_sefaz'];
			$senha = $dados_Login['senha_sefaz'];
			$id_empresa = $dados_Login['id'];

			//CRIANDO OS DADOS DO REQUEST COM O LOGIN FORNECIDO
			$data = array('ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin', 
			'__VIEWSTATEGENERATOR'=>'CC7A3876',
			'__PREVIOUSPAGE'=>'MEFqmrRFdAFxiPWkBvrml05rnIRe8jJVKDh1wj89FKs1G2Rek4IryjTaHlC-rNd-2HtkAqhiSUIKkE54qPVkuVoJhSkLx4ZYDPUwBFKncms1',
			'__EVENTVALIDATION'=>'/wEdAApjJ4ohBpDDzCIiP8wjuq6hIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniVoFUXmUm1skx5kv6KTgEBVucq+Nk=',
			'__VIEWSTATE'=>'/wEPDwUKMTYwMTM5MTE2OA9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFA0RURWQCBQ8WAh8ABS5Eb21pYyYjMjM3O2xpbyBUcmlidXQmIzIyNTtyaW8gRWxldHImIzI0NDtuaWNvZAIJD2QWAmYPZBYCAgEPZBYGAgMPDxYIHghDc3NDbGFzcwUKbW9kYWwgZmFkZR4wX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUc8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgRXJybx4yX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHgRfIVNCAgIWDB4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljHglkcmFnZ2FibGUFBHRydWVkAgUPZBYGAgcPFgIfAGVkAgsPDxYCHgdWaXNpYmxlZ2RkAg0PDxYCHwtnZGQCBw8WAh8ABf0HPHRhYmxlPg0KDQo8dHI+DQoNCiAgICA8dGQ+DQogICAgDQogICAgICAgIDxkaXYgY2xhc3M9ImNvbDEgc3BhbjUiPg0KICAgICAgICAgICAgPGltZyBzcmM9Ii9kdGUvZHRfZV9maW5hbC5wbmciIGFsdD0iTG9nbyBEVEUiLz4NCiAgICAgICAgICAgIDxiciAvPjxiciAvPjxiciAvPg0KICAgICAgICA8L2Rpdj4NCiAgICA8L3RkPg0KPC90cj4NCjx0cj4NCiAgICA8dGQ+DQogICANCiAgICAgIDxkaXY+DQoNCiAgICAgICAgPHA+RmVycmFtZW50YSBleHRlcm5hIHBhcmEgZ2VyZW5jaWFtZW50byBkYSBDb250YSBubyBEb21pY8OtbGlvIFRyaWJ1dMOhcmlvIEVsZXRyw7RuaWNvIC0gRFRFPC9wPg0KICAgIA0KICAgICAgICA8aDE+DQogICAgDQogICAgICAgIDwvaDE+DQogICAgICAgIDxoMj5JbmZvcm1hw6fDtWVzLjwvaDI+DQogICAgDQogICAgICAgIDxwPg0KICAgICAgICAgICAgUHJvamV0YWRvIGNvbSBhIGlkZWlhIGRlIGRlc2Vudm9sdmVyIGRlIGZvcm1hIG1haXMgaW50dWl0aXZhLCBlZmljaWVudGUsIMO6dGlsIGUgc2ltcGxlcy4gDQogICAgICAgICAgICA8YnIgLz4NCiAgICAgICAgICAgIENvbSBhIGZlcnJhbWVudGEgcG9kZXLDoSBjYWRhc3RyYXIgdW1hIENvbnRhIERvbWljw61saW8gVHJpYnV0w6FyaW8sIEVuZGVyZcOnb3MgRWxldHLDtG5pY29zLCBUZWxlZm9uZXMgQ2VsdWxhcmVzLCBMZXIgTWVuc2FnZW5zIFJlY2ViaWRhcyBlIFZpc3VhbGl6YXIgUmVsYXTDs3Jpb3MuDQogICAgICAgIDwvcD4NCiAgICAgICAgPHVsPg0KICAgICAgICAgICAgPGxpPg0KICAgICAgICAgICAgICAgIDxwPkFjZXNzZSBhIHBhcnRpciBkZSBxdWFscXVlciBkaXNwb3NpdGl2byBtw7N2ZWwsIG7Do28gc2UgcHJlb2N1cGUgY29tIGEgcmVzb2x1w6fDo28sIGFqdXN0YW1vcyBhIHRlbGEgcGFyYSBhIG1lbGhvciB2aXN1YWxpemHDp8Ojby48L3A+DQogICAgICAgICAgICA8L2xpPiAgICANCiAgICAgICAgPC91bD4NCg0KICAgICAgICA8L2Rpdj4NCg0KICAgIDwvdGQ+DQo8L3RyPg0KDQo8L3RhYmxlPmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFCm1vZGFsIGZhZGUfBAICFgwfBQUCLTEfBgUGZGlhbG9nHwcFBHRydWUfCAUFZmFsc2UfCQUGc3RhdGljHwoFBHRydWVkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFD0FTTElCOiAyLjEuMTAuMGRkEzytXcLa4YyfkajFBRKCVE2wQGc=',
			'ctl00$PHCentro$btnLogin'=>'Entrar',

			'ctl00$PHCentro$userLogin'=>$login,
			'ctl00$PHCentro$userPass'=>$senha
			);

			//FAZENDO A REQUISIÇÃO USANDO CURL
			$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);
			

			$pos = strpos($store, "Resumo Fiscal");
			if($pos != false){
				//LOGIN FOI REALIZADO COM SUCESSO
				//VOU CHAMAR O ARQUIVO PARA INSERIR O LOGIN NO BANCO
				$status_da_senha = "OK";
				$this->salvarLogin($banco, $id_empresa, $login, $senha, $status_da_senha, $data_verificacao);
				
				$resposta = array("status"=>"Logado","user"=>$login,"senha"=>$senha,"id_empresa"=>$id_empresa,"data_verificacao"=>$data_verificacao,"status_da_senha"=>$status_da_senha);
				echo json_encode($resposta);
			}else{
				//COMO O LOGIN FOI NEGADO, IREMOS FAZER UM REQUEST COM O LOGIN CORRETO PARA O CONTADOR NO SITE DA SEFAZ SE RESETAR
				$status_da_senha = "ERRO";
				$this->salvarLogin($banco, $id_empresa, $login, $senha, $status_da_senha, $data_verificacao);
				
				$resposta = array("status"=>"Incorreto","user"=>$login,"senha"=>$senha,"id_empresa"=>$id_empresa,"data_verificacao"=>$data_verificacao,"status_da_senha"=>$status_da_senha);
				echo json_encode($resposta);	
				$this->requestLoginCorreto();
			}
		}

	} 

	public function requestLoginCorreto(){

		//SETANDO O LOGIN MASTER
		$loginMaster = "10964257600";
		$senhaMaster = "exa2107";

		//CRIANDO OS DADOS DO REQUEST COM O LOGIN FORNECIDO
		$data = array('ctl00$ScriptManager2'=>'ctl00$updPanel|ctl00$PHCentro$btnLogin', 
		'__VIEWSTATEGENERATOR'=>'CC7A3876',
		'__PREVIOUSPAGE'=>'MEFqmrRFdAFxiPWkBvrml05rnIRe8jJVKDh1wj89FKs1G2Rek4IryjTaHlC-rNd-2HtkAqhiSUIKkE54qPVkuVoJhSkLx4ZYDPUwBFKncms1',
		'__EVENTVALIDATION'=>'/wEdAApjJ4ohBpDDzCIiP8wjuq6hIcB3LL+ZLSRU+2/jzoOPGLza/Lk9Oj/o6NoARNziCY8emv7bUJ6PsAheIGnGpx4mw6xK3/zDZ00IFY6CBay8KdQbDvbdcs+JluBDfOdHxfoY+Uj2VEjekvd830n7nomWJZRMa6EJEgYEBP+AVmD342htzfM52uVk4wp4VRjymESVX1zSXSMPTpCtx7eVuniVoFUXmUm1skx5kv6KTgEBVucq+Nk=',
		'__VIEWSTATE'=>'/wEPDwUKMTYwMTM5MTE2OA9kFgJmD2QWBgIDD2QWCAIDDxYCHglpbm5lcmh0bWwFA0RURWQCBQ8WAh8ABS5Eb21pYyYjMjM3O2xpbyBUcmlidXQmIzIyNTtyaW8gRWxldHImIzI0NDtuaWNvZAIJD2QWAmYPZBYCAgEPZBYGAgMPDxYIHghDc3NDbGFzcwUKbW9kYWwgZmFkZR4wX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUc8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgRXJybx4yX19QSENlbnRyb19tc2dFcnJvTG9naW5fVmlld1N0YXRlQVNNb2RhbENvbnRldWRvSURlHgRfIVNCAgIWDB4IdGFiaW5kZXgFAi0xHgRyb2xlBQZkaWFsb2ceC2FyaWEtaGlkZGVuBQR0cnVlHg1kYXRhLWtleWJvYXJkBQVmYWxzZR4NZGF0YS1iYWNrZHJvcAUGc3RhdGljHglkcmFnZ2FibGUFBHRydWVkAgUPZBYGAgcPFgIfAGVkAgsPDxYCHgdWaXNpYmxlZ2RkAg0PDxYCHwtnZGQCBw8WAh8ABf0HPHRhYmxlPg0KDQo8dHI+DQoNCiAgICA8dGQ+DQogICAgDQogICAgICAgIDxkaXYgY2xhc3M9ImNvbDEgc3BhbjUiPg0KICAgICAgICAgICAgPGltZyBzcmM9Ii9kdGUvZHRfZV9maW5hbC5wbmciIGFsdD0iTG9nbyBEVEUiLz4NCiAgICAgICAgICAgIDxiciAvPjxiciAvPjxiciAvPg0KICAgICAgICA8L2Rpdj4NCiAgICA8L3RkPg0KPC90cj4NCjx0cj4NCiAgICA8dGQ+DQogICANCiAgICAgIDxkaXY+DQoNCiAgICAgICAgPHA+RmVycmFtZW50YSBleHRlcm5hIHBhcmEgZ2VyZW5jaWFtZW50byBkYSBDb250YSBubyBEb21pY8OtbGlvIFRyaWJ1dMOhcmlvIEVsZXRyw7RuaWNvIC0gRFRFPC9wPg0KICAgIA0KICAgICAgICA8aDE+DQogICAgDQogICAgICAgIDwvaDE+DQogICAgICAgIDxoMj5JbmZvcm1hw6fDtWVzLjwvaDI+DQogICAgDQogICAgICAgIDxwPg0KICAgICAgICAgICAgUHJvamV0YWRvIGNvbSBhIGlkZWlhIGRlIGRlc2Vudm9sdmVyIGRlIGZvcm1hIG1haXMgaW50dWl0aXZhLCBlZmljaWVudGUsIMO6dGlsIGUgc2ltcGxlcy4gDQogICAgICAgICAgICA8YnIgLz4NCiAgICAgICAgICAgIENvbSBhIGZlcnJhbWVudGEgcG9kZXLDoSBjYWRhc3RyYXIgdW1hIENvbnRhIERvbWljw61saW8gVHJpYnV0w6FyaW8sIEVuZGVyZcOnb3MgRWxldHLDtG5pY29zLCBUZWxlZm9uZXMgQ2VsdWxhcmVzLCBMZXIgTWVuc2FnZW5zIFJlY2ViaWRhcyBlIFZpc3VhbGl6YXIgUmVsYXTDs3Jpb3MuDQogICAgICAgIDwvcD4NCiAgICAgICAgPHVsPg0KICAgICAgICAgICAgPGxpPg0KICAgICAgICAgICAgICAgIDxwPkFjZXNzZSBhIHBhcnRpciBkZSBxdWFscXVlciBkaXNwb3NpdGl2byBtw7N2ZWwsIG7Do28gc2UgcHJlb2N1cGUgY29tIGEgcmVzb2x1w6fDo28sIGFqdXN0YW1vcyBhIHRlbGEgcGFyYSBhIG1lbGhvciB2aXN1YWxpemHDp8Ojby48L3A+DQogICAgICAgICAgICA8L2xpPiAgICANCiAgICAgICAgPC91bD4NCg0KICAgICAgICA8L2Rpdj4NCg0KICAgIDwvdGQ+DQo8L3RyPg0KDQo8L3RhYmxlPmQCDQ8PFggeKV9fQVNNb2RhbF9FcnJvX1ZpZXdTdGF0ZUFTTW9kYWxDb250ZXVkb0lEZR4nX19BU01vZGFsX0Vycm9fVmlld1N0YXRlQVNNb2RhbFRpdHVsb0lEBUM8aSBjbGFzcz0naWNvbi1maXhlZC13aWR0aCBpY29uLXdhcm5pbmctc2lnbiB0ZXh0LXdhcm5pbmcnPjwvaT7CoMKgHwEFCm1vZGFsIGZhZGUfBAICFgwfBQUCLTEfBgUGZGlhbG9nHwcFBHRydWUfCAUFZmFsc2UfCQUGc3RhdGljHwoFBHRydWVkAgUPFgIfAAVIU0lTVEVNQSBUUklCVVQmIzE5MztSSU8gU0VGQVogLSBTZWNyZXRhcmlhIGRhIEZhemVuZGEgZG8gRXN0YWRvIGRhIEJhaGlhZAIHDxYCHwAFD0FTTElCOiAyLjEuMTAuMGRkEzytXcLa4YyfkajFBRKCVE2wQGc=',
		'ctl00$PHCentro$btnLogin'=>'Entrar',

		'ctl00$PHCentro$userLogin'=>$loginMaster,
		'ctl00$PHCentro$userPass'=>$senhaMaster
		);

		//FAZENDO A REQUISIÇÃO USANDO CURL
		$url = "https://sistemasweb.sefaz.ba.gov.br/sistemas/DTE/Contribuinte/SSL/ASLibrary/Login";
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$store = curl_exec($ch);
	}

	public function salvarLogin($banco, $id_empresa, $login, $senha, $status_da_senha, $data_verificacao){

		// error_reporting(0);

		$host ="207.246.115.230";
		$user = "user1*1outras*ma";
		$pass = "12*databyte*21";
		$db = $banco;
		$tabela = "dtb_logins_validos";

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');
		##############################################################

		$sqlSelect = "SELECT id_empresa FROM $tabela WHERE id_empresa = '$id_empresa'";

		if ($resultSelect = $con->query($sqlSelect)) {

		    $row_cnt = $resultSelect->num_rows;

			if($row_cnt != 0){
				$sqlUpdate = "UPDATE $tabela SET (`login`='$login',`senha`='$senha',`data_verificacao`='$data_verificacao',`status_da_senha`='$status_da_senha') WHERE `id_empresa`='$id_empresa';";

				$result = mysqli_query($con,$sqlUpdate);
			}else{
				$sql = "INSERT INTO $tabela (`login`, `senha`, `id_empresa`, `data_verificacao`, `status_da_senha`) ";
				$sql .= "VALUES ('$login','$senha','$id_empresa','$data_verificacao','$status_da_senha')";

				$result2 = mysqli_query($con,$sql);
			} 
		}
	}
}