<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_debito_ipva extends CI_Controller {

	public function buscar(){

		$banco = $this->uri->segment(4);

		$host ="localhost";
        $user = "root";
        $pass = "12*Gr4p0D4t4ByT3SySV3r1*21";
        $db = $banco;

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

		$query = ("SELECT id, cnpj, inscricao_estadual FROM dtb_empresas WHERE id < 500");
		$consulta = mysqli_query($con, $query);

		while($dados = mysqli_fetch_assoc($consulta)){

		// ---------------------------------------------------------------------------------------------------------

			$dadoss = http_build_query(array(
					'txt_numero_cnpj' => $dados['cnpj'],
					'exibir' => 'Exibir'

			));

			$contexto = stream_context_create(array(
					'http' => array(
							'method' => 'POST',
							'content' => $dadoss,
							'header' => "Content-type: application/x-www-form-urlencoded\r\n"
							. "Content-Length: " . strlen($dadoss) . "\r\n",
					)
			));
			
			//$resposta = file_get_contents('http://www.sefaz.ba.gov.br/scripts/ipva/IPVA_veiculo_com_pendencia_resultado.asp', null, $contexto);

			$lines = array();

			$url = "http://www.sefaz.ba.gov.br/scripts/ipva/IPVA_veiculo_com_pendencia_resultado.asp";

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $dadoss); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt'); 
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$store = curl_exec($ch);

			//echo strip_tags($store);
			$dom = new DOMDocument;
			libxml_use_internal_errors(true);
			$dom->validateOnParse = true; 
			libxml_use_internal_errors(true);
			$dom->loadHTML($store);
			libxml_clear_errors();
			$htmlRootElement = $dom->getElementsByTagName('html');
			$resposta = $dom->saveHTML();
			
			$id_empresa = $dados['id'];
			$cnpj = $dados['cnpj'];
			$inscricao_estadual = $dados['inscricao_estadual'];

			$pos = strpos($resposta, 'CNPJ informado !');
			if ($pos === false) {

				preg_match_all('#<tr[^>]*>(.*?)</tr>#is', $resposta, $lines);
				
				unset($lines[0][0]);
				unset($lines[0][1]);

				$results = array();
				$texto = "";

				foreach ($lines[0] as $k => $line) {
					preg_match_all('#<td[^>]*>(.*?)</td>#is', $line, $cell);

					foreach ($cell[1] as $cell) {
						$text = strip_tags($cell);
						$texto = $texto." ".$text;
					}
				}
				$string = substr($texto,0,strripos($texto, 'Consulta'));

				$placas_string = "";
				if ($a = explode(" ", $string)) { // create parts
				    foreach ($a as $s) { 
				    	if (trim($s) != "") {
				    		if($placas_string != ""){
				    			$placas_string = $placas_string.trim($s)."|";
				    		}else{
				    			$placas_string = trim($s)."|";
				    		}
				    		
				    	}
				    	
				    }
				}

				$placas_string = trim($placas_string);
				$placas = substr($placas_string, 0, -1);

				$query = ("UPDATE dtb_ipva SET `id_empresa`='$id_empresa' , `cnpj`='$cnpj' , `inscricao_estadual`='$inscricao_estadual' , `placas`='$placas' WHERE `cnpj`='$cnpj';");
				$query = str_replace("\n", " ",$query);
				$retorno = mysqli_query($con, $query);

				if(mysqli_num_rows($retorno)== FALSE){
					$query = ("INSERT INTO `dtb_ipva`(`id_empresa`, `cnpj`, `inscricao_estadual`, `placas`) VALUES ('$id_empresa', '$cnpj', '$inscricao_estadual', '$placas');");
					$query = str_replace("\n", " ",$query);		
					mysqli_query($con, $query);
				}else{
					#ESTE DADO JÁ EXISTE
				}	
			}else{
				$query = ("DELETE FROM dtb_ipva where cnpj = '$cnpj');");
				$query = str_replace("\n", " ",$query);		
				mysqli_query($con, $query);
			}
		}	
	}

}