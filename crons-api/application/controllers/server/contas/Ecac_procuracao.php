<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ecac_procuracao extends CI_Controller {
	/**
	 * Método  principal onde é disparado os processos do robo
	 */
	public function buscar_ecac()
	{	

		$banco = $this->uri->segment(4);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificado_model', 'certificado');

		date_default_timezone_set('America/Bahia');
		$cerficados = $this->certificado->get($banco);
		
		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library
			 */
			$params = array('caminho_certificado' => 'https://veri.com.br/'.str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
			'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));
			$this->load->library('Ecac_robo_library_procuracao', $params);

			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */
			if(!$this->ecac_robo_library_procuracao->acesso_valido()){
				unset($this->ecac_robo_library_procuracao);
				continue;
			}

			/**
			 * Grava as mensagens da caixa postal
			 */
			$caixa_postal = $this->ecac_robo_library_procuracao->obter_mensagem_caixa_postal();
			if($caixa_postal)
				$this->inserir_caixa_postal($caixa_postal, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);

			/**
			 * Grava as parcelas do simples nacional
			 */

			$parcelas = $this->ecac_robo_library_procuracao->obter_simples_nacional_emissao_parcela();
			if($parcelas)
				$this->inserir_parcelas_emitidas($parcelas, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
			/**
			 * Grava se possui pedidos de parcelas ou não
			 */

			$possui_pedidos = $this->ecac_robo_library_procuracao->obter_simples_nacional_pedidos_parcela();
			if($possui_pedidos)
				$this->inserir_consulta_pedidos($possui_pedidos, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
			/**
			 * Emite mensagem de sucesso e resumo da operação
			 */
			echo "==============SUCESSO NA OPERAÇÃO==========\n";
			// echo "Documento: {$this->ecac_robo_library_procuracao->obter_numero_documento()}\n";
			// $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
			// echo "Situação Fiscal: {$mensagem_pendencia}\n";
			// echo "PDF situação: {$path_pdf}\n";
			echo "===========================================\n";

//			Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
			unset($this->ecac_robo_library_procuracao);
		}

	}

    public function buscar_ecac_com_procuracao()
    {

        $banco = $this->uri->segment(4);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_contas_model', 'contadorprocuracao');

        date_default_timezone_set('America/Bahia');
        $cerficados = $this->certificado->get($banco);

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $this->config->item('caminho_pasta_pdf'));

            $this->load->library('Ecac_robo_library_procuracao', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if(!$this->ecac_robo_library_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);

            foreach ($empresas_com_procuracao as $item){

                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    continue;
                }

                /**
                 * Grava as mensagens da caixa postal
                 */
                $caixa_postal = $this->ecac_robo_library_procuracao->obter_mensagem_caixa_postal();
                if($caixa_postal)
                    $this->inserir_caixa_postal($caixa_postal, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);

                /**
                 * Grava as parcelas do simples nacional
                 */

                $parcelas = $this->ecac_robo_library_procuracao->obter_simples_nacional_emissao_parcela();
                if($parcelas)
                    $this->inserir_parcelas_emitidas($parcelas, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
                /**
                 * Grava se possui pedidos de parcelas ou não
                 */

                $possui_pedidos = $this->ecac_robo_library_procuracao->obter_simples_nacional_pedidos_parcela();
                if($possui_pedidos)
                    $this->inserir_consulta_pedidos($possui_pedidos, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
                /**
                 * Emite mensagem de sucesso e resumo da operação
                 */
                echo "==============SUCESSO NA OPERAÇÃO==========\n";
//                echo "Documento: {$this->ecac_robo_library_procuracao->obter_numero_documento()}\n";
                // $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
                // echo "Situação Fiscal: {$mensagem_pendencia}\n";
                // echo "PDF situação: {$path_pdf}\n";
                echo "===========================================\n";
            }

//			Tem que fazer unset pra ele executar  o destrutor da library e encerrar a connection
            unset($this->ecac_robo_library_procuracao);
        }

    }

	function inserir_caixa_postal($data, $cnpj_data, $banco){

		$this->load->model('caixa_postal_mensagem_model', 'caixa_postal_mensagem');
		$this->load->model('caixa_postal_model', 'caixa_postal');

		$result = $this->caixa_postal->existe_caixa_postal($cnpj_data, $banco);
		if($result->qtd > 0){
			$caixa_postal_id = $result->id;
			$this->caixa_postal->update($data, $cnpj_data, $caixa_postal_id, $banco);
		}else{
			$caixa_postal_id = $this->caixa_postal->insert($data, $cnpj_data, $banco);
		}

		$this->caixa_postal_mensagem->limpaTabelaMensagens($caixa_postal_id, $banco);

		foreach ($data['mensagens'] as $mensagem)
		{
			$mensagem['caixa_postal_id'] = $caixa_postal_id;
			$this->caixa_postal_mensagem->insert($mensagem, $banco);
		}
	}

	function inserir_parcelas_emitidas($parcelas, $cnpj_data, $banco){
		$this->load->model('emissao_parcela_model');
		$this->emissao_parcela_model->delete_parcelas($cnpj_data, $banco);
		foreach($parcelas as $parcela){
			$this->emissao_parcela_model->insert($parcela, $cnpj_data, $banco);
		}
	}

	function inserir_consulta_pedidos($possui_pedidos,  $cnpj_data, $banco){
		$this->load->model('pedidos_parcela_model');

		$this->pedidos_parcela_model->delete_consulta_pedido($cnpj_data, $banco);
		$this->pedidos_parcela_model->insert($possui_pedidos,  $cnpj_data, $banco);
	}


	/////////////////////////// Funções da situação fiscal //////////////////////////////////////////////
	function cron_situacao_fiscal(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

		$banco = $this->uri->segment(4);

		$this->config->load('ecac_robo_config');
		$this->load->model('certificado_model', 'certificado');
		date_default_timezone_set('America/Bahia');

		$cerficados = $this->certificado->get($banco);
		
		$folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

		foreach ($cerficados as $cerficado){

			/**
			 * Carrega a library principal Ecac_robo_library
			 */
			$params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
			'cerficado_senha' => $cerficado->pass,
			'caminho_da_pasta_pdfs' => $folder_pdf);
			$this->load->library('Ecac_robo_library', $params);

			/**
			 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
			 */
			if(!$this->ecac_robo_library_procuracao->acesso_valido()){
				unset($this->ecac_robo_library_procuracao);
				continue;
			}

			/**
			 * Grava a situação fiscal e o pdf
			 */

            $path_pdf = $this->ecac_robo_library_procuracao->baixar_pdf_situacao_fiscal();

            $pdf	=  new PdfToText() ;
            $pdf->Load( $path_pdf ) ;
            $texto_base = "Pendência -";
            $pos = strpos($pdf->Text, $texto_base);

            $possui_pendencia =  false;

            if ($pos !== false)
                $possui_pendencia = true;

            $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

            $caminho_download = "http://veri.com.br".$caminho_aux;

            $existe_situacao = $this->verifica_se_existe_situacao($this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
            if($existe_situacao > 0){
                $this->update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
            }else{
                $this->inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
            }

            echo "==============SUCESSO NA OPERAÇÃO==========\n";
            echo "Documento: {$this->ecac_robo_library_procuracao->obter_numero_documento()}\n";
            $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
            echo "Situação Fiscal: {$mensagem_pendencia}\n";
            echo "PDF situação: {$path_pdf}\n";
            echo "===========================================\n";


			unset($this->ecac_robo_library_procuracao);

		}

	}

    function cron_situacao_fiscal_com_procuracao(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(4);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_contas_model', 'contadorprocuracao');
        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaoecac/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_procuracao', $params);
            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */

            if(!$this->ecac_robo_library_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);

            //limpar tabela antes do laço
            $this->contadorprocuracao->clear_table($banco);

            foreach ($empresas_com_procuracao as $item){
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";
                    
                    $this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
                    continue;
                }
                /**
                 * Grava a situação fiscal e o pdf
                 */

                $pdf_carregado_com_sucesso = true;

                try{

                    $path_pdf = $this->ecac_robo_library_procuracao->baixar_pdf_situacao_fiscal();


                    $pdf	=  new PdfToText() ;
                    $pdf->Load( $path_pdf ) ;

                }catch (Exception $e){

                    $pdf_carregado_com_sucesso = false;
                    log_message('error', "Documento: {$this->ecac_robo_library_procuracao->obter_numero_documento()} - Erro ao gerar o PDF.\n");
                }

                if($pdf_carregado_com_sucesso){
                    $texto_base = "Pendência -";
                    $pos = strpos($pdf->Text, $texto_base);

                    $possui_pendencia =  false;

                    if ($pos !== false)
                        $possui_pendencia = true;

                    $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                    $caminho_download = "http://veri.com.br".$caminho_aux;

                    $existe_situacao = $this->verifica_se_existe_situacao($this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
                    if($existe_situacao > 0){
                        $this->update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
                    }else{
                        $this->inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $this->ecac_robo_library_procuracao->obter_numero_documento(), $banco);
                    }

                    echo "==============SUCESSO NA OPERAÇÃO==========\n";
                    echo "Documento: {$this->ecac_robo_library_procuracao->obter_numero_documento()}\n";
                    $mensagem_pendencia = $possui_pendencia ? "Possui pendência." : "Não foram encontradas pêndencias.";
                    echo "Situação Fiscal: {$mensagem_pendencia}\n";
                    echo "PDF situação: {$path_pdf}\n";
                    echo "===========================================\n";
                }
                
            }
            unset($this->ecac_robo_library_procuracao);

        }

    }

	function verifica_se_existe_situacao($cnpj_data, $banco){
		$this->load->model('situacao_fiscal_model');
		$resultado = $this->situacao_fiscal_model->verifica_se_existe($cnpj_data, $banco);
		return $resultado->qtd;
	}

	function inserir_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $cnpj_data, $banco){
		$this->load->model('situacao_fiscal_model');
		$this->situacao_fiscal_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $cnpj_data, $banco);
	}

	function update_situacao_fiscal($possui_pendencia, $path_pdf, $caminho_download, $cnpj_data, $banco){
		$this->load->model('situacao_fiscal_model');
		$this->situacao_fiscal_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $cnpj_data, $banco);
	}

    // CADIN
    function cron_pendencia_cadin_com_procuracao(){
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(4);

        $this->config->load('ecac_robo_config');
        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('situacao_cadin_model');

        date_default_timezone_set('America/Bahia');

        $cerficados = $this->certificado->get($banco);

        $folder_pdf = FCPATH . 'pdf-certidao-ecac/certidaocadin/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        foreach ($cerficados as $cerficado){

            /**
             * Carrega a library principal Ecac_robo_library_procuracao
             */
            $params = array('caminho_certificado' => str_replace('//','/', $cerficado->caminho_arq ) ,
                'cerficado_senha' => $cerficado->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_procuracao', $params);
            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */

            if(!$this->ecac_robo_library_procuracao->acesso_valido()){
                unset($this->ecac_robo_library_procuracao);
                continue;
            }

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);

            //limpar tabela antes do laço
            $this->contadorprocuracao->clear_table($banco);

            foreach ($empresas_com_procuracao as $item){
                /**
                 * Função que altera o perfil no portal
                 */
                $validado = $this->ecac_robo_library_procuracao->trocar_perfil($item->cnpj);
                if(! $validado){
                    echo "CNPJ: {$item->cnpj} - sem procuração";

                    $this->contadorprocuracao->insere_empresas_sem_procuracao($banco, $item->cnpj);
                    continue;
                }

                $path_pdf = $this->ecac_robo_library_procuracao->baixar_pdf_cadin();

                $pdf    =  new PdfToText() ;
                $carregado = false;
                try {
                    $pdf->Load( $path_pdf ) ;
                    $carregado = true;
                }catch (Exception $e){
                    $carregado = false;
                }

                if ( $carregado ){
                    $texto_base = "NÃO INCLUÍDO PELA RFB";
                    $pos = strpos($pdf->Text, $texto_base);

                    $texto_base2 = "EXCLUÍDO PELA RFB";
                    $pos2 = strpos($pdf->Text, $texto_base2);

                    $possui_pendencia =  true;

                    if ($pos !== false)
                        $possui_pendencia = false;

                    if ($pos2 !== false)
                        $possui_pendencia = false;

                    $caminho_aux = str_replace("/var/www/html", "",$path_pdf);

                    $caminho_download = "http://veri.com.br".$caminho_aux;

                    $numero_documento = $this->ecac_robo_library_procuracao->obter_numero_documento();
                    if(empty($numero_documento)){
                        $numero_documento = $item->cnpj;
                    }
                    
                    $existe_situacao = $this->situacao_cadin_model->verifica_se_existe($numero_documento, $banco);


                    if($existe_situacao->qtd > 0){
                        $this->situacao_cadin_model->update($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $numero_documento, $banco);
                    }else{
                        $this->situacao_cadin_model->insert($possui_pendencia, file_get_contents($path_pdf), $caminho_download, $numero_documento, $banco);
                    }
                }

            }

            unset($this->ecac_robo_library_procuracao);

        }

    }
}
