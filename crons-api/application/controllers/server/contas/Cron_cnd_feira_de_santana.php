<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_cnd_feira_de_santana extends CI_Controller {

	public function buscar(){

		$banco = $this->uri->segment(4);

		$host ="localhost";
        $user = "root";
        $pass = "12*Gr4p0D4t4ByT3SySV3r1*21";
        $db = $banco;

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

		$query = ("SELECT id, razao_social, cnpj, cnpj_completo, inscricao_estadual_completo, inscricao_estadual FROM dtb_empresas WHERE id < 500 ORDER BY id ASC");
		$consulta = mysqli_query($con, $query);

		while($dados = mysqli_fetch_assoc($consulta)){

			$dadoss = http_build_query(array(
					'POST_INSCRICAOMUNI'=> $dados['cnpj']
			));
			
			$contexto = stream_context_create(array(
					'http' => array(
							'method' => 'POST',
							'content' => $dadoss,
							'header' => "Content-type: application/x-www-form-urlencoded\r\n"
							. "Content-Length: " . strlen($dadoss) . "\r\n",
					)
			));
			
			//$resposta = file_get_contents('http://www.sefaz.feiradesantana.ba.gov.br/exec/exe_viacertidao.php', null, $contexto);
			$resposta = file_get_contents('http://www.sefaz.feiradesantana.ba.gov.br/exec/exe_CertidaodeDebitos.php', null, $contexto);

			$resposta = html_entity_decode($resposta);

			$pos = strpos($resposta, 'Inscrição inválida!');
			$pos2 = strpos($resposta, 'Inscrição não encontrada');
			$pos3 = strpos($resposta, 'As informaçõs disponíveis na Secretaria Municipal da Fazenda');

			if($pos3 === false){
				if ($pos === false && $pos2 === false) {
					$inicio_inscricao_municipal = strpos($resposta, 'Inscrição Municipal:') + 22;
					$inscricao_municipal = substr($resposta, $inicio_inscricao_municipal, 17);

					if(strlen(trim($inscricao_municipal)) == 16){
						$cnpj = $dados['cnpj'];
						$cnpj_completo = $dados['cnpj_completo'];
						$inscricao_estadual = $dados['inscricao_estadual'];
						$inscricao_estadual_completo = $dados['inscricao_estadual_completo'];
		 				$razao_social = $dados['razao_social'];
		 				$status = "TRUE";

		 				$inscricao_municipal = str_replace("<strong>","",$inscricao_municipal);

		 				$validar = ("SELECT * FROM dtb_certidao_feira WHERE `cnpj`='$cnpj';");
				  		$qtd_validar = mysqli_query($con, $validar); 

						if (mysqli_num_rows($qtd_validar)) {
							$query = ("UPDATE dtb_certidao_feira SET `cnpj`='$cnpj' , `cnpj_completo`='$cnpj_completo' , `inscricao_estadual`='$inscricao_estadual' , `inscricao_estadual_completo`='$inscricao_estadual_completo' , `razao_social`='$razao_social' , `inscricao_municipal`='$inscricao_municipal' , `status`='$status' WHERE `cnpj`='$cnpj';");
							$query = str_replace("\n", " ",$query);

							$retorno = mysqli_query($con, $query);
						}else{
							$query = ("INSERT INTO `dtb_certidao_feira`(`cnpj`, `cnpj_completo`, `inscricao_estadual`, `inscricao_estadual_completo`, `razao_social`, `inscricao_municipal`, `status`) VALUES ('$cnpj', '$cnpj_completo', '$inscricao_estadual', '$inscricao_estadual_completo', '$razao_social', '$inscricao_municipal', '$status');");
							$query = str_replace("\n", " ",$query);		
							mysqli_query($con, $query);
						}

					}
					
				} else {
				    //echo "Inscrição não encontrada";
				}	
			}else{
				$inicio_inscricao_municipal = strpos($resposta, 'Inscrição Municipal:') + 22;
				$inscricao_municipal = substr($resposta, $inicio_inscricao_municipal, 17);

				$cnpj = $dados['cnpj'];
				$cnpj_completo = $dados['cnpj_completo'];
				$inscricao_estadual = $dados['inscricao_estadual'];
				$inscricao_estadual_completo = $dados['inscricao_estadual_completo'];
				$razao_social = $dados['razao_social'];
				$status = "FALSE";
				$inscricao_municipal = str_replace("<strong>","",$inscricao_municipal);

				$validar = ("SELECT * FROM dtb_certidao_feira WHERE `cnpj`='$cnpj';");
		  		$qtd_validar = mysqli_query($con, $validar); 

				if (mysqli_num_rows($qtd_validar)) {
					$query = ("UPDATE dtb_certidao_feira SET `cnpj`='$cnpj' , `cnpj_completo`='$cnpj_completo' , `inscricao_estadual`='$inscricao_estadual' , `inscricao_estadual_completo`='$inscricao_estadual_completo' , `razao_social`='$razao_social' , `inscricao_municipal`='$inscricao_municipal' , `status`='$status' WHERE `cnpj`='$cnpj';");
					$query = str_replace("\n", " ",$query);

					$retorno = mysqli_query($con, $query);
				}else{
					$query = ("INSERT INTO `dtb_certidao_feira`(`cnpj`, `cnpj_completo`, `inscricao_estadual`, `inscricao_estadual_completo`, `razao_social`, `inscricao_municipal`, `status`) VALUES ('$cnpj', '$cnpj_completo', '$inscricao_estadual', '$inscricao_estadual_completo', '$razao_social', '$inscricao_municipal', '$status');");
					$query = str_replace("\n", " ",$query);		
					mysqli_query($con, $query);
				}

				//echo "Não pode gerar a certidão";
			}

		}
	}
}