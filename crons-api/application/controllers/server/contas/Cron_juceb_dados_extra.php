<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_juceb_dados_extra extends CI_Controller {

	public function buscar(){

		$banco = $this->uri->segment(4);

		$host ="localhost";
        $user = "root";
        $pass = "12*Gr4p0D4t4ByT3SySV3r1*21";
        $db = $banco;

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

		$query = ("SELECT id, cnpj, razao_social FROM dtb_empresas WHERE id >= 500 ORDER BY id ASC");
		$dados = mysqli_query($con, $query);

		while ($dados_consulta = mysqli_fetch_assoc($dados)) {

			$cnpj = $dados_consulta['cnpj'];
			$id_empresa = $dados_consulta['id'];
			// ---------------------------------------------------------------------------------------------------------
			
			$html = file('http://www.certidaoonline.juceb.ba.gov.br/certidao/publico/listarempresas?pessoaJuridicaVO.nrNire=&pessoaJuridicaVO.nrCgc='.$cnpj); 

			if (!empty($html)){  
				
				$linhas = array();
				
				foreach ($html as $key => $value) {
					$text = strip_tags($value);
					$linhas[] = trim(html_entity_decode($text));
				}
				
				$texto = implode(' ', $linhas);

				// NOME EMPRESARIAL
				$ini_nome_empresarial = strpos($texto, 'Nome Empresarial');
				$texto1 = substr($texto, $ini_nome_empresarial);
				$fim_nome_empresarial = strpos($texto1, 'NIRE/CNPJ');

				$local_nome_empresarial = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_nome_empresarial))));
				$local_nome_empresarial = preg_replace('/\s\s+/', ' ', substr($local_nome_empresarial, 16)); 

				$juceb_nome_empresarial = $local_nome_empresarial;
				// NOME EMPRESARIAL FIM 

				// CNPJ E NIRE
				$juceb_nire = trim(html_entity_decode(strip_tags($html[86])));

				$juceb_cnpj = trim(html_entity_decode(strip_tags($html[87]))); 
				$juceb_cnpj = preg_replace('/\s\s+/', ' ', substr($juceb_cnpj, 2));
				// CNPJ E NIRE FIM

				// SITUAÇÃO
				$ini_situacao = strpos($texto, 'Situação / Status');
				$texto1 = substr($texto, $ini_situacao);
				$fim_situacao = strpos($texto1, 'Natureza Jurídica');

				$local_situacao = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_situacao))));
				$local_situacao = preg_replace('/\s\s+/', ' ', substr($local_situacao, 19)); 

				$juceb_situacao = substr($local_situacao, 0, strpos($local_situacao, "/"));
				// SITUAÇÃO FIM

				// STATUS
				$juceb_status = strstr($local_situacao, '/');

				$juceb_status = str_replace("/", "", $juceb_status);
				// STATUS FIM

				// NATUREZA JURIDICA
				$ini_natureza_juridica = strpos($texto, 'Natureza Jurídica');
				$texto1 = substr($texto, $ini_natureza_juridica);
				$fim_natureza_juridica = strpos($texto1, 'CPF(do Empresário/Sócio)');

				$local_natureza_juridica = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_natureza_juridica))));
				$local_natureza_juridica = preg_replace('/\s\s+/', ' ', substr($local_natureza_juridica, 18)); 

				$juceb_natureza_juridica = $local_natureza_juridica;
				// NATUREZA JURIDICA FIM

				//CAPITAL SOCIAL
				$ini_capital_social = strpos($texto, 'Capital Social');
				$texto1 = substr($texto, $ini_capital_social);
				$fim_capital_social = strpos($texto1, 'Capital Integralizado');

				$local_capital_social = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_capital_social))));
				$local_capital_social = preg_replace('/\s\s+/', ' ', substr($local_capital_social, 14)); 

				$juceb_capital_social = $local_capital_social;
				//CAPITAL SOCIAL FIM

				//CAPITAL INTEGRALIZADO
				$ini_capital_integralizado = strpos($texto, 'Capital Integralizado');
				$texto1 = substr($texto, $ini_capital_integralizado);
				$fim_capital_integralizado = strpos($texto1, 'Data do Ato Constitutivo');

				$local_capital_integralizado = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_capital_integralizado))));
				$local_capital_integralizado = preg_replace('/\s\s+/', ' ', substr($local_capital_integralizado, 21)); 

				$juceb_capital_integralizado = $local_capital_integralizado;
				//CAPITAL INTEGRALIZADO FIM

				// DATA DO ATO CONSTITUTIVO
				$ini_data_do_ato_constitutivo = strpos($texto, 'Data do Ato Constitutivo');
				$texto1 = substr($texto, $ini_data_do_ato_constitutivo);
				$fim_data_do_ato_constitutivo = strpos($texto1, 'Data do Início das Atividades');

				$local_data_do_ato_constitutivo = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_data_do_ato_constitutivo))));
				$local_data_do_ato_constitutivo = preg_replace('/\s\s+/', ' ', substr($local_data_do_ato_constitutivo, 24)); 

				$juceb_data_do_ato_constitutivo = $local_data_do_ato_constitutivo;
				// DATA DO ATO CONSTITUTIVO FIM

				// DATA DO INICIO DAS ATIVIDADES
				$ini_data_inicio_das_atividades = strpos($texto, 'Data do Início das Atividades');
				$texto1 = substr($texto, $ini_data_inicio_das_atividades);
				$fim_data_inicio_das_atividades = strpos($texto1, 'Logradouro');

				$local_data_inicio_das_atividades = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_data_inicio_das_atividades))));
				$local_data_inicio_das_atividades = preg_replace('/\s\s+/', ' ', substr($local_data_inicio_das_atividades, 30)); 

				$juceb_data_inicio_das_atividades = $local_data_inicio_das_atividades;
				// DATA DO INICIO DAS ATIVIDADES FIM

				// LOGRADOURO
				$ini_logradouro = strpos($texto, 'Logradouro');
				$texto1 = substr($texto, $ini_logradouro);
				$fim_logradouro = strpos($texto1, 'Complemento');

				$local_logradouro = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_logradouro))));
				$local_logradouro = preg_replace('/\s\s+/', ' ', substr($local_logradouro, 10)); 

				$juceb_logradouro = $local_logradouro;
				// LOGRADOURO FIM

				// COMPLEMENTO
				$ini_complemento = strpos($texto, 'Complemento');
				$texto1 = substr($texto, $ini_complemento);
				$fim_complemento = strpos($texto1, 'Número');

				$local_complemento = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_complemento))));
				$local_complemento = preg_replace('/\s\s+/', ' ', substr($local_complemento, 11)); 

				$juceb_complemento = $local_complemento;
				// COMPLEMENTO FIM

				// NUMERO
				$ini_numero = strpos($texto, 'Número');
				$texto1 = substr($texto, $ini_numero);
				$fim_numero = strpos($texto1, 'Bairro');

				$local_numero = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_numero))));
				$local_numero = preg_replace('/\s\s+/', ' ', substr($local_numero, 7)); 

				$juceb_numero = $local_numero;
				// NUMERO FIM

				// BAIRRO
				$ini_bairro = strpos($texto, 'Bairro');
				$texto1 = substr($texto, $ini_bairro);
				$fim_bairro = strpos($texto1, 'CEP');

				$local_bairro = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_bairro))));
				$local_bairro = preg_replace('/\s\s+/', ' ', substr($local_bairro, 6)); 

				$juceb_bairro = $local_bairro;
				// BAIRRO FIM

				// CEP
				$ini_cep = strpos($texto, 'CEP');
				$texto1 = substr($texto, $ini_cep);
				$fim_cep = strpos($texto1, 'Município');

				$local_cep = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_cep))));
				$local_cep = preg_replace('/\s\s+/', ' ', substr($local_cep, 3)); 

				$juceb_cep = $local_cep;
				// CEP FIM

				// MUNICIPIO
				$ini_municipio = strpos($texto, 'Município');
				$texto1 = substr($texto, $ini_municipio);
				$fim_municipio = strpos($texto1, 'Objeto Social');

				$local_municipio = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_municipio))));
				$local_municipio = preg_replace('/\s\s+/', ' ', substr($local_municipio, 10)); 

				$juceb_municipio = $local_municipio;
				// MUNICIPIO FIM

				// OBJETO SOCIAL
				$ini_objeto_social = strpos($texto, 'Objeto Social');
				$texto1 = substr($texto, $ini_objeto_social);
				$fim_objeto_social = strpos($texto1, 'class="RotuloPadrao"');

				$local_objeto_social = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_objeto_social))));
				$local_objeto_social = preg_replace('/\s\s+/', ' ', substr($local_objeto_social, 13)); 

				$juceb_objeto_social = $local_objeto_social;
				// OBJETO SOCIAL FIM

				// HISTORICO DATA ULTIMO ARQUIVAMENTO
				$ini_historico_data_ultimo_arquivamento = strpos($texto, 'Descrição Ato');
				$texto1 = substr($texto, $ini_historico_data_ultimo_arquivamento);
				$fim_historico_data_ultimo_arquivamento = strpos($texto1, ' height="23"');

				$local_historico_data_ultimo_arquivamento = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_historico_data_ultimo_arquivamento))));
				$local_historico_data_ultimo_arquivamento = preg_replace('/\s\s+/', ' ', substr($local_historico_data_ultimo_arquivamento, 15)); 
				$juceb_historico_alteracao_geral = $local_historico_data_ultimo_arquivamento;
				// HISTORICO DATA ULTIMO ARQUIVAMENTO FIM

				// HISTORICO NOME DO EVENTO, NUMERO ARQUIVAMENTO E DESCRICAO ATO
				$pieces = explode(" ", $juceb_historico_alteracao_geral);
				$juceb_historico_data_ultimo_arquivamento = $pieces[1];
				$juceb_historico_nome_do_evento = $pieces[2];
				$juceb_historico_numero_arquivamento = $pieces[3];
				$juceb_historico_descricao_ato = $pieces[4];
				// HISTORICO NOME DO EVENTO, NUMERO ARQUIVAMENTO E DESCRICAO ATO FIM 
				
				date_default_timezone_set('America/Bahia');
				$data_alteracao = date("Y-m-d H:i:s");
				
				$validar = ("SELECT * FROM dtb_juceb_dados WHERE `id_empresa`='$id_empresa';");
		  		$qtd_validar = mysqli_query($con, $validar);

		  		$dateAux2 = '';

		  		if ($juceb_historico_data_ultimo_arquivamento != '') {
		  			 
		  		$dataValidadeEvento = $juceb_historico_data_ultimo_arquivamento;
				$dateAux = str_replace('/', '-', $dataValidadeEvento);
				$dateAux = date('Y-m-d', strtotime($dateAux));
				$dateAux2 = date('Y-m-d', strtotime('+10 years', strtotime($dateAux)));

		  		} 

				if (mysqli_num_rows($qtd_validar)) {
				
				$query = ("UPDATE dtb_juceb_dados 
					SET `juceb_nome_empresarial`='$juceb_nome_empresarial' , 
						`juceb_nire`='$juceb_nire' , 
						`juceb_cnpj`='$juceb_cnpj' , 
						`juceb_situacao`='$juceb_situacao' , 
						`juceb_status`='$juceb_status' , 
						`juceb_natureza_juridica`='$juceb_natureza_juridica' , 
						`juceb_capital_social`='$juceb_capital_social' , 
						`juceb_capital_integralizado`='$juceb_capital_integralizado' , 
						`juceb_data_do_ato_constitutivo`='$juceb_data_do_ato_constitutivo' , 
						`juceb_data_inicio_das_atividades`='$juceb_data_inicio_das_atividades' , 
						`juceb_logradouro`='$juceb_logradouro' , 
						`juceb_complemento`='$juceb_complemento' , 
						`juceb_numero`='$juceb_numero' , 
						`juceb_bairro`='$juceb_bairro' , 
						`juceb_cep`='$juceb_cep' , 
						`juceb_municipio`='$juceb_municipio' , 
						`juceb_objeto_social`='$juceb_objeto_social' , 
						`juceb_historico_data_ultimo_arquivamento`='$juceb_historico_data_ultimo_arquivamento' , 
						`juceb_historico_nome_do_evento`='$juceb_historico_nome_do_evento' , 
						`juceb_historico_numero_arquivamento`='$juceb_historico_numero_arquivamento' , 
						`juceb_historico_descricao_ato`='$juceb_historico_descricao_ato' ,
						`juceb_data_evento_expirar`='$dateAux2' ,
						`data_alteracao`='$data_alteracao' WHERE `id_empresa`='$id_empresa';");
				 
				 $query = str_replace("\n", " ",$query);
				 mysqli_query($con, $query); 

				 $query_inserir_nire = ("UPDATE dtb_empresas
					SET `nire`='$juceb_nire' WHERE `id`='$id_empresa';");
				 
				 $query_inserir_nire = str_replace("\n", " ",$query_inserir_nire);
				 mysqli_query($con, $query_inserir_nire);

				} else  {

				$query = ("INSERT INTO `dtb_juceb_dados`(
						`id_empresa`, 
						`juceb_nome_empresarial`, 
						`juceb_nire`, 
						`juceb_cnpj`, 
						`juceb_situacao`, 
						`juceb_status`, 
						`juceb_natureza_juridica`, 
						`juceb_capital_social`, 
						`juceb_capital_integralizado`, 
						`juceb_data_do_ato_constitutivo`, 
						`juceb_data_inicio_das_atividades`, 
						`juceb_logradouro`, 
						`juceb_complemento`, 
						`juceb_numero`, 
						`juceb_bairro`, 
						`juceb_cep`, 
						`juceb_municipio`, 
						`juceb_objeto_social`, 
						`juceb_historico_data_ultimo_arquivamento`, 
						`juceb_historico_nome_do_evento`, 
						`juceb_historico_numero_arquivamento`, 
						`juceb_historico_descricao_ato`,
						`juceb_data_evento_expirar`,				 
						`data_alteracao`) 

					VALUES (
						'$id_empresa', 
						'$juceb_nome_empresarial', 
						'$juceb_nire', 
						'$juceb_cnpj', 
						'$juceb_situacao', 
						'$juceb_status', 
						'$juceb_natureza_juridica', 
						'$juceb_capital_social', 
						'$juceb_capital_integralizado', 
						'$juceb_data_do_ato_constitutivo', 
						'$juceb_data_inicio_das_atividades', 
						'$juceb_logradouro', 
						'$juceb_complemento', 
						'$juceb_numero', 
						'$juceb_bairro', 
						'$juceb_cep', 
						'$juceb_municipio', 
						'$juceb_objeto_social', 
						'$juceb_historico_data_ultimo_arquivamento', 
						'$juceb_historico_nome_do_evento', 
						'$juceb_historico_numero_arquivamento', 
						'$juceb_historico_descricao_ato',
						'$dateAux2',
						'$data_alteracao');");
				
				$query = str_replace("\n", " ",$query);		
				mysqli_query($con, $query);

				$query_inserir_nire = ("UPDATE dtb_empresas
					SET `nire`='$juceb_nire' WHERE `id`='$id_empresa';");
				 
				 $query_inserir_nire = str_replace("\n", " ",$query_inserir_nire);
				 mysqli_query($con, $query_inserir_nire);

				}  

		 	} else {

		 		echo "Empresa não obteve resultados";
			}  

		}
	}
}