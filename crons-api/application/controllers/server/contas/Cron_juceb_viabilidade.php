<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_juceb_viabilidade extends CI_Controller {

	public function buscar(){
		
		$banco = $this->uri->segment(4);

		$host ="localhost";
        $user = "root";
        $pass = "12*Gr4p0D4t4ByT3SySV3r1*21";
        $db = $banco;

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

		$query = ("SELECT id, protocolo FROM dtb_viabilidade_juceb WHERE id < 500 ORDER BY id ASC");
		$dados = mysqli_query($con, $query);

		while ($dados_consulta = mysqli_fetch_assoc($dados)) {

			$protocolo = $dados_consulta['protocolo'];
			$id_protocolo = $dados_consulta['id'];
			// ---------------------------------------------------------------------------------------------------------

			$html = file('http://regin.juceb.ba.gov.br/regin.ba/CON_DadosIdentificacion.aspx?id='.$protocolo.'&pTipo=3&pTipoTela=CONSULTA'); 

			if (!empty($html)){   

				// COLETANDO O NUMERO DO PROTOCOLO
				$numero_do_protocolo_inicio = trim(html_entity_decode($html[177])); 

				$numero_do_protocolo_aux = strstr($numero_do_protocolo_inicio, 'value=');

				$numero_do_protocolo_fim = strstr($numero_do_protocolo_aux, 'size=', true);

				$numero_do_protocolo = $this->after('value=', $numero_do_protocolo_fim); 

				// COLETANDO O NOME DA EMPRESA E REMOVENDO POSIÇÕES
				$nome_da_empresa_inicio = trim(html_entity_decode($html[184])); 
				
				$nome_da_empresa_aux = strstr($nome_da_empresa_inicio, 'value=');

				$nome_da_empresa_fim = strstr($nome_da_empresa_aux, 'size=', true);

				$nome_da_empresa = $this->after('value=', $nome_da_empresa_fim);

				//COLETANDO O ESTADO DO PROCESSO E REMOVENDO POSIÇÕES
				$estado_do_processo_inicio = trim(html_entity_decode($html[190])); 

				$estado_do_processo_aux = strstr($estado_do_processo_inicio, 'value=');

				$estado_do_processo_fim = strstr($estado_do_processo_aux, 'size=', true);

				$estado_do_processo = $this->after('value=', $estado_do_processo_fim);

				//COLETANDO O STATUS DE ENVIO RFB 
				$status_envio_rfb = trim(html_entity_decode(strip_tags($html[195])));

				// COLETANDO DATA DE ATUALIZAÇÃO
				$data_de_atualizacao_inicio = trim(html_entity_decode($html[201])); 

				$data_de_atualizacao_aux = strstr($data_de_atualizacao_inicio, 'value=');

				$data_de_atualizacao_fim = strstr($data_de_atualizacao_aux, 'size=', true);

				$data_de_atualizacao = $this->after('value=', $data_de_atualizacao_fim);

				// COLETANDO DATA DE INCORPORAÇÃO DO PROTOCOLO
				$data_de_incoporacao_inicio = trim(html_entity_decode($html[205])); 

				$data_de_incoporacao_aux = strstr($data_de_incoporacao_inicio, 'value=');

				$data_de_incoporacao_fim = strstr($data_de_incoporacao_aux, 'size=', true);

				$data_de_incoporacao = $this->after('value=', $data_de_incoporacao_fim);

				$numero_do_protocolo = str_replace('"','',$numero_do_protocolo); 
				// $nome_da_empresa = str_replace('"','',$nome_da_empresa);
				$estado_do_processo = str_replace('"','',$estado_do_processo);
				$status_envio_rfb = str_replace('"','',$status_envio_rfb);
				$data_de_atualizacao = str_replace('"','',$data_de_atualizacao);
				$data_de_incoporacao = str_replace('"','',$data_de_incoporacao); 
		 		
				$validar = ("SELECT * FROM dtb_viabilidade_juceb WHERE `protocolo`='$numero_do_protocolo';");
		  		$qtd_validar = mysqli_query($con, $validar); 

				if (mysqli_num_rows($qtd_validar)) {
				
				$query = ("UPDATE dtb_viabilidade_juceb 
					SET `nome_da_empresa`='$nome_da_empresa' , 
						`estado_do_processo`='$estado_do_processo' , 
						`status_de_envio`='$status_envio_rfb' , 
						`data_atualizacao`='$data_de_atualizacao' , 
						`data_incorporacao`='$data_de_incoporacao' WHERE `protocolo`='$numero_do_protocolo';");
				 
				 $query = str_replace("\n", " ",$query);
				 mysqli_query($con, $query);

				} else  {

				echo "Contabilidade não cadastrou protocolos";

				}  

		 	} else {

		 		echo "Protocolo não obteve resultados";
			}  

		}
	}

	public function rBlankLines($str) {  
     $str = (preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $str));  
     $str = preg_replace('/( )+/', ' ', $str);    
     return $str;
	}

	public function pegarMeio($explode,$explode2,$replace,$trs){

		$pergunta = explode($explode, $trs);
		$pergunta = explode($explode2, $pergunta[0]);
		$pergunta = str_replace($replace, "", $pergunta[1]);
		return $pergunta;
	}

	public function multiexplode ($delimiters,$string) {
	    
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}

	public function after ($this_a, $inthat)
	{
	    if (!is_bool(strpos($inthat, $this_a)))
	    return substr($inthat, strpos($inthat,$this_a)+strlen($this_a));
	}

	public function after_last ($this_a, $inthat)
	{
	    if (!is_bool(strrevpos($inthat, $this_a)))
	    return substr($inthat, strrevpos($inthat, $this_a)+strlen($this_a));
	}

	public function before ($this_a, $inthat)
	{
	    return substr($inthat, 0, strpos($inthat, $this_a));
	}

	public function before_last ($this_a, $inthat)
	{
	    return substr($inthat, 0, strrevpos($inthat, $this_a));
	}

	public function between ($this_a, $that, $inthat)
	{
	    return before ($that, after($this_a, $inthat));
	}

	public function between_last ($this_a, $that, $inthat)
	{
	 return after_last($this_a, before_last($that, $inthat));
	}

	// use strrevpos function in case your php version does not include it
	public function strrevpos($instr, $needle)
	{
	    $rev_pos = strpos (strrev($instr), strrev($needle));
	    if ($rev_pos===false) return false;
	    else return strlen($instr) - $rev_pos - strlen($needle);
	}
}