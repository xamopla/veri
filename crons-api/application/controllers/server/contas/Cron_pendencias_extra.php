<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_pendencias_extra extends CI_Controller {

	public function buscar(){

		$banco = $this->uri->segment(4);
		
		define('HOST', 'localhost');  
		define('DBNAME', $banco);  
		define('USER', 'root');  
		define('PASSWORD', '12*Gr4p0D4t4ByT3SySV3r1*21');   

		try {
			$opcoes = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::ATTR_PERSISTENT => FALSE);
		    $db = new PDO("mysql:host=" . HOST . "; dbname=" . DBNAME . "; ", USER, PASSWORD, $opcoes);
		    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		}

		$consulta = $db -> query("SELECT id, inscricao_estadual, id_contador FROM dtb_empresas WHERE sync = 1 and flag_empresa_sem_ie is null and id >= 500 ORDER BY id ASC");

		while ($dados = $consulta -> fetch(PDO::FETCH_ASSOC)) {

			//echo $dados['id'] . ' - ' . $dados['inscricao_estadual'] . '<br>';
			$id_contador = $dados['id_contador'];
			// ---------------------------------------------------------------------------------------------------------

			$dadoss = http_build_query(array(
					'IE' => $dados['inscricao_estadual'],
					'B2' => 'IE  ->'
			));
			
			$contexto = stream_context_create(array(
					'http' => array(
							'method' => 'POST',
							'content' => $dadoss,
							'header' => "Content-type: application/x-www-form-urlencoded\r\n"
							. "Content-Length: " . strlen($dadoss) . "\r\n",
					)
			));
			
			$resposta = file('http://www.sefaz.ba.gov.br/scripts/cadastro/cadastroBa/result.asp', null, $contexto);	
				
			for ($i=0; $i <= 90 ; $i++) {
				unset($resposta[$i]);
			}
			
			$cnpj = trim(html_entity_decode(strip_tags($resposta[107])));
			$cnpj = preg_replace('/\s\s+/', ' ', substr($cnpj, 7));
			//echo "- CNPJ: $cnpj<br>";
			$array = array();
			
			if(trim($cnpj) == "" || trim($cnpj) == null){
				break;
			}else{
				$array['cnpj'] = $cnpj;
			
				$ie = trim(html_entity_decode(strip_tags($resposta[109])));
				$ie = preg_replace('/\s\s+/', ' ', substr($ie, 23));
				//echo "- Inscrição Estadual: $ie<br>";
				$array['ie'] = $ie;
				
				$contribuinte_mei = trim(html_entity_decode(strip_tags($resposta[114])));
				$contribuinte_mei = preg_replace('/\s\s+/', ' ', substr($contribuinte_mei, 48));
				//echo "..Contribuinte Micro Empreendedor Individual: $contribuinte_mei<br>";
				$array['mei'] = $contribuinte_mei;
				
				$razao_social = trim(html_entity_decode(strip_tags($resposta[119])));
				$razao_social = preg_replace('/\s\s+/', ' ', substr($razao_social, 16));
				//echo "- Razão Social: $razao_social<br>";
				$array['razao_social'] = $razao_social;
				
				if ($contribuinte_mei == 'Sim') {
				
					$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[128])));
					$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
					//echo "- Nome Fantasia: $nome_fantasia<br>";
					$array['nome_fantasia'] = $nome_fantasia;
				
					$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[132])));
					$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
					//echo "- Natureza Jurídica: $natureza_juridica<br>";
					$array['natureza_juridica'] = $natureza_juridica;
				
					$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[136])));
					$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
					//echo "- Unidade de Atendimento: $unidade_atendimento<br>";
					$array['unidade_atendimento'] = $unidade_atendimento;
				
					$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[139])));
					$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
					//echo "- Unidade de Fiscalização: $unidade_fiscalizacao<br><br>";
					$array['unidade_fiscalizacao'] = $unidade_fiscalizacao;
				
					//echo "Endereço<br>";
				
					$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[152])));
					$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
					//echo "- Logradouro: $logradouro_empresa<br>";
					$array['logradouro'] = $logradouro_empresa;
				
					$numero_empresa = trim(html_entity_decode(strip_tags($resposta[155])));
					$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
					//echo "- Número: $numero_empresa<br>";
					$array['numero'] = $numero_empresa;
				
					$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[156])));
					$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
					//echo "- Complemento: $complemento_empresa<br>";
					$array['complemento'] = $complemento_empresa;
				
					$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[159])));
					$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
					//echo "- Bairro/Distrito: $bairro_empresa<br>";
					$array['bairro'] = $bairro_empresa;
				
					$cep_empresa = trim(html_entity_decode(strip_tags($resposta[160])));
					$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
					//echo "- CEP: $cep_empresa<br>";
					$array['cep'] = $cep_empresa;
				
					$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[163])));
					$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
					//echo "- Município: $municipio_empresa<br>";
					$array['cidade'] = $municipio_empresa;
				
					$uf_empresa = trim(html_entity_decode(strip_tags($resposta[164])));
					$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
					//echo "- UF: $uf_empresa<br>";
					$array['uf'] = $uf_empresa;
				
					$telefone = trim(html_entity_decode(strip_tags($resposta[167])));
					$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
					//echo "- Telefone: $telefone<br>";
					$array['telefone'] = $telefone;
				
					$email = trim(html_entity_decode(strip_tags($resposta[168])));
					$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
					//echo "- E-mail: $email<br>";
					$array['email'] = $email;
				
					$referencia = trim(html_entity_decode(strip_tags($resposta[172])));
					$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
					//echo "- Referência: $referencia<br>";
					$array['referencia'] = $referencia;
				
					$localizacao = trim(html_entity_decode(strip_tags($resposta[173])));
					$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
					//echo "- Localização: $localizacao<br><br>";
					$array['localizacao'] = $localizacao;
				
				} else {
				
					$nome_fantasia = trim(html_entity_decode(strip_tags($resposta[126])));
					$nome_fantasia = preg_replace('/\s\s+/', ' ', substr($nome_fantasia, 16));
					//echo "- Nome Fantasia: $nome_fantasia<br>";
					$array['nome_fantasia'] = $nome_fantasia;
				
					$natureza_juridica = trim(html_entity_decode(strip_tags($resposta[130])));
					$natureza_juridica = preg_replace('/\s\s+/', ' ', substr($natureza_juridica, 21));
					//echo "- Natureza Jurídica: $natureza_juridica<br>";
					$array['natureza_juridica'] = $natureza_juridica;
				
					$unidade_atendimento = trim(html_entity_decode(strip_tags($resposta[134])));
					$unidade_atendimento = preg_replace('/\s\s+/', ' ', substr($unidade_atendimento, 23));
					//echo "- Unidade de Atendimento: $unidade_atendimento<br>";
					$array['unidade_atendimento'] = $unidade_atendimento;
				
					$unidade_fiscalizacao = trim(html_entity_decode(strip_tags($resposta[137])));
					$unidade_fiscalizacao = preg_replace('/\s\s+/', ' ', substr($unidade_fiscalizacao, 27));
					//echo "- Unidade de Fiscalização: $unidade_fiscalizacao<br><br>";
					$array['unidade_fiscalizacao'] = $unidade_fiscalizacao;
				
					//echo "Endereço<br>";
				
					$logradouro_empresa = trim(html_entity_decode(strip_tags($resposta[150])));
					$logradouro_empresa = preg_replace('/\s\s+/', ' ', substr($logradouro_empresa, 13));
					//echo "- Logradouro: $logradouro_empresa<br>";
					$array['logradouro'] = $logradouro_empresa;
				
					$numero_empresa = trim(html_entity_decode(strip_tags($resposta[153])));
					$numero_empresa = preg_replace('/\s\s+/', ' ', substr($numero_empresa, 10));
					//echo "- Número: $numero_empresa<br>";
					$array['numero'] = $numero_empresa;
				
					$complemento_empresa = trim(html_entity_decode(strip_tags($resposta[154])));
					$complemento_empresa = preg_replace('/\s\s+/', ' ', substr($complemento_empresa, 14));
					//echo "- Complemento: $complemento_empresa<br>";
					$array['complemento'] = $complemento_empresa;
				
					$bairro_empresa = trim(html_entity_decode(strip_tags($resposta[157])));
					$bairro_empresa = preg_replace('/\s\s+/', ' ', substr($bairro_empresa, 18));
					//echo "- Bairro/Distrito: $bairro_empresa<br>";
					$array['bairro'] = $bairro_empresa;
				
					$cep_empresa = trim(html_entity_decode(strip_tags($resposta[158])));
					$cep_empresa = preg_replace('/\s\s+/', ' ', substr($cep_empresa, 6));
					//echo "- CEP: $cep_empresa<br>";
					$array['cep'] = $cep_empresa;
				
					$municipio_empresa = trim(html_entity_decode(strip_tags($resposta[161])));
					$municipio_empresa = preg_replace('/\s\s+/', ' ', substr($municipio_empresa, 13));
					//echo "- Município: $municipio_empresa<br>";
					$array['cidade'] = $municipio_empresa;
				
					$uf_empresa = trim(html_entity_decode(strip_tags($resposta[162])));
					$uf_empresa = preg_replace('/\s\s+/', ' ', substr($uf_empresa, 5));
					//echo "- UF: $uf_empresa<br>";
					$array['uf'] = $uf_empresa;
				
					$telefone = trim(html_entity_decode(strip_tags($resposta[165])));
					$telefone = preg_replace('/\s\s+/', ' ', substr($telefone, 11));
					//echo "- Telefone: $telefone<br>";
					$array['telefone'] = $telefone;
				
					$email = trim(html_entity_decode(strip_tags($resposta[166])));
					$email = preg_replace('/\s\s+/', ' ', substr($email, 9));
					//echo "- E-mail: $email<br>";
					$array['email'] = $email;
				
					$referencia = trim(html_entity_decode(strip_tags($resposta[170])));
					$referencia = preg_replace('/\s\s+/', ' ', substr($referencia, 14));
					//echo "- Referência: $referencia<br>";
					$array['referencia'] = $referencia;
				
					$localizacao = trim(html_entity_decode(strip_tags($resposta[171])));
					$localizacao = preg_replace('/\s\s+/', ' ', substr($localizacao, 16));
					//echo "- Localização: $localizacao<br><br>";
					$array['localizacao'] = $localizacao;
				
				}
							
				//DADOS DO CONTADOR
				
				/*
				for ($i=90; $i <= 233 ; $i++) {
					unset($resposta[$i]);
				}
				*/
				$array['atividade_principal'] = '';
							
				$linhas = array();
				
				foreach ($resposta as $key => $value) {
					$text = strip_tags($value);
					$linhas[] = trim(html_entity_decode($text));
				}
				
				$texto = implode(' ', $linhas);
				
				$ini_dte = strpos($texto, 'Situação do DTE:');
				$texto1 = substr($texto, $ini_dte);
				$fim_dte = strpos($texto1, 'Data do Credenciamento:');
				
				$situacao_dte = trim(html_entity_decode(strip_tags(substr($texto1, 0, $fim_dte))));
				$situacao_dte = preg_replace('/\s\s+/', ' ', substr($situacao_dte, 21));
				$array['situacao_dte'] = $situacao_dte;
				
				$ini_conta_dte = strpos($texto1, 'Situação da Conta:');
				$texto2 = substr($texto1, $ini_conta_dte);
				$fim_conta_dte = strpos($texto2, 'Data da Criação da Conta:');
				
				$situacao_conta_dte = trim(html_entity_decode(strip_tags(substr($texto2, 0, $fim_conta_dte))));
				$situacao_conta_dte = preg_replace('/\s\s+/', ' ', substr($situacao_conta_dte, 23));
				$array['situacao_conta_dte'] = $situacao_conta_dte;
				
				$ini_condicao = strripos($texto2, 'Condição:');
				$texto3 = substr($texto2, $ini_condicao);
				$fim_condicao = strpos($texto3, 'Forma de pagamento:');
				
				$condicao = trim(html_entity_decode(strip_tags(substr($texto3, 0, $fim_condicao))));
				$condicao = preg_replace('/\s\s+/', ' ', substr($condicao, 13));
				$array['condicao'] = $condicao;
				
				$texto4 = substr($texto3, $fim_condicao);
				
				$ini_forma_pagamento = strpos($texto4, 'Forma de pagamento:');
				$texto5 = substr($texto4, $ini_forma_pagamento);
				$fim_forma_pagamento = strpos($texto5, 'Situação Cadastral Vigente:');
				
				$forma_pagamento = trim(html_entity_decode(strip_tags(substr($texto5, 0, $fim_forma_pagamento))));
				$forma_pagamento = preg_replace('/\s\s+/', ' ', substr($forma_pagamento, 21));
				$array['forma_pagamento'] = $forma_pagamento;

				$ini_dataintimacao = strpos($texto, 'Data desta Situação Cadastral:');
				$textoIntimacao = substr($texto, $ini_dataintimacao);
				$fim_dataintimacao = strpos($textoIntimacao, 'Motivo desta Situação Cadastral:');

				$data_intimacao = trim(html_entity_decode(strip_tags(substr($textoIntimacao, 0, $fim_dataintimacao))));
				$data_intimacao = preg_replace('/\s\s+/', ' ', substr($data_intimacao, 32));
				$array['data_intimacao'] = $data_intimacao;
				
				// $ini_motivo_situacao_cadastral = strpos($texto5, 'Motivo desta Situação Cadastral:');
				$ini_motivo_situacao_cadastral = strpos($texto, 'Motivo desta Situa');
				
				if ($ini_motivo_situacao_cadastral) {
					
					$texto6 = substr($texto5, $ini_motivo_situacao_cadastral);
					$fim_motivo_situacao_cadastral = strpos($texto6, 'Endereço de Correspondência');
				
					$motivo_situacao_cadastral = trim(html_entity_decode(strip_tags(substr($texto6, 0, $fim_motivo_situacao_cadastral))));
					$motivo_situacao_cadastral = preg_replace('/\s\s+/', ' ', substr($motivo_situacao_cadastral, 35));

					$ini_motivo_situacao_cadastral = $ini_motivo_situacao_cadastral + 2;
					$texto_aux = substr($texto, $ini_motivo_situacao_cadastral);

					$fim_motivo_situacao_cadastral = strpos($texto_aux, 'Endere');		
				
					$motivo_situacao_cadastral = trim(html_entity_decode(strip_tags(substr($texto_aux, 0, $fim_motivo_situacao_cadastral))));
					$motivo_situacao_cadastral = preg_replace('/\s\s+/', ' ', substr($motivo_situacao_cadastral, 32));
					$array['motivo_situacao_cadastral'] = $motivo_situacao_cadastral;
				
				} else {
					$array['motivo_situacao_cadastral'] = '';
				}
				
				$ini_atividade_economica = strripos($texto2, 'Atividade Econômica Principal:');
				$texto_atividade = substr($texto2, $ini_atividade_economica);
				$fim_atividade_economica = '';
				
				if (strpos($texto_atividade, 'Atividade Econômica Secundária')) {
					$fim_atividade_economica = strpos($texto_atividade, 'Atividade Econômica Secundária');
				} else {
					$fim_atividade_economica = strpos($texto_atividade, 'Unidade:');
				}
				
				$atividade_principal = trim(html_entity_decode(strip_tags(substr($texto_atividade, 0, $fim_atividade_economica))));
				$atividade_principal = preg_replace('/\s\s+/', ' ', substr($atividade_principal, 31));
				$array['atividade_principal'] = $atividade_principal;
				
				$cl = strpos($texto, 'Classificação CRC:');
				$texto17 = substr($texto, $cl);
				
				// CRC CONTADOR
				$crc = strpos($texto17, 'Tipo CRC:');
				$texto_crc_contador = substr($texto17, 0, $crc);
				$crc_contador = strripos($texto_crc_contador, 'CRC:');
				
				$crc_contador = substr($texto_crc_contador, $crc_contador);
				$crc_contador = trim(html_entity_decode(strip_tags($crc_contador)));
				$crc_contador = preg_replace('/\s\s+/', ' ', substr($crc_contador, 6));
				$array['crc_contador'] = preg_replace("/[^0-9]/", "", $crc_contador);
				
				$texto17 = substr($texto, $cl);
				$texto18 = substr($texto17, $crc);
				$nom = strpos($texto18, 'Nome:');
				$texto19 = substr($texto18, $nom);
				
				$resp = strpos($texto19, 'Responsável pela organização contábil');
				
				// NOME CONTADOR
				$nome_contador = trim(html_entity_decode(strip_tags(substr($texto19, 0, $resp))));
				$array['nome_contador'] = preg_replace('/\s\s+/', ' ', substr($nome_contador, 7));
				
				$texto20 = substr($texto19, $resp);
				$classi = strpos($texto20, 'Classificação CRC:');
				$texto21 = substr($texto20, $classi);
				
				// CRC RESPONSAVEL
				$crc_resp = strpos($texto21, 'Tipo CRC:');
				$texto_crc_responsavel = substr($texto21, 0, $crc_resp);
				$crc_responsavel = strripos($texto_crc_responsavel, 'CRC:');
				
				$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($texto_crc_responsavel, $crc_responsavel));
				$crc_responsavel = trim(html_entity_decode(strip_tags($crc_responsavel)));
				$crc_responsavel = preg_replace('/\s\s+/', ' ', substr($crc_responsavel, 5));
				$array['crc_responsavel'] = preg_replace("/[^0-9]/", "", $crc_responsavel);
				
				// NOME RESPONSAVEL
				$nomeresp = strpos($texto21, 'Nome:');
				$texto22 = substr($texto21, $nomeresp);
				
				$endresp = strpos($texto22, 'Endereço');
				$nome_responsavel = trim(html_entity_decode(strip_tags(substr($texto22, 0, $endresp))));
				$array['nome_responsavel'] = preg_replace('/\s\s+/', ' ', substr($nome_responsavel, 7));
				
				//----- PARTE 2 -----
				
				$dado_dois = http_build_query(array(
						'num_inscricao' => $dados['inscricao_estadual']
				));
				
				$contexto_dois = stream_context_create(array(
						'http' => array(
								'method' => 'POST',
								'content' => $dado_dois,
								'header' => "Content-type: application/x-www-form-urlencoded\r\n"
								. "Content-Length: " . strlen($dado_dois) . "\r\n",
						)
				));
				
				$resposta_dois = file_get_contents('http://www.sefaz.ba.gov.br/scripts/antc/result_consulta.asp', null, $contexto_dois);
				
				$lines = array();
				
				preg_match_all('#<tr[^>]*>(.*?)</tr>#is', $resposta_dois, $lines);
				
				unset($lines[0][0]);
				unset($lines[0][1]);
				unset($lines[0][2]);
				unset($lines[0][4]);
				unset($lines[0][8]);
				unset($lines[0][11]);
				unset($lines[0][12]);
				
				$results = array();
				foreach ($lines[0] as $k => $line) {
					preg_match_all('#<TR[^>]*>(.*?)</TR>#is', $line, $cell);
				
					foreach ($cell[1] as $cell) {
						$text = strip_tags($cell);
						$results[$k][] = trim(html_entity_decode($text));
					}
				}
				
				$inicio_situacao_cadastral = strripos($results[3][0], 'Situação Cadastral:');
				$situacao_cadastral = substr($results[3][0], $inicio_situacao_cadastral);
				
				$array['situacao_cadastral'] = preg_replace('/\s\s+/', ' ', substr($situacao_cadastral, 22));
				$array['credenciado'] = preg_replace('/\s\s+/', ' ', substr($results[9][0], 13));
				$array['motivo'] = preg_replace('/\s\s+/', ' ', substr($results[10][0], 10));

				// ---------------------------------------------------------------------------------------------------------

				// 0 -> DESVINCULADO; 1 -> VINCULADO
				// CRC RESPONSÁVEL, CRC CONTADOR, ID CONTADOR
				$vinc = $db -> query("SELECT COUNT(id) AS valor FROM contadores WHERE crc = '$array[crc_contador]' OR crc = '$array[crc_responsavel]' LIMIT 1") or die($link -> error);
				$row_vinc = $vinc -> fetch(PDO::FETCH_ASSOC);
				
				if ($row_vinc['valor'] > 0){
					$vinculoContador = 1;
				} else {
					$vinculoContador = 0;
				}

				$id = $dados['id'];
				
				date_default_timezone_set('America/Bahia');
				$dataatualizacao = date("Y-m-d H:i:s");
				
				$sql_alt = "UPDATE dtb_empresas 
				SET razao_social = '$array[razao_social]',
				nome_fantasia = '$array[nome_fantasia]',
				inscricao_estadual_completo = '$array[ie]',
				cnpj_completo = '$array[cnpj]',
				natureza_juridica = '$array[natureza_juridica]',
				unidade_atendimento = '$array[unidade_atendimento]',
				unidade_fiscalizacao = '$array[unidade_fiscalizacao]',
				cep = '$array[cep]',
				logradouro = '$array[logradouro]',
				numero = '$array[numero]',
				complemento = '$array[complemento]',
				bairro = '$array[bairro]',
				cidade = '$array[cidade]',
				uf = '$array[uf]',
				referencia = '$array[referencia]',
				localizacao = '$array[localizacao]',
				telefone = '$array[telefone]',
				email = '$array[email]',
				situacao_cadastral = '$array[situacao_cadastral]',
				situacao = '$array[credenciado]',
				motivo = '$array[motivo]',
				situacao_dte = '$array[situacao_dte]',
				situacao_conta_dte = '$array[situacao_conta_dte]',
				atividade_principal = '$array[atividade_principal]',
				condicao = '$array[condicao]',
				forma_pagamento = '$array[forma_pagamento]',
				motivo_situacao_cadastral = '$array[motivo_situacao_cadastral]',
				nome_contador = '$array[nome_contador]',
				crc_contador = '$array[crc_contador]',
				nome_responsavel = '$array[nome_responsavel]',
				crc_responsavel = '$array[crc_responsavel]',
				data_intimacao = '$array[data_intimacao]',
				data_atualizacao = '$dataatualizacao',
				vinculo_contador = $vinculoContador
				WHERE id = $id";
				
				 $q = $db -> prepare($sql_alt);
				 $q -> execute();
			}
		}

	}
}