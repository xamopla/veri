<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'libraries/Simple_html_dom.php');

class Cron_certidao_sefaz extends CI_Controller {

    private $curl;

    CONST CPF = 1;
    CONST CNPJ = 2;
    CONST INSCRICAO_ESTADUAL = 3;
    
	public function buscar(){

        $this->config->load('certidao_sefaz_config');

		$banco = $this->uri->segment(4);

		$host ="localhost";
        $user = "root";
        $pass = "12*Gr4p0D4t4ByT3SySV3r1*21";
        $db = $banco;

		$con = mysqli_connect($host, $user, $pass, $db);
		mysqli_set_charset($con,"utf8");
		mysqli_query($con,"SET NAMES 'utf8'");
		mysqli_query($con,'SET character_set_connection=utf8');
		mysqli_query($con,'SET character_set_client=utf8');
		mysqli_query($con,'SET character_set_results=utf8');

        $folder_pdf = FCPATH . 'pdf-certidao/certidaosefaz/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

		$query = ("SELECT id, cnpj,cnpj_completo, inscricao_estadual,inscricao_estadual_completo, razao_social FROM dtb_empresas WHERE id < 500");
		$consulta = mysqli_query($con, $query);

		while($dados = mysqli_fetch_assoc($consulta)){
			$cnpj = $dados['cnpj'];
            $cnpj_completo = $dados['cnpj_completo'];

            $id_empresa = $dados['id'];
			$inscricao_estadual = $dados['inscricao_estadual'];
            $inscricao_estadual_completo = $dados['inscricao_estadual_completo'];

            $razao_social = $dados['razao_social'];

			$tipo_documento = self::INSCRICAO_ESTADUAL;
            $numero_documento = $inscricao_estadual;

            if ( !isset($dados['inscricao_estadual']) || empty($dados['inscricao_estadual']) )
            {
                $tipo_documento = self::CNPJ;
                $numero_documento = $cnpj;
            }

            $status_situacao = $this->get_status_certidao_sefaz( $tipo_documento, $numero_documento );

            $path = $this->baixar_certidao_sefaz($tipo_documento, $numero_documento, $banco);


            $caminho_download = str_replace("/var/www/html", "https://veri.com.br", $path);

            $query_validar = ("SELECT * FROM dtb_certidao_sefaz WHERE `cnpj`='$cnpj';");

            $qtd_validar = mysqli_query($con, $query_validar);

            $data_pdf = null;

            if (mysqli_num_rows($qtd_validar)) {
                $stmt = mysqli_prepare($con, "UPDATE dtb_certidao_sefaz SET cnpj =?, cnpj_completo =?,inscricao_estadual=?, inscricao_estadual_completo=?, razao_social=?,status=?,caminho_download=?,data_pdf=? WHERE cnpj = ?");
                mysqli_stmt_bind_param($stmt , 'sssssssbs', $cnpj,$cnpj_completo, $inscricao_estadual,$inscricao_estadual_completo, $razao_social, $status_situacao, $caminho_download, $data_pdf, $cnpj);

//                grava o pdf em bytes
                $fp = fopen($path, "r");
                while (!feof($fp)) {
                    $stmt->send_long_data(7, fread($fp, filesize($path)));
                }

                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }else{
                $stmt = mysqli_prepare($con, "INSERT INTO dtb_certidao_sefaz(cnpj, cnpj_completo, inscricao_estadual, inscricao_estadual_completo , razao_social, status, data_pdf, caminho_download) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
                mysqli_stmt_bind_param($stmt , 'ssssssbs', $cnpj,$cnpj_completo, $inscricao_estadual,$inscricao_estadual_completo, $razao_social, $status_situacao, $data_pdf, $caminho_download);

//                grava o pdf em bytes
                $fp = fopen($path, "r");
                while (!feof($fp)) {
                    $stmt->send_long_data(6, fread($fp, filesize($path)));
                }

                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }
		}
	}

    public function get_status_certidao_sefaz( $tipo_documento, $numero_documento ){
        $this->post_login($tipo_documento, $numero_documento);

        $url = 'https://sistemas.sefaz.ba.gov.br/sistemas/sigat/Default.aspx?Aplicacao=SIGAT&Modulo=GERAL&Tela=VisualizarRelatorio&ExportButton=false';
        curl_setopt($this->curl, CURLOPT_URL , $url );
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_SSLVERSION,4); 
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 150);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'assets/'.$numero_documento.'.txt');

        $response = curl_exec( $this->curl );
        $html = new Simple_html_dom();
        $page = $this->converterCaracterEspecial( $response );
        $html->load($page);
        $resultado = $html->find("table[width=720px]", 0);

        $resultado_texto = !is_null($resultado) ? $resultado->plaintext : '';

        $status_situacao = 'Irregular';
        if (strpos($resultado_texto, 'Fica certificado que n ã o constam') !== false)
            $status_situacao = 'Regular';
        

        if(curl_errno($this->curl)){
            echo curl_error($this->curl);
        }

        return $status_situacao;
    }
    
    public function baixar_certidao_sefaz($tipo_documento, $numero_documento, $banco){
        $url_login = 'https://sistemas.sefaz.ba.gov.br/sistemas/sigat/Default.aspx?Aplicacao=SIGAT&Modulo=GERAL&Tela=VisualizarRelatorio&ExportButton=false' ;
        $post_str = $this->post_login($tipo_documento, $numero_documento);

// 		Faz login e pega o cookie de sessao
        curl_setopt($this->curl, CURLOPT_URL , $url_login );
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_SSLVERSION,4);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 150);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'assets/'.$numero_documento.'.txt');
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $post_str);
        $path = "{$this->config->item('caminho_pasta_pdf')}/".$banco."/certidao-sefaz-{$numero_documento}.pdf";
        $fp = fopen ($path, 'w+');
        curl_setopt($this->curl, CURLOPT_FILE, $fp);
        curl_exec( $this->curl );

        if( curl_errno( $this->curl ) )
        {
            echo curl_error( $this->curl );
            return false;
        }
        
        return $path;
    }

    function post_login($tipo_documento, $numero_documento){
        $url_login = 'https://sistemas.sefaz.ba.gov.br/sistemas/sigat/Default.Aspx?Modulo=CREDITO&Tela=DocEmissaoCertidaoInternet&limparSessao=1&sts_link_externo=2' ;

        $post_str = $this->get_post_string($tipo_documento, $numero_documento);

        $this->curl = curl_init();
// 		Faz login e pega o cookie de sessao
        curl_setopt($this->curl, CURLOPT_URL , $url_login );
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0); //has no impact on the result
        curl_setopt($this->curl, CURLOPT_SSLVERSION,4); //fix for PHP 7 and xxxxxxx.xxxxx.com
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 150);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'assets/'.$numero_documento.'.txt');
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $post_str);

        $response = curl_exec( $this->curl );
        $html = new Simple_html_dom();
        $page = $this->converterCaracterEspecial( $response );
        $html->load($page);
        $nodes = $html->find("input[type=hidden]");
        $vals = array();
        foreach ($nodes as $node) {
            $val = $node->value;
            if(!empty($val) && !is_null($val))
                $vals[] = $val;
        }

        $__VIEWSTATE = $vals[0];
        $__VIEWSTATEGENERATOR = $vals[1];

        if ( curl_errno($this->curl) )
            echo curl_error( $this->curl );

        return array('__EVENTTARGET' => '_ctl1:_ctl0:crv_relatorio:_ctl1:_ctl0:_ctl1',
            '__EVENTARGUMENT' => 'tb=crexport&text=PDF&range=false&from=1&to=1',
            '__VIEWSTATE' => $__VIEWSTATE,
            '__VIEWSTATEGENERATOR' => $__VIEWSTATEGENERATOR);
    }

    public function get_post_string($tipo_documento, $numero_documento)
    {
        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_VERBOSE,1);
        curl_setopt($curl, CURLOPT_URL , 'https://sistemas.sefaz.ba.gov.br/sistemas/sigat/Default.Aspx?Modulo=CREDITO&Tela=DocEmissaoCertidaoInternet&limparSessao=1&sts_link_externo=2' );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); //has no impact on the result
        curl_setopt($curl, CURLOPT_SSLVERSION,4); //fix for PHP 7 and xxxxxxx.xxxxx.com
        curl_setopt($curl, CURLOPT_TIMEOUT, 150);
        $response = curl_exec( $curl );
        if(curl_errno($curl)){
            echo curl_error($curl);
        }

        $html = new Simple_html_dom();
        $page = $this->converterCaracterEspecial( $response );
        $html->load($page);
        $nodes = $html->find("input[type=hidden]");
        $vals = array();
        foreach ($nodes as $node) {
            $val = $node->value;
            if(!empty($val) && !is_null($val))
                $vals[] = $val;
        }

        $__VIEWSTATE = $vals[0];
        $__VIEWSTATEGENERATOR = $vals[1];

        $post_str = http_build_query([
            '__VIEWSTATE' => $__VIEWSTATE,
            '__VIEWSTATEGENERATOR' => $__VIEWSTATEGENERATOR,
            '_ctl1:_ctl1:num_inscricao_estadual' => $tipo_documento == self::INSCRICAO_ESTADUAL ? $numero_documento : '',
            '_ctl1:_ctl1:num_cnpj' => $tipo_documento == self::CNPJ ? $numero_documento : '',
            '_ctl1:_ctl1:num_cpf' => $tipo_documento == self::CPF ? $numero_documento : '',
            '_ctl1:_ctl1:btn_Imprimir.x' => '10',
            '_ctl1:_ctl1:btn_Imprimir.y'=> '14',
            '_ctl1:_ctl1:txt_validacao'=> ''
        ]);
        curl_close( $curl );

        return $post_str;
    }

    public function converterCaracterEspecial($text){
        return html_entity_decode($text, ENT_QUOTES, "utf-8");
    }
}