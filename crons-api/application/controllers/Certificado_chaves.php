<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/Certificate/Pkcs12.php');

class Certificado_chaves extends CI_Controller {
    
    public function atualizar_todos(){
        $banco = $this->uri->segment(3);

        $this->load->model('certificado_model');
        $this->load->model('certificadocontador_model');

        $cerficados = $this->certificado_model->get($banco);
        $cerficados_contador = $this->certificadocontador_model->get($banco);
        $pkcs = new Pkcs12(APPPATH . 'libraries/Certificate/certificados_clientes/');

        /*
         * Atualiza as chaves dos certificados das empresas
         */
        foreach ($cerficados as $cerficado){
            try {
                $chaves = $this->gerar_chaves($cerficado->caminho_arq, $cerficado->pass, $pkcs);
                $erro_leitura = false;
            }catch (Exception $e){
                $erro_leitura = true;
            }
            if ($erro_leitura)
                continue;
            $cert_key = $chaves['cert_key'];
            $pub_key = $chaves['pub_key'];
            $pri_key = $chaves['pri_key'];

            $this->certificado_model->setCnpj($cerficado->cnpj_data);
            $this->certificado_model->setCertKey(file_get_contents($cert_key));
            $this->certificado_model->setPubKey(file_get_contents($pub_key));
            $this->certificado_model->setPriKey(file_get_contents($pri_key));
            $this->certificado_model->atualizar_chaves($banco);
        }

        /*
         * Atualiza as chaves dos certificados dos contadores
         */
        foreach ($cerficados_contador as $cerficado){
            try {
                $chaves = $this->gerar_chaves($cerficado->caminho_arq, $cerficado->pass, $pkcs);
                $erro_leitura = false;
            }catch (Exception $e){
                $erro_leitura = true;
            }
            if ($erro_leitura)
                continue;

            $cert_key = $chaves['cert_key'];
            $pub_key = $chaves['pub_key'];
            $pri_key = $chaves['pri_key'];
            $cnpj_data = $chaves['cnpj_data'];

            $this->certificadocontador_model->setId($cerficado->id);
            $this->certificadocontador_model->setCertKey(file_get_contents($cert_key));
            $this->certificadocontador_model->setPubKey(file_get_contents($pub_key));
            $this->certificadocontador_model->setPriKey(file_get_contents($pri_key));
            $this->certificadocontador_model->setCnpj($cnpj_data);
            $this->certificadocontador_model->atualizar_chaves($banco);
        }
    }

    function gerar_chaves($caminho_certificado, $cerficado_senha, $pkcs){
//      Gera a cadeia de cerficados do ecac
        $aCerts[] = APPPATH . 'libraries/Certificate/cadeia_certificados_receita/acrfbv3.cer';
        $aCerts[] = APPPATH . 'libraries/Certificate/cadeia_certificados_receita/acserprorfbv3.cer';
        $aCerts[] = APPPATH . 'libraries/Certificate/cadeia_certificados_receita/icpbrasilv2.cer';

        $pkcs->loadPfxFile($caminho_certificado, $cerficado_senha);
//      adiciona a cadeia ao certificado
        $pkcs->aadChain($aCerts);

        return array(
            'cert_key' => $pkcs->certKeyFile,
            'pub_key' => $pkcs->pubKeyFile,
            'pri_key' => $pkcs->priKeyFile,
            'cnpj_data' => $pkcs->cnpj
        );
    }
}
