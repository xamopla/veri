<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Das_teste extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // $this->load->model('certificadocontador_model');
        // $this->load->model('contadorprocuracao_model');
        $this->load->model('das_model');
    }

	public function gerar_das_function(){
        $banco = 'onixcontabilidade';$this->input->post('banco');
        $numero_declaracao = '22557165202103001';$this->input->post('numero_declaracao');
        $competencia = '03/2021';trim(str_replace('PA', "", $this->input->post('competencia')));
        $cnpj = '22557165000100';$this->input->post('cnpj');

        $id_empresa = $this->das_model->find_empresa_by_cnpj($banco, $cnpj)->id;
        $cerficados = $this->das_model->get_aux($id_empresa, $banco);

        if(!empty($cerficados)){
            $retorno = $this->gerar_das_procuracao($numero_declaracao, $competencia, $cnpj, $banco, $cerficados);
            echo $retorno;
        }else{
            $retorno = $this->gerar_das($numero_declaracao, $competencia, $cnpj, $banco);
            echo $retorno;
        }
    }

    public function gerar_das($numero_declaracao, $competencia, $cnpj, $banco){

        $certificado = $this->das_model->find_certificado($cnpj, $banco);
        $folder_pdf = FCPATH . 'crons-api/pdf-das-ecac/'.$banco.'/' . md5(uniqid(rand(), true));
        // $folder_pdf = FCPATH . 'pdf-das-ecac/'.$banco.'/'.str_replace('/', '-', $competencia);

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $params = array('caminho_certificado' => str_replace('//','/', $certificado->caminho_arq ) ,
            'cerficado_senha' => $certificado->pass,
            'caminho_da_pasta_pdfs' => $folder_pdf);
        $this->load->library('Ecac_robo_library_dctf', $params, 'ecac_robo_library');

        if(!$this->ecac_robo_library->acesso_valido()){
            unset($this->ecac_robo_library);
            echo json_encode(array("error"=> true, "mensagem"=>"erro1"));
        }
        $caminho_download = $this->ecac_robo_library->gerar_das($competencia);
        if($caminho_download != ""){

            $aux_dir_dec = str_replace("/var/www/html", "",$caminho_download);
            $cami_aux_dec = "https://veri-sp.com.br".$aux_dir_dec;

            $caminho_download = $cami_aux_dec;

            $this->das_model->update_caminho_download_das($caminho_download , $numero_declaracao, $banco);
            unset($this->ecac_robo_library);
            return $caminho_download;
        }else{
            unset($this->ecac_robo_library);
            return "ERRO1";
        }
    }

    public function gerar_das_procuracao($numero_declaracao, $competencia, $cnpj, $banco, $cerficados){

        $folder_pdf = FCPATH . 'crons-api/pdf-das-ecac/'.$banco.'/' . md5(uniqid(rand(), true));
        // $folder_pdf = FCPATH . 'pdf-das-ecac/'.$banco.'/'.str_replace('/', '-', $competencia);

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');

        //$cerficados = $this->certificadocontador_model->get();

        foreach ($cerficados as $cerficado_contador) {

            $params = array('caminho_certificado' => str_replace('//','/', $cerficado_contador->caminho_arq ),
                'cerficado_senha' => $cerficado_contador->pass,
                'caminho_da_pasta_pdfs' => $folder_pdf);

            $this->load->library('Ecac_robo_library_dctf', $params);

            /**
             * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
             */
            if (!$this->ecac_robo_library_dctf->acesso_valido()) {
                unset($this->ecac_robo_library_dctf);
                continue;
            }

            $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj, $banco);

            if( $empresa ){

                $validado = $this->ecac_robo_library_dctf->trocar_perfil($cnpj);

                if(! $validado){
                    echo "ERRO2";
                    continue;
                }

                $caminho_download = $this->ecac_robo_library_dctf->gerar_das($competencia, $cnpj);
                if($caminho_download != ""){

                    $aux_dir_dec = str_replace("/var/www/html", "",$caminho_download);
                    $cami_aux_dec = "https://veri-sp.com.br".$aux_dir_dec;

                    $caminho_download = $cami_aux_dec;

                    $this->das_model->update_caminho_download_das($caminho_download , $numero_declaracao, $banco);
                    unset($this->ecac_robo_library_dctf);
                    return $caminho_download;
                    break;
                }else{
                    unset($this->ecac_robo_library_dctf);
                    return "ERRO3";
                }

                
            }
        }

        unset($this->ecac_robo_library_dctf);
    }

}
