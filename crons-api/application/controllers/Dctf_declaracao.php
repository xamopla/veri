<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Dctf_declaracao extends CI_Controller {

	public function buscar_declaracao(){
        $cnpj = $this->input->post('cnpj');
        $periodo = $this->input->post('periodo');
        $banco = $this->input->post('banco');

        $this->load->model('certificadocontador_model');
        $this->load->model('contadorprocuracao_model');
        $this->load->model('dctf_model');

        $id_empresa = $this->dctf_model->find_empresa_by_cnpj($banco, $cnpj)->id;
        $cerficados = $this->dctf_model->get_aux($id_empresa, $banco);

        $folder_pdf = FCPATH . 'pdf-dctf-ecac/declaracaodctf/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }
        

        //verifica se existe procuração para a empresa
        if(!empty($cerficados)){
            foreach ($cerficados as $cerficado_contador) {

                $params = array('caminho_certificado' => str_replace('//','/', $cerficado_contador->caminho_arq ) ,
                    'cerficado_senha' => $cerficado_contador->pass,
                    'caminho_da_pasta_pdfs' => $folder_pdf);

                $this->load->library('Ecac_robo_library_dctf', $params);

                /**
                 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
                 */
                if (!$this->ecac_robo_library_dctf->acesso_valido()) {
                    unset($this->ecac_robo_library_dctf);
                    continue;
                }

                $empresa = $this->contadorprocuracao_model->buscar_empresa($cerficado_contador->id_contador, $cnpj, $banco);

                if( $empresa ){
                    
                    $validado = $this->ecac_robo_library_dctf->trocar_perfil($cnpj);
                    
                    if(! $validado){
                        echo "ERRO2";
                        continue;
                    }

                    $caminho_download = $this->ecac_robo_library_dctf->get_dctf_declaracao_individual($periodo);
                    if($caminho_download != ""){
                        $this->dctf_model->update_caminho_download($caminho_download , $periodo, $cnpj, $banco);
                        unset($this->ecac_robo_library_dctf);
                        echo $caminho_download;
                        break;
                    }else{
                        unset($this->ecac_robo_library_dctf);
                        echo "ERRO";
                    }
                }
            }

            unset($this->ecac_robo_library_dctf);
        }else{
            $certificado = $this->dctf_model->find_certificado($cnpj, $banco);

            if( $certificado ){

                $params = array('caminho_certificado' => str_replace('//','/', $certificado->caminho_arq ) ,
                    'cerficado_senha' => $certificado->pass,
                    'caminho_da_pasta_pdfs' => $folder_pdf);

                $this->load->library('Ecac_robo_library_dctf', $params);

                /**
                 * Verifica se o acesso foi validado com sucesso, caso contrário pula para o próximo
                 */
                if (!$this->ecac_robo_library_dctf->acesso_valido()) {
                    unset($this->ecac_robo_library_dctf);
                    echo "ERRO";
                }

                $caminho_download = $this->ecac_robo_library_dctf->get_dctf_declaracao_individual($periodo);
                if($caminho_download != ""){
                    $this->dctf_model->update_caminho_download($caminho_download , $periodo, $cnpj, $banco);
                    unset($this->ecac_robo_library_dctf);
                    echo $caminho_download;
                }else{
                    unset($this->ecac_robo_library_dctf);
                    echo "ERRO";
                }

            }else{
                echo "ERRO";
            }

        }
        
    }
}
