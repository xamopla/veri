<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class Dec_ler extends CI_Controller {

    function chamar_api( $url ){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_URL, $url);
      $result=curl_exec($ch);
      curl_close($ch);
      return json_decode($result, true);
    }

    public function ler_function(){
      $this->load->model('das_model');

      $banco = $this->input->post('banco');
      $id = $this->input->post('id');
      $tipo = $this->input->post('tipo');
      $cnpj = $this->input->post('cnpj');

      // $id_empresa = $this->das_model->find_empresa_by_cnpj($banco, $cnpj)->id;
      $cerficados = $this->das_model->find_all_cert($banco);
      $certificado_individual = $this->das_model->find_certificado($cnpj, $banco);

      if(!empty($certificado_individual)){
        if($tipo == 1){
          echo $this->ler_mensagem($banco, $id, $cnpj);
        }else{
          echo $this->ler_comunicado($banco, $id, $cnpj);
        }
      }elseif(!empty($cerficados)){
        if($tipo == 1){
          echo $this->ler_mensagem_procuracao($banco, $id, $cerficados, $cnpj);
        }else{
          echo $this->ler_comunicado_procuracao($banco, $id, $cerficados, $cnpj);
        }
      }
      else{
        echo "erro";
      }
    }

    function ler_comunicado_procuracao($banco, $id, $cerficados, $cnpj){
        $banco = $banco;
        $id_comunicado = $id;

        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('Dec_caixapostal_mensagens_model');
        $this->load->model('Dec_caixapostal_comunicados_model');

        date_default_timezone_set('America/Bahia');

        // $cerficados = $this->certificado->get($banco);
        foreach ($cerficados as $cerficado){
			$params = array(
                'banco' => $banco ,
                'cnpj' => $cerficado->cnpj_data,
            );
			$this->load->library('Dec_library', $params );

            $empresas_com_procuracao = $this->contadorprocuracao->buscar_empresas_vinculadas($banco, $cerficado->id_contador);
            foreach ($empresas_com_procuracao as $empresa){
                if (is_null($empresa->cnpj) || $empresa->cnpj != $cnpj )
                    continue;
                $comunicado = $this->Dec_caixapostal_comunicados_model->findById($id_comunicado, $banco);
				$resultado = $this->Dec_library->ler_comunicado();
                if ( $resultado ){
                    $pathArquivo = '';
                    $this->Dec_caixapostal_comunicados_model->update_lido($banco, $id_comunicado, $pathArquivo);

                    return $pathArquivo;
                }
            }
            unset($this->dec_library);
        }
    }

    function ler_mensagem_procuracao($banco, $id, $cerficados, $cnpj){
        $id_mensagem = $id;

        $this->load->model('certificadocontador_model', 'certificado');
        $this->load->model('contadorprocuracao_model', 'contadorprocuracao');
        $this->load->model('Dec_caixapostal_mensagens_model');

        date_default_timezone_set('America/Bahia');
		$folder_pdf = FCPATH . 'pdf-dec/mensagem/'.$banco.'/';

		if (!file_exists($folder_pdf)) {
			mkdir($folder_pdf, DIR_WRITE_MODE, true);
		}
        // $cerficados = $this->certificado->get($banco);
        foreach ($cerficados as $cerficado){
			$params = array(
                'banco' => $banco ,
                'cnpj' => $cerficado->cnpj_data,
            );
			$this->load->library('Dec_library', $params );
			$mensagem = $this->Dec_caixapostal_mensagens_model->findById($id_mensagem, $banco);
			$pathArquivo =  $this->dec_library->ler_mensagem($cnpj, $mensagem->identificacao);
			if ( $pathArquivo ){
				$this->Dec_caixapostal_mensagens_model->update_lido($banco, $id_mensagem, $pathArquivo);
				return str_replace('/var/www/html', '', $pathArquivo);
			}
            unset($this->dec_library);
        }
    }

    function ler_comunicado($banco, $id, $cnpj){
        $banco = $banco;
        $id_comunicado = $id;

        $this->load->model('certificado_model', 'certificado');
        $this->load->model('Dec_caixapostal_mensagens_model');
        $this->load->model('Dec_caixapostal_comunicados_model');

        date_default_timezone_set('America/Bahia');

        $cerficado = $this->das_model->find_certificado($cnpj, $banco);

        $cnpj = $cerficado->cnpj_data;
        $comunicado = $this->Dec_caixapostal_comunicados_model->findById($id_comunicado, $banco);
        $dados = array(
            'cnpj' => $cnpj,
            'banco' => $banco,
            'id' => $id_comunicado,
        );
        $url =  "http://191.252.193.86/api/v1/dec-sp/ler-comunicado?".http_build_query($dados);
        $resultado = $this->chamar_api($url);
        if ( $resultado && isset($resultado['file_name']) && !is_null($resultado['file_name'])){
            $pathArquivo = $this->baixar_pdf($banco, $resultado['path_download'], $resultado['file_name']);
            $this->Dec_caixapostal_comunicados_model->update_lido($banco, $id_comunicado, $pathArquivo);

            return $pathArquivo;
        }
    }

    function ler_mensagem($banco, $id, $cnpj){
        $banco = $banco;
        $id_mensagem = $id;

        $this->load->model('certificado_model', 'certificado');

        $this->load->model('Dec_caixapostal_mensagens_model');

        date_default_timezone_set('America/Bahia');

        $cerficado = $this->das_model->find_certificado($cnpj, $banco);
        
        $cnpj = $cerficado->cnpj_data;
        $mensagem = $this->Dec_caixapostal_mensagens_model->findById($id_mensagem, $banco);
        $dados = array(
            'cnpj' => $cnpj,
            'banco' => $banco,
            'id' => $id_mensagem,
        );
        $url =  "http://191.252.193.86/api/v1/dec-sp/ler-mensagem?".http_build_query($dados);
        $resultado = $this->chamar_api($url);
        if ( $resultado && isset($resultado['file_name']) && !is_null($resultado['file_name'])){
            $pathArquivo = $this->baixar_pdf($banco, $resultado['path_download'], $resultado['file_name']);
            $this->Dec_caixapostal_mensagens_model->update_lido($banco, $id_mensagem, $pathArquivo);

            return $pathArquivo;
        }
    }
    
    public function baixar_pdf($banco, $url, $name){
        $folder_pdf = FCPATH . 'pdf-caixapostal-dec/'.$banco.'/';

        if (!file_exists($folder_pdf)) {
            mkdir($folder_pdf, DIR_WRITE_MODE, true);
        }

        $ch = curl_init();
        $source = $url;
        curl_setopt($ch, CURLOPT_URL, $source);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        $pathArquivo = $folder_pdf . $name;
        $file = fopen($pathArquivo, "w+");
        fputs($file, $data);
        fclose($file);
        return str_replace('/var/www/html/', '', $pathArquivo);
    }
}
