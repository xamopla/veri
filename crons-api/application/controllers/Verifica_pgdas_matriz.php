<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifica_pgdas_matriz extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Verifica_pgdas_matriz/processar

        $banco = $this->uri->segment(3);
        $this->load->model('verifica_pgdas_matriz_model');

        $registros = $this->verifica_pgdas_matriz_model->busca_empresas_sem_pgdas($banco);

        foreach ($registros as $r) {

            $cnpj_base = substr($r->cnpj,0,8);

            $pgdas_filial = $this->verifica_pgdas_matriz_model->busca_pgdas_filial($banco, $cnpj_base);

            foreach ($pgdas_filial as $p) {
                $this->verifica_pgdas_matriz_model->insere_das($banco, $p, $r->cnpj);
            }
            echo $cnpj_base;
            echo "<br>";
        }

    
    }

}
