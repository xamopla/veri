<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Declaracao_gfip_ausente extends CI_Controller {

    public function processar(){
        //http://localhost/veri-sp-1.0/Api-Crons/Declaracao_ausente/processar
        // $banco = $this->uri->segment(3);
        include ( 'PdfToText/PdfToText.phpclass' ) ;

        $banco = $this->uri->segment(3);
        $this->load->model('declaracao_ausente_model');
        $this->load->model('declaracao_ausente_gfip_model');

        $registros = $this->declaracao_ausente_model->busca_situacao_fiscal($banco);
        $pdf    =  new PdfToText() ;

        foreach ($registros as $e) {

            if(!empty($e->caminho_download)){
                try{

                    //GFIP (Período de Apuração) CNPJ/CEI: 35.742.200/0001-02 2019 - DEZ 13º
                    //                                                        2020 - JAN
                    // $caminho_teste = 'https://veri-sp.com.br/crons-api/pdf-certidao-ecac/certidaoecac/gasparcontabilidade//situa%C3%A7%C3%A3o-fiscal-2021-06-04-35742200000102.pdf';


                    //DASN SIMEI (Ano-Calendário) 2020
                    //GFIP (Período de Apuração) CNPJ/CEI: 34.562.483/0001-30 2021 - FEV
                    $caminho_teste = 'https://veri-sp.com.br/crons-api/pdf-certidao-ecac/certidaoecac/gasparcontabilidade//situa%C3%A7%C3%A3o-fiscal-2021-06-04-34562483000130.pdf';


                    $caminho_teste = "https://veri-sp.com.br/crons-api/pdf-certidao-ecac/certidaoecac/gasparcontabilidade//situa%C3%A7%C3%A3o-fiscal-2021-06-04-27899360000123.pdf";

                    //DCTF (Período de Apuração) 2018 - MAR
                    //ECF  (Ano-Calendário) 2016 2017 2018
                    // $caminho_teste = 'https://veri-sp.com.br/crons-api/pdf-certidao-ecac/certidaoecac/gasparcontabilidade//situa%C3%A7%C3%A3o-fiscal-2021-06-04-17915300000799.pdf';

                    $pdf->Load( $e->caminho_download ) ;

                    $cnpj = $e->cnpj;

                    $texto = $pdf -> Text ;

                    $p = explode("Pendência - ", $texto); 

                    $declaracao_regular = true;
                    foreach ($p as $index) {

                        //verifica se existe ausencia de declaração
                        $pos_declaracao = strpos($index, "Ausência de Declaração");
                        if ($pos_declaracao !== false) {

                            $this->extrair_declaracao($index, $cnpj, $banco);
                            $declaracao_regular = false;
                            break;
                        }else{
                            //DEVE ATUALIZAR TODAS AS TABELAS COMO SEM AUSENCIA
                        }
                    }

                    // break;

                    if($declaracao_regular){
                        $existe_registro = $this->declaracao_ausente_gfip_model->verifica_se_existe($banco, $cnpj);

                        if($existe_registro->qtd > 0){
                            $this->declaracao_ausente_gfip_model->atualiza_ausencia_gfip_regular($banco, $cnpj);
                        }else{
                            $this->declaracao_ausente_gfip_model->insere_ausencia_gfip_regular($banco, $cnpj); 
                        }
                    }

                }catch(Exception $x){
                    echo "ERRO AO PROCESSAR CNPJ - ".$e->cnpj;
                    echo '<br>';
                    continue;
                }
                
            }
            
        }
        
    }


    public function extrair_declaracao($string, $cnpj, $banco){

        $area_total = $string;
        //Verifica se existe parcelamento no pdf
        $area_principal = explode("Parcelamento", $string);
        if(count($area_principal) > 1){
            $area_total = $area_principal[0];
        }

        $posicao_diagnostico = strpos($area_total, "__________________________ Diagnóstico");
        if ($posicao_diagnostico !== false) {

            $paginas = explode("__________________________ Diagnóstico", $area_total); 

            $this->extrai_gfip($paginas[0], $cnpj, $banco);
        }else{
            $this->extrai_gfip($area_total, $cnpj, $banco);
        }

        
        // $this->extraiDASN_SIMEI();
        // $this->extraiDCTF();

        
        

    }

    public function extrai_gfip($string, $cnpj, $banco){

        $area_reduzida_gfip = explode("GFIP(Período de Apuração)", $string); 

        $count_gfip = count($area_reduzida_gfip);
        if ($count_gfip > 1) {

            //SUBSTITUI TODAS AS STRINGS DE DECLARAÇAO PARA PODER DAR UM EXPLODE
            
            $texto_a_utilizar = str_replace("EFD-CONTRIB", "break;", $area_reduzida_gfip[1]);
            $texto_a_utilizar = str_replace("DIRF", "break;", $area_reduzida_gfip[1]);
            $texto_a_utilizar = str_replace("DASN", "break;", $area_reduzida_gfip[1]);
            $texto_a_utilizar = str_replace("DCTF", "break;", $area_reduzida_gfip[1]);
            $texto_a_utilizar = str_replace("ECF", "break;", $area_reduzida_gfip[1]);
            $texto_a_utilizar = str_replace("PGDAS", "break;", $area_reduzida_gfip[1]);
            $texto_a_utilizar = str_replace("DEFIS", "break;", $area_reduzida_gfip[1]);

            $texto_a_utilizar_final = explode("break;", $texto_a_utilizar);

            $existe_registro = $this->declaracao_ausente_gfip_model->verifica_se_existe($banco, $cnpj);

            if($existe_registro->qtd > 0){
                $this->declaracao_ausente_gfip_model->atualiza_ausencia_gfip_irregular($banco, $cnpj);
            }else{
                $this->declaracao_ausente_gfip_model->insere_ausencia_gfip_irregular($banco, $cnpj); 
            }

            $posicao_cnpj_cei = strpos($texto_a_utilizar_final[0], "CNPJ/CEI:");
            $gfip_sem_cnpj = substr($texto_a_utilizar_final[0], $posicao_cnpj_cei + 9);

            // echo $gfip_sem_cnpj;
            // echo "<br>";
            // echo "<br>";
            
            $t = nl2br($gfip_sem_cnpj);

            $paginas = explode("Página", $t);
            $count_paginas = count($paginas);
            if($count_paginas > 1){
                $t = $paginas[0];
            }

            $gfips = explode("<br />", $t);
            $this->declaracao_ausente_gfip_model->limpa_ausencia_gfip_detalhe($banco, $cnpj);

            $cnpj_cei = "";
            $ano = "";
            foreach ($gfips as $g) {
                $partes = explode(" - ", $g);
                $parte1 = $partes[0];
                if(!isset($partes[1])){
                    break;
                }
                $parte2 = $partes[1];

                //possui cnpj ou cei
                if(strlen($parte1) > 6){
                    $cnpj_cei = trim(substr($parte1,0,strlen($parte1)-4));
                    $ano = trim(substr($parte1,(strlen($parte1)-4),strlen($parte1)));

                    $this->declaracao_ausente_gfip_model->insere_ausencia_gfip_detalhe($banco, $cnpj, "GFIP", "(Período de Apuração)", "CNPJ/CEI: ".$cnpj_cei, $ano." - ".$parte2);

                }else{
                    $ano =  trim($parte1);

                    $this->declaracao_ausente_gfip_model->insere_ausencia_gfip_detalhe($banco, $cnpj, "GFIP", "(Período de Apuração)", "CNPJ/CEI: ".$cnpj_cei, $ano." - ".$parte2);

                }

                
            }
        
        }else{

            $existe_registro = $this->declaracao_ausente_gfip_model->verifica_se_existe($banco, $cnpj);

            if($existe_registro->qtd > 0){
                $this->declaracao_ausente_gfip_model->atualiza_ausencia_gfip_regular($banco, $cnpj);
            }else{
                $this->declaracao_ausente_gfip_model->insere_ausencia_gfip_regular($banco, $cnpj); 
            }

            echo "não possui";
        }
        
    }


    

}
