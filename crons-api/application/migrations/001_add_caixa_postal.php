<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_caixa_postal extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'cnpj_data' => array(
				'type' => 'VARCHAR',
				'constraint' => '14',
			),
			'lidas' => array(
				'type' => 'INT',
				'constraint' => 3,
				'unsigned' => TRUE,
			),
			'nao_lidas' => array(
				'type' => 'INT',
				'constraint' => 3,
				'unsigned' => TRUE,
			),
			'data_execucao' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dtb_ecac_caixa_postal');
	}

	public function down()
	{
		$this->dbforge->drop_table('dtb_ecac_caixa_postal');
	}
}
