<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_caixa_postal_mensagem extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'caixa_postal_id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
			),
			'assunto' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'remetente' => array(
				'type' => 'VARCHAR',
				'constraint' => '160',
				'null' => TRUE,
			),
			'recebida_em' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_field('CONSTRAINT FOREIGN KEY (caixa_postal_id) REFERENCES dtb_ecac_caixa_postal(id)');
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dtb_ecac_caixa_postal_mensagem');
	}

	public function down()
	{
		$this->dbforge->drop_table('dtb_ecac_caixa_postal_mensagem');
	}
}
