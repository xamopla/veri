<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_certificado extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'cnpj_data' => array(
				'type' => 'VARCHAR',
				'constraint' => '14',
			),
			'id_empresa' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE,
			),
			'pass' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'caminho_arq' => array(
				'type' => 'VARCHAR',
				'constraint' => '800',
				'null' => TRUE,
			),
			'data_validade' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dtb_certificado');
	}

	public function down()
	{
		$this->dbforge->drop_table('dtb_certificado');
	}
}
