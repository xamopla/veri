-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 31-Maio-2021 às 21:37
-- Versão do servidor: 10.4.18-MariaDB
-- versão do PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `script_atual_sp`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contadores`
--

CREATE TABLE `contadores` (
  `id` bigint(20) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `crc` varchar(30) NOT NULL,
  `id_contabilidade` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `controle`
--

CREATE TABLE `controle` (
  `id` bigint(11) NOT NULL,
  `id_usuario` bigint(11) NOT NULL,
  `data_hora_acao` datetime DEFAULT NULL,
  `acao_executada` varchar(255) DEFAULT NULL,
  `ip_acesso` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dae`
--

CREATE TABLE `dae` (
  `id` bigint(20) NOT NULL,
  `codigo_receita` varchar(10) DEFAULT NULL,
  `vencimento` varchar(10) DEFAULT NULL,
  `data_inclusao` varchar(10) DEFAULT NULL,
  `ie` varchar(11) DEFAULT NULL,
  `referencia` varchar(7) DEFAULT NULL,
  `num_dae` varchar(20) DEFAULT NULL,
  `cnpj` varchar(18) DEFAULT NULL,
  `razao_social` varchar(150) DEFAULT NULL,
  `valor` varchar(10) DEFAULT NULL,
  `notas` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_alvara`
--

CREATE TABLE `dtb_alvara` (
  `cnpj` varchar(100) DEFAULT NULL,
  `cnpj_sem_formatacao` varchar(100) NOT NULL,
  `inscricao_estadual` varchar(100) DEFAULT NULL,
  `inscricao_municipal` varchar(100) DEFAULT NULL,
  `razao_social` varchar(100) DEFAULT NULL,
  `numero_tvl` varchar(100) DEFAULT NULL,
  `situacao` varchar(100) DEFAULT NULL,
  `validade` varchar(100) DEFAULT NULL,
  `data_validade_publicidade` varchar(100) DEFAULT NULL,
  `data_validade_funcionamento` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_anexo`
--

CREATE TABLE `dtb_anexo` (
  `id` bigint(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `id_contabilidade` int(11) NOT NULL,
  `path` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_anexos_emails`
--

CREATE TABLE `dtb_anexos_emails` (
  `id` int(11) NOT NULL,
  `id_email` int(11) DEFAULT NULL,
  `anexo` varchar(255) DEFAULT NULL,
  `nome_anexo` varchar(500) DEFAULT NULL,
  `path_anexo` varchar(500) DEFAULT NULL,
  `id_cadastro` bigint(20) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_caixadeemail`
--

CREATE TABLE `dtb_caixadeemail` (
  `id` bigint(20) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `id_empresa` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `mensagem` varchar(255) DEFAULT NULL,
  `remetente` varchar(255) DEFAULT NULL,
  `dataemail` varchar(255) DEFAULT NULL,
  `importante` tinyint(1) DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `id_contabilidade` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_barreiras`
--

CREATE TABLE `dtb_certidao_barreiras` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(500) DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_barreiras`
--

CREATE TRIGGER `notificacoes_certidao_barreiras` AFTER UPDATE ON `dtb_certidao_barreiras` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 'Irregular') THEN
    INSERT INTO dtb_notificacao_certidao_barreiras (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_barreiras (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_caixa`
--

CREATE TABLE `dtb_certidao_caixa` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(200) DEFAULT NULL,
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_caixa`
--

CREATE TRIGGER `notificacoes_certidao_caixa` AFTER UPDATE ON `dtb_certidao_caixa` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 1) THEN
    INSERT INTO dtb_notificacao_certidao_caixa (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_caixa (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_conquista`
--

CREATE TABLE `dtb_certidao_conquista` (
  `cnpj` varchar(200) NOT NULL,
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_conquista`
--

CREATE TRIGGER `notificacoes_certidao_conquista` AFTER UPDATE ON `dtb_certidao_conquista` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 1) THEN
    INSERT INTO dtb_notificacao_certidao_conquista (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_conquista (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_estadual_sp`
--

CREATE TABLE `dtb_certidao_estadual_sp` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_estadual_sp`
--

CREATE TRIGGER `notificacoes_certidao_sefaz_sp` AFTER UPDATE ON `dtb_certidao_estadual_sp` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 1) THEN
    INSERT INTO dtb_notificacao_certidao_sefaz_sp (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_sefaz_sp (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_feira`
--

CREATE TABLE `dtb_certidao_feira` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(200) DEFAULT NULL,
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `inscricao_municipal` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `inscricao_localizacao` varchar(200) DEFAULT NULL,
  `atividade_economica` varchar(500) DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_feira`
--

CREATE TRIGGER `notificacoes_certidao_feira` AFTER UPDATE ON `dtb_certidao_feira` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 'FALSE') THEN
    INSERT INTO dtb_notificacao_certidao_feira (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_feira (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_luizeduardo`
--

CREATE TABLE `dtb_certidao_luizeduardo` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(500) DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_luizeduardo`
--

CREATE TRIGGER `notificacoes_certidao_luis_eduardo` AFTER UPDATE ON `dtb_certidao_luizeduardo` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 'Irregular') THEN
    INSERT INTO dtb_notificacao_certidao_luis_eduardo (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_luis_eduardo (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_receita`
--

CREATE TABLE `dtb_certidao_receita` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_receita`
--

CREATE TRIGGER `notificacoes_certidao_receita` AFTER UPDATE ON `dtb_certidao_receita` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 1) THEN
    INSERT INTO dtb_notificacao_certidao_receita (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_receita (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_sefaz`
--

CREATE TABLE `dtb_certidao_sefaz` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(200) DEFAULT NULL,
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_sefaz`
--

CREATE TRIGGER `notificacoes_certidao_sefaz` AFTER UPDATE ON `dtb_certidao_sefaz` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 'Irregular') THEN
    INSERT INTO dtb_notificacao_certidao_sefaz (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_sefaz (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_sp`
--

CREATE TABLE `dtb_certidao_sp` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_sp`
--

CREATE TRIGGER `notificacoes_certidao_municipio_sp` AFTER UPDATE ON `dtb_certidao_sp` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 1) THEN
    INSERT INTO dtb_notificacao_certidao_municipio_sp (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_municipio_sp (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certidao_trabalhista`
--

CREATE TABLE `dtb_certidao_trabalhista` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(200) DEFAULT NULL,
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL,
  `disponivel` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_certidao_trabalhista`
--

CREATE TRIGGER `notificacoes_certidao_trabalhista` AFTER UPDATE ON `dtb_certidao_trabalhista` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 1) THEN
    INSERT INTO dtb_notificacao_certidao_trabalhista (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_trabalhista (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certificado`
--

CREATE TABLE `dtb_certificado` (
  `cnpj_data` varchar(14) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `pass` varchar(150) DEFAULT NULL,
  `caminho_arq` varchar(800) DEFAULT NULL,
  `data_validade` varchar(20) DEFAULT NULL,
  `cert_key` text DEFAULT NULL,
  `pub_key` text DEFAULT NULL,
  `pri_key` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_certificado_contador`
--

CREATE TABLE `dtb_certificado_contador` (
  `id` int(11) NOT NULL,
  `id_contador` int(11) DEFAULT NULL,
  `pass` varchar(150) DEFAULT NULL,
  `caminho_arq` varchar(800) DEFAULT NULL,
  `data_validade` varchar(20) DEFAULT NULL,
  `cert_key` text DEFAULT NULL,
  `pub_key` text DEFAULT NULL,
  `pri_key` text DEFAULT NULL,
  `cnpj_data` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_chat`
--

CREATE TABLE `dtb_chat` (
  `id` int(11) NOT NULL,
  `id_envia` int(11) NOT NULL,
  `id_recebe` int(11) NOT NULL,
  `mensagem` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `hora` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` datetime NOT NULL,
  `data_only` date DEFAULT NULL,
  `lida` int(11) NOT NULL,
  `notificacao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_clientes_emails`
--

CREATE TABLE `dtb_clientes_emails` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `email_destinatario` varchar(500) DEFAULT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `mensagem` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `importante` varchar(255) DEFAULT 'N',
  `excluido` varchar(255) DEFAULT 'N',
  `com_estrela` varchar(255) DEFAULT 'N',
  `enviado_recebido` varchar(255) DEFAULT NULL,
  `contador_cliente` varchar(255) DEFAULT NULL,
  `possui_anexo` varchar(2) DEFAULT '0',
  `data_cadastro` datetime DEFAULT NULL,
  `id_cadastro` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_clientes_empresas`
--

CREATE TABLE `dtb_clientes_empresas` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `id_onesignal` varchar(225) DEFAULT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `cnpj_empresa` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(15) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `cep` varchar(15) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `celular` varchar(40) DEFAULT NULL,
  `logo` varchar(225) DEFAULT NULL,
  `id_cadastro` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  `data_cadastro` datetime NOT NULL,
  `sm` varchar(200) DEFAULT 'afb54a34f16578c7d9c755a7109e2be1',
  `data_acesso` datetime DEFAULT NULL,
  `id_alteracao` bigint(20) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_cnd_salvador`
--

CREATE TABLE `dtb_cnd_salvador` (
  `id_empresa` int(11) DEFAULT NULL,
  `cnpj` varchar(200) NOT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `motivo` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_cnd_salvador`
--

CREATE TRIGGER `notificacoes_certidao_salvador` AFTER UPDATE ON `dtb_cnd_salvador` FOR EACH ROW BEGIN

IF(trim(NEW.status) != trim(OLD.status)) THEN
  IF(trim(NEW.status) = 'Irregular') THEN
    INSERT INTO dtb_notificacao_certidao_salvador (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.status) = 0) THEN
    INSERT INTO dtb_notificacao_certidao_salvador (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'NORMAL', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_consulta_pedidos`
--

CREATE TABLE `dtb_consulta_pedidos` (
  `id` int(11) UNSIGNED NOT NULL,
  `cnpj_data` varchar(14) NOT NULL,
  `possui_pedido` tinyint(1) NOT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_contador_procuracao`
--

CREATE TABLE `dtb_contador_procuracao` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `id_contador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_dctf_sem_movimento`
--

CREATE TABLE `dtb_dctf_sem_movimento` (
  `id` int(11) NOT NULL,
  `mes` varchar(500) DEFAULT NULL,
  `cnpj` varchar(500) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome_usuario` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_dec_caixapostal_comunicados`
--

CREATE TABLE `dtb_dec_caixapostal_comunicados` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `data_envio` varchar(25) DEFAULT NULL,
  `lida` tinyint(1) DEFAULT 0,
  `path_anexo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_dec_caixapostal_mensagens`
--

CREATE TABLE `dtb_dec_caixapostal_mensagens` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `identificacao` varchar(50) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `data_envio` varchar(25) DEFAULT NULL,
  `data_ciencia` varchar(25) DEFAULT NULL,
  `lida` tinyint(1) DEFAULT 0,
  `path_anexo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_dec_comunicados`
--

CREATE TABLE `dtb_dec_comunicados` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `assunto` varchar(50) DEFAULT NULL,
  `complemento` varchar(25) DEFAULT NULL,
  `data_envio` varchar(25) DEFAULT NULL,
  `caminho_anexo` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_dec_mensagens`
--

CREATE TABLE `dtb_dec_mensagens` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `identificacao` varchar(50) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `assunto` varchar(50) DEFAULT NULL,
  `data_envio` varchar(25) DEFAULT NULL,
  `data_ciencia` varchar(25) DEFAULT NULL,
  `caminho_anexo` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_detalhada`
--

CREATE TABLE `dtb_divida_ativa_detalhada` (
  `id_empresa` int(11) DEFAULT NULL,
  `tipo_de_divida` varchar(400) DEFAULT NULL,
  `numero_inscricao` varchar(400) NOT NULL,
  `valor_total_da_divida` varchar(400) DEFAULT NULL,
  `domicilio_devedor` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_fgts`
--

CREATE TABLE `dtb_divida_ativa_fgts` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `tipo_pessoa` varchar(200) DEFAULT NULL,
  `tipo_devedor` varchar(200) DEFAULT NULL,
  `nome_devedor` varchar(500) DEFAULT NULL,
  `uf_unidade_responsavel` varchar(10) DEFAULT NULL,
  `unidade_responsavel` varchar(500) DEFAULT NULL,
  `entidade_responsavel` varchar(500) DEFAULT NULL,
  `unidade_inscricao` varchar(500) DEFAULT NULL,
  `numero_inscricao` varchar(200) DEFAULT NULL,
  `tipo_situacao_inscricao` varchar(500) DEFAULT NULL,
  `situacao_inscricao` varchar(500) DEFAULT NULL,
  `receita_principal` varchar(500) DEFAULT NULL,
  `data_inscricao` varchar(200) DEFAULT NULL,
  `indicador_ajuizado` varchar(500) DEFAULT NULL,
  `valor_consolidado` double DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_fgts_n`
--

CREATE TABLE `dtb_divida_ativa_fgts_n` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `nome_razaosocial` varchar(50) DEFAULT NULL,
  `nome_fantasia` varchar(50) DEFAULT NULL,
  `valor_total` varchar(255) DEFAULT NULL,
  `valor_divida_selecionada` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_geral`
--

CREATE TABLE `dtb_divida_ativa_geral` (
  `id_empresa` int(11) NOT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `nome` varchar(500) DEFAULT NULL,
  `data_consulta` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_multacriminal`
--

CREATE TABLE `dtb_divida_ativa_multacriminal` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `nome_razaosocial` varchar(50) DEFAULT NULL,
  `nome_fantasia` varchar(50) DEFAULT NULL,
  `valor_total` varchar(255) DEFAULT NULL,
  `valor_divida_selecionada` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_multaeleitoral`
--

CREATE TABLE `dtb_divida_ativa_multaeleitoral` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `nome_razaosocial` varchar(50) DEFAULT NULL,
  `nome_fantasia` varchar(50) DEFAULT NULL,
  `valor_total` varchar(255) DEFAULT NULL,
  `valor_divida_selecionada` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_multatrabalhista`
--

CREATE TABLE `dtb_divida_ativa_multatrabalhista` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `nome_razaosocial` varchar(50) DEFAULT NULL,
  `nome_fantasia` varchar(50) DEFAULT NULL,
  `valor_total` varchar(255) DEFAULT NULL,
  `valor_divida_selecionada` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_previdencia`
--

CREATE TABLE `dtb_divida_ativa_previdencia` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `tipo_pessoa` varchar(200) DEFAULT NULL,
  `tipo_devedor` varchar(200) DEFAULT NULL,
  `nome_devedor` varchar(500) DEFAULT NULL,
  `uf_unidade_responsavel` varchar(10) DEFAULT NULL,
  `unidade_responsavel` varchar(500) DEFAULT NULL,
  `numero_inscricao` varchar(200) DEFAULT NULL,
  `tipo_situacao_inscricao` varchar(500) DEFAULT NULL,
  `situacao_inscricao` varchar(500) DEFAULT NULL,
  `tipo_credito` varchar(500) DEFAULT NULL,
  `data_inscricao` varchar(200) DEFAULT NULL,
  `indicador_ajuizado` varchar(500) DEFAULT NULL,
  `valor_consolidado` double DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_divida_ativa_previdencia_n`
--

CREATE TABLE `dtb_divida_ativa_previdencia_n` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cpf_cnpj` varchar(50) DEFAULT NULL,
  `nome_razaosocial` varchar(50) DEFAULT NULL,
  `nome_fantasia` varchar(50) DEFAULT NULL,
  `valor_total` varchar(255) DEFAULT NULL,
  `valor_divida_selecionada` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_documentos`
--

CREATE TABLE `dtb_documentos` (
  `id` bigint(20) NOT NULL,
  `ativo` varchar(20) DEFAULT NULL,
  `id_empresa` bigint(20) NOT NULL,
  `id_tipoDocumento` bigint(20) NOT NULL,
  `numero_documento` varchar(150) DEFAULT NULL,
  `numero_protocolo` varchar(150) DEFAULT NULL,
  `dataEmissao` date NOT NULL DEFAULT '0000-00-00',
  `dataValidade` date NOT NULL DEFAULT '0000-00-00',
  `observacoes` varchar(100) DEFAULT NULL,
  `id_contabilidade` bigint(20) NOT NULL,
  `diasNotificacao` int(4) DEFAULT 30,
  `id_certificado_cnpj` varchar(14) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_documento_anexo`
--

CREATE TABLE `dtb_documento_anexo` (
  `id` bigint(11) NOT NULL,
  `id_documento` int(11) NOT NULL,
  `id_anexo` int(11) NOT NULL,
  `data_emissao` date DEFAULT '0000-00-00',
  `data_vencimento` date DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_caixa_postal`
--

CREATE TABLE `dtb_ecac_caixa_postal` (
  `id` int(11) UNSIGNED NOT NULL,
  `cnpj_data` varchar(14) NOT NULL,
  `lidas` int(3) UNSIGNED NOT NULL,
  `nao_lidas` int(3) UNSIGNED NOT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_caixa_postal_mensagem`
--

CREATE TABLE `dtb_ecac_caixa_postal_mensagem` (
  `id` int(11) UNSIGNED NOT NULL,
  `caixa_postal_id` int(11) UNSIGNED NOT NULL,
  `importante` int(3) DEFAULT NULL,
  `assunto` text DEFAULT NULL,
  `conteudo` text DEFAULT NULL,
  `remetente` varchar(160) DEFAULT NULL,
  `recebida_em` datetime DEFAULT NULL,
  `lida` int(3) DEFAULT NULL,
  `id_mensagem` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_das`
--

CREATE TABLE `dtb_ecac_das` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(50) DEFAULT NULL,
  `compentencia` varchar(50) DEFAULT NULL,
  `numero_declaracao` varchar(50) DEFAULT NULL,
  `data_hora_transmissao` varchar(50) DEFAULT NULL,
  `numero_das` varchar(50) DEFAULT NULL,
  `data_hora_emissao` varchar(50) DEFAULT NULL,
  `pago` varchar(10) DEFAULT NULL,
  `caminho_download_recibo` varchar(500) DEFAULT NULL,
  `caminho_download_declaracao` varchar(500) DEFAULT NULL,
  `caminho_download_extrato` varchar(500) DEFAULT NULL,
  `caminho_download_das` varchar(500) DEFAULT NULL,
  `operacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_ecac_das`
--

CREATE TRIGGER `notificacoes_das_pagos` AFTER UPDATE ON `dtb_ecac_das` FOR EACH ROW BEGIN

IF(trim(NEW.pago) != trim(OLD.pago)) THEN
  IF(trim(NEW.pago) = 'Sim') THEN
    INSERT INTO dtb_notificacao_das_pagos (cnpj, data, numero_das, descricao, periodo, lida) VALUES (OLD.cnpj, now(), NEW.numero_das, 'PAGO', NEW.compentencia, 0);
  ELSEIF (trim(NEW.pago) = 'NÃ£o') THEN
    INSERT INTO dtb_notificacao_das_pagos (cnpj, data, numero_das, descricao, periodo, lida) VALUES (OLD.cnpj, now(), NEW.numero_das, 'NÃƒO PAGO', NEW.compentencia, 0);
  END IF;
END IF;

END;

CREATE TRIGGER `notificacoes_pgdas_transmitidos` AFTER INSERT ON `dtb_ecac_das` FOR EACH ROW BEGIN

  INSERT INTO dtb_notificacao_pgdas_transmitidos (cnpj, data, numero_declaracao, descricao, periodo, lida) VALUES (NEW.cnpj, now(), NEW.numero_declaracao, 'Transmitido', NEW.compentencia, 0);

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_das_debitos`
--

CREATE TABLE `dtb_ecac_das_debitos` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `periodo_apuracao` varchar(7) DEFAULT NULL,
  `data_vencimento` varchar(10) DEFAULT NULL,
  `debito_declarado` varchar(15) DEFAULT NULL,
  `principal` varchar(15) DEFAULT NULL,
  `multa` varchar(500) DEFAULT NULL,
  `juros` varchar(500) DEFAULT NULL,
  `total` varchar(500) DEFAULT NULL,
  `exigibilidade_suspensa` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_dctf`
--

CREATE TABLE `dtb_ecac_dctf` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `cnpj_formatado` varchar(255) DEFAULT NULL,
  `periodo` varchar(255) DEFAULT NULL,
  `periodo_inicial` varchar(255) DEFAULT NULL,
  `periodo_final` varchar(255) DEFAULT NULL,
  `situacao` varchar(255) DEFAULT NULL,
  `tipo_status` varchar(255) DEFAULT NULL,
  `numero_declaracao` varchar(255) DEFAULT NULL,
  `numero_recibo` varchar(255) DEFAULT NULL,
  `data_recepcao` varchar(255) DEFAULT NULL,
  `data_processamento` varchar(255) DEFAULT NULL,
  `caminho_download_declaracao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `dtb_ecac_dctf`
--

CREATE TRIGGER `notificacoes_dctf` AFTER INSERT ON `dtb_ecac_dctf` FOR EACH ROW BEGIN

INSERT INTO dtb_notificacao_dctf (cnpj, data_recepcao, descricao, periodo, lida) VALUES (NEW.cnpj, NEW.data_recepcao, NEW.tipo_status, NEW.periodo, 0);

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_divida_ativa_fgts`
--

CREATE TABLE `dtb_ecac_divida_ativa_fgts` (
  `cnpj` varchar(18) DEFAULT NULL,
  `numero_inscricao` varchar(255) DEFAULT NULL,
  `cnpj_devedor_principal` varchar(255) DEFAULT NULL,
  `devedor_principal` varchar(255) DEFAULT NULL,
  `situacao` varchar(255) DEFAULT NULL,
  `valor_total_debito` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_divida_ativa_nao_previdenciaria`
--

CREATE TABLE `dtb_ecac_divida_ativa_nao_previdenciaria` (
  `cnpj` varchar(18) DEFAULT NULL,
  `numero_inscricao` varchar(255) DEFAULT NULL,
  `numero_processo` varchar(255) DEFAULT NULL,
  `cnpj_devedor_principal` varchar(255) DEFAULT NULL,
  `situacao` varchar(255) DEFAULT NULL,
  `valor_consolidado` varchar(255) DEFAULT NULL,
  `data_consolidacao` varchar(255) DEFAULT NULL,
  `extinta` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_divida_ativa_previdenciaria`
--

CREATE TABLE `dtb_ecac_divida_ativa_previdenciaria` (
  `cnpj` varchar(18) DEFAULT NULL,
  `numero_inscricao` varchar(255) DEFAULT NULL,
  `cnpj_devedor_principal` varchar(255) DEFAULT NULL,
  `devedor_principal` varchar(255) DEFAULT NULL,
  `fase_atual` varchar(255) DEFAULT NULL,
  `valor_total_debito` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_eprocessos_ativos`
--

CREATE TABLE `dtb_ecac_eprocessos_ativos` (
  `id` int(11) NOT NULL,
  `idProcesso` varchar(255) NOT NULL,
  `dataProtocolo` varchar(20) DEFAULT NULL,
  `dataProtocoloFormatada` varchar(30) DEFAULT NULL,
  `grupo` varchar(255) DEFAULT NULL,
  `indicadorVersaoProcesso` varchar(255) DEFAULT NULL,
  `localizacao` varchar(255) DEFAULT NULL,
  `natureza` varchar(255) DEFAULT NULL,
  `niInteressado` varchar(255) DEFAULT NULL,
  `niInteressadoFormatado` varchar(255) DEFAULT NULL,
  `nomeInteressado` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `numeroFormatado` varchar(255) DEFAULT NULL,
  `numeroProcessoProcedimento` varchar(255) DEFAULT NULL,
  `numeroProcessoProcedimentoFormatado` varchar(255) DEFAULT NULL,
  `permiteVisualizarProcesso` varchar(10) DEFAULT NULL,
  `situacao` varchar(255) DEFAULT NULL,
  `situacaoSolidario` varchar(255) DEFAULT NULL,
  `subtipo` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `tipoResponsabilidade` varchar(255) DEFAULT NULL,
  `cnpj` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_eprocessos_ativos_historico`
--

CREATE TABLE `dtb_ecac_eprocessos_ativos_historico` (
  `id` int(11) NOT NULL,
  `idProcesso` varchar(50) DEFAULT NULL,
  `dataEntrada` varchar(255) DEFAULT NULL,
  `dataEntradaFormatada` varchar(255) DEFAULT NULL,
  `equipeOuOperacao` varchar(255) DEFAULT NULL,
  `nomeAtividade` varchar(255) DEFAULT NULL,
  `siglaUnidade` varchar(255) DEFAULT NULL,
  `tempoAtividade` varchar(255) DEFAULT NULL,
  `tempoMedioAtividade` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_eprocessos_inativos`
--

CREATE TABLE `dtb_ecac_eprocessos_inativos` (
  `id` int(11) NOT NULL,
  `idProcesso` varchar(255) NOT NULL,
  `dataProtocolo` varchar(20) DEFAULT NULL,
  `dataProtocoloFormatada` varchar(30) DEFAULT NULL,
  `grupo` varchar(255) DEFAULT NULL,
  `indicadorVersaoProcesso` varchar(255) DEFAULT NULL,
  `localizacao` varchar(255) DEFAULT NULL,
  `natureza` varchar(255) DEFAULT NULL,
  `niInteressado` varchar(255) DEFAULT NULL,
  `niInteressadoFormatado` varchar(255) DEFAULT NULL,
  `nomeInteressado` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `numeroFormatado` varchar(255) DEFAULT NULL,
  `numeroProcessoProcedimento` varchar(255) DEFAULT NULL,
  `numeroProcessoProcedimentoFormatado` varchar(255) DEFAULT NULL,
  `permiteVisualizarProcesso` varchar(10) DEFAULT NULL,
  `situacao` varchar(255) DEFAULT NULL,
  `situacaoSolidario` varchar(255) DEFAULT NULL,
  `subtipo` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `tipoResponsabilidade` varchar(255) DEFAULT NULL,
  `cnpj` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_eprocessos_inativos_historico`
--

CREATE TABLE `dtb_ecac_eprocessos_inativos_historico` (
  `id` int(11) NOT NULL,
  `idProcesso` varchar(50) DEFAULT NULL,
  `dataEntrada` varchar(255) DEFAULT NULL,
  `dataEntradaFormatada` varchar(255) DEFAULT NULL,
  `equipeOuOperacao` varchar(255) DEFAULT NULL,
  `nomeAtividade` varchar(255) DEFAULT NULL,
  `siglaUnidade` varchar(255) DEFAULT NULL,
  `tempoAtividade` varchar(255) DEFAULT NULL,
  `tempoMedioAtividade` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_procuracao`
--

CREATE TABLE `dtb_ecac_procuracao` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `cnpj_outorgante` varchar(255) DEFAULT NULL,
  `nome_outorgante` varchar(255) DEFAULT NULL,
  `data_inicio` varchar(100) DEFAULT NULL,
  `data_fim` varchar(100) DEFAULT NULL,
  `situacao` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_simplesnacional_demonstrativo_pagamentos`
--

CREATE TABLE `dtb_ecac_simplesnacional_demonstrativo_pagamentos` (
  `id` int(11) NOT NULL,
  `id_parcelamento` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `mes_parcela` varchar(14) DEFAULT NULL,
  `vencimento_das` varchar(14) DEFAULT NULL,
  `data_arrecadacao` varchar(14) DEFAULT NULL,
  `valor_pago` varchar(14) DEFAULT NULL,
  `path_recibo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_simplesnacional_pedidos_parcelamentos`
--

CREATE TABLE `dtb_ecac_simplesnacional_pedidos_parcelamentos` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `data_pedido` varchar(30) DEFAULT NULL,
  `situacao` varchar(500) DEFAULT NULL,
  `data_situacao` varchar(30) DEFAULT NULL,
  `observacao` varchar(30) DEFAULT NULL,
  `path_recibo_adesao` varchar(255) DEFAULT NULL,
  `valor_total_consolidado` varchar(30) DEFAULT NULL,
  `qtd_parcelas` varchar(30) DEFAULT NULL,
  `primeira_parcela` varchar(30) DEFAULT NULL,
  `parcela_basica` varchar(30) DEFAULT NULL,
  `path_parcela` varchar(255) DEFAULT NULL,
  `data_consolidacao` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ecac_simplesnacional_relacao_debitos_parcelas`
--

CREATE TABLE `dtb_ecac_simplesnacional_relacao_debitos_parcelas` (
  `id` int(11) NOT NULL,
  `id_parcelamento` int(11) NOT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `periodo_apuracao` varchar(14) DEFAULT NULL,
  `vencimento` varchar(14) DEFAULT NULL,
  `numero_processo` varchar(14) DEFAULT NULL,
  `saldo_devedor_original` varchar(14) DEFAULT NULL,
  `valor_atualizado` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_email`
--

CREATE TABLE `dtb_email` (
  `id` bigint(20) NOT NULL,
  `ativo` varchar(20) DEFAULT NULL,
  `nome` varchar(220) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `id_contabilidade` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_empresas`
--

CREATE TABLE `dtb_empresas` (
  `id` bigint(20) NOT NULL,
  `inscricao_estadual` varchar(20) DEFAULT NULL,
  `cnpj` varchar(25) NOT NULL,
  `inscricao_estadual_completo` varchar(20) DEFAULT NULL,
  `cnpj_completo` varchar(25) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `mei` varchar(4) DEFAULT NULL,
  `natureza_juridica` varchar(255) DEFAULT NULL,
  `unidade_atendimento` varchar(255) DEFAULT NULL,
  `unidade_fiscalizacao` varchar(255) DEFAULT NULL,
  `cep` varchar(15) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `uf` char(2) DEFAULT NULL,
  `referencia` varchar(255) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `telefone_alternativo` varchar(20) DEFAULT NULL,
  `celular_alternativo` varchar(20) DEFAULT NULL,
  `email_alternativo` varchar(150) DEFAULT NULL,
  `situacao_cadastral` varchar(50) DEFAULT NULL,
  `situacao` varchar(20) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `situacao_dte` varchar(50) DEFAULT NULL,
  `situacao_conta_dte` varchar(50) DEFAULT NULL,
  `senha_sefaz` varchar(100) DEFAULT NULL,
  `atividade_principal` varchar(255) DEFAULT NULL,
  `condicao` varchar(255) DEFAULT NULL,
  `forma_pagamento` varchar(255) DEFAULT NULL,
  `motivo_situacao_cadastral` varchar(255) DEFAULT NULL,
  `crc_contador` varchar(30) DEFAULT NULL,
  `nome_contador` varchar(255) DEFAULT NULL,
  `crc_responsavel` varchar(30) DEFAULT NULL,
  `nome_responsavel` varchar(255) DEFAULT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 1,
  `id_contador` bigint(20) DEFAULT NULL,
  `vinculo_contador` tinyint(1) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `login_sefaz` varchar(100) DEFAULT NULL,
  `login_mei` varchar(100) DEFAULT NULL,
  `senha_mei` varchar(100) DEFAULT NULL,
  `id_funcionario` bigint(20) DEFAULT NULL,
  `flag_empresa_sem_ie` varchar(50) DEFAULT NULL,
  `situacao_cadastral_vigente` varchar(255) DEFAULT NULL,
  `motivo_intimacao` varchar(255) DEFAULT NULL,
  `data_intimacao` varchar(255) DEFAULT NULL,
  `nire` varchar(100) DEFAULT NULL,
  `anotacoes` varchar(500) DEFAULT NULL,
  `cpf_alvara` varchar(50) DEFAULT NULL,
  `tipo_regime` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Acionadores `dtb_empresas`
--

CREATE TRIGGER `historico_atualizacoes` AFTER UPDATE ON `dtb_empresas` FOR EACH ROW BEGIN

IF(NEW.situacao_cadastral != OLD.situacao_cadastral || NEW.situacao != OLD.situacao) THEN

  INSERT INTO historico_empresas(id_empresa, inscricao_estadual_completo, cnpj_completo, razao_social, nome_fantasia, mei, natureza_juridica, unidade_atendimento, unidade_fiscalizacao, cep, logradouro, numero, complemento, bairro, cidade, uf, referencia, localizacao, telefone, email, situacao_cadastral, situacao, motivo, situacao_dte, situacao_conta_dte, atividade_principal, condicao, forma_pagamento, motivo_situacao_cadastral, crc_contador, nome_contador, crc_responsavel, nome_responsavel, data_atualizacao) VALUES (OLD.id, NEW.inscricao_estadual_completo, NEW.cnpj_completo, NEW.razao_social, NEW.nome_fantasia, NEW.mei, NEW.natureza_juridica, NEW.unidade_atendimento, NEW.unidade_fiscalizacao, NEW.cep, NEW.logradouro, NEW.numero, NEW.complemento, NEW.bairro, NEW.cidade, NEW.uf, NEW.referencia, NEW.localizacao, NEW.telefone, NEW.email, NEW.situacao_cadastral, NEW.situacao, NEW.motivo, NEW.situacao_dte, NEW.situacao_conta_dte, NEW.atividade_principal, NEW.condicao, NEW.forma_pagamento, NEW.motivo_situacao_cadastral, NEW.crc_contador, NEW.nome_contador, NEW.crc_responsavel, NEW.nome_responsavel, NEW.data_atualizacao);
    
    INSERT INTO notificacoes (id_empresa, razao_social, situacao, id_usuario, status, data_atualizacao) VALUES (OLD.id, NEW.razao_social, NEW.situacao, OLD.id_contador, 1, NEW.data_atualizacao);
  
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_empresas_excluidas_mei`
--

CREATE TABLE `dtb_empresas_excluidas_mei` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(50) DEFAULT NULL,
  `data_inicio` varchar(100) DEFAULT NULL,
  `data_fim` varchar(100) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL,
  `is_excluida` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Acionadores `dtb_empresas_excluidas_mei`
--

CREATE TRIGGER `notificacoes_empresas_excluidas_mei` AFTER UPDATE ON `dtb_empresas_excluidas_mei` FOR EACH ROW BEGIN

IF(trim(NEW.is_excluida) != trim(OLD.is_excluida)) THEN
  IF(trim(NEW.is_excluida) = 1) THEN
    INSERT INTO dtb_notificacao_empresas_excluidas_mei (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'EXCLUIDA', 0);
  ELSEIF (trim(NEW.is_excluida) = 0) THEN
    INSERT INTO dtb_notificacao_empresas_excluidas_mei (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'MEI', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_empresas_excluidas_simples`
--

CREATE TABLE `dtb_empresas_excluidas_simples` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(50) DEFAULT NULL,
  `data_inicio` varchar(100) DEFAULT NULL,
  `data_fim` varchar(100) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL,
  `is_excluida` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Acionadores `dtb_empresas_excluidas_simples`
--

CREATE TRIGGER `notificacoes_empresas_excluidas_simples` AFTER UPDATE ON `dtb_empresas_excluidas_simples` FOR EACH ROW BEGIN

IF(trim(NEW.is_excluida) != trim(OLD.is_excluida)) THEN
  IF(trim(NEW.is_excluida) = 1) THEN
    INSERT INTO dtb_notificacao_empresas_excluidas_simples (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'EXCLUIDA', 0);
  ELSEIF (trim(NEW.is_excluida) = 0) THEN
    INSERT INTO dtb_notificacao_empresas_excluidas_simples (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'SIMPLES', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_empresas_sem_procuracao`
--

CREATE TABLE `dtb_empresas_sem_procuracao` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_empresa_usuario`
--

CREATE TABLE `dtb_empresa_usuario` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_exclusao_simples`
--

CREATE TABLE `dtb_exclusao_simples` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(500) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `situacao_simples` varchar(500) DEFAULT NULL,
  `situacao_simei` varchar(500) DEFAULT NULL,
  `data_inicial_simples` varchar(50) DEFAULT NULL,
  `data_final_simples` varchar(50) DEFAULT NULL,
  `detalhe_simples` varchar(500) DEFAULT NULL,
  `data_inicial_simei` varchar(50) DEFAULT NULL,
  `data_final_simei` varchar(50) DEFAULT NULL,
  `detalhe_simei` varchar(500) DEFAULT NULL,
  `evento_futuro_mei` varchar(500) DEFAULT NULL,
  `evento_futuro_simei` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_expedidor`
--

CREATE TABLE `dtb_expedidor` (
  `id` bigint(20) NOT NULL,
  `ativo` varchar(20) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `razao_social` varchar(225) DEFAULT NULL,
  `nome_fantasia` varchar(225) DEFAULT NULL,
  `email_principal` varchar(225) DEFAULT NULL,
  `email_secundario` varchar(225) DEFAULT NULL,
  `endereco` varchar(225) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `complemento` varchar(225) DEFAULT NULL,
  `bairro` varchar(225) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `cidade` varchar(225) DEFAULT NULL,
  `estado` varchar(225) DEFAULT NULL,
  `id_contabilidade` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_financeiro`
--

CREATE TABLE `dtb_financeiro` (
  `id` bigint(20) NOT NULL,
  `numero_fatura` varchar(150) DEFAULT NULL,
  `data_vencimento` date NOT NULL DEFAULT '0000-00-00',
  `valor` varchar(225) DEFAULT NULL,
  `data_pagamento` date NOT NULL DEFAULT '0000-00-00',
  `forma_pagamento` varchar(225) DEFAULT NULL,
  `situacao` varchar(225) DEFAULT NULL,
  `id_contabilidade` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_ipva`
--

CREATE TABLE `dtb_ipva` (
  `cnpj` varchar(100) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `inscricao_estadual` varchar(100) DEFAULT NULL,
  `placas` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_juceb`
--

CREATE TABLE `dtb_juceb` (
  `id` bigint(20) NOT NULL,
  `protocolo` varchar(50) DEFAULT NULL,
  `nome_da_empresa` varchar(220) DEFAULT NULL,
  `estado_do_processo` varchar(220) DEFAULT NULL,
  `status_de_envio` varchar(220) DEFAULT NULL,
  `data_atualizacao` varchar(220) DEFAULT NULL,
  `data_incorporacao` varchar(220) DEFAULT NULL,
  `id_contabilidade` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Acionadores `dtb_juceb`
--

CREATE TRIGGER `notificacoes_juceb_viabilidade` AFTER UPDATE ON `dtb_juceb` FOR EACH ROW BEGIN

IF(trim(NEW.estado_do_processo) != trim(OLD.estado_do_processo)) THEN
  INSERT INTO dtb_notificacao_juceb_viabilidade (id_protocolo, razao_social, data, descricao, lida) VALUES (OLD.protocolo, OLD.nome_da_empresa, now(), NEW.estado_do_processo, 0);
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_juceb_dados`
--

CREATE TABLE `dtb_juceb_dados` (
  `id` bigint(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `juceb_nome_empresarial` varchar(255) DEFAULT NULL,
  `juceb_nire` varchar(255) DEFAULT NULL,
  `juceb_cnpj` varchar(255) DEFAULT NULL,
  `juceb_situacao` varchar(255) DEFAULT NULL,
  `juceb_status` varchar(255) DEFAULT NULL,
  `juceb_natureza_juridica` varchar(255) DEFAULT NULL,
  `juceb_capital_social` varchar(255) DEFAULT NULL,
  `juceb_capital_integralizado` varchar(255) DEFAULT NULL,
  `juceb_data_do_ato_constitutivo` varchar(255) DEFAULT NULL,
  `juceb_data_inicio_das_atividades` varchar(255) DEFAULT NULL,
  `juceb_logradouro` varchar(255) DEFAULT NULL,
  `juceb_complemento` varchar(255) DEFAULT NULL,
  `juceb_numero` varchar(255) DEFAULT NULL,
  `juceb_bairro` varchar(255) DEFAULT NULL,
  `juceb_cep` varchar(255) DEFAULT NULL,
  `juceb_municipio` varchar(255) DEFAULT NULL,
  `juceb_objeto_social` varchar(255) DEFAULT NULL,
  `juceb_historico_data_ultimo_arquivamento` varchar(255) DEFAULT NULL,
  `juceb_historico_nome_do_evento` varchar(255) DEFAULT NULL,
  `juceb_historico_numero_arquivamento` varchar(255) DEFAULT NULL,
  `juceb_historico_descricao_ato` varchar(255) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `juceb_data_evento_expirar` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_limite_simples`
--

CREATE TABLE `dtb_limite_simples` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(100) DEFAULT NULL,
  `valor_atual` double DEFAULT NULL,
  `percentual` double DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL,
  `ano` varchar(50) DEFAULT NULL,
  `path` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Acionadores `dtb_limite_simples`
--

CREATE TRIGGER `notificacoes_sublimite_simples` AFTER UPDATE ON `dtb_limite_simples` FOR EACH ROW BEGIN

IF(trim(NEW.percentual) != trim(OLD.percentual)) THEN
  IF(trim(NEW.percentual) >= 70) THEN
    INSERT INTO dtb_notificacao_sublimite_simples (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), NEW.percentual, 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_logins_validos`
--

CREATE TABLE `dtb_logins_validos` (
  `id_empresa` int(11) NOT NULL,
  `login` varchar(15) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  `status_da_senha` varchar(100) DEFAULT NULL,
  `data_verificacao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_mensagens_dte`
--

CREATE TABLE `dtb_mensagens_dte` (
  `id` bigint(20) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `id_empresa` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `situacao_da_ciencia` varchar(255) DEFAULT NULL,
  `codigo_tipo` varchar(255) DEFAULT NULL,
  `destinatario` varchar(255) DEFAULT NULL,
  `remetente` varchar(255) DEFAULT NULL,
  `data_de_emissao` varchar(255) DEFAULT NULL,
  `assunto` varchar(800) DEFAULT NULL,
  `conteudo_mensagem` text DEFAULT NULL,
  `caminho_anexo` varchar(250) DEFAULT NULL,
  `data_de_leitura` varchar(255) DEFAULT NULL,
  `data_de_ciencia` varchar(255) DEFAULT NULL,
  `data_de_vencimento` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_barreiras`
--

CREATE TABLE `dtb_notificacao_certidao_barreiras` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_caixa`
--

CREATE TABLE `dtb_notificacao_certidao_caixa` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_conquista`
--

CREATE TABLE `dtb_notificacao_certidao_conquista` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_feira`
--

CREATE TABLE `dtb_notificacao_certidao_feira` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_luis_eduardo`
--

CREATE TABLE `dtb_notificacao_certidao_luis_eduardo` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_municipio_sp`
--

CREATE TABLE `dtb_notificacao_certidao_municipio_sp` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_receita`
--

CREATE TABLE `dtb_notificacao_certidao_receita` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_salvador`
--

CREATE TABLE `dtb_notificacao_certidao_salvador` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_sefaz`
--

CREATE TABLE `dtb_notificacao_certidao_sefaz` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_sefaz_sp`
--

CREATE TABLE `dtb_notificacao_certidao_sefaz_sp` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_certidao_trabalhista`
--

CREATE TABLE `dtb_notificacao_certidao_trabalhista` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_das_nao_pagos`
--

CREATE TABLE `dtb_notificacao_das_nao_pagos` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `mes` varchar(250) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_das_pagos`
--

CREATE TABLE `dtb_notificacao_das_pagos` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `numero_das` varchar(500) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `periodo` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_dctf`
--

CREATE TABLE `dtb_notificacao_dctf` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data_recepcao` varchar(500) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `periodo` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_dctf_nao_entregue`
--

CREATE TABLE `dtb_notificacao_dctf_nao_entregue` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `mes` varchar(250) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_dctf_proxima_vencimento`
--

CREATE TABLE `dtb_notificacao_dctf_proxima_vencimento` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `mes` varchar(250) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_dctf_sem_movimento`
--

CREATE TABLE `dtb_notificacao_dctf_sem_movimento` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `mes` varchar(250) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_ecac_cadin`
--

CREATE TABLE `dtb_notificacao_ecac_cadin` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_ecac_eprocessos_ativos`
--

CREATE TABLE `dtb_notificacao_ecac_eprocessos_ativos` (
  `id` int(11) NOT NULL,
  `numero_processo` varchar(250) DEFAULT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_ecac_eprocessos_inativos`
--

CREATE TABLE `dtb_notificacao_ecac_eprocessos_inativos` (
  `id` int(11) NOT NULL,
  `numero_processo` varchar(250) DEFAULT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_ecac_situacao`
--

CREATE TABLE `dtb_notificacao_ecac_situacao` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_empresas_excluidas_mei`
--

CREATE TABLE `dtb_notificacao_empresas_excluidas_mei` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_empresas_excluidas_simples`
--

CREATE TABLE `dtb_notificacao_empresas_excluidas_simples` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_juceb_viabilidade`
--

CREATE TABLE `dtb_notificacao_juceb_viabilidade` (
  `id` int(11) NOT NULL,
  `id_protocolo` varchar(250) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_pgdas_nao_entregue`
--

CREATE TABLE `dtb_notificacao_pgdas_nao_entregue` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `mes` varchar(250) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_pgdas_proxima_vencimento`
--

CREATE TABLE `dtb_notificacao_pgdas_proxima_vencimento` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `mes` varchar(250) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_pgdas_transmitidos`
--

CREATE TABLE `dtb_notificacao_pgdas_transmitidos` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `numero_declaracao` varchar(500) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `periodo` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_notificacao_sublimite_simples`
--

CREATE TABLE `dtb_notificacao_sublimite_simples` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `lida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_parcelas_emitidas`
--

CREATE TABLE `dtb_parcelas_emitidas` (
  `id` int(11) UNSIGNED NOT NULL,
  `cnpj_data` varchar(14) NOT NULL,
  `valor` float UNSIGNED NOT NULL,
  `data_parcela` varchar(10) NOT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_plano_contratado`
--

CREATE TABLE `dtb_plano_contratado` (
  `id` int(11) NOT NULL,
  `nome_do_plano` varchar(100) NOT NULL,
  `qtd_empresas` int(5) NOT NULL,
  `qtd_usuarios` int(5) NOT NULL,
  `cidade` varchar(500) DEFAULT NULL,
  `validade` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_dte` varchar(500) DEFAULT NULL,
  `senha_dte` varchar(500) DEFAULT NULL,
  `servidor_email` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dtb_plano_contratado`
--

INSERT INTO `dtb_plano_contratado` (`id`, `nome_do_plano`, `qtd_empresas`, `qtd_usuarios`, `cidade`, `validade`, `email_dte`, `senha_dte`, `servidor_email`) VALUES
(1, 'Plano PLUS', 1000, 555, NULL, '2022-12-31 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_resumofiscal`
--

CREATE TABLE `dtb_resumofiscal` (
  `unidadeDeAtendimento` varchar(100) DEFAULT NULL,
  `unidadeDeFiscalizacao` varchar(100) DEFAULT NULL,
  `inscricaoEstadual` varchar(100) DEFAULT NULL,
  `cnpjcpf` varchar(100) NOT NULL,
  `simplesNacional` varchar(100) DEFAULT NULL,
  `razaoSocial` varchar(100) DEFAULT NULL,
  `situacao` varchar(100) DEFAULT NULL,
  `motivo` varchar(100) DEFAULT NULL,
  `condicao` varchar(100) DEFAULT NULL,
  `cnaeFiscal` varchar(100) DEFAULT NULL,
  `telefone` varchar(100) DEFAULT NULL,
  `contador` varchar(100) DEFAULT NULL,
  `crc` varchar(100) DEFAULT NULL,
  `telefone2` varchar(100) DEFAULT NULL,
  `porte` varchar(100) DEFAULT NULL,
  `dataInclusao` varchar(100) DEFAULT NULL,
  `opcaoSimplesNacional` varchar(100) DEFAULT NULL,
  `situacaoProjetada` varchar(100) DEFAULT NULL,
  `condicaoProjetada` varchar(100) DEFAULT NULL,
  `MEI` varchar(100) DEFAULT NULL,
  `pergunta1` varchar(600) DEFAULT NULL,
  `pergunta2` varchar(600) DEFAULT NULL,
  `pergunta3` varchar(600) DEFAULT NULL,
  `pergunta4` varchar(600) DEFAULT NULL,
  `pergunta5` varchar(600) DEFAULT NULL,
  `pergunta6` varchar(600) DEFAULT NULL,
  `pergunta7` varchar(600) DEFAULT NULL,
  `pergunta8` varchar(600) DEFAULT NULL,
  `pergunta9` varchar(600) DEFAULT NULL,
  `pergunta10` varchar(600) DEFAULT NULL,
  `pergunta11` varchar(600) DEFAULT NULL,
  `pergunta12` varchar(600) DEFAULT NULL,
  `pergunta13` varchar(600) DEFAULT NULL,
  `pergunta14` varchar(600) DEFAULT NULL,
  `pergunta15` varchar(600) DEFAULT NULL,
  `pergunta16` varchar(600) DEFAULT NULL,
  `pergunta17` varchar(600) DEFAULT NULL,
  `pergunta18` varchar(600) DEFAULT NULL,
  `pergunta19` varchar(600) DEFAULT NULL,
  `pergunta20` varchar(600) DEFAULT NULL,
  `pergunta21` varchar(600) DEFAULT NULL,
  `pergunta22` varchar(600) DEFAULT NULL,
  `pergunta23` varchar(600) DEFAULT NULL,
  `pergunta24` varchar(600) DEFAULT NULL,
  `pergunta25` varchar(600) DEFAULT NULL,
  `pergunta26` varchar(600) DEFAULT NULL,
  `msg_nao_lidas` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Acionadores `dtb_resumofiscal`
--

CREATE TRIGGER `notificacoes_resumo_fiscal` AFTER UPDATE ON `dtb_resumofiscal` FOR EACH ROW BEGIN

IF(trim(NEW.pergunta1) != trim(OLD.pergunta1)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Sócio Irregular', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta2) != trim(OLD.pergunta2)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Omisso DMA', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta3) != trim(OLD.pergunta3)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Omisso EFD', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta4) != trim(OLD.pergunta4)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Parcelamento em atraso ou interrompido', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta7) != trim(OLD.pergunta7)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Diver. ICMS normal informado na DMA e o recolhido', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta8) != trim(OLD.pergunta8)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Diver. ICMS Subs. por Antecipação informado na DMA e o recolhido', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta9) != trim(OLD.pergunta9)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Diver. ICMS Subs. por Retenção informado na DMA e o recolhido', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta18) != trim(OLD.pergunta18)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'O.S. de Monitoramento ativa', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta19) != trim(OLD.pergunta19)) THEN
  INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'O.S. de Auditoria ativa', OLD.cnpjcpf);
ELSEIF (trim(NEW.pergunta6) != trim(OLD.pergunta6)) THEN
    INSERT INTO notificacoesresumofiscal (cnpjcpf, razaoSocial, status, data_atualizacao, descricao, cnpjstring) VALUES (OLD.cnpjcpf, OLD.razaoSocial, 1, now(), 'Outros PAFs com ciclo de vida ativo', OLD.cnpjcpf);
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_sefaz_destinatario_mdfe`
--

CREATE TABLE `dtb_sefaz_destinatario_mdfe` (
  `chave_acesso` varchar(44) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `mdf_data` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_situacao_cadin`
--

CREATE TABLE `dtb_situacao_cadin` (
  `id` int(11) NOT NULL,
  `cnpj_data` varchar(14) NOT NULL,
  `caminho_download` varchar(300) DEFAULT NULL,
  `possui_pendencia` tinyint(3) UNSIGNED NOT NULL,
  `data_pdf` mediumblob DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Acionadores `dtb_situacao_cadin`
--

CREATE TRIGGER `notificacoes_ecac_cadin` AFTER UPDATE ON `dtb_situacao_cadin` FOR EACH ROW BEGIN

IF(trim(NEW.possui_pendencia) != trim(OLD.possui_pendencia)) THEN
  IF(trim(NEW.possui_pendencia) = 1) THEN
    INSERT INTO dtb_notificacao_ecac_cadin (cnpj, data, descricao, lida) VALUES (OLD.cnpj_data, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.possui_pendencia) = 0) THEN
    INSERT INTO dtb_notificacao_ecac_cadin (cnpj, data, descricao, lida) VALUES (OLD.cnpj_data, now(), 'NORMAL', 0);
  END IF;
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_situacao_fiscal`
--

CREATE TABLE `dtb_situacao_fiscal` (
  `id` int(11) UNSIGNED NOT NULL,
  `cnpj_data` varchar(14) NOT NULL,
  `caminho_download` varchar(300) DEFAULT NULL,
  `possui_pendencia` tinyint(3) UNSIGNED NOT NULL,
  `data_pdf` mediumblob DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Acionadores `dtb_situacao_fiscal`
--

CREATE TRIGGER `notificacoes_ecac_situacao` AFTER UPDATE ON `dtb_situacao_fiscal` FOR EACH ROW BEGIN

IF(trim(NEW.possui_pendencia) != trim(OLD.possui_pendencia)) THEN
  IF(trim(NEW.possui_pendencia) = 1) THEN
    INSERT INTO dtb_notificacao_ecac_situacao (cnpj, data, descricao, lida) VALUES (OLD.cnpj_data, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.possui_pendencia) = 0) THEN
    INSERT INTO dtb_notificacao_ecac_situacao (cnpj, data, descricao, lida) VALUES (OLD.cnpj_data, now(), 'NORMAL', 0);
  END IF;
END IF;

END;
-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_tipodedocumento`
--

CREATE TABLE `dtb_tipodedocumento` (
  `id` bigint(20) NOT NULL,
  `ativo` varchar(20) DEFAULT NULL,
  `nome` varchar(225) DEFAULT NULL,
  `descricao` varchar(225) DEFAULT NULL,
  `tipo_de_validade` decimal(4,0) DEFAULT NULL,
  `url_consulta` varchar(225) DEFAULT NULL,
  `id_contabilidade` bigint(20) NOT NULL,
  `orgao_expedidor` varchar(225) DEFAULT NULL,
  `diasNotificacao` int(4) DEFAULT 30
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_tramitacao_processo_sipro`
--

CREATE TABLE `dtb_tramitacao_processo_sipro` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `numero_processo` varchar(255) DEFAULT NULL,
  `numero_processo_formatado` varchar(255) DEFAULT NULL,
  `data_cadastramento` varchar(500) DEFAULT NULL,
  `nome_interessado` varchar(500) DEFAULT NULL,
  `tipo_processo` varchar(500) DEFAULT NULL,
  `local_processo` varchar(500) DEFAULT NULL,
  `situacao_processo` varchar(500) DEFAULT NULL,
  `envio_data` varchar(500) DEFAULT NULL,
  `envio_local` varchar(500) DEFAULT NULL,
  `recepcao_data` varchar(500) DEFAULT NULL,
  `recepcao_local` varchar(500) DEFAULT NULL,
  `objetivo` varchar(500) DEFAULT NULL,
  `id_cadastro` bigint(20) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Acionadores `dtb_tramitacao_processo_sipro`
--

CREATE TRIGGER `notificacoes_processo_sipro` AFTER UPDATE ON `dtb_tramitacao_processo_sipro` FOR EACH ROW BEGIN

IF(NEW.envio_data != OLD.envio_data || NEW.recepcao_data != OLD.recepcao_data || NEW.objetivo != OLD.objetivo) THEN

  INSERT INTO notificacoesprocessosipro (id_empresa, id_processo_sipro, numero_processo, numero_processo_formatado, objetivo, envio_data, recepcao_data, status, data_atualizacao) VALUES (OLD.id_empresa, OLD.id, OLD.numero_processo, OLD.numero_processo_formatado, NEW.objetivo, NEW.envio_data, NEW.recepcao_data, 1, NEW.data_atualizacao);
  
END IF;

END;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_tramitacao_processo_sipro_documento`
--

CREATE TABLE `dtb_tramitacao_processo_sipro_documento` (
  `id` int(11) NOT NULL,
  `id_tramitacao_processo_sipro` int(11) NOT NULL,
  `anexo_nome` varchar(255) NOT NULL,
  `anexo_path` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_tutorial`
--

CREATE TABLE `dtb_tutorial` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `autor` varchar(500) DEFAULT NULL,
  `dia` varchar(50) DEFAULT NULL,
  `mes` varchar(100) DEFAULT NULL,
  `ano` varchar(100) DEFAULT NULL,
  `tempo` varchar(250) DEFAULT NULL,
  `categoria` varchar(250) DEFAULT NULL,
  `vista` int(11) DEFAULT 0,
  `link` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dtb_tutorial`
--

INSERT INTO `dtb_tutorial` (`id`, `titulo`, `descricao`, `autor`, `dia`, `mes`, `ano`, `tempo`, `categoria`, `vista`, `link`) VALUES
(1, 'Sócio Irregular', 'Saiba como consultar Sócios Irregulares no Sistema Veri e tenha acesso a essas informações com apenas 2 cliques', 'Robert Damacena', '27', 'Outubro', '2020', '1:04 min', 'ESTADUAL', 0, 'https://www.youtube.com/embed/YHPeDlIA4ww?rel=0&autoplay=0&mute=0'),
(2, 'JUCEB Próximo ao vencimento - JUCEB vencido', 'O Sistema VERI foi criado para levar praticidade e eficiência aos escritórios de contabilidade, saiba como verificar a situação do registro da JUCEB', 'Robert Damacena', '28', 'Outubro', '2020', '1:02 min', 'PROCESSOS', 0, 'https://www.youtube.com/embed/ENzlZN0Imek?rel=0&autoplay=0&mute=0'),
(3, 'O.S de Monitoramento e O.S de Auditoria', 'Sabemos que a sua rotina como contador não é simples, afinal, são muitos os aspectos com os quais você precisa lidar para garantir a seguridade fiscal, legal, tributária e financeira dos seus clientes. Saiba como consultar se há O.S de Monitoramento e/ou Auditoria abertas.', 'Robert Damacena', '28', 'Outubro', '2020', '0:57 min', 'ESTADUAL', 0, 'https://www.youtube.com/embed/DRfuC1vat0M?rel=0&autoplay=0&mute=0'),
(4, 'Mensagens DTE', 'O Sistema Veri verifica diariamente se há novas mensagens. Com apenas 2 cliques você acessa as mensagens evitando risco, perca de prazos, além de otimizar toda rotina.', 'Robert Damacena', '09', 'Novembro', '2020', '1:31 min', 'MENSAGENS', 0, 'https://www.youtube.com/embed/bLMqYHzmq6I?rel=0&autoplay=0&mute=0'),
(5, 'Monitoramento de MDF-E', 'O Sistema Veri facilita o monitoramento de MDF-E. Confira nesse tutorial como é fácil fazer essa consulta no sistema.', 'Robert Damacena', '09', 'Novembro', '2020', '2:48 min', 'ESTADUAL', 0, 'https://www.youtube.com/embed/Mc5xdd6rQn0?rel=0&autoplay=0&mute=0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_usuarios`
--

CREATE TABLE `dtb_usuarios` (
  `id` bigint(20) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cnpj` varchar(45) DEFAULT NULL,
  `ie` varchar(50) DEFAULT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(15) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `cep` varchar(15) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `logo` varchar(225) DEFAULT NULL,
  `validade` datetime DEFAULT NULL,
  `id_cadastro` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `nivel` tinyint(4) NOT NULL DEFAULT 1,
  `status` char(1) NOT NULL DEFAULT 'A',
  `data_cadastro` datetime NOT NULL,
  `id_contabilidade` bigint(20) DEFAULT NULL,
  `email_sefaz` varchar(150) DEFAULT NULL,
  `senha_sefaz` varchar(150) DEFAULT NULL,
  `servidor_email` varchar(30) DEFAULT NULL,
  `sm` varchar(200) DEFAULT 'afb54a34f16578c7d9c755a7109e2be1',
  `is_funcionario` tinyint(1) DEFAULT NULL,
  `data_acesso` datetime DEFAULT NULL,
  `permissoes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissoes_menu` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissoes_nova` text DEFAULT NULL,
  `data_logout` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dtb_usuarios`
--

INSERT INTO `dtb_usuarios` (`id`, `nome`, `cnpj`, `ie`, `razao_social`, `nome_fantasia`, `logradouro`, `numero`, `complemento`, `bairro`, `cidade`, `uf`, `cep`, `telefone`, `celular`, `logo`, `validade`, `id_cadastro`, `email`, `login`, `senha`, `nivel`, `status`, `data_cadastro`, `id_contabilidade`, `email_sefaz`, `senha_sefaz`, `servidor_email`, `sm`, `is_funcionario`, `data_acesso`, `permissoes`, `permissoes_menu`, `permissoes_nova`, `data_logout`) VALUES
(1, 'DATABYTE TECNOLOGIA', '11.111.111/1111-11', NULL, 'DATABYTE TECNOLOGIA LTda', 'DATABYTE TECNOLOGIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-31 18:24:00', 1, 'contato.databytetecnologia@gmail.com', 'contato.databytetecnologia@gmail.com', 'afb54a34f16578c7d9c755a7109e2be1', 0, 'A', '2017-06-19 18:26:11', NULL, NULL, NULL, NULL, 'afb54a34f16578c7d9c755a7109e2be1', NULL, '2020-07-29 16:01:30', NULL, 'a:23:{s:10:\"mDashboard\";s:1:\"1\";s:23:\"mIndicadoresEmpresarias\";s:1:\"1\";s:16:\"mPendenciasSefaz\";s:1:\"1\";s:14:\"mDividasAtivas\";s:1:\"1\";s:9:\"mGraficos\";s:1:\"1\";s:10:\"mCadastros\";s:1:\"1\";s:9:\"mEmpresas\";s:1:\"1\";s:9:\"mUsuarios\";s:1:\"1\";s:9:\"mContador\";s:1:\"1\";s:14:\"mAlterarSenhas\";s:1:\"1\";s:17:\"mCertidaoNegativa\";s:1:\"1\";s:14:\"mComprasVendas\";s:1:\"1\";s:11:\"mDocumentos\";s:1:\"1\";s:8:\"mAlvaras\";s:1:\"1\";s:16:\"mDocumentosLista\";s:1:\"1\";s:18:\"mTiposDeDocumentos\";s:1:\"1\";s:6:\"mJuceb\";s:1:\"1\";s:25:\"mJucebConsultaViabilidade\";s:1:\"1\";s:17:\"mSituacaoRegistro\";s:1:\"1\";s:18:\"mRegistroDeAcessos\";s:1:\"1\";s:19:\"mProtocoloMensagens\";s:1:\"1\";s:11:\"mCalendario\";s:1:\"1\";s:11:\"mRelatorios\";s:1:\"1\";}', 'a:37:{s:9:\"mEstadual\";s:1:\"1\";s:15:\"mDashboardSefaz\";s:1:\"1\";s:7:\"mMsgDte\";s:1:\"1\";s:13:\"mResumoFiscal\";s:1:\"1\";s:15:\"mSituacaoFiscal\";s:1:\"1\";s:17:\"mCertidaoNegativa\";s:1:\"1\";s:15:\"mProcessosSipro\";s:1:\"1\";s:13:\"mCompraVendas\";s:1:\"1\";s:5:\"mIpva\";s:1:\"1\";s:17:\"mJucebViabilidade\";s:1:\"1\";s:14:\"mJucebRegistro\";s:1:\"1\";s:8:\"mFederal\";s:1:\"1\";s:17:\"mDashboardFederal\";s:1:\"1\";s:8:\"mMsgEcac\";s:1:\"1\";s:19:\"mSituacaoFiscalEcac\";s:1:\"1\";s:20:\"mParcelamentoDasEcac\";s:1:\"1\";s:12:\"mDividaAtiva\";s:1:\"1\";s:11:\"mCalendario\";s:1:\"1\";s:11:\"mProtocolos\";s:1:\"1\";s:10:\"mCadastros\";s:1:\"1\";s:9:\"mEmpresas\";s:1:\"1\";s:9:\"mUsuarios\";s:1:\"1\";s:11:\"mContadores\";s:1:\"1\";s:11:\"mDocumentos\";s:1:\"1\";s:18:\"mTiposDeDocumentos\";s:1:\"1\";s:11:\"mRelatorios\";s:1:\"1\";s:21:\"mRelatorioPorEmpresas\";s:1:\"1\";s:18:\"mRelatorioTimeline\";s:1:\"1\";s:15:\"mRelatorioGeral\";s:1:\"1\";s:24:\"mRelatorioPorColaborador\";s:1:\"1\";s:24:\"mRelatorioSemColaborador\";s:1:\"1\";s:24:\"mRelatorioComCertificado\";s:1:\"1\";s:24:\"mRelatorioSemCertificado\";s:1:\"1\";s:7:\"mPainel\";s:1:\"1\";s:17:\"mRegistroDeAcesso\";s:1:\"1\";s:6:\"mPlano\";s:1:\"1\";s:14:\"mPainelCliente\";s:1:\"1\";}', NULL),
(2, NULL, 'CNPJEMPRESA55', NULL, 'NOMEEMPRESA55', 'NOMEEMPRESA55', 'RUA_EMPRESA', 'NUMERO_EMPRESA', 'Complemento', 'BAIRRO_EMPRESA', 'CIDADE_EMPRESA', 'BA', 'CEP_EMPRESA', NULL, NULL, '', '0000-00-00 00:00:00', 1, 'EMAIL_EMPRESA', 'EMAIL_EMPRESA', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'A', '2018-07-16 08:04:58', NULL, NULL, NULL, '3', 'afb54a34f16578c7d9c755a7109e2be1', NULL, '2020-08-05 14:54:50', NULL, 'a:23:{s:10:\"mDashboard\";s:1:\"1\";s:23:\"mIndicadoresEmpresarias\";s:1:\"1\";s:16:\"mPendenciasSefaz\";s:1:\"1\";s:14:\"mDividasAtivas\";s:1:\"1\";s:9:\"mGraficos\";s:1:\"1\";s:10:\"mCadastros\";s:1:\"1\";s:9:\"mEmpresas\";s:1:\"1\";s:9:\"mUsuarios\";s:1:\"1\";s:9:\"mContador\";s:1:\"1\";s:14:\"mAlterarSenhas\";s:1:\"1\";s:17:\"mCertidaoNegativa\";s:1:\"1\";s:14:\"mComprasVendas\";s:1:\"1\";s:11:\"mDocumentos\";s:1:\"1\";s:8:\"mAlvaras\";s:1:\"1\";s:16:\"mDocumentosLista\";s:1:\"1\";s:18:\"mTiposDeDocumentos\";s:1:\"1\";s:6:\"mJuceb\";s:1:\"1\";s:25:\"mJucebConsultaViabilidade\";s:1:\"1\";s:17:\"mSituacaoRegistro\";s:1:\"1\";s:18:\"mRegistroDeAcessos\";s:1:\"1\";s:19:\"mProtocoloMensagens\";s:1:\"1\";s:11:\"mCalendario\";s:1:\"1\";s:11:\"mRelatorios\";s:1:\"1\";}', 'a:37:{s:9:\"mEstadual\";s:1:\"1\";s:15:\"mDashboardSefaz\";s:1:\"1\";s:7:\"mMsgDte\";s:1:\"1\";s:13:\"mResumoFiscal\";s:1:\"1\";s:15:\"mSituacaoFiscal\";s:1:\"1\";s:17:\"mCertidaoNegativa\";s:1:\"1\";s:15:\"mProcessosSipro\";s:1:\"1\";s:13:\"mCompraVendas\";s:1:\"1\";s:5:\"mIpva\";s:1:\"1\";s:17:\"mJucebViabilidade\";s:1:\"1\";s:14:\"mJucebRegistro\";s:1:\"1\";s:8:\"mFederal\";s:1:\"1\";s:17:\"mDashboardFederal\";s:1:\"1\";s:8:\"mMsgEcac\";s:1:\"1\";s:19:\"mSituacaoFiscalEcac\";s:1:\"1\";s:20:\"mParcelamentoDasEcac\";s:1:\"1\";s:12:\"mDividaAtiva\";s:1:\"1\";s:11:\"mCalendario\";s:1:\"1\";s:11:\"mProtocolos\";s:1:\"1\";s:10:\"mCadastros\";s:1:\"1\";s:9:\"mEmpresas\";s:1:\"1\";s:9:\"mUsuarios\";s:1:\"1\";s:11:\"mContadores\";s:1:\"1\";s:11:\"mDocumentos\";s:1:\"1\";s:18:\"mTiposDeDocumentos\";s:1:\"1\";s:11:\"mRelatorios\";s:1:\"1\";s:21:\"mRelatorioPorEmpresas\";s:1:\"1\";s:18:\"mRelatorioTimeline\";s:1:\"1\";s:15:\"mRelatorioGeral\";s:1:\"1\";s:24:\"mRelatorioPorColaborador\";s:1:\"1\";s:24:\"mRelatorioSemColaborador\";s:1:\"1\";s:24:\"mRelatorioComCertificado\";s:1:\"1\";s:24:\"mRelatorioSemCertificado\";s:1:\"1\";s:7:\"mPainel\";s:1:\"1\";s:17:\"mRegistroDeAcesso\";s:1:\"1\";s:6:\"mPlano\";s:1:\"1\";s:14:\"mPainelCliente\";s:1:\"1\";}', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dtb_viabilidade_juceb`
--

CREATE TABLE `dtb_viabilidade_juceb` (
  `id` int(11) NOT NULL,
  `protocolo` varchar(50) DEFAULT NULL,
  `nome_empresa_mostrar` varchar(255) DEFAULT NULL,
  `nome_da_empresa` varchar(220) DEFAULT NULL,
  `estado_do_processo` varchar(220) DEFAULT NULL,
  `status_de_envio` varchar(220) DEFAULT NULL,
  `data_atualizacao` varchar(220) DEFAULT NULL,
  `data_incorporacao` varchar(220) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL,
  `id_cadastro` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos`
--

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `datainicio` date DEFAULT NULL,
  `datafim` date DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `remover` tinyint(1) NOT NULL DEFAULT 0,
  `cor` varchar(100) NOT NULL,
  `usuario_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos_criados`
--

CREATE TABLE `eventos_criados` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `datainicio` datetime DEFAULT NULL,
  `datafim` datetime DEFAULT NULL,
  `cor` varchar(100) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_documento` int(11) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `id_evento` int(11) DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico_empresas`
--

CREATE TABLE `historico_empresas` (
  `id` bigint(20) NOT NULL,
  `id_empresa` bigint(20) NOT NULL,
  `inscricao_estadual_completo` varchar(20) NOT NULL,
  `cnpj_completo` varchar(25) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `mei` varchar(4) DEFAULT NULL,
  `natureza_juridica` varchar(255) DEFAULT NULL,
  `unidade_atendimento` varchar(255) DEFAULT NULL,
  `unidade_fiscalizacao` varchar(255) DEFAULT NULL,
  `cep` varchar(15) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `uf` char(2) DEFAULT NULL,
  `referencia` varchar(255) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `situacao_cadastral` varchar(10) DEFAULT NULL,
  `situacao` varchar(15) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `situacao_dte` varchar(50) DEFAULT NULL,
  `situacao_conta_dte` varchar(50) DEFAULT NULL,
  `atividade_principal` varchar(255) DEFAULT NULL,
  `condicao` varchar(255) DEFAULT NULL,
  `forma_pagamento` varchar(255) DEFAULT NULL,
  `motivo_situacao_cadastral` varchar(255) DEFAULT NULL,
  `crc_contador` varchar(30) DEFAULT NULL,
  `nome_contador` varchar(255) DEFAULT NULL,
  `crc_responsavel` varchar(30) DEFAULT NULL,
  `nome_responsavel` varchar(255) DEFAULT NULL,
  `data_atualizacao` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nfe_destinatario`
--

CREATE TABLE `nfe_destinatario` (
  `cnpj_data` varchar(22) DEFAULT NULL,
  `doc_sefaz` varchar(9) DEFAULT NULL,
  `cnpj_emit` varchar(18) DEFAULT NULL,
  `razao_emit` varchar(200) DEFAULT NULL,
  `data_emissao` varchar(10) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `chave_nfe` varchar(44) NOT NULL,
  `situacao_nfe` varchar(20) DEFAULT NULL,
  `tipo_operacao` varchar(20) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `mdf_evento` varchar(100) DEFAULT NULL,
  `mdf_data` varchar(10) DEFAULT NULL,
  `user_report` varchar(50) DEFAULT NULL,
  `data_report` varchar(19) DEFAULT NULL,
  `consultou_mdfe` varchar(1) DEFAULT NULL,
  `cidade_emitente` varchar(500) DEFAULT NULL,
  `cidade_destinatario` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nfe_emitente`
--

CREATE TABLE `nfe_emitente` (
  `cnpj_data` varchar(22) DEFAULT NULL,
  `doc_sefaz` varchar(9) DEFAULT NULL,
  `cnpj_dest` varchar(18) DEFAULT NULL,
  `razao_dest` varchar(200) DEFAULT NULL,
  `data_emissao` varchar(10) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `chave_nfe` varchar(44) NOT NULL,
  `situacao_nfe` varchar(20) DEFAULT NULL,
  `tipo_operacao` varchar(20) DEFAULT NULL,
  `user_report` varchar(50) DEFAULT NULL,
  `data_report` varchar(19) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificacoes`
--

CREATE TABLE `notificacoes` (
  `id` bigint(20) NOT NULL,
  `id_empresa` bigint(20) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `situacao` varchar(15) DEFAULT NULL,
  `id_usuario` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `vinculo_contador` tinyint(1) DEFAULT NULL,
  `situacao_dte` varchar(15) DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificacoesdocumento`
--

CREATE TABLE `notificacoesdocumento` (
  `id` bigint(20) NOT NULL,
  `id_documento` bigint(20) NOT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `tipo_documento` varchar(15) DEFAULT NULL,
  `dataValidade` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `id_usuario` bigint(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificacoesprocessosipro`
--

CREATE TABLE `notificacoesprocessosipro` (
  `id` bigint(20) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `id_processo_sipro` int(11) NOT NULL,
  `numero_processo` varchar(255) DEFAULT NULL,
  `numero_processo_formatado` varchar(255) DEFAULT NULL,
  `objetivo` varchar(500) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `envio_data` varchar(500) DEFAULT NULL,
  `envio_local` varchar(500) DEFAULT NULL,
  `recepcao_data` varchar(500) DEFAULT NULL,
  `recepcao_local` varchar(500) DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificacoesresumofiscal`
--

CREATE TABLE `notificacoesresumofiscal` (
  `id` bigint(20) NOT NULL,
  `cnpjcpf` bigint(20) NOT NULL,
  `razaoSocial` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `cnpjstring` varchar(500) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `protocolo`
--

CREATE TABLE `protocolo` (
  `id` int(11) NOT NULL,
  `usuario` varchar(200) NOT NULL,
  `data` varchar(100) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `idnotificacao` int(11) DEFAULT NULL,
  `idnotificacaoresumo` int(11) DEFAULT NULL,
  `idnotificacaoprocessosipro` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tentativas_acesso`
--

CREATE TABLE `tentativas_acesso` (
  `id` bigint(11) NOT NULL,
  `nome_usuario` varchar(255) NOT NULL,
  `data_hora` datetime DEFAULT NULL,
  `senha_usada` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ultimo_acesso`
--

CREATE TABLE `ultimo_acesso` (
  `id` bigint(11) NOT NULL,
  `id_usuario` bigint(11) NOT NULL,
  `data_hora` datetime DEFAULT NULL,
  `acao` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `contadores`
--
ALTER TABLE `contadores`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `controle`
--
ALTER TABLE `controle`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dae`
--
ALTER TABLE `dae`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_alvara`
--
ALTER TABLE `dtb_alvara`
  ADD PRIMARY KEY (`cnpj_sem_formatacao`);

--
-- Índices para tabela `dtb_anexo`
--
ALTER TABLE `dtb_anexo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_anexos_emails`
--
ALTER TABLE `dtb_anexos_emails`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_caixadeemail`
--
ALTER TABLE `dtb_caixadeemail`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_certidao_barreiras`
--
ALTER TABLE `dtb_certidao_barreiras`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_certidao_caixa`
--
ALTER TABLE `dtb_certidao_caixa`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_certidao_conquista`
--
ALTER TABLE `dtb_certidao_conquista`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_certidao_estadual_sp`
--
ALTER TABLE `dtb_certidao_estadual_sp`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_certidao_feira`
--
ALTER TABLE `dtb_certidao_feira`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_certidao_luizeduardo`
--
ALTER TABLE `dtb_certidao_luizeduardo`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_certidao_receita`
--
ALTER TABLE `dtb_certidao_receita`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_certidao_sefaz`
--
ALTER TABLE `dtb_certidao_sefaz`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_certidao_sp`
--
ALTER TABLE `dtb_certidao_sp`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_certidao_trabalhista`
--
ALTER TABLE `dtb_certidao_trabalhista`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_certificado`
--
ALTER TABLE `dtb_certificado`
  ADD PRIMARY KEY (`cnpj_data`);

--
-- Índices para tabela `dtb_certificado_contador`
--
ALTER TABLE `dtb_certificado_contador`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_chat`
--
ALTER TABLE `dtb_chat`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_clientes_emails`
--
ALTER TABLE `dtb_clientes_emails`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_clientes_empresas`
--
ALTER TABLE `dtb_clientes_empresas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_cnd_salvador`
--
ALTER TABLE `dtb_cnd_salvador`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_consulta_pedidos`
--
ALTER TABLE `dtb_consulta_pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_contador_procuracao`
--
ALTER TABLE `dtb_contador_procuracao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_dctf_sem_movimento`
--
ALTER TABLE `dtb_dctf_sem_movimento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_dec_caixapostal_comunicados`
--
ALTER TABLE `dtb_dec_caixapostal_comunicados`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_dec_caixapostal_mensagens`
--
ALTER TABLE `dtb_dec_caixapostal_mensagens`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_dec_comunicados`
--
ALTER TABLE `dtb_dec_comunicados`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_dec_mensagens`
--
ALTER TABLE `dtb_dec_mensagens`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_divida_ativa_detalhada`
--
ALTER TABLE `dtb_divida_ativa_detalhada`
  ADD PRIMARY KEY (`numero_inscricao`);

--
-- Índices para tabela `dtb_divida_ativa_fgts`
--
ALTER TABLE `dtb_divida_ativa_fgts`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_divida_ativa_fgts_n`
--
ALTER TABLE `dtb_divida_ativa_fgts_n`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_divida_ativa_geral`
--
ALTER TABLE `dtb_divida_ativa_geral`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Índices para tabela `dtb_divida_ativa_multacriminal`
--
ALTER TABLE `dtb_divida_ativa_multacriminal`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_divida_ativa_multaeleitoral`
--
ALTER TABLE `dtb_divida_ativa_multaeleitoral`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_divida_ativa_multatrabalhista`
--
ALTER TABLE `dtb_divida_ativa_multatrabalhista`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_divida_ativa_previdencia`
--
ALTER TABLE `dtb_divida_ativa_previdencia`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_divida_ativa_previdencia_n`
--
ALTER TABLE `dtb_divida_ativa_previdencia_n`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_documentos`
--
ALTER TABLE `dtb_documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_empresa` (`id_empresa`),
  ADD KEY `id_tipoDocumento` (`id_tipoDocumento`);

--
-- Índices para tabela `dtb_documento_anexo`
--
ALTER TABLE `dtb_documento_anexo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_caixa_postal`
--
ALTER TABLE `dtb_ecac_caixa_postal`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_caixa_postal_mensagem`
--
ALTER TABLE `dtb_ecac_caixa_postal_mensagem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `caixa_postal_id` (`caixa_postal_id`);

--
-- Índices para tabela `dtb_ecac_das`
--
ALTER TABLE `dtb_ecac_das`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_das_debitos`
--
ALTER TABLE `dtb_ecac_das_debitos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_dctf`
--
ALTER TABLE `dtb_ecac_dctf`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_eprocessos_ativos`
--
ALTER TABLE `dtb_ecac_eprocessos_ativos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_eprocessos_ativos_historico`
--
ALTER TABLE `dtb_ecac_eprocessos_ativos_historico`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_eprocessos_inativos`
--
ALTER TABLE `dtb_ecac_eprocessos_inativos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_eprocessos_inativos_historico`
--
ALTER TABLE `dtb_ecac_eprocessos_inativos_historico`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_procuracao`
--
ALTER TABLE `dtb_ecac_procuracao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_simplesnacional_demonstrativo_pagamentos`
--
ALTER TABLE `dtb_ecac_simplesnacional_demonstrativo_pagamentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_simplesnacional_pedidos_parcelamentos`
--
ALTER TABLE `dtb_ecac_simplesnacional_pedidos_parcelamentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ecac_simplesnacional_relacao_debitos_parcelas`
--
ALTER TABLE `dtb_ecac_simplesnacional_relacao_debitos_parcelas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_email`
--
ALTER TABLE `dtb_email`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_empresas`
--
ALTER TABLE `dtb_empresas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cnpj` (`cnpj`) USING BTREE;

--
-- Índices para tabela `dtb_empresas_excluidas_mei`
--
ALTER TABLE `dtb_empresas_excluidas_mei`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_empresas_excluidas_simples`
--
ALTER TABLE `dtb_empresas_excluidas_simples`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_empresas_sem_procuracao`
--
ALTER TABLE `dtb_empresas_sem_procuracao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_empresa_usuario`
--
ALTER TABLE `dtb_empresa_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_exclusao_simples`
--
ALTER TABLE `dtb_exclusao_simples`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_expedidor`
--
ALTER TABLE `dtb_expedidor`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_financeiro`
--
ALTER TABLE `dtb_financeiro`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_ipva`
--
ALTER TABLE `dtb_ipva`
  ADD PRIMARY KEY (`cnpj`);

--
-- Índices para tabela `dtb_juceb`
--
ALTER TABLE `dtb_juceb`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_juceb_dados`
--
ALTER TABLE `dtb_juceb_dados`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_limite_simples`
--
ALTER TABLE `dtb_limite_simples`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_logins_validos`
--
ALTER TABLE `dtb_logins_validos`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Índices para tabela `dtb_mensagens_dte`
--
ALTER TABLE `dtb_mensagens_dte`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_barreiras`
--
ALTER TABLE `dtb_notificacao_certidao_barreiras`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_caixa`
--
ALTER TABLE `dtb_notificacao_certidao_caixa`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_conquista`
--
ALTER TABLE `dtb_notificacao_certidao_conquista`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_feira`
--
ALTER TABLE `dtb_notificacao_certidao_feira`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_luis_eduardo`
--
ALTER TABLE `dtb_notificacao_certidao_luis_eduardo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_municipio_sp`
--
ALTER TABLE `dtb_notificacao_certidao_municipio_sp`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_receita`
--
ALTER TABLE `dtb_notificacao_certidao_receita`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_salvador`
--
ALTER TABLE `dtb_notificacao_certidao_salvador`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_sefaz`
--
ALTER TABLE `dtb_notificacao_certidao_sefaz`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_sefaz_sp`
--
ALTER TABLE `dtb_notificacao_certidao_sefaz_sp`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_certidao_trabalhista`
--
ALTER TABLE `dtb_notificacao_certidao_trabalhista`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_das_nao_pagos`
--
ALTER TABLE `dtb_notificacao_das_nao_pagos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_das_pagos`
--
ALTER TABLE `dtb_notificacao_das_pagos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_dctf`
--
ALTER TABLE `dtb_notificacao_dctf`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_dctf_nao_entregue`
--
ALTER TABLE `dtb_notificacao_dctf_nao_entregue`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_dctf_proxima_vencimento`
--
ALTER TABLE `dtb_notificacao_dctf_proxima_vencimento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_dctf_sem_movimento`
--
ALTER TABLE `dtb_notificacao_dctf_sem_movimento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_ecac_cadin`
--
ALTER TABLE `dtb_notificacao_ecac_cadin`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_ecac_eprocessos_ativos`
--
ALTER TABLE `dtb_notificacao_ecac_eprocessos_ativos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_ecac_eprocessos_inativos`
--
ALTER TABLE `dtb_notificacao_ecac_eprocessos_inativos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_ecac_situacao`
--
ALTER TABLE `dtb_notificacao_ecac_situacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_empresas_excluidas_mei`
--
ALTER TABLE `dtb_notificacao_empresas_excluidas_mei`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_empresas_excluidas_simples`
--
ALTER TABLE `dtb_notificacao_empresas_excluidas_simples`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_juceb_viabilidade`
--
ALTER TABLE `dtb_notificacao_juceb_viabilidade`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_pgdas_nao_entregue`
--
ALTER TABLE `dtb_notificacao_pgdas_nao_entregue`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_pgdas_proxima_vencimento`
--
ALTER TABLE `dtb_notificacao_pgdas_proxima_vencimento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_pgdas_transmitidos`
--
ALTER TABLE `dtb_notificacao_pgdas_transmitidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_notificacao_sublimite_simples`
--
ALTER TABLE `dtb_notificacao_sublimite_simples`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_parcelas_emitidas`
--
ALTER TABLE `dtb_parcelas_emitidas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_plano_contratado`
--
ALTER TABLE `dtb_plano_contratado`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_resumofiscal`
--
ALTER TABLE `dtb_resumofiscal`
  ADD PRIMARY KEY (`cnpjcpf`);

--
-- Índices para tabela `dtb_sefaz_destinatario_mdfe`
--
ALTER TABLE `dtb_sefaz_destinatario_mdfe`
  ADD UNIQUE KEY `tbl_sefaz_destinatario_mdfe_chave_acesso_uindex` (`chave_acesso`);

--
-- Índices para tabela `dtb_situacao_cadin`
--
ALTER TABLE `dtb_situacao_cadin`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_situacao_fiscal`
--
ALTER TABLE `dtb_situacao_fiscal`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_tipodedocumento`
--
ALTER TABLE `dtb_tipodedocumento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_tramitacao_processo_sipro`
--
ALTER TABLE `dtb_tramitacao_processo_sipro`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_tramitacao_processo_sipro_documento`
--
ALTER TABLE `dtb_tramitacao_processo_sipro_documento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_tutorial`
--
ALTER TABLE `dtb_tutorial`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_usuarios`
--
ALTER TABLE `dtb_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dtb_viabilidade_juceb`
--
ALTER TABLE `dtb_viabilidade_juceb`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `eventos_criados`
--
ALTER TABLE `eventos_criados`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `historico_empresas`
--
ALTER TABLE `historico_empresas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Índices para tabela `nfe_destinatario`
--
ALTER TABLE `nfe_destinatario`
  ADD PRIMARY KEY (`chave_nfe`);

--
-- Índices para tabela `nfe_emitente`
--
ALTER TABLE `nfe_emitente`
  ADD PRIMARY KEY (`chave_nfe`);

--
-- Índices para tabela `notificacoes`
--
ALTER TABLE `notificacoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Índices para tabela `notificacoesdocumento`
--
ALTER TABLE `notificacoesdocumento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_documento` (`id_documento`);

--
-- Índices para tabela `notificacoesprocessosipro`
--
ALTER TABLE `notificacoesprocessosipro`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `notificacoesresumofiscal`
--
ALTER TABLE `notificacoesresumofiscal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cnpjcpf` (`cnpjcpf`);

--
-- Índices para tabela `protocolo`
--
ALTER TABLE `protocolo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tentativas_acesso`
--
ALTER TABLE `tentativas_acesso`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `ultimo_acesso`
--
ALTER TABLE `ultimo_acesso`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `contadores`
--
ALTER TABLE `contadores`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `controle`
--
ALTER TABLE `controle`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dae`
--
ALTER TABLE `dae`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_anexo`
--
ALTER TABLE `dtb_anexo`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_anexos_emails`
--
ALTER TABLE `dtb_anexos_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_caixadeemail`
--
ALTER TABLE `dtb_caixadeemail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_certidao_caixa`
--
ALTER TABLE `dtb_certidao_caixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_certidao_feira`
--
ALTER TABLE `dtb_certidao_feira`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_certidao_sefaz`
--
ALTER TABLE `dtb_certidao_sefaz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_certidao_trabalhista`
--
ALTER TABLE `dtb_certidao_trabalhista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_certificado_contador`
--
ALTER TABLE `dtb_certificado_contador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_chat`
--
ALTER TABLE `dtb_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_clientes_emails`
--
ALTER TABLE `dtb_clientes_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_clientes_empresas`
--
ALTER TABLE `dtb_clientes_empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_consulta_pedidos`
--
ALTER TABLE `dtb_consulta_pedidos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_contador_procuracao`
--
ALTER TABLE `dtb_contador_procuracao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_dctf_sem_movimento`
--
ALTER TABLE `dtb_dctf_sem_movimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_dec_caixapostal_comunicados`
--
ALTER TABLE `dtb_dec_caixapostal_comunicados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `dtb_dec_caixapostal_mensagens`
--
ALTER TABLE `dtb_dec_caixapostal_mensagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_dec_comunicados`
--
ALTER TABLE `dtb_dec_comunicados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_dec_mensagens`
--
ALTER TABLE `dtb_dec_mensagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_divida_ativa_fgts`
--
ALTER TABLE `dtb_divida_ativa_fgts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_divida_ativa_previdencia`
--
ALTER TABLE `dtb_divida_ativa_previdencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_documentos`
--
ALTER TABLE `dtb_documentos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_documento_anexo`
--
ALTER TABLE `dtb_documento_anexo`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_caixa_postal`
--
ALTER TABLE `dtb_ecac_caixa_postal`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_caixa_postal_mensagem`
--
ALTER TABLE `dtb_ecac_caixa_postal_mensagem`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_das`
--
ALTER TABLE `dtb_ecac_das`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_das_debitos`
--
ALTER TABLE `dtb_ecac_das_debitos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_dctf`
--
ALTER TABLE `dtb_ecac_dctf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_eprocessos_ativos`
--
ALTER TABLE `dtb_ecac_eprocessos_ativos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_eprocessos_ativos_historico`
--
ALTER TABLE `dtb_ecac_eprocessos_ativos_historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_eprocessos_inativos`
--
ALTER TABLE `dtb_ecac_eprocessos_inativos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_eprocessos_inativos_historico`
--
ALTER TABLE `dtb_ecac_eprocessos_inativos_historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_procuracao`
--
ALTER TABLE `dtb_ecac_procuracao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_simplesnacional_demonstrativo_pagamentos`
--
ALTER TABLE `dtb_ecac_simplesnacional_demonstrativo_pagamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_simplesnacional_pedidos_parcelamentos`
--
ALTER TABLE `dtb_ecac_simplesnacional_pedidos_parcelamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_ecac_simplesnacional_relacao_debitos_parcelas`
--
ALTER TABLE `dtb_ecac_simplesnacional_relacao_debitos_parcelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_email`
--
ALTER TABLE `dtb_email`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_empresas`
--
ALTER TABLE `dtb_empresas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_empresas_excluidas_mei`
--
ALTER TABLE `dtb_empresas_excluidas_mei`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_empresas_excluidas_simples`
--
ALTER TABLE `dtb_empresas_excluidas_simples`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_empresas_sem_procuracao`
--
ALTER TABLE `dtb_empresas_sem_procuracao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_empresa_usuario`
--
ALTER TABLE `dtb_empresa_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_exclusao_simples`
--
ALTER TABLE `dtb_exclusao_simples`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_expedidor`
--
ALTER TABLE `dtb_expedidor`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_financeiro`
--
ALTER TABLE `dtb_financeiro`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_juceb`
--
ALTER TABLE `dtb_juceb`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_juceb_dados`
--
ALTER TABLE `dtb_juceb_dados`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_limite_simples`
--
ALTER TABLE `dtb_limite_simples`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_mensagens_dte`
--
ALTER TABLE `dtb_mensagens_dte`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_barreiras`
--
ALTER TABLE `dtb_notificacao_certidao_barreiras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_caixa`
--
ALTER TABLE `dtb_notificacao_certidao_caixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_conquista`
--
ALTER TABLE `dtb_notificacao_certidao_conquista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_feira`
--
ALTER TABLE `dtb_notificacao_certidao_feira`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_luis_eduardo`
--
ALTER TABLE `dtb_notificacao_certidao_luis_eduardo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_municipio_sp`
--
ALTER TABLE `dtb_notificacao_certidao_municipio_sp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_receita`
--
ALTER TABLE `dtb_notificacao_certidao_receita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_salvador`
--
ALTER TABLE `dtb_notificacao_certidao_salvador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_sefaz`
--
ALTER TABLE `dtb_notificacao_certidao_sefaz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_sefaz_sp`
--
ALTER TABLE `dtb_notificacao_certidao_sefaz_sp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_certidao_trabalhista`
--
ALTER TABLE `dtb_notificacao_certidao_trabalhista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_das_nao_pagos`
--
ALTER TABLE `dtb_notificacao_das_nao_pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_das_pagos`
--
ALTER TABLE `dtb_notificacao_das_pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_dctf`
--
ALTER TABLE `dtb_notificacao_dctf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_dctf_nao_entregue`
--
ALTER TABLE `dtb_notificacao_dctf_nao_entregue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_dctf_proxima_vencimento`
--
ALTER TABLE `dtb_notificacao_dctf_proxima_vencimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_dctf_sem_movimento`
--
ALTER TABLE `dtb_notificacao_dctf_sem_movimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_ecac_cadin`
--
ALTER TABLE `dtb_notificacao_ecac_cadin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_ecac_eprocessos_ativos`
--
ALTER TABLE `dtb_notificacao_ecac_eprocessos_ativos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_ecac_eprocessos_inativos`
--
ALTER TABLE `dtb_notificacao_ecac_eprocessos_inativos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_ecac_situacao`
--
ALTER TABLE `dtb_notificacao_ecac_situacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_empresas_excluidas_mei`
--
ALTER TABLE `dtb_notificacao_empresas_excluidas_mei`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_empresas_excluidas_simples`
--
ALTER TABLE `dtb_notificacao_empresas_excluidas_simples`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_juceb_viabilidade`
--
ALTER TABLE `dtb_notificacao_juceb_viabilidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_pgdas_nao_entregue`
--
ALTER TABLE `dtb_notificacao_pgdas_nao_entregue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_pgdas_proxima_vencimento`
--
ALTER TABLE `dtb_notificacao_pgdas_proxima_vencimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_pgdas_transmitidos`
--
ALTER TABLE `dtb_notificacao_pgdas_transmitidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_notificacao_sublimite_simples`
--
ALTER TABLE `dtb_notificacao_sublimite_simples`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_parcelas_emitidas`
--
ALTER TABLE `dtb_parcelas_emitidas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_plano_contratado`
--
ALTER TABLE `dtb_plano_contratado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `dtb_situacao_cadin`
--
ALTER TABLE `dtb_situacao_cadin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_situacao_fiscal`
--
ALTER TABLE `dtb_situacao_fiscal`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_tipodedocumento`
--
ALTER TABLE `dtb_tipodedocumento`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_tramitacao_processo_sipro`
--
ALTER TABLE `dtb_tramitacao_processo_sipro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_tramitacao_processo_sipro_documento`
--
ALTER TABLE `dtb_tramitacao_processo_sipro_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `dtb_tutorial`
--
ALTER TABLE `dtb_tutorial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `dtb_usuarios`
--
ALTER TABLE `dtb_usuarios`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `dtb_viabilidade_juceb`
--
ALTER TABLE `dtb_viabilidade_juceb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `eventos_criados`
--
ALTER TABLE `eventos_criados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `historico_empresas`
--
ALTER TABLE `historico_empresas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `notificacoes`
--
ALTER TABLE `notificacoes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `notificacoesdocumento`
--
ALTER TABLE `notificacoesdocumento`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `notificacoesprocessosipro`
--
ALTER TABLE `notificacoesprocessosipro`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `notificacoesresumofiscal`
--
ALTER TABLE `notificacoesresumofiscal`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `protocolo`
--
ALTER TABLE `protocolo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tentativas_acesso`
--
ALTER TABLE `tentativas_acesso`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `ultimo_acesso`
--
ALTER TABLE `ultimo_acesso`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

-- ESPAÇO RESERVADO PARA NOVOS SCRIPTS
CREATE TABLE IF NOT EXISTS `dtb_certidao_riograndedosul` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_riograndedosul`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_portoalegre` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_portoalegre`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_aracaju` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_aracaju`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_sergipe` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_sergipe`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_riodejaneiro` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_riodejaneiro`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_ceara` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_ceara`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_fortaleza` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_fortaleza`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE `dtb_pendencia_gfip` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(500) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `dtb_pendencia_gfip_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(500) DEFAULT NULL,
 `competencia` varchar(100) DEFAULT NULL,
 `fpas` varchar(100) DEFAULT NULL,
 `situacao` varchar(100) DEFAULT NULL,
 `rubrica` varchar(500) DEFAULT NULL,
 `valor` varchar(500) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `dtb_certidao_previdenciaria` (
  `id` varchar(200) NOT NULL DEFAULT '',
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_emissao` datetime DEFAULT NULL,
  `data_validade` datetime DEFAULT NULL,
  `data_cancelamento` datetime DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_previdenciaria`
  ADD PRIMARY KEY (`id`);

CREATE TABLE `dtb_ausencia_gfip` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_gfip_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `cnpj_cei` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_notificacao_gfip_pendencia` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(250) DEFAULT NULL,
 `data` datetime DEFAULT NULL,
 `descricao` varchar(500) DEFAULT NULL,
 `lida` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `notificacoes_gfip_pendencia`
--

CREATE TRIGGER `notificacoes_gfip_pendencia` AFTER UPDATE ON `dtb_pendencia_gfip` FOR EACH ROW BEGIN

IF(trim(NEW.possui_pendencia) != trim(OLD.possui_pendencia)) THEN
  IF(trim(NEW.possui_pendencia) = 1) THEN
    INSERT INTO dtb_notificacao_gfip_pendencia (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'PENDENCIA', 0);
  ELSEIF (trim(NEW.possui_pendencia) = 0) THEN
    INSERT INTO dtb_notificacao_gfip_pendencia (cnpj, data, descricao, lida) VALUES (OLD.cnpj, now(), 'REGULAR', 0);
  END IF;
END IF;

END;

CREATE TABLE `dtb_simplesnacional_parcelas_emitidas` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `cnpj` varchar(500) DEFAULT NULL,
     `valor` varchar(100) DEFAULT NULL,
     `data_parcela` varchar(100) DEFAULT NULL,
     `path_download_parcela` varchar(500) DEFAULT NULL,
     PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `dtb_certidao_pernambuco` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_pernambuco`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_santa_catarina` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_santa_catarina`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE `dtb_ausencia_defis` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_defis_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_dasn` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_dasn_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_dctf` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_dctf_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_dirf` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_dirf_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_ecf` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_ecf_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_efd` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_efd_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_pgdas` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `possui_pendencia` varchar(20) DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_ausencia_pgdas_detalhe` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(100) DEFAULT NULL,
 `tipo` varchar(100) DEFAULT NULL,
 `tipo_periodo` varchar(100) DEFAULT NULL,
 `periodo` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_malha_fiscal` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `id_mensagem` varchar(100) DEFAULT NULL,
 `cnpj` varchar(100) DEFAULT NULL,
 `razao_social` varchar(500) DEFAULT NULL,
 `assunto` varchar(500) DEFAULT NULL,
 `conteudo` varchar(1000) DEFAULT NULL,
 `lida` int(11) DEFAULT NULL,
 `data` datetime DEFAULT NULL,
 `data_execucao` datetime DEFAULT NULL,
 `situacao` varchar(100) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `dtb_malha_fiscal_historico` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `id_malha_fiscal` int(11) DEFAULT NULL,
 `cnpj` varchar(100) DEFAULT NULL,
 `data_alteracao` datetime DEFAULT NULL,
 `nome_usuario` varchar(500) DEFAULT NULL,
 `situacao` varchar(1000) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `dtb_notificacao_malha_fiscal` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `cnpj` varchar(250) DEFAULT NULL,
 `data` datetime DEFAULT NULL,
 `descricao` varchar(500) DEFAULT NULL,
 `lida` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TRIGGER `notificacoes_malha_fiscal` AFTER INSERT ON `dtb_malha_fiscal` FOR EACH ROW BEGIN

INSERT INTO dtb_notificacao_malha_fiscal (cnpj, data, descricao, lida) VALUES (NEW.cnpj, now(), 'MALHA FISCAL', 0);

END;

CREATE TABLE `dtb_ecac_sessoes` (
  `cnpj` varchar(14) NOT NULL,
  `aspsession` varchar(255) NOT NULL,
  `cookiecav` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dtb_dec_qtd_mensagem` (
  `cnpj` varchar(14) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `qtd_nao_lida` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `dtb_historico_leitura_mensagem_ecac` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `id_mensagem` int(11) DEFAULT NULL,
 `assunto` varchar(500) DEFAULT NULL,
 `recebida_em` datetime DEFAULT NULL,
 `caixa_postal_id` int(11) DEFAULT NULL,
 `cnpj` varchar(100) DEFAULT NULL,
 `data_alteracao` datetime DEFAULT NULL,
 `nome_usuario` varchar(500) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `dtb_certidao_varginha` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_varginha`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_belohorizonte` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_belohorizonte`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_minas` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_minas`
  ADD PRIMARY KEY (`cnpj`);

CREATE TABLE IF NOT EXISTS `dtb_certidao_cuiaba` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(200) DEFAULT NULL,
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
ALTER TABLE `dtb_certidao_cuiaba`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `dtb_certidao_cuiaba`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE IF NOT EXISTS `dtb_certidao_matogrosso` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(200) DEFAULT NULL,
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob,
  `observacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtb_certidao_matogrosso`
  ADD PRIMARY KEY (`id`); 

ALTER TABLE `dtb_certidao_matogrosso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `dtb_certidao_alagoas` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
ALTER TABLE `dtb_certidao_alagoas`
  ADD PRIMARY KEY (`cnpj`);
COMMIT;

CREATE TABLE `dtb_certidao_amapa` (
  `cnpj` varchar(200) NOT NULL DEFAULT '',
  `cnpj_completo` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(200) DEFAULT NULL,
  `inscricao_estadual_completo` varchar(200) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `caminho_download` varchar(255) DEFAULT NULL,
  `data_pdf` blob DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `data_execucao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 
ALTER TABLE `dtb_certidao_amapa`
  ADD PRIMARY KEY (`cnpj`);
COMMIT;


CREATE TABLE `dtb_notificacao_atualizacoes` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `data` datetime DEFAULT NULL,
 `descricao` varchar(1000) DEFAULT NULL,
 `link_download` varchar(500) DEFAULT NULL,
 `lida` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- FIM DO ESPAÇO RESERVADO

--
-- Limitadores para a tabela `dtb_ecac_caixa_postal_mensagem`
--
ALTER TABLE `dtb_ecac_caixa_postal_mensagem`
  ADD CONSTRAINT `dtb_ecac_caixa_postal_mensagem_ibfk_1` FOREIGN KEY (`caixa_postal_id`) REFERENCES `dtb_ecac_caixa_postal` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
