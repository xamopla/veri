from seleniumwire import webdriver  # Import from seleniumwire
from selenium.webdriver.chrome.options import Options
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import json
from selenium.webdriver.support.ui import Select
import os
from shutil import copyfile
import uuid
from urllib import parse
import re
import sys

class DecQtd:
	def __init__(self, cert_path):
		self.cert_path = cert_path
		
		options  =  { 
			'backend' :  'mitmproxy',
			'mitm_client_certs': self.cert_path
		} 
		
		chrome_options = Options()
		chrome_options.add_argument("--headless")
		chrome_options.add_argument('--start-maximized')
		chrome_options.add_argument("--no-sandbox")
		chrome_options.add_argument("--disable-dev-shm-usage")

		self.driver  =  webdriver.Chrome(ChromeDriverManager().install(), seleniumwire_options = options, options=chrome_options)
		self.driver.get('https://www.dec.fazenda.sp.gov.br/DEC/UCLogin/Login.aspx')
		inputLogin = self.driver.find_element_by_xpath('/html/body/form/table/tbody/tr[4]/td/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/div/table/tbody/tr/td[2]/div/table[1]/tbody/tr/td/div/input')
		inputLogin.click()

		try:
			self.driver.find_element_by_id('ctl00_ConteudoPagina_tabContainerServicos_tabResponsavel_tab')
			achou_tag_responsavel = True
		except:
			achou_tag_responsavel = False

		if achou_tag_responsavel:
			buttonProcurador = self.driver.find_element_by_id('ctl00_ConteudoPagina_tabContainerServicos_tabProcurador_tab')
			buttonProcurador.click()
			time.sleep(3)

		
		
	def buscar(self):
		
		resultado = []
		
		self.raspar_conteudo(resultado)

		while self.paginar():
			self.raspar_conteudo(resultado)
		self.driver.close()
		return resultado

	def raspar_conteudo(self, resultado):
		try:
			table = WebDriverWait(self.driver, 120).until(
			EC.presence_of_element_located((By.ID, "ctl00_ConteudoPagina_tabContainerServicos_tabProcurador_gvServicosProcurador"))
		)
		except:
			pass
		trs = table.find_elements(By.TAG_NAME, 'tr')
		i = 0
		for tr in trs:
			if i > 0:
				qtd_nao_lida = self.remove_specialchar(tr.find_elements(By.TAG_NAME, "td")[2].find_element(By.TAG_NAME, "span").text.strip())
				if not qtd_nao_lida:
					qtd_nao_lida = 0
				resultado.append({
					'cnpj': self.remove_specialchar(tr.find_elements(By.TAG_NAME, "td")[0].find_element(By.TAG_NAME, "a").text.strip()),
					'razao_social': tr.find_elements(By.TAG_NAME, "td")[1].find_element(By.TAG_NAME, "span").text.strip(),	
					'qtd_nao_lida': qtd_nao_lida,	
				})
			i = i+1

	def remove_specialchar(self, mystring):
		return re.sub('[^A-Za-z0-9]+', '', mystring)

	def paginar(self):
		try:
			paginacao = self.driver.find_element(By.ID, 'ctl00_ConteudoPagina_tabContainerServicos_tabProcurador_DataPager1')
			tagsa = paginacao.find_elements(By.TAG_NAME, 'a')
			for a in tagsa:
				if a.text.strip() == '›':
					a.click()
					return True
			return False
		except:
			return False


d = DecQtd(sys.argv[1])

print(d.buscar())