import mysql.connector

class DataBase:

    def __init__(self, dbase):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="12*bDV3r1sA0pAuL0*21",
            database=dbase
        )
    
    def get_certs(self):
        mycursor = self.mydb.cursor()
        mycursor.execute("""
            select 
                dtb_certificado_contador.id_contador,
                dtb_certificado_contador.caminho_arq,
                dtb_certificado_contador.pass
            from dtb_certificado_contador
        """)
        myresult = mycursor.fetchall()
        return myresult

    def get_empresas_by_contador_id(self, id_contador):
        mycursor = self.mydb.cursor()
        mycursor.execute(f"""
            select
                dtb_empresas.id as id_empresa,
                dtb_empresas.cnpj as cnpj
                from dtb_contador_procuracao
                    inner join dtb_empresas
                        on dtb_empresas.id = dtb_contador_procuracao.id_empresa
                where dtb_empresas.uf = 'SP'
                    and dtb_contador_procuracao.id_contador = '{id_contador}'
        """)
        myresult = mycursor.fetchall()
        return myresult
    
    def insert(self, doc):
        mycursor = self.mydb.cursor()
        

        sql = f"""
            SELECT
                * 
            FROM dtb_gia 
            WHERE   dtb_gia.documento = '{doc['documento']}' 
                AND dtb_gia.data = '{doc['data']}' 
                AND dtb_gia.identificador = '{doc['identificador']}' 
                AND dtb_gia.cnpj = '{doc['cnpj']}' 
        """
        mycursor.execute(sql)

        if len(mycursor.fetchall()) == 0: # INSERT

            fields = ', '.join(doc.keys())

            values = []
            for key in doc.keys():
                values.append(doc[key])
            values = tuple(values)

            sql = f"INSERT INTO dtb_gia ({fields}) VALUES {str(values)}"
            mycursor.execute(sql)
            self.mydb.commit()
            
        