import requests
from lxml.html import fromstring
import os
import ssl
import tempfile
import re
import datetime
from utils import *
import json

class GiaSp:
    
    def __init__(self, cert_path, cert_pass):
        self.s = requests.Session()
        self.sId = ''
        
        # Convertendo o certificado de pfx para pem para realizar o login
        with pfx_to_pem(cert_path, cert_pass) as cert:
            self.cert = cert
    
    def str_to_float(self, value):
        if value != '':
            money = value.split(',')
            money[0] = money[0].replace('.', '')
            money = '.'.join(money)
            return float(money)
        else:
            return 0.00

    def get_data_tables(self, page_text):

        tree = fromstring(page_text)

        data_all_months = []

        # Percorrendo tabelas
        for mes in range(12):    
                
            month = (''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_LinkButton3_{mes}"]/text()')))
            month = ''.join([i for i in month if not i.isdigit()])
            
            number_lines_table = len(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}"]/tr'))

            for linha in range(number_lines_table - 1): # - 1 devido ao header ser criado como row
                data = {}

                documento = tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}_lnkEspecificacao_{linha}"]/text()')
                if len(documento) == 0:
                    documento = tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}_lblEspecificação_{linha}"]/text()')

                observacoes = ''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}"]/tr[{linha+2}]/td[9]/descendant::*/text()')).strip()
                observacoes = observacoes.split('\n\n')
                for index, observacao in enumerate(observacoes):
                    observacoes[index] = " ".join(re.split(r"\s+", observacao))
                observacoes = ''.join(observacoes)
                observacoes = observacoes.replace('  ', '\n')    
                
                data['documento']       = ''.join(documento)
                data['data']            = ''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}_labelData_{linha}"]/text()'))
                data['banco']           = ''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}_labelBanco_{linha}"]/text()'))
                data['identificador']   = ''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}_labelIdentificador_{linha}"]/text()'))
                data['debito']          = self.str_to_float(''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}"]/tr[{linha+2}]/td[5]/descendant::*/text()')).strip())
                data['credito']         = self.str_to_float(''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}_labelCredito_{linha}"]/text()')))
                data['lanc_especial']   = self.str_to_float(''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}_labelLancamento_{linha}"]/text()')))
                data['saldo']           = self.str_to_float(''.join(tree.xpath(f'//*[@id="MainContent_rptContaFiscalMes_gdvResultadoDetalhe_{mes}"]/tr[{linha+2}]/td[8]/descendant::*/text()')).strip())
                data['observacoes']     = observacoes
                data['ano']             = datetime.datetime.now().year
                data['mes']             = month.strip()
                data['cnpj']            = ''.join(tree.xpath(f'//*[@id="MainContent_WCDadosContribuinteCompleto_lblCnpj"]/text()'))
                data['cnpj']            = ''.join(filter(str.isdigit, data['cnpj']))
                
                data_all_months.append(data)
    
        return data_all_months
        
    def access_sistema_icms_page(self, cnpj):
        url = 'https://www10.fazenda.sp.gov.br/ContaFiscalICMS/Pages/ContaFiscal.aspx?SID='+self.sId
        sistema_icms_page = self.s.get(url)

        tree = fromstring(sistema_icms_page.text)
        form_data = {}
        form_data['__EVENTTARGET'] = 'ctl00$MainContent$btnConsultar'
        form_data['__EVENTARGUMENT'] = ''
        form_data['__VIEWSTATE'] = ''.join(tree.xpath('//*[@id="__VIEWSTATE"]/@value'))
        form_data['__VIEWSTATEGENERATOR'] = ''.join(tree.xpath('//*[@id="__VIEWSTATEGENERATOR"]/@value'))
        form_data['__EVENTVALIDATION'] = ''.join(tree.xpath('//*[@id="__EVENTVALIDATION"]/@value'))
        form_data['ctl00$MainContent$hdfConteudoPopupDataInicioAtividade'] = ''
        form_data['ctl00$MainContent$txtHdfIndicesMesesAbertos'] = ''
        form_data['ctl00$MainContent$hdfHeight'] = 969
        form_data['ctl00$MainContent$ddlContribuinte'] = 'CNPJ'
        form_data['ctl00$MainContent$txtCriterioConsulta'] = cnpj
        form_data['ctl00$MainContent$ddlReferencia'] = str(datetime.datetime.now().year)
        form_data['ctl00$MainContent$hfMesReferenciaSelecionado'] = ''
        form_data['ctl00$MainContent$HistoricoAjustePagamento$keyDictionary'] = ''
        
        sistema_icms_page_with_data = self.s.post(url, data=form_data, cert=self.cert, allow_redirects=False)
        
        if 'Validation of viewstate MAC failed' in sistema_icms_page_with_data.text:
            print("Realizando refresh no login devido a um erro de viewstate.")
            self.sign_in_with_cert()
            return self.access_sistema_icms_page(cnpj)
        else:
            return self.get_data_tables(sistema_icms_page_with_data.text)

    def get_data_sistema_icms_page(self, cnpj):
        return self.access_sistema_icms_page(cnpj)

    def sign_in_with_cert(self):

        self.s = requests.Session()

        # Solicitando a página Login para capturar os dados do form
        login_page = self.s.get('https://www3.fazenda.sp.gov.br/CAWEB/Account/Login.aspx')
        tree = fromstring(login_page.text)

        form_data = {}
        form_data['ctl00$ScriptManager1'] = 'ctl00$ConteudoPagina$upnConsulta|ctl00$ConteudoPagina$btn_Login_Certificado_WebForms'
        form_data['__EVENTTARGET'] = ''
        form_data['__EVENTARGUMENT'] = '' 
        form_data['__VIEWSTATE'] = ''.join(tree.xpath('/html/body/form/input[1]/@value'))
        form_data['__VIEWSTATEGENERATOR'] = ''.join(tree.xpath('//*[@id="__VIEWSTATEGENERATOR"]/@value'))
        form_data['__VIEWSTATEENCRYPTED'] = ''
        form_data['__EVENTVALIDATION'] = ''.join(tree.xpath('//*[@id="__EVENTVALIDATION"]/@value'))
        form_data['ctl00$ConteudoPagina$rdoListPerfil'] = 2
        form_data['ctl00$ConteudoPagina$cboPerfil'] = 0 
        form_data['ctl00$ConteudoPagina$txtUsuario'] = ''
        form_data['ctl00$ConteudoPagina$txtSenha'] = ''
        form_data['g-recaptcha-response'] = ''
        form_data['__ASYNCPOST'] = True
        form_data['ctl00$ConteudoPagina$btn_Login_Certificado_WebForms.x'] = 76.40000113844872
        form_data['ctl00$ConteudoPagina$btn_Login_Certificado_WebForms.y'] = 30.600000455975533       
            
        # Realizando login com o certificado
        login_page = self.s.post(url='https://www3.fazenda.sp.gov.br/CAWEB/Account/Login.aspx', data=form_data, cert=self.cert)
        tree = fromstring(login_page.text)

        form_data = {}
        form_data['wa'] = ''.join(tree.xpath('/html/body/form/input[1]/@value'))
        form_data['wresult'] = ''.join(tree.xpath('/html/body/form/input[2]/@value'))
        form_data['wctx'] = ''.join(tree.xpath('/html/body/form/input[3]/@value'))

        login_page = self.s.post(url='https://www3.fazenda.sp.gov.br/CAWeb/pages/Default.aspx', data=form_data, cert=self.cert)    
        
        tree = fromstring(login_page.text)
        form_data = {}
        form_data['wa'] = ''.join(tree.xpath('//*[@name="proxtela"]/@value'))
        form_data['SERVICO'] = ''.join(tree.xpath('//*[@name="SERVICO"]/@value'))
        form_data['ORIGEM'] = ''.join(tree.xpath('//*[@name="ORIGEM"]/@value'))
        form_data['funcao'] = ''.join(tree.xpath('//*[@name="funcao"]/@value'))
        form_data['documento'] = ''.join(tree.xpath('//*[@name="documento"]/@value'))
        form_data['UserName'] = ''.join(tree.xpath('//*[@name="UserName"]/@value'))
        form_data['CRC'] = ''.join(tree.xpath('//*[@name="CRC"]/@value'))
        form_data['Include'] = 0
        
        login_page = self.s.post(url='https://cert01.fazenda.sp.gov.br/ca/ca', data=form_data, cert=self.cert, allow_redirects=False)
        
        # Recuperando o sId utilizado nas rotinas no site
        try:
            sId = str(login_page.headers['Location']).split('SID=')[1]
            self.sId = sId.split('&')[0]
        except:
            self.sign_in_with_cert