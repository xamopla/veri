from gia import *
from database import *
import time
from math import *
from threading import Thread
import sys

THREADS = 15
database = DataBase(sys.argv[1])
gia = None

def thread_process(empresa):
    global gia
    cnpj = empresa[1]
    cnpj = "{}.{}.{}.{}-{}".format(cnpj[0:2], cnpj[2:5], cnpj[5:8], cnpj[8:12], cnpj[12:14])
    print(f"Buscando CNPJ: {cnpj}")
    documents = gia.get_data_sistema_icms_page(cnpj)
    print(f"Encontrado {len(documents)} documentos para o cnpj {cnpj}")
    for doc in documents:
        database.insert(doc)

def main():
    global gia
    numberThread = 0
    for certificado in database.get_certs():
        
        # print(f'Realizando login com o certificado /var/www/html/crons-api/{certificado[1]}')
        gia = GiaSp(f'/var/www/html/crons-api/{certificado[1]}', certificado[2])
        gia.sign_in_with_cert()
        empresas = database.get_empresas_by_contador_id(certificado[0])
        ciclos = ceil(len(empresas)/int(THREADS))    
        print("teste")
        for ciclo in range(ciclos):
            threads = []

            for x in range(THREADS):
                if numberThread < len(empresas):
                    threads.append(Thread(target=thread_process, args=[empresas[numberThread]]))
                    numberThread += 1
            
            for x in threads:
                x.start()

            for x in threads:
                x.join()

if __name__ == "__main__":
    main()