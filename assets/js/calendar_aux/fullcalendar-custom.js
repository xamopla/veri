"use strict";
var basic_calendar = {
    init: function() {
        $('#cal-event-colors').fullCalendar({
            header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '2016-06-12',
            businessHours: true,
            editable: true,
            selectable: true,
            selectHelper: true,
            droppable: true,
            eventLimit: true,
            select: function(start, end, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    $('#cal-event-colors').fullCalendar('renderEvent',
                    {
                        title: title,
                        start: start._d,
                        end: end._d,
                        allDay: allDay
                    },
                    true
                    );
                }
                $('#cal-event-colors').fullCalendar('unselect');
            },
            events: [
            {
                title: 'All Day Event',
                start: '2016-06-01',
                color: '#7e37d8'
            },
            {
                title: 'Long Event',
                start: '2016-06-07',
                end: '2016-06-10',
                color: '#4099ff'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-06-09T16:00:00',
                color: '#4099ff'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-06-16T16:00:00',
                color: '#FF5370'
            },
            {
                title: 'Conference',
                start: '2016-06-11',
                end: '2016-06-13',
                color: '#7e37d8'
            },
            {
                title: 'Meeting',
                start: '2016-06-12T10:30:00',
                end: '2016-06-12T12:30:00',
                color: '#7e37d8'
            },
            {
                title: 'Lunch',
                start: '2016-06-12T12:00:00',
                color: '#7e37d8'
            },
            {
                title: 'Meeting',
                start: '2016-06-12T14:30:00',
                color: '#7e37d8'
            },
            {
                title: 'Happy Hour',
                start: '2016-06-12T17:30:00',
                color: '#7e37d8'
            },
            {
                title: 'Dinner',
                start: '2016-06-12T20:00:00',
                color: '#7e37d8'
            },
            {
                title: 'Birthday Party',
                start: '2016-06-13T07:00:00',
                color: '#7e37d8'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2016-06-28',
                color: '#22af47'
            }
            ]
        }), $('#external-events .fc-event').each(function() {
                $(this).css({'backgroundColor': $(this).data('color'), 'borderColor': $(this).data('color')});
                $(this).data('event', {
                    title: $.trim($(this).text()), 
                    color: $(this).data('color'),
                    stick: true 
                });
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      
                    revertDuration: 0  
                });
        }), $('#fc-external-drag').fullCalendar({
            header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            defaultDate: '2021-06-12',
            selectable: true,
            selectHelper: true,
            droppable: true,
            eventLimit: true,
            select: function(start, end, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    $('#fc-external-drag').fullCalendar('renderEvent',
                    {
                        title: title,
                        start: start._d,
                        end: end._d,
                        allDay: allDay
                    },
                    true 
                    );
                }
                $('#fc-external-drag').fullCalendar('unselect');
            },
            events: [
            {
                title: 'All Day Event',
                start: '2021-06-01',
                color: '#7e37d8'
            },
            {
                title: 'Long Event',
                start: '2021-06-07',
                end: '2021-06-10',
                color: '#22af47'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2021-06-09T16:00:00',
                color: '#22af47'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2021-06-16T16:00:00',
                color: '#ff9f40'
            },
            {
                title: 'Conference',
                start: '2021-06-11',
                end: '2021-06-13',
                color: '#FF5370'
            },
            {
                title: 'Meeting',
                start: '2021-06-12T10:30:00',
                end: '2021-06-12T12:30:00',
                color: '#FF5370'
            },
            {
                title: 'Lunch',
                start: '2021-06-12T12:00:00',
                color: '#FF5370'
            },
            {
                title: 'Meeting',
                start: '2021-06-12T14:30:00',
                color: '#FF5370'
            },
            {
                title: 'Happy Hour',
                start: '2021-06-12T17:30:00',
                color: '#FF5370'
            },
            {
                title: 'Dinner',
                start: '2021-06-12T20:00:00',
                color: '#FF5370'
            },
            {
                title: 'Birthday Party',
                start: '2021-06-13T07:00:00',
                color: '#FF5370'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2021-06-28',
                color: '#7e37d8'
            }
            ],
            drop: function() {
                if ($('#drop-remove').is(':checked')) {
                    $(this).remove();
                }
            }   
        }), $('#external-events .fc-event').each(function() {
                $(this).css({'backgroundColor': $(this).data('color'), 'borderColor': $(this).data('color')});
                $(this).data('event', {
                    title: $.trim($(this).text()),
                    color: $(this).data('color'),
                    stick: true
                });
                $(this).draggable({
                    zIndex: 999,
                    revert: true, 
                    revertDuration: 0 
                });
        }), $('#fc-external-drag').fullCalendar({
            header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            defaultDate: '2021-06-12',
            selectable: true,
            selectHelper: true,
            droppable: true,
            eventLimit: true,
            select: function(start, end, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    $('#fc-external-drag').fullCalendar('renderEvent',
                    {
                        title: title,
                        start: start._d,
                        end: end._d,
                        allDay: allDay
                    },
                    true
                    );
                }
                $('#fc-external-drag').fullCalendar('unselect');
            },
            events: [
            {
                title: 'All Day Event',
                start: '2021-06-01',
                color: '#7e37d8'
            },
            {
                title: 'Long Event',
                start: '2021-06-07',
                end: '2021-06-10',
                color: '#22af47'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2021-06-09T16:00:00',
                color: '#22af47'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2021-06-16T16:00:00',
                color: '#ff9f40'
            },
            {
                title: 'Conference',
                start: '2021-06-11',
                end: '2021-06-13',
                color: '#FF5370'
            },
            {
                title: 'Meeting',
                start: '2021-06-12T10:30:00',
                end: '2021-06-12T12:30:00',
                color: '#FF5370'
            },
            {
                title: 'Lunch',
                start: '2021-06-12T12:00:00',
                color: '#FF5370'
            },
            {
                title: 'Meeting',
                start: '2021-06-12T14:30:00',
                color: '#FF5370'
            },
            {
                title: 'Happy Hour',
                start: '2021-06-12T17:30:00',
                color: '#FF5370'
            },
            {
                title: 'Dinner',
                start: '2021-06-12T20:00:00',
                color: '#FF5370'
            },
            {
                title: 'Birthday Party',
                start: '2021-06-13T07:00:00',
                color: '#FF5370'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2021-06-28',
                color: '#7e37d8'
            }
            ],
            drop: function() {
                if ($('#drop-remove').is(':checked')) {
                    $(this).remove();
                }
            }
        });
    }
};
(function($) {
    "use strict";
    basic_calendar.init()
})(jQuery);